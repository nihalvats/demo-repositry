@isTest(SeeAllData=false)
public class PPDSurveyConnew_Test{
    public static testMethod void test1(){
        
        
        Account acc1 = new Account();
         acc1.Name='Test';
        Insert acc1;
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact cont = WCT_UtilTestDataCreation.createEmployee(rt.id);
        cont.Email = 'test.test1@deloitte.com';
        cont.Accountid= acc1.id;
        insert cont;
        
        
        Profile prof = [select id from profile where name='System Administrator'];
        User usr = new User(alias = 'usr1', email='test.test1@deloitte.com',
            emailencodingkey='UTF-8', lastname='Testing1',
            timezonesidkey='America/Los_Angeles',
            languagelocalekey='en_US',
            localesidkey='en_US', profileid = prof.Id,
            username='test.test1@deloitte.com');
        insert usr;
        
    system.runas(usr){
        
                   
      
        
        PageReference pageRef = Page.PPDSurveynew;
        Test.setCurrentPage(pageRef); 
        
        //ApexPages.currentPage().getParameters().put('type','Employee');
        
        case cas = new case();
        cas.ContactId = cont.Id;
        cas.FCPA_PPD_First_Name__c = 'jeevan';
        cas.FCPA_PPD_Last_Name__c = 'kumar';
        cas.FCPA_PPD_Email_Address__c = 'test.test1@deloitte.com';
        cas.FCPA_Candidate_First_Name__c = 'test';
        cas.FCPA_Candidate_Last_Name__c = 'test1';
        insert cas;
        
        system.assert(cas.id != null);
        
        cas.FCPA_A3__c =  null;
        cas.FCPA_A1__c = null;
        cas.FCPA_Q1_Answer__c = 'Yes';
        cas.FCPA_Q2_Answer__c = 'No';
        cas.FCPA_Q3_Answer__c = 'No';
        cas.FCPA_A_Name_of_the_requesting_third_part__c = null;
        cas.FCPA_A_His_Her_position__c = null;
        cas.FCPA_A_Organization_name__c =  null;
        cas.FCPA_A_Known_connection__c = null;
        Update cas;
       
         ApexPages.CurrentPage().getParameters().put('Param1', String.valueof(cas.id));
         Test.startTest(); 
        PPDSurveyConnew rrc = new PPDSurveyConnew();
        rrc.radio1 = 'Yes';
        rrc.radio2 = 'Yes';
        rrc.radio3 = 'Yes';
        //rrc.radio1 = null ;
        //rrc.radio3 = null;
        rrc.pageError = true;
        rrc.pageErrorMessage = 'You are not authorised to submit the form....';
        rrc.id1 = cas.id;
        rrc.mysave();
        
        Test.stopTest();
        
    }
  }
  public static testMethod void test2(){
      
       Account acc1 = new Account();
            acc1.Name='Test';
        Insert acc1;
       recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact cont = WCT_UtilTestDataCreation.createEmployee(rt.id);
        cont.Email = 'test2.usr@deloitte.com';
        cont.Accountid= acc1.id;
        insert cont;
      
      
        Profile prof = [select id from profile where name='system Administrator'];
        User usr = new User(alias = 'usr', email='test2.usr@deloitte.com',
            emailencodingkey='UTF-8', lastname='Testing2',
            timezonesidkey='America/Los_Angeles',
            languagelocalekey='en_US',
            localesidkey='en_US', profileid = prof.Id,
            username='test2.usr@deloitte.com');
        insert usr;
        
    system.runas(usr){
      
        
        PageReference pageRef = Page.PPDSurveynew;
        Test.setCurrentPage(pageRef); 
          
        
        case cas = new case();
        cas.ContactId = cont.Id;
        cas.FCPA_PPD_First_Name__c = 'jeevan';
        cas.FCPA_PPD_Last_Name__c = 'kumar';
        cas.FCPA_PPD_Email_Address__c = 'test2.usr@deloitte.com';
        cas.FCPA_Candidate_First_Name__c = 'test';
        cas.FCPA_Candidate_Last_Name__c = 'test1';
        insert cas;
        
        system.assert(cas.id != null);
        
        cas.FCPA_A3__c =  null;
        cas.FCPA_A1__c = null;
        cas.FCPA_Q1_Answer__c = 'Yes';
        cas.FCPA_Q2_Answer__c = 'No';
        cas.FCPA_Q3_Answer__c = 'No';
        cas.FCPA_A_Name_of_the_requesting_third_part__c = null;
        cas.FCPA_A_His_Her_position__c = null;
        cas.FCPA_A_Organization_name__c =  null;
        cas.FCPA_A_Known_connection__c = null;
        Update cas;
        
        ApexPages.CurrentPage().getParameters().put('Param1', String.valueof(cas.id));
        
        Test.startTest(); 
        PPDSurveyConnew rrc = new PPDSurveyConnew();
        rrc.radio1 = null;
        rrc.radio2 = 'No';
        rrc.radio3 = 'No';
        rrc.id1 = cas.id;
        rrc.mysave();
        
        Test.stopTest();
    }
  }
  public static testMethod void test3(){
      
       Account acc1 = new Account();
            acc1.Name='Test';
        Insert acc1;
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact cont = WCT_UtilTestDataCreation.createEmployee(rt.id);
        cont.Email = 'test.test3@deloitte.com';
        cont.Accountid= acc1.id;
        insert cont;
      
      
        Profile prof = [select id from profile where name='system Administrator'];
        User usr = new User(alias = 'usr', email='test.test3@deloitte.com',
            emailencodingkey='UTF-8', lastname='Testing3',
            timezonesidkey='America/Los_Angeles',
            languagelocalekey='en_US',
            localesidkey='en_US', profileid = prof.Id,
            username='test.test3@deloitte.com');
        insert usr;
        
    system.runas(usr){
             
        PageReference pageRef = Page.PPDSurveynew;
        Test.setCurrentPage(pageRef); 
        
        case cas = new case();
        cas.ContactId = cont.Id;
        cas.FCPA_PPD_First_Name__c = 'jeevan';
        cas.FCPA_PPD_Last_Name__c = 'kumar';
        cas.FCPA_PPD_Email_Address__c = 'test.test3@deloitte.com';
        cas.FCPA_Candidate_First_Name__c = 'test';
        cas.FCPA_Candidate_Last_Name__c = 'test1';
        insert cas;
        
        system.assert(cas.id != null);
        
        cas.FCPA_A3__c =  null;
        cas.FCPA_A1__c = null;
        cas.FCPA_Q1_Answer__c = 'Yes';
        cas.FCPA_Q2_Answer__c = 'No';
        cas.FCPA_Q3_Answer__c = 'No';
        cas.FCPA_A_Name_of_the_requesting_third_part__c = null;
        cas.FCPA_A_His_Her_position__c = null;
        cas.FCPA_A_Organization_name__c =  null;
        cas.FCPA_A_Known_connection__c = null;
        Update cas;
        
        ApexPages.CurrentPage().getParameters().put('Param1', String.valueof(cas.id));
        
        Test.startTest(); 
        PPDSurveyConnew rrc = new PPDSurveyConnew();
        rrc.radio1 = 'No';
        rrc.radio2 = null;
        rrc.radio3 = null;
        rrc.id1 = cas.id;
        rrc.mysave();
        
        Test.stopTest();
        
    }
  }
  public static testMethod void test4(){
      	 Account acc1 = new Account();
            acc1.Name='Test';
        Insert acc1;
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact cont = WCT_UtilTestDataCreation.createEmployee(rt.id);
        cont.Email = 'test.test4@deloitte.com';
        cont.Accountid= acc1.id;
        insert cont;
      
      
        Profile prof = [select id from profile where name='system Administrator'];
        User usr = new User(alias = 'usr', email='test.test4@deloitte.com',
            emailencodingkey='UTF-8', lastname='Testing4',
            timezonesidkey='America/Los_Angeles',
            languagelocalekey='en_US',
            localesidkey='en_US', profileid = prof.Id,
            username='test.test4@deloitte.com');
        insert usr;
        
    system.runas(usr){
       
        PageReference pageRef = Page.PPDSurveynew;
        Test.setCurrentPage(pageRef); 
        
        
        case cas = new case();
        cas.ContactId = cont.Id;
        cas.FCPA_PPD_First_Name__c = 'jeevan';
        cas.FCPA_PPD_Last_Name__c = 'kumar';
        cas.FCPA_PPD_Email_Address__c = 'test.test4@deloitte.com';
        cas.FCPA_Candidate_First_Name__c = 'test';
        cas.FCPA_Candidate_Last_Name__c = 'test1';
        insert cas;
        
        system.assert(cas.id != null);
        
        cas.FCPA_A3__c =  null;
        cas.FCPA_A1__c = null;
        cas.FCPA_Q1_Answer__c = 'Yes';
        cas.FCPA_Q2_Answer__c = 'No';
        cas.FCPA_Q3_Answer__c = 'No';
        cas.FCPA_A_Name_of_the_requesting_third_part__c = null;
        cas.FCPA_A_His_Her_position__c = null;
        cas.FCPA_A_Organization_name__c =  null;
        cas.FCPA_A_Known_connection__c = null;
        Update cas;
        
        ApexPages.CurrentPage().getParameters().put('Param1', String.valueof(cas.id));
        
        Test.startTest(); 
        PPDSurveyConnew rrc = new PPDSurveyConnew();
        rrc.radio1 = 'No';
        rrc.radio2 = 'No';
        rrc.radio3 = null;
        rrc.id1 = cas.id;
        rrc.mysave();
        rrc.surveyQuestionnew.FCPA_A3__c = null;
        Test.stopTest();
        
    }
  }
  public static testMethod void test5(){
      
       Account acc1 = new Account();
            acc1.Name='Test';
        Insert acc1;
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact cont = WCT_UtilTestDataCreation.createEmployee(rt.id);
        cont.Email = 'test.test5@deloitte.com';
        cont.Accountid= acc1.id;
        insert cont;
       
      
        Profile prof = [select id from profile where name='system Administrator'];
        User usr = new User(alias = 'usr', email='test.test5@deloitte.com',
            emailencodingkey='UTF-8', lastname='Testing5',
            timezonesidkey='America/Los_Angeles',
            languagelocalekey='en_US',
            localesidkey='en_US', profileid = prof.Id,
            username='test.test5@deloitte.com');
        insert usr;
        
    system.runas(usr){
                   
       
        
        PageReference pageRef = Page.PPDSurveynew;
        Test.setCurrentPage(pageRef); 
       
        
        case cas = new case();
        cas.ContactId = cont.Id;
        cas.FCPA_PPD_First_Name__c = 'jeevan';
        cas.FCPA_PPD_Last_Name__c = 'kumar';
        cas.FCPA_PPD_Email_Address__c = 'test.test5@deloitte.com';
        cas.FCPA_Candidate_First_Name__c = 'test';
        cas.FCPA_Candidate_Last_Name__c = 'test1';
        insert cas;
        
        system.assert(cas.id != null);
        
        cas.FCPA_A3__c =  null;
        cas.FCPA_A1__c = null;
        cas.FCPA_Q1_Answer__c = 'Yes';
        cas.FCPA_Q2_Answer__c = 'No';
        cas.FCPA_Q3_Answer__c = 'No';
        cas.FCPA_A_Name_of_the_requesting_third_part__c = null;
        cas.FCPA_A_His_Her_position__c = null;
        cas.FCPA_A_Organization_name__c =  null;
        cas.FCPA_A_Known_connection__c = null;
        Update cas;
        
        ApexPages.CurrentPage().getParameters().put('Param1', String.valueof(cas.id));
        
        Test.startTest(); 
        PPDSurveyConnew rrc = new PPDSurveyConnew();
        rrc.radio1 = 'No';
        rrc.radio2 = 'Yes';
        rrc.radio3 = 'No';
        rrc.surveyQuestionnew.FCPA_A_Name_of_the_requesting_third_part__c = null;
        rrc.surveyQuestionnew.FCPA_A_Name_of_the_requesting_third_part__c = '';
        rrc.id1 = cas.id;
        rrc.mysave();
        
        Test.stopTest();
    }
  }
  public static testMethod void test6(){
      
      Account acc1 = new Account();
            acc1.Name='Test';
        Insert acc1;
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact cont = WCT_UtilTestDataCreation.createEmployee(rt.id);
        cont.Email = 'test.test6@deloitte.com';
        cont.Accountid= acc1.id;
        insert cont;
      
        Profile prof = [select id from profile where name='system Administrator'];
        User usr = new User(alias = 'usr', email='test.test6@deloitte.com',
            emailencodingkey='UTF-8', lastname='Testing6',
            timezonesidkey='America/Los_Angeles',
            languagelocalekey='en_US',
            localesidkey='en_US', profileid = prof.Id,
            username='test.test6@deloitte.com');
        insert usr;
        
    system.runas(usr){
        PageReference pageRef = Page.PPDSurveynew;
        Test.setCurrentPage(pageRef); 
       
        //ApexPages.currentPage().getParameters().put('type','Employee');
        
        case cas = new case();
        cas.ContactId = cont.Id;
        cas.FCPA_PPD_First_Name__c = 'jeevan';
        cas.FCPA_PPD_Last_Name__c = 'kumar';
        cas.FCPA_PPD_Email_Address__c = 'test@deloitte.com';
        cas.FCPA_Candidate_First_Name__c = 'test';
        cas.FCPA_Candidate_Last_Name__c = 'test1';
        insert cas;
        
        system.assert(cas.id != null);
        
        cas.FCPA_A3__c =  null;
        cas.FCPA_A1__c = null;
        cas.FCPA_Q1_Answer__c = 'Yes';
        cas.FCPA_Q2_Answer__c = 'No';
        cas.FCPA_Q3_Answer__c = 'No';
        cas.FCPA_A_Name_of_the_requesting_third_part__c = null;
        cas.FCPA_A_His_Her_position__c = null;
        cas.FCPA_A_Organization_name__c =  null;
        cas.FCPA_A_Known_connection__c = null;
        Update cas;
        //string ac
        ApexPages.CurrentPage().getParameters().put('Param1', String.valueof(cas.id));
        
       
        
        Test.startTest(); 
        PPDSurveyConnew rrc = new PPDSurveyConnew();
        rrc.radio1 = 'No';
        rrc.radio2 = 'No';
        rrc.radio3 = 'Yes';
        rrc.surveyQuestionnew.FCPA_A3__c = null;
        //rrc.surveyQuestionnew.FCPA_A3__c = '';
        rrc.id1 = cas.id;
        rrc.mysave();
        
        Test.stopTest();
    }
  }
  
}