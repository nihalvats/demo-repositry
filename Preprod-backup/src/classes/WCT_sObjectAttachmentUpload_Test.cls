/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class WCT_sObjectAttachmentUpload_Test {

    public static Contact Candidate;
    public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
 
    /** 
        Method Name  : createCandidate
        Return Type  : Contact
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createCandidate()
    {
        Candidate=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        insert Candidate;
        return  Candidate;
    } 

    static testMethod void noParentTest() {
        
         WCT_sObjectAttachmentUpload_ctrl aup= new  WCT_sObjectAttachmentUpload_ctrl();
        
    }
    
    static testMethod void noBodyTest() {
        Candidate = createCandidate();
        test.startTest();
        ApexPages.currentPage().getParameters().put('id',Candidate.Id);
        WCT_sObjectAttachmentUpload_ctrl aup= new  WCT_sObjectAttachmentUpload_ctrl();
        aup.attFive.name = 'Fifth Attachment';
        aup.uploadAttachments();
        test.stopTest();
        
    }
    
    static testMethod void withParentUpload(){
        Candidate = createCandidate();
        test.startTest();
        ApexPages.currentPage().getParameters().put('id',Candidate.Id);
        WCT_sObjectAttachmentUpload_ctrl aup= new WCT_sObjectAttachmentUpload_ctrl();
        aup.attOne.name = 'First Attachment';
        aup.attTwo.name = 'Second Attachment';
        aup.attThree.name = 'Third Attachment';
        aup.attFour.name = 'Fourth Attachment';
        aup.attFive.name = 'Fifth Attachment';
        
        aup.attOne.body = Blob.valueOf('Unit Test Attachment Body');
        aup.attTwo.body = Blob.valueOf('Unit Test Attachment Body');
        aup.attThree.body = Blob.valueOf('Unit Test Attachment Body');
        aup.attFour.body = Blob.valueOf('Unit Test Attachment Body');
        aup.attFive.body = Blob.valueOf('Unit Test Attachment Body');
        
        aup.uploadAttachments();
        aup.goToParentPage();
        test.stopTest();
    }

}