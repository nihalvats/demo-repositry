// vfpage : PPDsearchPage
public class PPDsearchCon {

    public List<case> lstcon{get; set;}
    public string searchText{get; set;}
    
    public PPDsearchCon() {
        lstcon = [select id, CaseNumber, FCPA_PPD_First_Name__c, FCPA_PPD_Last_Name__c, FCPA_Candidate_First_Name__c, FCPA_Candidate_Last_Name__c, 
                    status, FCPA_Recruiter_Name__c from Case where recordtype.name =: 'PPD-Referral' ];
    }
    
    public void dosearch(){
        lstcon = [select id, CaseNumber, FCPA_PPD_First_Name__c, FCPA_PPD_Last_Name__c,FCPA_Candidate_First_Name__c,FCPA_Candidate_Last_Name__c,
                    status,FCPA_Recruiter_Name__c from case where (FCPA_PPD_First_Name__c like:searchtext+'%' OR FCPA_PPD_Last_Name__c like:searchtext+'%' 
                    OR FCPA_Candidate_First_Name__c like:searchtext+'%' OR FCPA_Candidate_Last_Name__c like:searchtext+'%' OR
                    FCPA_Recruiter_Name__c like:searchtext+'%') And recordtype.name =: 'PPD-Referral'];
    }
    
    public void viewall(){
        searchtext='';  
        lstcon = [Select id, CaseNumber, FCPA_PPD_First_Name__c, FCPA_PPD_Last_Name__c,FCPA_Candidate_First_Name__c,FCPA_Candidate_Last_Name__c,
                    status,FCPA_Recruiter_Name__c from Case where recordtype.name =: 'PPD-Referral' ];
    }
    
}