/**************************************************************************************
Batch class       :  ELE_Batch_Clerance_NotificationSend
Version          : 1.0 
Created Date     : 25 April 2015
Function         : Batch to send notifications to stakeholders
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   25/04/2015            Original Version
*************************************************************************************/

global class ELE_Batch_Clerance_NotificationSend implements Database.Batchable < sObject > , Database.AllowsCallouts, Database.StateFul {

    List < Clearance_separation__c > lstclr = new List < Clearance_separation__c > ();
    //global Datetime r1,r2,r3; 
    global string sysTime = system.today().format();
    global String sPrimary = System.Label.ELE_Primary_Stakeholder_Label;
    global String sSecondary= System.Label.ELE_Secondary_Stakeholders_Label;

    /**
        Start method to return list of clearance records with notifications
    */
    global Database.QueryLocator start(Database.BatchableContext BC) {
      
      String query = 'select Name, createddate,ELE_Separation__c, ELE_Additional_Fields__c,ELE_Clearance_Authority_Type__c, ELE_Notification_Sent__c, ELE_ContactId__c,ELE_Notification_Reminder_1__c, ELE_Notification_Reminder_2__c,ELE_Notification_Reminder_3__c,ELE_Stakeholder_Designation__c,ELE_Reminder1_Time__c,ELE_Reminder2_Time__c,ELE_Reminder3_Time__c from Clearance_separation__c where ((ELE_Stakeholder_Designation__c != \'Closed\') and((ELE_Reminder1_Time__c <= today And(ELE_Notification_Reminder_1__c = false)) OR ((ELE_Reminder2_Time__c <= today) And(ELE_Notification_Reminder_1__c = true) AND(ELE_Notification_Reminder_2__c = false)) OR ((ELE_Reminder3_Time__c <= today) And(ELE_Notification_Reminder_1__c = true) AND(ELE_Notification_Reminder_2__c = true) AND(ELE_Notification_Reminder_3__c = false))))';
         
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Clearance_separation__c > Clr) {

        lstclr = new List < Clearance_separation__c > ();
        Map < string, integer > map_teamWithOpencase1 = new map < string, integer > ();
        Map < string, integer > map_teamWithOpencase2 = new map < string, integer > ();
        Map < string, integer > map_teamWithOpencase3 = new map < string, integer > ();
        map < string, Map < string, integer >> map_reminder_value = new map < string, Map < string, integer >> ();
        
        //Remainder 1 notification map
        AggregateResult[] groupedResults1 = [select ELE_Stakeholder_Designation__c, count(ID) ELE_Status__c from Clearance_separation__c where ELE_Clearance_Authority_Type__c = :sSecondary and ELE_Status__c != 'Closed'
            and((ELE_Reminder1_Time__c <= today And(ELE_Notification_Reminder_1__c = false))) group by ELE_Stakeholder_Designation__c
        ];
        for (AggregateResult arr: groupedResults1) {
            system.debug('>>>>>>>>>>>>>>>>' + arr.get('ELE_Stakeholder_Designation__c') + '>>>>' + arr.get('ELE_Status__c'));
            map_teamWithOpencase1.put((String) arr.get('ELE_Stakeholder_Designation__c'), (Integer) arr.get('ELE_Status__c'));

        }
        map_reminder_value.put('Reminder 1', map_teamWithOpencase1);

        //Remainder 2 notification map
        AggregateResult[] groupedResults2 = [select ELE_Stakeholder_Designation__c, count(ID) ELE_Status__c from Clearance_separation__c where ELE_Clearance_Authority_Type__c =:sSecondary and ELE_Status__c != 'Closed'
            and(
                ((ELE_Reminder2_Time__c <= today) And(ELE_Notification_Reminder_1__c = true) AND(ELE_Notification_Reminder_2__c = false))
            ) group by ELE_Stakeholder_Designation__c
        ];
        for (AggregateResult arr: groupedResults2) 
        {
            system.debug('>>>>>>>>>>>>>>>>' + arr.get('ELE_Stakeholder_Designation__c') + '>>>>' + arr.get('ELE_Status__c'));
            map_teamWithOpencase2.put((String) arr.get('ELE_Stakeholder_Designation__c'), (Integer) arr.get('ELE_Status__c'));
        }
        system.debug('>>>>>>>>>>>>>Inside batch >map_reminder_value R2>' + map_reminder_value);

        map_reminder_value.put('Reminder 2', map_teamWithOpencase2);

        //Remainder 3 notification map
        AggregateResult[] groupedResults3 = [select ELE_Stakeholder_Designation__c, count(ID) ELE_Status__c from Clearance_separation__c where ELE_Clearance_Authority_Type__c =:sSecondary and ELE_Status__c != 'Closed'
            and((ELE_Reminder3_Time__c <= today) And(ELE_Notification_Reminder_1__c = true) AND(ELE_Notification_Reminder_2__c = true) AND(ELE_Notification_Reminder_3__c = false))
            group by ELE_Stakeholder_Designation__c ];
        for (AggregateResult arr: groupedResults3) {
            system.debug('>>>>>>>>>>>>>>>>' + arr.get('ELE_Stakeholder_Designation__c') + '>>>>' + arr.get('ELE_Status__c'));
            map_teamWithOpencase3.put((String) arr.get('ELE_Stakeholder_Designation__c'), (Integer) arr.get('ELE_Status__c'));
        }

        map_reminder_value.put('Reminder 3', map_teamWithOpencase3);
        system.debug('>>>>>>>>>>>>>Inside batch >map_reminder_value r3>' + map_reminder_value);
        system.debug('>>>>>>>>>>>>>Inside batch >map_teamWithOpencase>' + map_teamWithOpencase3);
        ELE_CreateClearanceClass.SendReminderEmails(map_reminder_value);
        Date today = system.today();
        set<string> eleId = new set<string>();
        Map<id,string> mapContactId = new Map<id,string>();
        List<String> sAdditionalEmail = new List<String>();

        for(Clearance_separation__c clrObj : clr)
        {
            eleId.add(clrObj.ELE_Separation__c); 
            if(clrObj.ELE_Additional_Fields__c != null )
               sAdditionalEmail = clrObj.ELE_Additional_Fields__c.split('\\,');
        }

        List<ELE_Separation__c> eleLst = new List<ELE_Separation__c>([select id,name,ELE_Contact__r.id from ELE_Separation__c where id in :eleId]);
        for(ELE_Separation__c eObj : eleLst)
        {
            mapContactId.put(eObj.id,eObj.ELE_Contact__r.id);
        }

        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>(); 
              
        for (Clearance_separation__c ClrRecord: Clr) {

            if(ClrRecord.ELE_Clearance_Authority_Type__c == 'Primary Stakeholder')
            {
                emails.add(ELE_CreateClearanceClass.sendEmailNotifications(ClrRecord,System.label.PrimaryStakeholder_Reminder_TemplateId, ClrRecord.ELE_ContactId__c , sAdditionalEmail, '', System.label.ELE_DisplayName));
            }
             system.debug('Clr>>>>>>>>>>>>>' + ClrRecord.ELE_Reminder1_Time__c + '  system . today '+system.today()+' ***** '+ClrRecord.ELE_Notification_Reminder_1__c);
            if ((ClrRecord.ELE_Reminder1_Time__c <= today) && (ClrRecord.ELE_Notification_Reminder_1__c = false)) {
                ClrRecord.ELE_Notification_Reminder_1__c = true;
            }
            if ((ClrRecord.ELE_Reminder2_Time__c <= today) && (ClrRecord.ELE_Notification_Reminder_1__c = true) && (ClrRecord.ELE_Notification_Reminder_2__c = false)) {
                ClrRecord.ELE_Notification_Reminder_2__c = true;
            } else if ((ClrRecord.ELE_Reminder3_Time__c <= today) && (ClrRecord.ELE_Notification_Reminder_1__c = true) && (ClrRecord.ELE_Notification_Reminder_2__c = true) && (ClrRecord.ELE_Notification_Reminder_3__c = false)) {
                ClrRecord.ELE_Notification_Reminder_3__c = true;
            }
            lstclr.add(ClrRecord);

        }
        if(emails.size() > 0)
        {
            try
            {
              List<Messaging.SendEmailResult> results =  Messaging.sendEmail(emails);
            }
            catch(Exception e)
            {

            }
        }
        if (lstclr != null && lstclr.size() > 0) {
           
                  update lstclr;
        }
    
    }

    global void finish(Database.BatchableContext BC) {

    }
}