//5 days before the 'Assignment Start Date' field
//14 days before the 'Last Working Date' field
//3 days before the 'Travel End Date' field

global class WCT_MobilityRecordEmailSchedulableBatch implements Database.Batchable<sObject>,Schedulable, Database.Stateful{

    public string strSql{get;set;}
//-------------------------------------------------------------------------------------------------------------------------------------
//    Constructor
//-------------------------------------------------------------------------------------------------------------------------------------
    
    public WCT_MobilityRecordEmailSchedulableBatch(){

    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Scheduler Execute 
//-------------------------------------------------------------------------------------------------------------------------------------
    global void execute(SchedulableContext SC) {
        WCT_MobilityRecordEmailSchedulableBatch batch = new  WCT_MobilityRecordEmailSchedulableBatch();
        ID batchprocessid = Database.executeBatch(batch,200);
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Set the query
//-------------------------------------------------------------------------------------------------------------------------------------
    global Database.QueryLocator start(Database.BatchableContext bc){
    
        string strSql = 'SELECT Id, WCT_Travel_Start_Date__c, WCT_Last_Working_Day_in_US__c, WCT_Travel_End_Date__c, WCT_Project_Controller__c,'+
                        ' WCT_US_Project_Mngr__c, WCT_Mobility_Employee__c, WCT_Is_Last_Working_Day_In_The_US_Alert__c,'+
                        ' WCT_USI_Resource_Manager__c, WCT_Is_Travel_End_Date_Alert__c, WCT_Is_Travel_Start_Date_Alert__c FROM WCT_Mobility__c'+
                        ' WHERE (WCT_Last_Working_Day_in_US__c <= '+ string.valueof(system.today().addDays(14)) + 
                        ' OR WCT_Travel_End_Date__c <= '+ string.valueof(system.today().addDays(3)) +') AND WCT_Mobility_Employee__r.RecordType.Name = \'Employee\''; 
        return database.getQuerylocator(strSql);   
       
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Database execute method to Process Query results for email notification
//-------------------------------------------------------------------------------------------------------------------------------------
    global void execute(Database.batchableContext bc, List<sObject> scope){
    
        List<WCT_Mobility__c> lstMobility = new List<WCT_Mobility__c>();
        for(sObject tmp : scope){
            WCT_Mobility__c tmpMob = new WCT_Mobility__c();
            tmpMob = (WCT_Mobility__c)tmp;
            lstMobility.add(tmpMob);
            if(tmpMob.WCT_Last_Working_Day_in_US__c <= system.today().addDays(14))
                tmpMob.WCT_Is_Last_Working_Day_In_The_US_Alert__c = true;
            else if(tmpMob.WCT_Travel_End_Date__c <= system.today().addDays(3)) 
                tmpMob.WCT_Is_Travel_End_Date_Alert__c= true;
                 
        }
        Database.SaveResult[] sr = Database.update(lstMobility, false);
    }
    
    global void finish(Database.batchableContext info){     
   
    }     
}