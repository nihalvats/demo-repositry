/************
* This class is coded to test Apex Trigger on Task "Wct_OpentaskEmail_count "


*/

@isTest
public class Wct_OpentaskEmail_count_test
{
    public static testmethod void Taskcreatedusinginbound()
    { 
 
     Contact conRec = WCT_UtilTestDataCreation.createContactRec();
      insert conRec;
 
     WCT_Outreach__c outr= WCT_UtilTestDataCreation.createoutreach();
      insert outr;
 
     schema.WCT_Outreach_Activity__c outra= new  schema.WCT_Outreach_Activity__c();
     outra.WCT_Contact__c =conrec.id;
     outra.WCT_Outreach__c=outr.id;
      insert outra;
 
     Task t= new Task();
      t.Subject='call';
      t.WhatId=outra.id;
      t.whoid=conrec.id;
      t.Status='not started';
      t.email_status__c='replied';
      t.Email_Read_Unread__c = true;
       insert t;
 
     Task t1= new Task();
      t1.Subject='call';
      t1.WhatId=outra.id;
      t1.whoid=conrec.id;
      t1.email_status__c='replied';
      t1.Status='Not Started';
      t1.Email_Read_Unread__c = true;
       insert t1;
       
       
     t1.Email_Read_Unread__c = false;
      update t1;
      t1.Email_Read_Unread__c = false;
      update t1;
    
      delete t;
     }
   }