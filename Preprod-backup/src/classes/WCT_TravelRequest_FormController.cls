/********************************************************************************************************************************
Apex class         : <WCT_TravelRequest_FormController>
Description        : <Controller which allows to Update Port of Entry Details>
Type               :  Controller
Test Class         :  <WCT_TravelRequest_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 20/05/2016          80%                          --                            License Cleanup Project
************************************************************************************************************************************/  
public with sharing class WCT_TravelRequest_FormController extends SitesTodHeaderController{

   /* Creating a Port of Entry Instance */
    
   public WCT_Port_of_Entry__c portRecord {get;set;} 

   /* Date Fields related to Port of Entry and Travel Dates */
   
    public WCT_Port_of_Entry__c getFields{get;set;}
    public String PortStart{set;get;}
    public String PortEnd{set;get;}
    public String PortStart2{set;get;}
    public String PortEnd2{set;get;}
    public String PortStart3{set;get;}
    public String PortEnd3{set;get;}
    public String PortStart4{set;get;}
    public String PortEnd4{set;get;}
    public String PortStart5{set;get;}
    public String PortEnd5{set;get;}
    public List < SelectOption > ggsOptions {set;get;}
    public String ggsValue {set;get;} 
    public String travelstartdate {get; set;}     
    public String travelenddate { get; set;}  
    
    /* Error Message Related Variables */
   
    public boolean pageError {get; set;}
    public String  pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;} 
    
 
    /* Employee Related Variables */
    public Contact Employee{get;set;}
    public String  employeeEmail{set;get;}
    public boolean employeePresent{set;get;}
    public boolean employeePrevious{set;get;}
    public boolean getEmployeeInfo{set;get;}
    public String  employeeType;
    
   /*Initializing Constructor */

     public WCT_TravelRequest_FormController() {
      init();
      getParameterInfo();
      getEmployeeInfo = true;
      if (invalidEmployee) {
       return;
      }
    
     }

/********************************************************************************************
*Method Name         : <init()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Init() Used for Initializing Attachments and Passport>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/   
 
     public void init() {
      portRecord = new WCT_Port_of_Entry__c();
      ggsOptions = new List < SelectOption > ();
        ggsOptions.add(new SelectOption('', 'None'));
        ggsOptions.add(new SelectOption('Yes', 'Yes'));
        ggsOptions.add(new SelectOption('No', 'No'));
        
          ggsValue = 'None';
     }

/********************************************************************************************
*Method Name         : <getParameterInfo()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetParameterInfo() Used to get Parameter Information>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 
  
     @TestVisible
     private void getParameterInfo() {
      employeeType = ApexPages.currentPage().getParameters().get('type');
     }

/********************************************************************************************
*Method Name         : <getEmployeeDetails()>
*Return Type         : <pageReference>
*Param’s             : 
*Description         : <GetEmployeeDetails() Used for Confirming the email address of the employee>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/    
  
     public pageReference getEmployeeDetails() {
      try {
       Recordtype rt = [select id from Recordtype where DeveloperName = 'WCT_Employee'];
       employeeEmail = employeeEmail.trim();
    
       if ((employeeEmail == null) || (employeeEmail == '')) {
        pageErrorMessage = 'Please enter a email ID';
        pageError = true;
        Employee = new contact();
        employeePresent = false;
        return null;
       } else {
    
        Employee = [select id, name, FirstName, LastName, Email, WCT_Personnel_Number1__c, WCT_Function__c, WCT_Service_Area__c, WCT_Service_Line__c, WCT_Region__c, WCT_Designation_Code__c from contact where recordtypeid = : rt.id and email = : employeeEmail limit 1];
        employeePresent = true;
        getEmployeeInfo = false;
    
       }
      } catch (Exception e) {
       WCT_ExceptionUtility.logException('WCT_TravelRequest_FormController', 'Travel Request Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
       pageErrorMessage = 'Email Id is not associated with any employee';
       pageError = true;
       employee = new contact();
       employeePresent = false;
       return null;
      }
      pageError = false;
      return null;
     }

/********************************************************************************************
*Method Name         : <cancelPortRecord()>
*Return Type         : <pageReference>
*Param’s             : 
*Description         : <CancelPortRecord() Used for redirecting to Onboarding Mobility Page-Business Visa>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/
 
     public pageReference cancelPortRecord() {
    
      return new PageReference('/apex/WCT_Mobility_Onboarding_Form?type=' + employeeType);
    
     }

/********************************************************************************************
*Method Name         : <savePortRecord()>
*Return Type         : <Page Reference>
*Param’s             : 
*Description         : <SavePortRecord() Used for Upserting POE Record>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/   

     public pageReference savePortRecord() {
      
      try{
      if ((null != PortStart) && ('' != PortStart) && (null != PortEnd) &&  ('' != PortEnd) &&
       (null != travelstartdate) &&
       ('' != travelstartdate) &&
       (null != travelenddate) &&
       ('' != travelenddate) &&
       (null != portRecord.WCT_Purpose__c) &&
       ('' != portRecord.WCT_Purpose__c) &&
       (null != portRecord.WCT_Port_of_Entry_City__c) &&
       ('' != portRecord.WCT_Port_of_Entry_City__c) &&
       (null != portRecord.WCT_Port_of_Entry_State__c) &&
       ('' != portRecord.WCT_Port_of_Entry_State__c)) {
    
       portRecord.WCT_Email_Address__c = employeeEmail;
       pageError = false;
    
    
       if (String.isNotBlank(travelstartdate)) {
        portRecord.WCT_Travel_Start_Date__c = Date.parse(travelstartdate);
       } else {
        portRecord.WCT_Travel_Start_Date__c = null;
       }
       if (String.isNotBlank(travelenddate)) {
        portRecord.WCT_Travel_End_Date__c = Date.parse(travelenddate);
       } else {
        portRecord.WCT_Travel_End_Date__c = null;
       }
    
       if (String.isNotBlank(PortStart)) {
        portRecord.WCT_Start_Date__c = Date.parse(PortStart);
       } else {
        portRecord.WCT_Start_Date__c = null;
       }
       if (String.isNotBlank(PortEnd)) {
        portRecord.WCT_End_Date__c = Date.parse(PortEnd);
       } else {
        portRecord.WCT_End_Date__c = null;
       }
       if (String.isNotBlank(PortStart2)) {
        portRecord.WCT_Start_Date2__c = Date.parse(PortStart2);
       } else {
        portRecord.WCT_Start_Date2__c = null;
       }
    
       if (String.isNotBlank(PortEnd2)) {
        portRecord.WCT_End_Date2__c = Date.parse(PortEnd2);
       } else {
        portRecord.WCT_End_Date2__c = null;
       }
       if (String.isNotBlank(PortStart3)) {
        portRecord.WCT_Start_Date3__c = Date.parse(PortStart3);
       } else {
        portRecord.WCT_Start_Date3__c = null;
       }
       if (String.isNotBlank(PortEnd3)) {
        portRecord.WCT_End_Date3__c = Date.parse(PortEnd3);
       } else {
        portRecord.WCT_End_Date3__c = null;
       }
       if (String.isNotBlank(PortStart4)) {
        portRecord.WCT_Start_Date4__c = Date.parse(PortStart4);
       } else {
        portRecord.WCT_Start_Date4__c = null;
       }
    
       if (String.isNotBlank(PortEnd4)) {
        portRecord.WCT_End_Date4__c = Date.parse(PortEnd4);
       } else {
        portRecord.WCT_End_Date4__c = null;
       }
    
       if (String.isNotBlank(PortStart5)) {
        portRecord.WCT_Start_Date5__c = Date.parse(PortStart5);
       } else {
        portRecord.WCT_Start_Date5__c = null;
       }
    
    
       if (String.isNotBlank(PortEnd5)) {
        portRecord.WCT_End_Date5__c = Date.parse(PortEnd5);
       } else {
        portRecord.WCT_End_Date5__c = null;
       }
       recordtype rt = [select id from recordtype where DeveloperName = 'WCT_Employee'];
       Contact conRecord = [SELECT Id FROM Contact WHERE email = : portRecord.WCT_Email_Address__c AND recordtypeid = : rt.id];
    
       portRecord.WCT_Contact_ID__c = conRecord.id;
    if( portRecord.WCT_USI_Resource_Manager__c != null) { 
    Contact empRecord = [SELECT Name FROM Contact WHERE email = : portRecord.WCT_USI_Resource_Manager__c AND recordtypeid = : rt.id];
    
       portRecord.WCT_Resource_Manager_Name__c = empRecord.Name;
    }
    system.debug('@testing'+ggsValue);
    if((null== ggsValue)) {
    system.debug('@testing123'+ggsValue);
    pageErrorMessage = 'Please fill in all the required fields on the form.';
       pageError = true; 
       return null;
       }
       system.debug('@testing1'+ggsValue);
    portRecord.Do_you_belong_to_Strategy_and_Operations__c= ggsValue; 
       insert portRecord;
       system.debug('@testing2'+ggsValue);
       
        Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_TREQ_MSG');
        pg.setRedirect(true);
        return pg;
        
      
      } 
      else {
       pageErrorMessage = 'Please fill in all the required fields on the form.';
       pageError = true;
       return null;
      
     }
     
      
       
     }
     catch (Exception e) {
                Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_TravelRequest_FormController', 'Travel Request_Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_TLRT_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
     }
    
    }
    }