global class WCT_H1bcap_Prac_No_Resp_Schedule implements Schedulable {

     global void execute(SchedulableContext sc) {

       //Define  batch size.       

       integer BATCH_SIZE = 1; 

      WCT_H1bcap_Prac_No_Resp  sndBatch = new WCT_H1bcap_Prac_No_Resp ();
     system.debug('****WCT_H1bcap_Prac_No_Resp  : starting batch exection*****************');

     Id batchId = database.executeBatch(sndBatch , BATCH_SIZE);   

  system.debug('**** WCT_H1bcap_Prac_No_Resp : Batch executed batchId: '+batchId);

   }
 }