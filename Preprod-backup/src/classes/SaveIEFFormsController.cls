public with sharing class SaveIEFFormsController {
    
   public ApexPages.StandardSetController standardSetController;
   public List<WCT_Interview_Junction__c> records;
   public List<WCT_Interview_Junction__c> temprecords;
   public WCT_Interview__c intr;
   public string intrid {get;set;}
   
   public SaveIEFFormsController(ApexPages.StandardSetController standardSetController){
        this.records = standardSetController.getSelected();
        if(!this.records.isEmpty()) {
            standardSetController.setPageSize(this.records.size());
            /*get FSS, User Group, Job Type from Interview*/
            intrid=ApexPages.currentPage().getParameters().get('retURL');
            if(intrid!='' || intrid!=null)
            {
            integer lgth=intrid.length();
            intrid=intrid.substring(1,lgth);
            intr=[select WCT_Job_Type__c,WCT_FSS__c,WCT_User_group__c from WCT_Interview__c where id=:intrid];
            }
        } 
        /*get FSS, User Group, Job Type from Interview*/

        for(WCT_Interview_Junction__c i:records)
        {
        intrid=intr.WCT_Job_Type__c;
        if(i.WCT_Job_Type2__c==null || i.WCT_FSS__c==null || i.WCT_User_group__c== null )
        {
            i.WCT_User_group__c=intr.WCT_User_group__c;
            i.WCT_Job_Type2__c=intr.WCT_Job_Type__c;
            i.WCT_FSS__c=intr.WCT_FSS__c;

            if(temprecords==null)
            {
                    temprecords=new list<WCT_Interview_Junction__c>();
            }
            temprecords.add(i);
            }
         }
        //records=temprecords;
        //update temprecords;
       standardSetController.setSelected(this.records); 
     // this.records=temprecords;
     // standardSetController.set;
      
   }
public pagereference updaterec()
{
// updating the interview tracker records in order to persist FSS, User Group, Job Type from Interview and display it
if(temprecords!=null)
{
 update temprecords;
  return ApexPages.currentPage();
}
else
{
 return null;
}

}
   public PageReference doSomething(){
        // Apex code for handling records from a List View goes here
        PageReference pageRef = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
        return pageRef;
   }       
}