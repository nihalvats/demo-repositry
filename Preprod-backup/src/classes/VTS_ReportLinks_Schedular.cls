/*Developer : Deepu Ginde*/

global class VTS_ReportLinks_Schedular implements Schedulable {
   global void execute(SchedulableContext SC) 
   {
      
       
       /*
        * Updated Code Logic: 
        *   Step 1 : Query the GroupMember for group name present in Custom Setting -VTS Report Link Group Mapping . 
        *            Map the Report Link Type and list of User in each respective group.
        *   Step 2 : Loop Through the Each Report Link Type from report Link Map. And Execute Step 3 for every iteration.
        * 
        * 	Step 3 : Get the template Id and send the email to all identified users.
        * 
		*/
       
       
       /*STEP 1*/
       String userType = Schema.SObjectType.User.getKeyPrefix();
       String groupType = Schema.SObjectType.Group.getKeyPrefix();


       /*Step 1:
        *  
        */
       List<VTS_Report_Link_Group_Mapping__c> reporyLinksMappings= VTS_Report_Link_Group_Mapping__c.getAll().values();
       
       Map<String, string> mapGroupReportType= new Map<String, string>();
       Map<String, string> mapGroupEmailTemp= new Map<String, string>();
       Map<String, List<ID>> groupNameUsersMapping= new Map<String, List<ID>>();
       
       for(VTS_Report_Link_Group_Mapping__c reporyLinksMapping : reporyLinksMappings)
       {
           
           if(reporyLinksMapping.Name!=null && reporyLinksMapping.Name!='' && reporyLinksMapping.Report_Link_Type__c!=null && reporyLinksMapping.Report_Link_Type__c!='' && reporyLinksMapping.Email_Template_Id__c!=null && reporyLinksMapping.Email_Template_Id__c!='')
           {
              mapGroupReportType.put(reporyLinksMapping.Name, reporyLinksMapping.Report_Link_Type__c);
               
              mapGroupEmailTemp.put(reporyLinksMapping.Name, reporyLinksMapping.Email_Template_Id__c );
           }
       }
       system.debug('#### mapGroupReportType '+mapGroupReportType);
       system.debug('#### mapGroupEmailTemp '+mapGroupEmailTemp);
       
        for(GroupMember gm : [SELECT Id, group.name, group.type, UserOrGroupId FROM GroupMember where (group.type='Regular' and group.name in :mapGroupReportType.keySet())])
        {
            String groupName=gm.group.name;
            String ReportType= mapGroupReportType.get(groupName);
            if(ReportType!=null)
            {
                List<String> UserIds=groupNameUsersMapping.get(groupName)==null?new List<String>():groupNameUsersMapping.get(groupName);
                System.debug('### UserIds from existing if any '+UserIds);
                String userGroup=(string)gm.UserOrGroupId;
                System.debug('### userGroupID '+userGroup);
                if(userGroup.startsWithIgnoreCase(userType))
                {
                    UserIds.add(gm.UserOrGroupId);
                }
                else if(userGroup.startsWithIgnoreCase(groupType))
                {
                    /*This section is to handle the Roles added to the public group. 
                        To get the Role Id added we need to Query the group & relatedId gives the Role ID. 
                    */
                    List<Group> groupWithRole=[Select Id, RelatedId From Group where Id=:userGroup];
                    String roleId= groupWithRole.size()>0?groupWithRole[0].RelatedId:null;
                    Boolean isValidRole=roleId!=null?roleId.startsWithIgnoreCase('00E'):false;
                    if(isValidRole)
                    {
                        List<User> roleUsers= [Select Id from User where UserRole.Id=:roleId];
                        for(User tempUser : roleUsers)
                        {
                            UserIds.add(tempUser.ID);
                        }
                    }
                }
                system.debug('### groupName '+groupName+'#### User Ids '+UserIds);
               groupNameUsersMapping.put(groupName, UserIds);
            }
        }
       system.debug('#### groupNameUsersMapping '+groupNameUsersMapping);
       
       /*Step 2 */
       
       
       String orgURL=URL.getSalesforceBaseUrl().toExternalForm();
       for(String groupName : mapGroupReportType.keySet())
       {
           
           List<ID> userList= groupNameUsersMapping.get(groupName)==null?new List<ID>():groupNameUsersMapping.get(groupName) ;
           System.debug('#### userList '+userList);
           List<Messaging.SingleEmailMessage> messages= new List<Messaging.SingleEmailMessage>();
           String emailTemplateId= mapGroupEmailTemp.get(groupName);
           if(emailTemplateId!=null)
           {
               for(ID userId:userList)
               {
                  
                   Messaging.SingleEmailMessage email=new Messaging.SingleEmailMessage(); 
                   email.setTargetObjectId(userId);
                   email.setSaveAsActivity(false);
                   email.setTemplateId(emailTemplateId);
                   messages.add(email);
               }
           }
           
           if(messages.size()>0)
           {
               System.debug('## '+messages);
               try
               {
               		Messaging.SendEmail(messages);
                   WCT_ExceptionUtility.logException('VTS_ReportLinks_Schedular','execute','Not an Error : status '+groupName+' Count of Emails '+messages.size());
               }
               catch(Exception e)
               {
                    WCT_ExceptionUtility.logException('VTS_ReportLinks_Schedular','execute',e.getMessage()+' Line # '+e.getLineNumber());
               }
           }
       }

		
    }
}