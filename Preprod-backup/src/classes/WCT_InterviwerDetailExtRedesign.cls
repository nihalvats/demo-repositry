public class WCT_InterviwerDetailExtRedesign {
    String interviewJuncId ;
    WCT_Interview_Junction__c interviewJunction;
    public String nameFile{get;set;}
    public Blob contentFile{get;set;}
    public string IntvDate {get;set;}
    public string IntvStrTime {get;set;}
    public string IntvEndTime {get;set;}
    public string IntvSubmittedDate {get;set;}
    string IntviewId {get;set;}
    string candidatTrackerId {get;set;}
    public string label{get;set;}
    public string Intdue{get;set;}
    public string GrpInt{get;set;}
    public boolean attachmentSection {get; set;}
    public boolean removeAttachment{get; set;}
    public string candidateId;
    public integer actisze{get;set;}
    public integer upsze{get;set;}
    public integer allsze{get;set;}
    public integer pendsze{get;set;}
    public List<wrapcandnamewithattmnts> attachmentsWrapper  {get;set;}
    public List<wrapcandnamewithattmnts> attachmentsdocWrapper  {get;set;}
    public List<wrapcandnamewithattmnts> attachmentscsdWrapper  {get;set;}
    public List<wrapcandnamewithReq> RequistionNCanInf  {get;set;}
    public list<Feedback__c> fdbklistall{get;set;}
    public list<Feedback__c> fdbklist{get;set;}
    public list<id> cntid=new list<id>();
    public list<WCT_Interview_Junction__c> intjunction=new list<WCT_Interview_Junction__c>();
    // pop up code starts
    public boolean displayPopup {get; set;}     
    
    public void closePopup() {        
        displayPopup = false;    
    }     
    public void showPopup() {        
        displayPopup = true;    
    }
    // pop up code ends
    public WCT_InterviwerDetailExtRedesign(ApexPages.StandardController controller) {
        
        WCT_InterviewPortalControllerRedesign portal=new WCT_InterviewPortalControllerRedesign();
        actisze=portal.acitemSize;
        upsze=portal.upcomSize;
        allsze=portal.allSize;
        pendsze=portal.pndmSize;
        interviewJuncId = System.currentPagereference().getParameters().get('id');
        candidateId = System.currentPagereference().getParameters().get('cntid');
        Intdue=System.currentPagereference().getParameters().get('interviewdue');
        GrpInt=System.currentPagereference().getParameters().get('View');
        attachmentSection = false;
        removeAttachment = false;
        
        if(interviewJuncId!='' && interviewJuncId !=null){
            interviewJunction=[Select WCT_Interview__c, WCT_Candidate_Tracker__c,WCT_Candidate_Tracker__r.WCT_Contact__c,WCT_Last_Submitted_Date__c,WCT_IEF_Submission_Status__c,
                            WCT_Interview__r.WCT_Interview_Start_Time__c,WCT_Interview__r.WCT_Interview_End_Time__c,(Select Id from Interview_Forms__r) 
                            from WCT_Interview_Junction__c where id=:interviewJuncId ];
                            if(interviewJunction.WCT_IEF_Submission_Status__c=='Draft')
                            {
                            label='Edit IEF';
                            }
                            else
                            {
                             label='Complete IEF';
                            }
                            
            if(interviewJunction.WCT_Interview__r.WCT_Interview_Start_Time__c!=null)IntvDate = interviewJunction.WCT_Interview__r.WCT_Interview_Start_Time__c.format('MM/dd/yyyy');
            if(interviewJunction.WCT_Interview__r.WCT_Interview_Start_Time__c!=null)IntvStrTime = interviewJunction.WCT_Interview__r.WCT_Interview_Start_Time__c.format('hh:mm a');
            if(interviewJunction.WCT_Interview__r.WCT_Interview_End_Time__c!=null)IntvEndTime = interviewJunction.WCT_Interview__r.WCT_Interview_End_Time__c.format('hh:mm a');
            if(interviewJunction.WCT_Last_Submitted_Date__c != null)IntvSubmittedDate  = interviewJunction.WCT_Last_Submitted_Date__c.format('MM/dd/yyyy HH:mm:ss a');
            if(interviewJunction.WCT_Interview__c!=null)IntviewId = interviewJunction.WCT_Interview__c;
            if(interviewJunction.WCT_Candidate_Tracker__c!=null)candidatTrackerId=interviewJunction.WCT_Candidate_Tracker__c;
        }
        //for group interview
        if(GrpInt=='GIView')
        {
        attachmentwp();
        attachmentcsdwp();
        RequistionNCanInfowp();
        getEventfeedbckCandidateAll();
        }
        else
        {
            getEventfeedbckCandidate();
        }

        
    }
    public void setAttachmentSection(){
        if(attachmentSection ){
            attachmentSection = false;
        }else{
            attachmentSection = true;}

    }
    public List<Attachment> getAttachmentsCandidate() {
        list<String> attIds = new list<String>();

        set<Id> attParentId = new set<Id>();
        if(interviewJunction.WCT_Candidate_Tracker__r.WCT_Contact__c!=null)attParentId.add(interviewJunction.WCT_Candidate_Tracker__r.WCT_Contact__c);
        for(WCT_Candidate_Documents__c candidDocId : [SELECT Id FROM WCT_Candidate_Documents__c where WCT_Candidate__c  =: interviewJunction.WCT_Candidate_Tracker__r.WCT_Contact__c and WCT_Visible_to_Interviewer__c=true]){
            attParentId.add(candidDocId.Id);
        }
        if(!attParentId.IsEmpty()){
           return [select Id, ParentId, Parent.Name, Parent.Type, Name, Description, ContentType, BodyLength, CreatedBy.Name, LastModifiedDate from Attachment where parentId in :attParentId];
        }
        else{
          return null;
        }
    } 
    //Get event feedback list 
    public List<Feedback__c> getEventfeedbckCandidate() {
        fdbklist=[select Event__r.name,Event__r.CreatedDate, Feedback_Provider_Name__c,Feedback_Provider_Position__c,Feedback_Provider_Type__c,Rating__c, Comments_Personal_Interests__c, Comments_Industry_Service_Line_Interest__c,Comments_Event_Specific__c, Comments_Misc_Office_Preference_Other__c  from Feedback__c where Contact__r.id=:candidateId limit 500];
        return fdbklist;
    }
    
     public List<Feedback__c> getEventfeedbckCandidateAll() {
        fdbklistall=[select contact__r.name,Event__r.CreatedDate,Event__r.name,Feedback_Provider_Name__c,Feedback_Provider_Position__c,Feedback_Provider_Type__c,Rating__c , Comments_Personal_Interests__c, Comments_Industry_Service_Line_Interest__c,Comments_Event_Specific__c, Comments_Misc_Office_Preference_Other__c from Feedback__c  where Contact__r.id in :cntid order by Contact__r.name asc limit 500];
        return fdbklistall;
    }
    
    public List<Attachment> getAttachmentIEFs()
    {
        set<Id> candidatTrackerSet = new set<Id>();
        for(WCT_Interview_Junction__c IntJunctId : [select id, WCT_Candidate_Tracker__c  from WCT_Interview_Junction__c where WCT_Candidate_Tracker__c = : candidatTrackerId]){
            candidatTrackerSet.add(IntJunctId.Id);
        }
        if(!candidatTrackerSet.IsEmpty()){
            //return [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, Description, BodyLength, CreatedBy.Name, LastModifiedDate from Attachment where parentId =:interviewJuncId];
            return [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, Description, BodyLength, CreatedBy.Name, LastModifiedDate from Attachment where parentId in :candidatTrackerSet];
        }
        else{
            return null;
        }
    
    }
    
    //Added for fetching attachment IEF for candidate
    public List<Attachment> getAttachmentcanIEFs()
    {
        string canid = ApexPages.currentPage().getParameters().get('cid');
        set<Id> candidatTrackerSet = new set<Id>();
        for(WCT_Interview_Junction__c IntJunctId : [select id, WCT_Candidate_Tracker__c  from WCT_Interview_Junction__c where WCT_CandidateID__c = : canid]){
            candidatTrackerSet.add(IntJunctId.Id);
        }
        if(!candidatTrackerSet.IsEmpty()){
            //return [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, Description, BodyLength, CreatedBy.Name, LastModifiedDate from Attachment where parentId =:interviewJuncId];
            return [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, Description, BodyLength, CreatedBy.Name, LastModifiedDate from Attachment where parentId in :candidatTrackerSet];
        }
        else{
            return null;
        }
    
    }
    public Pagereference readFile()
    {
        if(contentFile!=null){
            Attachment att=new Attachment();
            att.ParentId=interviewJuncId ;
            att.Body=contentFile;
            att.Name=nameFile;
            
            WCT_Interview_Junction__c wctjun = new WCT_Interview_Junction__c(Id = interviewJuncId );
            wctjun.WCT_IEF_Submission_Status__c = WCT_UtilConstants.IEF_Form_Submitted;
            wctjun.WCT_IEF_Last_Submitted_Date__c = system.now();
            try{
                insert att;
                update wctjun;
            }
            catch(DMLException ex){
                WCT_ExceptionUtility.logException('InterviewDetailExt-AttachmentUpload,InterJunctionUpdate', null, ex.getMessage() + ' ::: ' + ex.getStackTraceString());
            }  
            contentFile = null;
            att=new Attachment();
            removeAttachment = false;
            setAttachmentSection();
            
            PageReference np = new PageReference('');
            list<WCT_Interview_Junction__c> IntvJuncRec = [select id from WCT_Interview_Junction__c 
                                                    where WCT_Interview__c=:IntviewId
                                                    and WCT_Interviewer__c=:UserInfo.getUserId()];
            if(!IntvJuncRec.IsEmpty()){
                if(IntvJuncRec.size()>1){
                    np = new PageReference('/apex/WCT_InterviewDetailRedesign?View=GIView');           
                }
                else{
                    np = new PageReference('/apex/WCT_InterviewDetailRedesign');
                }
                }
                
                np.getParameters().put('id', interviewJuncId );
                np.getParameters().put('cntid', candidateId );
                np.getParameters().put('interviewdue', Intdue );
                np.setRedirect(true);
                return np;
        }      
        else{return null;}
    }
    public void cancelAttachment(){
        setAttachmentSection();

    }    
    public Id getInvtEventId(){
        try{
            return [select id from Event where whatId = : interviewJunction.WCT_Interview__c limit 1].Id;// it has to return 1 row only based on where clause.
        }
        catch(exception e){
            return null;
        }
    }
    public List<Note> getNotesCandidate() {
        list<String> attIds = new list<String>();

        set<Id> notesParentId = new set<Id>();
        notesParentId.add(interviewJunction.WCT_Candidate_Tracker__r.WCT_Contact__c);
        if(!notesParentId.IsEmpty()){
            return [SELECT Body,CreatedBy.Name,Id,LastModifiedDate,ParentId,Title FROM Note where parentId in :notesParentId];
        }
        else{
            return null;
        }
    } 
    public list<WCT_Interview_Junction__c> getCandidateLst(){
        return [select Id, WCT_Candidate_Tracker__r.WCT_Contact__r.Id, WCT_Candidate_Name__c, WCT_Candidate_Tracker__r.WCT_Contact__c, 
                WCT_Candidate_Tracker__r.WCT_Requisition__c, WCT_Candidate_Tracker__r.WCT_Requisition__r.Name,  
                WCT_IEF_Submission_Status__c,
                WCT_Interview__r.WCT_Clicktools_Form__r.WCT_ClicktoolsID__c, WCT_Interviewer__r.FirstName, WCT_Interviewer__r.LastName,
                WCT_Interviewer_Email__c, WCT_Interviewer_Level__c, WCT_Interview__r.WCT_Clicktools_Form__r.WCT_Record_Name__c 
                from WCT_Interview_Junction__c 
                where WCT_Interview__c =: interviewJunction.WCT_Interview__c
                and WCT_Interviewer__c = :UserInfo.getUserId()];
    }
    public List<Attachment> getAttachmentsCandidateDoc() {
        list<String> attIds = new list<String>();

        set<Id> attParentId = new set<Id>();
                if(candidateId !='' && candidateId != null){     
                attParentId.add(candidateId);
                for(WCT_Candidate_Documents__c candidDocId : [SELECT Id FROM WCT_Candidate_Documents__c where WCT_Candidate__c  =: candidateId and WCT_Visible_to_Interviewer__c=true]){
                    attParentId.add(candidDocId.Id);
                }
                for(WCT_Candidate_Requisition__c canTrackerId : [SELECT Id FROM WCT_Candidate_Requisition__c where WCT_Contact__c  =: candidateId]){
                    attParentId.add(canTrackerId.Id);
                }
        }
        if(!attParentId.IsEmpty()){
            return [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, BodyLength, CreatedBy.Name, LastModifiedDate, Description  from Attachment where parentId in :attParentId];
        }
        else{
                
            return null;
        }
    }
    //Group Interview Candidate documents
   /* public list<wrapcandnamewithattmnts> attachmentdocwp()
    {
        attachmentsdocWrapper = new List<wrapcandnamewithattmnts>();
        List<Attachment> attachments =new List<Attachment>();
        list<String> attIds = new list<String>();
        Map<Id,string> ContactAttParentIdMap = new Map<Id,string>();
        set<Id> candidatTrackerSet = new set<Id>();
        set<Id> attParentId = new set<Id>();
                if(candidateId !='' && candidateId != null){     
                attParentId.add(candidateId);
                for(WCT_Candidate_Documents__c candidDocId : [SELECT Id,WCT_Candidate__r.name FROM WCT_Candidate_Documents__c where WCT_Candidate__c  in :cntid and WCT_Visible_to_Interviewer__c=true]){
                    attParentId.add(candidDocId.Id);
                
                ContactAttParentIdMap.put(candidDocId.Id,candidDocId.WCT_Candidate__r.name);
                }
                for(WCT_Candidate_Requisition__c canTrackerId : [SELECT Id,WCT_Contact__r.name FROM WCT_Candidate_Requisition__c where WCT_Contact__c  =: cntid]){
                    attParentId.add(canTrackerId.Id);
                ContactAttParentIdMap.put(canTrackerId.Id,canTrackerId.WCT_Contact__r.name);
                }
        }
        if(!attParentId.IsEmpty()){
            attachments = [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, BodyLength, CreatedBy.Name, LastModifiedDate, Description  from Attachment where parentId in :attParentId];
        }
       
        system.debug('##########'+attachments+'$$$$$$$$');
        for(Attachment att:attachments){
        attachmentsdocWrapper.add(new wrapcandnamewithattmnts(att,ContactAttParentIdMap.get(att.ParentId)));
        }
        system.debug('##########'+attachmentsWrapper+'$$$$$$$$');
        return attachmentsdocWrapper;
    }*/
    
    //Group Interview IEF attachements
    public list<wrapcandnamewithattmnts> attachmentwp()
    {
        List<Attachment> attachments =new List<Attachment>();
        list<id> candtrackids=new list<id>();
        for(WCT_Interview_Junction__c IntJuncId :[select  id,WCT_RMS_Taleo_ID__c,WCT_Candidate_Tracker__r.WCT_Email__c,WCT_Candidate_Tracker__r.WCT_Requisition_Number__c,WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.name,
                                                  WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.name,WCT_Candidate_Tracker__c,WCT_Job_Type__c, WCT_Interview__r.WCT_FSS__c,
                                                  WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Service_Area__c,WCT_Service_Line__c,WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Region__c,WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Primary_Location__c,
                                                  WCT_Candidate_Tracker__r.WCT_Contact__c,WCT_Candidate_Tracker__r.WCT_Contact__r.name,WCT_Interview_Type__c,WCT_Start_Date_Time__c,WCT_End_Date_Time__c,WCT_IEF_Submission_Status__c from WCT_Interview_Junction__c where WCT_Interview__c = :interviewJunction.WCT_Interview__c])
        {
            boolean isExsiting =false;
            for(WCT_Interview_Junction__c tempExistingIntrJnu :intjunction )
            {
                if(tempExistingIntrJnu.WCT_Candidate_Tracker__c==IntJuncId.WCT_Candidate_Tracker__c)isExsiting=true;
                
            }
            if(!isExsiting)intjunction.add(IntJuncId);
            
            candtrackids.add(IntJuncId.WCT_Candidate_Tracker__c);
        }
        attachmentsWrapper = new List<wrapcandnamewithattmnts>();
        list<String> attIds = new list<String>();
        Map<Id,string> ContactAttParentIdMap = new Map<Id,string>();
        set<Id> candidatTrackerSet = new set<Id>();
        for(WCT_Interview_Junction__c IntJunctId : [select id, WCT_Candidate_Name__c, WCT_Candidate_Tracker__c,WCT_Candidate_Tracker__r.WCT_Contact__c  from WCT_Interview_Junction__c where WCT_Candidate_Tracker__c in :candtrackids]){
            candidatTrackerSet.add(IntJunctId.Id);
            cntid.add(IntJunctId.WCT_Candidate_Tracker__r.WCT_Contact__c);
            ContactAttParentIdMap.put(IntJunctId.Id,IntJunctId.WCT_Candidate_Name__c);
        }
        system.debug('##########'+candidatTrackerSet+'$$$$$$$$'+ContactAttParentIdMap);
        if(!candidatTrackerSet.IsEmpty()){
            //return [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, Description, BodyLength, CreatedBy.Name, LastModifiedDate from Attachment where parentId =:interviewJuncId];
            attachments = [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, Description, BodyLength, CreatedBy.Name, LastModifiedDate from Attachment where parentId in :candidatTrackerSet];
        }
        system.debug('##########'+attachments+'$$$$$$$$');
        for(Attachment att:attachments){
        attachmentsWrapper.add(new wrapcandnamewithattmnts(att,ContactAttParentIdMap.get(att.ParentId)));
        }
        system.debug('##########'+attachmentsWrapper+'$$$$$$$$');
        return attachmentsWrapper;
    }
   //Group Interview Candidate submitted documents 
   public list<wrapcandnamewithattmnts> attachmentcsdwp()
    {
        list<id> candtrackids=new list<id>();
        List<Attachment> attachments =new List<Attachment>();
        attachmentscsdWrapper = new List<wrapcandnamewithattmnts>();
        list<String> attIds = new list<String>();
        Map<Id,string> ContactAttParentIdMap = new Map<Id,string>();
        set<Id> candidatTrackerSet = new set<Id>();
        set<Id> attParentId = new set<Id>();
        for(WCT_Interview_Junction__c inj:intjunction)
        {
        attParentId.add(inj.WCT_Candidate_Tracker__r.WCT_Contact__c);
        ContactAttParentIdMap.put(inj.WCT_Candidate_Tracker__r.WCT_Contact__c,inj.WCT_Candidate_Tracker__r.WCT_Contact__r.name);
        }
        for(WCT_Candidate_Documents__c candidDocId : [SELECT Id,WCT_Candidate__r.name FROM WCT_Candidate_Documents__c where WCT_Candidate__c  in: cntid and WCT_Visible_to_Interviewer__c=true]){
            attParentId.add(candidDocId.Id);
            ContactAttParentIdMap.put(candidDocId.Id,candidDocId.WCT_Candidate__r.name);
        }
        system.debug('##########'+attParentId+'$$$$$$$$'+ContactAttParentIdMap+'%%%%%%'+cntid);
        if(!attParentId.IsEmpty()){
           attachments= [select Id, ParentId, Parent.Name, Parent.Type, Name, Description, ContentType, BodyLength, CreatedBy.Name, LastModifiedDate from Attachment where parentId in :attParentId];
        }
         system.debug('##########'+attachments+'$$$$$$$$');
        for(Attachment att:attachments){
        attachmentscsdWrapper.add(new wrapcandnamewithattmnts(att,ContactAttParentIdMap.get(att.ParentId)));
        }
        system.debug('##########'+attachmentsWrapper+'$$$$$$$$');
        return attachmentscsdWrapper;
    }
    //Group Interview Candidate and requisition Information
    public list<wrapcandnamewithReq> RequistionNcanInfowp()
    {
        list<id> candtrackids=new list<id>();
        List<Attachment> attachments =new List<Attachment>();
        RequistionNcanInf = new List<wrapcandnamewithReq>();

        for(WCT_Interview_Junction__c req:intjunction){
        RequistionNcanInf.add(new wrapcandnamewithReq(req,req.WCT_Candidate_Tracker__r.WCT_Contact__r.name));
        }
        return RequistionNcanInf;
    }
    
    //wrapper class from group interview attachments
    public class wrapcandnamewithattmnts{
        public string candt{get;set;}
        public Attachment AttachmentIEF{get;set;}
        wrapcandnamewithattmnts(Attachment retAtt,string cand)
        {
           candt=cand;
           AttachmentIEF=retAtt;     
        }
        
    }
    //wrapper class from group interview candidate and requisition Information
     public class wrapcandnamewithReq{
        public string candtnm{get;set;}
        public string IntvDateGrp {get;set;}
        public string IntvStrTimeGrp {get;set;}
        public string IntvEndTimeGrp {get;set;}
        public WCT_Interview_Junction__c Requ{get;set;}
        wrapcandnamewithReq(WCT_Interview_Junction__c ret,string cand)
        {
           candtnm=cand;
           Requ=ret;
           if(ret.WCT_Start_Date_Time__c!=null)IntvDateGrp = ret.WCT_Start_Date_Time__c.format('MM/dd/yyyy');
           if(ret.WCT_Start_Date_Time__c!=null)IntvStrTimeGrp = ret.WCT_Start_Date_Time__c.format('hh:mm a'); 
           if(ret.WCT_End_Date_Time__c!=null)IntvEndTimeGrp = ret.WCT_End_Date_Time__c.format('hh:mm a'); 
        }
        
    }

}