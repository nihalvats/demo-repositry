global class WCT_CustomIterable implements Iterator<KnowledgeArticleVersion>{ 

   List<KnowledgeArticleVersion> KAV {get; set;} 
   Integer i {get; set;} 

   public WCT_CustomIterable(){ 
       KAV = [SELECT ArticleNumber,ArticleType,CreatedBy.email,KnowledgeArticleId,Title,UrlName FROM KnowledgeArticleVersion where PublishStatus = 'Online' and Language = 'en_US' limit 1];
       i = 0; 
   }   

   global boolean hasNext(){ 
       if(i >= KAV.size()) {
           return false; 
       } else {
           return true; 
       }
   }    

   global KnowledgeArticleVersion next(){ 
       // 8 is an arbitrary 
       // constant in this example
       // that represents the 
       // maximum size of the list.
       if(i == 8){return null;} 
       i++; 
       return KAV[i-1]; 
   } 
}