/**
    * Class Name  : WCT_UserHelper_Test
    * Description : This apex test class will use to test the WCT_UserHelper
*/
@isTest
private class WCT_UserHelper_Test {

    public static Contact contactRec;
    public static string nonInterviewUserEmail='interviewUser'+System.now().format('ddMMYYYY')+'@deloitte.com';
    public static string interviewUserEmail='nonInterviewUser'+System.now().format('ddMMYYYY')+'@deloitte.com';
    public static string nonUserEmail='nonUserContact'+System.now().format('ddMMYYYY')+'@deloitte.com';
    /** 
        Method Name  : createContact
        Return Type  : Contact
        Type         : private
        Description  : Create temp record for data mapping         
    */
    
    
    private static List<Contact> createContact()
    {
        List<Contact> contacts = new List<Contact>();
        
        contactRec=WCT_UtilTestDataCreation.createContact();
        contactRec.email=interviewUserEmail;
        
        contacts.add(contactRec);
        Contact nonInterview=WCT_UtilTestDataCreation.createContact();
        nonInterview.email=nonInterviewUserEmail;
        contacts.add(nonInterview);
        Contact noUserContact=WCT_UtilTestDataCreation.createContact();
        noUserContact.email=nonUserEmail;
        
        
        
        contacts.add(noUserContact);
        
        insert contacts;
        return contacts;
    }
    
    
    private static List<User> createUsers()
    {
        List<User> users= new List<User>();
        
        Id interViewerProfileId=[Select id from profile where name =:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
        Id integrationProfileId=[Select id from profile where name =:WCT_UtilConstants.INTEGRATION_PROFILE_NAME].Id;
        User interviewUser =WCT_UtilTestDataCreation.createUser('test1',interViewerProfileId, interviewUserEmail,interviewUserEmail);
        interviewUser.isActive=false;
        interviewUser.WCT_Riva_Flag__c=false;
        users.add(interviewUser);
        
        
        User nonInterviewUser =WCT_UtilTestDataCreation.createUser('test2',integrationProfileId, nonInterviewUserEmail,nonInterviewUserEmail);
        nonInterviewUser.isActive=false;
        nonInterviewUser.WCT_Riva_Flag__c=false;
        users.add(nonInterviewUser);
        
       Database.insert(users);
       return users;
        
    }
    /** 
        Method Name  : createUser
        Return Type  : TestMethod
        Type         : private
        Description  : Create temp records for data mapping         
    */
    static testMethod void createUser() {
        Test.startTest();
        
        createUsers();
        List<Id> contactIds=new List<Id>();
        List<Contact> contacts= createContact();
        for (Contact contact :  contacts)
        {
            contactIds.add(contact.id);
        }
        
        System.runAs ( new User(Id = UserInfo.getUserId()) )
        {
            String returnStatement=WCT_UserHelper.createUsers(contactIds);
            String deactivateStatement=WCT_UserHelper.deactivateUsers(contactIds);
        }
        Test.stopTest();
        
    }
    
    /** 
        Method Name  : createDupUser
        Return Type  : TestMethod
        Type         : private
        Description  : Test duplicate records.          
    */
    static testMethod void createDupUser() {
        Test.startTest();
        List<Id> contactIds=new List<Id>();
        List<Contact> contacts= createContact();
        for (Contact contact :  contacts)
        {
            contactIds.add(contact.id);
        }
        System.runAs ( new User(Id = UserInfo.getUserId()) ){
            String returnStatement=WCT_UserHelper.createUsers(contactIds);
            String returnStatementDup=WCT_UserHelper.createUsers(contactIds);
            System.assertEquals(returnStatementDup, Label.WCT_User_Present);
            String deactivateStatement=WCT_UserHelper.deactivateUsers(contactIds);
            String deactivateStatementDup=WCT_UserHelper.deactivateUsers(contactIds);
        }
        Test.stopTest();
        
          
    }
}