/*=========================================Test Class=================================================

***************************************************************************************************************************************** 
 * Class Name   : Test_ProjectsByTalentChannelController
 * Description  : test Class for ProjectsByTalentChannelController
 * Created By   : Deloitte India.
 *
 *****************************************************************************************************************************************/
 
 @
istest(SeeAllData = false)
public class Test_ProjectsByPersonController{


        static testmethod void ProjectsByPersonControllernew() {
        
        Test.startTest();
        Contact con = new Contact();
        con.lastname = 'test name';
        insert con;
        
        Project__c prj = new Project__c();
        prj.Status__c = 'In Progress';
        prj.Talent_Channel__c = 'test';
        prj.Project_Lead_US__c= con.id;
        prj.Priority__c = 'high';
        prj.Project_Category__c ='test';
        prj.Project_Lead_USI__c= con.id;
        insert prj;
        
      
       ProjectsByPersonController testcontroller = new ProjectsByPersonController();
        
        testcontroller.SelectedPS ='In Progress';
        testcontroller.SelectedPCat= 'test';
        testcontroller.SearchLead = 'test name';
        testcontroller.ProcessData();
        apexpages.currentpage().getparameters().put('SearchLead','test name');   
        apexpages.currentpage().getparameters().put('SelectedPCat','-None-');
        apexpages.currentpage().getparameters().put('SelectedPS','-None-');

        testcontroller.FilterResult();
      
      
         Test.stopTest();
      
        
        
        }
        
        
        
         



}