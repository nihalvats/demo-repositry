public class H1B_Client_Location_Trigger_HDLR {

  
    
    public static void update_ClientLoc_To_Project(List<Client_Location__c> clientlocations)
    {
        /*
        * Step 1: Loop through all the Locations to find the distinct Client Project the Client location are associated with
        * Step 2: Query all the client Locations assciated to identified distinct client Projects with order by Created Date. 
        * Step 3: 
        */
        
        System.debug('# clientlocations : '+clientlocations);
        Set<Id> projectIds= new Set<Id>();
        for(Client_Location__c loc : clientlocations)
        {
            projectIds.add(loc.Project_Name__c);
        }
        
        /*STEP 2: Project_H1B__c */
        if(projectIds.size()>0)
        {
            List<Client_Location__c> allLocations=[Select id, H1B_City__c, H1B_State__c, H1B_Street__c, Zip_Code__c,Project_Name__c  From Client_Location__c where Project_Name__c in :projectIds order by CreatedDate asc];
            System.debug('# locations : '+allLocations);
            
            Map<Id, Project_H1B__c> projectMap=new Map<Id,Project_H1B__c>();
            for(Client_Location__c loc : allLocations)
            {
                if(loc.Project_Name__c==null)
                {
                    /*
                     * Return if no project is assigned to the client Location.
                    */
                    break;
                }
                System.debug('# ProjectMap  : '+projectMap);
                Project_H1B__c tempProject;
                if(projectMap.get(loc.Project_Name__c)==null)
                {
                    System.debug('### new project to be added to project map'+loc.Project_Name__c);
                    tempProject= new Project_H1B__c();
                    tempProject.id=loc.Project_Name__c;
                    for(integer i=1; i<=10;i++)
                    {
                        tempProject.put('H1B_Client_Location_'+i+'_lk__c',null);
                    }
                }
                else
                {
                    tempProject=projectMap.get(loc.Project_Name__c);
                }
                
                
                //string temp=loc.H1B_Street__c+'\n'+loc.H1B_City__c+'\n'+loc.H1B_State__c+'\n'+loc.Zip_Code__c;
                for(integer i=1; i<=10;i++)
                {
                    if(tempProject.get('H1B_Client_Location_'+i+'_lk__c')==null)
                    {
                        tempProject.put('H1B_Client_Location_'+i+'_lk__c',loc.id);
                        break;
                    }
                    else
                    {
                        System.debug(i+'th value'+tempProject.get('H1B_Client_Location_'+i+'_lk__c'));
                    }
                }
                projectMap.put(loc.Project_Name__c,tempProject);
            }
            
            List<Project_H1B__c> listtoUpdate= new List<Project_H1B__c>();
            for(Id tempId: projectMap.keySet())
            {
                listtoUpdate.add(projectMap.get(tempId));
            }
            
            System.debug(''+listtoUpdate);
            
            update listtoUpdate;
        }
        
    }
    
}