@isTest(seealldata = true)
private class WCT_FindWageNoticesReadyToSend_test {

        
        static testMethod void UnitTest(){
      
        
   Recordtype r = [select id,Name from Recordtype where DeveloperName  = 'WCT_Employee'];     
        Contact con=WCT_UtilTestDataCreation.createContact();
        con.email='test@deloitte.com';
        con.Recordtypeid = r.id ;         
        insert con;
       
       list<WTPAA_Wage_Notice__c> lst = new list<WTPAA_Wage_Notice__c>();
       
        WTPAA_Wage_Notice__c wg = new WTPAA_Wage_Notice__c ();
        wg.WTPAA_Status__c = 'New';
        wg.WTPAA_Is_Notice_Sent__c = false;
     //   wg.WTPAA_Controlling_Field_HeadupMsg__c  = true;
        wg.WTPAA_Related_To__c = con.id;
        wg.WTPAA_Notice_Type__c = 'At Hire';
        lst.add(wg);
        insert lst;
     lst[0].recalculateFormulas();  
        list<id> lstids = new list<id>();
        lstids.add(lst[0].id);
       
     system.debug('ID************'+lst[0].id);
     system.debug('List of IDs****'+lstids);
     
   
   
    Test.starttest(); 
   String returnStatement='test';   
     
    WCT_FindWageNoticesReadyToSend  fnrs = new WCT_FindWageNoticesReadyToSend ();
    WCT_FindWageNoticesReadyToSend.findWageNoticesRecords('New',lstids);
   
  //  return null;
  
    Test.stoptest();
   
    
        }
        
       static testMethod void UnitTest1(){
      
        
   Recordtype r = [select id,Name from Recordtype where DeveloperName  = 'WCT_Employee'];     
        Contact con=WCT_UtilTestDataCreation.createContact();
        con.email='test@deloitte.com';
        con.Recordtypeid = r.id ;         
        insert con;
   
   list<id> lstids = new list<id>();
   list<WTPAA_Wage_Notice__c> lst1 = new list<WTPAA_Wage_Notice__c>();
       
        WTPAA_Wage_Notice__c wg1 = new WTPAA_Wage_Notice__c ();
       wg1.WTPAA_Status__c = 'sent';
        wg1.WTPAA_Is_Notice_Sent__c = true;
        wg1.WTPAA_Controlling_Field_HeadupMsg__c  = true;
        wg1.WTPAA_Related_To__c = con.id;
        wg1.WTPAA_Notice_Type__c = 'At Hire';
        wg1.WTPAA_Batch_controlled__c = false;
        lst1.add(wg1);
        insert lst1;
   
        lstids.add(lst1[0].id);
     
     system.assertequals(true,wg1.WTPAA_Controlling_Field_HeadupMsg__c) ;   
      Test.starttest(); 
   String returnStatement='test';   
     
    WCT_FindWageNoticesReadyToSend  fnrs = new WCT_FindWageNoticesReadyToSend ();
    WCT_FindWageNoticesReadyToSend.findWageNoticesRecords('New',lstids);
   
  //  return null;
  
    Test.stoptest();  
        }
      }