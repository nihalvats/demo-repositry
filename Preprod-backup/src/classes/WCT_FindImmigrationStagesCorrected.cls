/**
    * Class Name  : WCT_FindImmigrationStagesCorrected 
    * Description : This apex class will use to create immigrations. 
*/

global class WCT_FindImmigrationStagesCorrected {
  
   /** 
        Method Name  : findImmigrationStagingRecords
        Return Type  : Void
        Description  : Find all records which needs to treated as immigrations.         
    */
  
    WebService static String findImmigrationStagingRecords(String stagingStatus)
    {
      Set<Id> stagingIds = new Set<id>();
      String returnStatement=Label.WCT_Message_Processing_Starting;
    for(WCT_Immigration_Stagging_Table__c immigrationStageRecord: [Select id from WCT_Immigration_Stagging_Table__c where (Status__c = : stagingStatus or status__c='Not Started')
                            limit:Limits.getLimitQueryRows()])
    {
      stagingIds.add(immigrationStageRecord.Id);
    
    }
    
    if(!stagingIds.IsEmpty())
      {
        
        try{
            WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, stagingStatus);
          }Catch(Exception e)
          {
            WCT_ExceptionUtility.logException('WCT_FindImmigrationStagesCorrected','Create Immigrations',e.getMessage());
            throw e;
          }
      }
      else
      {
        returnStatement=WCT_UtilConstants.PROCESS_NONE;
      
      }
      return returnStatement;
    
    }

}