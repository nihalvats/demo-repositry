/**
    * Class Name  : WCT_CreateCasesHelper_Test
    * Description : This apex test class will use to test the WCT_CreateCasesHelper
*/
@isTest
private class WCT_CreateCasesHelper_Test {
    
    public static String caseRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get(WCT_UtilConstants.CASE_RECORDTYPE_PRE_BI).getRecordTypeId();
    public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
   // public static String Case1RecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Case').getRecordTypeId();
    public static List<WCT_PreBIStageTable__c> preBiStageList=new List<WCT_PreBIStageTable__c>();
    public static List<Contact> contactList=new List<Contact>();
    public static List<WCT_PreBIQAReferenceTable__c> refList=new List<WCT_PreBIQAReferenceTable__c>();
    public static User userRec=new User();
    public static Id profileId=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    public static Id roleId=[SELECT Id FROM UserRole where developerName ='CTS_Recruiter'].Id;
    public static WCT_Requisition__c req;
    public static List<Case> CaseList=new List<Case>();
   
    public static WCT_Candidate_Requisition__c ct;
    
    /** 
        Method Name  : createContacts
        Return Type  : List<Contact>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static List<Contact> createContacts()
    {
        contactList=WCT_UtilTestDataCreation.createContactWithCandidate(candidateRecordTypeId);
        insert contactList;
        return contactList;
    }
    /** 
        Method Name  : createRequistion
        Return Type  : List<WCT_Requisition__c>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static WCT_Requisition__c createRequistion()
    {
        req=WCT_UtilTestDataCreation.createRequistion();
        insert req;
        return req;
    }
    /** 
        Method Name  : createCandidateRequisition
        Return Type  : List<WCT_Candidate_Requisition__c>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static WCT_Candidate_Requisition__c createCandidateRequisition(Id CandidateId,Id RequisitionId)
    {
        ct=WCT_UtilTestDataCreation.createCandidateRequisition(CandidateId,RequisitionId);
        insert ct;
        return ct;
    }    
    /** 
        Method Name  : createuser
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUser()
    {
        userRec=WCT_UtilTestDataCreation.createUser('CaseHelp', profileId, 'vinBhatiaPreBICaseHelp@deloitte.com', 'Recruiter1@deloitte.com');
        return  userRec;
    }
    /** 
        Method Name  : createPreBIStageTable
        Return Type  : List<WCT_PreBIStageTable__c>
        Type         : private
        Description  : Create Pre-Bi Stage Table Records.         
    */
    private Static List<WCT_PreBIStageTable__c> createPreBIStageTable()
    {
        preBiStageList=WCT_UtilTestDataCreation.createPreBIStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        insert preBiStageList;
        return  preBiStageList;
    }
    
    /** 
        Method Name  : createPreBiReferenceRecords
        Return Type  : List<WCT_PreBIQAReferenceTable__c>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static List<WCT_PreBIQAReferenceTable__c> createPreBiReferenceRecords()
    {
        refList=WCT_UtilTestDataCreation.createPreBiReferenceRecords();
        
         
        
        
        refList[0].WCT_Parent__c= refList[0].id;
 
       // update refList[0];
        
         refList[1].WCT_Parent__c= refList[1].id;
         
        // update refList[1];
         
          refList[2].WCT_Parent__c= refList[2].id;
          
        //  update refList[2];
          
          
         insert refList; 
        return refList;
    }
    private static List<Case> createcase(String caseId)
    { 
        //CAsList=WCT_UtilTestDataCreation.createcaseRecord();
         caseList=new List<Case>();
   
    
        Case CaseRec = new Case();
        CaseRec.OwnerId = caseId;
        CaseRec.Status=WCT_UtilConstants.CASE_STATUS_NEW;
        CaseRec.Priority=WCT_UtilConstants.CASE_PRIORITY_MEDIUM;
        //CaseRec.Subject=WCT_UtilConstants.CASE_SUBJECT_PREBIGPA;
        CaseRec.WCT_Category__c=WCT_UtilConstants.CASE_CATEFORY;
        CaseRec.WCT_SubCategory1__c=WCT_UtilConstants.CASE_SUBCATEGORY1_CAMPUS;
        CaseRec.WCT_SubCategory2__c=WCT_UtilConstants.CASE_SUBCATEGORY2;
        CaseRec.Origin=WCT_UtilConstants.CASE_ORIGIN;
       
        caseList.add(CaseRec);
        insert caseList;
        return caseList;
       
         
        
    }
   
    /** 
        Method Name  : createCasesTestMethod
        Return Type  : void
        Description  : Create cases with all staging Table.         
    */
    static testMethod void createCasesTestMethod() {
        
        userRec=createUser();
        insert userRec;
        refList=createPreBiReferenceRecords();
        contactList=createContacts();
        req=createRequistion();
        ct=createCandidateRequisition(contactList[0].id,req.id);
        preBiStageList=createPreBIStageTable();
        Set<Id> stagingIds=new Set<id>();
        for(WCT_PreBIStageTable__c rec:preBiStageList)
        {
                stagingIds.add(rec.Id);
        }
        Test.startTest();
        WCT_CreateCasesHelper.createCases(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_PreBIStageTable__c> preList=[select Id from WCT_PreBIStageTable__c where Id IN:stagingIds and WCT_Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
        //System.assertEquals(preList.Size(), preBiStageList.Size());
        System.assertEquals(10, preBiStageList.Size());
    }
    /** 
        Method Name  : recuriterNotFound
        Return Type  : void
        Description  : recuriterNotFound Error check while inserting Cases.         
    */
    static testMethod void recuriterNotFound() {
        
        refList=createPreBiReferenceRecords();
        contactList=createContacts();
        req=createRequistion();
        ct=createCandidateRequisition(contactList[0].id,req.id);
        preBiStageList=createPreBIStageTable();
        Set<Id> stagingIds=new Set<id>();
        for(WCT_PreBIStageTable__c rec:preBiStageList)
        {
                stagingIds.add(rec.Id);
        }
        Test.startTest();
        WCT_CreateCasesHelper.createCases(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_PreBIStageTable__c> preList=[select Id,WCT_Status__c,WCT_Error_message__c from WCT_PreBIStageTable__c where Id IN:stagingIds ];
        for(WCT_PreBIStageTable__c rec: preList)
        {
            //System.assertEquals(rec.WCT_Status__c, WCT_UtilConstants.STAGING_STATUS_ERROR);
            //System.assertEquals(rec.WCT_Error_message__c, Label.Recruiter_Email_Error);
        }
    }
    /** 
        Method Name  : contactNotFound
        Return Type  : void
        Description  : Not valid contacts check         
    */
    static testMethod void contactNotFound() {
        
        userRec=createUser();
        insert userRec;
        refList=createPreBiReferenceRecords();
        req=createRequistion();
        preBiStageList=createPreBIStageTable();
        Set<Id> stagingIds=new Set<id>();
        for(WCT_PreBIStageTable__c rec:preBiStageList)
        {
                stagingIds.add(rec.Id);
        }
        Test.startTest();
        WCT_CreateCasesHelper.createCases(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_PreBIStageTable__c> preList=[select Id,WCT_Status__c,WCT_Error_message__c from WCT_PreBIStageTable__c where Id IN:stagingIds ];
        for(WCT_PreBIStageTable__c rec: preList)
        {
            //System.assertEquals(rec.WCT_Status__c, WCT_UtilConstants.STAGING_STATUS_ERROR);
            //System.assertEquals(rec.WCT_Error_message__c, Label.Candidate_Email_Error);
        }
        
    }
    /** 
        Method Name  : duplicateRowsError
        Return Type  : void
        Description  : Create cases with all staging Table.         
    */
    static testMethod void duplicateRowsError() {
        
        userRec=createUser();
        insert userRec;
        refList=createPreBiReferenceRecords();
        contactList=createContacts();
        req=createRequistion();
        ct=createCandidateRequisition(contactList[0].id,req.id);
        preBiStageList=createPreBIStageTable();
     
        List<WCT_PreBIStageTable__c> dupPreBIList=WCT_UtilTestDataCreation.createPreBIStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        insert dupPreBIList;
        System.assertEquals(dupPreBIList.Size(),10);
        Set<Id> stagingIds=new Set<id>();
        for(WCT_PreBIStageTable__c rec:preBiStageList)
        {
                stagingIds.add(rec.Id);
        }
        for(WCT_PreBIStageTable__c rec:dupPreBIList)
        {
                stagingIds.add(rec.Id);
        }
        Test.startTest(); 
        System.assertEquals(stagingIds.Size(),20);
        WCT_CreateCasesHelper.createCases(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_PreBIStageTable__c> preList=[select Id from WCT_PreBIStageTable__c where Id IN:stagingIds and WCT_Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
        System.assertEquals(preList.Size(), preBiStageList.Size());
        System.assertEquals(10, preBiStageList.Size());
        List<WCT_PreBIStageTable__c> dupPreList=[select Id from WCT_PreBIStageTable__c where Id IN:stagingIds and WCT_Status__c=:WCT_UtilConstants.STAGING_STATUS_ERROR];
        System.assertEquals(dupPreList.Size(), dupPreBIList.Size());
        //System.assertEquals(dupPreList.Size(), 20);
    }
    
    
 @isTest
 public static void m3() {
 
         Test.startTest(); 
         
         
         List<WCT_PreBIStageTable__c> preBIList=new List<WCT_PreBIStageTable__c>();
          
       WCT_PreBIStageTable__c stagingRecord = new WCT_PreBIStageTable__c();
       
        stagingRecord.WCT_RMS_ID__c='1234553';
        stagingRecord.WCT_Candidate_Last_Name__c='Sample';
        stagingRecord.WCT_Candidate_First_Name__c='FirstName';
        stagingRecord.WCT_Candidate_Email__c='Email3@deloitte.com';
        stagingRecord.WCT_Recruiter_Name__c='Recruiter';
        stagingRecord.WCT_Recruiter_Email__c='Recruiter1@deloitte.com';
        stagingRecord.WCT_Requisition_ID__c='12345';
        stagingRecord.WCT_status__c=WCT_UtilConstants.STAGING_STATUS_NOT_STARTED;
        stagingRecord.WCT_A1__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A2__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A3__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A4__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A5__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A6__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A7__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A8__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A9__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A10__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A11__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A12__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A13__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A14__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A15__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A16__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A17__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A18__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord.WCT_A19__c=WCT_UtilConstants.ANSWER_YES;
        
        stagingRecord.WCT_Status__c=WCT_UtilConstants.STAGING_STATUS_NOT_STARTED;
        stagingRecord.WCT_Case_Creation__c=true;
        stagingRecord.WCT_Version_Number__c='1';
        stagingRecord.WCT_Criminal_History__c = true;
        
         preBIList.add(stagingRecord);
         
        WCT_PreBIStageTable__c stagingRecord1 = new WCT_PreBIStageTable__c();
       
        stagingRecord1.WCT_RMS_ID__c='1234553';
        stagingRecord1.WCT_Candidate_Last_Name__c='Sample';
        stagingRecord1.WCT_Candidate_First_Name__c='FirstName';
        stagingRecord1.WCT_Candidate_Email__c='sample@deloitte.com';
        stagingRecord1.WCT_Recruiter_Name__c='Recruiter';
        stagingRecord1.WCT_Recruiter_Email__c='Recruiter3@deloitte.com';
        stagingRecord1.WCT_Requisition_ID__c='12345';
        stagingRecord1.WCT_status__c=WCT_UtilConstants.STAGING_STATUS_NOT_STARTED;
        stagingRecord1.WCT_A1__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A2__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A3__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A4__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A5__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A6__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A7__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A8__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A9__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A10__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A11__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A12__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A13__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A14__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A15__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A16__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A17__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A18__c=WCT_UtilConstants.ANSWER_YES;
        stagingRecord1.WCT_A19__c=WCT_UtilConstants.ANSWER_YES;
        
        stagingRecord1.WCT_Status__c=WCT_UtilConstants.STAGING_STATUS_NOT_STARTED;
        stagingRecord1.WCT_Case_Creation__c=true;
        stagingRecord1.WCT_Version_Number__c='1';
        
        
        
        preBIList.add(stagingRecord1); 
        
        insert preBIList;
         
         
         
        
      userRec=createUser();
        insert userRec;
        refList=createPreBiReferenceRecords();
        
         // contactList=createContacts();
       
            Contact c=new Contact();
            c.lastName='Sample';
            c.Email= 'Email3@deloitte.com';  
            c.RecordTypeId=candidateRecordTypeId;
            c.WCT_Taleo_Id__c= '1234553';
            insert c;
            
          Contact c1=new Contact();
            c1.lastName='Sample1';
            c1.Email= 'sample@deloitte.com';  
            c1.RecordTypeId=candidateRecordTypeId;
            c1.WCT_Taleo_Id__c= '111';
            insert c1;  
        
        req=createRequistion();
        
        ct=createCandidateRequisition(c.id,req.id);
        
        
        preBiStageList = createPreBIStageTable();
       
             //  preBiStageList = preBIList;
          
          List<Case> lcasvar = new List<case>();
          
          
        
         //Case casvar =  WCT_UtilTestDataCreation.createCase(c.id);
         Case casvar = new Case();
         casvar.Priority = '3 - Medium';
         casvar.Origin = 'Email';
         casvar.WCT_Category__c = 'Compliance';
         casvar.Subject ='Sample';
         casvar.Description = 'Sample';
         casvar.WCT_Criminal_History_Case__c = false;
         casvar.RecordTypeID = caseRecordTypeId;
         casvar.Status = 'New';
         casvar.WCT_Criminal_History_Case__c = false;
         casvar.Contactid = c.id;
         casvar.ToD_Case_Category__c = 'Other - US';
         //casvar.Contact.Email = c.Email;
         lcasvar.add(casvar);
         
         Case casvar1 = new Case();
         casvar1.Priority = '3 - Medium';
         casvar1.Origin = 'Email';
         casvar1.WCT_Category__c = 'Compliance';
         casvar1.Subject ='Sample';
         casvar1.Description = 'Sample';
         casvar1.WCT_Criminal_History_Case__c = false;
         casvar1.RecordTypeID = caseRecordTypeId;
         casvar1.Status = 'New';
         casvar1.WCT_Criminal_History_Case__c = true;
         casvar1.Contactid = c.id;
         casvar1.ToD_Case_Category__c = 'Other - US';
         //casvar1.Contact.Email = c.Email;
         
         lcasvar.add(casvar1);
         
         Case casvar3 = new Case();
         casvar3.Priority = '3 - Medium';
         casvar3.Origin = 'Email';
         casvar3.WCT_Category__c = 'Compliance';
         casvar3.Subject ='Sample';
         casvar3.Description = 'Sample';
         casvar3.WCT_Criminal_History_Case__c = false;
         casvar3.RecordTypeID = caseRecordTypeId;
         casvar3.Status = 'New';
         casvar3.WCT_Criminal_History_Case__c = true;
         casvar3.Contactid = c1.id;
         casvar3.ToD_Case_Category__c = 'Other - US';
         //casvar3.Contact.Email = c.Email;
         
         lcasvar.add(casvar3);
         

         insert lcasvar;
         
        //   insert casvar;
           
        List<WCT_PreBIStageTable__c> dupPreBIList=WCT_UtilTestDataCreation.createPreBIStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        insert dupPreBIList;
        System.assertEquals(dupPreBIList.Size(),10);
        Set<Id> stagingIds=new Set<id>();
        
        Set<Id> casset = new Set<Id>();
        
         for(Case casset1: lcasvar)
        {
                casset.add(casset1.Id);
        }
        
        
        
        for(WCT_PreBIStageTable__c rec:preBiStageList)
        {
                stagingIds.add(rec.Id);
        }
        for(WCT_PreBIStageTable__c rec:dupPreBIList)
        {
                stagingIds.add(rec.Id);
        }
        
        for(WCT_PreBIStageTable__c rec : preBIList)
        
        {
        
        stagingIds.add(rec.Id);
        
        }
      
   
        WCT_CreateCasesHelper.createCases(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
       // WCT_CreateCasesHelper.createQAReferences(casset,'1');
        
        Test.stopTest();
      
    
    }   
    
    
}