global class  WCT_H1bcap_SLL_No_Resp Implements Database.Batchable <sObject> {
    global Database.queryLocator start(Database.BatchableContext bc) {
        
    Date d= system.today();
            
        String SOQL = 'SELECT Id,WCT_H1BCAP_Status__c,WCT_H1BCAP_Resource_Manager_Email_ID__c,WC_H1BCAP_USI_SLL_email_id__c,WCT_H1BCAP_USI_SM_Name_GDM__c FROM WCT_H1BCAP__c WHERE WCT_H1BCAP_Status__c =  \'Ready for SLL approval\' AND (WCT_H1BCAP_After7thBusinessday_SLL__c =:d)' ;
    
        return Database.getQueryLocator(SOQL);
       
    }

    global void execute(Database.BatchableContext bc, List<WCT_H1BCAP__c> H1bcap) {
    
         for(WCT_H1BCAP__c w: H1bcap) 
  {
       System.debug('Entering ind loop*******');   
        
          w.WCT_H1BCAP_Status__c  = 'USI SLL No Response';
    
    }
    update H1bcap;
  }  
   global void finish(Database.BatchableContext bc) {
    }
  }