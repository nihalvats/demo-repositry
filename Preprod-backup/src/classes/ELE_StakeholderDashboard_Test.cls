@isTest
private class ELE_StakeholderDashboard_Test {
	public static Account accountObj ;
	public static Contact contactObj; 
	/** 
	        Method Name  : createAccount
	        Return Type  : Account
	        Type         : private
	        Description  : Create temp Account record for data mapping         
	    */
		private static Account createAccount()
		{
			Account accObj = new Account(name = 'Test Account');
			insert accObj;
			return accObj;
		}

		/** 

	        Method Name  : createContact
	        Return Type  : Contact
	        Type         : private
	        Description  : Create temp Contact record for data mapping         
	    */
	    private static Contact createContact(Account acc,string email)
		{
			Contact conObj = new Contact(firstname = 'Test',lastname = 'Contact',Accountid =acc.id,email=email,RecordTypeId = System.Label.Employee_Record_Type_ID,ELE_Access_Level__c = 'ITS',ELE_Contact__c=true);
			insert conObj;
			return conObj;
		}

	@isTest static void test_method_one() {
		//accountObj = ELE_StakeholderDashboard_Test.createAccount();
		//contactObj = ELE_StakeholderDashboard_Test.createContact(accountObj);
		
        Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');             
            User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
            insert u;
            System.runAs(u)
            {
        
		Test.startTest();
        String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        Test.setCurrentPageReference(new PageReference('Page.ELE_stakeholderDashboard')); 
		System.currentPageReference().getParameters().put('em', strEncryptEmail);
		System.currentPageReference().getParameters().put('Param1','p');
		ELE_StakeholderDashboard_crt ele = new ELE_StakeholderDashboard_crt();

		//ele.RedirectToEmp();
        ele.CloseSrh='0';
        ele.searchcls();
        ele.CloseSrh='';
        ele.searchcls();
        ele.backbtn();
		Test.stopTest();
            }
	}
	
	@isTest static void test_method_two() {
        
        Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');             
            User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
            insert u;
            System.runAs(u)
            {
        
		//accountObj = ELE_StakeholderDashboard_Test.createAccount();
		//contactObj = ELE_StakeholderDashboard_Test.createContact(accountObj);
		Test.startTest();
        String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        Test.setCurrentPageReference(new PageReference('Page.ELE_stakeholderDashboard')); 
		System.currentPageReference().getParameters().put('em', strEncryptEmail);
		System.currentPageReference().getParameters().put('Param1','s');
		ELE_StakeholderDashboard_crt ele = new ELE_StakeholderDashboard_crt();
		ele.sStakeholderDesignation = 'Finance';
        ele.CloseSrh='0';
        ele.searchcls();
        ele.backbtn();
		Test.stopTest();
            }
	}
	@isTest static void test_method_3() {
        Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');             
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;
        System.runAs(u)
        {
        
		Test.startTest();
        //String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt('test@test.com'), 'UTF-8');
        Test.setCurrentPageReference(new PageReference('Page.ELE_stakeholderDashboard')); 
		//System.currentPageReference().getParameters().put('em', strEncryptEmail);
		System.currentPageReference().getParameters().put('Param1','s');
		ELE_StakeholderDashboard_crt ele = new ELE_StakeholderDashboard_crt();
		Test.stopTest();
        }		
	}
    
	@isTest static void test_method_4()
	{
        Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');             
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;
        System.runAs(u)
        {
            
		Test.startTest();
        //String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(''), 'UTF-8');
        Test.setCurrentPageReference(new PageReference('Page.ELE_stakeholderDashboard')); 
		//System.currentPageReference().getParameters().put('em', strEncryptEmail);
		System.currentPageReference().getParameters().put('Param1','p');
		ELE_StakeholderDashboard_crt ele = new ELE_StakeholderDashboard_crt();
		Test.stopTest();
        }
	}
    
    @isTest static void test_method_5()
	{
        Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');             
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;
        System.runAs(u)
        {
            
		Test.startTest();
            con.Email=null;
            u.Email=null;
        //String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(''), 'UTF-8');
        Test.setCurrentPageReference(new PageReference('Page.ELE_stakeholderDashboard')); 
		//System.currentPageReference().getParameters().put('em', strEncryptEmail);
		System.currentPageReference().getParameters().put('Param1','p');
		ELE_StakeholderDashboard_crt ele = new ELE_StakeholderDashboard_crt();
            ele.sUserEmail=null;
		Test.stopTest();
        }
	}
    
    
	
}