public with sharing class WCT_H1BCAP_EmailtoRm {
Integer Year = Date.Today().Year();
integer currentYear = Year;
Integer NextYear = Year+1;
public User us{get;set;}
public String surid = ApexPages.currentPage().getParameters().get('id');
public String cuupdt = ApexPages.currentPage().getParameters().get('cup');
public string currupdt;
public WCT_H1BCAP_EmailtoRm (ApexPages.StandardController controller) 
{
Us = new user();  
Us= (user)controller.getRecord();   
}

public pagereference SendEmail()
{
 if(Us.HIB_CAP_Financial_Year__c =='' ||Us.HIB_CAP_Financial_Year__c==null )
    {
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Select the Financial Year Field value'));
    return null;
    }
   // string s1 = us.HIB_CAP_Financial_Year__c;
 // String s2 =s1.removeStart('FY-');
// integer i=integer.valueof(s2);
 String str = us.HIB_CAP_Financial_Year__c;
str = str.replaceAll('[^\\d.]', '');
 integer i=integer.valueof(str);
 system.debug('str--------'+str);


    if(i!=currentYear )
    {
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Select the Current  Financial Year Field value'));
    return null;
    } 
List<User> Users = [SELECT Id,Profile.Name,Is_SendRM_Email__c,Year__c,HIB_CAP_Comments__c,RM_Intiation_Date__c,HIB_CAP_Financial_Year__c  FROM user WHERE Profile.Name ='20_H1BCAP_RM' and Is_SendRM_Email__c=FALSE AND Isactive = True limit 100] ;
//List<User> Users = [SELECT Id,Profile.Name  FROM user WHERE id='00540000003XKQC'] ;
    List<user> userUpdateList = new List<user>();
  
    //if(users.Is_SendRM_Email__c==false){   
    for(user u: Users)
    {

       
       u.Is_SendRM_Email__c=TRUE;
       u.Year__c=Year ;
       u.HIB_CAP_Financial_Year__c=Us.HIB_CAP_Financial_Year__c;
       u.HIB_CAP_Comments__c=us.HIB_CAP_Comments__c;
       currupdt = 'Y';
       
       userUpdateList.add(u);
 
       
            
    }
    
    if(userUpdateList.size() > 0)
    {
        update userUpdateList;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm,'Successfully Completed'));
 /*
 //After updated the list of user's & sendrm email should change to false for next time  
     for(integer j = 0 ; j < userUpdateList.size(); j++) 
      {
        userUpdateList[j].Is_SendRM_Email__c=False;
        
       }
  */     
        return null;
    }
    else
    {
   
    
    currupdt = 'N';
    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Evalauation is already completed. Next Cycle will be on '+ NextYear));
    return null;
    }
    
//}
     pagereference ref;
        ref = new pagereference('/apex/WCT_H1BCAP_EmailtoRM?cup='+currupdt);
        ref.setredirect(true);
        return ref;
        //return null;

}
public pagereference cancel()
{
pagereference ref;
        ref = new pagereference('/home/home.jsp');
        ref.setredirect(true);
        return ref;
        //return null;
}
}