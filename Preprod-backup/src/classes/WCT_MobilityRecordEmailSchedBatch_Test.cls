@isTest
public class WCT_MobilityRecordEmailSchedBatch_Test
{
    static testMethod void m1(){
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        mob.WCT_Last_Working_Day_in_US__c=system.today().addDays(16);
        mob.WCT_Travel_End_Date__c=system.today().addDays(3);
        insert mob;
        Test.startTest();
        WCT_MobilityRecordEmailSchedulableBatch createCon = new WCT_MobilityRecordEmailSchedulableBatch();
        system.schedule('New','0 0 2 1 * ?',createCon); 
        Test.stopTest(); 
     } 
     static testMethod void m2(){
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        mob.WCT_Last_Working_Day_in_US__c=system.today().addDays(11);
        mob.WCT_Travel_End_Date__c=system.today();
        insert mob;
        Test.startTest();
        WCT_MobilityRecordEmailSchedulableBatch createCon = new WCT_MobilityRecordEmailSchedulableBatch();
        system.schedule('New1','0 0 2 1 * ?',createCon); 
        Test.stopTest(); 
     }
     static testMethod void m3(){
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        mob.WCT_Last_Working_Day_in_US__c=system.today().addDays(-1);
        mob.WCT_Travel_End_Date__c=system.today().addDays(3);
        insert mob;
        Test.startTest();
        WCT_MobilityRecordEmailSchedulableBatch createCon = new WCT_MobilityRecordEmailSchedulableBatch();
        system.schedule('New3','0 0 2 1 * ?',createCon); 
        Test.stopTest(); 
     } 
      
}