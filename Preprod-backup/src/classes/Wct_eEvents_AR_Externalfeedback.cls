public class Wct_eEvents_AR_Externalfeedback {

    public Boolean AR_Outcome_No_direct_outcome { get; set; }

    public String AR_Outcome_Other_specify { get; set; }

    public Boolean AR_Outcome_Other { get; set; }

    public String AR_Name_of_the_alum { get; set; }

    public Boolean Made_contact_with_potential_boomerang { get; set; }

    public String Client_name_and_opportunity_2 { get; set; }

    public Boolean Contributed_to_a_win_or_likeli { get; set; }

    public String Client_name_and_opportunity { get; set; }

    public Boolean Contributed_to_an_opportunity_to_propose { get; set; }

    public String Name_and_Company_of_alum { get; set; }

    public Boolean Scheduled_follow_up_meeting_with_alum { get; set; }

    public String AR_what_would_you_recommend { get; set; }

    public String AR_your_level_other_specify { get; set; }

    public String AR_Deloitte_email_address { get; set; }

    public boolean thankyoumess { get; set; }
    public boolean mainpage { get; set; }
public Feedback__c EF{get;set;} 
public Event__c event{get;set;}
public list<contact> liscon{get;set;}
public string CPE{get;set;}
public string Generational{get;set;}
public string FunctionSpecific{get;set;}
public string IndustrySpecific{get;set;}
public string region_specific{get;set;}
public string currentcompany{get;set;}
public String eid = ApexPages.currentPage().getParameters().get('eid');
public String conid = ApexPages.currentPage().getParameters().get('conid');
public  Wct_eEvents_AR_Externalfeedback()
{
mainpage=true;
thankyoumess=false;
event=[select id,name,School__c,AR_image_for_registration_page__c,End_Date_Time__c,Start_Date_time__c,NIR_Venue__c,recordtype.name from event__c where id=:eid];

}

public string Q1{get;set;}
public string Q2{get;set;}
public string Q3{get;set;}
public string Q4{get;set;}
public string Q5{get;set;}
public string function{get;set;}
public string Level{get;set;}
public string Meet_your_expectations_comment{get;set;}
public string LOI_Others_specify{get;set;}
public string AR_what_can_Deloitte_do_differently{get;set;}


public List<SelectOption> getQA1() {
         List<SelectOption> optns = new List<SelectOption>();
         Schema.DescribeFieldResult fieldResult =  Feedback__c.Did_this_event_meet_your_expectations__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
            {
             optns.add(new SelectOption(f.getLabel(), f.getValue()));
            }
          return optns; 
    }
    
    public List<SelectOption> getQA2() {
        List<SelectOption> optns = new List<SelectOption>();
         Schema.DescribeFieldResult fieldResult =  Feedback__c.Please_select_the_day_of_the_week__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
            {
             optns.add(new SelectOption(f.getLabel(), f.getValue()));
            }
          return optns; 
    }
    public List<SelectOption> getQA3() {
   
         List<SelectOption> optns = new List<SelectOption>();
         Schema.DescribeFieldResult fieldResult =  Feedback__c.AR_Select_the_time_of_day__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
            {
             optns.add(new SelectOption(f.getLabel(), f.getValue()));
            }
          return optns;  
    }
    public List<SelectOption> getQA4() {
    
        List<SelectOption> optns = new List<SelectOption>();
         Schema.DescribeFieldResult fieldResult =  Feedback__c.AR_How_many_events_attended__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
            {
             optns.add(new SelectOption(f.getLabel(), f.getValue()));
            }
          return optns;
    }
    
    public List<SelectOption> getARfunction() {
    
        List<SelectOption> optns = new List<SelectOption>();
         Schema.DescribeFieldResult fieldResult =  Feedback__c.AR_your_function__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
            {
             optns.add(new SelectOption(f.getLabel(), f.getValue()));
            }
          return optns;
    }
    public List<SelectOption> getARlevel() {
    
        List<SelectOption> optns = new List<SelectOption>();
         Schema.DescribeFieldResult fieldResult =  Feedback__c.AR_your_level__c.getDescribe();
         List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry f : ple)
            {
             optns.add(new SelectOption(f.getLabel(), f.getValue()));
            }
          return optns;
    }
    public pagereference feedsubmit()
    {
    Feedback__c EF = new Feedback__c ();
    EF.Did_this_event_meet_your_expectations__c=Q1;
    EF.Meet_your_expectations_comment__c=Meet_your_expectations_comment;
    EF.Please_select_the_day_of_the_week__c=q2;
    EF.AR_Select_the_time_of_day__c=q3;
    EF.AR_How_many_events_attended__c=q4;
    EF.Contact__c=conid;
    if(CPE==''||CPE=='undefined')
    CPE='';
    EF.LOI_CPE__c=CPE;
    if(FunctionSpecific==''||FunctionSpecific=='undefined')
    FunctionSpecific='';
    EF.LOI_Function_Specific__c=FunctionSpecific;
    if(Generational==''||Generational=='undefined')
    Generational='';
    EF.LOI_Generational__c=Generational;
    if(IndustrySpecific==''||IndustrySpecific=='undefined')
    IndustrySpecific='';
    EF.LOI_Industry_Specific__c=IndustrySpecific;
    if(region_specific==''||region_specific=='undefined')
    region_specific='';
    EF.LOI_Office_region_specific__c=region_specific;
    if(currentcompany==''||currentcompany=='undefined')
    currentcompany='';
    EF.LOI_Specific_to_Current_Client__c=currentcompany;
    EF.LOI_Others_specify__c=LOI_Others_specify;
    EF.AR_what_can_Deloitte_do_differently__c=AR_what_can_Deloitte_do_differently;
    EF.Event__c=eid;
    EF.recordtypeid=label.Wct_eEvents_AR_feedback_rec;
    insert EF;
    mainpage=false;
thankyoumess=true;
    return null;
    }
    
    public pagereference feedsubmitinternal()
    {
    Feedback__c EF = new Feedback__c ();
    EF.Did_this_event_meet_your_expectations__c=Q1;
    EF.Meet_your_expectations_comment__c=Meet_your_expectations_comment;
    EF.Please_select_the_day_of_the_week__c=q2;
    EF.AR_Select_the_time_of_day__c=q3;
    EF.AR_How_many_events_attended__c=q4;
    EF.Contact__c=conid;
    if(CPE==''||CPE=='undefined')
    CPE='';
    EF.LOI_CPE__c=CPE;
    if(FunctionSpecific==''||FunctionSpecific=='undefined')
    FunctionSpecific='';
    EF.LOI_Function_Specific__c=FunctionSpecific;
    if(Generational==''||Generational=='undefined')
    Generational='';
    EF.LOI_Generational__c=Generational;
    if(IndustrySpecific==''||IndustrySpecific=='undefined')
    IndustrySpecific='';
    EF.LOI_Industry_Specific__c=IndustrySpecific;
    if(region_specific==''||region_specific=='undefined')
    region_specific='';
    EF.LOI_Office_region_specific__c=region_specific;
    if(currentcompany==''||currentcompany=='undefined')
    currentcompany='';
    EF.LOI_Specific_to_Current_Client__c=currentcompany;
    EF.LOI_Others_specify__c=LOI_Others_specify;
    EF.AR_Deloitte_email_address__c=AR_Deloitte_email_address;
    EF.AR_your_level__c=level;
    EF.AR_your_level_other_specify__c=AR_your_level_other_specify;
    EF.AR_what_would_you_recommend__c=AR_what_would_you_recommend;
    EF.AR_your_function__c=function;
    EF.Scheduled_follow_up_meeting_with_alum__c=Scheduled_follow_up_meeting_with_alum;
    EF.Name_and_Company_of_alum__c=Name_and_Company_of_alum;
    EF.Contributed_to_an_opportunity_to_propose__c=Contributed_to_an_opportunity_to_propose;
    EF.Client_name_and_opportunity__c=Client_name_and_opportunity;
    EF.Contributed_to_a_win_or_likeli__c=Contributed_to_a_win_or_likeli;
    EF.Client_name_and_opportunity_2_c__c=Client_name_and_opportunity_2;
    EF.Made_contact_with_potential_boomerang__c=Made_contact_with_potential_boomerang;
    EF.AR_Name_of_the_alum__c=AR_Name_of_the_alum;
    EF.AR_Outcome_Other__c=AR_Outcome_Other;
    EF.AR_Outcome_Other_specify__c=AR_Outcome_Other_specify;
    EF.AR_Outcome_No_direct_outcome__c=AR_Outcome_No_direct_outcome;
    EF.Event__c=eid;
    EF.recordtypeid=label.Wct_eEvents_AR_feedback_rec;


    insert EF;
    mainpage=false;
thankyoumess=true;
    return null;
    }
    
}