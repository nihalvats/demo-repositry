/**
    * Class Name  : WCT_UploadCases_Test
    * Description : This apex test class will use to test the WCT_UploadCases
*/

@isTest
private class WCT_UploadImmigration_Test{

    public static List<WCT_Immigration_Stagging_Table__c> immgStageList=new List<WCT_Immigration_Stagging_Table__c>();
        
     /** 
        Method Name  : createImmigrationStageTable
        Return Type  : List<WCT_Immigration_Stagging_Table__c>
        Type      : private
        Description  : Create Immigration Stage Table Records.         
    */
    private Static List<WCT_Immigration_Stagging_Table__c> createImmigrationStageTable()
    {
      immgStageList = WCT_UtilTestDataCreation.createImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_CORRECTED);
      insert immgStageList;
      return  immgStageList;
    }
    
        
    /** 
        Method Name  : readFileMethod
        Return Type  : void
        Type      : Test Method
        Description  : Test will do read file in all cases.         
    */         
    static testMethod void readFileMethod(){
        String csvheader = 'Identifier,Last Name,First Name,Email Address,Case Type,Category,L-1 Blanket or Individual,'+
            'H-1B Cap,Action [EOS, COS, COE, Amendment],Date Initation Sent to Fragomen,Initiator,Initiator Email Address,'+
            'Milestone,Milestone Date,RFE Due Date,Petition Start Date,Petition End Date,Personnel Number';
        String dataRow = '1,Ambrose,James,email1@tst.deloitte.com,H-1B,test,test,Cap,test,4/2/2015,Shivani,email2@tst.deloitte.com,'+
            'LCA filed,4/2/2014,4/2/2014,4/2/2014,4/2/2014,123456,5000$,Deloitte,Texas';
        String blobCreator = csvheader + '\r\n' + dataRow ;
        PageReference pageRef = Page.WCT_UploadImmigration;
        Test.setCurrentPageReference(pageRef);
        WCT_UploadImmigration uploadImmigrationsCls = new WCT_UploadImmigration();
        uploadImmigrationsCls.contentFile=blob.valueof(blobCreator);
        uploadImmigrationsCls.readFile();
        List<WCT_Immigration_Stagging_Table__c> imigrationStageRecordList=uploadImmigrationsCls.getAllStagingRecords();
        System.assertEquals(1, imigrationStageRecordList.Size());
    }
    
     /** 
        Method Name  : fileNotValid
        Return Type  : void
        Type      : Test Method
        Description  : Test will do when no file will be selected or file is not valid CSV.         
    */  
    static testMethod void fileNotValid() {
      PageReference pageRef = Page.WCT_UploadImmigration;
      Test.setCurrentPageReference(pageRef);
      WCT_UploadImmigration uploadImmigrationsCls = new WCT_UploadImmigration();
      uploadImmigrationsCls.readFile();
      List<Apexpages.Message> msgs = ApexPages.getMessages(); 
      boolean b = false;
      for(Apexpages.Message msg:msgs){
          if (msg.getDetail().contains(Label.Upload_Case_Exception)) b = true;
       }
       system.assert(b);
    }
    
     /** 
        Method Name  : noOfColsException
        Return Type  : void
        Type      : Test Method
        Description  : Test will do on number of column in CSV and send error message on page.         
    */  
    static testMethod void noOfColsException() {      
      String blobCreator = 'Identifier' + '\r\n' + 'Initiator' + '\r\n' + 'Milestone' + '\r\n' ; 
      PageReference pageRef = Page.WCT_UploadImmigration;
      Test.setCurrentPageReference(pageRef);
      WCT_UploadImmigration uploadImmigrationsCls = new WCT_UploadImmigration();
      uploadImmigrationsCls.contentFile = blob.valueof(blobCreator);
      uploadImmigrationsCls.readFile();
      List<Apexpages.Message> msgs = ApexPages.getMessages(); 
      boolean b = false;
      for(Apexpages.Message msg:msgs){
          if (msg.getDetail().contains(Label.Columns_count_not_proper)) b = true;
       }
       system.assert(b);
    }
    
}