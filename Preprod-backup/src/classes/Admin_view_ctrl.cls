/********************************************************************************************************************************
Apex class         : admin_View_CTLR
Description        : Controller which handles the view for the requestor for its Request
Type               : Controller
Test Class         : admin_view_test

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 02                 Deloitte                   26/07/2016          89%                         <Req / Case #>                  Original Version
************************************************************************************************************************************/
global class Admin_view_ctrl extends SitesTodHeaderController
{
    public String setBtnValue{set;get;}//holds the record id
    public Case_form_Extn__c treq{get;set;} // holds the record value of Case_form_Extn__c
    public list<Case> reportingList;// holds the list of records of Case_form_Extn__c
    public string comment {get;set;}// holds the reopen comment value
    ApexPages.StandardController controller;// standard controller
    //public List<caseFormWrapper> caseFormWrapperList{get;set;}
    //public list<case> caseQuestionsList{get;set;}// holds the case question/suggestions list
    public string currentFeedBackId{get;set;}// to hold the feedback id
    Public decimal promptness{get;set;}
    Public decimal accuracy{get;set;}
    Public decimal helpfullness{get;set;}
    Public string commentbox{get;set;}
    public string csename{get;set;}
    public string pagename{get;set;}
    public  Map<id,list<Case_form_Extn__c>> mapcfe{get;set;}
    // constructor 
    public  Admin_view_ctrl()
    {
        //method to Initializing the Reporting object.
        init();
        // Method which returns the requested requests by the requestor 
        getReportingInfo();
        
        
    }
    
    /********************************************************************************************
*Method Name             : Init
*Return Type             : None
*Param’s                 : None
*Description             : method to Initializing the Reporting object.
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 02                 Original Version
*********************************************************************************************/
    public Void Init()
    {
        
        //Initializing the Reporting object
        Treq = new Case_form_Extn__c();
        
    }
    /********************************************************************************************
*Method Name             : getReportingInfo
*Return Type             :Page reference
*Param’s                 : None
*Description             : Method which returns the requested requests by the requestor
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 02                 Original Version
*********************************************************************************************/
    
    public list<Case> getReportingInfo()
    {
        // List the pass the values in Apex Page
        system.debug('loggedInContact111'  +loggedInContact);
        if(loggedInContact != null)
        {
            reportingList=[Select id,CaseNumber,owner.Name,Status,CreatedDate,Description,Contact.id,(select id,TRT_Feedback__c  from Case_Form_Extension__r) from Case where Contact.id=:loggedInContact.Id and RecordType.Name='Case' and WCT_Category__c='Technology Issue - Talent' ORDER BY CreatedDate ASC];
            system.debug('reportinglist' +reportinglist);
            // Map<id,list<Case_form_Extn__c>> mapcfe = new  Map<id,list<Case_form_Extn__c>> ();
            //  for (case c:reportingList){
            //     mapcfe.put(c.id,c.Case_Form_Extension__r);
            // }
        }
        return reportingList;
        
    }
    
    
    
    
    /*
@Name        : rateAsset
@description : The following method upserts the Rating record if the User rate an report
@parameters  : ReportingId(ID),rating(decimal)
@returns     : Boolean
*/
    
    // for submitting feedback
    @RemoteAction
    global static pagereference submit(String currentFeedBackId_1,decimal accuracy_1,decimal promptness_1,decimal helpfullness_1, decimal feedback,  string commentbox_1 ){
        
        Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        string rtId =rtMapByName.get('MIS Feedback').getRecordTypeId();//particular RecordId by  Name
        
        Case_form_Extn__c treq= new Case_form_Extn__c();
        system.debug('record'+treq);
        system.debug('feedback'+rtid);
        treq.RecordTypeId=rtId;
        treq.GEN_Case__c=currentFeedBackId_1;
        treq.TRT_Accuracy__c=accuracy_1;
        treq.TRT_Promptness__c=promptness_1;
        treq.TRT_Helpfulness__c=helpfullness_1;
        treq.TRT_Feedback_Comments__c=commentbox_1;
        treq.TRT_Feedback__c= feedback;
        
        try{
            
            insert treq;
            system.debug('recordinserted'+treq);
            
        }
        catch(Exception e){
            system.debug('An exception occurred'+e);
            Exception_Log__c log = WCT_ExceptionUtility.logException('Admin view ctrl', 'Admin view page', e.getMessage());
        }
        return null;
        
    }
    
    
    
    
}