global class GENERATE_PDF_OF_RECORD
{
     WebService static string generateCFEPDF(String id)
        {
            
            Case_Form_Extn__c CFEName = [SELECT Name,id, GEN_Case__c from Case_Form_Extn__c WHERE id=:id];

            PageReference pageRef= Page.TOD_RECORD_TO_PDF;
            pageRef.getParameters().put('id',id);
            
            Attachment attachment= new Attachment();
            attachment.parentId=CFEName.GEN_Case__c ;
            
               Blob content;
               if (Test.IsRunningTest())
               {
                   attachment.Body=Blob.valueOf('Sample');
               }
               else
               {
                    attachment.Body=pageRef.getContentAsPDF();
               }
            
            
            
            attachment.Name=CFEName.Name;
            attachment.ContentType='application/pdf';
            
            insert attachment;
            return null;
        }
}