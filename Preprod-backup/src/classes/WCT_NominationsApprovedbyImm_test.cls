@isTest(seealldata = true)
private class WCT_NominationsApprovedbyImm_test {

        
  static testMethod void UnitTest(){
         
   Recordtype r = [select id,Name from Recordtype where DeveloperName  = 'WCT_Employee'];     
    Contact con=WCT_UtilTestDataCreation.createContact();
     con.email='test@deloitte.com';
      con.Recordtypeid = r.id ;         
       insert con;
       
       WCT_H1BCAP__c h1 = new WCT_H1BCAP__c();
        h1.WCT_H1BCAP_Practitioner_Name__c = con.id;
         h1.WCT_H1BCAP_Email_ID__c = 'svalluru@deloitte.com';
          h1.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
           h1.WCT_H1BCAP_Status__c = 'Review Practitioner Nomination';
            h1.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'abc@gmail.com';
           insert h1;
          list<id> lstids = new list<id>();
         lstids.add(h1.id);
        Test.starttest();
       WCT_NominationsApprovedbyImm contr = new WCT_NominationsApprovedbyImm();
      WCT_NominationsApprovedbyImm.NominationsApprovedbyImm(lstids);
     Test.stoptest();
           
        
     }
     
     static testMethod void UnitTest1(){
         
   Recordtype r = [select id,Name from Recordtype where DeveloperName  = 'WCT_Employee'];     
    Contact con=WCT_UtilTestDataCreation.createContact();
     con.email='test@deloitte.com';
      con.Recordtypeid = r.id ;         
       insert con;
     list<WCT_H1BCAP__c>  lst = new list<WCT_H1BCAP__c>();
       WCT_H1BCAP__c h1 = new WCT_H1BCAP__c();
        h1.WCT_H1BCAP_Practitioner_Name__c = con.id;
         h1.WCT_H1BCAP_Email_ID__c = 'svalluru@deloitte.com';
          h1.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
           h1.WCT_H1BCAP_Status__c = 'USI Approved';
            h1.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'abc@gmail.com';
            lst.add(h1);
           insert lst;
          list<id> lstids = new list<id>();
         lstids.add(lst[0].id);
        Test.starttest();
       WCT_NominationsApprovedbyImm contr = new WCT_NominationsApprovedbyImm();
      WCT_NominationsApprovedbyImm.NominationsApprovedbyImm(lstids);
     Test.stoptest();
           
        
     }
     
   }