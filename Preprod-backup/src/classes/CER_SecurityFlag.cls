public class CER_SecurityFlag {

   public  List<CER_Expense_Reimbursement__c> expenses {get; set;}
   public CER_Expense_Reimbursement__c currentExpense{get; set;}
   public boolean isInline{get; set;}
   public CER_SecurityFlag(ApexPages.StandardController controller)
    {
        isInline=true;
        Id expenseId= controller.getId();
        List<CER_Expense_Reimbursement__c> currentExpenses=[Select Id,CER_Is_Cand_Tracker_Present__c, Name,CER_Requester_Name__c,CER_Requester_Name__r.Name,CER_Requester_Name__r.Email, CER_Expense_Grand_Total__c,    CER_Travel_Departure_Date__c, CER_Travel_Return_Date__c From CER_Expense_Reimbursement__c where id=:expenseId];
        if(currentExpenses.size()>0)
        {
          currentExpense=currentExpenses[0];
          expenses= [Select Id, Name, CreatedDate, CER_Request_Status__C,CER_Expense_Grand_Total__c,    CER_Travel_Departure_Date__c, CER_Travel_Return_Date__c  From CER_Expense_Reimbursement__c where CER_Requester_Name__c=:currentExpense.CER_Requester_Name__c and id!=:currentExpense.id];
        }
        if(ApexPages.currentPage().getParameters().get('isFullPage')!=null)
        {
            isInline=false;
        }
    }
}