@isTest

private class WCT_ContactTriggerHandler_Test
{
    


static testMethod void Test() 
{
    test.startTest();
    
    Contact adhocContact= WCT_UtilTestDataCreation.createAdhocContactsbyEmail('abc@deloitte.com');
    insert adhocContact;
    Id AdhocID=adhocContact.id;
    try{
    insert WCT_UtilTestDataCreation.createAdhocContactsbyEmail('abc@deloitte.com');}
    catch(exception e){
    }
    
    Profile p = [SELECT Id FROM Profile WHERE Name=:WCT_UtilConstants.RECRUITER_COMPANY]; 
    Test_Data_Utility.createAccount();
    User recruitingCoordinator = WCT_UtilTestDataCreation.createUser('testest',p.id,'gjdfas@test.com','email7152@testing.com');
    insert recruitingCoordinator;
    WCT_Requisition__c Requisition_1= WCT_UtilTestDataCreation.createRequisitionbyUserEmails(null,recruitingCoordinator.Email);
    insert Requisition_1;
    insert WCT_UtilTestDataCreation.createCandidateRequisition(AdhocId,Requisition_1.id);
    insert WCT_UtilTestDataCreation.createAdhocCase(AdhocId);
    insert WCT_UtilTestDataCreation.createAdhocTask(AdhocId);
    insert WCT_UtilTestDataCreation.createAdhocCandidateDocument(AdhocId);
    insert WCT_UtilTestDataCreation.createAdhocNotes(AdhocId);
    insert WCT_UtilTestDataCreation.createAdhocAttachment(AdhocId);
    insert WCT_UtilTestDataCreation.createContactbyEmail('abc@deloitte.com');
    
    test.stopTest();      
   
}  
    

static testMethod void m1(){

            test.startTest();
            list<account> acclist = new list<account>();
            list<Contact> conlist = new list<Contact>();
            list<Contact> conlist1 = new list<Contact>();
            list<Contact> conlist2 = new list<Contact>();

        Contact cvar =  new Contact();
        
        cvar.Lastname = 'sample';
        cvar.email = 'abc@deloitte.com'+'.merged';
        cvar.WCT_Related_Contact_Status__c= 'Merged' ;
        cvar.AR_Current_Company__c = null;
       
        insert cvar;
        
        Contact cvar1 =  new Contact();
        
        cvar1.Lastname = 'sample';
        cvar1.email = 'abc@deloitte.com'+'.merged';
        cvar1.WCT_Related_Contact_Status__c= 'Merged' ;
        //cvar.AR_Current_Company__c = null;
        insert cvar1;

        Contact convar = new Contact();
        convar.lastname = 'A';
        convar.WCT_Region__c = 'INDIA/REGION10';
        convar.recordtypeId = '01240000000QLGb';
        convar.WCT_Ad_Hoc_Contact__c = cvar.id;
        convar.Email = 'abc@deloitte.com';
        convar.WCT_Designation_Code__c = 'PC';
        convar.WCT_Lookup_Code__c = 'HYD3I330I08';
        convar.WCT_Sub_Area_Code__c = 'FAS';
        convar.AR_Current_Employer__c ='sample';
        convar.WCT_Contact_Type__c = 'Candidate';
        conlist.add(convar);
        
        
        
        
        
       Contact convar1 = new Contact();
        convar1.lastname = 'A';
        convar1.WCT_Region__c = 'INDIA'; 
        convar1.recordtypeId = '01240000000QLGb';
        convar1.Email = 'test@gmail.com';
        convar1.AR_Current_Employer__c ='sample';
        insert convar1;
        conlist1.add(convar1);  
       // insert conlist1;
        insert conlist;
      Contact convar2 = new Contact();
        convar2.lastname = 'A';
        convar2.WCT_Region__c = 'INDIA';
        convar2.recordtypeId = '01240000000QLGb';
        
        insert convar2;
        
        account acc = new account();
        acc.name = 'Deloitte US-India Offices';
        insert acc;
        account acc1 = new account();
        acc1.name = 'Not Applicable';
        insert acc1;
        account acc2 = new account();
        acc2.name = 'Deloitte US Offices';
        insert acc2;
        account acc3 = new account();
        acc3.name = 'Candidates';
        insert acc3;
        account acc4 = new account();
        acc4.name = 'Friend';
        insert acc4;
        account acc5 = new account();
        acc5.name = 'Not Available';
        insert acc5;
        acclist = [select id,name from account where name = 'Deloitte US-India Offices' or name = 'Not Applicable' or name = 'Deloitte US Offices' or name ='Not Available' or name ='Candidates' or name='Friend'] ;
        test.stopTest();   
    ContactTriggerHandler_AT ct = new ContactTriggerHandler_AT();
    ct.PopulateAccount(conlist); 
    ct.checkDuplicateEmail(conlist1); 
    ct.checkDuplicateEmail(conlist2); 
    ct.MergeAdhocCandidateToCandidate(conlist);
   //ct.avoidEmployeeToContactOnTaleoLoad(conlist,map1);
   // ct.InsertOfferLetter(conlist);
    //ct.IsChanged(conlist);
    ct.handleTaleoLoad(conlist);
    //ct.UpdateOldwithNewFields(convar2);
    //ct.assignSchoolName(conlist);
    //ct.updateWrittenOfferExtendedDate(conlist);
    ct.InsertOfferLetteronConInsert(conlist);
    ct.doNeedToUpdateCmpLookup(cvar,cvar1);
    ct.assignCompanyName(conlist,conlist1);
    //ct.checkSeparatedFlag(conlist);

}


static testMethod void m2(){

            test.startTest();
            list<account> acclist = new list<account>();
            list<Contact> conlist = new list<Contact>();
            list<Contact> conlist1 = new list<Contact>();
            list<Contact> conlist2 = new list<Contact>();
            
            Contact cvar =  new Contact();
        
        cvar.Lastname = 'sample';
        cvar.email = 'abc@deloitte.com'+'.merged';
        cvar.WCT_Related_Contact_Status__c= 'Merged' ;
        insert cvar;
        
        

        Contact convar = new Contact();
        convar.lastname = 'A';
        convar.WCT_Region__c = 'REGION10';
        convar.recordtypeId = '01240000000QLGb';
        convar.WCT_Employment_Application_Complete__c = 'No';
        convar.WCT_Taleo_Status__c = 'Offer - To be Extended';
        convar.WCT_Requisition_Number__c = '12345';
        convar.WCT_Contact_Type__c = 'Candidate';
        convar.Email = 'abc@deloitte.com';
        conlist.add(convar);
         Contact convar1 = new Contact();
        convar1.lastname = 'A';
        convar1.WCT_Region__c = 'REGION10';
        convar1.recordtypeId = '01240000000QLGb';
        convar1.WCT_Employment_Application_Complete__c = 'Yes';
        convar1.WCT_Taleo_Status__c = 'Offer Details Updated';
        convar1.WCT_Requisition_Number__c = '12345';
        convar1.WCT_Contact_Type__c = 'Employee';
        
        
        conlist1.add(convar1);
        
        
        Contact convar2 = new Contact();
        convar2.lastname = 'A';
        convar2.WCT_Region__c = 'REGION10';
        convar2.recordtypeId = '01240000000QLGb';
        convar2.WCT_Employment_Application_Complete__c = 'Yes';
        convar2.WCT_Taleo_Status__c = 'Offer Details Updated';
        convar2.WCT_Requisition_Number__c = '12345';
        convar2.WCT_Contact_Type__c = 'Employee';
        
        //insert convar2;
        
        Case cvar1 = WCT_UtilTestDataCreation.createCase(cvar.id);
        
        insert cvar1;
        
        

        //convar2.WCT_Contact_Type = 'Candidate';
        conlist2.add(convar2);
        
        //insert convar2;
        
      WCT_Candidate_Requisition__c canrecvar = WCT_UtilTestDataCreation.createAdhocCandidateRequisition(cvar.id);
      
      insert canrecvar;
      
            
        
     Map<id,Contact> mapvar = new Map<id,Contact>(); 
     
     mapvar.put(convar2.id,convar2);
     
     boolean bvar = true;  
     
     
         ContactTriggerHandler_AT ct = new ContactTriggerHandler_AT();
         ct.PopulateAccount(conlist);
         ct.MergeAdhocCandidate(mapvar,bvar);
         ct.avoidEmployeeToContactOnTaleoLoad(conlist,mapvar);
         ct.InsertOfferLetter(conlist,mapvar);
         ct.handleTaleoLoad(conlist1);
      test.stopTest(); 
      
    
  }
  
static testMethod void m3(){

            test.startTest();
            list<account> acclist = new list<account>();
            list<Contact> conlist = new list<Contact>();
            list<Contact> conlist1 = new list<Contact>();
            list<Contact> conlist2 = new list<Contact>();

        Contact convar = new Contact();
        convar.lastname = 'A';
        convar.WCT_Region__c = 'REGION10';
        convar.recordtypeId = '01240000000QLGb';
        convar.WCT_Taleo_Status__c = 'Offer - To be Extended';
        convar.WCT_Requisition_Number__c = '12345';
        convar.WCT_Employment_Application_Complete__c = 'No';
        convar.Email = 'test@gmail.com';
        convar.WCT_CN_Other_Institution__c = 'sample';
        convar.WCT_Name_Suffix__c = 'sample';
        convar.WCT_Primary_City_of_Residence__c = 'sample';
        convar.WCT_Gender__c = 'sample';
        convar.WCT_Education_Level__c = 'sample';
        convar.WCT_Hiring_Location__c ='sample';
        convar.WCT_GLS_Belit_Score__c = 123;
        convar.WCT_Candidate_Program__c = 'sample';
        convar.WCT_GLS_Versant_Score__c = 123;
        convar.WCT_Home_Country__c = 'sample';
        convar.WCT_Home_Zip__c='sample';
        convar.WCT_Middle_Name__c='sample';
        convar.WCT_Preferred_Name__c='sample';
        convar.WCT_Race__c='sample';
        convar.HomePhone= '123';
        convar.MobilePhone='2414';
        convar.WCT_Home_State__c='sample';
        convar.LastName='sample';
        convar.WCT_Candidate_Hiring_Level__c ='High';
        convar.WCT_Home_Street_2__c = 'sample';
        convar.FirstName = 'hi';
        convar.WCT_Home_Street__c ='sample';
        convar.WCT_Taleo_Id__c = '12345';
        convar.WCT_Candidate_School__c = 'india';
        convar.WCT_CN_Other_Employer__c = 'sample';
        convar.Email = 'test@gmail.com';
        convar.Is_Merged_Contact_Attached__c = true;
        convar.WCT_Entity__c = 'sample1';
        //convar.WCT_Type__c = 'Separated';
        convar.WCT_Contact_Type__c = 'Candidate';
        convar.WCT_Employee_Group__c  ='Separated';
        
        conlist.add(convar);
        
        Contact convar1 = new Contact();
        convar1.lastname = 'A';
        convar1.WCT_Region__c = 'REGION10';
        convar1.recordtypeId = '01240000000QLGb';
        convar1.WCT_Taleo_Status__c = 'Offer - To be Extended';
        convar1.WCT_Employment_Application_Complete__c = 'Yes';
        convar1.WCT_Requisition_Number__c = '12345';
        convar1.WCT_Employee_Status__c = 'Retiree';
        //convar1.WCT_Taleo_Status__c = 'Offer - To be Extended';
        convar1.WCT_Entity__c = 'sample';
        convar1.WCT_Contact_Type__c = 'Employee';
        convar1.WCT_Employee_Group__c  ='Separated';
        
        
        
        
        conlist1.add(convar1);
       // insert convar1;
       
       User uvar = WCT_UtilTestDataCreation.createUser('sa','00e40000001BemV','huhbh@test.com','sa@test.com');
       
       User uvar1 = WCT_UtilTestDataCreation.createUser('samp','00e40000001BemW','samp@testing.com','sam@test.com');
       
       insert uvar;
       insert uvar1;
       
     Contact adhocContact= WCT_UtilTestDataCreation.createAdhocContactsbyEmail('abc@deloitte.com');
    insert adhocContact;
    Id AdhocID=adhocContact.id;
    
    
       
       Contact convar2 = new Contact();
        convar2.lastname = 'A';
        convar2.WCT_Region__c = 'REGION10';
        convar2.recordtypeId = '01240000000QLGb';
        convar.WCT_Taleo_Status__c = 'Offer Details Updated';
        convar2.WCT_Employment_Application_Complete__c = 'Yes';
        convar2.WCT_Requisition_Number__c = '12345';
        convar2.WCT_Employee_Status__c = 'Retiree';
        convar2.WCT_Taleo_Status__c = 'Offer - Negotiating';
        convar2.Email = 'sample@deloitte.com';
        
        insert convar2;
        
       
        
        //insert WCT_UtilTestDataCreation.createTask(AdhocId);
          Contact cvar =  new Contact();
        
        cvar.Lastname = 'sample';
        cvar.email = 'abc@deloitte.com'+'.merged';
        cvar.WCT_Related_Contact_Status__c= 'Merged' ;
        cvar.WCT_Requisition_Number__c = '12345';
        insert cvar;

       
       WCT_Requisition__c  reqvar = new WCT_Requisition__c();
       reqvar.WCT_Requisition_Number__c = '12345';
       reqvar.Name = 'sample';
       reqvar.WCT_Recruiter__c = uvar.id;
       reqvar.WCT_Recruiting_Coordinator__c = uvar1.id;
       reqvar.WCT_FSS__c = 'All';
       
       insert reqvar;
       
       
       
            
    WCT_List_Of_Names__c wlistvar = new WCT_List_Of_Names__c();
     
    wlistvar.WCT_Office_Location_Hiring_Location__c = 'sample';
    wlistvar.Recordtypeid = '01240000000QLhb';
    wlistvar.WCT_Hiring_Location__c = 'sample';
    wlistvar.WCT_Type__c = 'sample';
    
    insert wlistvar;
    
     Map<id,Contact> mapvar = new Map<id,Contact>(); 
     
     mapvar.put(convar1.id,convar1);
     
     Map<string,Contact> mapvar1 = new Map<string,Contact>(); 
     
     mapvar1.put('test@gmail.com',convar);
     
    //  Map<id,id> mapvar2 = new Map<id,id>(); 
    //  mapvar2.put(AdhocId,convar1.id);
      
      Map<id,Contact> mapvar3 = new Map<id,Contact>(); 
      mapvar3.put(convar.id,convar);
      
      Map<id,Contact> mapvar5 = new Map<id,Contact>(); 
      mapvar5.put(adhocContact.id,adhocContact);
      
      id conid ;
      conid = convar2.id;
      
      
      list<Id> conIds = new list<Id>();
      
      conIds.add(conid);
      
      
     
     boolean bvar = true;  
     
       WCT_Offer_Data_Fields_Parameters__c  offdatevar = new WCT_Offer_Data_Fields_Parameters__c();
       
       offdatevar.WCT_Car_Lease_Eligibility__c = 123;
       offdatevar.WCT_Communication_Allowance__c = 123;
       offdatevar.WCT_Designation_Code__c = 'sample';
       offdatevar.WCT_Driver_Code__c = 'sample';
       offdatevar.WCT_Final_Code__c = 'sample';
       offdatevar.WCT_Final_Title_to_be_used__c = 'sample';
       offdatevar.WCT_Fuel_Designation__c = 'sample';
       offdatevar.WCT_Look_Up_Code__c = 'sample';
        offdatevar.Name = 'Sample';
        offdatevar.WCT_Designation_Code__c = 'AA';
        //convar1.WCT_Lookup_Code__c = 'I442';
        offdatevar.WCT_Sub_Area_Code__c = 'FAS';
       upsert offdatevar ;
       
      WCT_Offer_Data_Fields_Parameters__c  offdatavar = new WCT_Offer_Data_Fields_Parameters__c();
      
      offdatavar.WCT_Car_Lease_Eligibility__c = 123;
      offdatavar.WCT_Communication_Allowance__c = 123;
      offdatavar.WCT_Designation_Code__c = 'sample';
      offdatavar.WCT_Driver_Code__c = 'sample';
      offdatavar.WCT_Final_Code__c = 'sample';
      offdatavar.WCT_Final_Title_to_be_used__c = 'sample';
      offdatavar.WCT_Fuel_Designation__c = 'sample';
      offdatavar.WCT_Look_Up_Code__c = 'sample';
      offdatavar.WCT_Medical_Insurance_Premium__c= 123;
      offdatavar.WCT_Sub_Area_Code__c = 'sample';
      offdatavar.WCT_US_Level__c = 'sample';
      offdatavar.WCT_US_Level_Designation__c = 'sample';
      offdatavar.WCT_Variable_Percentage__c = 'sample';
      offdatavar.WCT_Vehicle_Running_Expenses__c = 123;
      offdatavar.name ='sample';
      
      list<WCT_Offer_Data_Fields_Parameters__c> off1 =  new  list<WCT_Offer_Data_Fields_Parameters__c>();
      
      off1.add(offdatavar);
      
      insert off1;
       
     list<WCT_Offer_Data_Fields_Parameters__c> off =  new  list<WCT_Offer_Data_Fields_Parameters__c>();
     
        off = [SELECT WCT_Driver_Code__c, WCT_Look_Up_Code__c, WCT_Sub_Area_Code__c, WCT_US_Level__c, 
                                                                                WCT_Designation_Code__c, WCT_Car_Lease_Eligibility__c, WCT_Communication_Allowance__c, 
                                                                                WCT_Final_Code__c, WCT_Final_Title_to_be_used__c, WCT_Fuel_Designation__c, 
                                                                                WCT_Medical_Insurance_Premium__c, WCT_US_Level_Designation__c, WCT_Variable_Percentage__c, 
                                                                                WCT_Vehicle_Running_Expenses__c 
                                                                                FROM WCT_Offer_Data_Fields_Parameters__c 
                                                                                WHERE (WCT_Look_Up_Code__c  =: offdatevar.WCT_Look_Up_Code__c ) AND 
                                                                                (WCT_Sub_Area_Code__c  =: offdatevar.WCT_Sub_Area_Code__c) AND 
                                                                                (WCT_Designation_Code__c  =: offdatevar.WCT_Designation_Code__c)];
       
         ContactTriggerHandler_AT ct = new ContactTriggerHandler_AT();
         
        ct.InsertOfferLetter(conlist,mapvar);
        ContactTriggerHandler_AT.IsChanged(conlist1,mapvar);
        ct.UpdateOldwithNewFields(convar,mapvar1);
       // ContactTriggerHandler_AT.MergeCanEmpRecords(mapvar2);
        ct.updateWrittenOfferExtendedDate(conlist,mapvar3);
        ContactTriggerHandler_AT.UpDateMergeChkbox(conIds);
        ContactTriggerHandler_AT.checkSeparatedFlag(mapvar3,conlist1);
        ct.InsertOfferLetteronConInsert(conlist1);
        ct.updateCompanyAlumniPresent(conlist,conlist1, 'INSERT');
         ct.assignSchoolName(conlist);
         
         
      test.stopTest(); 
      
    
  }  
  

static  testmethod void m4(){

            test.startTest();
            list<account> acclist = new list<account>();
            list<Contact> conlist = new list<Contact>();
            list<Contact> conlist1 = new list<Contact>();
            list<Contact> conlist2 = new list<Contact>();
            
            Contact adhocContact= WCT_UtilTestDataCreation.createAdhocContactsbyEmail('abc@deloitte.com');
    insert adhocContact;
    Id AdhocID=adhocContact.id;
    
    insert WCT_UtilTestDataCreation.createCase(AdhocId);
        insert WCT_UtilTestDataCreation.createAttachment(AdhocId);
        
        
        
        insert WCT_UtilTestDataCreation.createOfferRec(AdhocId);
       
         
        WCT_Requisition__c recvar1 = new WCT_Requisition__c();
        
        recvar1.Name = '12345';
        
        insert recvar1;
        
        insert WCT_UtilTestDataCreation.createCandidateRequisition(AdhocId, recvar1.id);
     
        
        /*Event_Registration_Attendance__c  EVRvar = new Event_Registration_Attendance__c();
        
        EVRvar.Contact__c = AdhocId;
        EVRvar.Name = 'sample';
        
        insert EVRvar; */
        
        Feedback__c fvar = new Feedback__c();
        fvar.Contact__c = AdhocId;
        fvar.Rating__c = '1';
           
        insert fvar;
        
        Task tvar = new Task();
        
        tvar.WhoId = AdhocId;
        
        insert tvar;
        
        WCT_Candidate_Documents__c  cdvar = WCT_UtilTestDataCreation.createCandidateDocument(AdhocId);
        
        insert cdvar;
        
        Note nvar = new Note();
        
        nvar.ParentId =AdhocId;
        nvar.Title ='Sample';
        nvar.Body = 'sample';
        
        insert nvar;
    
    

        Contact convar = new Contact();
        convar.lastname = 'A';
        convar.WCT_Region__c = 'REGION10';
        convar.recordtypeId = '01240000000QLGb';
        convar.WCT_Taleo_Status__c = 'Offer - To be Extended';
        convar.WCT_Requisition_Number__c = '12345';
        convar.WCT_Employment_Application_Complete__c = 'No';
        convar.Email = 'test@gmail.com';
        convar.WCT_CN_Other_Institution__c = 'sample';
        convar.WCT_Name_Suffix__c = 'sample';
        convar.WCT_Primary_City_of_Residence__c = 'sample';
        convar.WCT_Gender__c = 'sample';
        convar.WCT_Education_Level__c = 'sample';
        convar.WCT_Hiring_Location__c ='sample';
        convar.WCT_GLS_Belit_Score__c = 123;
        convar.WCT_Candidate_Program__c = 'sample';
        convar.WCT_GLS_Versant_Score__c = 123;
        convar.WCT_Home_Country__c = 'sample';
        convar.WCT_Home_Zip__c='sample';
        convar.WCT_Middle_Name__c='sample';
        convar.WCT_Preferred_Name__c='sample';
        convar.WCT_Race__c='sample';
        convar.HomePhone= '123';
        convar.MobilePhone='2414';
        convar.WCT_Home_State__c='sample';
        convar.LastName='sample';
        convar.WCT_Candidate_Hiring_Level__c ='High';
        convar.WCT_Home_Street_2__c = 'sample';
        convar.FirstName = 'hi';
        convar.WCT_Home_Street__c ='sample';
        convar.WCT_Taleo_Id__c = '12345';
        convar.WCT_Candidate_School__c = 'india';
        convar.WCT_CN_Other_Employer__c = 'sample';
        convar.Email = 'test@gmail.com';
        convar.Is_Merged_Contact_Attached__c = true;
        convar.WCT_Designation_Code__c = 'PC';
        convar.WCT_Lookup_Code__c = 'HYD3I330I08';
        convar.WCT_Sub_Area_Code__c = 'FAS';
        
        conlist.add(convar);
        
        Contact convar1 = new Contact();
        convar1.lastname = 'A';
        convar1.WCT_Region__c = 'REGION10';
        convar1.recordtypeId = '01240000000QLGb';
        convar.WCT_Taleo_Status__c = 'Offer Details Updated';
        convar1.WCT_Employment_Application_Complete__c = 'Yes';
        convar1.WCT_Requisition_Number__c = '1234';
        convar1.WCT_Employee_Status__c = 'Retiree';
        convar1.WCT_Taleo_Status__c = 'Offer - Negotiating';
        convar1.WCT_Designation_Code__c = 'PC';
        convar1.WCT_Lookup_Code__c = 'HYD3I330I08';
        convar1.WCT_Sub_Area_Code__c = 'FAS';
        
        conlist1.add(convar1);
        insert conlist1;
       //insert convar1;
           
           
         School__c svar = new School__c();
         
         svar.Name = 'india';
         svar.School_University_Name__c = 'india';   
         
         insert svar;
    
     Map<id,Contact> mapvar = new Map<id,Contact>(); 
     
     mapvar.put(convar1.id,convar1);
     
     Map<string,Contact> mapvar1 = new Map<string,Contact>(); 
     
     mapvar1.put('test@gmail.com',convar);
     
      Map<id,id> mapvar2 = new Map<id,id>(); 
      mapvar2.put(convar.id,convar1.id);
      
      Map<id,Contact> mapvar3 = new Map<id,Contact>(); 
      mapvar3.put(convar.id,convar);
      
      Map<string,string> mapvar4 = new Map<string,string>();
      mapvar4.put('india','india');
      
      Map<id,Contact> mapvar5 = new Map<id,Contact>(); 
      mapvar5.put(adhocContact.id,adhocContact);
      
      
      id conid ;
      conid = convar.id;
      
      
      list<Id> conIds = new list<Id>();
      
      conIds.add(conid);
      
      
     
     boolean bvar = true;  
     
       WCT_Offer_Data_Fields_Parameters__c  offdatevar = new WCT_Offer_Data_Fields_Parameters__c();
       
       list<WCT_Offer_Data_Fields_Parameters__c> listoffdatevar = new list<WCT_Offer_Data_Fields_Parameters__c>();
       
       offdatevar.WCT_Car_Lease_Eligibility__c = 123;
       offdatevar.WCT_Communication_Allowance__c = 123;
       offdatevar.WCT_Designation_Code__c = 'sample';
       offdatevar.WCT_Driver_Code__c = 'sample';
       offdatevar.WCT_Final_Code__c = 'sample';
       offdatevar.WCT_Final_Title_to_be_used__c = 'sample';
       offdatevar.WCT_Fuel_Designation__c = 'sample';
       offdatevar.WCT_Look_Up_Code__c = 'sample';
        offdatevar.Name = 'Sample';
        offdatevar.WCT_Designation_Code__c = 'PC';
        convar1.WCT_Lookup_Code__c = 'HYD3I330I08';
        offdatevar.WCT_Sub_Area_Code__c = 'FAS';
       //upsert offdatevar ;
       
       listoffdatevar.add(offdatevar);
       
       insert listoffdatevar;
       
       WCT_Requisition__c recvar = new WCT_Requisition__c();
       
       
       recvar.WCT_Requisition_Number__c = '12345';
       insert recvar;
       
       ContactTriggerHandler_AT ct = new ContactTriggerHandler_AT();
        ct.InsertOfferLetteronConInsert(conlist1);
        ContactTriggerHandler_AT.SchoolNameList();
      test.stopTest(); 
      
   // ct.MergeAdhocCandidate(mapvar5,bvar);
  } 
  
  static  testmethod void m5(){

   test.startTest();
     Contact adhocContact= WCT_UtilTestDataCreation.createAdhocContactsbyEmail('abc@deloitte.com');
         insert adhocContact;
    Id AdhocID=adhocContact.id;
        insert WCT_UtilTestDataCreation.createCase(AdhocId);
        insert WCT_UtilTestDataCreation.createAttachment(AdhocId);
        insert WCT_UtilTestDataCreation.createOfferRec(AdhocId);
        WCT_Requisition__c recvar1 = new WCT_Requisition__c();
        recvar1.Name = '12345';
        insert recvar1;
        insert WCT_UtilTestDataCreation.createCandidateRequisition(AdhocId, recvar1.id);
    Feedback__c fvar = new Feedback__c();
        fvar.Contact__c = AdhocId;
        fvar.Rating__c = '1';
        insert fvar;
     /* Task tvar = new Task();
        tvar.WhoId = AdhocId;
        insert tvar; */
        WCT_Candidate_Documents__c  cdvar = WCT_UtilTestDataCreation.createCandidateDocument(AdhocId);
        insert cdvar;
        Note nvar = new Note();
        nvar.ParentId =AdhocId;
        nvar.Title ='Sample';
        nvar.Body = 'sample';
        insert nvar; 
    Map<id,Contact> mapvar5 = new Map<id,Contact>(); 
        mapvar5.put(adhocContact.id,adhocContact);
    boolean bvar = true;  
     ContactTriggerHandler_AT ct = new ContactTriggerHandler_AT();
        ct.MergeAdhocCandidate(mapvar5,bvar);
   test.stopTest(); 
  }
  
static testMethod void m7(){

            test.startTest();   
        Contact convar1 = new Contact();
        convar1.lastname = 'A';
        convar1.WCT_Region__c = 'REGION10';
        convar1.recordtypeId = '01240000000QLGb';
        convar1.WCT_Taleo_Status__c = 'Offer - To be Extended';
        convar1.WCT_Employment_Application_Complete__c = 'Yes';
        convar1.WCT_Requisition_Number__c = '12345';
        convar1.WCT_Employee_Status__c = 'Retiree';
        //convar1.WCT_Taleo_Status__c = 'Offer - To be Extended';
        convar1.WCT_Entity__c = 'sample';
        convar1.WCT_Contact_Type__c = 'Employee';
        convar1.WCT_Employee_Group__c  ='Separated';
        
    User uvar = WCT_UtilTestDataCreation.createUser('testest','00e40000001BemV','gjdfas@test.com','email7152@testing.com');
    User uvar1 = WCT_UtilTestDataCreation.createUser('testest1','00e40000001BemV','gjdfas1@test.com','email71521@testing.com');
       insert uvar;
       insert uvar1;
       
    Contact adhocContact= WCT_UtilTestDataCreation.createAdhocContactsbyEmail('abc@deloitte.com');
        insert adhocContact;
        Id AdhocID=adhocContact.id;
        insert WCT_UtilTestDataCreation.createCase(AdhocId);
        insert WCT_UtilTestDataCreation.createAttachment(AdhocId);
        insert WCT_UtilTestDataCreation.createOfferRec(AdhocId);
    WCT_Requisition__c recvar1 = new WCT_Requisition__c();
        recvar1.Name = '12345';
        insert recvar1;
        insert WCT_UtilTestDataCreation.createCandidateRequisition(AdhocId, recvar1.id);
     
    Feedback__c fvar = new Feedback__c();
        fvar.Contact__c = AdhocId;
        fvar.Rating__c = '1';
           
        insert fvar;
     
    WCT_Candidate_Documents__c  cdvar = WCT_UtilTestDataCreation.createCandidateDocument(AdhocId);
        
        insert cdvar;
        
      Note nvar = new Note();
        nvar.ParentId =AdhocId;
        nvar.Title ='Sample';
        nvar.Body = 'sample';
        
        insert nvar;
  
   Map<id,id> mapvar2 = new Map<id,id>(); 
      mapvar2.put(AdhocId,convar1.id);
     
  ContactTriggerHandler_AT ct = new ContactTriggerHandler_AT();
       ContactTriggerHandler_AT.MergeCanEmpRecords(mapvar2);
       
    test.stopTest(); 
      
    
  }       
    
}