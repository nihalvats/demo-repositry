@isTest
public class userDeactivation_test {
    
    public static testmethod  void deact()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User ur1 = new User(Alias = 'sample1', Email='testct1@deloitte.com', 
            EmailEncodingKey='UTF-8', LastName='testct1', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,CommunityNickname='testct1',IsActive=true,
            FederationIdentifier='testct1', 
            TimeZoneSidKey='America/Los_Angeles', UserName='testct1@deloitte.com.wct.prd');  
        insert ur1;
       // func();
        system.runas(ur1){
       Test.StartTest();
        
     UserDeactivation_Scheduler sc1 = new UserDeactivation_Scheduler();
            
            sc1.query='SELECT Id,Email,Name,IsActive,Role_Account__c,Usertype  from User where IsActive=True and Role_Account__c=false LIMIT 5';
     String sch = '0 15 15 * * ?';
    string jobID= system.schedule('Schedule Report class',sch,sc1);
         sc1.execute(null);   
     CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
         FROM CronTrigger WHERE id = :jobId];
           // System.abortJob(ct.id);
          System.assertEquals(sch,ct.CronExpression);
          System.assertEquals(0, ct.TimesTriggered);
        
          

     Test.stopTest();   
     }
        
    }   
    
    
    @future
    public static void func(){
        contact con = new contact();
        con.lastname='test';
        con.Email='test@deloitte.com';
        con.WCT_Contact_Type__c='Employee';
        con.WCT_Employee_Status__c='Active';
        insert con;        
    }
    
    
}