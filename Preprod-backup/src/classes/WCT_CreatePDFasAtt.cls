/**************************************************************************************
Apex Class Name: WCT_CreatePDFasAtt
Created Date     : 12 December 2013
Function         : WS Class for Creatiing .pdf for Inter Forms at Interview.
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                    12/12/2013              Original Version
*************************************************************************************/
@RestResource(urlMapping='/IEF_PDF_Creation/*')
global class WCT_CreatePDFasAtt{
    // Defining the POST method
    @HttpPOST
    global static string createPDFasAttFunc(string InterviewFormId, string InterviewJunctionId)
    {
    
    PageReference pdf = Page.WCT_IEF_PDF_Attachment;
    pdf.getParameters().put('Id',InterviewFormId);
    pdf.getParameters().put('InterviewJunctionId',InterviewJunctionId);
    string CanName, IntvName, fileName, IntvId, InterviewRound,IntvType,IntTrackerName;
    list<WCT_Interview_Junction__c >InvJuncREc = [select WCT_Candidate_Name__c,WCT_Interviewer__r.Name,  WCT_Interview__r.Name, WCT_Interview__r.WCT_Interview_Round__c ,
                                                    WCT_Interview_Stage__c,WCT_Interview_Type__c,Name from WCT_Interview_Junction__c 
                                                    where id=:InterviewJunctionId];
    if(!InvJuncREc.IsEmpty()){
        CanName = InvJuncREc[0].WCT_Candidate_Name__c;
        IntvName = InvJuncREc[0].WCT_Interviewer__r.Name;
        IntvId = InvJuncREc[0].WCT_Interview__r.Name;
        InterviewRound = InvJuncREc[0].WCT_Interview__r.WCT_Interview_Round__c;
        IntvType = InvJuncREc[0].WCT_Interview_Type__c;
        IntTrackerName = InvJuncREc[0].Name;
    } 
    if(InterviewRound==Null)InterviewRound='';
    if(IntvType==Null)IntvType='';
    //fileName = 'IEF_'+IntvName+'_'+CanName+'_'+InterviewRound+'.pdf';
    fileName = CanName+'_'+InterviewRound+'_'+IntvType+'_'+IntvName+'_'+'IEF_'+IntTrackerName+'.pdf';
    Blob body;
 
    try {
        body =  pdf.getContent();
    } catch (VisualforceException e) {
        
        body = Blob.valueOf('Some Text');
    }
    list<Attachment> IsAlreayExist = [select id from Attachment
                                        where ParentId=:InterviewJunctionId 
                                        and Name=:fileName];
        if(!IsAlreayExist.IsEmpty()){
            for(Attachment att : IsAlreayExist){
                att.Body=Body;
            }
        }
        else{
            Attachment IEFasAtt = new Attachment();
            IEFasAtt.ParentId=InterviewJunctionId; 
            
            IEFasAtt.Name=fileName;
            IEFasAtt.Body=Body;
            IEFasAtt.ContentType='application/pdf';
            IsAlreayExist.add(IEFasAtt);
        }
        try{
        upsert IsAlreayExist;
      }
      catch(DMLException ex){
        WCT_ExceptionUtility.logException('CreatePDFasAtt-createPDFasAttFunc', null, ex.getMessage() + ' ::: ' + ex.getStackTraceString());
      }
    return 'SUCCESS';  
    } 
}