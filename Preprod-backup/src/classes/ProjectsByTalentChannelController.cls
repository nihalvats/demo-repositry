public with sharing class ProjectsByTalentChannelController{

/*
 Apex Class:  ProjectsByTalentChannelController
 Purpose: This class is used to prepare view of Projects By talent Channerl
 Created On:  3rd July,2015.
 Created by:  Balu Devarapu.(Deloitte India)
*/


public set<string> lstTalChannel{get;set;}
public List<ChartData> objChartData{get;set;}
public List<SelectOption> TcOptions{get;set;}
public List<SelectOption> PSOptions{get;set;}
public List<SelectOption> PCatOptions{get;set;}
public string SelectedTC{get;set;}
public string SelectedPS{get;set;}
public string SelectedPCat{get;set;}
public Map<string,string> mapTC{get;set;}
public List<integer> lstTCOccurenceCount{get;set;}
set<string> lstTalChannelTemp=new set<string>();
Map<string,integer> mapTcOccournace{get;set;}
Public boolean IsShowTConly=false;
/* Constructor */
public ProjectsByTalentChannelController(){
    IsShowTConly=false;
    lstTCOccurenceCount=new List<integer>();
    mapTcOccournace=new Map<string,integer>();
    SelectedTC='-None-';
    lstTalChannel=new set<string>();
    
    Schema.DescribeFieldResult objlstTC = Project__c.Talent_Channel__c.getDescribe();
    List<Schema.PicklistEntry> lstTC = objlstTC.getPicklistValues();
    
    TcOptions = new List<SelectOption>();
    TcOptions.add(new SelectOption('-None-','-None-'));
    for(Schema.picklistEntry f:lstTC)    
    {    
        if(f.getLabel()!='------------------None------------------'){
          
        lstTalChannel.add(f.getLabel());  
        TcOptions.add(new SelectOption(f.getLabel(),f.getLabel()));     
        }
    }
    lstTalChannelTemp=lstTalChannel;
    PSOptions = new List<SelectOption>();
    Schema.DescribeFieldResult objlstPS = Project__c.Status__c.getDescribe();
    List<Schema.PicklistEntry> lstPS = objlstPS.getPicklistValues();
        for(Schema.picklistEntry f:lstPS)    
        {    
            PSOptions.add(new SelectOption(f.getLabel(),f.getLabel()));     
        }
    SelectedPS='In Progress';   
    
    PCatOptions= new List<SelectOption>();
    PCatOptions.add(new SelectOption('-None-','-None-'));
    Schema.DescribeFieldResult objPCatOptions = Project__c.Project_Category__c.getDescribe();
    List<Schema.PicklistEntry> lstPCat = objPCatOptions.getPicklistValues();
        for(Schema.picklistEntry f:lstPCat)    
        {    
            PCatOptions.add(new SelectOption(f.getLabel(),f.getLabel()));     
        }
    SelectedPCat='-None-';  
    ProcessData();
} 

    /* 
        Method:ProcessData
        Description: Data view Perparation 
    */
public void ProcessData(){
    try{
    lstTCOccurenceCount=new List<integer>();
    mapTcOccournace=new Map<string,integer>();
    objChartData = new List<ChartData>();

    mapTC=new Map<string,string>();

    If(string.IsEmpty(SelectedPS))
    SelectedPS='In Progress';
    If(string.IsEmpty(SelectedPCat) || SelectedPCat=='-None-')
    SelectedPCat=null;
   
    if(IsShowTConly){
        if(SelectedPCat!=null){
            for(Project__c objProj: [select Name,Talent_Channel__c from Project__c where Status__c=:SelectedPS and Talent_Channel__c!=null and Project_Category__c=:SelectedPCat order by Name]) 
            {
                mapTC.put(objProj.Name,objProj.Talent_Channel__c);
            }   
        }else{
            for(Project__c objProj: [select Name,Talent_Channel__c from Project__c where Status__c=:SelectedPS and Talent_Channel__c!=null order by Name]) 
            {
                mapTC.put(objProj.Name,objProj.Talent_Channel__c);
            }   
        }
          
    }
    else{
        if(SelectedPCat!=null){
            for(Project__c objProj: [select Name,Talent_Channel__c from Project__c where Status__c=:SelectedPS and Project_Category__c=:SelectedPCat order by Name]) 
            {
                mapTC.put(objProj.Name,objProj.Talent_Channel__c);
            }   
        }
        else{
            for(Project__c objProj: [select Name,Talent_Channel__c from Project__c where Status__c=:SelectedPS order by Name]) 
            {
                mapTC.put(objProj.Name,objProj.Talent_Channel__c);
            }   
        }

    }
    system.debug('----mapTC-Size--'+mapTC.Size());
    for(string objPName:mapTC.keyset())
    {
        string TalentChannels=mapTC.get(objPName);
        if(string.IsNotEmpty(TalentChannels)){
        List<string> lstTalentChannels=TalentChannels.split(';');
        
        Set<string> setTalentChannels=new Set<string>();
        
        for(string strTC:lstTalentChannels)
        {
          if(lstTalChannel.contains(strTC)){
           setTalentChannels.add(strTC);
           Integer iCount=1;
           if(mapTcOccournace.get(strTC)!=null){
            iCount= mapTcOccournace.get(strTC);
            iCount++;
           }
           mapTcOccournace.put(strTC,iCount);
           
          }
        }
        
        setTalentChannels.remove('------------------None------------------');
        if(IsShowTConly){
        if(setTalentChannels.Size()>0){
         objChartData.add(new ChartData(objPName, setTalentChannels)); 
        } 
        }else{
        objChartData.add(new ChartData(objPName, setTalentChannels));   
        }
    
     }
    }
     system.debug('----objChartData-Size--'+objChartData.Size());
     for(string strTalC:lstTalChannel){
     integer iCount=0;
     if(mapTcOccournace.get(strTalC)!=null){
        iCount= mapTcOccournace.get(strTalC);
       }
     lstTCOccurenceCount.add(iCount);
    }

 
 }catch(Exception e){
  system.debug('----------Exception-------------'+e.getMessage()+'-----at Line #----'+e.getLineNumber()); 
  }
 
 
 
} 

    /* 
    Method:FilterResult
    Description: Populates data based on Filter conditions provided by User. 
    */
public void FilterResult(){
    lstTCOccurenceCount=new List<integer>();
    mapTcOccournace=new Map<string,integer>();
    SelectedPS='';
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SelectedPS'))){
    SelectedPS=apexpages.currentpage().getparameters().get('SelectedPS');
    }
    
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('IsShowTConly'))){
    string strIsShowTConly=apexpages.currentpage().getparameters().get('IsShowTConly');
    system.debug('-------IsShowTCOnly----'+strIsShowTConly);
     if(strIsShowTConly=='true')
         IsShowTConly=true;
     else
         IsShowTConly=false;
    }
    
    SelectedPCat='';
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SelectedPCat'))){
    SelectedPCat=apexpages.currentpage().getparameters().get('SelectedPCat');
    }
    
    
    SelectedTC='-None-';
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SelectedTC'))){
    SelectedTC=apexpages.currentpage().getparameters().get('SelectedTC');
    if(SelectedTC=='-None-'){
    lstTalChannel=lstTalChannelTemp; // Show all Channels;
    }
    else{
    lstTalChannel=new set<string>();
    objChartData = new List<ChartData>();
    lstTalChannel.add(SelectedTC);
    }
    ProcessData();
    }
}


     /* 
    Class:ChartData 
    Description: Wrapper class used to prepare data
    */

    public class ChartData {
        public String ProjectName { get; set; }
        public set<string> ItemsPresent{ get; set; }

        public ChartData(String ProjectName1, set<string> setTalentChannels1) {
            
            this.ProjectName = ProjectName1;
            this.ItemsPresent= setTalentChannels1;

        }
    }


}