public class WCT_SCHDCreateTaskForSepEmp implements Schedulable {
     
    public void execute(SchedulableContext SC) {
        WCT_BatchCreateTaskForSepEmp BCTSE=  new WCT_BatchCreateTaskForSepEmp();
        Database.executeBatch(BCTSE); 
    } 
}