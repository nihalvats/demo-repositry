/*******************************************************************
 APEX CLASS :batchArticleExpNotify
 DESCRIPTION: This batch class is written to update the article to Draft if the article gets expired
 CREATED BY: Chandrasekhar Pinarouthu
 CREATED DATE:8/15/2013
********************************************************************/
global class WCT_batchArticleExpNotify implements Database.batchable< KnowledgeArticleVersion >{

    //Global variable
    global string query;

    /******************************************
    METHOD NAME: Start
    DESCRIPTION: This method will fetch all the article from KnowledgeArticleVersion. 
                 The output of this method is a query passed as an input to Execute method
    ******************************************/
global Iterable< KnowledgeArticleVersion > start(Database.batchableContext info){
        // Fetching all the Article which are Online
   //      List<query> = 'SELECT ArticleNumber,ArticleType,CreatedBy.email,KnowledgeArticleId,Title,UrlName FROM KnowledgeArticleVersion where PublishStatus = \'Online\' and Language = \'en_US\'';
        // Return the query results to execute method
    //    return Database.getQueryLocator([SELECT ArticleNumber,ArticleType,CreatedBy.email,KnowledgeArticleId,Title,UrlName FROM KnowledgeArticleVersion where PublishStatus = 'Online' and Language = 'en_US']);
       return new WCT_KAVExp();
    }
    
    /******************************************
    METHOD NAME: execute
    DESCRIPTION: This method will take the input from the Start method.
                 In this method, atricle type and article number information is segrigated and passed dynamically to a query
    ******************************************/
    global void execute(Database.BatchableContext BC, List < sObject > articles) {

        String query = 'SELECT ArticleNumber,ArticleType,CreatedBy.email,KnowledgeArticleId,Title,UrlName FROM KnowledgeArticleVersion where PublishStatus = \'Online\' and Language = \'en_US\'';
        Map < String, KnowledgeArticleVersion > knowledgearticleversiondata = new Map < String, KnowledgeArticleVersion > (); // To Store all the article data from KAV
        Map < String, List < Sobject >> knowledgearticlemap = new Map < String, List < Sobject >> (); //To Store the Article information from each Article type
        Set < String > articletype = new Set < String > (); // Collects all the Article Types from query results
        Map < string, Set < String >> articletypetoarticles = new Map < string, Set < String >> (); // Collects the articles of different article type
        List < Messaging.SingleEmailMessage > emailmessages = new List < Messaging.SingleEmailMessage > (); // Initializing the emailmessage

        for (Sobject obj: database.query(query)) { // this for loop will iterate over the query and all the distinct ArticleTypes and Articlenumbers are captured

            KnowledgeArticleVersion kav = (KnowledgeArticleVersion) obj;
            String s = kav.ArticleType;
            String s1 = '__kav';
            if (s.contains(s1)) { 
            articletype.add(s);
            system.debug('Article Types to query' + articletype);             
            }            
            String artnumber = kav.Articlenumber;
            articletype.add(s);
            knowledgearticleversiondata.put(artnumber, kav);

            if (articletypetoarticles.containsKey(s)) {
                Set < String > existingids = articletypetoarticles.get(s);
                existingids.add(artnumber);
                System.debug('existingids'+existingids);
                articletypetoarticles.put(s, existingids);
                System.debug('existingids'+articletypetoarticles);                
            } else {
                Set < String > articleIds = new Set < String > ();
                articleIds.add((String) kav.get('ArticleNumber'));
                articletypetoarticles.put(s, articleIds);
            }
        }

        List < String > temparticletypes = new List < String > ();
        Map < String, Knowledgearticleversion > validrecordsForUpdate = new Map < String, Knowledgearticleversion > (); // This map will capture the valid records which are expired
        temparticletypes.addAll(articletype);
        for (String s: temparticletypes) { // this block will execute only number of articletypes times
            Date dt = Date.today();
            Set < String > ids = articletypetoarticles.get(s);
            System.debug('the value of ids' + ids);
            string idstring = '(';
            for (String idv: ids) {
                idstring = idstring + ',' + '\'' + idv + '\'';
            }
            idstring = idstring.replace('(,', '(');
            idstring = idstring + ')';
            // the below query will get the ArticleNumbers and ArticleTypes dynamicaly and check the expiration date
            String query1 = 'Select Id,ArticleNumber,Expiration_Date__c, Data_Source_Link__c from ' + s + ' where ArticleNumber IN ' + idstring + ' and PublishStatus = \'Online\' and Language = \'en_US\' and Expiration_Date__c <=' + dt.year() + '-' + ((dt.month() < 10) ? '0' + dt.month() : dt.month() + '') + '-' + ((dt.day() < 10) ? '0' + dt.day() : dt.day() + '');

            knowledgearticlemap.put(s, (List < Sobject > ) database.query(query1));

        }
        Messaging.SingleEmailMessage mail;
        try { // To catch any exceptions, try block is created
            for (String s: knowledgearticlemap.keySet()) {
                for (Sobject sob: (List < Sobject > ) knowledgearticlemap.get(s)) {
                    String mailbody = '<html><body>Dear Employee,<br><br>Article:{title} has expired as of {expiration} <br><br>As a result, the article has been moved to the “drafts” folder.  You can either re-publish the article and create a new expiration date, or you will need to make any necessary updates to the article within the SharePoint site.  The article can be accessed directly using the URL below:<br>{Link} <br><br>Thanks for your support!';
                    KnowledgeArticleVersion kavrec = knowledgearticleversiondata.get((string) sob.get('ArticleNumber'));
                    String artid = kavrec.KnowledgeArticleId;
                    KbManagement.PublishingService.editOnlineArticle(artid, false); // All the articles that satisfy the results of the query1,are updated to Draft using this method
                    validrecordsForUpdate.put((string) sob.get('ArticleNumber'), kavrec);
                    mail = new Messaging.SingleEmailMessage();
                    mail.setUseSignature(false);
                    mail.setToAddresses(new String[] {
                        knowledgearticleversiondata.get((string) sob.get('ArticleNumber')).createdBy.email
                    });
                    mail.setSubject('Article: ' + knowledgearticleversiondata.get((string) sob.get('ArticleNumber')).Title + ' is expired');
                    String title = knowledgearticleversiondata.get((string) sob.get('ArticleNumber')).Title;
                    String datasourceurl = String.valueOf(sob.get('Data_Source_Link__c'));
                    String expirationdate = String.valueOf(sob.get('Expiration_Date__c'));
                    mailbody = mailbody.replace('{title}', title);
                    mailbody = mailbody.replace('{Link}', datasourceurl);
                    mailbody = mailbody.replace('{expiration}', expirationdate);
                    mail.setHtmlBody(mailbody);
                    emailmessages.add(mail);
                }
                List < Messaging.SendEmailResult > results = Messaging.sendEmail(emailmessages);
            }
        } catch (Exception ex) {
            // Whenever the batca class fails, the exceptions are captured in an object and from there email will be sent to the login user with the exception reason
            WCT_ExceptionUtility.logException('batchArticleExpNotify', null, ex.getMessage() + ' ::: ' + ex.getStackTraceString());

        }
                
    }
    global void finish(Database.BatchableContext BC) {

    }

}