@istest
public class admin_view_test {
    static testmethod void adminviewtest(){
        
          
        Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        id rtId =rtMapByName.get('TRT SAP Reporting').getRecordTypeId();//particular RecordId by  Name
        
        schema.DescribeSObjectResult Cas = Case.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames = Cas.getRecordTypeInfosByName();// getting the record Type Info
        Id caseRtId =rtMapByNames.get('Case Mail Consolidation').getRecordTypeId();//particular RecordId by  Name
        
         
 recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con1=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con1;
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con1.email), 'UTF-8');
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        
     //   TRT_RequestorView_ctrl trtReport1 = new TRT_RequestorView_ctrl();
        
       Admin_view_ctrl trtReport1= new Admin_view_ctrl();
         
        try{
            trtReport1.loggedInContact = con1;
            trtReport1.promptness=2;
            trtReport1.accuracy=2;
            trtReport1.helpfullness=2;
            trtReport1.helpfullness=2;
            trtReport1.commentbox='test comment';
        }catch(exception e){
        }
        
        case objCase = new case();
        objCase.Status='New';
        objCase.RecordTypeId=caseRtId;
        objCase.WCT_Category__c='TRT Reporting';  
        objCase.Origin = 'Web';
        objCase.Gen_Request_Type__c ='';
        objCase.Priority='3 - Medium';
        objCase.Description ='TRT test';  
        objCase.Gen_RecordType__c ='TRT SAP Reporting'; 
        objCase.Subject='A reporting request';
        objCase.contactId=con1.id;
        insert objCase;
        
       
        
        Case_form_Extn__c cfs = new Case_form_Extn__c();
        cfs.GEN_Case__c =objCase.id;
        cfs.RecordTypeId=rtId;
        cfs.Case_Form_App__c ='TRT Reporting';
        cfs.TRT_Request_Status__c='New';
        cfs.TRT_Requestor_Name__c=con1.id;
        cfs.TRT_Feedback__c=1;
        //cfs.TRT_Feedback_Comments__c='test';
        cfs.TRT_ReOpen_Comment__c='test';
        cfs.TRT_Business_need_for_the_data_request__c='test';
        insert cfs; 

        Attachment attach=new Attachment();    
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=objCase.id;
        insert attach;
        
        trtReport1.getReportingInfo();
        //trtReport1.getCaseFormWrapper();
        // trtReport1.getQuestionsInfo();
        apexPages.currentPage().getParameters().put('acctidvalue',cfs.id);
       
        //trtReport1.Hidecomment();
        // trtReport1.setBtnValue = cfs.id;
        trtReport1.treq = cfs;

        trtReport1.comment ='12334';
      //  trtReport1.sendRequest(); 
       
        trtReport1.setBtnValue = cfs.id;
        trtReport1.comment ='';
        //trtReport1.sendRequest();
        //TRT_RequestorView_ctrl.submit('',2.0,2.0,2.0,2.0,'test');
        Admin_View_ctrl.submit(cfs.id,2.0,2.0,2.0,2.0,'test');
       
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(cfs);
        TRT_AddAttachment trtattach =new TRT_AddAttachment(sc);
        ApexPages.CurrentPage().getParameters().put('id',cfs.id);
        trtattach.Cancel();
        trtattach.getmyfile();
        trtattach.cfsid = cfs.id;
        Blob bodyBlobs=Blob.valueOf('Unit Test Attachment Body');
        trtattach.myfile.name='test';
        trtattach.myfile.body=bodyBlobs;
        trtattach.Savedoc(); 
    }
    
        
    static testmethod void trtCaseUpdate(){
        
        Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        id rtId =rtMapByName.get('TRT SAP Reporting').getRecordTypeId();//particular RecordId by  Name
        
        schema.DescribeSObjectResult Cas = Case.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames = Cas.getRecordTypeInfosByName();// getting the record Type Info
        Id caseRtId =rtMapByNames.get('Case Mail Consolidation').getRecordTypeId();//particular RecordId by  Name
        
         // Setup test data
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
         
        User u = new User(Alias = 'delt', Email='testdeloitte@testorg.com', 
        EmailEncodingKey='UTF-8', LastName='deloitte', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='testdeloitte@testorg.com');
        insert u;
       
       
         System.runAs(u)
         {
         recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee' LIMIT 1];
        Contact con1=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con1.email='testdeloitte@testorg.com';
        insert con1;  
              case objCase = new case();
        objCase.Status='New';
        objCase.RecordTypeId=caseRtId;
        objCase.WCT_Category__c='TRT Reporting';  
        objCase.Origin = 'Web';
        objCase.Gen_Request_Type__c ='';
        objCase.Priority='3 - Medium';
        objCase.Description ='TRT test';  
        objCase.Gen_RecordType__c ='TRT SAP Reporting'; 
        objCase.Subject='A reporting request';
        objCase.contactId=con1.id;
        insert objCase;
        
        Case_form_Extn__c cfs = new Case_form_Extn__c();
        cfs.GEN_Case__c =objCase.id;
        cfs.RecordTypeId=rtId;
        cfs.Case_Form_App__c ='TRT Reporting';
        cfs.TRT_Request_Status__c='New';
        cfs.TRT_Requestor_Name__c=con1.id;
        cfs.TRT_Feedback__c=1;
        //cfs.TRT_Feedback_Comments__c='test';
        cfs.TRT_ReOpen_Comment__c='test';
        cfs.TRT_Business_need_for_the_data_request__c='test';
        insert cfs;
        
      TRT_Record_Type_Details__c test=new TRT_Record_Type_Details__c(name='SAP test',TRT_Parameter_Value__c='SAP',TRT_Main_Value__c='SAP',TRT_SubCategory__c='',TRT_Page_Name__c='thanku',Record_Type__c='TRT SAP Reporting');
        insert test;
             
           
         TRT_RequestorView_ctrl trtReport1 = new TRT_RequestorView_ctrl();
           trtReport1.csename=cfs.id;
           trtReport1.currentFeedBackId=cfs.id;  
           //  trtReport1.clonerequest();
           
        }
         
        
    }   

}