global class MISScheduler implements Schedulable {
    
    global void execute(SchedulableContext ctx) {
        
        //logic to run only on business hours
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault=true];
        system.debug('businesshours'+bh);
        Datetime targetTime = Datetime.now();
        Boolean isWithin= BusinessHours.isWithin(bh.id, targetTime);
        system.debug('business hours is'+isWithin);
        //calling batch class
        if(iswithin){
            MIS_Batchclass a=new MIS_Batchclass();
            database.executebatch(a);
        }
        else
        {
            //abort the job
          //  System.abortJob(ctx.getTriggerId());
            System.debug('job aborted');
        }
    }
}