/********************************************************************************************************************************
Apex class         : User_Creation_Scheduler
Description        : scheduler class for user creation
Type               :  Scheduler
Test Class         :  Test_UserCreation

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01                 Deloitte                   27/06/2015          100%                         <Req / Case #>                  Original Version
************************************************************************************************************************************/
global class User_Creation_Scheduler implements Schedulable{
    //querying the record type 'employee' of contact
    global Id contid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
    global void execute(SchedulableContext ctx) {
        //query that gets passed to the User_creation Batch
      string query='SELECT Id,Email,Name,Firstname,lastname,App_Assignment__c,WCT_Region__c,WCT_Person_Id__c,WCT_Personnel_Number__c,WCT_Personnel_Number1__c,User_Management_category__c,WCT_Employee_Status__c  from Contact where  WCT_Contact_Type__c=\'Employee\' and RecordTypeId= :contid and WCT_Type__c=\'Employee\' and WCT_Employee_Status__c=\'Active\' and User_Management_category__c=true';
      system.debug('hello'+Database.Query(query));
      database.executeBatch(new User_Creation(query));
  }
    
}