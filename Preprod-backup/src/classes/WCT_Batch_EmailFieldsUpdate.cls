global class WCT_Batch_EmailFieldsUpdate implements Database.Batchable<sObject>{

    public List<Contact> lstContacts = new List<Contact>();
    public List<DebugData__c> lstdbinfo = new List <DebugData__c>();
    
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        QueriesTable__c[] queriesTableList = [SELECT Id, ObjectName__c, QuertyStatus__c, Query__c,QuertyStatus1__c FROM QueriesTable__c where QuertyStatus__c = 'New' limit 1];
        lstdbinfo.add(  new DebugData__c(Dinfo__c = 'QueryLocator - Debug Info: queriesTableList : ' + queriesTableList));
        String strQuery = queriesTableList[0].Query__c; 
        System.debug('test query: ' + strQuery);   
        lstdbinfo.add(  new DebugData__c(Dinfo__c = 'QueryLocator - Debug Info: Query : ' + strQuery));
        queriesTableList[0].QuertyStatus__c='Processing';
        
        update queriesTableList;

        lstdbinfo.add(new DebugData__c(Dinfo__c = 'QueryLocator - Debug Info: QuertyStatus__c: = Processing '));
        
        upsert lstdbinfo;
        
        return Database.getQueryLocator(strQuery);
    }
    global void execute(Database.BatchableContext BC,  List<sObject> scope) {
        string ReplaceString;
        string searchFor;
        string xfielddata;
        QueriesTable__c[] queriesTableList = [SELECT Id, ObjectName__c, QuertyStatus__c, Query__c,QuertyStatus1__c FROM QueriesTable__c where QuertyStatus__c = 'Processing' limit 1];
        String strQuery = queriesTableList[0].Query__c;
        
        lstdbinfo.add(  new DebugData__c(Dinfo__c = 'execute method - Debug Info: queriesTableList : ' + queriesTableList));  
        lstdbinfo.add(  new DebugData__c(Dinfo__c = 'execute method - Debug Info: Query : ' + strQuery)); 
        if(queriesTableList!=null && queriesTableList.size()>0) {
            
            List<String> listOfFields = getColumnList(strQuery);
            List<sObject> lstObjectsToUpdate = new List<sObject>();
            ReplaceString = '@tst.deloitte.com';
            searchFor = '@deloitte.com';
            for(sObject sobj : scope) {
                 
                  for(string field : listOfFields){
                        field =  field.trim();
                        if (sobj.get(field) !='' && sobj.get(field) != null && field.tolowercase().trim() != 'id') {
                            xfielddata =  UpdateEmailToInvalid( String.valueOf(sobj.get(field)),String.valueOf(sobj.get('id')));
                            //lstdbinfo.add(new DebugData__c(Dinfo__c = 'execute Method Field: ' + field  + ' = '  + String.valueOf(sobj.get(field.trim())) + ' = ' + xfielddata  ));
                            sobj.put(field,xfielddata) ;
                        }
                  }
                  lstObjectsToUpdate.add(sobj);
            }   
            if(lstObjectsToUpdate.size() > 0 ) update lstObjectsToUpdate;
        }
        upsert lstdbinfo;
    } 
    
    public List<String> getColumnList(string strQuery) {
        List<String> listOfFields= new List<String>();
        //lstdbinfo.add(new DebugData__c(Dinfo__c = 'getColumnList Method- ColumnList 1: '+ strQuery));
        strQuery = strQuery.toLowerCase(); 
        string tempString = strQuery.substring(strQuery.indexOf('id'), strQuery.indexOf('from'));
        
        listOfFields=tempString.split(',');

        //lstdbinfo.add(new DebugData__c(Dinfo__c = 'getColumnList Method- ColumnList 2: '+ listOfFields));
        upsert lstdbinfo;
        return listOfFields;
    }
    public string UpdateEmailToInvalid(string xContactEmail, id xContactId)
    {
        string strReturnString;
        string ReplaceString;
        string searchFor;
        ReplaceString = '@tst.deloitte.com';
        searchFor = '@deloitte.com';
        if  (xContactEmail.contains('@deloitte.com'))
            {
                strReturnString = xContactEmail.replace(searchFor, ReplaceString);
            }
         else if(xContactEmail != null && xContactEmail.indexOf('@tst.deloitte.com') == -1 )
            {   
                strReturnString = xContactId + '@InvalidEmail.com';
            }   

        return  strReturnString;
    }
    global void finish(Database.BatchableContext BC){    
        system.debug('Contact Records Processed : '  + lstContacts.size()); 
        QueriesTable__c[] queriesTableList = [SELECT Id, ObjectName__c, QuertyStatus__c, Query__c,QuertyStatus1__c FROM QueriesTable__c where QuertyStatus__c = 'Processing' limit 1];
        String strQuery = queriesTableList[0].Query__c;
        lstdbinfo.add(  new DebugData__c(Dinfo__c = 'Finish method - Debug Info: queriesTableList : ' + queriesTableList));  
        lstdbinfo.add(  new DebugData__c(Dinfo__c = 'Finish method - Debug Info: Query : ' + strQuery)); 
        if(QueriesTableList!=null && QueriesTableList.size()>0) {
            queriesTableList[0].QuertyStatus__c = 'Done';
            update QueriesTableList;
            system.debug('QuertyStatus__c=\'Done\'');
        }
        system.debug('QuertyStatus__c=\'Done\'');
        lstdbinfo.add(new DebugData__c(Dinfo__c = 'Finish Method- QuertyStatus__c : \'Done\''));
        upsert lstdbinfo;
        
        QueriesTable__c[] QueriesTableList1 = [SELECT Id, ObjectName__c, QuertyStatus__c, Query__c,QuertyStatus1__c FROM QueriesTable__c where QuertyStatus__c = 'New' limit 1];
        if(QueriesTableList1.size()>0)
        {
            WCT_Batch_EmailFieldsUpdate  batch = new WCT_Batch_EmailFieldsUpdate();
            ID batchprocessid = Database.executeBatch(batch);
        }

    }
    
}