@isTest
private class massUploadOfferNotesTest1 {

    @isTest
 public   Static void m1(){
    
    Test.StartTest() ;
        Blob contentFile;
        string strvar ='sample';
        contentFile = Blob.valueof(strvar);
        
        Contact convar = WCT_UtilTestDataCreation.createContact();
        insert convar;
        
        WCT_Offer__c offervar = WCT_UtilTestDataCreation.createOfferRec(convar.id);
        offervar.User_Group_Editable__c = 'United States';
        insert offervar;
        
        string namevar = offervar.Name;
        
        WCT_Offer_Note__c OfferNotevar  =WCT_UtilTestDataCreation.CreateOfferNote(offervar.id);
        
        list<WCT_Offer_Note__c> lOfferNotevar = new list<WCT_Offer_Note__c>();
        
            lOfferNotevar.add(OfferNotevar);
        database.insert(lOfferNotevar,false);
        string csvcontent=namevar+'Date sent to USI, Update (enter an X), Recruiter Notes for Updates, RMS Tracking Process,RMS Candidate ID,Office Recruiter,Coordinator,Last Name,First Name,Address 1,Address 2,City,State,Zip Code,Home Phone Number,Cellular Number,Permanent Email,Gender,Race,School,Degree,Major,GPA Overall,SAT,ACT,GRE,GMAT,Source Type,Source,School / Office Requisition #,Alternative Email Address,FSS,RMS Step,RMS Status,U.S. Meets Basic Qualifications Date (No date for conference candidates), Interview 1 to be Scheduled Date, Interview 1 Conducted Date, Status after Interview #1, Interview 2 Conducted Date, Status after Interview #2, Send RMS Rejection Notification, Application Initiated Date, Application Status, Application Completion Date, Travel Required, Type of Event,Last Day of Travel (6 months from trigger date), LPX Code, Host Recruiter: Should receive copy of travel confirmation, Notes, Hiring Location, Tentative Start Date (RMS),Start Date Year Offer Letter),Length of Internship,Hired by,RMS - Salary, Offer - Sign-On Bonus, Offer - Relocation Amount, Offer - Decision Deadline Date,Offer- Other Bonus (Incentive), Next Gen Amount,Transition Assistance Amount,Offer Letter Template, Other Offers (Please add any specific comments for OL) This could include: Specific Position Title etc, Hiring Partner, Title, Entity, Recruiter Phone No' + '\r\n' + '3/2/2015,,,Offer Draft,123456,abc.test@deloitte.com,abc.test@deloitte.com,Podduturi,Neeleshh,new street,kk nagar,chennai,tamil nadu,653875,,,abc@gmail.com,,,,,,8.1,,400,80,90,,,test_req_001,,Audit,Offer,Offer - To be Extended,5/5/2015,5/5/2015,5/5/2015,,5/5/2015,,,5/5/2015,,5/8/2015,TRUE,,5/10/2015,,,,Chicago,9/1/2015,Summer/Fall 2015,""14-16"",Kelly Sunderbuch,27.41,20000,1000,8/1/2015,5000,6300,Intern,test,Test Kelly Sunderbuch,Software Engineer,Deloitte Consulting LLP,9874561230';
       
           PageReference pageRef = Page.Offer_Note_Mass_Update;
           Test.setCurrentPage(pageRef);
           
           massUploadOfferNotes offnotevar = new massUploadOfferNotes();
           offnotevar.contentFile = Blob.valueof(csvContent);
           offnotevar.b = true;
           offnotevar.save();
           offnotevar.pg();
           List<Apexpages.Message> msgs = ApexPages.getMessages(); 
          
       Test.StopTest() ;
       } 
    @isTest    
  public   Static void m3(){
    
    Test.StartTest() ;
        Blob contentFile1;
        string strvar ='sample';
        contentFile1 = Blob.valueof(strvar);
        
       
      
           PageReference pageRef = Page.Offer_Note_Mass_Update;
           Test.setCurrentPage(pageRef);
           
           massUploadOfferNotes offnotevar = new massUploadOfferNotes();
           //offnotevar.contentFile = contentFile1;
           
           offnotevar.save();
           offnotevar.pg();
       Test.StopTest() ;
       } 
 }