global class MISCreateCase {
webservice static Void create(string recordid){
        
        list<WCT_List_Of_Names__c> CFErecord=[SELECT Id,MIS_Report_Start_Date__c,MIS_Assigned_Analyst_Report__c,MIS_Report_Lead_Email__c,MIS_Team_Name__c,MIS_Quality_Check_Analyst_Email__c,MIS_Frequency__c,MIS_DL_Email_ID__c,MIS_Report_Number__c,MIS_Report_End_Date__c,MIS_Day_Of_Report__c FROM WCT_List_Of_Names__c WHERE RecordType.Name='MIS_Report_Number'
                       and MIS_Report_End_Date__c!=null and MIS_Report_Start_Date__c!=null and 
                       Status__c='Active'  and id=:recordid]; 
      
       
            list<string> emails1=new list<string>();
            Map<string,ID> emailtouser1 = new Map<string,Id>();
            For(WCT_List_Of_Names__c l:CFErecord){
                string analystemail1=(string)l.get('MIS_Assigned_Analyst_Report__c');
                emails1.add(analystemail1);
            }
            List<user> userid1=[select id,email from User where email IN :emails1];
            For(User u:userid1){
                emailtouser1.put(u.email,u.id);
            }
            
            
            list<string> emails2=new list<string>();
            Map<string,ID> emailtouser2 = new Map<string,Id>();
            For(WCT_List_Of_Names__c lst:CFErecord){
                string analystemail2=(string)lst.get('MIS_Report_Lead_Email__c');
                emails2.add(analystemail2);
            }
            List<user> userid2=[select id,email from User where email IN :emails2];
            For(User u:userid2){
                emailtouser2.put(u.email,u.id);
            }
            
            
            //inserting case
            List<Case> Caselist=new List<Case>();
            
            if(CFErecord.size()>0){
                For(WCT_List_Of_Names__c nam : CFErecord){
                    
                    if(nam.MIS_Assigned_Analyst_Report__c!=null){
                        
                        Case objCase= new Case();
                        
                        Schema.DescribeSObjectResult Cas = Case.SObjectType.getDescribe();// getting Sobject Type
                        Map<String,Schema.RecordTypeInfo> rtMapByNames = Cas.getRecordTypeInfosByName();// getting the record Type Info
                        Id caseRtId =rtMapByNames.get('Case Mail Consolidation').getRecordTypeId();//particular RecordId by  Name
                        
                        
                        //inserting case
                        
                        objCase.Status='New';
                        objCase.RecordTypeId=caseRtId;
                        objCase.WCT_Category__c='MIS Scheduled Report';  
                        objCase.Origin = 'Web';
                        objCase.Gen_Request_Type__c ='';
                        objCase.Priority='3 - Medium';
                        objCase.OwnerId=emailtouser1.get(nam.MIS_Assigned_Analyst_Report__c);
                        objCase.MIS_ReportsList__c=nam.id;
                        objCase.MIS_DL_Email_id__c=nam.MIS_DL_Email_ID__c;
                        objCase.MIS_Report_Lead__c=nam.MIS_Report_Lead_Email__c;
                        objCase.MIS_Quality_Check_Analyst_Email__c=nam.MIS_Quality_Check_Analyst_Email__c;
                        
                        Caselist.add(objCase);
                    } 
                }
                if (!Caselist.isEmpty()){
                    try{
                        insert Caselist;
                    }
                    catch(Exception e){
                        
                        system.debug('-------exception---------'+e.getMessage()+'----at---Line #----'+e.getLineNumber());
                        
                        
                    }
                }
                system.debug('case record id is'+caselist);
                list<string> emails=new list<string>();
                Map<string,ID> emailtouser = new Map<string,Id>();
                For(case cl:Caselist){
                    string analystemail=(string)cl.get('MIS_Quality_Check_Analyst_Email__c');
                    emails.add(analystemail);
                }
                List<user> userid=[select id,email from User where email IN :emails];
                For(User u:userid){
                    emailtouser.put(u.email,u.id);
                }
                
                List<Case_form_Extn__c> CFElist=new List<Case_form_Extn__c>();
                
                
                
                For(Case  clist : Caselist ){
                    
                    Case_form_Extn__c MISreport=new Case_form_Extn__c();
                    
                    Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
                    Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
                    Id rtId =rtMapByName.get('MIS Scheduled Report').getRecordTypeId();//particular RecordId by  Name -->
                    
                    
                    //inserting case form extension
                    MISreport.RecordTypeId=rtId;
                    MISreport.GEN_Case__c=clist.id;
                    MISreport.MIS_Status_of_Report__c='NEW';
                    MISreport.MIS_DL_Email_id__c=clist.MIS_DL_Email_ID__c;
                    MISreport.MIS_ReportLead__c=emailtouser2.get(clist.MIS_Report_Lead__c);
                    MISreport.MIS_Quality_Check_Analyst__c=emailtouser.get(clist.MIS_Quality_Check_Analyst_Email__c); 
                    CFElist.add(MISreport);
                }
                if (!CFElist.isEmpty()){
                    try{
                        insert CFElist;  
                    }
                    catch(Exception e){
                        
                        system.debug('-------exception---------'+e.getMessage()+'----at---Line #----'+e.getLineNumber());
                        
                        
                    }
                }
                
                system.debug('cfe record id is'+CFElist);
                
                list<string> dl=new list<string>();
                Map<string,ID> emailtoid = new Map<string,Id>();
                //parsing strings
                for(Case_form_Extn__c cf:CFElist){
                    string dlvalue=(string) cf.get('MIS_DL_Email_ID__c'); 
                    list<string> strs =dlvalue.split(',');
                    for(string sr:strs){
                        dl.add(sr);   
                    }
                    
                }
                
                system.debug(''+dl);
                
                //mapping emailid to contact ids                  
                list<contact> newemail =[SELECT Id,email FROM contact where email IN :dl and recordtype.name='Employee' and WCT_Employee_Status__c='active'];
                
                for(contact c:newemail){
                    emailtoid.put(c.email,c.id);
                }
                system.debug('10'+emailtoid);
                List<Case_form_Extn__c> CFEprt=new List<Case_form_Extn__c>();
                
                for(Case_form_Extn__c cf:CFElist){
                    string dlvalue=(string) cf.get('MIS_DL_Email_ID__c'); 
                    list<string> strs =dlvalue.split(',');
                    
                    
                    for(string st:strs){
                        
                        Case_form_Extn__c MISfd=new Case_form_Extn__c();
                        
                        Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
                        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
                        Id rtId =rtMapByName.get('MIS Feedback').getRecordTypeId();//particular RecordId by  Name -->
                        
                        //inserting case form extension child records
                        MISfd.MIS_Report_Detail_Object__c=cf.id;
                        MISfd.RecordTypeId=rtId;
                        MISfd.GEN_Case__c=cf.GEN_Case__c;
                        MISfd.MIS_ReportLead__c=cf.MIS_ReportLead__c;
                        MISfd.TRT_Requestor_Name__c=emailtoid.get(st);
                        MISfd.MIS_DL_Email_id__c=st;
                        
                        CFEprt.add(MISfd);
                    }
                }
                if (!CFEprt.isEmpty()){
                    try{
                        insert CFEprt; 
                        
                        
                    }
                    catch(Exception e){
                        
                        system.debug('-------exception---------'+e.getMessage()+'----at---Line #----'+e.getLineNumber());
                        
                    } 
                    
                }
            } 
        
        
    }

}