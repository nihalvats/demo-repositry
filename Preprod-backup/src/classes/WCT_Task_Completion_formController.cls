/********************************************************************************************************************************
Apex class         : <WCT_Task_Completion_formController>
Description        : <Controller which allows to Update Task and Mobility>
Type               :  Controller
Test Class         : <WCT_Task_Completion_formController_test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 24/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/

public  class WCT_Task_Completion_formController{
    public task taskRecord{get;set;}
    public String taskSubject {get;set;}
    public String taskid{get;set;}
    public boolean display{get;set;}
    
    // DEFINING A CONSTRUCTOR 
    public WCT_Task_Completion_formController()
    {
        taskid = ApexPages.currentPage().getParameters().get('taskid');
        display=true;
        taskSubject = '';
    }

    /********************************************************************************************
    *Method Name         : <updateTaskFlags()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <UpdateTaskFlags() Used to Update Mobility and Task>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/    
    
    public PageReference updateTaskFlags()
    {
       try {
        if(taskid==''|| taskid==null){
            display=false;
           return null;
        }
     
        taskRecord=[SELECT id, Subject,Ownerid, Status,WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c FROM Task WHERE Id =: taskid];
        
        if(taskRecord != null){
            //Get Task Subject
            taskSubject = taskRecord.Subject;
            //Set Task Status
            if(taskRecord.WCT_Auto_Close__c == true){
                taskRecord.status='Completed';
            }
            
            if(taskRecord.WCT_Auto_Close__c == false){
            taskRecord.status='Not Started';
            }
            update taskRecord;
            }else{
            return null;
           }
        }
        
         catch (Exception e) {
           
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Task_Completion_formController', 'Task Completion Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_TSKCMP_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
            
        }
        return null;
    }
}