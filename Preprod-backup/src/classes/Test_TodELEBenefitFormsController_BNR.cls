/*=========================================Test Class=================================================

***************************************************************************************************************************************** 
 * Class Name   : Test_TodELEBenefitFormsController
 * Description  : test Class for TodELEBenefitFormsController   (SeeAllData = false)
 
 *****************************************************************************************************************************************/
 
 @istest
public class Test_TodELEBenefitFormsController_BNR{

    public static date std = system.today().adddays(-10);
    public static date enddt = system.today().adddays(10);
    public static date delend = system.today().adddays(-18);
    public static Date hiredt = system.today().adddays(180);
  static testmethod void EleBenifits() {
    
    String listRecrd = Schema.SObjectType.WCT_List_Of_Names__c.getRecordTypeInfosByName().get('ELEB Category Instruction').getRecordTypeId();  
      recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
     Account acc = new Account();
     acc.Name = 'Test';
     acc.Region__c = 'US';
     acc.Function__c = 'AERS';
     insert acc;
     
     
      Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
      con.WCT_Employee_Group__c = 'Active';
      con.Accountid = acc.id;
      con.WCT_PS_Group__c = 'I391';
      con.Email = 'svalluru@deloitte.com';
      con.WCT_Region__c = 'US - National Office';
      con.WCT_Contact_Type__c = 'Employee';
      insert con;
      
      recordtype crt=[select id from recordtype where DeveloperName  = 'Case_Mail_Consolidation' limit 1]; 
      Case cas =  WCT_UtilTestDataCreation.createCase(con.id);
      cas.RecordTypeid = crt.id;
      cas.Status = 'New';
      cas.Subject = 'ELE NEW CASE CREATED';
      cas.WCT_Category__c=  'ELE: Benefits India';  
      cas.Origin = 'Direct';
      cas.WCT_Support_Area__c = 'INDIA';
      insert cas;
      
      recordtype cfrmrt=[select id from recordtype where DeveloperName = 'ELE_Leaves']; 
      Case_form_Extn__c casfrm = new Case_form_Extn__c();
      casfrm.GEN_Case__c = cas.id;
      casfrm.RecordTypeid = cfrmrt.id;
      casfrm.ELE_ToD_Sub_Category2__c = 'Paternity Leave';
      casfrm.ELE_Expected_Date_of_Delivery__c = enddt;
      casfrm.ELE_EMP_Prefered_Contact_channel__c = 'Email';
      casfrm.ELE_Leave_Start_Date__c = std;
      casfrm.ELE_Leave_End_Date__c = enddt ;
      casfrm.ELE_Expected_Date_of_Delivery__c = enddt;
      casfrm.ELE_Subject__c = 'Test';
      casfrm.ELE_Description__c = 'Test Des';
      insert casfrm;
      
  // recordtype lstnmrec=[select id from recordtype where DeveloperName = 'ELEB_Category_Instruction'];
     
      WCT_List_Of_Names__c olistofnames1 = new WCT_List_Of_Names__c();
       olistofnames1.WCT_Type__c = 'Paternity Leave';
       olistofnames1.ToD_Case_Category_Instructions__c = 'ELE Paternity Leave';
       olistofnames1.Name = 'Leaves';
      // olistofnames1.RecordTypeId = lstnmrec.id;
       insert olistofnames1; 
     system.debug('LSTNMES*****'+olistofnames1);
      
       Test.startTest();
     
     PageReference pageRef = Page.TodELEBenefitForms;
    
   //  pageRef.getparameters().put('Param1','Paternity Leave');
     Test.setCurrentPageReference(pageRef);

       
      TodELEBenefitFormsController testcon = new TodELEBenefitFormsController(); 
    // testcon.TodELEBenefitFormsController_BNR();
    testcon.ProfessionalJoinDate = system.today().adddays(5);
      testcon.saveRequest();
      testcon.requesttypedef();
      testcon.getplaces();
      testcon.setrequesttype();
      testcon.getCategoryList();
      testcon.salaryadverror();
      testcon.carleaseerror();
      testcon.GenderValdn_Mat_Pat_Leaves();
      testcon.LoggedInProfBaseDataVal();
      testcon.LoggedContactJoinDate();
       
        Test.stopTest();
   
       
    }
    
    
     static testmethod void ElePaternity() {
     
      Profile p = [SELECT Id FROM Profile WHERE Name='20_Employee']; 
      User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','test@deloitte.com');
      insert platuser;
               
      system.runas(platuser){
   
      String listRecrd = Schema.SObjectType.WCT_List_Of_Names__c.getRecordTypeInfosByName().get('ELEB Category Instruction').getRecordTypeId();  
      recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
      Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
      con.WCT_Employee_Group__c = 'Active';
      con.Email = 'test@deloitte.com';
      con.WCT_Internship_Start_Date__c = hiredt ;
      con.WCT_Most_Recent_Rehire__c = system.today().adddays(10);
      con.WCT_Original_Hire_Date__c = system.today().adddays(83);
      con.WCT_Gender__c = 'Male';
      CON.WCT_PS_Group__c = 'I370';
      con.WCT_Function__c = 'CONSULTING';
      con.WCT_Region__c = 'US - National Office';
      con.WCT_Contact_Type__c = 'Employee';
      insert con;
      
      recordtype crt=[select id from recordtype where DeveloperName  = 'Case_Mail_Consolidation' limit 1]; 
      Case cas =  WCT_UtilTestDataCreation.createCase(con.id);
      cas.RecordTypeid = crt.id;
      cas.Status = 'New';
      cas.Subject = 'ELE NEW CASE CREATED';
      cas.WCT_Category__c=  'ELE: Benefits India';  
      cas.Origin = 'Direct';
      cas.WCT_Support_Area__c = 'INDIA';
      insert cas;
    
      recordtype cfrmrt=[select id from recordtype where DeveloperName = 'ELE_Leaves']; 
      Case_form_Extn__c casfrm = new Case_form_Extn__c();
      casfrm.GEN_Case__c = cas.id;
      casfrm.RecordTypeid = cfrmrt.id;
      casfrm.ELE_ToD_Sub_Category2__c = 'Paternity Leave';
      casfrm.ELE_Expected_Date_of_Delivery__c = enddt;
      casfrm.ELE_EMP_Prefered_Contact_channel__c = 'Email';
      casfrm.ELE_Leave_Start_Date__c = std;
      casfrm.ELE_Leave_End_Date__c = enddt ;
      casfrm.ELE_Expected_Date_of_Delivery__c = enddt;
      casfrm.ELE_Subject__c = 'Test';
      casfrm.ELE_Description__c = 'Test Des';
      casfrm.ELE_EMP_Joining_Date__c = system.today().adddays(100);
      insert casfrm;
      
   recordtype lstnmrec=[select id from recordtype where DeveloperName = 'ELEB_Category_Instruction'];
     
      list<WCT_List_Of_Names__c> lstnames = new list<WCT_List_Of_Names__c>();
     
       WCT_List_Of_Names__c olistofnames1 = new WCT_List_Of_Names__c();
       olistofnames1.WCT_Type__c = 'Paternity Leave';
       olistofnames1.ToD_Case_Category_Instructions__c = 'ELE Paternity Leave';
       olistofnames1.Name = 'Leaves';
       olistofnames1.RecordTypeId = lstnmrec.id;
       lstnames.add(olistofnames1);
       insert lstnames; 
       system.debug('LSTNMES*****'+lstnames);
       
     Recordtype rectype = [SELECT ID, Name FROM RecordType WHERE sObjectType = 'Case_form_Extn__c' And Name =: lstnames[0].Name];
     system.debug('REC*****'+rectype);
     
       
      system.assertequals(cfrmrt.id,rectype.id);
   
     Test.startTest();
     PageReference pageRef = Page.TodELEBenefitForms;
     pageRef.getparameters().put('Param1','Paternity Leave');
     Test.setCurrentPageReference(pageRef);

    
      TodELEBenefitFormsController testcon = new TodELEBenefitFormsController(); 
     
     // testcon.ProfessionalJoinDate =  system.today().adddays(100);
      Integer daysdiff=(testcon.ProfessionalJoinDate).daysBetween(casfrm.ELE_Leave_Start_Date__c);
    //  system.assertequals(-20,daysdiff);
      Date srtdt = system.today();
      Date enddate = system.today().adddays(10);
      testcon.saveRequest(); 
      testcon.requesttypedef();
     // testcon.saveRequest(); 
      testcon.getplaces();
      testcon.setrequesttype();
      testcon.getCategoryList();
      testcon.salaryadverror();
      testcon.carleaseerror();
      testcon.GenderValdn_Mat_Pat_Leaves();
      testcon.LoggedInProfBaseDataVal();
      testcon.LoggedContactJoinDate();
      testcon.convertMonthTextToNumber('Feb');
      testcon.toProperCaseText('test,text');
      testcon.FinancialYearValidation(std);
      testcon.findNoOfDays(srtdt,enddate);
      
        Test.stopTest();
   
       }
    }
    
  
    
}