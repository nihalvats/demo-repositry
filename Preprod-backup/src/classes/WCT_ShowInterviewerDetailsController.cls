/*********
Name     :WCT_ShowInterviewerDetailsController
Purpose  : This acts as a controller to ShowInterviewerDetails VF Page to bring the interviewer employee details on the Event Page
           of Interview.
Date     : 2/3/2014
Developer:Mrudula,Deloitte.
********/
public class WCT_ShowInterviewerDetailsController{

    Public Id SelectedEventId{get;set;}
      
    Public List<Contact> lstInterviewers_Contact=new List<Contact>();
    private final Event EventRecord;                                    

    public WCT_ShowInterviewerDetailsController(ApexPages.StandardController stdController) {
        this.EventRecord=(Event)stdController.getRecord();
        SelectedEventId =EventRecord.id;
       
    }   
    
    public  List<Contact> getlstInterviewers_Contact() {
        Set<String> lstInterviewers_UserEmails= new Set<String>();
        ID employeeRecordtypeID=WCT_Util.getRecordTypeIdByLabel('Contact','Employee');       
        for( EventRelation eventRelation:[SELECT EventId, RelationId,Relation.email FROM EventRelation where IsInvitee = true
                                            and Relation.type = 'User' and EventId=:SelectedEventId]){
        
            lstInterviewers_UserEmails.add(eventRelation.Relation.email);                                     
        }
        
        List<Contact> lstemployeecontacts=[select Name,Email,WCT_Job_Level_Text__c,WCT_Service_Area__c,WCT_Service_Line__c,WCT_Function__c,WCT_Facility_Name__c from contact where email in :lstInterviewers_UserEmails and recordtypeid=:employeeRecordtypeID limit :Limits.getLimitQueryRows()];
                      
        return lstemployeecontacts;

    }
    public PageReference Cancel() {
        PageReference pageRef = new PageReference('/' +SelectedEventId);
        pageRef.setRedirect(true);
        return pageRef;
    }
}