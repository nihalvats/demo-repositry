public with sharing class WCT_InterviwerDetailExt {

    String interviewJuncId ;
    WCT_Interview_Junction__c interviewJunction;
    public String nameFile{get;set;}
    public Blob contentFile{get;set;}
    public string IntvDate {get;set;}
    public string IntvStrTime {get;set;}
    public string IntvEndTime {get;set;}
    public string IntvSubmittedDate {get;set;}
    string IntviewId {get;set;}
    string candidatTrackerId {get;set;}
       public string label{get;set;}
    
    public boolean attachmentSection {get; set;}
    public boolean removeAttachment{get; set;}
    public WCT_InterviwerDetailExt(ApexPages.StandardController controller) {
        
        attachmentSection = false;
        removeAttachment = false;
        interviewJuncId = System.currentPagereference().getParameters().get('id');
        if(interviewJuncId!='' && interviewJuncId !=null){
            interviewJunction=[Select WCT_Interview__c, WCT_Candidate_Tracker__c,WCT_Candidate_Tracker__r.WCT_Contact__c,WCT_Last_Submitted_Date__c,WCT_IEF_Submission_Status__c,
                            WCT_Interview__r.WCT_Interview_Start_Time__c,WCT_Interview__r.WCT_Interview_End_Time__c,(Select Id from Interview_Forms__r) 
                            from WCT_Interview_Junction__c where id=:interviewJuncId ];
                            if(interviewJunction.WCT_IEF_Submission_Status__c=='Draft')
                            {
                            label='Edit IEF';
                            }
                            else
                            {
                             label='Complete IEF';
                            }
                            
            if(interviewJunction.WCT_Interview__r.WCT_Interview_Start_Time__c!=null)IntvDate = interviewJunction.WCT_Interview__r.WCT_Interview_Start_Time__c.format('MM/dd/yyyy');
            if(interviewJunction.WCT_Interview__r.WCT_Interview_Start_Time__c!=null)IntvStrTime = interviewJunction.WCT_Interview__r.WCT_Interview_Start_Time__c.format('hh:mm a');
            if(interviewJunction.WCT_Interview__r.WCT_Interview_End_Time__c!=null)IntvEndTime = interviewJunction.WCT_Interview__r.WCT_Interview_End_Time__c.format('hh:mm a');
            if(interviewJunction.WCT_Last_Submitted_Date__c != null)IntvSubmittedDate  = interviewJunction.WCT_Last_Submitted_Date__c.format('MM/dd/yyyy HH:mm:ss a');
            if(interviewJunction.WCT_Interview__c!=null)IntviewId = interviewJunction.WCT_Interview__c;
            if(interviewJunction.WCT_Candidate_Tracker__c!=null)candidatTrackerId=interviewJunction.WCT_Candidate_Tracker__c;
        }
        
    }
    public void setAttachmentSection(){
        if(attachmentSection ){
            attachmentSection = false;
        }else{
            attachmentSection = true;}

    }
    public List<Attachment> getAttachmentsCandidate() {
        list<String> attIds = new list<String>();

        set<Id> attParentId = new set<Id>();
        if(interviewJunction.WCT_Candidate_Tracker__r.WCT_Contact__c!=null)attParentId.add(interviewJunction.WCT_Candidate_Tracker__r.WCT_Contact__c);
        for(WCT_Candidate_Documents__c candidDocId : [SELECT Id FROM WCT_Candidate_Documents__c where WCT_Candidate__c  =: interviewJunction.WCT_Candidate_Tracker__r.WCT_Contact__c and WCT_Visible_to_Interviewer__c=true]){
            attParentId.add(candidDocId.Id);
        }
        if(!attParentId.IsEmpty()){
           return [select Id, ParentId, Parent.Name, Parent.Type, Name, Description, ContentType, BodyLength, CreatedBy.Name, LastModifiedDate from Attachment where parentId in :attParentId];
        }
        else{
          return null;
        }
    } 
    
    public List<Attachment> getAttachmentIEFs()
    {
        set<Id> candidatTrackerSet = new set<Id>();
        for(WCT_Interview_Junction__c IntJunctId : [select id, WCT_Candidate_Tracker__c  from WCT_Interview_Junction__c where WCT_Candidate_Tracker__c = : candidatTrackerId]){
            candidatTrackerSet.add(IntJunctId.Id);
        }
        if(!candidatTrackerSet.IsEmpty()){
            //return [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, Description, BodyLength, CreatedBy.Name, LastModifiedDate from Attachment where parentId =:interviewJuncId];
            return [select Id, ParentId, Parent.Name, Parent.Type, Name, ContentType, Description, BodyLength, CreatedBy.Name, LastModifiedDate from Attachment where parentId in :candidatTrackerSet];
        }
        else{
            return null;
        }
    
    }
    public Pagereference readFile()
    {
        if(contentFile!=null){
            Attachment att=new Attachment();
            att.ParentId=interviewJuncId ;
            att.Body=contentFile;
            att.Name=nameFile;
            
            WCT_Interview_Junction__c wctjun = new WCT_Interview_Junction__c(Id = interviewJuncId );
            wctjun.WCT_IEF_Submission_Status__c = WCT_UtilConstants.IEF_Form_Submitted;
            wctjun.WCT_IEF_Last_Submitted_Date__c = system.now();
            try{
                insert att;
                update wctjun;
            }
            catch(DMLException ex){
                WCT_ExceptionUtility.logException('InterviewDetailExt-AttachmentUpload,InterJunctionUpdate', null, ex.getMessage() + ' ::: ' + ex.getStackTraceString());
            }  
            contentFile = null;
            att=new Attachment();
            removeAttachment = false;
            setAttachmentSection();
            
            PageReference np = new PageReference('');
            list<WCT_Interview_Junction__c> IntvJuncRec = [select id from WCT_Interview_Junction__c 
                                                    where WCT_Interview__c=:IntviewId
                                                    and WCT_Interviewer__c=:UserInfo.getUserId()];
            if(!IntvJuncRec.IsEmpty()){
                if(IntvJuncRec.size()>1){
                    np = new PageReference('/apex/WCT_InterviewGroupDetail');           
                }
                else{
                    np = new PageReference('/apex/WCT_InterviewDetail');
                }
                }
                
                np.getParameters().put('id', interviewJuncId );
                np.setRedirect(true);
                return np;
        }      
        else{return null;}
    }
    public void cancelAttachment(){
        setAttachmentSection();

    }    
    public Id getInvtEventId(){
        try{
            return [select id from Event where whatId = : interviewJunction.WCT_Interview__c limit 1].Id;// it has to return 1 row only based on where clause.
        }
        catch(exception e){
            return null;
        }
    }
    public List<Note> getNotesCandidate() {
        list<String> attIds = new list<String>();

        set<Id> notesParentId = new set<Id>();
        notesParentId.add(interviewJunction.WCT_Candidate_Tracker__r.WCT_Contact__c);
        if(!notesParentId.IsEmpty()){
            return [SELECT Body,CreatedBy.Name,Id,LastModifiedDate,ParentId,Title FROM Note where parentId in :notesParentId];
        }
        else{
            return null;
        }
    } 
    public list<WCT_Interview_Junction__c> getCandidateLst(){
        return [select Id, WCT_Candidate_Tracker__r.WCT_Contact__r.Id, WCT_Candidate_Name__c, WCT_Candidate_Tracker__r.WCT_Contact__c, 
                WCT_Candidate_Tracker__r.WCT_Requisition__c, WCT_Candidate_Tracker__r.WCT_Requisition__r.Name,  
                WCT_IEF_Submission_Status__c,
                WCT_Interview__r.WCT_Clicktools_Form__r.WCT_ClicktoolsID__c, WCT_Interviewer__r.FirstName, WCT_Interviewer__r.LastName,
                WCT_Interviewer_Email__c, WCT_Interviewer_Level__c, WCT_Interview__r.WCT_Clicktools_Form__r.WCT_Record_Name__c 
                from WCT_Interview_Junction__c 
                where WCT_Interview__c =: interviewJunction.WCT_Interview__c
                and WCT_Interviewer__c = :UserInfo.getUserId()];
    }
}