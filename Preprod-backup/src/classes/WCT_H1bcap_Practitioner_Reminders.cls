global class  WCT_H1bcap_Practitioner_Reminders Implements Database.Batchable <sObject> {
    global Database.queryLocator start(Database.BatchableContext bc) {
         
           
     String SOQL = 'SELECT Id,WCT_H1BCAP_RM_Name__c,WCT_H1BCAP_USI_SLL_Name__r.id, WCT_H1BCAP_Practitioner_Name__r.id,WCT_H1BCAP_Resource_Manager_Email_ID__c,WCT_H1BCAP_Email_ID__c,WC_H1BCAP_USI_SLL_email_id__c,WCT_H1BCAP_Status__c, WCT_H1BCAP_Business_Days__c FROM WCT_H1BCAP__c WHERE WCT_H1BCAP_Status__c = Null  AND (WCT_H1BCAP_Business_Days__c = 3 OR WCT_H1BCAP_Business_Days__c = 4  OR WCT_H1BCAP_Business_Days__c = 5) ' ;
               
        return Database.getQueryLocator(SOQL);
       
    }

    global void execute(Database.BatchableContext bc, List<WCT_H1BCAP__c> H1bcap) {
    
   
        string orgmail =  Label.WCT_H1BCAP_Mailbox;
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList1 = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList4 = new List<Messaging.SingleEmailMessage>();
        OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
       Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'WCT_Reminder_1_notification_to_practitioner_for_Willing_to_travel' AND IsActive = true];
       Emailtemplate et2 = [select id, developername , IsActive from Emailtemplate where developername = 'WCT_Reminder_2_notification_to_practitioner_for_Willing_to_travel' AND IsActive = true];
       Emailtemplate et_RM = [select id, developername , IsActive from Emailtemplate where developername = 'Escalation_Mail_to_RM_abt_Prcatitioners' AND IsActive = true];
    
        
 
        for(WCT_H1BCAP__c w: H1bcap) {
      
        if(w.WCT_H1BCAP_Business_Days__c == 3 && null <> w.WCT_H1BCAP_Resource_Manager_Email_ID__c && ''<>w.WCT_H1BCAP_Resource_Manager_Email_ID__c && Null <> w.WCT_H1BCAP_Practitioner_Name__c) 
     {
           system.debug('size of the list' +H1bcap.size());
           system.debug('ID**********' +w.id);
           list<string>  currentRMEmail = new list<string>();
           currentRMEmail.add( w.WCT_H1BCAP_Resource_Manager_Email_ID__c); 
                    
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.SetTemplateid(et.id);
            mail.setSaveAsActivity(false);
            mail.setCcAddresses(currentRMEmail);  
            mail.setTargetObjectId(w.WCT_H1BCAP_Practitioner_Name__r.id);
            mail.setWhatid(w.id);
            mail.setOrgWideEmailAddressId(owe.id);
           
            system.debug('IDDDDDDDDDDD*******' +owe.id);
            system.debug('IDDDDDDDDDDD*******' +et.id);
            mailList.add(mail);   
           } 
   
   } 
   
        if(mailList.size() >0)
      {  
          try{
                Messaging.sendEmail(mailList);   
             }   
          catch(Exception npe) {
      System.debug('The following exception has occurred: ' + npe.getMessage());
             }    
      } 
             
        // Second Mail Method 
        
        for(WCT_H1BCAP__c w: H1bcap) {
          
          if( w.WCT_H1BCAP_Business_Days__c == 4 && null <> w.WCT_H1BCAP_Resource_Manager_Email_ID__c && ''<>w.WCT_H1BCAP_Resource_Manager_Email_ID__c && Null <> w.WCT_H1BCAP_Practitioner_Name__c) 
     {
           list<string>  currentRMEmail = new list<string>();
           currentRMEmail.add( w.WCT_H1BCAP_Resource_Manager_Email_ID__c);
                     
          Messaging.SingleEmailMessage mail4 = new Messaging.SingleEmailMessage();          
            mail4.SetTemplateid(et2.id);
            mail4.setSaveAsActivity(false);
            mail4.setTargetObjectId(w.WCT_H1BCAP_Practitioner_Name__r.id);
            mail4.setCcAddresses(currentRMEmail);
            mail4.setWhatid(w.id);
            mail4.setOrgWideEmailAddressId(owe.id);
           
            system.debug('IDDDDDDDDDDD*******' +owe.id);
            system.debug('IDDDDDDDDDDD*******' +et.id);
            mailList4.add(mail4);   
           } 
              
        } 
        
                if(mailList4.size() >0)
              {  
                  try    {
                            Messaging.sendEmail(mailList4);   
                          }   
                  catch(Exception npe) {
                                  System.debug('The following exception has occurred: ' + npe.getMessage());
                                        }      
              } 
                
        
   
       // Third Mail Method 
       // Single Mail should be sent to RM's if he has multiple 'n' records
       
       map <set<id>,WCT_H1BCAP__c> Map1 = new map<set<id>,WCT_H1BCAP__c>();
       set<id> setrmids = new set<id>();
        
         for(WCT_H1BCAP__c w: H1bcap)
         {
           if(w.WCT_H1BCAP_Business_Days__c == 5 && w.WCT_H1BCAP_RM_Name__c <> null )  
            {
             
             setrmids.add(w.WCT_H1BCAP_RM_Name__c);
             Map1.put(setrmids,w);
             }
         }
       
  
         for(WCT_H1BCAP__c w: Map1.values())
     {
         
         Messaging.SingleEmailMessage mailtorms = new Messaging.SingleEmailMessage();
         
          mailtorms.SetTemplateid(et_RM.id);
          mailtorms.setSaveAsActivity(false);
          mailtorms.setTargetObjectId(w.WCT_H1BCAP_RM_Name__c);
         // mailtorms.setCcAddresses(currentUserEmail );
          mailtorms.setWhatid(w.id);
          mailtorms.setOrgWideEmailAddressId(owe.id);
          mailList1.add(mailtorms);
     } 

       
    
    
            if(mailList1.size()>0)
            {
                 try{
                        Messaging.sendEmail(mailList1);  
                    }   
                  catch(Exception npe) {
                                  System.debug('The following exception has occurred: ' + npe.getMessage());
                                        }
            }
}
    global void finish(Database.BatchableContext bc) {
    }
  }