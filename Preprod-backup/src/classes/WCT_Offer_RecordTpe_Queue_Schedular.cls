/*********************************************************************************
Class Name      : WCT_Offer_RecordTpe_Queue_Schedular
Description     : This class will be Schedule WCT_Offer_RecordTpe_Queue_Schedular .
Created By      : Dhruv Bhalodi
Created Date    : 05-Aug-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Dhruv Bhalodi            05-Aug-14             Initial Version
*********************************************************************************/

global class WCT_Offer_RecordTpe_Queue_Schedular implements Schedulable {
    global void execute(SchedulableContext SC) {  
            Database.executebatch(new  WCT_Batch_Offer_RecordType_Queue_Update(),10);
        }
}