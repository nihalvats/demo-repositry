/*
Class name : WCT_HomePageComponentsController
Description : This class is controller for Home page components for Cases , Candidate Trackers and Interviews

Task number     Date            Modified By                         Description
------------    ---------       ---------------------               ----------------------
                11-Dec-13       Nilesh Adkar (Deloitte)                 Created
                                                            
*/
public class WCT_HomePageComponentsController{
 public String filterId {get;set;}
    public List<Case> lstCase {get;set;}
    public List<WCT_Interview__c> lstInterview {get;set;}
    public List<WCT_Candidate_Requisition__c> lstCandidateRequisition  {get;set;}
    set<id> interviewTrackerSet = new set<id>();

   public WCT_HomePageComponentsController(){
   
        string strObjType =ApexPages.CurrentPage().getParameters().get('objType');
        if(strObjType==null)
            return;
        system.debug('**strObjType**'+ strObjType );
        for(WCT_Interview_Junction__c IntvTrackerId : [select WCT_Interview__c from WCT_Interview_Junction__c where WCT_Requisition_Recruiter_Email__c =: userinfo.getUserEmail()]){
            interviewTrackerSet.add(IntvTrackerId.WCT_Interview__c);
        }
        if('WCT_Candidate_Requisition__c'==strObjType){
            lstCandidateRequisition = [select id,name,WCT_Contact__r.name,WCT_Requisition__r.Name,WCT_Recommended_Level__c,
                                    WCT_Candidate_Requisition_Status__c,WCT_Interview_Status__c,WCT_CR_School__c,WCT_CR_Other_Institution__c,WCT_CR_Current_Employer__c,WCT_CR_Other_Employer__c 
                                    from WCT_Candidate_Requisition__c 
                                    where WCT_Candidate_Requisition_Status__c='New' AND (ownerId = :userinfo.getUserId() or WCT_Requisition__r.OwnerId = :userinfo.getUserId()) order by createdDate desc limit 100];        
        }
        
        else if('cases'.equalsIgnoreCase(strObjType)){
            lstCase = [select id,subject,CreatedDate,Priority,CaseNumber from case where status='New' AND ownerId = :userinfo.getUserId() order by createdDate desc limit 100 ];
        }
                
        else if('WCT_Interview__c'.equalsIgnoreCase(strObjType)){
            lstInterview = [select name,
                            WCT_Interview_Round__c,WCT_Interview_Start_Time__c,
                            WCT_Interview_Type__c 
                            from WCT_Interview__c  
                            where WCT_Interview_Status__c = 'Open'  AND (ownerId = :userinfo.getUserId() or Id in :interviewTrackerSet)  order by WCT_Interview_Start_Time__c  asc limit 100 ];        
        }
    }


    public List<SelectOption> getCaseFilter() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Open', 'Open'));
        options.add(new SelectOption('Closed', 'Closed'));
        options.add(new SelectOption('All', 'All'));
        return options;
    }

    
    public List<SelectOption> getCandidateRequisitionFilter() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('New', 'New'));
        options.add(new SelectOption('In Progress', 'In Progress'));
        options.add(new SelectOption('Completed', 'Completed'));
        options.add(new SelectOption('Cancelled', 'Canceled'));
        options.add(new SelectOption('All', 'All'));
        return options;
    }

    
    public List<SelectOption> getInterviewsFilter() {
        List<SelectOption> options = new List<SelectOption>();
        //options.add(new SelectOption('New','New'));
        options.add(new SelectOption('In Progress','Open'));
        options.add(new SelectOption('Cancelled','Canceled'));
        options.add(new SelectOption('Complete','Complete'));
        options.add(new SelectOption('Today','Today'));
        options.add(new SelectOption('Tomorrow','Tomorrow'));
        options.add(new SelectOption('Next 7 Days','Next 7 Days'));
        options.add(new SelectOption('This Month','This Month'));
        options.add(new SelectOption('All','All'));
        options.add(new SelectOption('Invite Sent','Invite Sent'));
        //options.add(new SelectOption('Cancelled','Canceled'));
        return options;

    }

    public void getInterviewList()
    {
    //WCT_Candidate_Requisition_Owner__c,WCT_Interviewer__r.name,WCT_Contact__r.name,WCT_Is_it_my_Interview__c=true,WCT_Candidate_Requistion__r.name,  removed from Interview.In all list views its been removed

        if('Cancelled'.equalsIgnoreCase(filterId)==true){
            lstInterview = [select name,
                            WCT_Interview_Round__c,WCT_Interview_Start_Time__c,
                            WCT_Interview_Type__c from WCT_Interview__c  
                            where WCT_Interview_Status__c = 'Canceled' AND  (ownerId = :userinfo.getUserId() or Id in :interviewTrackerSet)  order by WCT_Interview_Start_Time__c  asc limit 100 ];
        }
        else if('In Progress'.equalsIgnoreCase(filterId)==true) {
            lstInterview = [select name,
                           WCT_Interview_Round__c,WCT_Interview_Start_Time__c,WCT_Interview_Type__c 
                            from WCT_Interview__c  
                            where WCT_Interview_Status__c = 'Open' AND  (ownerId = :userinfo.getUserId() or Id in :interviewTrackerSet)   order by WCT_Interview_Start_Time__c  asc limit 100 ];
        }
        else if('Invite Sent'.equalsIgnoreCase(filterId)==true) {
            lstInterview = [select name,
                           WCT_Interview_Round__c,WCT_Interview_Start_Time__c,WCT_Interview_Type__c 
                            from WCT_Interview__c  
                            where WCT_Interview_Status__c = 'Invite Sent' AND  (ownerId = :userinfo.getUserId() or Id in :interviewTrackerSet)   order by WCT_Interview_Start_Time__c  asc limit 100 ];
        }
        else if('Complete'.equalsIgnoreCase(filterId)==true) {
            lstInterview = [select name,
                            WCT_Interview_Round__c,WCT_Interview_Start_Time__c,WCT_Interview_Type__c 
                            from WCT_Interview__c  
                            where WCT_Interview_Status__c in('Completed - Pending Decision','Complete') AND  (ownerId = :userinfo.getUserId() or Id in :interviewTrackerSet)   order by WCT_Interview_Start_Time__c  asc limit 100 ];
        }
        else if('Today'.equalsIgnoreCase(filterId)==true) {
            lstInterview = [select name,
                            WCT_Interview_Round__c,WCT_Interview_Start_Time__c,WCT_Interview_Type__c 
                            from WCT_Interview__c  
                            where WCT_Interview_Start_Time__c = TODAY AND  (ownerId = :userinfo.getUserId() or Id in :interviewTrackerSet)  order by WCT_Interview_Start_Time__c  asc limit 100 ];
        }
        else if('Tomorrow'.equalsIgnoreCase(filterId)==true) {
            lstInterview = [select name,
                            WCT_Interview_Round__c,WCT_Interview_Start_Time__c,WCT_Interview_Type__c 
                            from WCT_Interview__c  
                            where  WCT_Interview_Start_Time__c = TOMORROW AND  (ownerId = :userinfo.getUserId() or Id in :interviewTrackerSet)  order by WCT_Interview_Start_Time__c  asc limit 100 ];
        }
        else if('Next 7 Days'.equalsIgnoreCase(filterId)==true) {
            lstInterview = [select name,
                            WCT_Interview_Round__c,WCT_Interview_Start_Time__c,WCT_Interview_Type__c 
                            from WCT_Interview__c  
                            where WCT_Interview_Start_Time__c <=NEXT_N_DAYS:7 and WCT_Interview_Start_Time__c >= TODAY  AND  (ownerId = :userinfo.getUserId() or Id in :interviewTrackerSet)  order by WCT_Interview_Start_Time__c  asc limit 100 ];
        }
        else if('This Month'.equalsIgnoreCase(filterId)==true) {
            lstInterview = [select name,WCT_Interview_Round__c,WCT_Interview_Start_Time__c,WCT_Interview_Type__c 
                            from WCT_Interview__c  
                            where WCT_Interview_Start_Time__c = THIS_MONTH AND  (ownerId = :userinfo.getUserId() or Id in :interviewTrackerSet)   order by WCT_Interview_Start_Time__c  asc limit 100 ];
        }
        else if('All'.equalsIgnoreCase(filterId)==true) {
            lstInterview = [select name,
                            WCT_Interview_Round__c,WCT_Interview_Start_Time__c,WCT_Interview_Type__c 
                            from WCT_Interview__c  
                            where  (ownerId = :userinfo.getUserId() or Id in :interviewTrackerSet)   order by WCT_Interview_Start_Time__c  asc limit 100 ];
        }
    }


    public void getCandidateRequisitions(){
        if('Cancelled'.equalsIgnoreCase(filterId)==true){
            lstCandidateRequisition = [select id,name,WCT_Contact__r.name,WCT_Requisition__r.Name,WCT_Recommended_Level__c,
                                    WCT_Candidate_Requisition_Status__c,WCT_Interview_Status__c,WCT_CR_School__c,WCT_CR_Other_Institution__c,WCT_CR_Current_Employer__c,WCT_CR_Other_Employer__c 
                                    from WCT_Candidate_Requisition__c where WCT_Candidate_Requisition_Status__c='Canceled' AND (ownerId = :userinfo.getUserId() or WCT_Requisition__r.OwnerId = :userinfo.getUserId()) order by createdDate desc limit 100];    
        }
        else if('New'.equalsIgnoreCase(filterId)==true) {
            lstCandidateRequisition = [select id,name,WCT_Contact__r.name,WCT_Requisition__r.Name,WCT_Recommended_Level__c,
                                    WCT_Candidate_Requisition_Status__c,WCT_Interview_Status__c,WCT_CR_School__c,WCT_CR_Other_Institution__c,WCT_CR_Current_Employer__c,WCT_CR_Other_Employer__c 
                                    from WCT_Candidate_Requisition__c where WCT_Candidate_Requisition_Status__c='New' AND (ownerId = :userinfo.getUserId() or WCT_Requisition__r.OwnerId = :userinfo.getUserId()) order by createdDate desc limit 100];        
        }
        if('In Progress'.equalsIgnoreCase(filterId)==true){
            lstCandidateRequisition = [select id,name,WCT_Contact__r.name,WCT_Requisition__r.Name,WCT_Recommended_Level__c,
                                    WCT_Candidate_Requisition_Status__c,WCT_Interview_Status__c,WCT_CR_School__c,WCT_CR_Other_Institution__c,WCT_CR_Current_Employer__c,WCT_CR_Other_Employer__c 
                                    from WCT_Candidate_Requisition__c where WCT_Candidate_Requisition_Status__c='In Progress' AND (ownerId = :userinfo.getUserId() or WCT_Requisition__r.OwnerId = :userinfo.getUserId()) order by createdDate desc limit 100];    
        }
        else if('Completed'.equalsIgnoreCase(filterId)==true) {
            lstCandidateRequisition = [select id,name,WCT_Contact__r.name,WCT_Requisition__r.Name,WCT_Recommended_Level__c,
                                    WCT_Candidate_Requisition_Status__c,WCT_Interview_Status__c,WCT_CR_School__c,WCT_CR_Other_Institution__c,WCT_CR_Current_Employer__c,WCT_CR_Other_Employer__c 
                                    from WCT_Candidate_Requisition__c where WCT_Candidate_Requisition_Status__c = 'Completed' AND (ownerId = :userinfo.getUserId() or WCT_Requisition__r.OwnerId = :userinfo.getUserId()) order by createdDate desc limit 100];        
        }
        else if('All'.equalsIgnoreCase(filterId)==true) {
            lstCandidateRequisition = [select id,name,WCT_Contact__r.name,WCT_Requisition__r.Name,WCT_Recommended_Level__c,
                                    WCT_Candidate_Requisition_Status__c,WCT_Interview_Status__c,WCT_CR_School__c,WCT_CR_Other_Institution__c,WCT_CR_Current_Employer__c,WCT_CR_Other_Employer__c 
                                    from WCT_Candidate_Requisition__c where (ownerId = :userinfo.getUserId() or WCT_Requisition__r.OwnerId = :userinfo.getUserId()) order by createdDate desc limit 100];        
        }        
    }
    
    
    
    public void getCases(){
        
        if('Open'.equalsIgnoreCase(filterId)==true) {
            lstCase = [select id,subject,CaseNumber,Priority,createdDate  from case where status='New' AND ownerId = :userinfo.getUserId()  order by createdDate desc limit 100 ];
        }
        else if('Closed'.equalsIgnoreCase(filterId)==true) {
            lstCase = [select id,subject,CaseNumber,Priority,createdDate  from case where status='Closed' AND ownerId = :userinfo.getUserId()  order by createdDate desc limit 100];
        }
        else if('All'.equalsIgnoreCase(filterId)==true) {
            lstCase = [select id,subject,CaseNumber,Priority,createdDate  from case where ownerId = :userinfo.getUserId()  order by createdDate desc limit 100];
        }
    }
    
}