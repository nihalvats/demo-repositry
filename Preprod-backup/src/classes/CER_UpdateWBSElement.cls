public class CER_UpdateWBSElement {
    
    public List<CER_Expense_Reimbursement__c> records{get; set;}
     public List<String> ids;
     public string WBSValue{get;set;}
    public boolean isError{get; set;}
    public string returnURL{get; set;}
    
     public CER_UpdateWBSElement()
     {
         string selectedid= ApexPages.currentPage().getParameters().get('selectedId');
         returnURL=ApexPages.currentPage().getParameters().get('returnURL');
         if(string.isEmpty(returnURL))
         {
             returnURL='\\';
         }
         ids= new List<String>();
         isError=false;
         if(selectedid!='' && selectedid !=null )
         {
         	ids=selectedid.split(',');
         }
         else
         {
             isError=true;
         }
         records = [Select id, Name, CER_Requester_Name__c, CER_Requester_Name__r.Name, CER_WBS_Element__c  From CER_Expense_Reimbursement__c where id in :ids];
         
         system.debug(''+selectedid);
     }
   
    public PageReference updateWBSElement()
    {
       if(records.size()>0)
        {
            Database.update(records);
        }
        return new PageReference(returnURL);
        
    }
    public PageReference cancelAction()
    {
        return new PageReference(returnURL);
    }
    
    

}