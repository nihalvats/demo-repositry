public class TOD_ContactLookup_Restricted{
    
 // public Account account {get;set;} // new account to create
  public List<contact> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
  public string label{get; set;}  
  
    
  public TOD_ContactLookup_Restricted() {
      results= new List<contact>();
     
    }   
   
  // performs the keyword search
  public PageReference search() {
    runSearch();

    return null;
  }
  
  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
        if(searchString != '' && searchString != null)
        {
            results = performSearch(searchString); 
        }              
  } 
  
  // run the search and return the records found. 
  private List<contact> performSearch(string searchString) {
 
       label='Search Results :';
    String soql = 'select Name, email from contact';
    if(searchString != '' && searchString != null)
     soql = soql +  ' where email like \'%' + searchString +'%\'';
     soql = soql +  ' and RecordType.Name = \'Employee\'';
    
      
    soql = soql + ' limit 25';
    System.debug(soql);
    return database.query(soql); 
 
  }
    

    

  
  // save the new account record
 /* public PageReference saveAccount() {
    insert account;
    // reset the account
    account = new Account();
    return null;
  }*/
  
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
    
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
 
}