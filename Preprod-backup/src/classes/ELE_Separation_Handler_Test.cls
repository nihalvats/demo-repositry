@isTest
private class ELE_Separation_Handler_Test {


    /**
        Declare Public class variables 
        */
        public static Account accountObj ;
        public static Contact contactObj; 
        public static Clearance_separation__c clrObject; 
        public static Contact stakeHoldercontactObj;
        public static Asset_Master__c assetMst;
        public static Attachment atchObj;
        public static Clearance_Master_Separation__c clrMstrObj;
      public static ELE_Separation__c eleObjs;
      public static List <ELE_Separation_Leads__c> leadObj;
      //public static User u;

    /** 
        Method Name  : createAccount
        Return Type  : Account
        Type         : private
        Description  : Create temp Account record for data mapping         
        */
        private static Account createAccount()
        {
            Account accObj = new Account(name = 'Test Account');
            insert accObj;
            return accObj;
        }

    /** 
        Method Name  : createContact
        Return Type  : Contact
        Type         : private
        Description  : Create temp Contact record for data mapping         
        */
        private static Contact createContact(Account acc,string email)
        {
            Contact conObj = new Contact(firstname = 'Test',WCT_Original_Hire_Date__c=system.today()-10,lastname = 'Contact',Accountid =acc.id,ELE_Access_Level__c='ITS',email=email,RecordTypeId = System.Label.Employee_Record_Type_ID);
            conObj.WCT_Function__c='Talent';
            conObj.WCT_Service_Area__c='Core Talent Services';
            conObj.WCT_Service_Line__c='Talent Reporting & Tech';
            
            insert conObj;
            return conObj;
        }
        
        
    /** 
        Method Name  : createClearanceMaster
        Return Type  : Clearance_Master_Separation__c
        Type         : private
        Description  : Create temp Clearance_Master_Separation__c record for data mapping         
        */
        private static Clearance_Master_Separation__c createClearanceMaster()
        {
            Clearance_Master_Separation__c clrMstr = new Clearance_Master_Separation__c();
            clrMstr.ELE_Clearance_Type__c= 'Voluntary';
            clrMstr.ELE_fss__c='Consulting';
            clrMstr.ELE_region__c ='USI';
            clrMstr.ELE_Clearance_Authority_Type__c='Sencondary Stakeholders';
            clrMstr.ELE_Record_Status_Active__c = true;
            insert clrMstr;
            return clrMstr;
        }

/** 
        Method Name  : createAssetMaster
        Return Type  : Asset_Master__c
        Type         : private
        Description  : Create temp Asset_Master__c record for data mapping         
        */
        private static Asset_Master__c createAssetMaster()
        {
            Asset_Master__c amstr = new Asset_Master__c();
            amstr.ELE_Asset_status_values__c= 'Yes/No/Submitted';
            amstr.Ele_Asset_Name__c='Keyboard';
            amstr.Ele_Team_Name__c ='ITS';
            insert amstr;
            return amstr;
        }

    /** 
        Method Name  : addAttachmentToParent
        Return Type  : Attachment
        Type         : private
        Description  :  Create temp Attachment record for data mapping   
        */
        private static Attachment addAttachmentToParent(Id parentId) {  
            Blob b = Blob.valueOf('Test Data');  
            Attachment attachment = new Attachment();  
            attachment.ParentId = parentId;  
            attachment.Name = 'Test Attachment for Parent';  
            attachment.Body = b;  
            insert attachment;  
            return attachment;
        }
    /** 
        Method Name  : createELE_Case
        Return Type  : Ele_separation__c
        Type         : private
        Description  :  Create temp ELE Separation record for data mapping   
        */
        private static ELE_Separation__c createELE_Case(Contact contact) {  
           ELE_Separation__c eleList = new ELE_Separation__c();
            eleList.ELE_Work_Location__c='worklocation';
            eleList.ELE_Reason_for_Resignation__c='Test';
            eleList.ELE_Consulted__c='Yes';
            eleList.ELE_Location__c='Hyderabad';
            eleList.ELE_Last_Date__c=System.Today();
            eleList.ELE_Date_of_resignation__c=System.Today();
            eleList.ELE_Alternate_Contact_Number__c='3209099999';
            eleList.ELE_Mobile_Number__c='9394232323';
            eleList.ELE_Address__c='Hyderabad';
            eleList.ELE_Email_id__c='tester@test1.com';
            eleList.ELE_Contact__c = contact.id;
            eleList.ELE_fss__c = 'Consulting';
            eleList.ELE_region__c='USI';
            eleList.ELE_case_Type__c = 'Voluntary';
            eleList.ELE_Onsite_Office_Location__c ='onsite'; 
            eleList.ELE_Future_Employer_Name__c ='Future Test EMployee';
            eleList.ELE_Future_Company_Start_Date__c = System.today();
            eleList.ELE_Future_company_designation__c ='SE';
            insert eleList;
            return eleList;
        }
        
        public static ELE_Separation_Leads__c createESL()
        {     
             ELE_Separation_Leads__c temp = new ELE_Separation_Leads__c();
             temp.Function__c='Talent';
             temp.Service_Area__c='Core Talent Services';
             temp.Service_Line__c='Talent Reporting & Tech';
             temp.RC_Code__c = 0;
             temp.Office_City__c='*';
             temp.Job_Level__c='*';
             temp.Leads_email__c = 'fazhussain@deloitte.com,apurandare@deloitte.com';
             insert temp;
             return temp;
    }
    
      
    /** 
        Method Name  : createClearanceRec
        Return Type  : Clearance_separation__c
        Type         : private
        Description  :  Create temp Clearance_separation__c record for data mapping   
        */
        private static Clearance_separation__c createClearanceRec(ELE_Separation__c eleObj,string authority,Boolean rem1,Boolean rem2,Boolean rem3,Date rem1time,Date rem2time,Date rem3time)
        {
            clearance_separation__c clrObj = new clearance_separation__c();
            clrObj.ELE_Stakeholder_Designation__c = 'ITS';
            clrObj.ELE_Status__c='Open';
            clrObj.ELE_Comments_For_Employee__c='test';
            clrObj.ELE_Employee_Comments__c='';
            clrObj.ELE_Employee_comment_updated__c='';
            clrObj.ELE_Separation__c = eleObj.id;
            clrObj.ELE_Clearance_Authority_Type__c= authority;
            clrObj.ELE_Reminder1_Time__c = rem1time; 
            clrObj.ELE_Notification_Reminder_1__c = rem1;
            clrObj.ELE_Reminder2_Time__c = rem2time;
            clrObj.ELE_Notification_Reminder_2__c = rem2;
            clrObj.ELE_Reminder3_Time__c = rem3time;
            clrObj.ELE_Notification_Reminder_3__c = rem3;
            clrObj.ELE_Additional_Fields__c = 'masandeep@deloitte.com';
            insert clrObj;
            return clrObj;
        }
    /** 
        Method Name  : createResignRecord_ELE
        Return Type  : ResignationFormController
        Type         : private
        Description  : Create instance for temp ELE_Separation record for data mapping         
        */

        private static ResignationFormController createResignRecord_ELE(String worklocation,String contact,String fss )
        {
            ResignationFormController rInstance = new ResignationFormController(); 
            rInstance.eleList.ELE_Work_Location__c=worklocation;
            rInstance.eleList.ELE_Reason_for_Resignation__c='Test';
            rInstance.eleList.ELE_Consulted__c='Yes';
            rInstance.eleList.ELE_Location__c='Hyderabad';
            rInstance.eleList.ELE_Last_Date__c=System.Today();
            rInstance.eleList.ELE_Date_of_resignation__c=System.Today();
            rInstance.eleList.ELE_Alternate_Contact_Number__c='3209099999';
            rInstance.eleList.ELE_Mobile_Number__c='9394232323';
            rInstance.eleList.ELE_Address__c='Hyderabad';
            rInstance.eleList.ELE_Email_id__c='tester@test1.com';
            rInstance.eleList.ELE_Contact__c = contact;
            rInstance.eleList.ELE_fss__c = fss;
            rInstance.eleList.ELE_region__c='USI';
            rInstance.eleList.ELE_case_Type__c = 'Voluntary';
            rInstance.eleList.ELE_Onsite_Office_Location__c ='onsite'; 
            rInstance.eleList.ELE_Future_Employer_Name__c ='Future Test EMployee';
            rInstance.eleList.ELE_Future_Company_Start_Date__c = System.today();
            rInstance.eleList.ELE_Future_company_designation__c ='SE';
            return rInstance;
        }   
    /** 
        Method Name  : init
        Description  : Init Test method   
        */
        /** Created individual Users with respective to logged in contact.
         private static void init(String email,String Pagename)
        {
            accountObj = ELE_Separation_Handler_Test.createAccount();
            contactObj = ELE_Separation_Handler_Test.createContact(accountObj,email);
                        
            ELE_Separation_Handler_Test.createESL();
            //String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(contactObj.email), 'UTF-8');
            Test.setCurrentPageReference(new PageReference(Pagename)); 
            //System.currentPageReference().getParameters().put('em', strEncryptEmail);

        }
		*/

    /** 
        Method Name  : createResignEle
        Description  : create resignation ele records  
        */
      /**  private static void createResignEle(String siteLoc,id contactId,String fss)
        {
            ResignationFormController rInstanceRec = ELE_Separation_Handler_Test.createResignRecord_ELE(siteLoc,contactId,fss); 
            rInstanceRec.saveResignRecord(); 
        }*/
    /** 
        Method Name  : createELE_Records
        Description  : Test method for creating Ele records for Enabling Areas    
        */
        @isTest public static void createELE_Records()
        {
            Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');   
            
            User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
            insert u;
            System.runAs(u)
                //Running the test case with user u
            {
            //init('jbahuguna@deloitte.com','Page.Resignationform');
            ResignationFormController rInstanceRec = ELE_Separation_Handler_Test.createResignRecord_ELE('India Offices',con.id,'Enabling Areas'); 
            ELE_Separation_Handler_Test.createESL();
            //rInstanceRec.doc = WCT_UtilTestDataCreation.createDocument();
            //rInstanceRec.uploadAttachment();        
            rInstanceRec.saveResignRecord();    
            rInstanceRec.pageError=true;
            rInstanceRec.pageErrorMessage='error message';
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
            ApexPages.addMessage(msg);
            //rInstanceRec.uploadAttachment();     
            List<contact> conList = new List<Contact>([select id,ELE_Open_ELE_case__c from contact where id =:con.id]);
            rInstanceRec.listContact = conList;
            rInstanceRec.redirect();
            }
        }
    
    /** 
	Method Name  : ResignationFormControllertest2
	Description  : Test method for code coverage of redirect method in ResignationFormController and if condition in saveResignRecord method.
	*/
    
    @isTest static void ResignationFormControllertest2(){
        
        Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');   
        
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;
        System.runAs(u)
            //Running the test case with user u
        {
            //Passing the worklocation as Onsite Assignment to cover the if condition for saveResignRecord method.
            ResignationFormController rInstanceRec = ELE_Separation_Handler_Test.createResignRecord_ELE('Onsite Assignment',con.id,'Enabling Areas'); 
            ELE_Separation_Handler_Test.createESL();
            rInstanceRec.saveResignRecord();
            
            rInstanceRec.pageError=true;
            rInstanceRec.pageErrorMessage='error message';
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
            ApexPages.addMessage(msg);
            
            List<contact> conList = new List<Contact>([select id,ELE_Open_ELE_case__c from contact where id =:con.id]);
            // Asigning the listContact as null to access the redirect method.
            rInstanceRec.listContact = null;
            rInstanceRec.redirect();
        }
    }
    
    /** 
	Method Name  : ResignationFormControllertest3
	Description  : Test method for code coverage of redirect method in ResignationFormController.
	*/
    
    @isTest static void ResignationFormControllertest3(){
        
        Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');   
        
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;
        System.runAs(u)
            //Running the test case with user u
        {
            //Passing the fss as global to cover the if condition for saveResignRecord method.
            ResignationFormController rInstanceRec = ELE_Separation_Handler_Test.createResignRecord_ELE('Onsite Assignment',con.id,'Global'); 
            ELE_Separation_Handler_Test.createESL();
            rInstanceRec.saveResignRecord();
        }
    }
    
    /** 
	Method Name  : ResignationFormControllertest3
	Description  : Test method for code coverage of redirect method in ResignationFormController.
	*/
    
    @isTest static void ResignationFormControllertest4(){
        
        Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');   
        
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;
        System.runAs(u)
            //Running the test case with user u
        {
            //Passing the fss as Consulting to cover the if condition for saveResignRecord method.
            ResignationFormController rInstanceRec = ELE_Separation_Handler_Test.createResignRecord_ELE('Onsite Assignment',con.id,'Consulting'); 
            ELE_Separation_Handler_Test.createESL();
            rInstanceRec.saveResignRecord();
            
        }
    }
    
    /** 
        Method Name  : createELE_Records
        Description  : Test method for creating Ele records for Global   
        */
        @isTest static void createELE_GLOBAL() {
            
            Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');             
            User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
            insert u;
            System.runAs(u)
                //Running the test case with user u
            {
            //init('jbahuguna@deloitte.com','Page.Resignationform');
            //createResignEle('Onsite Assignment',contactObj.id,'Global');
            } 
        }

    /** 
        Method Name  : createELE_Records
        Description  : Test method for creating Ele records for Consulting
        */
        @isTest static void createELE_Consulting() {
            Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');             
            User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
            insert u;
            System.runAs(u)
                //Running the test case with user u
            {
            //init('jbahuguna@deloitte.com','Page.Resignationform');
           // createResignEle('Onsite Assignment',contactObj.id,'Consulting'); 

            //ELE_Separation__c eleObj = [select id from ELE_Separation__c limit 1]; 
            eleObjs = ELE_Separation_Handler_Test.createELE_Case(con);
            clrObject = ELE_Separation_Handler_Test.createClearanceRec(eleObjs,'Primary Stakeholder',false,false,false,System.today(),System.today(),System.today());
            
            EmpDashBoardController empctrlr = new EmpDashBoardController();
            empctrlr.getWrapperList();
            EmpDashBoardController.Clearancewrapperclass Wrapvar = new EmpDashBoardController.Clearancewrapperclass(clrObject,con);
            empctrlr.UpdateDetails();
            //empctrlr.fetch();
            empctrlr.listAppForUpdt = null;
            empctrlr.UpdateDetails();
            empctrlr.revokeresign();
                
            ELE_Batch_Clerance_NotificationSend eleBatch = new ELE_Batch_Clerance_NotificationSend();
            Database.executeBatch(eleBatch);
            }
        }
    
    /**
        Method Name : EmpDashoardControllerTest2
        Description : code coverage for getWrapperList Method in the EmpDashBoardController
        */    
    
    @isTest static void EmpDashoardControllerTest2(){
        
        Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');             
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;
        System.runAs(u)
            //Running the test case with user u
        {
            eleObjs = ELE_Separation_Handler_Test.createELE_Case(con);
            clrObject = ELE_Separation_Handler_Test.createClearanceRec(eleObjs,'Primary Stakeholder',false,false,false,System.today(),System.today(),System.today());
            //Assigning the Values as per the getWrapperList Method in the EmpDashBoardController. 
            clrObject.ELE_Stakeholder_Designation__c = 'CRM';
            clrObject.ELE_Status__c = null;
            update clrObject;
            
            EmpDashBoardController empctrlr = new EmpDashBoardController();
            empctrlr.getWrapperList();
            EmpDashBoardController.Clearancewrapperclass Wrapvar = new EmpDashBoardController.Clearancewrapperclass(clrObject,con);
            empctrlr.UpdateDetails();
            
            ELE_Batch_Clerance_NotificationSend eleBatch = new ELE_Batch_Clerance_NotificationSend();
            Database.executeBatch(eleBatch);
        }
    }
    
     /**
        Method Name : EmpDashoardControllerTest2
        Description : catch code coverage in the updateDetails method in the EmpDashBoardController.
        */    
    
    @isTest static void EmpDashoardControllerTest3(){
        
        Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');             
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;
        System.runAs(u)
            //Running the test case with user u
        {
            eleObjs = ELE_Separation_Handler_Test.createELE_Case(con);
            clrObject = ELE_Separation_Handler_Test.createClearanceRec(eleObjs,'Primary Stakeholder',false,false,false,System.today(),System.today(),System.today());
            
            EmpDashBoardController empctrlr = new EmpDashBoardController();
            empctrlr.getWrapperList();
            EmpDashBoardController.Clearancewrapperclass Wrapvar = new EmpDashBoardController.Clearancewrapperclass(clrObject,con);
            empctrlr.UpdateDetails();
            // Assigning the value wraplist as null to access the catch part of the updateDetails Method.
            empctrlr.wrapList=null;
            empctrlr.UpdateDetails();
            
            ELE_Batch_Clerance_NotificationSend eleBatch = new ELE_Batch_Clerance_NotificationSend();
            Database.executeBatch(eleBatch);
        }
    }
    
    /**
        Method Name : createRemainderNotificationinBatch
        Description : code coverage for sending remainder notifications and employeeupdate controller scenarios
        */
        @isTest static void createRemainderNotificationinBatch() {
            Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');             
          	
            User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
            insert u;
            System.runAs(u)
                //Running the test case with user u
            {
            //init('jbahuguna@deloitte.com','Page.Resignationform');
            //createResignEle('Onsite Assignment',contactObj.id,'Consulting'); 

            eleObjs = ELE_Separation_Handler_Test.createELE_Case(con);
            clrMstrObj = ELE_Separation_Handler_Test.createClearanceMaster();
            ELE_CreateClearanceClass.ELE_CreateClearance(eleObjs.id,eleObjs.ELE_case_Type__c, eleObjs.ELE_fss__c,eleObjs.ELE_region__c,'Sencondary Stakeholders');
            assetMst = ELE_Separation_Handler_Test.createAssetMaster();
            clrObject = ELE_Separation_Handler_Test.createClearanceRec(eleObjs,System.Label.ELE_Secondary_Stakeholders_Label,true,false,false,System.today(),System.today(),System.today());
                
            List<Clearance_separation__c> clrLst = new List<clearance_separation__c>();
            clrLst.add(clrObject);
           
            ELE_Batch_Clerance_NotificationSend eleBatch = new ELE_Batch_Clerance_NotificationSend();
            Database.executeBatch(eleBatch);
            

            //stakeHoldercontactObj = ELE_Separation_Handler_Test.createContact(accountObj,'masandeep@deloitte.com');
            ELE_Separation_Handler_Test.createESL();
            Test.setCurrentPageReference(new PageReference('Page.ELE_employeeDataUpdatePage'));
            System.currentPageReference().getParameters().put('em', EncodingUtil.urlDecode(CryptoHelper.encrypt('masandeep@deloitte.com'), 'UTF-8'));
            System.currentPageReference().getParameters().put('cue', 'jbahuguna@deloitte.com');
            System.currentPageReference().getParameters().put('type', 'p');
            System.currentPageReference().getParameters().put('tm', 'ITS');
            System.currentPageReference().getParameters().put('id', clrObject.id);
            
            ELE_employeeDataUpdateController eleUpdate = new ELE_employeeDataUpdateController();
            eleUpdate.cancelDashBoard();
            eleUpdate.getList_cAssetToDisplay();
            eleUpdate.getsDashBoardView();
            eleUpdate.stakeholderType = 's';
            eleUpdate.getsDashBoardView();
            eleUpdate.cClToUpdate.ELE_Stakeholder_Designation__c = 'Treasury'; 
            eleUpdate.getsDashBoardView();
            
            // To return null in the getsDashBoardView method.    
            eleUpdate.stakeholderType = null;
            eleUpdate.getsDashBoardView();
                
            eleUpdate.AddAsset();
            eleUpdate.rendering = false;
            eleUpdate.sAssetToupdateID = 'Keyboard/Mouse';
            eleUpdate.sValue = 'Open';
            eleUpdate.status = 'Open';
            eleUpdate.listcAssetToUpdate[0].bSelectedAsset = true;
            eleUpdate.attNoticePay.body = Blob.valueOf('Test Data') ;
            eleUpdate.attNoticePay.name = 'notice_attachment' ;
            eleUpdate.attPartial.body = Blob.valueOf('Test Data') ;
            eleUpdate.attPartial.name = 'attPartial_attachment' ;
            eleUpdate.attRehireStatus.body = Blob.valueOf('Test Data') ;
            eleUpdate.attRehireStatus.name = 'attRehireStatus_attachment' ;
            eleUpdate.attSpecialLeav.body = Blob.valueOf('Test Data') ;
            eleUpdate.attSpecialLeav.name = 'attSpecialLeav_attachment' ;
            eleUpdate.attNotice.body = Blob.valueOf('Test Data') ;
            eleUpdate.attNotice.name = 'attNotice_attachment' ;
            eleUpdate.cClearanceToupdate[0].ELE_Notice_period_waived__c = 'Yes';
            eleUpdate.cClearanceToupdate[0].ELE_Special_Leaves__c = 'Yes';
            eleUpdate.cClearanceToupdate[0].ELE_Rehire_Status__c = 'No';
            eleUpdate.cClearanceToupdate[0].ELE_Notice_or_Severance_pay__c = 'Yes';
            eleUpdate.cClearanceToupdate[0].ELE_Notice_period_partial__c = 'Yes';
            eleUpdate.doc=WCT_UtilTestDataCreation.createDocument();
            eleUpdate.uploadAttachment();  
            eleUpdate.updateDashBoard();
            }  
        }

    /**
	Method Name : ELE_employeeDataUpdateControllerTest2
	Description : code coverage for ELE_Status__c in the validateattatchuploaded method of ELE_employeeDataUpdateController Controller.
	*/
    
    @isTest static void ELE_employeeDataUpdateControllerTest2() {
        Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');             
        
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;
        System.runAs(u)
            //Running the test case with user u
        {            
            eleObjs = ELE_Separation_Handler_Test.createELE_Case(con);
            clrMstrObj = ELE_Separation_Handler_Test.createClearanceMaster();
            ELE_CreateClearanceClass.ELE_CreateClearance(eleObjs.id,eleObjs.ELE_case_Type__c, eleObjs.ELE_fss__c,eleObjs.ELE_region__c,'Sencondary Stakeholders');
            assetMst = ELE_Separation_Handler_Test.createAssetMaster();
            clrObject = ELE_Separation_Handler_Test.createClearanceRec(eleObjs,System.Label.ELE_Secondary_Stakeholders_Label,true,false,false,System.today(),System.today(),System.today());
            
            List<Clearance_separation__c> clrLst = new List<clearance_separation__c>();
            clrLst.add(clrObject);
            
            ELE_Batch_Clerance_NotificationSend eleBatch = new ELE_Batch_Clerance_NotificationSend();
            Database.executeBatch(eleBatch);
            
            ELE_Separation_Handler_Test.createESL();
            Test.setCurrentPageReference(new PageReference('Page.ELE_employeeDataUpdatePage'));
            System.currentPageReference().getParameters().put('em', EncodingUtil.urlDecode(CryptoHelper.encrypt('masandeep@deloitte.com'), 'UTF-8'));
            System.currentPageReference().getParameters().put('cue', 'jbahuguna@deloitte.com');
            System.currentPageReference().getParameters().put('type', 'p');
            System.currentPageReference().getParameters().put('tm', 'ITS');
            System.currentPageReference().getParameters().put('id', clrObject.id);
            
            ELE_employeeDataUpdateController eleUpdate = new ELE_employeeDataUpdateController();
            eleUpdate.cancelDashBoard();
            eleUpdate.getList_cAssetToDisplay();
            eleUpdate.getsDashBoardView();
            
            eleUpdate.AddAsset();
            eleUpdate.rendering = false;
            eleUpdate.sAssetToupdateID = 'Keyboard/Mouse';
            eleUpdate.sValue = 'Open';
            eleUpdate.status = 'Open';
            eleUpdate.listcAssetToUpdate[0].bSelectedAsset = true;
            eleUpdate.attNoticePay.body = Blob.valueOf('Test Data') ;
            eleUpdate.attNoticePay.name = 'notice_attachment' ;
            eleUpdate.attPartial.body = Blob.valueOf('Test Data') ;
            eleUpdate.attPartial.name = 'attPartial_attachment' ;
            eleUpdate.attRehireStatus.body = Blob.valueOf('Test Data') ;
            eleUpdate.attRehireStatus.name = 'attRehireStatus_attachment' ;
            eleUpdate.attSpecialLeav.body = Blob.valueOf('Test Data') ;
            eleUpdate.attSpecialLeav.name = 'attSpecialLeav_attachment' ;
            eleUpdate.attNotice.body = Blob.valueOf('Test Data') ;
            eleUpdate.attNotice.name = 'attNotice_attachment' ;
            eleUpdate.cClearanceToupdate[0].ELE_Status__c = 'Closed';
            eleUpdate.cClearanceToupdate[0].ELE_Notice_period_waived__c = 'Yes';
            eleUpdate.cClearanceToupdate[0].ELE_Special_Leaves__c = 'Yes';
            eleUpdate.cClearanceToupdate[0].ELE_Rehire_Status__c = 'No';
            eleUpdate.cClearanceToupdate[0].ELE_Notice_or_Severance_pay__c = 'Yes';
            eleUpdate.cClearanceToupdate[0].ELE_Notice_period_partial__c = 'Yes';
            eleUpdate.doc=WCT_UtilTestDataCreation.createDocument();
            eleUpdate.uploadAttachment();  
            eleUpdate.updateDashBoard();
        }  
    }
       
    public static testmethod void  datareadyonlytest()
    {
         Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');             
            User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
            insert u;
            System.runAs(u)
                //Running the test case with user u
            {
         //init('jbahuguna@deloitte.com','Page.Resignationform');
            //createResignEle('Onsite Assignment',contactObj.id,'Consulting'); 

            eleObjs = ELE_Separation_Handler_Test.createELE_Case(con);
            clrMstrObj = ELE_Separation_Handler_Test.createClearanceMaster();
            ELE_CreateClearanceClass.ELE_CreateClearance(eleObjs.id,eleObjs.ELE_case_Type__c, eleObjs.ELE_fss__c,eleObjs.ELE_region__c,'Sencondary Stakeholders');
            assetMst = ELE_Separation_Handler_Test.createAssetMaster();
            clrObject = ELE_Separation_Handler_Test.createClearanceRec(eleObjs,System.Label.ELE_Secondary_Stakeholders_Label,true,false,false,System.today(),System.today(),System.today());
            List<Clearance_separation__c> clrLst = new List<clearance_separation__c>();
            clrLst.add(clrObject);
           
            ELE_Batch_Clerance_NotificationSend eleBatch = new ELE_Batch_Clerance_NotificationSend();
            Database.executeBatch(eleBatch);
            

            //stakeHoldercontactObj = ELE_Separation_Handler_Test.createContact(accountObj,'masandeep@deloitte.com');
             ELE_Separation_Handler_Test.createESL();
            Test.setCurrentPageReference(new PageReference('Page.ELE_employeeDataUpdatePage'));
            System.currentPageReference().getParameters().put('em', EncodingUtil.urlDecode(CryptoHelper.encrypt('masandeep@deloitte.com'), 'UTF-8'));
            System.currentPageReference().getParameters().put('cue', 'jbahuguna@deloitte.com');
            System.currentPageReference().getParameters().put('type', 'p');
            System.currentPageReference().getParameters().put('tm', 'ITS');
            System.currentPageReference().getParameters().put('id', clrObject.id);
        ELE_employeeDataReadonlyController eleUpdate=new ELE_employeeDataReadonlyController();
           //eleUpdate.cancelDashBoard();
            eleUpdate.getList_cAssetToDisplay();
            eleUpdate.getsDashBoardView();
            eleUpdate.stakeholderType = 's';
            eleUpdate.getsDashBoardView();
            eleUpdate.cClToUpdate.ELE_Stakeholder_Designation__c = 'Treasury';
            eleUpdate.getsDashBoardView();
            eleUpdate.AddAsset();
            eleUpdate.rendering = false;
            eleUpdate.sAssetToupdateID = 'Keyboard/Mouse';
            eleUpdate.sValue = 'Open';
            eleUpdate.status = 'Open';
            eleUpdate.listcAssetToUpdate[0].bSelectedAsset = true;
            eleUpdate.attNoticePay.body = Blob.valueOf('Test Data') ;
            eleUpdate.attNoticePay.name = 'notice_attachment' ;
            eleUpdate.attPartial.body = Blob.valueOf('Test Data') ;
            eleUpdate.attPartial.name = 'attPartial_attachment' ;
            eleUpdate.attRehireStatus.body = Blob.valueOf('Test Data') ;
            eleUpdate.attRehireStatus.name = 'attRehireStatus_attachment' ;
            eleUpdate.attSpecialLeav.body = Blob.valueOf('Test Data') ;
            eleUpdate.attSpecialLeav.name = 'attSpecialLeav_attachment' ;
            eleUpdate.attNotice.body = Blob.valueOf('Test Data') ;
            eleUpdate.attNotice.name = 'attNotice_attachment' ;
            eleUpdate.cClearanceToupdate[0].ELE_Notice_period_waived__c = 'Yes';
            eleUpdate.cClearanceToupdate[0].ELE_Special_Leaves__c = 'Yes';
            eleUpdate.cClearanceToupdate[0].ELE_Rehire_Status__c = 'No';
            eleUpdate.cClearanceToupdate[0].ELE_Notice_or_Severance_pay__c = 'Yes';
            eleUpdate.cClearanceToupdate[0].ELE_Notice_period_partial__c = 'Yes';
            //eleUpdate.updateDashBoard(); 
            } 
    }
    }