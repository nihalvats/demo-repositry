@isTest
public class WCT_Visa_Approval_FormController_Test
{
  public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        recordtype immRecType = [select id from recordtype where DeveloperName = 'L1_Visa'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        WCT_Immigration__c immi=new WCT_Immigration__c();
        immi.WCT_Assignment_Owner__c=con.id;
        immi.ownerId=UserInfo.getUserId();
        immi.WCT_Visa_Type__c = 'L1A Individual';
        immi.RecordTypeId = immRecType.Id;
        Insert immi;
        
        
       
        WCT_Task_Reference_Table__c taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        insert taskRef;
        task t=WCT_UtilTestDataCreation.createTask(immi.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=false;
        t.WCT_Task_Reference_Table_ID__c = taskRef.Id;
        insert t; 
        
        datetime visastart=date.Today().adddays(10);
        datetime visaend=date.Today().adddays(20);
        Test.starttest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_Visa_Approval_Form;
        Test.setCurrentPage(pageRef); 
        WCT_Visa_Approval_FormController controller=new WCT_Visa_Approval_FormController();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_Visa_Approval_FormController();
        GBL_Attachments attachmentHelper = new GBL_Attachments();      
        attachmentHelper.doc=WCT_UtilTestDataCreation.createDocument();
        attachmentHelper.uploadDocument();
        attachmentHelper.uploadRelatedAttachment(t.Id);
        controller.getAttachmentInfo();
        List<Id> st=new List<Id>();
        st.add(t.id);
        controller.attachmentHelper.docIdList=st;
        
        controller.visaStartDate =date.today().adddays(10).format();
        controller.visaEndDate = date.today().adddays(10).format();
        controller.save();
        controller.getImmigrationDetails();
        controller.save();
        
        //controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        controller.supportAreaErrorMesssage ='Error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
     //   controller.uploadAttachment();
     
    }
    
    public static testmethod void m2(){
    WCT_Visa_Approval_FormController controller=new WCT_Visa_Approval_FormController();
    controller=new WCT_Visa_Approval_FormController();
    
    controller.visaStartDate = '02/02/2016';
    controller.visaEndDate = '02/02/2016';
    controller.ImmigrationRecord.WCT_Visa_Approval_Status__c = false;
    }
    
            
}