@isTest
public class VTS_GBL_SearchPage_CTRL_Test 
{
	testmethod static void test1()
    {
        VTS_GBL_SearchPage_CTRL searchPage= new VTS_GBL_SearchPage_CTRL();
        searchPage.deloitteOffice='Test Office';
        searchPage.RFEPVL='Test PVL';
        searchPage.serviceArea='Test serviceArea';
        searchPage.serviceLine='Test serviceLine';
        searchPage.industry='Test industry';
        searchPage.region='Test region';
        searchPage.hireStatus='Test hireStatus';
        searchPage.OBS='Test OBS';
        searchPage.recruiter='Test recruiter';
        searchPage.Type_of_H1B_process='Test Type_of_H1B_process';
        searchPage.processStatus='Test processStatus';
        searchPage.RFEStatus='Test RFEStatus';
        searchPage.US_Or_USI='US,USI';
        searchPage.tempImmigration2.H1B_RFE_Due_Date__c= Date.today();
        searchPage.tempImmigration.H1B_RFE_Due_Date__c= Date.today();
        searchPage.tempImmigration2.H1B_Date_PPD_Letter_Received__c= Date.today();
        searchPage.tempImmigration.H1B_Date_PPD_Letter_Received__c= Date.today();
        searchPage.PVL='Test PVL';
        searchPage.Escalation='Yes';
        searchPage.capability='Test capability';
        searchPage.H1B_No_PVL_PPD_Letter_Required='Yes';
        searchPage.tempImmigration.H1B_Type_of_H1B_Application_Process__c='Yes';
        searchPage.tempImmigration.H1B_Process_Status__c='Test Value ';
        searchPage.tempImmigration.H1B_RFE_Status__c='Test Value ';
        searchPage.tempImmigration.H1B_Confirmation_Status_from_PPD__c='Test Value ';
        searchPage.tempImmigration.H1B_Work_Authorization_Through_4_1__c='Test Value';
        List<WCT_Immigration__c> temp=searchPage.topList;
        searchPage.search();
        searchPage.export();
        searchPage.ClearControls();
        VTS_GBL_SearchPage_CTRL.getExistingCompanies('TEst Field Name');
        VTS_GBL_SearchPage_CTRL.getItemsList('TEst Field Name');
        
        
        
        
    }
}