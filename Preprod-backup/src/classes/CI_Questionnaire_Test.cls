@isTest
public class CI_Questionnaire_Test {
    public static Id recdtype = Schema.SObjectType.Exit_Survey__c.getRecordTypeInfosByName().get('CI Questionnaire').getRecordTypeId();
        
      public static testmethod void createCIQ(){
        
        case cs = new case();
     // cs.ContactId='0031b000003hTaX';
        cs.Status='New';
        cs.Priority='3 - Medium';
        cs.Origin='Email';
        cs.WCT_Category__c='Separations';
        cs.Subject='test';
        cs.Description='Test description';
        insert cs;
        
    Exit_Survey__c es =new Exit_Survey__c();
          
        es.Case__c=cs.Id;
        es.recordtypeId=recdtype;
        es.ELE_Question_1__c ='Yes';
        es.ELE_Q1_Additional_Comments__c='Test Comments';
        es.ELE_Question_2__c='Yes';
        es.ELE_Question_2_approver_name__c='Test Name';
        es.ELE_Q2_Additional_Comments__c='Test Comments';
        es.ELE_Question_3__c='Yes';
        es.ELE_Question_3_Detail_steps__c='Test Comments';
        es.ELE_Question3_PPD_Name__c='Test PPD Name';
        es.ELE_Q3_Additional_Comments__c='Test Comments';
        es.ELE_Question_4__c='Yes';
        es.ELE_Question_4_Type_of_Data__c='Test Comments';
        es.ELE_Question_4_Type_of_Media__c='Hard Drive';
        es.ELE_Q4_Additional_Comments__c='Test Comments';
        es.ELE_Question_5__c='Yes';
        es.ELE_Q5_Additional_Comments__c='Test Comments';
        es.ELE_Question_6__c='Yes';
        es.ELE_Question_6_Details__c='Test Comments';
        es.ELE_Q6_Additional_Comments__c='Test Comments';
        es.ELE_Question_7__c='No';
        es.ELE_Q7_Additional_Comments__c='Test Comments';
  		insert es;
     
         CiTemplt temp = new CiTemplt ();
          temp.getCiQ();
          
	}
}