Public Class AR_Test_Search_Cont{
public List<Job__c> objJob{get;set;}
Transient List<Job__c> joblist{get;set;}
public string searchemail{get;set;}
Set<Id> sObjectIds = new Set<Id>();
public AR_Test_Search_Cont(ApexPages.StandardController controller)
{
objJob= new List<Job__c>();
search();
}
Public void search(){
  try{
   List<List <sObject>> searchList = [FIND :'Manager' IN All FIELDS RETURNING  Job__c(Job_Category__c,AR_Account_Industry__c,Post_Date__c,Name,Job_ID__c,
Description__c,AR_Requirements__c,CCL_Company_posting_name_communications__c,AR_Nearest_Metropolitan_Area_1__c,AR_Nearest_Metropolitan_Area_2__c,AR_Nearest_Metropolitan_Area_3__c)];
   joblist = ((List<Job__c>) searchList[0]);
   for(Job__c j : joblist){
   sObjectIds.add(j.id);
   }
   objJob = [select Name,id,AR_Account_Industry__c,Country__c,Industry__c,Location__c,CCL_Company_posting_name_communications__c ,Confidential__c,Job_Category__c,AR_Primary_Location_City__c,AR_Primary_Location_State__c,Job_ID__c,Post_Date__c from Job__c where id in :sObjectIds and Status__c='Active'  and RecordType.Name = 'Alumni Relations'];
  }
catch(System.SearchException ex){
         apexpages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Enter keywords.'));
    } 
}
}