public class GBL_Merge_Record_Bulk_Handler 
{
    public GBL_Merge_Record_Bulk_Handler(string objectType, string srcName, string srcId,  string destName, string destId, string configName, string staggingId )
    {
        try
        {
            AR_Merge_Record_HDLR mergeHandler= new AR_Merge_Record_HDLR(objectType, srcName, srcId, destName,  destId, configName);
            
            mergeHandler.recordMergeStagging.id=staggingId;
            mergeHandler.saveStagingRecord();
            if(!mergeHandler.isError)
            {
                /*
                 *	Selecting all the fields to merge, as its a automated process the manual checking of the fields needed to be merged is automated.
                 */
                for(AR_Merge_Record_HDLR.MergeFieldsWrapper temp  : mergeHandler.mergeFieldsWR)
                {
                    temp.include=true;
                }
                
                for( AR_Merge_Record_HDLR.RelatedListWrapper tempRelatedWrapper :mergeHandler.relatedLists)
                {
                    tempRelatedWrapper.include=true;
                }
                
                mergeHandler.mergeRecord();
             }
            else
            {
                
                
            }
        }
        catch(Exception ex)
        {
            
            WCT_sObject_Staging_Records__c stagging=new WCT_sObject_Staging_Records__c();
            stagging.id=staggingId;
            stagging.MR_Merged_Status__c='Failed';
            stagging.MR_Error_Message__c=''+ex;
            update stagging;
            System.debug('Exception'+ex);
            
        }
        
    }
}