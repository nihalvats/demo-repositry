public class AR_CMP_AlumniCount_CTRL {

    public integer allumniCount{get; set;}
    public boolean allumniPresent{get; set;}
    public string allumniPresentCheckImg{get; set;}
    public string companyId{get; set;}
    public AR_CMP_AlumniCount_CTRL(ApexPages.StandardController sc)
    {
        companyId=sc.getId();
        
        List<AggregateResult> results = [select count(Id) allumniCount From Contact where AR_Current_Company__c = :companyId and (WCT_Type__c='Separated' or WCT_Type__c='Retiree/pensioner')];
        
        if(results.size()>0)
        {
            
            allumniCount=(integer)results[0].get('allumniCount');
            
        }
        else
        {
           allumniCount=0; 
        }
        
        allumniPresentCheckImg=allumniCount>0?'/img/checkbox_checked.gif':'/img/checkbox_unchecked.gif';
        
    }
    
}