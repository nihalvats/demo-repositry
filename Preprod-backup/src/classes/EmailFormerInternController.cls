public class EmailFormerInternController {
    public string emailarea {get;set;}
    public string Eventid;
    public string Eventname;
    public string Eventstartdate;
    public string Eventenddate;
    public string venuename;
    public string frmid;
    public string rsst;
    public boolean mailsent{get;set;}
    public string fromaddress{get;set;}
    public list<string> invalidemails{get;set;}
    public boolean mailinvalid{get;set;}
    public EmailFormerInternController()
    {
        mailsent=true;
        mailinvalid=false;
        invalidemails=new list<string>();
    }
    // method to send mail to former interns
    public pagereference sendmail()
    {
       //testing
       system.debug('Called sendmail');
        Eventid=apexpages.currentPage().getParameters().get('id');
        Eventname=apexpages.currentPage().getParameters().get('name');
        Eventstartdate=apexpages.currentPage().getParameters().get('startdate');
        Eventenddate=apexpages.currentPage().getParameters().get('enddate');
        List<string> temptext=new List<string>();
        frmid='';
        rsst='';
        //var checkvalidemail=;
        temptext=emailarea.split(';');
        list<Messaging.SingleEmailMessage> messages=new list<Messaging.SingleEmailMessage>();
        list<Messaging.SingleEmailMessage> messagespwd=new list<Messaging.SingleEmailMessage>();
        for(integer i=0;i<temptext.size();i++)
        {
            temptext[i]=temptext[i].trim();
            if(Pattern.matches('^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$',temptext[i]))
            {
                string tempEncryptemail=CryptoHelper.encrypt(temptext[i]);
                string pwd=generatepwd(temptext[i]);
                string urllink=system.Label.Site_Base_URL+'/event/FormerInternRSVP?id='+Eventid+'&em='+tempEncryptemail+'&frmid='+frmid+'&rsst='+rsst;
                List<string> tmp=new List<string>();
                tmp.add(temptext[i]);
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.setSubject(system.Label.Former_Intern_sub);
                message.setToAddresses(tmp);
                message.setOrgWideEmailAddressId(fromaddress);
                message.sethtmlbody('<table style="font-family:Verdana"><tbody><tr><td style="padding-bottom:20px;padding-top:10px"><img src="'+system.label.Document_URL+'id='+system.label.Deloitte_Logo_Label+'&oid='+system.label.Org_ID+'" alt=""></td></tr><tr><td style="padding-a:20px;font-size:9pt">Deloitte LLP|2016</td></tr><tr><td></td></tr><tr><td style="COLOR: #000000; FONT-SIZE: 15pt">Beyond all limits</td></tr><tr><td style="color: #000000; FONT-SIZE: 15pt">2016 intern conferences</td></tr></tbody></table><table><tbody><tr><td style="COLOR: #000000; FONT-SIZE: 15pt;padding-top:10px;padding-bottom:10px;font-family:Verdana">You\'re Invited!</td></tr><tr><td style="font-size:9pt;font-family:Verdana;">We welcome you to join your fellow interns and future colleagues for a multi-day conference at Deloitte University focusing on professional development, networking and team building. The conference will be an excellent opportunity for you to:<div><br/>1. Gain valuable insight about Deloitte’s business and culture<br/>2. Create valuable professional relationships<br/>3. Meet interns from other offices<br/>4. Have fun!</div></td><tr></table><br/><table><tr><td style="background:#000000;width:45px; color : white; padding: 10px ;font-weight:bold;font-size:9pt;font-family:Verdana">What</td><td style="width:348px; background:white ;border-bottom: 1px #000000 solid;font-size:9pt;font-family:Verdana">'+Eventname+'</td></tr><tr><td style="background:#000000;width:45px; color : white;padding: 10px;font-size:9pt;font-family:Verdana;font-weight:bold">When</td><td style="width:348px; background:white; border-bottom: 1px #000000 solid;font-size:9pt;font-family:Verdana">'+Eventstartdate+' through '+Eventenddate+'</td></tr><tr><td style="background:#000000;width:45px; color : white; padding: 10px;font-size:9pt;font-family:Verdana;font-weight:bold ">Where</td><td style="width:348px; background:white; border-bottom: 1px #000000 solid;font-size:9pt;font-family:Verdana">Deloitte University</td></tr><tr><td style="background:#000000;width:45px; color : white; padding: 10px;font-size:9pt;font-family:Verdana;font-weight:bold ">Attire</td ><td style="width:348px ; background:white; border-bottom: 1px #000000 solid;font-size:9pt;font-family:Verdana">Business Casual<td></td></tr></tbody></table><table><tr><td style="font-weight:bold;font-size:9pt;font-family:Verdana">Click here to&nbsp;<a href=\"'+urllink+'\" alt="" style="font-family:Verdana;font-size:9pt;text-decoration: underline;color:#000000">RSVP</a></td></tr></table><table><tr><td style="padding-top:10px;font-size:9pt;font-family:Verdana">Thanks and regards</td></tr><tr><td style="font-size:9pt;font-family:Verdana">Deloitte Campus Recruiting Team</td></tr></table><table><tr><td><div></p><p style="font-size: 12pt; margin-top: 0.17in; margin-bottom: 0in;"><a class="western" href="https://www.deloitte.com/us"><font color="#002776"><font face="OpenSans-Regular,Verdana" style="font-size: 8pt;"><b>Deloitte.com</b></font></font></a><font face="OpenSans-Regular,Verdana"><font size="2">&nbsp;|&nbsp;</font></font><a class="western" href="http://www.deloitte.com/us/legal"><font color="#002776"><font face="OpenSans-Regular,Verdana" style="font-size: 8pt;"><b>Legal</b></font></font></a><font face="OpenSans-Regular,Verdana"><font size="2">&nbsp;|&nbsp;</font></font><a class="western" href="http://www.deloitte.com/us/privacy"><font color="#002776"><font face="OpenSans-Regular,Verdana" style="font-size: 8pt;"><b>Privacy</a></b></font></font></p><p style="font-size: 12pt; margin-top: 0.17in; margin-bottom: 0in;"><a href="http://www.linkedin.com/company/deloitte" style="font-size: 12pt; background-color: rgb(255, 255, 255);"><img src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000000hJPM&amp;oid=00D40000000MxFD" alt="Right click on the image to save it to your computer."></a><span style="font-size: 12pt; background-color: rgb(255, 255, 255);">&nbsp;</span><a href="http://www.facebook.com/YourFutureAtDeloitte" style="font-size: 12pt; background-color: rgb(255, 255, 255);"><img src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000000hJPH&amp;oid=00D40000000MxFD" alt="Right click on the image to save it to your computer." style="font-size: 12pt;"></a><span style="font-size: 12pt; background-color: rgb(255, 255, 255);">&nbsp;</span><a href="http://www.youtube.com/deloittellp" style="font-size: 12pt; background-color: rgb(255, 255, 255);"><img src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000000hJPW&amp;oid=00D40000000MxFD" alt="Right click on the image to save it to your computer." style="font-size: 12pt;"></a><span style="font-size: 12pt; background-color: rgb(255, 255, 255);">&nbsp;</span><a href="http://www.deloitte.com/view/en_US/us/press/social-media/index.htm" style="font-size: 12pt; background-color: rgb(255, 255, 255);"><img src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000000hJPT&amp;oid=00D40000000MxFD" alt="Right click on the image to save it to your computer." style="font-size: 12pt;"></a><span style="font-size: 12pt; background-color: rgb(255, 255, 255);">&nbsp;</span><a href="http://www.deloitte.com/view/en_US/us/press/rss-feeds/index.htm" style="font-size: 12pt; background-color: rgb(255, 255, 255);"><img src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000000hJPI&amp;oid=00D40000000MxFD" alt="Right click on the image to save it to your computer." style="font-size: 12pt;"></a></p><p style="font-size: 12pt; margin-top: 0.17in; margin-bottom: 0.17in;"><font face="OpenSans-Regular,Verdana" size="1">30 Rockefeller Plaza<br>New York, NY 10112-0015<br>United States</font></p><p style="font-size: 12pt; margin-top: 0.17in; margin-bottom: 0.17in;"><img src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000000hJP9&amp;oid=00D40000000MxFD"></p><p class="MsoNoSpacing" style="font-size: 15.5555562973022px;"><font face="OpenSans-Regular,Verdana" size="1"><p>Deloitte refers to one or more of Deloitte Touche Tohmatsu Limited, a UK private company limited by guarantee (&ldquo;DTTL&rdquo;), its network of</br>member firms, and their related entities. DTTL and each of its member firms are legally separate and independent entities. DTTL (also </br>referred to as &ldquo;Deloitte Global&rdquo;) does not provide services to clients. Please see www.deloitte.com/about for a more detailed description of </br> DTTL and its member firms.</p><p>Copyright &copy; 2016 Deloitte Development LLC. All rights reserved.</p><p>To no longer receive emails about this topic please send a return email to the sender with the word &ldquo;Unsubscribe&rdquo; in the subject line.</p></td></tr></tbody></table>'); 
                //it was inactive before 
                messages.add(message);
                //send password
                Messaging.SingleEmailMessage messagesendpwd = new Messaging.SingleEmailMessage();
                messagesendpwd.setSubject(system.Label.Former_Intern_Pwd_sub);
                messagesendpwd.setToAddresses(tmp);
                messagesendpwd.setOrgWideEmailAddressId(fromaddress);
                messagesendpwd.setplaintextbody('Hi,\n\nplease find your password for registration\n\n'+pwd+'\n\nThanks and regards\nDeloitte Campus Recruiting Team');
                messagesendpwd.setsaveAsActivity(false); 
              //  messagespwd.add(messagesendpwd);
        }
            else
            {
             mailinvalid=true;
             invalidemails.add(temptext[i]);   
            }
        }
         if(!messages.isEmpty()){
             //it was inactive before
                Messaging.sendEmail(messages);
            }
            if(!messagespwd.isEmpty()){
                //it was inactive before
                Messaging.sendEmail(messagespwd);
            }
        mailsent=false;
        return null;
    }
    //method to get the mailboxes
        public List<SelectOption> getItemsmailbox() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption(system.label.Tax_Intern_Conf,'Tax Intern Conference (US)')); 
        options.add(new SelectOption(system.label.Central_Audit_Intern_Conference_US,'Central Audit Intern Conference (US)')); 
        options.add(new SelectOption(system.label.East_and_West_Audit_Intern_Conference_US,'East and West Audit Intern Conference (US)'));
        options.add(new SelectOption(system.label.Consulting_Summer,'Consulting Summer Scholar Conference (US)'));
        options.add(new SelectOption(system.label.Discovery_Intern_Summit_US,'Discovery Intern Summit (US)'));
        options.add(new SelectOption(system.label.Advisory_Intern_Conference_US,'Advisory Intern Conference (US)'));
        options.add(new SelectOption(system.label.Enabling_Areas_Intern_Conference_US,'Enabling Areas Intern Conference (US)'));
        return options; 
    }
    public static string generatepwd(string email)
    {
        string pwd;
        pwd=email.substring(0, 4)+'4712';
        return pwd;
    }
}