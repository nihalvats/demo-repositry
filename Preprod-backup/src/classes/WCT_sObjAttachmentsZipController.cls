global class WCT_sObjAttachmentsZipController {
 
    public String zipFileName {get; set;}
    public String zipContent {get; set;}
    public String AttIdsArr {get; set;}  
    public Id docId {get;set;}
    public String recordId {get;set;}
    
    //Variables Specific to Leaves Search Page
    public String empName {get;set;}
    public String empEmail {get;set;}
    public String empPersonId {get;set;}
    public String leaveName {get;set;}
    
    public set<Id> leaveIdSet = new set<Id>();
    public List<LeaveWrapper> LeaveWrapperList {get;set;}
    
    //Wrapper class to show Leave Search results
    public class LeaveWrapper{
        public boolean isSelected{get;set;}
        public WCT_Leave__c leave{get;set;}
        
        public LeaveWrapper(boolean selected,WCT_Leave__c lve){
            isSelected = selected;
            leave = lve;
        }
    
    }
    
    //Constructor
    global WCT_sObjAttachmentsZipController(){
        if(ApexPages.currentPage().getParameters().get('Id') != null) recordId = ApexPages.currentPage().getParameters().get('Id');
    }
    
    /*
        Method Name  : searchLeaves   
        Return Type  : Void
    */
    public void searchLeaves(){
        string query;
        List<WCT_Leave__c> leaveList = new List<WCT_Leave__c>();
        LeaveWrapperList = new List<LeaveWrapper>();
        query = 'SELECT id,name,WCT_Employee__c,WCT_Employee_Name__c,WCT_Personnel__c,WCT_Deloitte_Email__c,WCT_Leave_Status__c FROM WCT_Leave__c WHERE WCT_Leave_Status__c <> \'Cancelled\'';
        
        if(leaveName!= null && leaveName!=''){
            query += ' AND Name LIKE \'%'+leaveName.trim()+'%\'' ;
        }
        
        if(empName!= null && empName!=''){
            query += ' AND WCT_Employee_Name__c LIKE \'%'+empName.trim()+'%\'' ;
        }
        
        if(empEmail!= null && empEmail!=''){
            query += ' AND WCT_Deloitte_Email__c LIKE \'%'+empEmail.trim()+'%\'' ;
        }
        
        if(empPersonId!= null && empPersonId!=''){
            query += ' AND WCT_Personnel__c LIKE \'%'+empPersonId.trim()+'%\'' ;
        }
        
        query += ' LIMIT 100';
        system.debug('**'+query);
        leaveList = DataBase.query(query);
        
        if(!leaveList.isEmpty()){
            for(WCT_Leave__c lv:leaveList){
                LeaveWrapperList.add(new LeaveWrapper(false,lv));
            
            }
        }
    
    }
    /*
        Method Name  : getDocuments   
        Return Type  : Download Page
    */
    
    public pageReference getDocuments(){
        leaveIdSet = new Set<Id>();
        if(!LeaveWrapperList.isEmpty()){
        for(LeaveWrapper lw:LeaveWrapperList){
            if(lw.isSelected){
               leaveIdSet.add(lw.leave.id); 
            }
        } 
        }
        getAttachments();
         PageReference returnURL;
         returnURL =  Page.WCT_sObjAttachmentsZipPage;
         
        returnURL.getParameters().put('nooverride', '1');
        returnURL.setRedirect(false);  
        return returnURL;
    }

    /*
        Method Name  : uploadZip   
        Return Type  : Void
    */
    global void uploadZip() {
        if (String.isEmpty(zipFileName) ||
            String.isBlank(zipFileName)) {
            zipFileName = 'BulkZip_File.zip';//Default Name for zip file.
        }
        else {
            zipFileName.replace('.', '');// To prevent file extension
            zipFileName += '.zip';
        }
         
        Document doc = new Document();
        doc.Name = zipFileName;
        doc.ContentType = 'application/zip';
        doc.FolderId = Label.Attachment_Zip_Document_Folder_Id;
        doc.Body = EncodingUtil.base64Decode(zipContent);
        
        try{
            insert doc;  
        }Catch(DMLException ex)
        {
            WCT_ExceptionUtility.logException('WCT_sObjAttachmentsZipController-uploadZip', null, ex.getMessage() + ' ::: ' + ex.getStackTraceString());
        }        
        docId = doc.Id; 
        this.zipFileName = null;
        this.zipContent = null;
    }
    
    /*
        Method Name  : getAttachments   
        Return Type  : List of Attachments
    */
    global List<sObjAttachmentWrapper> getAttachments() {
        
        Set<Id> setParentIds = new Set<Id>();
        setParentIds.add(recordId);
        //Add Leave Parent Ids
        if(!leaveIdSet.isEmpty()){
            setParentIds.addAll(leaveIdSet);
        }
        if(recordId <> null && recordId.startsWith(case.sObjectType.getDescribe().getKeyPrefix())){
            
            for(EmailMessage em: [SELECT Id FROM EmailMessage WHERE ParentId = :recordId]){
                setParentIds.add(em.id);
            }
        }
                  
        List<Attachment> listAttachments = [
                                                SELECT 
                                                    Id, 
                                                    ParentId,
                                                    Name, 
                                                    Parent.Name,
                                                    Parent.Type,
                                                    ContentType, 
                                                    BodyLength, 
                                                    CreatedDate 
                                                FROM 
                                                    Attachment 
                                                WHERE 
                                                    ParentId in :setParentIds
                                            ];                         
        List<sObjAttachmentWrapper> listAttWrapper = new List<sObjAttachmentWrapper>();
        for(Attachment a : listAttachments) {
            sObjAttachmentWrapper taw = new sObjAttachmentWrapper();
            taw.attRecord = a;
            String size = null;
            if(1048576 < a.BodyLength) {
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength) {
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + a.BodyLength + ' bytes';
            }
            taw.size = size;
            listAttWrapper.add(taw);
        }
        
        return listAttWrapper;
    }

    @RemoteAction
    global static AttachmentWrapper getAttachment(String fullName) {
        String attId = fullName.subString(0,18);
        String attName = '';
        
        Attachment att = [select Id, Name, ContentType, Body,parent.type 
                          from Attachment
                          where Id = :attId];
        if(att.parent.type == 'EmailMessage'){
            attName = 'Email Message';
        }else{                  
            if(fullName.length() > 18) {
                attName = fullName.subString(18, fullName.length());
            } 
        }                 
         
        AttachmentWrapper attWrapper = new AttachmentWrapper();
        attWrapper.attEncodedBody = EncodingUtil.base64Encode(att.body);
        attName = attName + ' - ' + att.Name;
        if(attName.length() > 255) {
            String[] nameParts = attName.split('\\.', 2);        
            attName = nameParts[0].subString(0, nameParts[0].length() - (attName.length() - 255)) + '.' + nameParts[1];
        }
        attWrapper.attName = attName;
        return attWrapper;
    }
    /*
        Wrapper Class: AttachmentWrapper
        Description  : Wrapper class to wrap Attachment Name and Attachment body.
    */       
    global class AttachmentWrapper {
        public String attEncodedBody {get; set;}
        public String attName {get; set;}
    }
    
    /**
     * Class to wrap the Attachment along with the task name
     */
    global class sObjAttachmentWrapper {
        
        public Attachment attRecord {get; set;}
        public String size {get; set;}
    }
}