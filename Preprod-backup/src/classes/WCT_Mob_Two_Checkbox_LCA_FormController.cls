/********************************************************************************************************************************
Apex class         : <WCT_Mob_Two_Checkbox_LCA_FormController>
Description        : <Controller which allows to Update Task, Assignment and Mobility>
Type               :  Controller
Test Class         : <WCT_Mob_Two_Checkbox_LCA_Form_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 24/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/  
public  class WCT_Mob_Two_Checkbox_LCA_FormController extends SitesTodHeaderController {
    
    //TASK RELATED VARIABLES
    public task taskRecord{get;set;}
    public WCT_Task_Reference_Table__c taskRefRecord{get;set;}
    public String taskSubject {get;set;}
    public String taskid{get;set;}
    public String taskVerbiage{get;set;}
    
    //LCA RELATED VARIABLES
    public boolean display{get;set;}
    public boolean validLCA{get;set;}
    public boolean newLCA{get;set;}
    
   //PUBLIC  VARIABLES
    public WCT_Mobility__c MobilityRec {get;set;}
    public WCT_Assignment__C AssignRec {get;set;}
    
    //ERROR RELATED VARIABLES
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    
    // DEFINING A CONSTRUCTOR 
    public WCT_Mob_Two_Checkbox_LCA_FormController()
    {
        init();
        taskid = ApexPages.currentPage().getParameters().get('taskid');
        display=true;
        validLCA=false;
        newLCA=false;
        taskSubject = '';
        taskVerbiage = '';
    }
    
    /********************************************************************************************
    *Method Name         : <init()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <Init() Used for loading Mobility, Assignment>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/ 
    //INIT METHOD FOR INITIALISING MOBILITY AND ASSIGNMENTS 
    private void init(){
        MobilityRec = new WCT_Mobility__c();
        AssignRec = new WCT_Assignment__c();
    }   

        /********************************************************************************************
        *Method Name         : <updateTaskFlags()>
        *Return Type         : <Null>
        *Param’s             : 
        *Description         : <updateTaskFlags() Used for fetching TaskReference, Mobility & Assignment Record>
        
        *Version          Description
        * -----------------------------------------------------------------------------------------------------------                 
        * 01              Original Version
        *********************************************************************************************/     
 
    public void updateTaskFlags()
    {
        if(taskid==''|| taskid==null){
            display=false;
            return;
        }

        taskRecord=[SELECT id, Subject, WhatId, Ownerid, Status, WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c, WCT_Task_Reference_Table_ID__c FROM Task WHERE Id =: taskid];
        taskRefRecord = [SELECT Id, Name, WCT_Task_for_Object__c, WCT_Task_Subject__c, Form_Verbiage__c, ownerId FROM WCT_Task_Reference_Table__c Where id=:taskRecord.WCT_Task_Reference_Table_ID__c];
        MobilityRec = [SELECT Id, Name, OwnerId FROM WCT_Mobility__c WHERE Id=:taskRecord.WhatId];
        AssignRec = [Select Id, Name, WCT_Status__c, WCT_Mobility__c FROM WCT_Assignment__c WHERE WCT_Mobility__c =: MobilityRec.Id AND (WCT_Status__c = 'Active' OR WCT_Status__c = 'New')];

        taskSubject = taskRecord.Subject;
        taskVerbiage = taskRefRecord.Form_Verbiage__c;

    }

    /********************************************************************************************
    *Method Name         : <save()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <Save() Used for Updating Task, Mobility, Assignment>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/     
   //SAVE METHOD FOR UPDATING TASK
    public pageReference save()
    {
       if(newLCA != true && validLCA != true)
        {
            pageErrorMessage = 'Please check any one of the checkboxes in order to submit the form.';
            pageError = true;
            return null;
        }
        if(newLCA == true && validLCA == true)
        {
            pageErrorMessage = 'Please check any ONE checkbox in order to submit the form.';
            pageError = true;
            return null;
        }
        if(newLCA == true)
            AssignRec.WCT_Assignment_LCA_Status__c = 'Applied for a New LCA';
        else if(validLCA == true)
            AssignRec.WCT_Assignment_LCA_Status__c = 'Hold a valid LCA';

        update MobilityRec;
        update AssignRec;
        try{    
        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        taskRecord.OwnerId=UserInfo.getUserId();
        upsert taskRecord;

        if(string.valueOf(MobilityRec.OwnerId).startsWith('00G')){
            taskRecord.OwnerId = System.Label.GMI_User;
        }else{
            taskRecord.OwnerId = MobilityRec.Ownerid;
        }
        
        if(taskRecord.WCT_Auto_Close__c == true){   
            taskRecord.status = 'Completed';
        }else{
            taskRecord.status = 'Employee Replied';  
        }

        taskRecord.WCT_Is_Visible_in_TOD__c = false; 
        upsert taskRecord;

        pageError = false;
        }
        catch(Exception e){
            
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Mob_Two_Checkbox_LCA_FormController', 'Mob Two Checkbox Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_MOB_TOCHX_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
            
        }
        PageReference pageRef = new PageReference('/apex/WCT_Mob_Two_Checkbox_LCA_FormThankYou?taskid='+taskid);  
        pageRef.setRedirect(true);
        return pageRef;
       
    }
}