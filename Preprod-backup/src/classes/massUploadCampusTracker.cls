public class massUploadCampusTracker
{
    public integer totalrecords { get; set; }
    public integer totalsuccessrec { get; set; }
    public integer totalsuccessreccount { get; set; }
    public integer totalunsuccessrec { get; set; }
    public integer totalinvalid { get; set; }
 
    public integer totalNoRMSID { get; set; }
    public String nameFile { get; set; }
    
    public list<contact> lcon = new list<contact> ();
    transient public list<id> listids = new list<id>();
    transient public list<id> recTypes = new list<id>();
    public integer size{get; set;}
   
    public static final id candRecTypeId = WCT_Util.getRecordTypeIdByLabel('Contact','Candidate');
    public static final id adhocCandRecTypeId = WCT_Util.getRecordTypeIdByLabel('Contact','Candidate Ad Hoc');
    public static final id empRecTypeId = WCT_Util.getRecordTypeIdByLabel('Contact','Employee');
        public List<MyWrapper> wrapper1 {get; set;}
    public List<MyWrapper> wrapper {get; set;}
    public boolean b {get{
      if(wrapper!=null)
      {
          return wrapper.size()>0?true:false;
      }
      else 
      {
          return false;
      }
    }}
       public boolean b1 {get{
      if(wrapper1!=null)
      {
          return wrapper1.size()>0?true:false;
      }
      else 
      {
          return false;
      }
    }
  }

  transient public Map<string, contact> conNamesIdMap = new Map<string, contact>();
  transient public Blob contentFile { get; set; }
  public List<List<String>> fileLines = new List<List<String>>();
  List<Contact> Conlist;
  public Contact UpdateCon;
  List<String> conNames;
  List<String> conEmails;
  Set<String> conNamesf;
  Set<String> conNamesr;
  public  list<string> emaillist= new list<string> (); 
  Map<String, Contact> newlistmap = new Map<String, Contact>();
  Map<String, Contact> newlistmap1 = new Map<String, Contact>();
  List<Contact> newCons = new List<Contact>();
    
  WCT_parseCSV parseCSVInstance = new WCT_parseCSV();  
   
  public Pagereference ReadFile()
  {        
        recTypes = new list<id>();
        recTypes.add(candRecTypeId); 
        //recTypes.add(adhocCandRecTypeId); 
        recTypes.add(empRecTypeId); 
        
        conNamesIdMap =new Map<string, contact>();
        
        /*Validtion if no file selected */
        if(contentFile!=null)
        {
            nameFile = contentFile.toString();
            filelines = parseCSVInstance.parseCSV(nameFile, true);
        }
        else
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please choose a file to upload !'));
            return null;
        }
        System.debug('filelines'+filelines);
        Conlist= new List<Contact>();
        conNames = new List<String>();
        conEmails = new List<String>();
        conNamesf = new set<String>();
        conNamesr= new set<String>();
        wrapper = new List<MyWrapper>() ;
          wrapper1 = new List<MyWrapper>() ;
        listids= new list<id>();
        newlistmap= new Map<String, Contact>();
        totalunsuccessrec =0;
        totalsuccessrec =0;
        totalrecords=0;
        totalsuccessreccount=0;
        totalinvalid=0;
         
        List<String> recruiterEmailIDs = new List<String>();        
                
        /*Step 1 :: Processing the uploaded file and fetching the various details into  the contact object List newlistmap.*/       

        for(List<String> inputValues:filelines) 
        {   
            totalrecords++;
             
            for (integer j=0; j<inputvalues.size(); j++) {
                //System.debug('inputvalues['+j+']'+inputvalues[j]);
            }
            
            Contact c = new Contact();    
            c.WCT_User_Group__c = 'United States';   
            c.WCT_Job_Type__c = 'Campus';  
            c.WCT_RecordCreated_from_BulkUpload__c = true;                     
            if((inputvalues.size()>0 && inputvalues[0]!=null)?inputvalues[0].trim()!='':false){
                c.WCT_Date_sent_to_USI__c=date.parse(inputvalues[0]!=null?inputvalues[0].trim():inputvalues[0]);
            }
            c.WCT_Update_enter_an_X__c = (inputvalues.size()>1 && inputvalues[1]!='')?inputvalues[1]:'';
            c.WCT_Recruiter_Notes_for_Updates__c = (inputvalues.size()>2 && inputvalues[2]!='')?inputvalues[2]:'';
            c.WCT_RMS_Tracking_Process__c = (inputvalues.size()>3 && inputvalues[3]!='')?inputvalues[3]:'';
            c.WCT_Taleo_Id__c = (inputvalues.size()>4 && inputvalues[4]!='')?inputvalues[4]:'';
            c.CR_School_Recruiter__c = (inputvalues.size()>5 && inputvalues[5]!='')?inputvalues[5].trim():'';
            c.WCT_Candidate_Recruiting_Coordinator__c = (inputvalues.size()>6 && inputvalues[6]!='')?inputvalues[6].trim():'';
            
            if(c.CR_School_Recruiter__c != '') {
                recruiterEmailIDs.add(c.CR_School_Recruiter__c);
            }
            if(c.WCT_Candidate_Recruiting_Coordinator__c != '') {
                recruiterEmailIDs.add(c.WCT_Candidate_Recruiting_Coordinator__c);
            }            
            
            c.LastName = (inputvalues.size()>7 && inputvalues[7]!='')?inputvalues[7]:'';
            c.FirstName = (inputvalues.size()>8 && inputvalues[8]!='')?inputvalues[8]:'';
            c.WCT_Home_Street__c = (inputvalues.size()>9 && inputvalues[9]!='')?inputvalues[9]:'';
            c.WCT_Home_Street_2__c = (inputvalues.size()>10 && inputvalues[10]!='')?inputvalues[10]:'';
            c.WCT_Home_City__c = (inputvalues.size()>11 && inputvalues[11]!='')?inputvalues[11]:'';
            c.WCT_Home_State__c = (inputvalues.size()>12 && inputvalues[12]!='')?inputvalues[12]:'';
            c.WCT_Home_Zip__c = (inputvalues.size()>13 && inputvalues[13]!='')?inputvalues[13]:'';
            c.HomePhone = (inputvalues.size()>14 && inputvalues[14]!='')?inputvalues[14]:'';
            c.MobilePhone = (inputvalues.size()>15 && inputvalues[15]!='')?inputvalues[15]:'';
            c.Email = (inputvalues.size()>16 && inputvalues[16]!='')?inputvalues[16].trim():'';
            c.WCT_Gender__c = (inputvalues.size()>17 && inputvalues[17]!='')?inputvalues[17]:'';
            c.WCT_Race__c = (inputvalues.size()>18 && inputvalues[18]!='')?inputvalues[18]:'';
            c.WCT_Candidate_School__c = (inputvalues.size()>19 && inputvalues[19]!='')?inputvalues[19]:'';
            c.Degree_Program__c = (inputvalues.size()>20 && inputvalues[20]!='')?inputvalues[20]:'';
            c.CR_Undergrad_Major__c = (inputvalues.size()>21 && inputvalues[21]!='')?inputvalues[21]:'';
            if((inputvalues.size()>22 && inputvalues[22]!=null)?inputvalues[22].trim()!='':false){
                c.CR_Undergraduate_Overall_GPA_4_00_Scale__c=decimal.valueOf(inputvalues[22]!=null?inputvalues[22].trim():inputvalues[22]);
            }
            c.SAT_Total__c = (inputvalues.size()>23 && inputvalues[23]!='')?inputvalues[23]:'';
            if((inputvalues.size()>24 && inputvalues[24]!=null)?inputvalues[24].trim()!='':false){
                c.CR_ACT__c=decimal.valueOf(inputvalues[24]!=null?inputvalues[24].trim():inputvalues[24]);
            }
            if((inputvalues.size()>25 && inputvalues[25]!=null)?inputvalues[25].trim()!='':false){
                c.GRE_Pre_8_1_2011__c=decimal.valueOf(inputvalues[25]!=null?inputvalues[25].trim():inputvalues[25]);
            }            
            if((inputvalues.size()>26 && inputvalues[26]!=null)?inputvalues[26].trim()!='':false){
                c.CR_GMAT__c=decimal.valueOf(inputvalues[26]!=null?inputvalues[26].trim():inputvalues[26]);
            }      
            c.WCT_Type_of_Hire__c = (inputvalues.size()>27 && inputvalues[27]!='')?inputvalues[27]:'';
            //c.WCT_Job_Type__c = (inputvalues.size()>28 && inputvalues[28]!='')?inputvalues[28]:'';
            c.WCT_Requisition_Number__c = (inputvalues.size()>29 && inputvalues[29]!='')?inputvalues[29]:'';
            c.Additional_Email__c = (inputvalues.size()>30 && inputvalues[30]!='')?inputvalues[30]:'';
            c.WCT_Function__c = (inputvalues.size()>31 && inputvalues[31]!='')?inputvalues[31]:'';
            c.WCT_RMS_Step__c = (inputvalues.size()>32 && inputvalues[32]!='')?inputvalues[32]:'';
            c.WCT_Taleo_Status__c = (inputvalues.size()>33 && inputvalues[33]!='')?inputvalues[33]:'';
            if((inputvalues.size()>34 && inputvalues[34]!=null)?inputvalues[34].trim()!='':false){
                c.US_Meets_Basic_Qualifications_date__c=date.parse(inputvalues[34]!=null?inputvalues[34].trim():inputvalues[34]);
            }            
            if((inputvalues.size()>35 && inputvalues[35]!=null)?inputvalues[35].trim()!='':false){
                c.Interview_1_to_be_Scheduled_Date__c=date.parse(inputvalues[35]!=null?inputvalues[35].trim():inputvalues[35]);
            }
            if((inputvalues.size()>36 && inputvalues[36]!=null)?inputvalues[36].trim()!='':false){
                c.WCT_Interview_1_Conducted_Date__c=date.parse(inputvalues[36]!=null?inputvalues[36].trim():inputvalues[36]);
            }
            c.WCT_Status_after_Interview_1__c = (inputvalues.size()>37 && inputvalues[37]!='')?inputvalues[37]:'';
            if((inputvalues.size()>38 && inputvalues[38]!=null)?inputvalues[38].trim()!='':false){
                c.WCT_Interview_2_Conducted_Date__c=date.parse(inputvalues[38]!=null?inputvalues[38].trim():inputvalues[38]);
            }
            c.WCT_Status_after_Interview_2__c = (inputvalues.size()>39 && inputvalues[39]!='')?inputvalues[39]:'';
            c.WCT_Send_RMS_Rejection_Notification__c = (inputvalues.size()>40 && inputvalues[40]!='')?inputvalues[40]:'';
            if((inputvalues.size()>41 && inputvalues[41]!=null)?inputvalues[41].trim()!='':false){
                c.WCT_Application_Initiated_Date__c=date.parse(inputvalues[41]!=null?inputvalues[41].trim():inputvalues[41]);
            }            
            c.WCT_Application_Status__c = (inputvalues.size()>42 && inputvalues[42]!='')?inputvalues[42]:'';
            if((inputvalues.size()>43 && inputvalues[43]!=null)?inputvalues[43].trim()!='':false){
                c.WCT_Application_Completion_Date__c=date.parse(inputvalues[43]!=null?inputvalues[43].trim():inputvalues[43]);
            }                        
            if(inputvalues.size()>44 && inputvalues[44]!='') {
                 c.WCT_Travel_Required__c = Boolean.valueOf(inputvalues[44]);
            }
            c.WCT_Type_of_Event__c = (inputvalues.size()>45 && inputvalues[45]!='')?inputvalues[45]:'';
            if((inputvalues.size()>46 && inputvalues[46]!=null)?inputvalues[46].trim()!='':false){
                c.WCT_Last_Day_of_Travel__c=date.parse(inputvalues[46]!=null?inputvalues[46].trim():inputvalues[46]);
            }                                    
            c.WCT_LPX_Code__c = (inputvalues.size()>47 && inputvalues[47]!='')?inputvalues[47]:'';
            c.WCT_Host_Recruiter__c = (inputvalues.size()>48 && inputvalues[48]!='')?inputvalues[48]:'';
            c.WCT_Notes__c = (inputvalues.size()>49 && inputvalues[49]!='')?inputvalues[49]:'';
            c.WCT_Hiring_Location__c = (inputvalues.size()>50 && inputvalues[50]!='')?inputvalues[50]:'';
            if((inputvalues.size()>51 && inputvalues[51]!=null)?inputvalues[51].trim()!='':false){
                c.WCT_Internship_Start_Date__c=date.parse(inputvalues[51]!=null?inputvalues[51].trim():inputvalues[51]);
            }            
            c.WCT_Start_Date_Year__c = (inputvalues.size()>52 && inputvalues[52]!='')?inputvalues[52]:'';
            c.WCT_Length_of_Internship__c = (inputvalues.size()>53 && inputvalues[53]!='')?inputvalues[53]:'';
            c.WCT_Hired_By__c = (inputvalues.size()>54 && inputvalues[54]!='')?inputvalues[54]:'';
            if((inputvalues.size()>55 && inputvalues[55]!=null)?inputvalues[55].trim()!='':false){
                c.WCT_Salary__c=decimal.valueOf(inputvalues[55]!=null?inputvalues[55].trim():inputvalues[55]);
            }            
            if((inputvalues.size()>56 && inputvalues[56]!=null)?inputvalues[56].trim()!='':false){
                c.WCT_Sign_On_Bonus__c=decimal.valueOf(inputvalues[56]!=null?inputvalues[56].trim():inputvalues[56]);
                System.debug('c.WCT_Sign_On_Bonus__c'+c.WCT_Sign_On_Bonus__c);
            }           
            if((inputvalues.size()>57 && inputvalues[57]!=null)?inputvalues[57].trim()!='':false){
                c.WCT_Relocation_Amt__c=decimal.valueOf(inputvalues[57]!=null?inputvalues[57].trim():inputvalues[57]);
            }        
            if((inputvalues.size()>58 && inputvalues[58]!=null)?inputvalues[58].trim()!='':false){
                c.WCT_Offer_Decision_Deadline_Date__c=date.parse(inputvalues[58]!=null?inputvalues[58].trim():inputvalues[58]);
            }             
            c.WCT_NextGen_Amount__c = (inputvalues.size()>59 && inputvalues[59]!='')?inputvalues[59]:'';
            if((inputvalues.size()>60 && inputvalues[60]!=null)?inputvalues[60].trim()!='':false){
                c.WCT_Promisory_Note_amount__c=decimal.valueOf(inputvalues[60]!=null?inputvalues[60].trim():inputvalues[60]);
            }           
            c.WCT_Transition_Assistance_Amount__c = (inputvalues.size()>60 && inputvalues[60]!='')?inputvalues[60]:'';
            c.WCT_Offer_Letter_Template__c = (inputvalues.size()>61 && inputvalues[61]!='')?inputvalues[61]:'';
            c.WCT_Other_Offers__c = (inputvalues.size()>62 && inputvalues[62]!='')?inputvalues[62]:'';
            c.WCT_Hiring_Partner__c = (inputvalues.size()>63 && inputvalues[63]!='')?inputvalues[63]:'';
            c.WCT_RequisitionHiringLevel__c = (inputvalues.size()>64 && inputvalues[64]!='')?inputvalues[64]:'';
            c.WCT_Entity__c = (inputvalues.size()>65 && inputvalues[65]!='')?inputvalues[65]:'';
            c.WCT_Recruiter_Phone__c = (inputvalues.size()>66 && inputvalues[66]!='')?inputvalues[66]:'';
            c.WCT_State_of_Hiring_Location__c = (inputvalues.size()>67 && inputvalues[67]!='')?inputvalues[67]:'';
            
            conNames.add(c.WCT_Taleo_Id__c);
            conEmails.add(c.Email);
            newlistmap.put(c.WCT_Taleo_Id__c,c);
            //newlistmap.put(c.Email,c);
            conNamesf.add(c.FirstName);
            Conlist.add(c);
            emaillist.add(c.WCT_Taleo_Id__c);
               
        }
         
      
        List<Contact> recruitersList = [SELECT Id, Name, Email FROM Contact where Email in :recruiterEmailIDs];
        Map<String, String> recruiterIds = new Map<String, String>();
        Map<String, String> recruiterNames = new Map<String, String>();
        for(Contact con: recruitersList) {
            recruiterIds.put(con.Email, con.Id);
            recruiterNames.put(con.Email, con.Name);
        }
        
        /*
        System.debug('recruiterEmailIDs'+recruiterEmailIDs);
        System.debug('recruitersList'+recruitersList);
        System.debug('recruiterIds'+recruiterIds);
        System.debug('recruiterNames'+recruiterNames);
        */
        
        /*Step 2 : Searching for the contacts with the emails ids from the Uploaded file in the SFDC. */
        
        
        system.debug('names::'+conNames);
        
        system.debug('emails::'+conEmails);
        
        
        
        List<Contact> existingConts = [SELECT 
                                        Id, Email, WCT_Taleo_Status__c, WCT_User_Group__c, WCT_Recruiter_Id__c, WCT_Recruiting_Coordinator_Id__c, WCT_RecordCreated_from_BulkUpload__c,
                                        WCT_Taleo_Id__c, CR_School_Recruiter__c, WCT_Candidate_Recruiting_Coordinator__c, LastName, FirstName, WCT_Home_Street__c, WCT_Home_Street_2__c,
                                        WCT_Home_City__c, WCT_Home_State__c, WCT_Home_Zip__c, HomePhone, MobilePhone, WCT_Gender__c, WCT_Race__c, WCT_Candidate_School__c, Degree_Program__c,
                                        CR_Undergrad_Major__c, CR_Undergraduate_Overall_GPA_4_00_Scale__c, SAT_Total__c, CR_ACT__c, GRE_Pre_8_1_2011__c, CR_GMAT__c, WCT_Type_of_Hire__c,
                                        WCT_Job_Type__c, WCT_Requisition_Number__c, Additional_Email__c, WCT_Function__c, WCT_Hiring_Location__c, WCT_Internship_Start_Date__c, WCT_Salary__c,
                                        WCT_Sign_On_Bonus__c, WCT_Relocation_Amt__c, WCT_NextGen_Amount__c, WCT_Promisory_Note_amount__c, WCT_Hiring_Partner__c, WCT_Entity__c, WCT_Recruiter_Phone__c,
                                        WCT_Date_sent_to_USI__c, WCT_Update_enter_an_X__c, WCT_Recruiter_Notes_for_Updates__c, WCT_RMS_Tracking_Process__c, 
                                        US_Meets_Basic_Qualifications_date__c, Interview_1_to_be_Scheduled_Date__c, WCT_Interview_1_Conducted_Date__c, WCT_RMS_Step__c, 
                                        WCT_Status_after_Interview_1__c, WCT_Interview_2_Conducted_Date__c, WCT_Status_after_Interview_2__c, WCT_Send_RMS_Rejection_Notification__c,
                                        WCT_Application_Initiated_Date__c, WCT_Application_Status__c, WCT_Application_Completion_Date__c, WCT_Travel_Required__c, WCT_Type_of_Event__c,
                                        WCT_Last_Day_of_Travel__c, WCT_LPX_Code__c, WCT_Host_Recruiter__c, WCT_Notes__c, WCT_Start_Date_Year__c, WCT_Length_of_Internship__c, WCT_Hired_By__c,
                                        WCT_Offer_Decision_Deadline_Date__c, WCT_Transition_Assistance_Amount__c, WCT_Offer_Letter_Template__c, WCT_Other_Offers__c, WCT_RequisitionHiringLevel__c,WCT_State_of_Hiring_Location__c                                          
                                        FROM Contact WHERE WCT_Taleo_Id__c IN :conNames AND Email IN :conEmails AND recordtypeid IN :recTypes limit 5000];
            
             system.debug('existingConts7::'+existingConts);
        /*Step 3 : By this step 'existingConts' has contacts record present in SFDC. 
                  A. Now, Modifying the existing contacts (existingConts) with new values from 'newlistmap' (from uploaded file)
                  B. At the same time remove the record which are been existing from the 'newlistmap' */
                  
      for (Contact cont: existingConts)
      {
     //  If(newlistmap.containsKey(cont.WCT_Taleo_Id__c) && newlistmap.containsKey(cont.Email))
       // {
            UpdateCon = newlistmap.get(cont.WCT_Taleo_Id__c);
            cont.WCT_User_Group__c=UpdateCon.WCT_User_Group__c;
            cont.WCT_Job_Type__c=UpdateCon.WCT_Job_Type__c; 
            cont.WCT_RecordCreated_from_BulkUpload__c=UpdateCon.WCT_RecordCreated_from_BulkUpload__c;
            cont.WCT_Date_sent_to_USI__c=UpdateCon.WCT_Date_sent_to_USI__c;
            cont.WCT_Update_enter_an_X__c=UpdateCon.WCT_Update_enter_an_X__c;
            cont.WCT_Recruiter_Notes_for_Updates__c=UpdateCon.WCT_Recruiter_Notes_for_Updates__c;
            cont.WCT_RMS_Tracking_Process__c=UpdateCon.WCT_RMS_Tracking_Process__c;
            //cont.WCT_Taleo_Id__c = UpdateCon.WCT_Taleo_Id__c;
            cont.CR_School_Recruiter__c=(recruiterNames.get(UpdateCon.CR_School_Recruiter__c) != null) ? recruiterNames.get(UpdateCon.CR_School_Recruiter__c) : '';
            cont.WCT_Candidate_Recruiting_Coordinator__c=(recruiterNames.get(UpdateCon.WCT_Candidate_Recruiting_Coordinator__c) != null) ? recruiterNames.get(UpdateCon.WCT_Candidate_Recruiting_Coordinator__c) : '';
            cont.WCT_Recruiter_Id__c=(recruiterIds.get(UpdateCon.CR_School_Recruiter__c) != null) ? recruiterIds.get(UpdateCon.CR_School_Recruiter__c) : '';
            cont.WCT_Recruiting_Coordinator_Id__c=(recruiterIds.get(UpdateCon.WCT_Candidate_Recruiting_Coordinator__c) != null) ? recruiterIds.get(UpdateCon.WCT_Candidate_Recruiting_Coordinator__c) : '';            
            cont.LastName=UpdateCon.LastName; 
            cont.FirstName=UpdateCon.FirstName; 
            if(UpdateCon.Email != '') {
                cont.Email=UpdateCon.Email;
            }
            if(UpdateCon.WCT_Home_Street__c != '') {
                cont.WCT_Home_Street__c=UpdateCon.WCT_Home_Street__c;
            }
            if(UpdateCon.WCT_Home_Street_2__c != '') {
                cont.WCT_Home_Street_2__c=UpdateCon.WCT_Home_Street_2__c;
            }
            if(UpdateCon.WCT_Home_City__c != '') {
                cont.WCT_Home_City__c=UpdateCon.WCT_Home_City__c;
            }
            if(UpdateCon.WCT_Home_State__c != '') {
                cont.WCT_Home_State__c=UpdateCon.WCT_Home_State__c;
            }
            if(UpdateCon.WCT_Home_Zip__c != '') {
                cont.WCT_Home_Zip__c=UpdateCon.WCT_Home_Zip__c;
            }
            cont.HomePhone=UpdateCon.HomePhone;
            cont.MobilePhone=UpdateCon.MobilePhone;
            cont.WCT_Gender__c=UpdateCon.WCT_Gender__c;
            cont.WCT_Race__c=UpdateCon.WCT_Race__c;
            cont.WCT_Candidate_School__c=UpdateCon.WCT_Candidate_School__c;
            cont.Degree_Program__c=UpdateCon.Degree_Program__c;
            cont.CR_Undergrad_Major__c=UpdateCon.CR_Undergrad_Major__c;
            cont.CR_Undergraduate_Overall_GPA_4_00_Scale__c=UpdateCon.CR_Undergraduate_Overall_GPA_4_00_Scale__c;
            cont.SAT_Total__c=UpdateCon.SAT_Total__c;
            cont.CR_ACT__c=UpdateCon.CR_ACT__c;
            cont.GRE_Pre_8_1_2011__c=UpdateCon.GRE_Pre_8_1_2011__c;
            cont.CR_GMAT__c=UpdateCon.CR_GMAT__c;
            cont.WCT_Type_of_Hire__c=UpdateCon.WCT_Type_of_Hire__c;
            //cont.WCT_Job_Type__c=UpdateCon.WCT_Job_Type__c;
            cont.WCT_Requisition_Number__c=UpdateCon.WCT_Requisition_Number__c;
            cont.Additional_Email__c=UpdateCon.Additional_Email__c;
            cont.WCT_Function__c=UpdateCon.WCT_Function__c;
            cont.WCT_RMS_Step__c=UpdateCon.WCT_RMS_Step__c;
            cont.WCT_Taleo_Status__c=UpdateCon.WCT_Taleo_Status__c;
            cont.US_Meets_Basic_Qualifications_date__c=UpdateCon.US_Meets_Basic_Qualifications_date__c;
            cont.Interview_1_to_be_Scheduled_Date__c=UpdateCon.Interview_1_to_be_Scheduled_Date__c;
            cont.WCT_Interview_1_Conducted_Date__c=UpdateCon.WCT_Interview_1_Conducted_Date__c;
            cont.WCT_Status_after_Interview_1__c=UpdateCon.WCT_Status_after_Interview_1__c;
            cont.WCT_Interview_2_Conducted_Date__c=UpdateCon.WCT_Interview_2_Conducted_Date__c;
            cont.WCT_Status_after_Interview_2__c=UpdateCon.WCT_Status_after_Interview_2__c;
            cont.WCT_Send_RMS_Rejection_Notification__c=UpdateCon.WCT_Send_RMS_Rejection_Notification__c;
            cont.WCT_Application_Initiated_Date__c=UpdateCon.WCT_Application_Initiated_Date__c;
            cont.WCT_Application_Status__c=UpdateCon.WCT_Application_Status__c;
            cont.WCT_Application_Completion_Date__c=UpdateCon.WCT_Application_Completion_Date__c;
            cont.WCT_Travel_Required__c=UpdateCon.WCT_Travel_Required__c;
            cont.WCT_Type_of_Event__c=UpdateCon.WCT_Type_of_Event__c;
            cont.WCT_Last_Day_of_Travel__c=UpdateCon.WCT_Last_Day_of_Travel__c;
            cont.WCT_LPX_Code__c=UpdateCon.WCT_LPX_Code__c;
            cont.WCT_Host_Recruiter__c=UpdateCon.WCT_Host_Recruiter__c;
            cont.WCT_Notes__c=UpdateCon.WCT_Notes__c;            
            cont.WCT_Hiring_Location__c=UpdateCon.WCT_Hiring_Location__c;
            cont.WCT_Internship_Start_Date__c=UpdateCon.WCT_Internship_Start_Date__c;
            cont.WCT_Start_Date_Year__c=UpdateCon.WCT_Start_Date_Year__c;
            cont.WCT_Length_of_Internship__c=UpdateCon.WCT_Length_of_Internship__c;
            cont.WCT_Salary__c=UpdateCon.WCT_Salary__c;
            cont.WCT_Sign_On_Bonus__c=UpdateCon.WCT_Sign_On_Bonus__c;
            cont.WCT_Relocation_Amt__c=UpdateCon.WCT_Relocation_Amt__c;
            cont.WCT_Offer_Decision_Deadline_Date__c=UpdateCon.WCT_Offer_Decision_Deadline_Date__c;
            cont.WCT_NextGen_Amount__c=UpdateCon.WCT_NextGen_Amount__c;
            cont.WCT_Hired_By__c=UpdateCon.WCT_Hired_By__c; 
            cont.WCT_Promisory_Note_amount__c=UpdateCon.WCT_Promisory_Note_amount__c;
            cont.WCT_Transition_Assistance_Amount__c=UpdateCon.WCT_Transition_Assistance_Amount__c;
            cont.WCT_Offer_Letter_Template__c=UpdateCon.WCT_Offer_Letter_Template__c; 
            cont.WCT_Other_Offers__c=UpdateCon.WCT_Other_Offers__c; 
            cont.WCT_Hiring_Partner__c=UpdateCon.WCT_Hiring_Partner__c; 
            cont.WCT_State_of_Hiring_Location__c = UpdateCon.WCT_State_of_Hiring_Location__c;
            cont.WCT_RequisitionHiringLevel__c=UpdateCon.WCT_RequisitionHiringLevel__c; 
            if(UpdateCon.WCT_Entity__c != '') {
                cont.WCT_Entity__c=UpdateCon.WCT_Entity__c; 
            }
            if(UpdateCon.WCT_Recruiter_Phone__c != '') {
                cont.WCT_Recruiter_Phone__c=UpdateCon.WCT_Recruiter_Phone__c; 
            }    

        /*Step 3 B : Deleting the Contact of the existing contact from the newlistmap. By this at the end of this loop, newlistmap will contain only the contact not present in the SFDC.*/
            newlistmap.remove(cont.WCT_Taleo_Id__c);
       //  }  
            
      }
        
        /*Step 4 : Now 'existingConts' has contacts record present in SFDC with modified values from the uploaded files. Now update these contact into SFDC.*/
           if(existingConts.size()>0)
           {    System.debug('existingConts'+existingConts);
                update existingConts;
           }
           
        /*Step 5 : Generating the report for End User.
         *        1. Records do not exist : Size of newlistmap. Hint :: Since all existing records are been removed from newlistmap.
         *        2. Records successfully updated : 'totalrecords' (total records in Uploaded file) - (Records do not exist).
         *        3. Update the wrapper class with non existing contact details.
        */
        
        
        system.debug('NORMSID::'+totalunsuccessrec);
       
        totalsuccessreccount=existingConts.size();
         system.debug('NORMSID::'+totalsuccessreccount);
        Set<String> rmsids=newlistmap.keySet();
        system.debug('NORMSID'+rmsids);
        
        List<Contact> NoRMSID = [SELECT Id,LastName,FirstName,WCT_Taleo_Id__c,WCT_Home_Street__c,WCT_Home_Street_2__c,WCT_Home_City__c,WCT_Home_State__c,WCT_Home_Zip__c,Email,WCT_Gender__c,MobilePhone,HomePhone,recordtypeid FROM Contact where WCT_Taleo_Id__c in :rmsids AND recordtypeid IN :recTypes];
        system.debug('NORMSIDlidt'+NoRMSID );
        Set<String> rmsids1= new Set<String>();
        newlistmap1= new Map<String, Contact>();
        for (Contact cont: NoRMSID)
        {
        rmsids1.add(cont.WCT_Taleo_Id__c);
        newlistmap1.put(cont.WCT_Taleo_Id__c,cont);
        }
        system.debug('NORMSID11::'+newlistmap);
        
        totalNoRMSID = NoRMSID.size();
        system.debug('NORMSID::'+totalNoRMSID);
        
        for (Contact cont: NoRMSID)
        {
        newlistmap.remove(cont.WCT_Taleo_Id__c);
        }
        totalunsuccessrec=newlistmap.size();
        system.debug('NORMSID11::'+newlistmap);
        
        
       
        for(String rmsid : rmsids){
        system.debug('rmsids::'+rmsids);
            Contact c=newlistmap.get(rmsid);
            
          wrapper.add(new MyWrapper(c.WCT_Taleo_Id__c, c.LastName, c.FirstName, c.WCT_Home_Street__c, c.WCT_Home_Street_2__c, c.WCT_Home_City__c, c.WCT_Home_State__c,c.WCT_Home_Zip__c, c.HomePhone, c.MobilePhone, c.WCT_Gender__c, c.Email));
        }
        
        for(String rmsid : rmsids1){
        system.debug('rmsids22::'+rmsids1);
            Contact c1=newlistmap1.get(rmsid);
            system.debug('rmsids2::'+newlistmap1);
            wrapper1.add(new MyWrapper(c1.WCT_Taleo_Id__c, c1.LastName, c1.FirstName, c1.WCT_Home_Street__c, c1.WCT_Home_Street_2__c, c1.WCT_Home_City__c, c1.WCT_Home_State__c,c1.WCT_Home_Zip__c, c1.HomePhone, c1.MobilePhone, c1.WCT_Gender__c, c1.Email));
        }
                        
        try
        {
            for(string s:conNamesr)
            {
                if(conNamesf.contains(s))
                {
                  conNamesf.remove(s);     
                }
            }
        }
        catch (Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Contact Email already exist, change Contact Email and try again');
            ApexPages.addMessage(errormsg);
        }
    
        return null; 
   }     
    
    
    public List<Contact> getuploadedAccounts()
    {
        list<contact> lcon1;
            if (Conlist!= NULL)    
                if (Conlist.size() > 0) { conlist.clear(); return conlist; }
                            
                else
                 return null;               
                else
                return null;
    }  
    
    public pagereference pg()
    {
        return page.ViewFailedContactsInUI;
    }
    
    public class MyWrapper
    {
         public string WCT_Taleo_Id1 {get; set;}
         public string LastName1 {get; set;} 
         public string FirstName1 {get; set;}
         public string WCT_Home_Street1 {get; set;}
         public string WCT_Home_Street_21 {get; set;}
         public string WCT_Home_City1  {get; set;}
         public string WCT_Home_State1 {get; set;}
         public string WCT_Home_Zip1 {get; set;}
         public string HomePhone1 {get; set;}
         public string MobilePhone1 {get; set;}
         public string WCT_Gender1 {get; set;}
         public string Email1 {get; set;}
                          
         public MyWrapper(string WCT_Taleo_Id, String LastName, String FirstName, String WCT_Home_Street, String WCT_Home_Street_2, String WCT_Home_City, 
                          String WCT_Home_State,String WCT_Home_Zip, String HomePhone, String MobilePhone, String WCT_Gender, String Email)
         {
                WCT_Taleo_Id1 = WCT_Taleo_Id;
                LastName1 = LastName;
                FirstName1 = FirstName;
                WCT_Home_Street1 = WCT_Home_Street;
                WCT_Home_Street_21 = WCT_Home_Street_2;
                WCT_Home_City1 = WCT_Home_City;
                WCT_Home_State1 = WCT_Home_State;
                WCT_Home_Zip1 = WCT_Home_Zip;
                HomePhone1 = HomePhone;
                MobilePhone1 = MobilePhone;
                WCT_Gender1 = WCT_Gender;
                Email1 = Email;
        }
    }    
}