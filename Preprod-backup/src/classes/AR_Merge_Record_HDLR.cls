/***
Description : 
	Read the Request parameters.
	Method to Get the set of fields from custom settings
	Method to Get the extra fields needed from the real class using this.
	Method to Query the object with above set of fields and objectType assign the sobject to wrapper class.
**/


public class AR_Merge_Record_HDLR {

    public RecordWrapper recordDest{get; set;}
    public RecordWrapper recordSrc{get; set;}
    public string sourceRecordName{get;set;}
    public string destinationRecordName{get;set;}
    public List<MergeFieldsWrapper> mergeFieldsWR{get; set;}
    public List<String> extrasfields;
    public List<WCT_List_Of_Names__c> srcDefaultsValueFields;
    public List<WCT_List_Of_Names__c> destDefaultsValueFields;
    public List<WCT_List_Of_Names__c> sourceFieldToField;
    public WCT_List_Of_Names__c staggingRecordNames;
    public string  staggingRecordField;
   // public List<sObject> sobjects{get; set;}
    public boolean isError{get;set;}
    public string  errorMessage{get;set;}
    public string  activityHistory{get;set;}
    public boolean islimitReached {get; set;}
    public static final integer MF_TYPE_SOURCE_TO_DESTINATION=1;
    public static final integer MF_TYPE_SOURCE_DEFAULT_VALUE=2;
    public static final integer MF_TYPE_DESt_DEFAULT_VALUE=3;
    public static final integer MF_TYPE_STAGGING_RECORD_ID=4;
    public static final integer MF_TYPE_SOURCE_FIELD_To_FIELD=6;
    public static final integer MF_TYPE_STAGGING_RECORD_NAMES=7;
    public static final integer MF_TYPE_RELATED_LIST=5;
    public  final string MF_FIELD_TYPE_REFERENCE{get;set;}
    public boolean mergeRelatedList{get;set;}
    
    public string mergeConfigName{get; set;}
    	
    /*Record Backup*/
    public Map<string, Object > sourceBackup;
    public Map<string, Object > destinationBackup;
    public List<RelatedListBackup> relatedListBackups;
    public WCT_sObject_Staging_Records__c recordMergeStagging; 
    public List<RelatedListWrapper> relatedLists{get;set;}
    public List<WCT_List_Of_Names__c> validRelatedList{get;set;}
    
    public string objectType;
    
    public AR_Merge_Record_HDLR(string sobjectType, string sourceName, string sourceId , string destName , string destId , string configName)
    {
        System.debug(' AR_Merge_Record_HDLR Started ');
        /*Intialize all the variables used in this process.*/
          initialize();
          MF_FIELD_TYPE_REFERENCE='REFERENCE';
          objectType=sobjectType;
        /*Ids of the Records to be merge*/
          recordSrc.idString = sourceId;
          recordDest.idString = destId ;
          sourceRecordName=sourceName;
          destinationRecordName=destName;
          mergeConfigName=configName;
          preProcessForMerge();
        System.debug(' AR_Merge_Record_HDLR Ended ');
    }
    
    public AR_Merge_Record_HDLR()
    {
        /*Intialize all the variables used in this process.*/
        initialize();
          MF_FIELD_TYPE_REFERENCE='REFERENCE';
          objectType=ApexPages.currentPage().getParameters().get('objectType');
         /*Name of the Records to be merge*/
          sourceRecordName=ApexPages.currentPage().getParameters().get('srcName')!=null?ApexPages.currentPage().getParameters().get('srcName'):'';
          destinationRecordName=ApexPages.currentPage().getParameters().get('destName')!=null?ApexPages.currentPage().getParameters().get('destName'):'';
        /*Ids of the Records to be merge*/
          recordSrc.idString = ApexPages.currentPage().getParameters().get('srcId');  
          recordDest.idString = ApexPages.currentPage().getParameters().get('destId');
          mergeConfigName=Label.GBL_Merge_Config_Name_Manual;
        preProcessForMerge();
          
    }
    
    public void initialize()
    {
        relatedListBackups= new List<RelatedListBackup>();
        isError=false;
        errorMessage='';
        islimitReached=false;
         /*Interemediate data process variables.*/
        mergeFieldsWR=new List<MergeFieldsWrapper>();
        srcDefaultsValueFields= new List<WCT_List_Of_Names__c>();
        destDefaultsValueFields= new List<WCT_List_Of_Names__c>();
        sourceFieldToField= new List<WCT_List_Of_Names__c>();
        staggingRecordField='';
        /*Bacup intial declaration*/
        recordMergeStagging= new WCT_sObject_Staging_Records__c();
        recordMergeStagging.RecordTypeId=Label.Merge_Record_Stagging_Record_Type_ID;
        sourceBackup= new Map<string, Object >();
        destinationBackup= new Map<string, Object >();
        /*The two records id to be merged*/
        recordDest = new RecordWrapper();
        recordSrc = new RecordWrapper();
        /*Initalization of Variables*/
        mergeRelatedList=true;
        
        validRelatedList= new List<WCT_List_Of_Names__c>();
}
    
    public void preProcessForMerge()
    {
        
        System.debug('### preProcessForMerge');
        
       /*Activity Log intialization*/
        activityHistory=string.valueOfGmt(System.now());
        activityHistory=activityHistory+' Merging Started;; \n';
       
       /*Record Merge Stagging Record*/
        recordMergeStagging.MR_Source_Record_Id__c=recordSrc.idString;
        recordMergeStagging.MR_Destination_Record_Id__c=recordDest.idString;
        /*API name of the object to merge */
      
        System.debug('### preProcessForMerge 2');
        recordMergeStagging.MR_Object_Type__c=objectType;
        if(validateRequest())
        {
            System.debug('### preProcessForMerge 3');
            queryMergeFields();
            if(mergeFieldsWR.size()>0)
            {
                System.debug('### preProcessForMerge 4');
                activityHistory=activityHistory+' STEP: Merge Fields Merged ;;\n';
                List<sObject> sobjects=querysObject();
                System.debug('### preProcessForMerge 5');
                
                if(sobjects.size()==2)
                {
                    System.debug('### preProcessForMerge 6');
                    activityHistory=activityHistory+' STEP: Source and Destination records quried ;;\n';
                    for(sobject tempobject:sobjects)
                    {
                        if(tempobject.id==recordDest.idString)
                        {
                            recordDest.record=tempobject;
                        }
                        if(tempobject.id==recordSrc.idString)
                        {
                            recordSrc.record=tempobject;
                        }
                    }
                    
                      /* Finding the Related List Details */
       				relatedLists= getRelatedListDetails(sobjects[0]);
                }
                else
                {
                     isError=true;
                	activityHistory=activityHistory+' ERROR: No records ;;\n';
                    errorMessage='No records';
                }
                
               
             }
            else
            {
                isError=true;
                activityHistory=activityHistory+' ERROR: No fields are configured to merge ;;\n';
                errorMessage='No fields are configured to merge';
            }
        }
         
       
    }
    
    public PageReference saveStagingRecord()
    {
        System.debug('## saveStagingRecord'+activityHistory);
        System.debug('## saveStagingRecord'+activityHistory);
        
        if(isError==true)
        {
            recordMergeStagging.MR_Activity_History__c=activityHistory;
           	recordMergeStagging.MR_Merged_Status__c='Failed';
        }
        else
        {
            recordMergeStagging.MR_Merged_Status__c='Processing';
            
            if(sourceRecordName!=null && sourceRecordName!='')
            {
               recordMergeStagging.MR_Source_Record_Name__c=sourceRecordName;
            }
            else
            {
              recordMergeStagging.MR_Source_Record_Name__c= staggingRecordNames!=null?(String) recordSrc.record.get(staggingRecordNames.MR_Field_API_Name__c):'';
            }
            
            if(destinationRecordName!=null && destinationRecordName!='')
            {
               recordMergeStagging.MR_Destination_Record_Name__c=destinationRecordName;
            }
            else
            {
              recordMergeStagging.MR_Destination_Record_Name__c=  staggingRecordNames!=null?(string) recordDest.record.get(staggingRecordNames.MR_Field_API_Name__c):'';
            }
            
            
       		 
        }
        
		recordMergeStagging.MR_Merge_Config_Name__c=mergeConfigName;
        if(recordMergeStagging.id!=null)
        {
            update recordMergeStagging;
        }
        else
        {
        	insert recordMergeStagging;
        }
        return null;
    }
    public PageReference mergeRecord()
    {
        boolean isValidUpdate=false;
        if(!isError)
        {
            if(mergeFieldsWR.size()>0)
            {
                activityHistory=activityHistory+' STEP: Merging source and destination records details ;;\n';
				for(MergeFieldsWrapper temp : mergeFieldsWR)
                {
                    /* User selected this field to use in merge.*/
                    if(temp.include==true)
                    {
                        destinationBackup.put(temp.mergeField.MR_Field_API_Name__c, recordDest.record.get(temp.mergeField.MR_Field_API_Name__c));
                        if(temp.mergeField.MR_Is_Append__c==true)
                        {
                            string record1vlaue=(string)recordDest.record.get(temp.mergeField.MR_Field_API_Name__c);
                            string record2vlaue=(string)recordSrc.record.get(temp.mergeField.MR_Field_API_Name__c);
                            record1vlaue=(string.isBlank(record1vlaue))?'':destinationRecordName+' : \n'+record1vlaue;
                            record1vlaue=(string.isBlank(record2vlaue))?record1vlaue:record1vlaue+'\n\n'+sourceRecordName+' : \n'+record2vlaue;
                            recordDest.record.put(temp.mergeField.MR_Field_API_Name__c,record1vlaue);
                        }
                        else
                        {
                            recordDest.record.put(temp.mergeField.MR_Field_API_Name__c,recordSrc.record.get(temp.mergeField.MR_Field_API_Name__c));
                        }
                     	isValidUpdate=true;
                    }
                 }
                
                for(WCT_List_Of_Names__c sourceFieldBackup :  sourceFieldToField)
                {
                    recordSrc.record.put(sourceFieldBackup.MR_Field_API_Name__c,recordSrc.record.get(sourceFieldBackup.MR_New_Value__c));  
                }

                
                for(WCT_List_Of_Names__c srcDefaultValue : srcDefaultsValueFields)
                {
                    /*Source Backup */
                    object value = parseStringToObject(srcDefaultValue.MR_New_Value__c);
                    sourceBackup.put(srcDefaultValue.MR_Field_API_Name__c, recordSrc.record.get(srcDefaultValue.MR_Field_API_Name__c));
                    recordSrc.record.put(srcDefaultValue.MR_Field_API_Name__c,value);  
                }
                
                for(WCT_List_Of_Names__c destDefaultValue : destDefaultsValueFields)
                {
                    /*Destination Backup */
                    object value = parseStringToObject(destDefaultValue.MR_New_Value__c);
                    destinationBackup.put(destDefaultValue.MR_Field_API_Name__c, recordDest.record.get(destDefaultValue.MR_Field_API_Name__c));
                  	recordDest.record.put(destDefaultValue.MR_Field_API_Name__c,value);  
                }
                
                
                
                if(staggingRecordField!='')
                {
                    recordSrc.record.put(staggingRecordField,recordMergeStagging.id);  
                    recordDest.record.put(staggingRecordField,recordMergeStagging.id);  
                }
            }
            
        }
        if(isValidUpdate==true)
        {
            List<sObject> sobjects= new List<sObject>();
            List<sObject> sobjectsDest= new List<sObject>();
            sobjects.add(recordSrc.record);
            sobjectsDest.add(recordDest.record);
            system.debug('#######3'+sobjects);
            List<Database.SaveResult> result1=Database.update(sobjects);
           List<Database.SaveResult> result2= Database.update(sobjectsDest);
            activityHistory=activityHistory+' STEP:  source and destination records are merged ;;\n';
            recordMergeStagging.MR_Destination_Record_Backup__c=JSON.serialize(destinationBackup);
         	recordMergeStagging.MR_Source_Record_Backup__c=JSON.serialize(sourceBackup);
            if(!result1[0].isSuccess())
            {
                recordMergeStagging.MR_Error_Message__c=String.valueOf(result1[0].getErrors());
            }
            if(!result2[0].isSuccess())
            {
                recordMergeStagging.MR_Error_Message__c=String.valueOf(result2[0].getErrors());
            }
            /*Related List Merging. */
            mergeRelatedList();
           }
         return null;
    }
    
    public object parseStringToObject(string value)
    {
        object valueObject;
        if(value=='false')
        {
           valueObject=false;
        }
        else if(value=='true')
        {
           valueObject=true;
        }
        else
        {
            valueObject=value;
        }
        return valueObject;
    }
    public boolean validateRequest()
    {
		
	  if(recordDest.idString==null || recordDest.idString=='')
        {
            errorMessage='Invalid request. Missing mandatory details : Destination Record is mandatory';
            isError=true;
        }
      if(recordSrc.idString==null || recordSrc.idString=='')
        { 
            errorMessage=(errorMessage!=''?errorMessage+', Source Record is mandatory':'ERROR: Invalid request. Missing mandatory details :  Source Record is mandatory'); 
            isError=true;
        }
      if(objectType==null || objectType=='')
        { 
           errorMessage=(errorMessage!=''?errorMessage+', objectType is mandatory':'ERROR: Invalid request. Missing mandatory details :  objectType is mandatory'); 
            isError=true;
        }
        activityHistory=activityHistory+(errorMessage==''?'':'Error :'+errorMessage+' ;; \n');
            return !isError;
    }
    
    /*
		Query the metadata from List of Names, which is configured.
		Which fields need to be merged.
		How the merge of fields should be merged.
       
    */
    public void queryMergeFields()
    {
       	String query='Select Name, MR_Field_API_Name__c, MR_Is_Append__c, MR_Object_API_Name__c, MR_Object_Type__c, MR_New_Value__c From WCT_List_Of_Names__c where MR_Object_API_Name__c= \''+objectType+'\''+' and  MR_Merge_Config_Name__c =\''+mergeConfigName+'\'';
        List<WCT_List_Of_Names__c> mergeFields= Database.query(query);
        
       //Schema.DescribeSObjectResult[] descResult = Schema.describeSObjects(new String[]{objectType});
       
       /*
        *  Describe the metadata of the Object Type about to merge.
        *  Describe each field which is configured in the List of Names.
        */
       Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
       Schema.sObjectType objType = globalDescription.get(objectType); 
       
       if(objType!=null)
        {
           Schema.DescribeSObjectResult r1 = objType.getDescribe(); 
           Map<String , Schema.SObjectField> mapFieldList = r1.fields.getMap();
            
                      
            for(WCT_List_Of_Names__c tempMergefield :mergeFields )
            {
            
               if(tempMergefield.MR_Object_Type__c==MF_TYPE_SOURCE_TO_DESTINATION )
               {
               system.debug('###'+tempMergefield.MR_Field_API_Name__c);   
			   Schema.SObjectField fieldDetails=mapFieldList.get(tempMergefield.MR_Field_API_Name__c);
               Schema.DescribeFieldResult dfr=fieldDetails.getDescribe();
                   
               system.debug(''+dfr);
                MergeFieldsWrapper mergeField= new MergeFieldsWrapper();
                mergeField.include=false;
                mergeField.mergeField=tempMergefield;
                mergeField.FieldLabel=dfr.getLabel();
                mergeField.FieldType=dfr.getType().Name();
                mergeField.displayField=getDisplayField(dfr);
                mergeFieldsWR.add(mergeField);
               }
              else if (tempMergefield.MR_Object_Type__c== MF_TYPE_SOURCE_DEFAULT_VALUE)
               {
                 srcDefaultsValueFields.add(tempMergefield); 
               }
             else if (tempMergefield.MR_Object_Type__c== MF_TYPE_DEST_DEFAULT_VALUE)
               {
                 destDefaultsValueFields.add(tempMergefield); 
               }
             else if(tempMergefield.MR_Object_Type__c== MF_TYPE_SOURCE_FIELD_To_FIELD)
             {
                 sourceFieldToField.add(tempMergefield);
             }
             else if(tempMergefield.MR_Object_Type__c== MF_TYPE_STAGGING_RECORD_ID)
               {
                 staggingRecordField=tempMergefield.MR_Field_API_Name__c;
               }
              else if(tempMergefield.MR_Object_Type__c==MF_TYPE_RELATED_LIST)
              {
                  validRelatedList.add(tempMergefield);
              }
             else if(tempMergefield.MR_Object_Type__c==MF_TYPE_STAGGING_RECORD_NAMES)
              {
                    staggingRecordNames=tempMergefield;
              }
                    
            }
        }
    }
    
    /*
     * Define the field to Query to get display name of the REFERENCE Type : Lookup or Master Details.
     * If the Field is of Reference Type then field with display name (name to show it to user) will be "__r.Name " .
	 */
    public string getDisplayField(Schema.DescribeFieldResult dfr)
    {
        string displayName='';
        if(dfr.getType().name()==MF_FIELD_TYPE_REFERENCE)
        {
           displayName=dfr.getName().replace('__c','__r');
           displayName=displayName+'.Name';
        }
        
        return displayName;
    }
    
    public List<sobject> querysObject()
    {
        /* START : Gettting all required parameters to be quried, 
         * Parameters are combination of
         * 	 mergeFieldsWR          - user will select the params to merge from source
         * 	 
         */
        
        /*	 
         * Converting to "set" to avoid duplicate fields in query.
		 */
        System.debug('# querysObject');
        set<String> allParametersToQuery= new Set<String>();
		/*	
         * mergeFieldsWR          - user will select the params to merge from source
         */
        for(MergeFieldsWrapper tempFields: mergeFieldsWR)
        {
            allParametersToQuery.add(tempFields.mergeField.MR_Field_API_Name__c);
            if(tempFields.FieldType==MF_FIELD_TYPE_REFERENCE && tempFields.displayField!='')
            {
                allParametersToQuery.add(tempFields.displayField);
            }
        }
        
        System.debug('# querysObject');
        
        /*
         * srcDefaultsValueFields - default value to the source record.
		*/
        for(WCT_List_Of_Names__c mergeField: srcDefaultsValueFields)
        {
            allParametersToQuery.add(mergeField.MR_Field_API_Name__c);
        }
        /*
         * destDefaultsValueFields - default value to the destination record. 
		*/
        for(WCT_List_Of_Names__c mergeField: destDefaultsValueFields)
        {
            allParametersToQuery.add(mergeField.MR_Field_API_Name__c);
        }
        
        /*
         * Source and Destination Names in Stagging Record, Basically the Expected Field name is either name or Formula field with different combi as required.
         * 
		*/
        if(staggingRecordNames!=null)
        {
            allParametersToQuery.add(staggingRecordNames.MR_Field_API_Name__c);
        }
        
        /*
         * END - Gettting the parameters to be quried, 
		 */
              
        /*Build the Query */
        List<sobject> sobjects= new List<sobject>();
        string dynamicQuery='Select id';
        
        dynamicQuery=dynamicQuery+AR_Merge_Record_HDLR.setToCSV(allParametersToQuery);
        dynamicQuery=dynamicQuery+' From '+objectType;
        dynamicQuery=dynamicQuery+' Where id in ( \''+recordDest.idString+'\',\''+recordSrc.idString+'\')';
        system.debug('###########Query For Source And DEst REcords'+dynamicQuery);
        try
        {
        	sobjects = Database.query(dynamicQuery);
        }
        catch(Exception e)
        {
            
        }
        return sobjects;
    }
    
   public static string setToCSV(Set<String> paramsToQuery)
    {
        string commaSeplist='';
        for(String tempParam: paramsToQuery)
        {
            commaSeplist=commaSeplist+','+tempParam;
        }
        return commaSeplist;
    }
    /*
	@desc The Wrapper class to hold the Source and destination Record and it details.
	*/	
    public  class RecordWrapper 
    {
       	public  string idString {get;set;}
        public sObject record {get;set;}
        /*The possible values for the */
        public string recordType {get;set;}
    }
    public class MergeFieldsWrapper
    {
        public boolean include{get;set;}
        public WCT_List_Of_Names__c mergeField{get;set;}
        public string FieldLabel{get;set;}
        public string FieldType{get;set;}
        public string displayField{get;set;}
        
    }
    
    /*
     * @desc The Wrapper class to hold the related List details.
     * fieldAPIName - Field API Name of the field which is lookup field (to parent ) in the Child object.
     * fieldLabel - Field Label of field which is lookup field (to parent ) in the Child object.
     * relationshipName - 
	*/
    public class RelatedListWrapper 
    {
        public boolean include{get;set;}
        public Schema.SObjectField field {get;set;}
        public string relationshipName {get;set;}
        public string fieldLabel {get;set;}
        	
        public string fieldAPIName{get;set;}
        public string sObjectName {get;set;}
        public string sObjectAPIName {get;set;}
        
    }
    
    public List<RelatedListWrapper> getRelatedListDetails(sObject objectType)
    {
        List<RelatedListWrapper> relatedListWRs= new List<RelatedListWrapper>();
        Schema.DescribeSObjectResult dsr = objectType.getSObjectType().getDescribe();
        List<Schema.ChildRelationship> clientRelaions = dsr.getChildRelationships();
       	system.debug('###################### :'+clientRelaions);
        
       
        
        for(Schema.ChildRelationship clientRelation : clientRelaions)
        {
            if(validateRelatedList(clientRelation))
            {
                RelatedListWrapper tempRelatedWrapper = new RelatedListWrapper();
                tempRelatedWrapper.include=true;
                string relationshipName=clientRelation.getRelationshipName();
                /*Making relationShip name viewable */
                relationshipName=relationshipName.replace('__r', '');
                relationshipName=relationshipName.replace('_', ' ');
                tempRelatedWrapper.relationshipName=relationshipName;
                
                tempRelatedWrapper.fieldLabel= clientRelation.getField().getDescribe().getLabel();
                tempRelatedWrapper.fieldAPIName=String.valueOf(clientRelation.getField());
                tempRelatedWrapper.sObjectName = clientRelation.getChildSObject().getDescribe().getLabel();
                tempRelatedWrapper.sObjectAPIName = clientRelation.getChildSObject().getDescribe().getName();
                relatedListWRs.add(tempRelatedWrapper);
            }
        }
        return relatedListWRs;
    }
    /*
     * Inputs : ChildRelationship object
     * desc   : Logic to check if child relationship is valid based on following  critaria. 
     * 			1. Relationship Name is not empty.
     * 			2. Child object is querable and updateble. 
     * 			3. If validRelatedList has any item i.e.., for this object the valid related lists are configured. 
     * 				Hence only the related list once confugured are allowed.
     * 			   If validRelatedList is empty 
     * Return : True - if valid  / False - if invalid.
	*/
    
   
    public boolean validateRelatedList(Schema.ChildRelationship clientRelation)
    {
        boolean isValid=true;
        /* @Rule :  RelationShip name is not empty */
        if(clientRelation.getRelationshipName()=='' || clientRelation.getRelationshipName()==null )
        {
            isValid=false;
        }
       
        if(validRelatedList.size()>0 && isValid==true)
            {
                boolean isExisting=false;
                string relationshipName=clientRelation.getRelationshipName().replace('__r','');
                for(WCT_List_Of_Names__c mergeField: validRelatedList)
                {
            	 if(mergeField.MR_Field_API_Name__c.equals(relationshipName))
                   {
                       isExisting=true;
                   }
                }
                isValid=isExisting;
            }
        
        if(isValid)
            {
              Schema.DescribeSObjectResult childsObjectResult= clientRelation.getChildSObject().getDescribe();
              boolean isOperatable = (childsObjectResult.isQueryable()&&childsObjectResult.isUpdateable())&& (!childsObjectResult.isDeprecatedAndHidden());
              isValid=isOperatable;
                
            }
            
        return isValid;
    }
    
    public void mergeRelatedList()
    {
        
        /*This list is supposed to be handle by future call due to limits in the execution limits.*/
        List<RelatedListWrapper> futureHandleList= new List<RelatedListWrapper>();
        /*Backup of the related List */
        List<Map<string,string>> relatedListBackup= new List<Map<string,string>>();
       	for( RelatedListWrapper tempRelatedWrapper :relatedLists)
        {
           try
            {
                /* Check if the limit is already reached or not. */
               if(islimitReached==false)
                {
                   
                    List<sObject> sobjects=queryRelatedList(tempRelatedWrapper.sObjectAPIName,tempRelatedWrapper.fieldAPIName, recordSrc.idString);
                  
                    /* 
                     * Check if the limit will exceded with this set of related list records. 
                     * If this list will exceeds limit then, islimitReached is set true, and now the process should be handled by future call.
					 */
                     if(willLimitExceed(sobjects.size())==false )
                        {
                            AR_Merge_Record_HDLR.updateRelatedList(sobjects,tempRelatedWrapper.fieldAPIName,recordDest.idString);
                            /*
                             * Backing up the details. 
							 */
                            List<string> ids= new List<string>();
                            for(sObject sobjectTemp:sobjects)
                            {
                            	ids.add((string)sobjectTemp.id);
                            }
                            if(ids.size()>0) relatedListBackups.add(new RelatedListBackup(ids, tempRelatedWrapper.fieldAPIName,tempRelatedWrapper.sObjectName));
                        }
                }
                /*
                 * handle islimitReached scenario where the activities should be done in future context. 
				 */
                if(islimitReached==true)
                {
                     futureHandleList.add(tempRelatedWrapper);
                }
                
                                
            }
            catch (Exception ex)
            {
                /*
                 * 
                 * 1. Invalid soql query - ignore and go  to next related list
				*/
            }
        }
       
        if(futureHandleList.size()>0)
        {
            List<String> objectNames= new List<String>();
            List<String> apiFieldNames= new List<String>();
            for(RelatedListWrapper tempRelatedWrapper: futureHandleList)
            {
                objectNames.add(tempRelatedWrapper.sObjectAPIName);
                apiFieldNames.add(tempRelatedWrapper.fieldAPIName);
            }
            /*
             * Requesting the future to handle the request. 
			 */
            activityHistory=activityHistory+' STEP : Future context for related List is intiated';
            asyncMergeRelatedList(objectNames,apiFieldNames,recordSrc.idString,recordDest.idString,recordMergeStagging.id);
        }
        else
        {
           /*Merge Status only when all related List are handle. ie..., no future context for related list is required.*/
            recordMergeStagging.MR_Merged_Status__c='Merged';
        }
        
        
       
        recordMergeStagging.MR_Activity_History__c=activityHistory;
        recordMergeStagging.MR_Merged_Date__c= system.now();
        recordMergeStagging.MR_Related_List_Backup__c=JSON.serialize(relatedListBackups);
        update recordMergeStagging;
    }
    
    /*
     * Wrapper class for the Related List Backup JSON object.
	 */
    public class RelatedListBackup
    {
        public List<string> idValue{get;set;}
        public string apiFieldName{get;set;}
        public string objectName{get;set;}
        public RelatedListBackup(List<string> idValueTemp, string apiFieldNameTemp, string objectNameTemp)
        {
            idValue=idValueTemp;
            apiFieldName=apiFieldNameTemp;
            objectName=objectNameTemp;
            
        }
    }
    
    /*
     * 
     * @desc : This Method current instance limits and check if future request can be handled by this method or not. 
     *  	   Hint used to reduce the logic : In this scenarios, Every related list record query will be performed DML to update with new merged record id.So no need to check the query limit, as query limits are much greater than the DML record limits.
     * 		   If the 
	 */
    public boolean willLimitExceed(integer noOfRecordsToHandle)
    {
       boolean limitExceed=false;
       
            /* 
             * Limit will exceed, if noofRecordToHandle is greater than available Limit.
             * OR
             * Available DML Statement Limit is Zero.
			 */
            limitExceed=(noOfRecordsToHandle > Limits.getLimitDMLRows()) ;

       return limitExceed;
    }
    
    /*
     * Input : tempRelatedWrapper - RelatedListWrapper, srcRecordId- source Record ID.
     * @desc : Static Utiltiy method to build the query for relatedList query the record.
     * 
	*/
    public static List<sObject> queryRelatedList(string objectName, string fieldApiName, string srcRecordId) 
    {
        List<sObject> relatedListRecords=new List<sObject>();
          /*Generate the Query for the RelatedList details */
        string dynoQuery='Select id, '+fieldApiName+' From '+objectName+' where '+fieldApiName+'= \''+srcRecordId+'\''+' Limit '+Limits.getLimitQueryRows();
        system.debug('#############'+dynoQuery);
        relatedListRecords= Database.query(dynoQuery);
        system.debug('#############'+relatedListRecords);
        
        return relatedListRecords;
    }
    
    /*
     * Inputs : relatedListRecords - quried related List , apiName - API Name of field, destRecordId - Destination Record ID.
     * desc   : modify the sobject records with destination id and update the values 
	 * 
	 */
    public static void updateRelatedList(List<sObject> relatedListRecords, string apiName, string destRecordId)
    {
        for(sObject relatedListRecord : relatedListRecords)
        {
            relatedListRecord.put(apiName, destRecordId);
        }
        
        if(relatedListRecords.size()>0)
            Database.update(relatedListRecords);
    }
    
    @future
    public static void asyncMergeRelatedList(List<String> sobjectNames, List<String> fieldNames, string srcRecordId, string destRecordId, string staggingId)
    {
        List<RelatedListBackup> futureRelatedListBackups = new List<RelatedListBackup>();
        List<WCT_sObject_Staging_Records__c> records=[select id, MR_Source_Record_Id__c,MR_Destination_Record_Id__c,MR_Future_Related_List_backup__c, MR_Merged_Date__c, MR_Activity_History__c,MR_Merged_Status__c From WCT_sObject_Staging_Records__c where id=:staggingId ];
       for(integer i=0; i<sobjectNames.size(); i++)
      	{
            try
            {
                List<sObject> sobjects=queryRelatedList(sobjectNames[i],fieldNames[i], srcRecordId);
    			AR_Merge_Record_HDLR.updateRelatedList(sobjects,fieldNames[i],destRecordId);
         		/*
                * Backing up the details. 
				*/
                List<string> ids= new List<string>();
                for(sObject sobjectTemp:sobjects)
                {
                    ids.add((string)sobjectTemp.id);
                }
                if(ids.size()>0) futureRelatedListBackups.add(new RelatedListBackup(ids, fieldNames[i],sobjectNames[i]));       
                
            }
            catch(Exception E)
            {
                /*Any Error Handling as required */
            }
        }
        
       /*Persist the future context related list details to stagging record.*/
        if(records.size()>0)
        {
            records[0].MR_Future_Related_List_backup__c= JSON.serialize(futureRelatedListBackups);
            records[0].MR_Merged_Status__c='Merged';
            records[0].MR_Activity_History__c=records[0].MR_Activity_History__c+' Future Related List :'+string.valueOfGmt(system.now())+' Future context related Lists are merged \n';
            update records;
        }
        
    }
    

    
       
}