public class AR_alljobs_cont 
{ 
    public List<Job__c> jobList{get;set;}
    public String query{get;set;}
    public String conditionIndustry{get;set;}
    public String conditionCategory{get;set;}
    public String conditionState{get;set;}
    public String conditionMetro{get;set;}
    public List<selectOption> getoptionlist{get;set;}
    public string selectindustryvalue{get;set;}
    public string selectCategoryValue{get;set;}
    public string selectStateValue{get;set;}
    public string selectMetroValue{get;set;}
    public string s;
    public boolean disresults{get;set;}
    //public string astring='Active';
    public string astring='Active';
    public string alrelations='Alumni Relations';
    public string alumnirels='\''+string.escapeSingleQuotes(alrelations)+'\'';
    public string astring1='\''+string.escapeSingleQuotes(astring)+'\'';
    public AR_alljobs_cont(){
    disresults=true;
    fetch();
    }
    public void setselectCategoryValue(String selectCategoryValue) {
                  this.selectCategoryValue = selectCategoryValue;
            }
    public void setselectindustryvalue(String selectindustryvalue) {
                  this.selectindustryvalue = selectindustryvalue;
            }        
    public void setselectStateValue(String selectStateValue) {
                  this.selectStateValue = selectStateValue;
            }
    public void setselectMetroValue(String selectMetroValue) {
                  this.selectMetroValue = selectMetroValue;
            }              
    public PageReference fetch(){
       astring='Active';
    alrelations='Alumni Relations';
    astring1='\''+string.escapeSingleQuotes(astring)+'\'';
    alumnirels='\''+string.escapeSingleQuotes(alrelations)+'\'';
        getoptionlist =new List<selectOption>();
        String SobjectApiName = 'Job__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(SobjectApiName).getDescribe().fields.getMap();
        System.debug('fieldMap ======'+fieldMap );
        String commaSepratedFields = '';
        for(String fieldName : fieldMap.keyset()){
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = fieldName;
                
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + fieldName;
            }
        }
        alumnirels=' RecordType.Name='+alumnirels+'and ';
        query = 'select id,Post_Date__c,AR_Reopen_Date__c,AR_Account_Industry__c ,Country__c,Location__c,Confidential__c,CCL_Company_posting_name_communications__c,AR_Nearest_Metropolitan_Area_1__c,AR_Nearest_Metropolitan_Area_2__c,AR_Nearest_Metropolitan_Area_3__c,name,industry__c,job_category__c,Account__c,AR_Primary_Location_State__c,Job_Id__c,AR_Primary_Location_City__c from Job__c Where'+ alumnirels +' Status__c ='+ astring1;
       
        jobList = Database.query(query);
        return null;
    }
    public list<selectoption> getindustrylistJob(){
        List<SelectOption> Options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult =
 Job__c.industry__c.getDescribe();
   List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
   for( Schema.PicklistEntry f : ple)
   {
      options.add(new SelectOption(f.getLabel(), f.getValue()));
   }
        return Options;
    }
    public list<selectoption> getCategorylist(){
List<SelectOption> options = new List<SelectOption>();
options.add(new SelectOption('Accounting/GAAP/Reporting','Accounting/GAAP/Reporting'));
options.add(new SelectOption('Administrative/Support Services','Administrative/Support Services'));
options.add(new SelectOption('Audit/Governance/Risk/Compliance','Audit/Governance/Risk/Compliance'));
options.add(new SelectOption('Finance','Finance'));
options.add(new SelectOption('HR','HR'));
options.add(new SelectOption('Legal','Legal'));
options.add(new SelectOption('Marketing','Marketing'));
options.add(new SelectOption('Operations','Operations'));
options.add(new SelectOption('Other','Other'));
options.add(new SelectOption('Sales','Sales'));
options.add(new SelectOption('Strategy','Strategy'));
options.add(new SelectOption('Tax','Tax'));
options.add(new SelectOption('Technology/Information Security','Technology/Information Security'));

  return options;
    }
         public list<selectoption> getStatelist(){
        List<SelectOption> Options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult =
 job__c.AR_Primary_Location_State__c.getDescribe();
   List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
   for( Schema.PicklistEntry f : ple)
   {
      options.add(new SelectOption(f.getLabel(), f.getValue()));
   }
        return Options;
    }
    public list<selectoption> getMetrolist(){
        List<SelectOption> Options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult =
 job__c.AR_Nearest_Metropolitan_Area_1__c.getDescribe();
   List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

   for( Schema.PicklistEntry f : ple)
   {
      options.add(new SelectOption(f.getLabel(), f.getValue()));
   }
        return Options;
    }
    public pageReference fetchJob(){
    try
    {
    fetch();
    conditionIndustry='';
    conditionCategory='';
    conditionState='';
    conditionMetro='';
    
        selectCategoryValue= selectCategoryValue.replace('[','');
        selectCategoryValue= selectCategoryValue.replace(']','');
        
        selectindustryvalue=selectindustryvalue.replace('[','');
        selectindustryvalue=selectindustryvalue.replace(']','');
        selectindustryvalue=selectindustryvalue.replace(',','ZZZ');
      selectindustryvalue=selectindustryvalue.replace('TechnologyZZZ Media & Telecommunications','Technology, Media & Telecommunications');
        selectStateValue=selectStateValue.replace('[','');
        system.debug('pak'+selectStateValue);
        selectStateValue=selectStateValue.replace(']','');
        selectMetroValue= selectMetroValue.replace('[','');
        selectMetroValue= selectMetroValue.replace(']','');
        system.debug('**k**'+selectindustryvalue);
        String[] str=selectindustryvalue.split('ZZZ');
        String[] strcategory=selectCategoryValue.split(',');
        string[] strstate=selectStateValue.split(',');
        string[] strMetro=selectMetroValue.split(',');
        query+=' and ';
        for(string s:str)
        {
           conditionIndustry+='industry__c=\''+s.trim()+'\' OR ';
        }
        system.debug('pakind'+conditionIndustry);
        if(selectindustryvalue.length()!=0)
        {
        conditionIndustry= conditionIndustry.removeStart('null');
        conditionIndustry= conditionIndustry.removeend(' OR ');
        query+='('+conditionIndustry+') and';
        
        }
        
        for(string scat:strcategory)
        {
        conditionCategory+='Job_Category__c=\''+scat.trim()+'\' OR ';
        system.debug('ind2'+conditionCategory);
        }
        if(selectCategoryvalue.length()!=0)
        {
        conditionCategory= conditionCategory.removeStart('null');
        conditionCategory= conditionCategory.removeEnd(' OR ');
        conditionCategory= '('+conditionCategory+') and';
        query+=conditionCategory;
        system.debug('*ind'+conditionCategory);
        }
        //Metro SOQL condition through code
        for(string smet:strmetro)
        {
        conditionMetro+='AR_Nearest_Metropolitan_Area_1__c =\''+smet.trim()+'\' OR ';
        conditionMetro+='AR_Nearest_Metropolitan_Area_2__c =\''+smet.trim()+'\' OR ';
        conditionMetro+='AR_Nearest_Metropolitan_Area_3__c =\''+smet.trim()+'\' OR ';
        system.debug('ind4'+ conditionMetro);
        }
        if(selectMetrovalue.length()!=0)
        {
        conditionMetro= conditionMetro.removeStart('null');
        conditionMetro= conditionMetro.removeEnd(' OR ');
        conditionMetro= '('+conditionMetro+') and';
        query+=conditionMetro;
        system.debug('*ind5'+conditionMetro);
        }
        for(string sstate:strstate)
        {
           conditionState+='AR_Primary_Location_State__c=\''+sstate.trim()+'\' OR ';
        }
        if(selectStateValue.length()!=0)
        {
        conditionState= conditionState.removeStart('null');
        conditionState= conditionState.removeend(' OR ');
        query+='('+conditionState+') and';
        system.debug('*ABC'+conditionState);
        }
        //query=query.substring(0,query.length()-4);
        list<string> lststr = str;
        system.debug('sss:1:'+lststr.size());
       // query+=' where name in : lststr';
        system.debug('queryqueryquery@@'+query.Substring(0,query.length()-3));
        query=query.Substring(0,query.length()-3); 
        system.debug('ind*pak'+query);     
        jobList =Database.query(query);
        system.debug('queryqueryquery'+ jobList);
        return null;
    }
    catch(System.QueryException ex){
         apexpages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select at least one filter criterion given below before clicking on Apply Filters button.'));
    }
    return null;
    }
}