public class SchedulingHelper {
    
    /*
     * 
     * Corn Job Formate 
     *    "Seconds Minutes Hours Day_of_month Month Day_of_week optional_year ".
     * Seconds	        0–59	None
        Minutes	        0–59	None
        Hours	        0–23	, - * /
        Day_of_month	1–31	, - * ? / L W
        Month	        1–12 or the following:
                         OR
                        JAN to DEC
                         OR
                        , - * /
        Day_of_week	    1–7 
                          OR
                        SAT to SUN
                         OR
                        , - * ? / L #
        optional_year	null or 1970–2099	, -  * /
     * 
     * 
     */

    public void wageNoticeSchedule()
    {
        WTPAA_BulkWageNotice_Schedular scheduleClass = new WTPAA_BulkWageNotice_Schedular();
        
		String cronSch = '0 30 * * * ?';
		String jobID = system.schedule('Wage Notice Auto Sent Hourly', cronSch, scheduleClass);
        
    }
    
}