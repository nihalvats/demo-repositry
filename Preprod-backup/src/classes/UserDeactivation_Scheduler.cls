/********************************************************************************************************************************
Apex class         : UserDeactivation_Scheduler
Description        : Scheduler class for user deactivation
Type               : Scheduler
Test Class         :  

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01                 Deloitte               05/27/2015                                                               Original Version
************************************************************************************************************************************/
global class UserDeactivation_Scheduler implements Schedulable{
 
    public string query='SELECT Id,Email,Name,IsActive,Role_Account__c,Usertype  from User where IsActive=True and Role_Account__c=false';
    global void execute(SchedulableContext ctx) {
        //query that gets passed to the  Batch
        
        string tempquery=query;
        
        system.debug('hello query'+Database.Query(tempquery));
        
        database.executeBatch(new UserDeactivation(tempquery));
    }
    
}