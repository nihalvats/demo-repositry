@isTest
public class WCT_SitesTaskRedirectController_Test
{
    public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Task_Reference_Table__c tasktod=new WCT_Task_Reference_Table__c();
        tasktod.WCT_Page_Name__c = 'Testing';
        insert tasktod;  
        Task t = new Task();
        t.subject = 'Success';
        t.ActivityDate=date.today().adddays(5);
        t.WCT_Task_Reference_Table_ID__c=tasktod.id;
        insert t;
        Test.startTest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_W7_Form;
        Test.setCurrentPage(pageRef); 

        
        SitesTaskRedirectController controller=new SitesTaskRedirectController();
        //controller.redirect();
        ApexPages.CurrentPage().getParameters().put('id',String.valueOf(t.id));
      
        encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt('test@testing.com'), 'UTF-8');
  
          controller.redirect();
     
        Test.stopTest(); 
    }
}