@istest

public class FMLA_Trigger_test{

public static testmethod void InsertFMLA(){

wct_Leave__c l= new wct_Leave__c();
 l.Wct_FMLA_Used_Over_the_Past_12_Months1__c =0;
 insert l;

WCT_FMLA_Hours__c wf= new WCT_FMLA_Hours__c();

wf.WCT_Associated_Leave__c = l.id;
wf.WCT_FMLA_Hours_Used__c=1000;

insert wf;

l.Wct_FMLA_Used_Over_the_Past_12_Months1__c +=wf.WCT_FMLA_Hours_Used__c;
 
 update l;
}
public static testmethod void UpdateFMLA(){
wct_Leave__c l= new wct_Leave__c();
 l.Wct_FMLA_Used_Over_the_Past_12_Months1__c =0;
 insert l;

WCT_FMLA_Hours__c wf= new WCT_FMLA_Hours__c();

wf.WCT_Associated_Leave__c = l.id;
wf.WCT_FMLA_Hours_Used__c=1000;

insert wf;

l.Wct_FMLA_Used_Over_the_Past_12_Months1__c +=wf.WCT_FMLA_Hours_Used__c;
 
 update l;
 decimal d= wf.WCT_FMLA_Hours_Used__c;
 wf.WCT_FMLA_Hours_Used__c =100;
 update wf;
l.Wct_FMLA_Used_Over_the_Past_12_Months1__c +=wf.WCT_FMLA_Hours_Used__c-d;
 
 update l;

}
public static testmethod void DeleteFMLA(){

wct_Leave__c l= new wct_Leave__c();
 l.Wct_FMLA_Used_Over_the_Past_12_Months1__c =0;
 insert l;

WCT_FMLA_Hours__c wf= new WCT_FMLA_Hours__c();

wf.WCT_Associated_Leave__c = l.id;
wf.WCT_FMLA_Hours_Used__c=1000;

insert wf;

l.Wct_FMLA_Used_Over_the_Past_12_Months1__c +=wf.WCT_FMLA_Hours_Used__c;
 
 update l;
 system.assertequals(l.Wct_FMLA_Used_Over_the_Past_12_Months1__c, 1000);
 decimal d= wf.WCT_FMLA_Hours_Used__c;
 delete wf;
 l.Wct_FMLA_Used_Over_the_Past_12_Months1__c -=d;
 
 update l;
 system.assertequals(l.Wct_FMLA_Used_Over_the_Past_12_Months1__c, 0);
}





}