@isTest
public class WCT_CT_BulkUpload_Test {

    static testMethod void TestFileUploader2 (){
        string csvcontent ='FirstName,LastName,Phone,Email,Requisition No,Hiring Location'+ '\r\n' +  'Haojun,Sun,9898956265,abc852015@gmail.com,Test REQ#01,New York'+ '\r\n' +  'Fake,Data,9898956598,abc2344@gmail.com,Test REQ#01,New York';
        test.startTest();
        
        Contact Cont = new Contact(FirstName='rakuten12345',LastName='sri',Email= 'abc852015@gmail.com');
        insert Cont;
        
        WCT_Requisition__c req = new WCT_Requisition__c();
        req.Name = 'Test REQ#01';
        insert req;
        
        WCT_Candidate_Requisition__c ct = new WCT_Candidate_Requisition__c();
        ct.WCT_Contact__c = Cont.id;
        ct.WCT_Requisition__c = req.id;
        insert ct;     
        WCT_CT_BulkUpload ct_bupload= new WCT_CT_BulkUpload();
        ct_bupload.candreq.WCT_Time_to_Schedule_First_Interview__c=system.now()-1;
        ct_bupload.candreq.WCT_Time_till_start_of_first_Interview__c=system.now()+1;
        ct_bupload.contentFile = Blob.valueof(csvContent);
        ct_bupload.ReadFile();
        ct_bupload.getAllStagingRecords();
        ct_bupload.WCT_CT_BulkUpload1();
        ct_bupload.getErrorRecords();
        ct_bupload.WCT_CT_BulkExportRange();
        ct_bupload.exportrngerr=true;
        ct_bupload.getAllStagingRecords();
        ct_bupload.getErrorRecordsRange();
        csvcontent ='FirstName,LastName,Phone,Email,Requisition No,Hiring Location'+'\r\n' +  'Fake,Data,9898956598,,Test REQ#013,New York';
        ct_bupload= new WCT_CT_BulkUpload();
        ct_bupload.candreq.WCT_Time_to_Schedule_First_Interview__c=system.now()-1;
        ct_bupload.candreq.WCT_Time_till_start_of_first_Interview__c=system.now()+1;
        ct_bupload.contentFile = Blob.valueof(csvContent);
        ct_bupload.ReadFile();
        ct_bupload.getAllStagingRecords();
        ct_bupload.WCT_CT_BulkUpload1();
        ct_bupload.getErrorRecords();
        ct_bupload.WCT_CT_BulkExportRange();
        ct_bupload.exportrngerr=true;
        ct_bupload.getAllStagingRecords();
        ct_bupload.getErrorRecordsRange();
        test.stopTest(); 
    }
}