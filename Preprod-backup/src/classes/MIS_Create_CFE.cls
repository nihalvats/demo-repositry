public class MIS_Create_CFE {
    public string recordid{set;get;}
    public string emaildl{set;get;}
    public list<Case_form_Extn__c> CFErecord{set;get;}
    public list<contact> cont{set;get;}
     public MIS_Create_CFE(ApexPages.StandardController controller) { 
         recordid=ApexPages.currentPage().getParameters().get('id');
         if (recordid!=null){
        CFErecord = [select id,TRT_Requestor_Name__c,GEN_Case__c,MIS_DL_Email_id__c from Case_form_Extn__c where id =:recordid LIMIT 1];
        
}
}
    public PageReference validate(){
        cont=[select id,email from contact where recordtype.name='Employee' and WCT_Employee_Status__c='active' and email=:emaildl];
        string con=',';
        if(emaildl.contains(con)){
          Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,''+'You can only add one DL at a time')); 
            return null;
        }
        else if(cont.size()>0 ){
            string emailcon=CFErecord[0].MIS_DL_Email_id__c;
             
             if(emailcon !=null && emailcon.contains(emaildl)){
              Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,''+'Email already exits')); 
                 return null;
             }
             
            else{
                create();
                PageReference pr = new PageReference('/'+recordid); 
               pr.setRedirect(true);  
               return pr;
            }
        }
        
        else{
          
       Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,''+'Please Enter a valid Email'));  
            return null;
        }
         
    }
    public  void create(){
        Case_form_Extn__c MISfd=new Case_form_Extn__c();
                
                Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
                Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
                Id rtId =rtMapByName.get('MIS Feedback').getRecordTypeId();//particular RecordId by  Name -->
                
                //inserting case form extension child records
                MISfd.MIS_Report_Detail_Object__c=CFErecord[0].id;
                MISfd.RecordTypeId=rtId;
                MISfd.GEN_Case__c=CFErecord[0].GEN_Case__c;
                MISfd.TRT_Requestor_Name__c=cont[0].id;
                MISfd.MIS_DL_Email_id__c=emaildl;
                
                 
          try{
            insert MISfd;
            updatedl();
              
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,''+'The new DL is updated successfully.'));    
            
            
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'New DL is not added for case form extension'+ e.getMessage()));
            system.debug('-------exception---------'+e.getMessage()+'----at---Line #----'+e.getLineNumber());
            
           
        }
               } 
    public void updatedl(){
        list<Case_form_Extn__c> cs=new list<Case_form_Extn__c>();
        
        cs=[select MIS_DL_Email_id__c from Case_form_Extn__c where id=:recordid LIMIT 1];
        string st=cs[0].MIS_DL_Email_id__c;
        string newst=st+','+emaildl;
        cs[0].MIS_DL_Email_id__c=newst;
        update cs;
        
    }
    }