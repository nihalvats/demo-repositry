/********************************************************************************************************************************
Apex class         : <WCT_LCAFormController>
Description        : <Controller which allows to Update Task and LCA>
Type               :  Controller
Test Class         : <WCT_LCAFormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 25/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/ 
public class WCT_LCAFormController  extends SitesTodHeaderController{

    /* upload related variable */
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public task t{get;set;}  
    public String taskid{get;set;}
    public List<Attachment> listAttachments {get; set;}
    public Map<Id, String> mapAttachmentSize {get; set;}  
    public GBL_Attachments attachmentHelper{get; set;}
    
    /* public variables */
    public WCT_LCA__c LCARecord{get;set;}
    public string visaEndDate{get;set;}
    
    /* Error Message related variables */
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}   
    public Boolean isError {get;set;}
    public String isErrorText {get;set;}
    
    public WCT_LCAFormController ()
    {  
        try{
        /*Initialize all Variables*/
        init();
        attachmentHelper= new GBL_Attachments();
        /*Get Task ID from Parameter*/
        getParameterInfo();  
        /*Task ID Null Check*/
        if(taskid=='' || taskid==null)
        {
            invalidEmployee=true;
            return;
        }
        /*Get Task Instance*/
        getTaskInstance();
        /*Get Attachment List and Size*/
        getAttachmentInfo();
        /*Query to get Immigration Record*/  
        getLCADetails();
    }
    catch(Exception e) {isError=true; isErrorText=e.getMessage();}  
    }
        public pagereference setStatus() {
        if(isError == true) {
         Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_LCAFormController', 'LCA Form',isErrorText);
            
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_LCAF_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
        }
        return null;
        }
  
    

/********************************************************************************************
*Method Name         : <init()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Init() Used for loading Mobility,Attachments>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/    
   
    private void init(){
        //Custom Object Instances
        LCARecord=new WCT_LCA__c();
        //Document Related Init
        doc = new Document();
        listAttachments = new List<Attachment>();
        mapAttachmentSize = new Map<Id, String>();
    }  

/********************************************************************************************
*Method Name         : <getParameterInfo()>
*Return Type         : <Null>
*Param’s             : URL
*Description         : <GetParameterInfo() Used to get URL Params for TaskID>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     

    private void getParameterInfo(){
        taskid = ApexPages.currentPage().getParameters().get('taskid');
    }

/********************************************************************************************
*Method Name         : <getTaskInstance()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetTaskInstance() Used for Querying Task Instance>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     
    private void getTaskInstance(){
        t=[SELECT Status,OwnerId,WhatId  FROM Task WHERE Id =: taskid];
    }

/********************************************************************************************
*Method Name         : <getAttachmentInfo()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetAttachmentInfo() Used for Retrieving Attachment Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/    
 
    @TestVisible
    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId , 
                            Name, 
                            BodyLength, 
                            Id,
                            CreatedDate
                            FROM  Attachment 
                            WHERE ParentId = :taskid
                            ORDER BY CreatedDate DESC
                            LIMIT 50 ];    
    
        for(Attachment a : listAttachments) {
            String size = null;
            if(1048576 < a.BodyLength){
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }

/********************************************************************************************
*Method Name         : <getLCADetails()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetLCADetails() Used for fetching LCA Information>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     
 
    public void getLCADetails()
    {
        LCARecord=[SELECT Id,
               Name,
               WCT_Assignment_Owner__c,
               WCT_City__c,
               WCT_Expiration_Date__c,
               WCT_Fragomen_External_Key__c,
               WCT_Immigration__c,
               WCT_Start_Date__c,
               WCT_State__c,
               WCT_Status__c,
               WCT_Zip_Code__c,
               ownerId,
               WCT_City_2__c,
               WCT_City_3__c,
               WCT_State_2__c,
               WCT_State_3__c,
               WCT_Zip_Code_2__c,
               WCT_Zip_Code_3__c
               from WCT_LCA__c 
               where  id=:t.WhatId ];
    }
    
/********************************************************************************************
*Method Name         : <save()>
*Return Type         : <PageReference>
*Param’s             : 
*Description         : <Save() Used for for Updating Task and Mobility>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     
    
    public pageReference save()
    { 
        /*Custom Validation Rules are handled here*/
        if( (null == LCARecord.WCT_City__c) || (null == LCARecord.WCT_State__c) || (null == LCARecord.WCT_Zip_Code__c))
            {
                pageErrorMessage = 'Please fill in all the required fields on the form.';
                pageError = true;
                return null;
            }
        if(String.isNotBlank(visaEndDate)){
            LCARecord.WCT_Expiration_Date__c = Date.parse(visaEndDate);
        }
        if(LCARecord.WCT_Expiration_Date__c<date.today())
        {
            pageErrorMessage = 'Expiration Date cannot be in the past.';
            pageError = true;
            return null;
        }
        try{
        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        t.OwnerId=UserInfo.getUserId();
        upsert t;
        /*update LCARecord Record*/
        LCARecord.WCT_Status__c = 'LCA Completed';
        upsert LCARecord ;
        /* Upload Documents as Related Attachments to Case After the Case Record is created */
        if(attachmentHelper.docIdList.isEmpty()) {
            pageErrorMessage = 'Attachment is required to submit the form.';
            pageError = true;
            return null;
        }
        attachmentHelper.uploadRelatedAttachment(t.id);
        //updating task record
        If (t.WCT_Auto_Close__c == true){   
            t.status = 'completed';
        }
        else{
            t.status = 'Employee Replied';  
        }
        t.OwnerId=LCARecord.Ownerid;
        t.WCT_Is_Visible_in_TOD__c=false; 
        if(string.valueOf(LCARecord.Ownerid).startsWith('00G')){
            //owner is Queue. So set it to GM&I User
            //t.Ownerid = '005f0000001AWth';
            t.Ownerid = System.Label.GMI_User;
        }
        else{
            t.OwnerId = LCARecord.Ownerid;
        }
        upsert t;
        //Reset Page Error Message
        pageError = false;
        getAttachmentInfo();
        }
        catch (Exception e) {
        Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_LCAFormController', 'LCA Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            
        Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_LCAF_EXP&expCode='+errLog.Name);
        pg.setRedirect(true);
        return pg;
        }
        Pagereference pageRef = new Pagereference('/apex/WCT_LCA_Form_Thankyou?taskid='+taskid);
        pageRef.setRedirect(true);
        return pageRef;
        
        
    }
}