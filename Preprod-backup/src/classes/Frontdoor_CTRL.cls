/*
Apex Class: Frontdoor_CTRL
CreatedOn: 29/03/2016
Author: Sainath(deloitte)
--------------------------------
Modified by   :   Anji Reddy K
Modified Date :   March/20/2016
Description   :   Here we need to navigate the page based on the Internal or External user. 
                  And we need to pass the one more parameter is TYPE (Internal or External)

*/
public with sharing class Frontdoor_CTRL{
public Frontdoor_CTRL(){
    
}
public PageReference goTalantOnDemand() {
   PageReference objPageRef=null; 
   string Key1 = null;
   String Key2 = null;
   string URL=null;
   string ReturnURL = null;
 
   
   try{
   Key1 = apexpages.currentpage().getparameters().get('Key') ; // we are passing Key1 (id)
   Map<String,String> allParams= apexpages.currentpage().getparameters();
   List<FrontDoorURLs__c> lstTODPref=new List<FrontDoorURLs__c>();
   lstTODPref=[select Id,Name, Final_URL__c from FrontDoorURLs__c where Name=:key1];
   system.debug('----lstTODPref-----'+lstTODPref);
       
   if(!lstTODPref.IsEmpty())
   {
       
    URL=lstTODPref[0].Final_URL__c;
    if(string.IsNotEmpty(URL))
    {
       String dynamicURL= '';
        
        for(String params : allParams.keySet())
        {
            if(params!='Key'&& params!='type'&params!=null)
            {
                dynamicURL=dynamicURL!=''?dynamicURL+'&'+params+'='+allParams.get(params):params+'='+allParams.get(params);
            }
        }
        if(dynamicURL!='')
        {
            ReturnURL= URL.contains('?')?URL+'&'+dynamicURL:URL+'?'+dynamicURL;
        }
        else
        {
            ReturnURL=URL;
        }
   
    }
     

    objPageRef = new PageReference(ReturnURL);
    objPageRef.setRedirect(true);
    system.debug('----The New URL is -----'+ReturnURL);
    }   
    }
    catch(Exception ex){
    system.debug('----Exception----'+ex.getMessage()+'----at Line #---'+ex.getLineNumber());
    }
    return objPageRef;
    }
}