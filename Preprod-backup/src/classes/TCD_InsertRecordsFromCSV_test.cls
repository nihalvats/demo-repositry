@isTest
public class TCD_InsertRecordsFromCSV_test {
    
    static testmethod void TCD_insertRecordfromCSV_TestMethod(){
        integer size=0;
        String csvContent ='Name,HR_Function__c,HR_Service_Area__c,HR_Service_Line__c,India_Talent_Contact_1__c,India_Talent_Contact_2__c,US_Talent_Contact_1__c,US_Talent_Contact_2__c,US_Talent_Contact_3__c,US_Talent_Contact_4__c,Payroll_Contact_Email__c,Workers_Comp_Contact_Email__c,Cost_Center_Description__c';
        
        String data ='0087667, HR_functionTest,HR ServiceArea_test, HR_ServiceLine_test,Talent_Contact1@deloitte.com, Talent_Contact2@deloitte.com,US_Talent_Contact1@deloitte.com, US_Talent_contact2@deloitte.com,US_Talent_contact3@deloitte.com, US_Talent_contact4@deloitte.com,Payroll_contact1@deloitte.com,Workers_comp_contact@deloitte.com, Cost_center_Description1';
        String blobCreator = csvContent + '\r\n' + data ;
        
        WCT_parseCSV parseCSVInstance = new WCT_parseCSV();  
        String namefile = 'test.csv';
        List<List<String>> fileLines = new List<List<String>>(); 
        filelines = parseCSVInstance.parseCSV(nameFile, true);
        String extension = namefile.substring(namefile.lastindexof('.')+1);       
        
        test.startTest();
        pagereference p = page.TCDInsertRecordsFromCsv;
        list<Talent_Delivery_Contact_Database__c> TCDlist = new list<Talent_Delivery_Contact_Database__c>();
        Test.SetCurrentPage(p); 
        
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error');
        ApexPages.addMessage(msg);
        
        list<Talent_Delivery_Contact_Database__c> tcdTemplist = new list<Talent_Delivery_Contact_Database__c>();
        Talent_Delivery_Contact_Database__c tdc = new Talent_Delivery_Contact_Database__c();
         tdc.Name = '000566987754';
        tdc.HR_Function__c ='Test_HR Function 1';
        tdc.HR_Service_Area__c = 'Test_HR_service_area 1';
        tdc.HR_Service_Line__c = ' Test Service Line 1';
        tdc.India_Talent_Contact_1__c = 'TalentContact11@deloitte.com';
        tdc.India_Talent_Contact_2__c = 'TalentContact21@deloitte.com';
        tdc.US_Talent_Contact_1__c = ' US_TalentContact11@deloitte.com';
        tdc.US_Talent_Contact_2__c= ' US_TalentContact21@deloitte.com';
        tdc.US_Talent_Contact_3__c = ' US_TalentContact31@deloitte.com';
        tdc.US_Talent_Contact_4__c =  ' US_TalentContact41@deloitte.com';
        tdc.Payroll_Contact_Email__c = 'Payroll_contact1@deloitte.com';
        tdc.Workers_Comp_Contact_Email__c = 'Workers_comp_contact1@deloitte.com';
        
        tcdTemplist.add(tdc);
        
        insert tcdTemplist;
        list<Talent_Delivery_Contact_Database__c> tcdtemp = [select id, name, HR_function__c, HR_service_line__c, HR_service_area__c,India_Talent_Contact_1__c, India_Talent_Contact_2__c, US_Talent_Contact_1__c,US_Talent_Contact_2__c, US_Talent_Contact_3__c, US_Talent_Contact_4__c,Payroll_Contact_Email__c, Workers_Comp_Contact_Email__c from Talent_Delivery_Contact_Database__c where id=:tcdTemplist[0].id];
        
        Talent_Delivery_Contact_Database__c tdc1 = new Talent_Delivery_Contact_Database__c();
        tdc1.Name = '000875876586';
        tdc1.HR_Function__c ='Test_HR Function';
        tdc1.HR_Service_Area__c = 'Test_HR_service_area';
        tdc1.HR_Service_Line__c = ' Test Service Line';
        tdc1.India_Talent_Contact_1__c = 'TalentContact1@deloitte.com';
        tdc1.India_Talent_Contact_2__c = 'TalentContact2@deloitte.com ';
        tdc1.US_Talent_Contact_1__c = 'US_TalentContact1@deloitte.com';
        tdc1.US_Talent_Contact_2__c= 'US_TalentContact2@deloitte.com';
        tdc1.US_Talent_Contact_3__c = 'US_TalentContact3@deloitte.com';
        tdc1.US_Talent_Contact_4__c = ' US_TalentContact4@deloitte.com';
        tdc1.Payroll_Contact_Email__c = 'Payroll_contact@deloitte.com';
        tdc1.Workers_Comp_Contact_Email__c = 'Workers_comp_contact@deloitte.com';
        insert tdc1;
     /*   for(Talent_Delivery_Contact_Database__c t : tcdtemp){
            string tcdname1 = t.name.replaceFirst('^0+(?!$)','');
            string tcdname2 = '8758764586';
            if(tcdname1 == tcdname2){
                tdc.Id = t.Id;
            }
            
        }
        tcdTemplist.add(tdc);
  */      
       // upsert tcdTemplist;
        
        TCDInsertRecordsFromCsv_controller cntrl = new TCDInsertRecordsFromCsv_controller();
        cntrl.contentFile = Blob.valueof(blobCreator);
        cntrl.ReadFile();
        cntrl.uploadprocess();
        cntrl.getFailedRows();
        cntrl.size=0;
        cntrl.totalunsuccessrec =0;
        cntrl.totalsuccessrec =0;
        cntrl.totalrecords=0;
        cntrl.totalsuccessreccount =0;
        test.stoptest();
    }
}