public class WCT_BatchUpdateToDToDoFailureHandlerS implements Schedulable {
     
    public void execute(SchedulableContext SC) {
        WCT_BatchUpdateToDToDoFailureHandler BTT =  new WCT_BatchUpdateToDToDoFailureHandler();
        Database.executeBatch(BTT, 10); 
    } 
}