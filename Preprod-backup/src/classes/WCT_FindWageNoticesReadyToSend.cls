/**
    * Class Name  : WCT_FindWageNoticesReadyToSend 
    * Description : This apex class will use to create immigrations. 
*/

global class WCT_FindWageNoticesReadyToSend {
  
   /** 
        Method Name  : findWageNoticesRecords
        Return Type  : Void
        Description  : Find all records which needs to treated as Ready To Send.         
    */
  
    WebService static String findWageNoticesRecords(String noticeStatus, List<Id> recordIds)
    {
        List<WTPAA_Wage_Notice__c> wageList = new List<WTPAA_Wage_Notice__c>();
        String returnStatement=Label.WCT_Message_Processing_Starting;
        List<String> wageStatus=Label.WCT_Wage_Status.split(',');
        
        for(WTPAA_Wage_Notice__c wageNoticeRecord: [SELECT Id, WTPAA_Status__c, WTPAA_Is_Notice_Sent__c,WTPAA_Controlling_Field_HeadupMsg__c  FROM WTPAA_Wage_Notice__c WHERE WTPAA_Status__c IN: wageStatus AND ID IN: recordIds LIMIT:Limits.getLimitQueryRows()])
        {    
            if(wageNoticeRecord.WTPAA_Controlling_Field_HeadupMsg__c == false)// Added by Siv on 27/12/15
          {  
            wageNoticeRecord.WTPAA_Status__c = 'Ready To Send';
            wageNoticeRecord.WTPAA_Is_Notice_Sent__c = false;
            wageNoticeRecord.WTPAA_Controlling_Field_HeadupMsg__c = true; // Added by Siv on 27/12/15
            wageList.add(wageNoticeRecord);
           } 
        // Added by Siv on 27/12/15  
           else 
          {
              wageNoticeRecord.WTPAA_Controlling_Field_HeadupMsg__c = false;
              wageNoticeRecord.WTPAA_Status__c = 'Ready To Send';
              wageNoticeRecord.WTPAA_Is_Notice_Sent__c = false;
              wageNoticeRecord.WTPAA_Batch_controlled__c = false; // Added by Siv on 19/01/16
              wageList.add(wageNoticeRecord);
          }
         // Ended by Siv on 27/12/15  
        }
        
        if(!wageList.IsEmpty())
        {
            try{
                UPDATE wageList;
            }
            Catch(Exception e)
            {
                WCT_ExceptionUtility.logException('WCT_FindWageNoticesReadyToSend','findWageNoticesRecords',e.getMessage());
                throw e;
            }
        }
        else
        {
            returnStatement=WCT_UtilConstants.PROCESS_NONE;
        }
        return returnStatement;
    }
}