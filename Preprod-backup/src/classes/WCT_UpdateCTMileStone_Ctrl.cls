public class WCT_UpdateCTMileStone_Ctrl{

    public string reqId {get;set;}
    public list<WCT_Candidate_Requisition__c> CTList {get;set;}
    public list<WCT_Candidate_Requisition__c> CTListToUpdate {get;set;}
    public Map<id,string> CTMasterMapStep {get;set;}
    public Map<id,string> CTMasterMapMilStatus {get;set;}
    public boolean noCTRecord {get;set;}
    
    public WCT_UpdateCTMileStone_Ctrl(){
        CTList = new list<WCT_Candidate_Requisition__c>();
        CTMasterMapStep = new Map<id,string>();
        CTMasterMapMilStatus = new Map<id,string>();
        noCTRecord = false;
        reqId = ApexPages.currentpage().getParameters().get('reqId');
        
        for(WCT_Candidate_Requisition__c ct : [SELECT Id,Name,WCT_RMS_Requisition__c,WCT_Interview_Status__c,WCT_Interview_Step__c,WCT_Candidate_Requisition_Status__c,
                                                WCT_Contact__c FROM WCT_Candidate_Requisition__c WHERE WCT_Requisition__c = :reqId AND 
                                                WCT_Candidate_Requisition_Status__c IN ('New','In Progress') LIMIT 100]){
            CTList.add(ct);
            CTMasterMapStep.put(ct.id,ct.WCT_Interview_Step__c);
            CTMasterMapMilStatus.put(ct.id,ct.WCT_Interview_Status__c);
        }
        system.debug('size'+ctlist.size());
        if(!CTList.isEmpty()){
            noCTRecord = true;
        }
    }
    
    public pageReference updateCTRecords(){
        CTListToUpdate = new list<WCT_Candidate_Requisition__c>();
        for(WCT_Candidate_Requisition__c ctUpdate : CTList){
            system.debug('$$'+ctUpdate.WCT_Interview_Status__c+'$$1'+CTMasterMapMilStatus.get(ctUpdate.id) );
            if(ctUpdate.WCT_Interview_Status__c <> CTMasterMapMilStatus.get(ctUpdate.id) || ctUpdate.WCT_Interview_Step__c <> CTMasterMapStep.get(ctUpdate.id)){
                CTListToUpdate.add(ctUpdate);
            }
        }
        system.debug('%%'+CTListToUpdate.size());
        if(!CTListToUpdate.isEmpty()){
            DataBase.update(CTListToUpdate,false);
        }
        return new pageReference('/'+reqId);
    }
    public pageReference cancel(){
        return new pageReference('/'+reqId);
    }

}