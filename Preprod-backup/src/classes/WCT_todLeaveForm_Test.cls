@isTest
public class WCT_todLeaveForm_Test
{
    public static Account accountObj ;
    public static Contact contactObj; 
    private static Account createAccount()
    {
        Account accObj = new Account(name = 'Test Account');
        insert accObj;
        return accObj;
    }
     private static Contact createContact(Account acc,string email)
		{
			Contact conObj = new Contact(firstname = 'Test',lastname = 'Contact',Accountid =acc.id,email=email,RecordTypeId = System.Label.Employee_Record_Type_ID,ELE_Access_Level__c = 'ITS',ELE_Contact__c=true);
			insert conObj;
			return conObj;
		}
   
    public static testmethod void m1()
    {     
        Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');  
   
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;         
       
        System.runAs(u)
        {
       // recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
       // Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
      //  insert con;
       // recordtype rt=[select id from recordtype where DeveloperName = 'Emergency_Contact'];
        //Contact conemer=WCT_UtilTestDataCreation.createEmployee(rt.id);
       // conemer.WCT_Primary_Contact__c =con.id;
        datetime startdate=date.Today().adddays(-10);
        datetime enddate=date.Today().adddays(-1);
        Test.starttest();
       // String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_LeavesForm;
        Test.setCurrentPage(pageRef); 
      //  ApexPages.CurrentPage().getParameters().put('em',encrypt);
        //todLeaveForm controller=new todLeaveForm ();
      //  insert conemer;        
        
        todLeaveForm controller=new todLeaveForm ();
            controller.loggedInContact=con;
      	controller.saveLeavesRecord();
        controller.loggedInContact.WCT_ExternalEmail__c='Testingleaves@test.com';
        controller.saveLeavesRecord();
        controller.objLeaves.WCT_Code_of_Ethics_Certification__c=true;
        controller.objLeaves.WCT_Leave_Reason_Other__c=null;
        controller.estimatedLastWorkDay=startdate.format('MM/dd/yyyy');
        controller.estimatedReturnWorkDay=enddate.format('MM/dd/yyyy');
        controller.fmlaPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.fmlaPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.fmlaUnpaidStartDate=null;
        controller.fmlaUnpaidEndDate=enddate.format('MM/dd/yyyy');
        controller.leavereason='Other';
       recordtype rt=[select id from recordtype where DeveloperName = 'fmla'];
        controller.strRecordType = rt.id; 
        controller.objLeaves.Wct_FMLA_Continuous__c=true; 
        controller.saveLeavesRecord();   
        //controller.objLeaves.WCT_Leave_Reason__c='test';
        controller.saveLeavesRecord();
        controller.getleavereasons();
        
        controller.personalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.personalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.personalUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.personalUnpaidEndDate=enddate.format('MM/dd/yyyy');
     	 rt=[select id from recordtype where DeveloperName = 'Personal'];
        controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();   
        controller.objLeaves.WCT_Leave_Reason__c='test';
        controller.doc=WCT_UtilTestDataCreation.createDocument();
        //controller.uploadAttachment();
        controller.saveLeavesRecord();  
        controller.parentalChildDOB=enddate.format('MM/dd/yyyy');
        controller.parentalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalSTDStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalSTDEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalPaidParentalStartDate=startdate.format('MM/dd/yyyy');
        controller.objLeaves.WCT_Leave_Start_Date__c= Date.parse(controller.parentalPaidParentalStartDate);
        controller.parentalPaidParentalEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalAdditionalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalAdditionalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalUnpaidEndDate=enddate.format('MM/dd/yyyy');
        rt=[select id from recordtype where DeveloperName = 'Parental'];
        controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();  
        rt=[select id from recordtype where DeveloperName = 'Military'];
        controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();  
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
       // controller.uploadAttachment(); 
        //todLeaveForm tlf = new todLeaveForm();
        controller.dateRange('10/11/1983','10/09/1983','10/10/1983','10/11/1983');
        controller.dateRange('10/10/1983','10/12/1983','10/11/1983','10/11/1983');
        //controller.leavereason=
        Test.stoptest();
        }
    }
    
      /*public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createContact();
        con.LastName='Testing Approval';
        con.email='test@test.com';
        con.WCT_Visa_Type__c='temp';
        con.MobilePhone='123';
        con.recordtypeid=rt.id;
        insert con;
        rt=[select id from recordtype where DeveloperName = 'Emergency_Contact'];
        Contact conemer=WCT_UtilTestDataCreation.createContact();
        conemer.LastName='Testing Approval';
        conemer.email='testing@test.com';
        conemer.MobilePhone='123';
        conemer.WCT_Relationship__c='Friend';
        conemer.WCT_Primary_Contact__c =con.id;
        conemer.recordtypeid=rt.id;
        Test.starttest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_LeavesForm;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        todLeaveForm leaves=new todLeaveForm ();
        insert conemer;
        leaves=new todLeaveForm ();
        leaves.saveLeavesRecord();
        
        leaves.saveLeavesRecord();
        datetime estimatedLastWorkDay=date.Today().adddays(10);
        datetime estimatedReturnWorkDay=date.Today().adddays(20);
        leaves.estimatedLastWorkDay=estimatedLastWorkDay.format('MM/dd/yyyy');
        leaves.estimatedReturnWorkDay=estimatedReturnWorkDay.format('MM/dd/yyyy');
        leaves.objLeaves.WCT_Code_of_Ethics_Certification__c=true;
        rt=[select id from recordtype where DeveloperName = 'Personal'];
        leaves.strRecordType = rt.id;
        leaves.saveLeavesRecord();
        leaves.objLeaves.WCT_Leave_Reason__c='test';
        leaves.docIdList.add('test');
        Attachment a=WCT_UtilTestDataCreation.createAttachment(leaves.objLeaves.id);
        a.name='test';
        //insert a;
        Document d=WCT_UtilTestDataCreation.createDocument(); 
        todLeaveForm.AttachmentsWrapper ta = new todLeaveForm.AttachmentsWrapper(true,'',String.valueof(d.id),'2000');   
        leaves.UploadedDocumentList.add(ta); 
        leaves.doc=d;
        leaves.uploadAttachment();
        leaves.pageError=true;
        leaves.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        leaves.uploadAttachment();
        leaves.saveLeavesRecord(); 
        rt=[select id from recordtype where DeveloperName = 'Parental'];
        leaves.strRecordType = rt.id;
        leaves.saveLeavesRecord();
        leaves.parentalChildDOB=estimatedLastWorkDay.format('MM/dd/yyyy');
        leaves.parentalPaidParentalStartDate=estimatedLastWorkDay.format('MM/dd/yyyy');
        leaves.parentalPaidParentalEndDate=estimatedReturnWorkDay.format('MM/dd/yyyy');
       // leaves.objLeaves.WCT_Is_Employee_Child_Caregiver__c='Primary Caregiver';
        leaves.saveLeavesRecord(); 
        rt=[select id from recordtype where DeveloperName = 'Military'];
        leaves.strRecordType = rt.id;
        leaves.saveLeavesRecord();
        //leaves.objLeaves.WCT_Military_Branch__c='U.S. Army';
        //leaves.objLeaves.WCT_Military_Status__c='Active';
        leaves.saveLeavesRecord(); 
        test.stoptest();
    }*/
    public static testMethod void test1(){
        
         Contact con1 = createContact(createAccount(),'jbahuguna@deloitte.com');  
   
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;         
       
       
        System.runAs(u)
        {
            
       /*  recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        rt=[select id from recordtype where DeveloperName = 'Emergency_Contact'];
        Contact conemer=WCT_UtilTestDataCreation.createEmployee(rt.id);
        conemer.WCT_Primary_Contact__c =con.id;
		*/
       
        datetime startdate=date.Today().adddays(-10);
        datetime enddate=date.Today().adddays(-1);
        Test.starttest();
       // String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_LeavesForm;
        Test.setCurrentPage(pageRef); 
       // ApexPages.CurrentPage().getParameters().put('em',encrypt);
        //todLeaveForm controller=new todLeaveForm ();
       // insert conemer;
        
        todLeaveForm controller=new todLeaveForm ();
              controller.loggedInContact=con1;
        controller.saveLeavesRecord();
        controller.loggedInContact.WCT_ExternalEmail__c='Testingleaves@test.com';
        controller.saveLeavesRecord();
        controller.objLeaves.WCT_Code_of_Ethics_Certification__c=true;
        //controller.objLeaves.WCT_Leave_Reason_Other__c=null;
        controller.estimatedLastWorkDay=startdate.format('MM/dd/yyyy');
        controller.estimatedReturnWorkDay=enddate.format('MM/dd/yyyy');
        controller.fmlaPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.fmlaPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.fmlaUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.fmlaUnpaidEndDate=enddate.format('MM/dd/yyyy');
        controller.leavereason='others';
        recordtype rt=[select id from recordtype where DeveloperName = 'fmla'];
        controller.strRecordType = rt.id; 
        controller.objLeaves.Wct_FMLA_Continuous__c=false; 
        controller.saveLeavesRecord();   
        //controller.objLeaves.WCT_Leave_Reason__c='test';
        controller.saveLeavesRecord();
        controller.getleavereasons();
        
        controller.personalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.personalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.personalUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.personalUnpaidEndDate=enddate.format('MM/dd/yyyy');
        rt=[select id from recordtype where DeveloperName = 'Personal'];
        controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();   
        controller.objLeaves.WCT_Leave_Reason__c='test';
        controller.doc=WCT_UtilTestDataCreation.createDocument();
       // controller.uploadAttachment();
        controller.saveLeavesRecord();  
        controller.parentalChildDOB=enddate.format('MM/dd/yyyy');
        controller.parentalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalSTDStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalSTDEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalPaidParentalStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalPaidParentalEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalAdditionalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalAdditionalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalUnpaidEndDate=enddate.format('MM/dd/yyyy');
        rt=[select id from recordtype where DeveloperName = 'Parental'];
       controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();  
       rt=[select id from recordtype where DeveloperName = 'Military'];
       controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();  
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        //controller.uploadAttachment(); 
        controller.dateRange('10/11/1983','10/09/1983','10/10/1983','10/11/1983');
        controller.dateRange('10/10/1983','10/12/1983','10/11/1983','10/11/1983');
        
        Test.stoptest();
        }
    }
    
     public static testmethod void test2()
    {
        Contact con2 = createContact(createAccount(),'jbahuguna@deloitte.com');     
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;         
       
        System.runAs(u)
        {
    /* recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
       insert con;
        rt=[select id from recordtype where DeveloperName = 'Emergency_Contact'];
        Contact conemer=WCT_UtilTestDataCreation.createEmployee(rt.id);
        conemer.WCT_Primary_Contact__c =con.id;
	*/
        datetime startdate=date.Today().adddays(-10);
        datetime enddate=date.Today().adddays(-1);
        Test.starttest();
       // String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_LeavesForm;
        Test.setCurrentPage(pageRef); 
     //   ApexPages.CurrentPage().getParameters().put('em',encrypt);
        //todLeaveForm controller=new todLeaveForm ();
       // insert conemer;
        
        
        todLeaveForm controller=new todLeaveForm ();
             controller.loggedInContact=con2;
        controller.saveLeavesRecord();
        controller.loggedInContact.WCT_ExternalEmail__c='Testingleaves@test.com';
        controller.saveLeavesRecord();
        controller.objLeaves.WCT_Code_of_Ethics_Certification__c=true;
        controller.objLeaves.WCT_Leave_Reason_Other__c=null;
        controller.estimatedLastWorkDay=startdate.format('MM/dd/yyyy');
        controller.estimatedReturnWorkDay=enddate.format('MM/dd/yyyy');
        controller.fmlaPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.fmlaPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.fmlaUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.fmlaUnpaidEndDate=enddate.format('MM/dd/yyyy');
        controller.leavereason='';
       recordtype rt=[select id from recordtype where DeveloperName = 'fmla'];
        controller.strRecordType = rt.id; 
        controller.objLeaves.Wct_FMLA_Continuous__c=true; 
        controller.saveLeavesRecord();   
        //controller.objLeaves.WCT_Leave_Reason__c='test';
        controller.saveLeavesRecord();
        controller.getleavereasons();
        
        controller.personalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.personalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.personalUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.personalUnpaidEndDate=enddate.format('MM/dd/yyyy');
            
        rt=[select id from recordtype where DeveloperName = 'Personal'];
        controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();   
        controller.objLeaves.WCT_Leave_Reason__c='test';
        controller.doc=WCT_UtilTestDataCreation.createDocument();
       // controller.uploadAttachment();
        controller.saveLeavesRecord();  
        controller.parentalChildDOB=enddate.format('MM/dd/yyyy');
        controller.parentalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalSTDStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalSTDEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalPaidParentalStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalPaidParentalEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalAdditionalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalAdditionalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalUnpaidEndDate=enddate.format('MM/dd/yyyy');
            
            
            Document doc = new Document();
           doc.Body = Blob.valueOf('JSON string');
            doc.Name = 'samplename';
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert doc;
        controller.attachmentHelper.docIdList.add(doc.id);   
        controller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'30'));
        controller.objLeaves.WCT_Leave_Reason_other__c='test';
        controller.saveLeavesRecord(); 
            
        rt=[select id from recordtype where DeveloperName = 'Parental'];
        controller.strRecordType = rt.id;  
            
            
            
            
            
        controller.saveLeavesRecord();  
        rt=[select id from recordtype where DeveloperName = 'Military'];
        controller.strRecordType = rt.id;  
            
            
            
        controller.saveLeavesRecord();  
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
       // controller.uploadAttachment(); 
        controller.dateRange('10/11/1983','10/09/1983','10/10/1983','10/11/1983');
        controller.dateRange('10/10/1983','10/12/1983','10/11/1983','10/11/1983');
        Test.stoptest();
        }
    }
    public static testmethod void test3()
    {
         Contact con3 = createContact(createAccount(),'jbahuguna@deloitte.com');     
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;         
       
        System.runAs(u)
        {
      /*recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
       Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
       insert con;
       recordtype rt=[select id from recordtype where DeveloperName = 'Emergency_Contact'];
        Contact conemer=WCT_UtilTestDataCreation.createEmployee(rt.id);
        conemer.WCT_Primary_Contact__c =con.id;
            */
        datetime startdate=date.Today().adddays(-10);
        datetime enddate=date.Today().adddays(-1);
        Test.starttest();
        //String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_LeavesForm;
        Test.setCurrentPage(pageRef); 
       // ApexPages.CurrentPage().getParameters().put('em',encrypt);
        //todLeaveForm controller=new todLeaveForm ();
     //   insert conemer;
        
        
        todLeaveForm controller=new todLeaveForm();
             controller.loggedInContact=con3;
        controller.saveLeavesRecord();
        controller.loggedInContact.WCT_ExternalEmail__c='Testingleaves@test.com';
        controller.saveLeavesRecord();
        controller.objLeaves.WCT_Code_of_Ethics_Certification__c=true;
        //controller.objLeaves.WCT_Leave_Reason_Other__c=null;
        controller.estimatedLastWorkDay=startdate.format('MM/dd/yyyy');
        controller.estimatedReturnWorkDay=enddate.format('MM/dd/yyyy');
        controller.fmlaPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.fmlaPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.fmlaUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.fmlaUnpaidEndDate=null;//===================
        controller.leavereason='others';
        recordtype rt=[select id from recordtype where DeveloperName = 'fmla'];
        controller.strRecordType = rt.id; 
        controller.objLeaves.Wct_FMLA_Continuous__c=true; 
        controller.saveLeavesRecord();   
		
		controller.fmlaPTOStartDate=null; 
		controller.saveLeavesRecord(); 
		
		controller.fmlaPTOStartDate=(System.now()).format('MM/dd/yyyy');
        controller.fmlaPTOEndDate=(System.now()+2).format('MM/dd/yyyy');
        controller.fmlaUnpaidStartDate=(System.now()).format('MM/dd/yyyy');
        controller.fmlaUnpaidEndDate=(System.now()+1).format('MM/dd/yyyy');
		controller.saveLeavesRecord(); 
		
		
		
		
        
        controller.getleavereasons();
        
        controller.personalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.personalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.personalUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.personalUnpaidEndDate=enddate.format('MM/dd/yyyy');
        rt=[select id from recordtype where DeveloperName = 'Personal'];
		
		
        controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();   
        controller.objLeaves.WCT_Leave_Reason__c='test';
        controller.doc=WCT_UtilTestDataCreation.createDocument();
      //  controller.uploadAttachment();
        controller.saveLeavesRecord();  
		
		
		controller.objLeaves.WCT_Leave_Reason_other__c=null;
		
		   Document doc = new Document();
           doc.Body = Blob.valueOf('JSON string');
            doc.Name = 'samplename';
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert doc;
        controller.attachmentHelper.docIdList.add(doc.id);   
        controller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'30'));
		
		controller.saveLeavesRecord(); 
		 
		controller.objLeaves.WCT_Leave_Reason_other__c='test';
		controller.personalPTOStartDate=null;
		controller.saveLeavesRecord(); 
		
	
		   
		
        rt=[select id from recordtype where DeveloperName = 'Military'];
        controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();  
		
		
		controller.parentalChildDOB=enddate.format('MM/dd/yyyy');
        controller.parentalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalSTDStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalSTDEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalPaidParentalStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalPaidParentalEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalAdditionalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalAdditionalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalUnpaidEndDate=enddate.format('MM/dd/yyyy');
		
		
		
        rt=[select id from recordtype where DeveloperName = 'Parental'];
        controller.strRecordType = rt.id; 
		
		controller.saveLeavesRecord();
		
			controller.parentalChildDOB=enddate.format('MM/dd/yyyy');
        controller.parentalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalSTDStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalSTDEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalPaidParentalStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalPaidParentalEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalAdditionalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalAdditionalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalUnpaidEndDate=enddate.format('MM/dd/yyyy');
		
		controller.objLeaves.WCT_Parental_Event__c='test';
		
		
		controller.saveLeavesRecord();
		
		
		 doc = new Document();
           doc.Body = Blob.valueOf('JSON string');
            doc.Name = 'samplename';
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert doc;
        controller.attachmentHelper.docIdList.add(doc.id);   
        controller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'30'));
		
				
		
		controller.saveLeavesRecord();
		
		
		
		
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
      //  controller.uploadAttachment(); 
        //todLeaveForm tlf = new todLeaveForm();
        controller.dateRange('10/11/1983','10/09/1983','10/10/1983','10/11/1983');
        controller.dateRange('10/10/1983','10/12/1983','10/11/1983','10/11/1983');
        controller.objLeaves.WCT_Leave_Reason__c=null;
		
            controller.saveLeavesRecord();
            test.stoptest();
        }
    }
    public static testmethod void test4()
    {
         Contact con4 = createContact(createAccount(),'jbahuguna@deloitte.com');     
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;         
       
        System.runAs(u)
        {
       /* recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        rt=[select id from recordtype where DeveloperName = 'Emergency_Contact'];
        Contact conemer=WCT_UtilTestDataCreation.createEmployee(rt.id);
        conemer.WCT_Primary_Contact__c =con.id;
*/
        datetime startdate=date.Today().adddays(-10);
        datetime enddate=date.Today().adddays(-1);
        Test.starttest();
       // String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_LeavesForm;
        Test.setCurrentPage(pageRef); 
       // ApexPages.CurrentPage().getParameters().put('em',encrypt);
        //todLeaveForm controller=new todLeaveForm ();
       // insert conemer;
        
        
        todLeaveForm controller=new todLeaveForm ();
             controller.loggedInContact=con4;
        controller.saveLeavesRecord();
        controller.loggedInContact.WCT_ExternalEmail__c='Testingleaves@test.com';
        controller.saveLeavesRecord();
        controller.objLeaves.WCT_Code_of_Ethics_Certification__c=true;
        //controller.objLeaves.WCT_Leave_Reason_Other__c=null;
        controller.estimatedLastWorkDay=startdate.format('MM/dd/yyyy');
        controller.estimatedReturnWorkDay=enddate.format('MM/dd/yyyy');
        controller.fmlaPTOStartDate=system.now().format('MM/dd/yyyy');
        controller.fmlaPTOEndDate=(system.now()+2).format('MM/dd/yyyy');
        controller.fmlaUnpaidStartDate=(system.now()+3).format('MM/dd/yyyy');
        controller.fmlaUnpaidEndDate=(system.now()+4).format('MM/dd/yyyy');
        controller.leavereason='others';
        recordtype rt=[select id from recordtype where DeveloperName = 'fmla'];
        controller.strRecordType = rt.id; 
        controller.objLeaves.Wct_FMLA_Continuous__c=true; 
        controller.saveLeavesRecord();
            
            
            
        Document doc = new Document();
           doc.Body = Blob.valueOf('JSON string');
            doc.Name = 'samplename';
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert doc;
        controller.attachmentHelper.docIdList.add(doc.id);   
        controller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'30'));
     
		controller.objLeaves.id=null;            
            controller.saveLeavesRecord();
        controller.getleavereasons();
        
        controller.personalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.personalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.personalUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.personalUnpaidEndDate=enddate.format('MM/dd/yyyy');
        rt=[select id from recordtype where DeveloperName = 'Personal'];
        controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();   
            
            
            
        controller.objLeaves.WCT_Leave_Reason__c='test';
        controller.doc=WCT_UtilTestDataCreation.createDocument();
       // controller.uploadAttachment();
        controller.saveLeavesRecord();  
        controller.parentalChildDOB=enddate.format('MM/dd/yyyy');
        controller.parentalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalSTDStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalSTDEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalPaidParentalStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalPaidParentalEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalAdditionalPTOStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalAdditionalPTOEndDate=enddate.format('MM/dd/yyyy');
        controller.parentalUnpaidStartDate=startdate.format('MM/dd/yyyy');
        controller.parentalUnpaidEndDate=enddate.format('MM/dd/yyyy');
        rt=[select id from recordtype where DeveloperName = 'Parental'];
        controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();  
        rt=[select id from recordtype where DeveloperName = 'Military'];
        controller.strRecordType = rt.id;  
        controller.saveLeavesRecord();  
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
       // controller.uploadAttachment(); 
        //todLeaveForm tlf = new todLeaveForm();
        controller.dateRange('10/11/1983','10/09/1983','10/10/1983','10/11/1983');
        controller.dateRange('10/10/1983','10/12/1983','10/11/1983','10/11/1983');
        //controller.leavereason=
        Test.stoptest();
        }
    }
     public static testmethod void test5()
    {
         Contact con5 = createContact(createAccount(),'jbahuguna@deloitte.com');     
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;         
       
        System.runAs(u)
        {
       /* recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        rt=[select id from recordtype where DeveloperName = 'Emergency_Contact'];
        Contact conemer=WCT_UtilTestDataCreation.createEmployee(rt.id);
        conemer.WCT_Primary_Contact__c =con.id;
	*/
        datetime startdate=date.Today().adddays(-10);
        datetime enddate=date.Today().adddays(-1);
        Test.starttest();
      //  String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_LeavesForm;
        Test.setCurrentPage(pageRef); 
        //ApexPages.CurrentPage().getParameters().put('em',encrypt);
        //todLeaveForm controller=new todLeaveForm ();
     //   insert conemer;
        
        
        todLeaveForm controller=new todLeaveForm ();
        	 controller.loggedInContact=con5;
        controller.formType='Sample Form';
        controller.FMLA_Continuous=true;
        controller.FMLA_Intermittent_Leave=true;
        controller.strRecordType='SampleRecordType';
        controller.saveLeavesRecord(); 
        list<string> doclist= new list<string>();
        doclist.add('sampledocString1'); 
        doclist.add('sampledocString2');
        doclist.add('sampledocString3');
        controller.docIdList=doclist;
      //  todLeaveForm.AttachmentsWrapper attwrap = new todLeaveForm.AttachmentsWrapper(true, 'sample Name','01p40000000U7cX', '21234');
      //  list<todLeaveForm.AttachmentsWrapper> attwraplist1 = new list<todLeaveForm.AttachmentsWrapper>();
      //  attwraplist1.add(attwrap); 
      //  list<todLeaveForm.AttachmentsWrapper> attwraplist = new list<todLeaveForm.AttachmentsWrapper>();
        
        //controller.UploadedDocumentList.add(attwrap);
      //  controller.uploadRelatedAttachment(); 
       // controller.TestCoverage1();
      //  controller.TestCoverage2();
     //   controller.TestCoverage3();
      //  controller.TestCoverage4();
        
        Test.setCurrentPage(Page.WCT_LeavesForm);
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean isErrorMessage = false;
        for(Apexpages.Message msg1 : msgs){
            if(controller.parentalPTOStartDate!=null && controller.parentalPTOEndDate==null){
                if (msg1.getDetail().contains('Please enter the Parental PTO date range') )
                isErrorMessage  = true;
            }
            
        }
        //system.assert(isErrorMessage);


        
        Test.stoptest();
     }   
    }
    
    
}