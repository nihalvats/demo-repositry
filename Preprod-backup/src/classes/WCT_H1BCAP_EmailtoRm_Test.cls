@isTest
 public class WCT_H1BCAP_EmailtoRm_Test{
 
 public String surid = ApexPages.currentPage().getParameters().get('id');
 public String cuupdt = ApexPages.currentPage().getParameters().get('cup');
 
 
 Static testmethod void WCT_H1BCAP_EmailtoRm_TestMethod(){
     String d = '2016';
     Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_RM']; 
     User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','svalluru@deloitte.com');
     platuser.HIB_CAP_Financial_Year__c = d;
     platuser.HIB_CAP_Comments__c = 'start';
     platuser.Is_SendRM_Email__c = false;
     platuser.Year__c = 2016;
     insert platuser;
 
 
     recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
     Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
     con.WCT_Employee_Group__c = 'Active';
     insert con;
     
    Test.startTest();
    pagereference pgf = page.WCT_H1BCAP_EmailtoRM;
   // pgf.getParameters().put('id', String.valueOf(platuser.Id));
    Test.SetCurrentPage(pgf ); 
    
     ApexPages.CurrentPage().getParameters().put('id',platuser.id);
      ApexPages.standardcontroller c= new ApexPages.standardcontroller(platuser);
      WCT_H1BCAP_EmailtoRm obj = new  WCT_H1BCAP_EmailtoRm(c);     
      obj.SendEmail();
      obj.cancel();
   
   Test.stoptest();
 
 
     }
    }