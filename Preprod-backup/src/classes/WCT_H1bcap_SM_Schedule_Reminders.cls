global class WCT_H1bcap_SM_Schedule_Reminders implements Schedulable {

     global void execute(SchedulableContext sc) {

         

       //Define  batch size.       

       integer BATCH_SIZE = 1; 

      WCT_H1bcap_SM_Reminders sndBatch = new WCT_H1bcap_SM_Reminders();
     system.debug('****WCT_H1bcap_SM_Reminders : starting batch exection*****************');

     Id batchId = database.executeBatch(sndBatch , BATCH_SIZE);   

  system.debug('**** WCT_H1bcap_SM_Reminders : Batch executed batchId: '+batchId);

   }
 }