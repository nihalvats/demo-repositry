@isTest

private class WCT_SMList_View_Test {

static testMethod void myUnitTest() {

    Test.startTest();
    
    
  Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_RM']; 
  User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','svalluru@deloitte.com');
  insert platuser;
  
  Profile PPPD = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_PPD']; 
  User platPPDuser=WCT_UtilTestDataCreation.createUser( 'sivappd',PPPD.id,' svallurutestppd@deloitte.com.preprod','svalluruppd@deloitte.com');
  insert platPPDuser;
  Profile PSLL = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SLL']; 
  User platSLLuser=WCT_UtilTestDataCreation.createUser( 'sivasll',PPPD.id,' svallurutestsll@deloitte.com.preprod','svallurusll@deloitte.com');
  insert platSLLuser;
  Profile PSM = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SM']; 
  User platSMuser=WCT_UtilTestDataCreation.createUser( 'siva283',PSM.id,' svallurutest99@deloitte.com.preprod','svalluru1@deloitte.com');
  insert platSMuser;
  system.runas(platSMuser){
  
   Recordtype rt=[select id from Recordtype where DeveloperName = 'WCT_Employee'];
   Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
   con.Email = 'test1@gmail.com';
   insert con;
 

   Contact consm=WCT_UtilTestDataCreation.createEmployee(rt.id);
   consm.Email = 'testrm@gmail.com';
   insert consm;
  
   WCT_H1BCAP__c capobj= new WCT_H1BCAP__c();
   //user platSMuser= new user();
   //user platSLLuser= new user();
   
   capobj.Current_FY_Nomination__c = true;
   capobj.WCT_H1BCAP_Email_ID__c ='test1@gmail.com';
   capobj.WCT_H1BCAP_GDM_Email_id__c ='svalluru1@deloitte.com';
   capobj.WC_H1BCAP_USI_SLL_email_id__c ='test1@gmail.com';
   capobj.WCT_H1BCAP_US_PPD_mail_Id__c ='test1@gmail.com';
   capobj.WCT_H1BCAP_Status__c ='USI SLL Approved';
   capobj.WCT_H1BCAP_Resource_Manager_Email_ID__c= 'test@deloitte.com';
  
  
   //capobj.WCT_H1BCAP_USI_SLL_Name__c =platSLLuser.id;    
   
   capobj.WCT_H1BCAP_Practitioner_Name__c =con.id;
   insert capobj;
 
   Boolean b = true;
  //List<wrapAccount> wrapAccountList = new List<wrapAccount>();
  PageReference pageRef = Page.WCT_ApprovalList_View;
    Test.setCurrentPage(pageRef); 
    
   WCT_SMList_View smlist = new WCT_SMList_View();
      smlist.loggedName = '';
  WCT_SMList_View.wrapAccount wrap = new WCT_SMList_View.wrapAccount(b,capobj);
 smlist.pageError=false;
 smlist.pageErrorMessage='testing';
   //smlist.wrapAccountList[0].selected=true;

  //list<WCT_SMList_View.wrapAccount> smlist.getSMItems();
 // wrap.realignRecord(b,capobj); 
   smlist.loggedName = '20_H1BCAP_SM';
   smlist.SMComments='test';
   smlist.SLLComments='test';
   smlist.SMRealignComments='test';
   System.debug('log_name::'+smlist.loggedName);

   smlist.LoggedInUserEmail='testrm@gmail.com'; 
   System.debug('log_email::'+smlist.LoggedInUserEmail);
   //smlist= new WCT_SMList_View();
   smlist.rejectAction();
   smlist.cancelAction();
   smlist.realignAction();
   smlist.cancelRealignAction();
   smlist.callCheck();
   smlist.DoRefresh();
   
   smlist.getSMItems();
   smlist.loggedName = '20_H1BCAP_SLL';
    smlist.getSMItems();
    
   smlist.loggedName = '20_H1BCAP_PPD';
    smlist.getSMItems();
    
   smlist.rejectRecord();
   smlist.approveRecord();
      smlist.loggedName = '20_H1BCAP_SM';
   smlist.approveRecord();
   
   smlist.realignRecord();
      smlist.loggedName = '20_H1BCAP_SLL';

      smlist.realignAction();
      smlist.SMFlag=0;
      smlist.SMComments='test1';
            smlist.rejectRecord();
        smlist.SMFlag=1;
        smlist.SMComments='test111';
       smlist.rejectRecord();
         smlist.loggedName = '20_H1BCAP_SM';
         capobj.WCT_H1BCAP_Rejected_Reason_from_SM__c = 'Test Comments';
       smlist.rejectRecord();    
   //     smlist.loggedName = '20_H1BCAP_SLL';
   smlist.approveRecord();
   smlist.realignRecord();
   smlist.filterSMId='All-SM';
   smlist.processSMRequests();
      smlist.filterSMId='SMApproved';
   smlist.processSMRequests();
      smlist.filterSMId='SMRejected';
   smlist.processSMRequests();
    smlist.filterSMId='SMRealigned';
   smlist.processSMRequests();
   

      smlist.filterSMId='All-SLL';
     // smlist.realignAction();
   smlist.processSMRequests();
      smlist.filterSMId='SLLApproved';
   smlist.processSMRequests();
      smlist.filterSMId='SLLRejected';
   smlist.processSMRequests();
    smlist.filterSMId='SLLRealigned';
   smlist.processSMRequests();

   
      smlist.filterPPDId='All-SLL';
      //smlist.realignAction();
   smlist.processPPDRequests();
      smlist.filterPPDId='SLLApproved';
   smlist.processPPDRequests();
      smlist.filterPPDId='SLLRejected';
   smlist.processPPDRequests();
    smlist.filterPPDId='SLLRealigned';
   smlist.processPPDRequests();
          smlist.loggedName = '20_H1BCAP_SLL';
   smlist.approveRecord();
   smlist.realignRecord();

   
   Test.stopTest();
    }
  }
  
 static testMethod void myUnitTest1() {

    Test.startTest();
    
    
  Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_RM']; 
  User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','svalluru@deloitte.com');
  insert platuser;
  
  Profile PPPD = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_PPD']; 
  User platPPDuser=WCT_UtilTestDataCreation.createUser( 'sivappd',PPPD.id,' svallurutestppd@deloitte.com.preprod','svalluruppd@deloitte.com');
  insert platPPDuser;
  Profile PSLL = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SLL']; 
  User platSLLuser=WCT_UtilTestDataCreation.createUser( 'sivasll',PSLL.id,' svallurutestsll@deloitte.com.preprod','svallurusll@deloitte.com');
  insert platSLLuser;
  Profile PSM = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SM']; 
  User platSMuser=WCT_UtilTestDataCreation.createUser( 'siva283',PSM.id,' svallurutest99@deloitte.com.preprod','svalluru1@deloitte.com');
  insert platSMuser;
  system.runas(platSLLuser){
  
   Recordtype rt=[select id from Recordtype where DeveloperName = 'WCT_Employee'];
   Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
   con.Email = 'test1@gmail.com';
   insert con;
 

   Contact consm=WCT_UtilTestDataCreation.createEmployee(rt.id);
   consm.Email = 'testrm@gmail.com';
   insert consm;
  
   WCT_H1BCAP__c capobj= new WCT_H1BCAP__c();
   //user platSMuser= new user();
   //user platSLLuser= new user();
   
   capobj.Current_FY_Nomination__c = true;
   capobj.WCT_H1BCAP_Email_ID__c ='test1@gmail.com';
   capobj.WCT_H1BCAP_GDM_Email_id__c ='testrm@gmail.com';
   capobj.WC_H1BCAP_USI_SLL_email_id__c ='svallurusll@deloitte.com';
   capobj.WCT_H1BCAP_US_PPD_mail_Id__c ='test1@gmail.com';
   capobj.WCT_H1BCAP_Status__c ='Ready for SLL approval';
   capobj.WCT_H1BCAP_Resource_Manager_Email_ID__c= 'test@deloitte.com';
  
   capobj.WCT_H1BCAP_USI_SLL_Name__c =platSLLuser.id;    
   
   capobj.WCT_H1BCAP_Practitioner_Name__c =con.id;
   insert capobj;
 
   Boolean b = true;
  //List<wrapAccount> wrapAccountList = new List<wrapAccount>();
  
   PageReference pageRef = Page.WCT_ApprovalList_View;
    Test.setCurrentPage(pageRef); 
   WCT_SMList_View smlist = new WCT_SMList_View();
      smlist.loggedName = '';
      WCT_SMList_View.wrapAccount wrap = new WCT_SMList_View.wrapAccount(b,capobj);
     smlist.pageError=false;
     smlist.pageErrorMessage='testing';
  // smlist.wrap.selected=true;

  //list<WCT_SMList_View.wrapAccount> smlist.getSMItems();
 // wrap.realignRecord(b,capobj); 
   smlist.loggedName = '20_H1BCAP_SM';
   smlist.SMComments='test';
   smlist.SLLComments='test';
   smlist.SMRealignComments='test';
   System.debug('log_name::'+smlist.loggedName);

   smlist.LoggedInUserEmail='testrm@gmail.com'; 
   System.debug('log_email::'+smlist.LoggedInUserEmail);
   //smlist= new WCT_SMList_View();
   smlist.rejectAction();
   smlist.cancelAction();
   smlist.realignAction();
   smlist.cancelRealignAction();
   smlist.callCheck();
   smlist.DoRefresh();
   
   smlist.getSMItems();
   smlist.loggedName = '20_H1BCAP_SLL';
    smlist.getSMItems();
    
   smlist.loggedName = '20_H1BCAP_PPD';
    smlist.getSMItems();
    
   smlist.rejectRecord();
   smlist.approveRecord();
      smlist.loggedName = '20_H1BCAP_SM';
   smlist.approveRecord();
   
   smlist.realignRecord();
      smlist.loggedName = '20_H1BCAP_SLL';

      smlist.realignAction();
      smlist.SMFlag=0;
            smlist.rejectRecord();
smlist.SMFlag=1;
       smlist.rejectRecord();
         smlist.loggedName = '20_H1BCAP_SM';
         capobj.WCT_H1BCAP_Rejected_Reason_from_SM__c = 'Test Comments';
       smlist.rejectRecord();    
   //     smlist.loggedName = '20_H1BCAP_SLL';
   smlist.approveRecord();
   smlist.realignRecord();
   smlist.filterSMId='All-SM';
   smlist.processSMRequests();
      smlist.filterSMId='SMApproved';
   smlist.processSMRequests();
      smlist.filterSMId='SMRejected';
   smlist.processSMRequests();
    smlist.filterSMId='SMRealigned';
   smlist.processSMRequests();
   

      smlist.filterSMId='All-SLL';
     // smlist.realignAction();
   smlist.processSMRequests();
      smlist.filterSMId='SLLApproved';
   smlist.processSMRequests();
      smlist.filterSMId='SLLRejected';
   smlist.processSMRequests();
    smlist.filterSMId='SLLRealigned';
   smlist.processSMRequests();

   
      smlist.filterPPDId='All-SLL';
      //smlist.realignAction();
   smlist.processPPDRequests();
      smlist.filterPPDId='SLLApproved';
   smlist.processPPDRequests();
      smlist.filterPPDId='SLLRejected';
   smlist.processPPDRequests();
    smlist.filterPPDId='SLLRealigned';
   smlist.processPPDRequests();
          smlist.loggedName = '20_H1BCAP_SLL';
   smlist.approveRecord();
   smlist.realignRecord();

   
   Test.stopTest();
    }
  } 
  
  static testMethod void myUnitTestppd() {

    Test.startTest();
    
    
  Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_RM']; 
  User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','svalluru@deloitte.com');
  insert platuser;
  
  Profile PPPD = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_PPD']; 
  User platPPDuser=WCT_UtilTestDataCreation.createUser( 'sivappd',PPPD.id,' svallurutestppd@deloitte.com.preprod','svalluruppd@deloitte.com');
  insert platPPDuser;
  Profile PSLL = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SLL']; 
  User platSLLuser=WCT_UtilTestDataCreation.createUser( 'sivasll',PSLL.id,' svallurutestsll@deloitte.com.preprod','svallurusll@deloitte.com');
  insert platSLLuser;
  Profile PSM = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SM']; 
  User platSMuser=WCT_UtilTestDataCreation.createUser( 'siva283',PSM.id,' svallurutest99@deloitte.com.preprod','svalluru1@deloitte.com');
  insert platSMuser;
  system.runas(platPPDuser){
  
   Recordtype rt=[select id from Recordtype where DeveloperName = 'WCT_Employee'];
   Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
   con.Email = 'test1@gmail.com';
   insert con;
 

   Contact consm=WCT_UtilTestDataCreation.createEmployee(rt.id);
   consm.Email = 'testrm@gmail.com';
   insert consm;
  
   WCT_H1BCAP__c capobj= new WCT_H1BCAP__c();
   //user platSMuser= new user();
   //user platSLLuser= new user();
   
   capobj.Current_FY_Nomination__c = true;
   capobj.WCT_H1BCAP_Email_ID__c ='test1@gmail.com';
   capobj.WCT_H1BCAP_GDM_Email_id__c ='testrm@gmail.com';
   capobj.WC_H1BCAP_USI_SLL_email_id__c ='svallurusll@deloitte.com';
   capobj.WCT_H1BCAP_US_PPD_mail_Id__c ='svalluruppd@deloitte.com';
   capobj.WCT_H1BCAP_Status__c ='Ready for SLL approval';
   capobj.WCT_H1BCAP_Resource_Manager_Email_ID__c= 'test@deloitte.com';
  
   capobj.WCT_H1BCAP_USI_SLL_Name__c =platSLLuser.id;    
   
   capobj.WCT_H1BCAP_Practitioner_Name__c =con.id;
   insert capobj;
 
   Boolean b = true;
  //List<wrapAccount> wrapAccountList = new List<wrapAccount>();
  
   PageReference pageRef = Page.WCT_ApprovalList_View;
    Test.setCurrentPage(pageRef); 
   WCT_SMList_View smlist = new WCT_SMList_View();
      smlist.loggedName = '';
      WCT_SMList_View.wrapAccount wrap = new WCT_SMList_View.wrapAccount(b,capobj);
     smlist.pageError=false;
     smlist.pageErrorMessage='testing';
  // smlist.wrap.selected=true;

  //list<WCT_SMList_View.wrapAccount> smlist.getSMItems();
 // wrap.realignRecord(b,capobj); 
   smlist.loggedName = '20_H1BCAP_SM';
   smlist.SMComments='test';
   smlist.SLLComments='test';
   smlist.SMRealignComments='test';
   System.debug('log_name::'+smlist.loggedName);

   smlist.LoggedInUserEmail='testrm@gmail.com'; 
   System.debug('log_email::'+smlist.LoggedInUserEmail);
   //smlist= new WCT_SMList_View();
   smlist.rejectAction();
   smlist.cancelAction();
   smlist.realignAction();
   smlist.cancelRealignAction();
   smlist.callCheck();
   smlist.DoRefresh();
   
   smlist.getSMItems();
   smlist.loggedName = '20_H1BCAP_SLL';
    smlist.getSMItems();
    
   smlist.loggedName = '20_H1BCAP_PPD';
    smlist.getSMItems();
    
   smlist.rejectRecord();
   smlist.approveRecord();
      smlist.loggedName = '20_H1BCAP_SM';
   smlist.approveRecord();
   
   smlist.realignRecord();
      smlist.loggedName = '20_H1BCAP_SLL';

      smlist.realignAction();
      smlist.SMFlag=0;
            smlist.rejectRecord();
smlist.SMFlag=1;
       smlist.rejectRecord();
         smlist.loggedName = '20_H1BCAP_SM';
         capobj.WCT_H1BCAP_Rejected_Reason_from_SM__c = 'Test Comments';
       smlist.rejectRecord();    
   //     smlist.loggedName = '20_H1BCAP_SLL';
   smlist.approveRecord();
   smlist.realignRecord();
   smlist.filterSMId='All-SM';
   smlist.processSMRequests();
      smlist.filterSMId='SMApproved';
   smlist.processSMRequests();
      smlist.filterSMId='SMRejected';
   smlist.processSMRequests();
    smlist.filterSMId='SMRealigned';
   smlist.processSMRequests();
   

      smlist.filterSMId='All-SLL';
     // smlist.realignAction();
   smlist.processSMRequests();
      smlist.filterSMId='SLLApproved';
   smlist.processSMRequests();
      smlist.filterSMId='SLLRejected';
   smlist.processSMRequests();
    smlist.filterSMId='SLLRealigned';
   smlist.processSMRequests();

   
      smlist.filterPPDId='All-SLL';
      //smlist.realignAction();
   smlist.processPPDRequests();
      smlist.filterPPDId='SLLApproved';
   smlist.processPPDRequests();
      smlist.filterPPDId='SLLRejected';
   smlist.processPPDRequests();
    smlist.filterPPDId='SLLRealigned';
   smlist.processPPDRequests();
          smlist.loggedName = '20_H1BCAP_SLL';
   smlist.approveRecord();
   smlist.realignRecord();
   
   WCT_SMList_View wctlistvar = new WCT_SMList_View();
   
       wctlistvar.pageError_SM = false;
       wctlistvar.pageErrorMessage_SM ='String';
       wctlistvar.pageError_SM_Real = false;
       wctlistvar.pageErrorMessage_SM_Real = 'String';
   

   
   Test.stopTest();
    }
  } 
  
  
  
  
}