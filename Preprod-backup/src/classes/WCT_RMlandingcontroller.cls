public class WCT_RMlandingcontroller {
 
 
 public String profileName{get;set;}
 public Boolean logFlag{get;set;}
 
 /* CONSTRUCTOR */
 
 
 public WCT_RMlandingcontroller() {
 
 init();
 }
 
 public PageReference init() {
 
 /* Querying Profile Name For HomePage Component to redirect to VF - Communities Licenses have no access to Query Profile thought JS. :( )*/
    User UserInf =[Select profile.Name from User where id=:UserInfo.getUserId()];
    profileName = UserInf.profile.Name;
    
   
    if(profileName == '20_H1BCAP_RM' || profileName == '20_H1BCAP_SUPERRM') {
    logFlag=true;
    
    }
    else {
      logFlag = false;
        
    }
   
 return null;
 }
}