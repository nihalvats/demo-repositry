@isTest
public class WCT_Mobility_QASession_Test {

    static testMethod void UnitTest(){
            
            PageReference pageRef = Page.WCT_Mobility_QASession;
            Test.setCurrentPage(pageRef); 
            
            WCT_Mobility_QASession controller=new WCT_Mobility_QASession();
              test.startTest();
            recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
            Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
            insert con;
            
            WCT_Mobility__c mobRec1 = new WCT_Mobility__c();
            mobRec1.WCT_Mobility_Employee__c = con.Id;
            mobRec1.WCT_Mobility_Status__c = 'Onboarding Completed';
           // mobRec1.CT_Mobility_Type__c = 'Employment Visa';
            mobRec1.WCT_Q_A_Session_attended__c=true;
            mobRec1.WCT_Visa_Brief_Session__c=false;
            mobRec1.WCT_First_Working_Day_in_US__c = Date.Today().AddDays(3);
            INSERT mobRec1;
            
             
            WCT_Task_Reference_Table__c refRec2 = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
            refRec2.WCT_Assigned_to__c = 'Employee';
            refRec2.WCT_Visa_Type__c = 'Employment Visa';
            insert refRec2;
            
            
            Task t1= new Task();
            t1.Subject='Attend Q&A session';
            t1.Status='Not Started';
            t1.WCT_Task_Reference_Table_ID__c= refRec2.id;
            t1.WhatId = mobRec1.Id;
            t1.Task_Type__c = 'New';
            t1.whoId = con.Id;
            t1.WCT_Is_Visible_in_TOD__c=true;
            t1.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Immigration').getRecordTypeId();
            insert t1;
            
            
            Task t2= new Task();
            t2.Subject='Complete pre-departure training session';
            t2.Status='Completed';
            t2.WCT_Task_Reference_Table_ID__c= refRec2.id;
            t2.WhatId = mobRec1.Id;
            t2.Task_Type__c = 'New';
            t2.whoId = con.Id;
            t2.WCT_Is_Visible_in_TOD__c=false;
            t2.RecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Mobility').getRecordTypeId();
            insert t2;
            
            controller.getContacts();
             list<WCT_Mobility_QASession.cContact> cmp =controller.getContacts();
            
            for(WCT_Mobility_QASession.cContact c1 : cmp)
            {
                c1.selected = true;
                
            }
            //system.debug('cmp'+cmp);
            //cmp.selected = true;
            //system.debug('cmp1'+cmp);
            controller.processSelected();
            controller.geteventRegObj();
            
            recordtype rt1=[select id from recordtype where DeveloperName = 'Event_GM_I'];
            Event__c gmiEvent = new Event__c();
            gmiEvent.Name = 'Mobility Event';
            gmiEvent.Start_Date_Time__c = DateTime.now();
            gmiEvent.End_Date_Time__c = DateTime.now();
            gmiEvent.RecordTypeId = rt1.id;
            gmiEvent.Is_Event_Information_Changed__c = false;
            gmiEvent.RecordTypeId=Schema.SObjectType.Event__c.getRecordTypeInfosByName().get('GM&I').getRecordTypeId();
            insert gmiEvent;
            
            
            recordtype rt2=[select id from recordtype where DeveloperName = 'Event_Regristration_GM_I'];
            Event_Registration_Attendance__c Eventreg = new Event_Registration_Attendance__c();
            Eventreg.Event__c = gmiEvent.id;
            Eventreg.Contact__c = con.id;
            Eventreg.WCT_MobilityName__c = mobRec1.id;
            Eventreg.WCT_Is_Mobility_record__c = true;
            Eventreg.Attended__c = true;
            Eventreg.Is_Event_Information_Changed__c = false;
            Eventreg.RecordTypeId = rt2.id;
            insert Eventreg;
            
           // WCT_Mobility_QASession.cContact cmp = new WCT_Mobility_QASession.cContact(mobRec1);
                       
            controller.AddeventRegistrations();
            controller.getSelectedSize();
           // controller.selectAll();
               test.stopTest();
             
    }
}