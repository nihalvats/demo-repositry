@isTest
private class sObjectCreateManualTasks_Test{

    public static Contact Employee;
    public static List<WCT_Leave__c> leaveRecList;
    public static WCT_List_Of_Names__c lonRec,lonRecM,lonRecY,lonRecY1,lonRecM1,lonRec1,lonRecY2;
    
    /** 
        Method Name  : createEmployee
        Return Type  : Contact
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createEmployee()
    {
        Employee=WCT_UtilTestDataCreation.createEmployee(WCT_Util.getRecordTypeIdByLabel('Contact','Employee'));
        insert Employee;
        return  Employee;
    }  

    /** 
        Method Name  : createLeavesForEmp
        Return Type  : List<WCT_Leave__c>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static List<WCT_Leave__c> createLeavesForEmp(){
        leaveRecList = WCT_UtilTestDataCreation.createLeaves(WCT_Util.getRecordTypeIdByLabel('WCT_Leave__c','Personal'),Employee);
        insert leaveRecList;
        return leaveRecList;
    }
    
    private static WCT_List_Of_Names__c createLON(){
        lonRec = WCT_UtilTestDataCreation.createListOfNames('LeaveTask',WCT_Util.getRecordTypeIdByLabel('WCT_List_Of_Names__c','sObject Task Reference'),'Task Reference');
        lonRec.WCT_Criteria_for_Due_Date__c = 'Today';
        lonRec.WCT_Due_Date__c = 1;
        lonRec.Time_Frame_for_Task__c = 'Days';
        lonRec.WCT_Task_for_Object__c = 'WCT_Leave__c';
        lonRec.WCT_Task_Subject__c = 'Test';
        lonRec.WCT_Task_Assigned_User__c = userInfo.getUserId();
        //insert lonRec;
        return lonRec;
    }
    
    static testMethod void LeaveTask(){
        Employee = createEmployee();
        leaveRecList = createLeavesForEmp();
        lonRec = createLON();
        insert lonRec;
        
        lonRecM = createLON();
        lonRecM.Time_Frame_for_Task__c = 'Months';
        insert lonRecM;
        
        lonRecY = createLON();
        lonRecY.Time_Frame_for_Task__c = 'Years';
        insert lonRecY;
        
        lonRec1 = createLON();
        lonRec1.WCT_Criteria_for_Due_Date__c = 'WCT_Return_to_work_date__c';
        insert lonRec1;
        
        lonRecM1 = createLON();
        lonRecM1.Time_Frame_for_Task__c = 'Months';
        lonRecM1.WCT_Criteria_for_Due_Date__c = 'WCT_Return_to_work_date__c';
        insert lonRecM1;
        
        lonRecY1 = createLON();
        lonRecY1.Time_Frame_for_Task__c = 'Years';
        lonRecY1.WCT_Criteria_for_Due_Date__c = 'WCT_Return_to_work_date__c';
        lonRecY1.WCT_Task_Assigned_User__c = null;
        insert lonRecY1;
        
        lonRecY2 = createLON();
        lonRecY2.Time_Frame_for_Task__c = 'Years';
        lonRecY2.WCT_Criteria_for_Due_Date__c = '';
        insert lonRecY2;
        
        ApexPages.currentPage().getParameters().put('id', leaveRecList[0].id);
        ApexPages.currentPage().getParameters().put('objType', 'WCT_Leave__c');
        sObjectCreateManualTasks socmt = new sObjectCreateManualTasks();
        socmt.taskWrapListToDisplay[0].isSelected = true;
        socmt.taskWrapListToDisplay[1].isSelected = true;
        socmt.taskWrapListToDisplay[2].isSelected = true;
        socmt.taskWrapListToDisplay[3].isSelected = true;
        socmt.taskWrapListToDisplay[4].isSelected = true;
        socmt.taskWrapListToDisplay[5].isSelected = true;
        socmt.taskWrapListToDisplay[6].isSelected = true;
        socmt.createTasks();
        socmt.Cancel();
    
    }
    
    
}