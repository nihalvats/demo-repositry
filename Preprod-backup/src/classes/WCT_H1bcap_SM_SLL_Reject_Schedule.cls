global class WCT_H1bcap_SM_SLL_Reject_Schedule  implements Schedulable {

     global void execute(SchedulableContext sc) {

         

       //Define  batch size.       

       integer BATCH_SIZE = 200; 

      WCT_H1bcap_SM_SLL_Reject sndBatch = new WCT_H1bcap_SM_SLL_Reject();
     system.debug('****WCT_H1bcap_SM_SLL_Reject : starting batch exection*****************');

     Id batchId = database.executeBatch(sndBatch , BATCH_SIZE);   

  system.debug('**** WCT_H1bcap_SM_SLL_Reject: Batch executed batchId: '+batchId);

   }
 }