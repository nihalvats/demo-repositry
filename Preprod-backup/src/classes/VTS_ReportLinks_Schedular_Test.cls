@isTest
public class VTS_ReportLinks_Schedular_Test 
{
    public static testmethod void test1()
    {
        
        /*Test Report Type - Group Map. Date */
        VTS_Report_Link_Group_Mapping__c tempMapping = new VTS_Report_Link_Group_Mapping__c();
        tempMapping.Name='H1B IDS Group';
        tempMapping.Report_Link_Type__c='IDS';
        tempMapping.Email_Template_Id__c='00X18000000DpXKEA7';
        insert tempMapping;
        
        String recordTypeIDs= Schema.SObjectType.WCT_List_Of_Names__c.getRecordTypeInfosByName().get('VTS Report Links').getRecordTypeId();
        WCT_List_Of_Names__c reportLinks1= new WCT_List_Of_Names__c(Name='Sample Report 1', WCT_Type__c='IDS',VTS_Record_Type_ID__c='dummyID', VTS_IsReport__c=true, recordTypeID=recordTypeIDs);
        WCT_List_Of_Names__c reportLinks2= new WCT_List_Of_Names__c(Name='Sample Report 2', WCT_Type__c='IDS',VTS_Record_Type_ID__c='dummyID', VTS_IsReport__c=false, recordTypeID=recordTypeIDs);

        List<WCT_List_Of_Names__c> listOfNames= new List<WCT_List_Of_Names__c>();
        listOfNames.add(reportLinks1);
        listOfNames.add(reportLinks2);
        insert listOfNames;
        
        
        VTS_ReportLinks_Schedular sch= new VTS_ReportLinks_Schedular();
        sch.execute(null);
    }

}