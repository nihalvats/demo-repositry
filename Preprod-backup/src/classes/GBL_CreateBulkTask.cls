public class GBL_CreateBulkTask {
    public string recordId {get; set;}
    public string configName {get; set;}
    public string sobjectTypeStr {get; set;}
    public sObject sObjectRecord{get; set;}
    public List<TaskCanBeCreated> tasksAvailableToCreate{get; set;}
    
    /*Maps to hold the fields.*/
    public Map<String, String> taskFiltersValueMap;
    public Map<integer, String> taskFiltersAPINamMap;
    
    /*Error Related Variable*/
    public boolean isError{get; set;}
    public string errorMessage{get; set;}
    
    
    public GBL_CreateBulkTask()
    {
        /*Initilaize */
        taskFiltersValueMap= new Map<String , string>();
        taskFiltersAPINamMap= new Map<integer, String>();
        tasksAvailableToCreate= new List<TaskCanBeCreated> ();
        isError=false;
        errorMessage='';
        configName='';
        
        /*
         * GENERIC CODE FOR BULK TASK CREATION.
         * 
         * STEP 1 : Get the id of the record for which the task needs to be created.
         * STEP 2 : Find the sobject type using the Id.
         * STEP 3 : Find the filters used in the Task Reference table for this particular sobject Type, and create map of taskFiltersValueMap with keys.
         *          Query a dummy task reference for sobject Type. Query all filter fields to get API name of the filters needed.
         *          Currently have 5 filters.
         *          
         * STEP 4 : Build dynamic query for sObject type to the values for the filters needed to find the task associated with this record id. and update the taskFiltersValueMap with values to the filters.
         * STEP 5 : Build the dynamic query for Task Reference Table, with the param filters with the above quried vaues.
         */
        tasksAvailableToCreate= new List<TaskCanBeCreated>();
        
        /*
         * STEP 1 : Get the id of the record for which the task needs to be created.
         */
        recordId=ApexPages.currentPage().getParameters().get('recordId');
        configName=ApexPages.currentPage().getParameters().get('configName');
        system.debug('## id #'+recordId+'#');
        Id recId= recordId;
        if(recId!=null && configName!=null && configName!='')
        {
            /*
             *  STEP 2 : Find the object type of the Id.
             */
           
            Schema.sObjectType sObjectTypeObj=recId.getSobjectType(); 
            sobjectTypeStr=sObjectTypeObj.getDescribe().getName(); 
            System.debug(''+sobjectTypeStr);
            
                
            
            /*
             * STEP 3 : Find the filters used in the Task Reference table for this particular sobject Type, and create map of taskFiltersValueMap with keys.
             *          Query a dummy task reference for sobject Type. Query all filter fields to get API name of the filters needed.
             *          Currently have 5 filters, if anything more add the same here.
             */
            List<WCT_Task_Reference_Table__c> dummyTaskRefernce= [Select  GBL_Filter_1__c, GBL_Filter_2__c, GBL_Filter_3__c, GBL_Filter_4__c, GBL_Filter_5__c From WCT_Task_Reference_Table__c where WCT_Task_for_Object__c=:sobjectTypeStr and GBL_Config_Type__c=:configName];
            System.debug(' dummyTaskRefernce '+dummyTaskRefernce);
            
            /*Checking to have atleast one Task Reference record for this object Type.*/
            if(dummyTaskRefernce.size()>0)
            {
                sObject temp=dummyTaskRefernce[0];
                
                /*Currenly having only 5 filter fields , if added more add here. */
                for(integer i=1; i<=5; i++)
                {
                    if(temp.get('GBL_Filter_'+i+'__c')!=null)
                    {
                        /*All the API names of the filter which are used in Task Reference for this sepecific Object Type and COnfig Type. */
                        taskFiltersValueMap.put(String.valueof(temp.get('GBL_Filter_'+i+'__c')), null);
                        taskFiltersAPINamMap.put(i, String.valueof(temp.get('GBL_Filter_'+i+'__c')));
                    }
                }
                
                /*STEP 4 : Build dynamic query for sObject type to the values for the filters needed to find the task associated with this record id. and update the taskFiltersValueMap with values to the filters.*/
                
                String dymanicQuery='Select id, Owner.Name, OwnerId';
                system.debug('##'+dymanicQuery);
                for(String field: taskFiltersValueMap.keySet())
                {
                    if(field!=null && field!='')
                    {
                         dymanicQuery+=', ';
                         dymanicQuery+=field;
                    }
                }
                dymanicQuery+=' From '+sobjectTypeStr;
                dymanicQuery+=' where id=\''+recId+'\'';
                System.debug('## Query '+dymanicQuery);
                
                List<sObject> sobjects= Database.query(dymanicQuery);
                
                if(sobjects.size()>0)
                {
                    sObjectRecord=sobjects[0];
                    for(String field: taskFiltersValueMap.keySet())
                    {
                        if(field.contains('.'))
                        {
                            /*
                             * For the case such as filter is RecordType.Name. We cannot direclty get the value of multilevel from sobject. 
                             * Access multilevel Value using sObject.
                             */
                            system.debug('Field with .'+field);
                            List<String> splittedAPIName=field.split('\\.');
                            system.debug('## After Splitted API Name '+splittedAPIName);
                            string filteredValue=null;
                            if(splittedAPIName.size()==2)
                                filteredValue=String.valueOf(sobjects[0].getsObject(splittedAPIName[0]).get(splittedAPIName[1]));
                            taskFiltersValueMap.put(field,filteredValue); 
                        }
                        else
                        {
                            taskFiltersValueMap.put(field,String.valueOf(sobjects[0].get(field))); 
                        }
                    }
                }
                System.debug('## taskFiltersValueMap '+taskFiltersValueMap);
                /*
                 * STEP 5 : Build the dynamic query for Task Reference Table, with the param filters with the above quried vaues.
                 *          using the taskfilterMap build above.
                 */
                
                    String dynamicQuery='Select Id, WCT_Task_Subject__c, WCT_Assigned_to__c, WCT_Due_Date__c, WCT_Criteria_Of_Due_Date__c From WCT_Task_Reference_Table__c ';
                    String whereClause='';
                    for(integer filterCount: taskFiltersAPINamMap.keySet())
                    {
                        if(taskFiltersValueMap.get(taskFiltersAPINamMap.get(filterCount))!=null)
                        {
                           whereClause= whereClause==''?' Where ': whereClause+' and ';
                            /* Build the where class such that the it fetch all the task configred with filter value equal to record id or filter value ='*'.   */
                           whereClause+='( GBL_Value_'+filterCount+'__c = \''+taskFiltersValueMap.get(taskFiltersAPINamMap.get(filterCount))+'\' or GBL_Value_'+filterCount+'__c =\'*\')';
                        }
                    }
                
                    whereClause+= whereClause==''?' Where ':' and ';
                    whereClause+=' WCT_Task_for_Object__c = \''+sobjectTypeStr+'\'';
                    dynamicQuery+=whereClause;
                    system.debug('## dynamic Query for Task refernce '+dynamicQuery);
                        
                    List<WCT_Task_Reference_Table__c> taskRefernceTables= Database.query(dynamicQuery);
                       for(WCT_Task_Reference_Table__c tempTask : taskRefernceTables)
                    {     
                        String assignedTo='';
                        String assignedToId='';
                        if(tempTask.WCT_Assigned_to__c=='Owner')
                        {
                            assignedTo=String.valueof(sObjectRecord.getSObject('Owner').get('Name'));
                            assignedToId=String.valueof(sObjectRecord.get('OwnerId'));
                        }
                        else
                        {
                           assignedTo=UserInfo.getName();
                           assignedToId=UserInfo.getUserId();
                        }
                        System.debug('Assigned to '+assignedTo);
                        tasksAvailableToCreate.add(new TaskCanBeCreated(tempTask,assignedTo,assignedToId,(integer)tempTask.WCT_Due_Date__c));
                    } 
                                
            }
            else
            {
                isError=true;
                errorMessage='Cannot find any task configured for this object Type.';
            }
            
        }
        else
        {
            isError=true;
            errorMessage='Invalid Request.';
        }
       
    }
    
    public class TaskCanBeCreated
    {
        public WCT_Task_Reference_Table__c task {get;set;}
        public String assigneTo {get; set;}
        public String assigneToId {get; set;}
        public boolean isSelected{get; set;}
        public Date dueDate{get; set;}
        
        public TaskCanBeCreated( WCT_Task_Reference_Table__c tempTask, String assigneToTemp,string assignedToIdTemp, integer duedays)
        {
            task=tempTask;
            isSelected=false;
            assigneTo=assigneToTemp;
            assigneToId=assignedToIdTemp;
            dueDate=Date.Today().addDays(duedays); 
        }
    }
    
    public PageReference createTask()
    {
        List<Task> taskToCreate = new List<Task>();
        
        for(TaskCanBeCreated selecteTask : tasksAvailableToCreate)
        { 
           if(selecteTask.isSelected)
           {
               Task tasktemp= new Task();
               /*For contact and lead the task are associated with "who" rather than the "WhatId" */
              if(sobjectTypeStr=='Contact'||sobjectTypeStr=='Lead')
              {
                 tasktemp.WhoId=recordId; 
              }
             else
               {
                   tasktemp.whatId=recordId;
               }
               
               tasktemp.Subject=selecteTask.task.WCT_Task_Subject__c;
               tasktemp.OwnerId=selecteTask.assigneToId;
               taskTemp.ActivityDate=selecteTask.dueDate;
               taskToCreate.add(tasktemp);
           }
        }
        if(taskToCreate.size()>0)
            insert taskToCreate;
        
        return new PageReference('/'+recordId);
    }
    
    

}