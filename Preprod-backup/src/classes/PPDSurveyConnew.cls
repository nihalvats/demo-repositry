public without sharing class PPDSurveyConnew extends SitesTodHeaderController{
    
    /*Public Variables*/
    public String radio3 { get; set; }
    public String radio2 { get; set; }
    public String radio1 { get; set; }
    Public case surveyQuestion { get; set; }
    Public case surveyQuestionnew { get; set; }
    Public case CaseAns { get; set; }
    public String pageErrorMessage {get; set;}
    public boolean pageError {get; set;}
    public String id1 { get; set; }
    public boolean Qform { get; set; }
    

    String use = UserInfo.getUserName();
    
    //constructor
    public PPDSurveyConnew(){
    
        surveyQuestion = new case();
        surveyQuestionnew = new case();
    
        id1 = apexpages.currentpage().getparameters().get('Param1');
        surveyQuestion = [select id,FCPA_A1__c,FCPA_A2__c,FCPA_A3__c,FCPA_Q1_Answer__c,FCPA_Q2_Answer__c,FCPA_Q3_Answer__c,FCPA_A_Name_of_the_requesting_third_part__c,
                            FCPA_A_His_Her_position__c,FCPA_A_Organization_name__c,FCPA_A_Known_connection__c,FCPA_Candidate_First_Name__c,FCPA_Candidate_Last_Name__c,
                            FCPA_PPD_First_Name__c,FCPA_PPD_Last_Name__c,FCPA_PPD_Email_Address__c,FCPA_PPD_Survey_Submitted__c from case where id =: id1];
        system.debug('surveyQuestion*********'+ surveyQuestion);
        Qform = surveyQuestion.FCPA_PPD_Survey_Submitted__c;
        system.debug('Qform*********'+ Qform);
        
        if(Qform == True)
        pageErrorMessage  = 'Form can not be Resubmitted';
    }
    
     // save button method and conditions for form
     public PageReference mysave() {
     
       pageError =false;
       surveyQuestionnew.FCPA_Q1_Answer__c=radio1;
       surveyQuestionnew.FCPA_Q2_Answer__c=radio2;
       system.debug('########surveyQuestion.FCPA_Q2_Answer__c'+surveyQuestion.FCPA_Q2_Answer__c);
       surveyQuestionnew.FCPA_Q3_Answer__c=radio3;
       surveyQuestionnew.FCPA_PPD_Survey_Submitted__c=true;
      
       //system.debug('activeUser*********'+ activeUser.email);
       system.debug('loggedInemail*********'+ loggedInContact.email);
       system.debug('surveyQuestion*********'+ surveyQuestion.FCPA_PPD_Email_Address__c);
       if(surveyQuestion.FCPA_PPD_Email_Address__c != loggedInContact.email){
              pageError =true;
              pageErrorMessage ='You are not authorised person to submit the form';  
              return null;
       }
       
       if(radio1=='Yes'){
              if(surveyQuestionnew.FCPA_A1__c=='' || surveyQuestionnew.FCPA_A1__c== null)
              {
              pageError =true;
              pageErrorMessage ='Please provide answer for Q1';  
              return null;  
              }
          } 
          if(radio2=='Yes'){
              if(surveyQuestionnew.FCPA_A_Name_of_the_requesting_third_part__c=='' || surveyQuestionnew.FCPA_A_Name_of_the_requesting_third_part__c== null || surveyQuestionnew.FCPA_A_His_Her_position__c=='' || surveyQuestionnew.FCPA_A_His_Her_position__c== null || surveyQuestionnew.FCPA_A_Organization_name__c=='' || surveyQuestionnew.FCPA_A_Organization_name__c== null || surveyQuestionnew.FCPA_A_Known_connection__c=='' || surveyQuestionnew.FCPA_A_Known_connection__c== null)
              {
              pageError =true;
              pageErrorMessage ='Please provide answer for Q2';  
              return null;  
              }
          } 
          if(radio3=='Yes'){
              if(surveyQuestionnew.FCPA_A3__c=='' || surveyQuestionnew.FCPA_A3__c== null)
              {
              pageError =true;
              pageErrorMessage ='Please provide answer for Q3';  
              return null;  
              }
          } 
        
          if(radio1==null){
              pageError =true;
              pageErrorMessage ='Please select answer for Q1';  
              return null;  
            }
          if(radio2==null){
              pageError =true;
              pageErrorMessage ='Please select answer for Q2';  
              return null;  
            } 
           if(radio3==null){
              pageError =true;
              pageErrorMessage ='Please select answer for Q3';  
              return null;  
            }     
        try{
        //Update surveyQuestion;
        surveyQuestionnew.Id= surveyQuestion.Id;
        
        update surveyQuestionnew;
        system.debug('#################sucesss');
        }catch(System.DmlException e){
            system.debug('#################error');
             return null;  
           } 
        
         //thank you page
        //PageReference ThankyouPage = Page.surveyThankyouPage;
        PageReference ThankyouPage = new PageReference('/apex/GBL_Page_Notification?id=' +id1+'&key=PPDThankyou');
        ThankyouPage.setRedirect(true);
        return ThankyouPage;
    }

}