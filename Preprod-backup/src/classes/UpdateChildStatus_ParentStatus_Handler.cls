/********************************************************************************************************************************
Apex class         : Update_child_from_parent_Handler
Description        : This is about ISBEFORE functionality Handler  class for UpdateChildStatus_ParentStatus_Handler.
Type                : Handler  Class for Trigger
Test Class         : Update_child_from_parent_HandlerTest

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Anji Reddy                 26/05/2016          100%                  <Req / Case #>                  Original Version
************************************************************************************************************************************/


public class UpdateChildStatus_ParentStatus_Handler {
    
    public  void updatemethod(list<wct_leave__c> leave) 
    {
        
        list<string> Initial_wct_Cost_Center_numbers = new list<string>();
        //list<string> Modified_WCT_Cost_Center_numbers = new list<string>()
        string[] Modified_WCT_Cost_Center_numbers = new string[]{};
        List<wct_leave__c> listleave = new list<wct_leave__c> ();
        Map<id, string> MapModifiedCCNumber = new map<id, string> ();
        
        for(wct_leave__c l:leave)
        {
            listleave.add(l);
            if(l.WCT_Cost_Center__c != '' && l.WCT_Cost_Center__c != null )
            {
                Initial_wct_Cost_Center_numbers.add(l.WCT_Cost_Center__c);
            }
            
            for( string s : Initial_wct_Cost_Center_numbers)
            {
                string new_WCT = s.replaceFirst('^0+(?!$)','');  // To Remove the Leading Zeros From the WCT_Cost_Center_Number..        
                Modified_WCT_Cost_Center_numbers.add('%'+new_WCT);
                system.debug('The modified Cost Center Number is   ---------------------->'+Modified_WCT_Cost_Center_numbers);
                MapModifiedCCNumber.put(l.id, new_WCT);
                system.debug('The new Map with Modified  values are ---------------------> '+MapModifiedCCNumber);
                system.debug('The new Map with Modified  values are ---------------------> '+MapModifiedCCNumber.get(l.id));
            }
        }
        
        list<Talent_Delivery_Contact_Database__c> listtalentcon = [select id, name,Us_Talent_Contact_4__c, India_Talent_Contact_1__c,India_Talent_Contact_2__c,Payroll_Contact_Email__c,US_Talent_Contact_1__c,US_Talent_Contact_3__c,US_Talent_Contact_2__c from Talent_Delivery_Contact_Database__c where name like :Modified_WCT_Cost_Center_numbers ];
        system.debug('the modified list --------->'+listtalentcon);
        
        for(wct_leave__c leaves : listleave)
        {
          boolean foundTCD=false;
            if(leaves.wct_employee__c !=null)
            {
              
                for(Talent_Delivery_Contact_Database__c t: listtalentcon)
                {   
                    system.debug('The TCD_Name Before Removing Zeros --------------------------->  '+t.name);
                    system.debug('New Modified  MAp is  having  result of ----------------------> '+MapModifiedCCNumber.get(leaves.Id));
                    string WCT_Cost_Center_Number =  MapModifiedCCNumber.get(leaves.Id);
                    system.debug('WCT_Cost_Center_Number--------'+WCT_Cost_Center_Number);
                    string Modified_Name = t.name.replaceFirst('^0+(?!$)','');
                    system.debug('The TCD_Name After REmoving Zeros -----------------------------> '+Modified_Name);
                    if(Modified_Name == WCT_Cost_Center_Number)
                    {
                            leaves.Talent_Contact_Database__c=t.id;
                            foundTCD=true;
                    }
                }
                
            }
            if(leaves.wct_employee__c ==null || foundTCD==false )
            {
                leaves.Talent_Contact_Database__c=null;
            }
        }
        
    }
    
}