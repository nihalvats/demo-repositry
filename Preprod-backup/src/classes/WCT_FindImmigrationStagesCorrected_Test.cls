/**
    * Class Name  : WCT_FindImmigrationStagesCorrected
    * Description : This apex test class will use to test the WCT_CreateImmigrationsHelper2
*/

@isTest
private class WCT_FindImmigrationStagesCorrected_Test{
    public static List<WCT_Immigration_Stagging_Table__c> immgStageList=new List<WCT_Immigration_Stagging_Table__c>();
    
     /** 
        Method Name  : createImmigrationStageTable
        Return Type  : List<WCT_Immigration_Stagging_Table__c>
        Type      : private
        Description  : Create Pre-Bi Stage Table Records.         
    */
    private Static List<WCT_Immigration_Stagging_Table__c> createImmigrationStageTable()
    {
      immgStageList = WCT_UtilTestDataCreation.createImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_CORRECTED);
      insert immgStageList;
      return  immgStageList;
    }
    
     /** 
        Method Name  : createImmigrationsHelperTestMethod
        Return Type  : Void
        Type      : private
        Description  : Create Immigration Stage Table Records and pass to CreateHelper.         
    */
    static testMethod void createImmigrationsHelperTestMethod() {
        immgStageList = createImmigrationStageTable();        
        Test.startTest();
        String returnStatement = WCT_FindImmigrationStagesCorrected.findImmigrationStagingRecords(WCT_UtilConstants.STAGING_STATUS_CORRECTED);
        Test.stopTest();
        System.assertEquals(Label.WCT_Message_Processing_Starting, returnStatement);
         
    }
    
    /** 
        Method Name  : noRecordFoundTestMethod
        Return Type  : Void
        Type      : private
        Description  : When there is no record to process.          
    */
    static testMethod void noRecordFoundTestMethod() {
      immgStageList = createImmigrationStageTable();
        Test.startTest();
        String msg=WCT_FindImmigrationStagesCorrected.findImmigrationStagingRecords(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        System.assertEquals(WCT_UtilConstants.PROCESS_NONE, msg);
    }
}