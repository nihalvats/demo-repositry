/**
    * Class Name  : WCT_DescribeCalls
    * Description : This apex class will use to get PickList Values for recordType. 
*/



public class WCT_DescribeCalls{
    
         
        public static Map<string,Set<String>> findPickListValues(String sObjectName,String recordTypeId)
        {
           
           Http http = new Http();
           HttpRequest req = new HttpRequest();
           String sEndPoint=Label.DescribecallEndPoint; 
           req.setEndpoint(sEndPoint);
           req.setMethod('POST');
           req.setHeader ('Content-Type','text/XML');
           req.setHeader('SOAPAction','""');
           req.setBody('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com"><soapenv:Header><urn:SessionHeader><urn:sessionId>'+UserInfo.getSessionId()+'</urn:sessionId></urn:SessionHeader></soapenv:Header><soapenv:Body><urn:describeLayout><urn:sObjectType>'+sObjectName+'</urn:sObjectType><urn:recordTypeIds>'+recordTypeId+'</urn:recordTypeIds></urn:describeLayout></soapenv:Body></soapenv:Envelope>');
           system.debug(req.getBody());
           HttpResponse res = http.send(req);
          
           Map<String,Set<String>> pickListMap=new Map<String,Set<String>>();
           
           Dom.Document doc = res.getBodyDocument();
           
           //Retrieve the root element for this document.
           Dom.XMLNode layout= doc.getRootElement();
           Dom.XMLNode findRecordTypeNode=layout.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/').getChildElement('describeLayoutResponse','urn:partner.soap.sforce.com').getChildElement('result','urn:partner.soap.sforce.com').getChildElement('recordTypeMappings','urn:partner.soap.sforce.com');
           
           
           for(Dom.XMLNode nodeXML:findRecordTypeNode.getChildElements())
           {
                String parentName=null;
                Set<String> strSet=new Set<String>();
                for(DOM.XMLNode nodeXMLChild:nodeXML.getChildElements())
                {
                    if(nodeXMLChild.getText()!=''){
                        parentName=nodeXMLChild.getText();
                        system.debug(parentName);
                        strSet=new Set<String>();
                    }
                    for(DOM.XMLNode grandChild:nodeXMLChild.getChildElements())
                    {
                        if(grandChild.getName()=='value' )
                        {
                            strSet.add(grandChild.getText());
                        }
                    }
    
                pickListMap.put(parentName,strSet);
    
                }
    
           }
           
           return pickListMap;
        }   

  
}