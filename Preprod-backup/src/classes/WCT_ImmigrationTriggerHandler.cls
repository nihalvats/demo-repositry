// Description : This class is called by Immigration Trigger
//
//
public class WCT_ImmigrationTriggerHandler{
 
//------------------------------------------------------------------------------------------
// Description : This method populates visa type to L2 when immigration record type is 'EAD'
//-------------------------------------------------------------------------------------------
public static void prePopulateVisaType(List<WCT_Immigration__c> lstImmigration){

    set<string> setRecordTypeIds = new set<string>();

    for(WCT_Immigration__c tmpImm : lstImmigration){
        setRecordTypeIds.add(tmpImm.RecordTypeId); 
    }

    List<RecordType> lstRecType = [SELECT Id,Name FROM RecordType WHERE id in : setRecordTypeIds];
    set<string> setRecordTypeName = new set<string>();
    Map<string,id> mapNameTypeId = new Map<string,id>();
    for(RecordType tmpRecTyp : lstRecType ){
        setRecordTypeName.add(tmpRecTyp.name);
        mapNameTypeId.put(tmpRecTyp.name,tmpRecTyp.id);
    }
    for(WCT_Immigration__c tmpImm : lstImmigration){
        if(tmpImm.RecordTypeId==mapNameTypeId.get('L2 with EAD')){
            tmpImm.WCT_Visa_Type__c = 'L2';
        }
        else if(tmpImm.RecordTypeId==mapNameTypeId.get('B1 Visa')){
            tmpImm.WCT_Visa_Type__c = 'B1 Individual';
        }
    }
  }
  
  
 public static void updateIsStatusChanged(Map<Id,WCT_Immigration__c> mapOldImmigration, List<WCT_Immigration__c > lstNewImmigration)
 {
     List<WCT_Immigration__c> lstImmigration = [select id,WCT_Immigration_Status__c,WCT_Is_Immigration_Status_Changed__c,(select id, subject, status from Tasks where status<>'Completed')
                                                  from WCT_Immigration__c where id in : mapOldImmigration.keySet()];
     Map<Id,WCT_Immigration__c> mapIdImm = new Map<Id,WCT_Immigration__c> (lstImmigration );
     for(WCT_Immigration__c tmpImm : lstNewImmigration){
         if(mapIdImm.get(tmpImm.id)!=null 
         && tmpImm.WCT_Immigration_Status__c!=mapIdImm.get(tmpImm.id).WCT_Immigration_Status__c 
         && mapIdImm.get(tmpImm.id).Tasks !=null && mapIdImm.get(tmpImm.id).Tasks.size()>0 ){
             tmpImm.WCT_Is_Immigration_Status_Changed__c=true;
         }    
     }
     
 }  


 public static void populateISTDateTime(List<WCT_Immigration__c> lstImmigration){

    for(WCT_Immigration__c tmpImm : lstImmigration){
        if(tmpImm.WCT_Visa_Interview_Date__c != null){
            tmpImm.Visa_Interview_Date_Time_IST__c = tmpImm.WCT_Visa_Interview_Date__c.format('MM/dd/yyyy h:mm a', System.Label.WCT_IST_Time_Zone);
        }
        if(tmpImm.WCT_OFC_Appointment_Date__c != null){
            tmpImm.WCT_OFC_Appointment_Date_Time_IST__c= tmpImm.WCT_OFC_Appointment_Date__c.format('MM/dd/yyyy h:mm a', System.Label.WCT_IST_Time_Zone);
        }
    }
 }
 
 public static void dependentFormSubmitted(List<WCT_Immigration__c> lstImmigration){
     for(WCT_Immigration__c tmpImm : lstImmigration){
         if(tmpImm.WCT_Dependent_Form_Submitted__c == true){
             List<WCT_Mobility__c> mobRec = new List<WCT_Mobility__c>();
             mobRec = [SELECT Id, Name, WCT_Mobility_Employee__c, WCT_Mobility_Status__c, RecordTypeId, ownerId FROM WCT_Mobility__c WHERE WCT_Mobility_Type__c = 'Employment Visa' AND Immigration__c =: tmpImm.Id LIMIT 1];
             List<Task> lstTask = new List<Task>();
             if(!mobRec.isEmpty()){
                 lstTask = [SELECT Id, WhatId, Status, Subject FROM Task WHERE WhatId =: mobRec[0].Id and Subject = 'Apply for dependent Visa'];
                 for(Task t : lstTask){
                     t.Status = 'Completed';
                 }
                 if(!lstTask.isEmpty())
                     update lstTask;
             }
             tmpImm.WCT_Dependent_Form_Submitted__c = false;
         }
     }       
 }
 

public static void PVLupdate(List<WCT_Immigration__c> ImgList,List<WCT_Immigration__c> oldList){
    // Map<ID, PVL_Database__c> pvlToUpdate = new Map<ID, PVL_Database__c>();   
    List<PVL_Database__c> pvlList = new List<PVL_Database__c>();
    List<Id> listIds = new List<Id>();
    List<Id> mapKeys;
    list<WCT_Immigration__c> immList = new list<WCT_Immigration__c>();
    for (WCT_Immigration__c childObj : ImgList){
        listIds.add(childObj.PVL_Database__c);
        if(listIds.size()==1)
            {
            for (WCT_Immigration__c childObj1 : oldList){
                listIds.add(childObj1.PVL_Database__c);
            }
            }
        }
    // pvlToUpdate = new Map<Id, PVL_Database__c>([Select id, Name,PVL_With_Immigration__c from PVL_Database__c WHERE ID IN :listIds]);
    pvlList = [SELECT id, PVL_With_Immigration__c,(SELECT ID, PVL_Database__c FROM Immigration_PVL__r) FROM PVL_Database__c WHERE ID IN :listIds];
    for (PVL_Database__c pvl_obj: pvlList){
        if(pvl_obj.Immigration_PVL__r.size() > 0) {
            pvl_obj.PVL_With_Immigration__c = true;
        }
        else {
            pvl_obj.PVL_With_Immigration__c = false;
        }
    }
    update pvlList;
    }
   
    
   /*
    * Method to check if any immigration is updated with "Petition Approved", then update the contact with has H1B checkbox.
     */ 
 public static void markEmployeeHasH1B(Map<Id, WCT_Immigration__c> lstImmigration_old, Map<Id, WCT_Immigration__c> lstImmigration_new )
 {
     List<String> contactsIdToUpdate= new List<string>();
     for(Id indexId: lstImmigration_old.keyset())
     {
         /*Checking Status is Updated.*/
         if(lstImmigration_old.get(indexId).H1B_Process_Status__c!=lstImmigration_new.get(indexId).H1B_Process_Status__c)
         {
            /*
                If the new updated Status is "Petition Approved"
            */
             if(lstImmigration_new.get(indexId).H1B_Process_Status__c=='Petition Approved')
             {
                if(lstImmigration_new.get(indexId).WCT_Assignment_Owner__c!=null) contactsIdToUpdate.add(lstImmigration_new.get(indexId).WCT_Assignment_Owner__c);
             }
             
         }
     }
     
     /*
      * Query the contact identified above to see if they are already marked a Has H1B. 
      * If not marked add them to list to update by checking the Is H1B Visa checkbox.
      */
     List<Contact> employeesToMark= new List<Contact>();
     if(contactsIdToUpdate.size()>0)
     {
         List<Contact> employees= [Select Id, H1B_Has_H1B__c from contact Where id in :contactsIdToUpdate ];
         
         for(Contact employee: employees)
         {
            if(employee.H1B_Has_H1B__c!='Yes')
             {
                 employee.H1B_Has_H1B__c='Yes';
                 employeesToMark.add(employee);
             }
            
         }
     }
     
     /*Update the employees who has new got the H1B visa.*/
     if(employeesToMark.size()>0)
     {
         update employeesToMark;
     }
     
     
 }
     
     // Updating the Email Field's field when manager's left the firm
       public static void contemailupdate (List<WCT_Immigration__c> lstImmigration)
       {
     system.debug('Entering in conlastloop****');
       list<WCT_Immigration__c> fnlstimm = new list<WCT_Immigration__c> (); 
       list<WCT_Immigration__c> fnlimm = new list<WCT_Immigration__c> (); 
       map<id,string> spocemail = new map<id,string>();
       map<id,string> rmemail = new map<id,string>();
       map<id,string> pmemail = new map<id,string>();  
       map<id,string> fsspocemail = new map<id,string>(); 
       map<id,string> pcemail = new map<id,string>();
        list<string> rmstring = new list<string>();
        string BSstring ;
        map<id,WCT_Immigration__c> mapspocrec = new map<id,WCT_Immigration__c>(); 
        map<id,WCT_Immigration__c> maprmrec = new map<id,WCT_Immigration__c>();
        map<id,WCT_Immigration__c> mappmrec = new map<id,WCT_Immigration__c>();
        map<id,WCT_Immigration__c> mapfsspcrec = new map<id,WCT_Immigration__c>();
        map<id,WCT_Immigration__c> mappcrec = new map<id,WCT_Immigration__c>();
           
        Set<Id> allContacts= new Set<Id>();   
         for(WCT_Immigration__c imm : lstImmigration)
		{
     		if(imm.H1B_Is_USI_Upload__c == true)
			{
			   
			  mapspocrec.put(imm.WCT_Business_SPOC__c,imm);
                allContacts.add(imm.WCT_Business_SPOC__c);
			  maprmrec.put(imm.WCT_Resource_Manager__c,imm);
                allContacts.add(imm.WCT_Resource_Manager__c);
			  mappmrec.put(imm.WCT_Project_Manager__c,imm);
                allContacts.add(imm.WCT_Project_Manager__c);
			  mapfsspcrec.put(imm.WCT_FSS_SPOC__c,imm);
                allContacts.add(imm.WCT_FSS_SPOC__c);
			  mappcrec.put(imm.WCT_Project_Controller__c,imm);
                allContacts.add(imm.WCT_Project_Controller__c);
            }
        }
		
		/**
			Suggested Imprvistion of the Code : 
				1. Get all the ids to set<Id>
				2. One Query to get all teh Contact.
				3. loop thourgh the contact resuelt and Check if there are in resepctive Map and permform the operation as needed. 
		**/
           
           
       if(allContacts.size()>0)
       {
           list<contact>  allContactsLst= [select id,Email from contact where id = :allContacts];
           for(contact tempCont: allContactsLst)
           {
               if(maprmrec.containsKey(tempCont.id))
               {
                   if(tempCont.Email <>null)
                   {
                       rmemail.put(tempCont.id,tempCont.Email);
                   }
                   
               }
               
               if(mapspocrec.containsKey(tempCont.id))
               {
                   if(tempCont.Email <>null)
                   {
                       spocemail.put(tempCont.id,tempCont.Email);
                   }
                   
               }
               
               if(mappmrec.containsKey(tempCont.id))
               {
                   if(tempCont.Email <>null)
                   {
                       pmemail.put(tempCont.id,tempCont.Email);
                   }
               }
               
               if(mapfsspcrec.containsKey(tempCont.id))
               {
                   if(tempCont.Email <>null)
                   {
                       fsspocemail.put(tempCont.id,tempCont.Email);
                   }
                   
               }
               
                if(mappcrec.containsKey(tempCont.id))
               {
                   if(tempCont.Email <>null)
                   {
                       pcemail.put(tempCont.id,tempCont.Email);
                   }
                   
               }
           }
       }
           
		
   /*   if(!maprmrec.isempty())
	  { 
      list<contact>  lstrmcon= [select id,Email from contact where id = : maprmrec.keyset()];
       
       for(contact c : lstrmcon){
       
       if(c.Email <> null){
        rmemail.put(c.id,c.Email);
       }
     } 
    } 


    
     if(!mapspocrec.isempty()){ 
      list<contact>  lstspoccon= [select id,Email from contact where id = : mapspocrec.keyset()];
       
       for(contact c : lstspoccon){
       system.debug('Entering in conlastloop****');
       if(c.Email <> null){
        spocemail.put(c.id,c.Email);
       }
     } 
     
    } 

    if(!mappmrec.isempty()){ 
      list<contact>  lstpmcon= [select id,Email from contact where id = : mappmrec.keyset()];
       
       for(contact c : lstpmcon){
           if(c.Email <> null){
        pmemail.put(c.id,c.Email);
       }
     } 
     
    } 
    
    if(!mapfsspcrec.isempty()){ 
      list<contact>  lstfsspocon= [select id,Email from contact where id = : mapfsspcrec.keyset()];
       
       for(contact c : lstfsspocon){
           if(c.Email <> null){
        fsspocemail.put(c.id,c.Email);
       }
     } 
     
    } 
    
     if(!mappcrec.isempty()){ 
      list<contact>  lstpccon= [select id,Email from contact where id = : mappcrec.keyset()];
       
       for(contact c : lstpccon){
           if(c.Email <> null){
        pcemail.put(c.id,c.Email);
       }
     } 
     
    } 
   */  
       
       // Capturing the Business Spoc Email's
           
        for(WCT_Immigration__c immg :lstImmigration){
            
        if(spocemail.containskey(immg.WCT_Business_SPOC__c))
           {
            
             BSstring = spocemail.get(immg.WCT_Business_SPOC__c);
             rmstring= BSstring.split('\\@');       
              
        if(rmstring.size()>0){
           if(rmstring[1] <> 'deloitte.com')
           {
          
           immg.WCT_Business_Spoc_Emailid__c= label.WCT_Talent_SFDC_Admin;
           fnlstimm.add(immg);
         
           }
           
           else {
                system.debug('Entering2 loop****');
                immg.WCT_Business_Spoc_Emailid__c= spocemail.get(immg.WCT_Business_SPOC__c);
           }
           
        }  
       
       }
        // Capturing FSS Spoc Email's
       if(fsspocemail.containskey(immg.WCT_FSS_SPOC__c))
           {
            
             BSstring = fsspocemail.get(immg.WCT_FSS_SPOC__c);
             rmstring= BSstring.split('\\@');       
              
        if(rmstring.size()>0){
           if(rmstring[1] <> 'deloitte.com')
           {
          
           immg.WCT_FSS_SPOC_Email_Id__c = label.WCT_Talent_SFDC_Admin;
           fnlstimm.add(immg);
         
           }
           
           else {
                system.debug('Entering2 loop****');
                immg.WCT_FSS_SPOC_Email_Id__c = fsspocemail.get(immg.WCT_FSS_SPOC__c);
           }
           
        }  
       
       }
       //Capturing the PC Email's
       if(pcemail.containskey(immg.WCT_Project_Controller__c))
           {
            
             BSstring = pcemail.get(immg.WCT_Project_Controller__c);
             rmstring= BSstring.split('\\@');       
              
        if(rmstring.size()>0){
           if(rmstring[1] <> 'deloitte.com')
           {
          
           immg.WCT_Project_Controller_Email_ID__c= label.WCT_Talent_SFDC_Admin;
           fnlstimm.add(immg);
         
           }
           
           else {
                system.debug('Entering2 loop****');
                immg.WCT_Project_Controller_Email_ID__c = pcemail.get(immg.WCT_Project_Controller__c);
           }
           
        }  
       
       }
       //Capturing PM Emails 
       if(pmemail.containskey(immg.WCT_Project_Manager__c))
           {
            
             BSstring = pmemail.get(immg.WCT_Project_Manager__c);
             rmstring= BSstring.split('\\@');       
              
        if(rmstring.size()>0){
           if(rmstring[1] <> 'deloitte.com')
           {
          
           immg.WCT_Project_Manager_Email_Id__c = label.WCT_Talent_SFDC_Admin;
           fnlstimm.add(immg);
         
           }
           
           else {
                system.debug('Entering2 loop****');
                immg.WCT_Project_Manager_Email_Id__c= pmemail.get(immg.WCT_Project_Manager__c);
           }
           
        }  
       
       }
       //Cpturing RM Emails
       if(rmemail.containskey(immg.WCT_Resource_Manager__c))
           {
              system.debug('RM email******'+rmemail.get(immg.WCT_Resource_Manager__c));
              BSstring = rmemail.get(immg.WCT_Resource_Manager__c);
            rmstring= BSstring.split('\\@');
            system.debug('RMString Value******'+rmstring);
        if(rmstring.size()>0){
           if(rmstring[1] <> 'deloitte.com')
           {
           system.debug('Entering1 loop****');
           immg.WCT_Resource_Manager_Email_ID__c= label.WCT_Talent_SFDC_Admin;
           system.debug('RMEmail*****'+immg.WCT_Resource_Manager_Email_ID__c);
           fnlstimm.add(immg);
           system.debug('Finalstsize*****'+fnlstimm);
           
           }
           
           else {
                system.debug('Entering2 loop****');
                immg.WCT_Resource_Manager_Email_ID__c= rmemail.get(immg.WCT_Resource_Manager__c);
           }
           
        }  
       
       }
       
    } 
     
  }
}