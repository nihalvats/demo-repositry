@isTest
public class LeaveSurvey_test{
    static Id employeeRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
    
    public static testMethod void test() {
   
         Account acc = new Account();
    acc.Name='INDIA';
    acc.phone='123456790';
    Insert acc;
    
    recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
    Contact con = WCT_UtilTestDataCreation.createEmployee(rt.id);
    con.Email = 'test.test2@deloitte.com';
    con.Dependent__c ='EE';
    con.Plan_Num__c = 371;
    con.WCT_Benefit_plan__c = 'Health';
    con.Accountid= acc.id;
    con.WCT_Personnel_Number__c = 12345;
    insert con;
    
        
    Profile prof = [select id from profile where name='system Administrator'];
     User usr = new User(alias = 'usr', email='test.test2@deloitte.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='test.test2@deloitte.com');
     insert usr;
     
    system.runas(usr){
   
       ApexPages.currentPage().getParameters().put('xLeaveid','id');
             
     recordtype rt1 =[select id from recordtype where DeveloperName = 'LOA_Code'];    
      WCT_Leave__c leav = new WCT_Leave__c();
        leav.WCT_Leave_Start_Date__c = system.today();
        leav.WCT_Leave_End_Date__c = system.today()+1;
        leav.WCT_Leave_Category__c = 'Disability Parental';
        leav.WCT_Employee__c = con.id;
        leav.WCT_Parental_Leave_Exhausted__c = true;
        leav.Recordtypeid = rt1.id;
        leav.WCT_Parental_Event__c = 'Birth';
       insert leav;
        
        leav.WCT_Leave_survey_submitted__c = True;
        Update leav;
       
       recordtype rt2 =[select id from recordtype where DeveloperName = 'Parental'];
       WCT_Leave__c leav1 = new WCT_Leave__c();
        leav1.WCT_Leave_Start_Date__c = system.today();
        leav1.WCT_Leave_End_Date__c = system.today()+1;
        leav1.WCT_Leave_Category__c = 'Disability Parental';
        leav1.WCT_Sub_Category_2__c='paid';
        leav1.WCT_Employee__c = con.id;
        leav1.WCT_Parental_Leave_Exhausted__c = false;
        leav1.WCT_Related_Record__c = leav.id;
        leav1.Recordtypeid = rt2.id;
      insert leav1;
      
  
       Test.starttest(); 
       
       PageReference pageRef = Page.Adoption;
       Test.setCurrentPage(pageRef); 
       
        LeaveSurveyController lea = new LeaveSurveyController();
        lea.childleavecnt = 0;
        lea.Insurence = true;
        lea.ptoweekcnt = 0;
        lea.ptohol =0;
        lea.RemainPTO = 0;
        lea.RemainPTOdays = 0;
        lea.InsurenceN = false;
        lea.intDaysRemain = 0;
        lea.Displaystr= 'test';
        lea.pageError = true;
        lea.pageErrorMessage ='Test message';
        lea.supportAreaErrorMesssage = 'Test Error message';
        lea.Displaystr1 = 'test2';
        lea.Displaystr2 = 'test3';
        lea.question1 = true;
        lea.question2 = true;
        lea.question3 = true;
        lea.ptoweekcount = 0;
        lea.paidweekcnt = 0;
        lea.paidDayCnt = 0;
        lea.paidleavecnt = 0;
        lea.ptoleavecnt = 0;
        lea.unpaidweekcnt = 1;
        lea.totalweeks = 1;
        lea.unpaidleavecnt  = 0;
        
        lea.saveleaveRecord();
        
        lea.pageerrormessage = 'Error';
       
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);

       Test.stoptest(); 
        
   }    
    }
    
    public static testMethod void test1() {
        
         Account acc = new Account();
        acc.Name='INDIA';
        acc.phone='123456790';
        Insert acc;
    
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con = WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email = 'test.test1@deloitte.com';
        con.Dependent__c ='EE+F';
        con.Plan_Num__c = 843;
        con.WCT_Benefit_plan__c = 'Health';
        con.Accountid= acc.id;
        con.WCT_Personnel_Number__c = 12345;
        insert con;
        
        Contact con1 = WCT_UtilTestDataCreation.createEmployee(rt.id);
        con1.Email = 'test.test2@deloitte.com';
        con1.Dependent__c ='EE+F';
        con1.Plan_Num__c = 843;
        con1.WCT_Benefit_plan__c = 'Health';
        con1.Accountid= acc.id;
        con1.WCT_Personnel_Number__c = 12345;
        insert con1;
    
        Profile prof = [select id from profile where name='System Administrator'];
        User usr = new User(alias = 'usr', email='test.test1@deloitte.com',
                   emailencodingkey='UTF-8', lastname='lstname',
                   timezonesidkey='America/Los_Angeles',
                   languagelocalekey='en_US',
                   localesidkey='en_US', profileid = prof.Id,
                   username='test.test1@deloitte.com');
        insert usr;
     
        system.runas(usr){
       
    
        ApexPages.currentPage().getParameters().put('xLeaveid','id');
             
        recordtype rt1 =[select id from recordtype where DeveloperName = 'LOA_Code'];    
        WCT_Leave__c leav = new WCT_Leave__c();
        leav.WCT_Leave_Start_Date__c = system.today();
        leav.WCT_Leave_End_Date__c = system.today()+1;
        leav.WCT_Leave_Category__c = 'Disability Parental';
        leav.WCT_Employee__c = con.id;
        leav.WCT_Parental_Leave_Exhausted__c = true;
        leav.Recordtypeid = rt1.id;
        leav.WCT_Parental_Event__c = 'Birth';
        insert leav;
        
        leav.WCT_Leave_survey_submitted__c = True;
        Update leav;
       
        recordtype rt2 =[select id from recordtype where DeveloperName = 'Parental'];
        WCT_Leave__c leav1 = new WCT_Leave__c();
        leav1.WCT_Leave_Start_Date__c = system.today();
        leav1.WCT_Leave_End_Date__c = system.today()+1;
        leav1.WCT_Leave_Category__c = 'Disability Parental';
        leav1.WCT_Sub_Category_2__c='Unpaid';
        leav1.WCT_Employee__c = con.id;
        leav1.WCT_Parental_Leave_Exhausted__c = false;
        leav1.WCT_Related_Record__c = leav.id;
        leav1.Recordtypeid = rt2.id;
        insert leav1;
        
     
  
        Test.starttest(); 
       
        PageReference pageRef = Page.Adoption;
        Test.setCurrentPage(pageRef); 
       
        LeaveSurveyController lea = new LeaveSurveyController();
        lea.paidleavecnt = 0;
        lea.ptoleavecnt = 0;
        lea.unpaidleavecnt  = 0;
        lea.saveleaveRecord();
        //lea.updateLeave.WCT_Leave_survey_submitted__c = True;
        lea.pageerrormessage = 'Error';
        lea.supportAreaErrorMesssage = 'Error';
        lea.paidDaycnt= 2;
        lea.childleavecnt= 2;
        lea.ptoweekcnt= 2;
        lea.paidweekcnt= 2;
        lea.unpaidweekcnt= 2;
        lea.totalweeks= 2;
        lea.question1= true;
        lea.question2= true;
        lea.question3= true;
        lea.Insurence= true;
        lea.InsurenceN= true;
        lea.RemainPTO= 2;
        lea.ptohol= 3;
        lea.RemainPTOdays= 3;
        lea.ptoweekcount= 4;
        lea.intDaysRemain= 4;
        lea.Displaystr= 'test';
        lea.Displaystr1= 'test';
        lea.Displaystr2= 'test';
       
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);

       Test.stoptest(); 
        
   }    
    }
     public static testMethod void test3() {
        
         Account acc = new Account();
        acc.Name='INDIA';
        acc.phone='123456790';
        Insert acc;
    
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con = WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email = 'test.test1@deloitte.com';
        con.Dependent__c ='EE+F';
        con.Plan_Num__c = 843;
        con.WCT_Benefit_plan__c = 'Health';
        con.Accountid= acc.id;
        con.WCT_Personnel_Number__c = 12345;
        insert con;
        
        Contact con1 = WCT_UtilTestDataCreation.createEmployee(rt.id);
        con1.Email = 'test.test2@deloitte.com';
        con1.Dependent__c ='EE+F';
        con1.Plan_Num__c = 843;
        con1.WCT_Benefit_plan__c = 'Health';
        con1.Accountid= acc.id;
        con1.WCT_Personnel_Number__c = 12345;
        insert con1;
    
        Profile prof = [select id from profile where name='System Administrator'];
        User usr = new User(alias = 'usr', email='test.test2@deloitte.com',
                   emailencodingkey='UTF-8', lastname='lstname',
                   timezonesidkey='America/Los_Angeles',
                   languagelocalekey='en_US',
                   localesidkey='en_US', profileid = prof.Id,
                   username='test.test2@deloitte.com');
        insert usr;
     
        system.runas(usr){
         recordtype rt1 =[select id from recordtype where DeveloperName = 'LOA_Code'];    
        WCT_Leave__c leav = new WCT_Leave__c();
        leav.WCT_Leave_Start_Date__c = system.today();
        leav.WCT_Leave_End_Date__c = system.today()+1;
        leav.WCT_Leave_Category__c = 'Disability Parental';
        leav.WCT_Employee__c = con1.id;
        leav.WCT_Parental_Leave_Exhausted__c = true;
        leav.Recordtypeid = rt1.id;
        leav.WCT_Parental_Event__c = 'Birth';
        
        insert leav;    
       
    
        
             
       
       
        recordtype rt2 =[select id from recordtype where DeveloperName = 'Parental'];
        WCT_Leave__c leav1 = new WCT_Leave__c();
        leav1.WCT_Leave_Start_Date__c = system.today();
        leav1.WCT_Leave_End_Date__c = system.today()+1;
        leav1.WCT_Leave_Category__c = 'Disability Parental';
        leav1.WCT_Sub_Category_2__c='Unpaid';
        leav1.WCT_Employee__c = con.id;
        leav1.WCT_Parental_Leave_Exhausted__c = false;
        leav1.WCT_Related_Record__c = leav.id;
        leav1.Recordtypeid = rt2.id;
        insert leav1;
        
     
  
        Test.starttest(); 
       
        PageReference pageRef = Page.Adoption;
        Test.setCurrentPage(pageRef); 
       ApexPages.currentPage().getParameters().put('Param1',leav.id);
        LeaveSurveyController lea = new LeaveSurveyController();
            lea.xLeaveid=leav.id;
        lea.paidleavecnt = 0;
        lea.ptoleavecnt = 0;
        lea.unpaidleavecnt  = 0;
        lea.saveleaveRecord();
        lea.pageerrormessage = 'Error';
        lea.supportAreaErrorMesssage = 'Error';
        lea.paidDaycnt= 2;
        lea.childleavecnt= 2;
        lea.ptoweekcnt= 2;
        lea.paidweekcnt= 2;
        lea.unpaidweekcnt= 2;
        lea.totalweeks= 2;
        lea.question1= true;
        lea.question2= true;
        lea.question3= true;
        lea.Insurence= true;
        lea.InsurenceN= true;
        lea.RemainPTO= 2;
        lea.ptohol= 3;
        lea.RemainPTOdays= 3;
        lea.ptoweekcount= 4;
        lea.intDaysRemain= 4;
        lea.Displaystr= 'test';
        lea.Displaystr1= 'test';
        lea.Displaystr2= 'test';
       
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);

       Test.stoptest(); 
        
   }    
    }
    
    
}