/**  
* Class Name  : WCT_Interview_BulkUpload 
* Description : This apex class will be used to create Interview Records in Bulk
*/
public class WCT_Interview_BulkUpload{
    public String nameFile{get;set;}
    public Blob contentFile{get;set;}
    public Integer noOfRows{get;set;}
    public Integer noOfRowsProcessed{get;set;}
    public Integer noOfError{get;set;}
    public List<List<String>> fileLines = new List<List<String>>();
    public Set<Id> stagingIds=new Set<Id>();
    public List<Contact> addContact= new List<Contact>(); 
    //start Variable for Export date with date Range
    public WCT_Candidate_Requisition__c candreq{get;set;}
    public boolean exportrngerr;
    public boolean exportrngInt;
    //end
    public boolean duplicateRows = false;
    public list<partialerr> InvalidemailIds{get;set;}
    public List<WCT_sObject_Staging_Records__c> stagingRecordsList = new List<WCT_sObject_Staging_Records__c>();
    
    //Interview List
    public List<WCT_Interview__c> interviewList= new List<WCT_Interview__c>();
    public map<id,WCT_Interview__c> intervwmp=new map<id,WCT_Interview__c>();
    public WCT_Interview_BulkUpload()
    {
        candreq=new WCT_Candidate_Requisition__c();
        exportrngerr=false;
        exportrngInt=false;
    } 
    /** 
Method Name  : readFile
Return Type  : PageReference
Description  : Read CSV and push records into staging table        
*/
    
    public Pagereference readFile()
    {
        stagingRecordsList = new List<WCT_sObject_Staging_Records__c>(); 
        stagingIds=new Set<Id>();
        noOfRowsProcessed=0;
        try{
            duplicateRows = false;
            stagingRecordsList = mapStagingRecords();
            if(duplicateRows){
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Duplicate unique Ids OR Blank Unique ID in file');
                ApexPages.addMessage(errormsg);
                return null;
            }
            
        }
        Catch(System.StringException stringException)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_String_Exception_Error+stringException.getMessage());
            ApexPages.addMessage(errormsg);    
        }
        Catch(System.ListException listException)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Columns_count_not_proper);
            ApexPages.addMessage(errormsg); 
            
        }
        
        Catch(Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,e.getmessage()+Label.Upload_Case_Exception);
            ApexPages.addMessage(errormsg);    
            
        }
        if(stagingRecordsList.size()>Limits.getLimitQueryRows())
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Error_Data_File_too_large + Limits.getLimitQueryRows());
            ApexPages.addMessage(errormsg);   
        }
        Database.SaveResult[] srList = Database.insert(stagingRecordsList, false);
        for(Database.SaveResult sr: srList)
        {
            if(sr.IsSuccess())
            {   
                stagingIds.add(sr.getId());
                noOfRowsProcessed=stagingIds.size();
            }
            else if(!sr.IsSuccess())
            {
                for(Database.Error err : sr.getErrors())
                {
                    ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.WCT_Data_Error+err.getMessage());
                    ApexPages.addMessage(errormsg);       
                }
            }
        }   
        if(!stagingIds.IsEmpty())
        {
            
            try{
                processStagingRecords(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
            }Catch(Exception e)
            {
                throw e;
            }
            
        }
        
        return null;
    }
    
    private List<WCT_sObject_Staging_Records__c> mapStagingRecords()
    {
        nameFile=contentFile.toString();
        filelines=parseCSV(nameFile, true);
        List<WCT_sObject_Staging_Records__c> stagingRecordsList=new List<WCT_sObject_Staging_Records__c>();
        noOfRows=fileLines.size();
        system.debug(filelines);
        set<string> uniqueIdSet = new set<string>();
        for(List<String> inputValues:filelines) 
        {   
            
            system.debug(inputValues.size()+'stringSize');
            WCT_sObject_Staging_Records__c stagingRecord = new WCT_sObject_Staging_Records__c();
            
            stagingRecord.Intw_Form_Name__c=inputValues[0];
            stagingRecord.Intw_Cand_Tracker_Unique_Id__c=inputValues[1];
            stagingRecord.Intw_Event_Start_date__c= DateTime.valueOf(inputValues[2]);
            stagingRecord.Intw_Event_End_date__c= DateTime.valueOf(inputValues[3]);
            
            System.debug('***'+stagingRecord.Intw_Event_Start_date__c);
            System.debug('*****'+stagingRecord.Intw_Event_End_date__c);
            stagingRecord.Intw_Subject__c=inputValues[4];
            stagingRecord.WCT_interviewer_email__c=inputValues[5];
            
            stagingRecord.Type__c='Interview Load';
            stagingRecord.WCT_status__c=WCT_UtilConstants.STAGING_STATUS_NOT_STARTED;
            
            stagingRecordsList.add(stagingRecord);
        }
        return stagingRecordsList;
    }
    
    /** 
Method Name  : parseCSV
Return Type  : String contents,Boolean skipHeaders
Description  : Business logic for parsing the string         
*/
    private List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
        List<List<String>> allFields = new List<List<String>>();
        
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        // we are not attempting to handle fields with a newline inside of them
        // so, split on newline to get the spreadsheet rows
        List<String> lines = new List<String>();
        try {
            lines = contents.split('\r\n');
        } catch (System.ListException e) {
            System.debug('Limits exceeded?' + e.getMessage());
        }
        system.debug(lines+'lines');
        Integer num = 0;
        for(String line : lines) {
            system.debug('line'+line);
            // check for blank CSV lines (only commas)
            if (line.replaceAll(',','').trim().length() == 0) continue;
            
            List<String> fields = line.split(',');  
            system.debug(fields);
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field : fields) {
                if (field.startsWith('"') && field.endsWith('"')) {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                } else if (field.startsWith('"')) {
                    makeCompositeField = true;
                    compositeField = field;
                } else if (field.endsWith('"')) {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                } else if (makeCompositeField) {
                    compositeField +=  ',' + field;
                } else {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
            }
            
            allFields.add(cleanFields);
        }
        if (skipHeaders) allFields.remove(0);
        return allFields;       
    }
    
    public void processStagingRecords(Set<Id> stagingIds, String stagingStatus){
        set<string> CTUniqueIdentifiesSet = new set<string>();
        Map<String,WCT_sObject_Staging_Records__c> ctIndentifierStgRecMap = new Map<String,WCT_sObject_Staging_Records__c>();
        List<WCT_Candidate_Requisition__c> candTrackListToUpdate = new List<WCT_Candidate_Requisition__c>();
        List<WCT_sObject_Staging_Records__c> stgRecToUpdate = new List<WCT_sObject_Staging_Records__c>();
        Map<Id,string> CTIdUniqueIdentifierMap = new Map<Id,string>();
        // Set<string> stagingRecsEmail= new Set<string>();
        Map<String,Contact> conMap= new Map<String,Contact>();
        set<string> CTUniqueIdentifiesSetfromSys = new set<string>();
        List<Contact> updateCon= new List<Contact>();
        List<WCT_sObject_Staging_Records__c> stgList= new List<WCT_sObject_Staging_Records__c>();
        Contact tmpcon= new Contact();
        addContact= new List<Contact>(); 
        // Set<String> stagingRecsReqNum= new Set<String>();
        Map<String, String> ReqNumMap= new Map<String, String>();
        Map<String, WCT_List_Of_Names__c> ListOfNamesMap= new Map<String, WCT_List_Of_Names__c>();
        //List<WCT_Interview__c> interviewList= new List<WCT_Interview__c>();
        Set<String> CTUniqueIdset= new Set<String>();
        set<String> cInterviewerEmailIdset= new set<String>();
        Map<String,WCT_Interview__c> intwMap= new Map<String,WCT_Interview__c>();
        Map<String,Id> ctMap= new Map<String, Id>();
        List<WCT_Interview_Junction__c> addIntwTracker= new List<WCT_Interview_Junction__c>();
        List<Event> eventLst= new List<Event>();
        
        //Validation variables
        Map<Id,WCT_sObject_Staging_Records__c> successFulRec = new Map<Id,WCT_sObject_Staging_Records__c>();
        Map<Id,WCT_sObject_Staging_Records__c> unSuccessFulRec = new Map<Id,WCT_sObject_Staging_Records__c>();
        Map<String, Id> Map_Interviewer_ID_email=  new Map<String, Id>();
        
        for(WCT_sObject_Staging_Records__c stgRec:[SELECT id,Intw_Form_Name__c,Intw_Cand_Tracker_Unique_Id__c,Event_start_date__c,
                                                   Event_end_date__c,Intw_Subject__c, Type__c, Intw_Event_Start_date__c, Intw_Event_End_date__c, WCT_status__c,WCT_interviewer_email__c 
                                                   FROM WCT_sObject_Staging_Records__c 
                                                   WHERE id IN :stagingIds AND WCT_Status__c = : stagingStatus]){
                                                       
                                                       //stagingRecsEmail.add(stgRec.Cand_Email__c);
                                                       //stagingRecsReqNum.add(stgRec.Cand_Requisition_Number__c); //set of all the requisition numbers from staging recs object
                                                       stgList.add(stgRec);
                                                       CTUniqueIdset.add(stgRec.Intw_Cand_Tracker_Unique_Id__c);
                                                       cInterviewerEmailIdset.add(stgRec.WCT_interviewer_email__c );
                                                       System.debug('enter here'+stgList);
                                                       System.debug('enter here 5'+CTUniqueIdset);
                                                       
                                                   }
        
        
        //Getting interviewer record id,
        system.debug('>>>>>>>cInterviewerEmailIdset>>>>'+cInterviewerEmailIdset);
        for(User cInterviwersToAdd:[ select id, name, email from user where email IN : cInterviewerEmailIdset ])
        {
            Map_Interviewer_ID_email.put(cInterviwersToAdd.email,cInterviwersToAdd.id);
        }
        
        System.debug('>>>>>>>>Map_Interviewer_ID_email>>>>>>'+Map_Interviewer_ID_email);
        
        for(WCT_List_Of_Names__c lon: [SELECT id, Name, WCT_Interview_Step__c,WCT_Job_Type__c,WCT_User_Group__c,
                                       WCT_FSS__c,WCT_Type__c FROM WCT_List_Of_Names__c 
                                       WHERE WCT_Type__c = : 'IEF'])
        {
            ListOfNamesMap.put(lon.Name,lon);
            System.debug('enter here 1'+ListOfNamesMap);
        }   
        for(WCT_Candidate_Requisition__c ct : [SELECT Id,Name, WCT_RMS_Requisition__c, WCT_Job_Type__c, WCT_User_Group__c, WCT_FSS__c 
                                               FROM WCT_Candidate_Requisition__c WHERE Name IN : CTUniqueIdset])
        {                                       
            //   ctMap.put(ct.WCT_RMS_Requisition__c,ct.Id);  
            ctMap.put(ct.Name,ct.Id);  
            
        } 
        
        for(WCT_sObject_Staging_Records__c stgRec1: stgList)
        {
            if(ctMap.containsKey(stgRec1.Intw_Cand_Tracker_Unique_Id__c)){
                WCT_Interview__c tmpInterview= new WCT_Interview__c();
                tmpInterview.sObject_Staging_Records__c= stgRec1.id;
                tmpInterview.WCT_User_group__c= ListOfNamesMap.get(stgRec1.Intw_Form_Name__c).WCT_User_Group__c;
                tmpInterview.WCT_FSS__c= ListOfNamesMap.get(stgRec1.Intw_Form_Name__c).WCT_FSS__c;
                tmpInterview.WCT_Job_Type__c= ListOfNamesMap.get(stgRec1.Intw_Form_Name__c).WCT_Job_Type__c;
                tmpInterview.WCT_Interview_Status__c= 'Open';
                tmpInterview.WCT_Clicktools_Form__c=ListOfNamesMap.get(stgRec1.Intw_Form_Name__c).id;
                tmpInterview.isBulkupload__c=true;
                interviewList.add(tmpInterview);
                System.debug('enter here 4'+interviewList);
            }
            
        }
        if(!interviewList.isEmpty())
        {
            insert interviewList;
            System.debug('enter here 3'+interviewList);
        }
        for(WCT_Interview__c intw : [SELECT id, Name,WCT_Job_Type__c,WCT_User_Group__c, WCT_FSS__c, WCT_Interview_Status__c,sObject_Staging_Records__c 
                                     FROM WCT_Interview__c where sObject_Staging_Records__c IN : stgList])
        {
            intwMap.put(intw.sObject_Staging_Records__c, intw);
            System.debug('enter here 6'+intwMap);
            intervwmp.put(intw.id, intw);
        }   
        
        for(WCT_sObject_Staging_Records__c stgRec2: stgList)
        {
            
            if(ctMap.containsKey(stgRec2.Intw_Cand_Tracker_Unique_Id__c)){
                WCT_Interview_Junction__c tempIntTracker= new WCT_Interview_Junction__c();
                tempIntTracker.WCT_Interview__c= intwMap.get(stgRec2.id).id;
                tempIntTracker.WCT_Candidate_Tracker__c=ctMap.get(stgRec2.Intw_Cand_Tracker_Unique_Id__c);
                System.debug('>Map_Interviewer_ID_email.get(stgRec2.WCT_interviewer_email__c)>>'+Map_Interviewer_ID_email.get(stgRec2.WCT_interviewer_email__c));
                
                tempIntTracker.WCT_Interviewer__c = Map_Interviewer_ID_email.get(stgRec2.WCT_interviewer_email__c);
                //tempIntTracker.isBulkupload__c=true;
                System.debug('>tempIntTracker>>'+tempIntTracker);
                addIntwTracker.add(tempIntTracker);
                successFulRec.put(stgRec2.id,stgRec2);
            }else{
                unSuccessFulRec.put(stgRec2.id,stgRec2);
            }
            
            
        }   
        System.debug('>>>>>>addIntwTracker>>'+addIntwTracker);
        
        if(!addIntwTracker.isEmpty())
        {
            insert addIntwTracker;
        }
        for(WCT_sObject_Staging_Records__c stgRec2: stgList)
        {
            if(ctMap.containsKey(stgRec2.Intw_Cand_Tracker_Unique_Id__c))
            {
                Event tmpEvent= new Event();
                //tmpEvent.Owner= UserInfo.getUserId();
                tmpEvent.WhatId= intwMap.get(stgRec2.id).id;
                tmpEvent.Subject= stgRec2.Intw_Subject__c;
                tmpEvent.Staging_ID__c=stgRec2.Id;
                tmpEvent.StartDateTime= stgRec2.Intw_Event_Start_date__c;
                tmpEvent.EndDateTime= stgRec2.Intw_Event_End_date__c;
                
                
                System.debug('***'+stgRec2.Intw_Event_Start_date__c);
                System.debug('*****'+stgRec2.Intw_Event_End_date__c);
                eventLst.add(tmpEvent);
            }
        }
        if(!eventLst.isEmpty())
        {
            insert eventLst;
        }
        
        List<EventRelation> lst_EvtRel_Toadd= new list<EventRelation>();
        InvalidemailIds = new list<partialerr>();
        for(WCT_sObject_Staging_Records__c stgRec2: stgList)
        {
            for(Event ev: eventLst)
            {
                system.debug('###'+ev);
                if(ev.Staging_ID__c.contains(stgRec2.Id)  && Map_Interviewer_ID_email.get(stgRec2.WCT_interviewer_email__c)!=null)
                {
                    system.debug('### Create Invite');    
                    EventRelation cEventRelationToadd= new EventRelation();
                    cEventRelationToadd.eventID= ev.id;
                    cEventRelationToadd.RelationId= Map_Interviewer_ID_email.get(stgRec2.WCT_interviewer_email__c);
                    system.debug('$$$$'+Map_Interviewer_ID_email.get(stgRec2.WCT_interviewer_email__c));  
                    cEventRelationToadd.isInvitee = true;
                    cEventRelationToadd.isParent =false;
                    lst_EvtRel_Toadd.add(cEventRelationToadd);
                }
                else if(ev.Staging_ID__c.contains(stgRec2.Id)  && Map_Interviewer_ID_email.get(stgRec2.WCT_interviewer_email__c)==null)
                {
                   WCT_Interview__c tmpintv=intervwmp.get(ev.WhatId); 
                   partialerr prtlerr=new partialerr(stgRec2.WCT_interviewer_email__c,ev.id,ev.WhatId,ev.Subject,tmpintv.name); 
                   InvalidemailIds.add(prtlerr);
                //InvalidemailIds.add(stgRec2.WCT_interviewer_email__c);
                system.debug('$$$$'+tmpintv);
                }
            }
        }
        system.debug('$$$$'+lst_EvtRel_Toadd); 
        if(lst_EvtRel_Toadd.size()>0)
        {
            system.debug('$$$$'+lst_EvtRel_Toadd);  
            insert lst_EvtRel_Toadd;
            
            system.debug('$$$$'+lst_EvtRel_Toadd); 
        }
        if(!eventLst.isEmpty())
        {
            update eventLst;
        }        
        for(WCT_sObject_Staging_Records__c stgRec2: stgList)
        {
            if(successFulRec.containsKey(stgRec2.id))
            {
                WCT_sObject_Staging_Records__c tmpStgRec = new WCT_sObject_Staging_Records__c();
                tmpStgRec = successFulRec.get(stgRec2.id);
                tmpStgRec.WCT_Status__c = 'Completed';
                tmpStgRec.WCT_Error_Message__c = 'No Error';
                stgRecToUpdate.add(tmpStgRec);
                
            }
            else if(unSuccessFulRec.containsKey(stgRec2.id))
            {
                WCT_sObject_Staging_Records__c tmpStgRec = new WCT_sObject_Staging_Records__c();
                tmpStgRec = unSuccessFulRec.get(stgRec2.id);
                tmpStgRec.WCT_Status__c = 'Failed';
                tmpStgRec.WCT_Error_Message__c = 'No Candidate Tracker present in the system with the Id in excel sheet '+ tmpStgRec.Intw_Cand_Tracker_Unique_Id__c;
                stgRecToUpdate.add(tmpStgRec);
            }
        }
        
        
        
        if(!stgRecToUpdate.isEmpty()){
            Database.update(stgRecToUpdate, false);
        }
        
    }
    
    public pageReference getInterviewRecords(){
        exportrngInt=false;
        return Page.WCT_Interview_BulkUpload_PageSuccess;       
    }
    //method to get interview with date range
    public pageReference getInterviewRecordsRange(){
        exportrngInt=true;
        return Page.WCT_Interview_BulkUpload_PageSuccess;       
    }
    public pageReference getErrorRecords(){
        exportrngerr=false;
        return Page.WCT_Interview_BulkUpload_PageErrors;
    }
    //method to get upload errors with date range
    public pageReference getErrorRecordsRange(){
        exportrngerr=true;
        return Page.WCT_Interview_BulkUpload_PageErrors;
    }
    public List<WCT_Interview__c> getAllInterviewCreated(){
        List<WCT_Interview__c> retList = new List<WCT_Interview__c>();
        if(!exportrngInt)
        {
            for(WCT_Interview__c intv : [SELECT id,name,WCT_User_group__c,WCT_FSS__c,WCT_Job_Type__c,WCT_Interview_Status__c,WCT_Clicktools_Form__c
                                         FROM WCT_Interview__c WHERE id IN :interviewList]){
                                             retList.add(intv);
                                             
                                         }
        }
        else{
            for(WCT_Interview__c intv : [SELECT id,name,WCT_User_group__c,WCT_FSS__c,WCT_Job_Type__c,WCT_Interview_Status__c,WCT_Clicktools_Form__c
                                         FROM WCT_Interview__c WHERE createdDate >= :candreq.WCT_Time_to_Schedule_First_Interview__c and createdDate <= :candreq.WCT_Time_till_start_of_first_Interview__c.addminutes(1) and createdbyid=:system.UserInfo.getUserId() order by createddate asc limit 500]){
                                             retList.add(intv);
                                             
                                         }    
        }
        if(!retList.isEmpty()){
            return retList;
        }else{
            return new List<WCT_Interview__c>();
        }
        
        
    }
    
    
    public List<WCT_sObject_Staging_Records__c> getAllStagingRecords()
    {
        if(!exportrngerr)
        {
            if (stagingIds!= NULL)
                if (stagingIds.size() > 0)
            {
                return [Select id,Name,Intw_Form_Name__c,Intw_Cand_Tracker_Unique_Id__c,Event_start_date__c,WCT_Error_Message__c,
                        Event_end_date__c,Intw_Subject__c, Type__c, Intw_Event_Start_date__c, Intw_Event_End_date__c
                        from WCT_sObject_Staging_Records__c where Id In:stagingIds and WCT_Status__c='Failed'];
                
            }
            else
                return null;                   
            else
                return null;
        }
        else
        {
            return[Select id,Name,Intw_Form_Name__c,Intw_Cand_Tracker_Unique_Id__c,Event_start_date__c,WCT_Error_Message__c,
                   Event_end_date__c,Intw_Subject__c, Type__c, Intw_Event_Start_date__c, Intw_Event_End_date__c
                   from WCT_sObject_Staging_Records__c where Type__c='Interview Load' and WCT_Status__c='Failed' and createdbyid=:system.UserInfo.getUserId() and createdDate >= :candreq.WCT_Time_to_Schedule_First_Interview__c and createdDate <= :candreq.WCT_Time_till_start_of_first_Interview__c.addminutes(1)  order by createddate asc limit 500];    
        }
    } 
    
    public class partialerr
    {
        public string invalemail{get;set;}
        public string eventid {get;set;}
        public string intervwid{get;set;}
        public string subject{get;set;}
        public string Intvname{get;set;}
        public partialerr(string invalemail1,string eventid1,string intervwid1, string subject1,string Intvname1)
        {
            invalemail=invalemail1;
            eventid=eventid1;
            intervwid=intervwid1;
            subject=subject1;
            Intvname=Intvname1;
        }
    }
    
}