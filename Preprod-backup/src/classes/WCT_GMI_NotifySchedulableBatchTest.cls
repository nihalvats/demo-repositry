@isTest
public class WCT_GMI_NotifySchedulableBatchTest  
{
  static Id employeeRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
  static Id immigrationRecordTypeId = Schema.SObjectType.WCT_Immigration__c.getRecordTypeInfosByName().get('L1 Visa').getRecordTypeId();
  static Id taskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Immigration').getRecordTypeId();
  static String CRON_EXP = '0 0 0 15 3 ? 2022';

  static testMethod void Test_WCT_GMI_NotifySchedulableBatch()  
  {
      Test.startTest();
      Contact con = WCT_UtilTestDataCreation.createEmployee(employeeRecordTypeId);
      insert con;
      WCT_Immigration__c imm = WCT_UtilTestDataCreation.createImmigration(con.id);
      imm.RecordTypeId = immigrationRecordTypeId;
      insert imm;
      Task immTask = WCT_UtilTestDataCreation.createTask(imm.id);
      immTask.RecordTypeId =  taskRecordTypeId;
      immTask.ActivityDate = date.today();
      immTask.whoId = con.id; 
      insert immTask;
      
      //WCT_GMI_NotifySchedulableBatch CreateCon = new WCT_GMI_NotifySchedulableBatch();
      //Task task = new Task();
     // system.schedule('New','0 0 2 1 * ?',createCon); 
      System.schedule('ScheduleApexClassTest',CRON_EXP, new WCT_GMI_NotifySchedulableBatch()); 
      Test.stopTest();
  }
  
  static testMethod void Test_WCT_GMI_NotifySchedulableBatch1()
 
  {
  Test.startTest();
  
        WCT_Mobility__c mobRec = new WCT_Mobility__c();
        mobRec.WCT_Total_Family_Size_at_Home__c = 2;
        mobRec.WCT_Mobility_Status__c = 'New';
        insert mobRec ;
        
        WCT_Immigration__c immi=new WCT_Immigration__c();
       // immi.WCT_Assignment_Owner__c=im;
        //immi.ownerId=UserInfo.getUserId();
        immi.WCT_Visa_Type__c = 'L1A Individual';
        insert immi;
        
        Contact ct = WCT_UtilTestDataCreation.createEmployee(WCT_Util.getRecordTypeIdByLabel('Contact','Employee'));
         insert ct;
         
    
    Set<Id> stagingIds = new Set<id>();
    List<EmailTemplate> lstEmailTemplate = [select id, developerName from EmailTemplate]; 
    
    WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
    taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
    taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
    insert taskRef;
    
    Task t1= new Task();
     t1.Subject='call';
     t1.WCT_Notification_1_Template_API_Name__c = lstEmailTemplate[0].developerName  ;
     t1.WhatId=mobRec.Id;
     t1.WCT_Task_Reference_Table_ID__c= taskRef.id;
     t1.whoId=ct.id;     
     t1.ActivityDate =System.today();
     t1.Status='Completed';
    // t1.WCT_Notification_1__c= ;
     //t1.WCT_Notification_1_Template_API_Name__c= 'Srikanth';
     t1.WCT_Business_Owner__c='sabberaboina1@deloitte.com';
     t1.WCT_R10_Resource_Manager__c ='sabberaboina2@deloitte.com';
     t1.WCT_Resource_Manager_Email_ID__c='sabberaboina@deloitte.com';
     t1.WCT_USI_Resource_Manager__c ='sabberaboina3@deloitte.com';
     t1.WCT_US_Project_Mngr__c ='sabberaboina4@deloitte.com';
     t1.WCT_GMI_Closed_Task_Notification_Date__c = Date.today();
     t1.recordTypeId = WCT_Util.getRecordTypeIdByLabel('Task','Immigration');
     t1.WCT_Is_Closed_Task_Notification__c = true;
     
    // t1.OwnerId= system.Userinfo.getUserId()
    insert t1;
              
      WCT_GMI_NotifySchedulableBatch createCon = new WCT_GMI_NotifySchedulableBatch();
      Task task = new Task();
      system.schedule('New','0 0 2 1 * ?',createCon); 
    
          Test.stopTest();   
        

     }
   
  
   }