@isTest
 public class WCT_H1bcap_Prac_No_Resp_Test{
 Static testmethod void WCT_H1bcap_Prac_No_Resp_TestMethod(){
   
   
   
   Recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
   Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
   con.WCT_Employee_Group__c = 'Active';
   insert con;
  
        list<WCT_H1BCAP__c> lst = new list<WCT_H1BCAP__c>();
        WCT_H1BCAP__c h1 = new WCT_H1BCAP__c();
        h1.WCT_H1BCAP_Practitioner_Name__c = con.id;
        h1.WCT_H1BCAP_Email_ID__c = 'svalluru@gmail.com';
        h1.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
        h1.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'abc@yahoo.com';
        h1.WCT_H1BCAP_Status__c = null;     
        h1.recalculateFormulas(); 
        lst.add(h1);
        insert lst;   
        
        
        Date d = system.today().adddays(-10); 
        Test.setCreatedDate(lst[0].Id, d);
    
    Test.StartTest(); 
         WCT_H1bcap_Prac_No_Resp h1bs = new WCT_H1bcap_Prac_No_Resp();
         WCT_H1bcap_Prac_No_Resp_Schedule createCon = new WCT_H1bcap_Prac_No_Resp_Schedule();
         system.schedule('New','0 0 2 1 * ?',createCon); 
         database.Executebatch(h1bs); 
    Test.StopTest(); 
    }
   }