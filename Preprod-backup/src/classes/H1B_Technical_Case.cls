public class H1B_Technical_Case extends SitesTodHeaderController
{
    public Case h1bCase{get;set;}
    public integer  viewState{get;set;}
    public String   errorMessage{get;set;}
    public blob attach{get; set;}
    public String fileName{get;set;}
    public H1B_Technical_Case()
    {
        h1bCase= new Case();
        fileName= '';
        
        if(loggedInContact==null)
        {
            viewState=0;
        }
        else
        {
            viewState=1;
        }
    }
    
    public void saveCase()
    {
       if(h1bCase.subject!='' && h1bCase.description!='')
       {
           try
           {
               string recordTypeID= Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case').getRecordTypeId();
               h1bCase.ContactId=loggedInContact.Id;
               h1bCase.WCT_ReportedBy__c=loggedInContact.Id;
               h1bCase.WCT_Category__c=label.Talent_Technology_Catogery;
               h1bCase.WCT_SubCategory1__c=label.Talent_Technology_Case_Sub_Catogery;
               h1bCase.WCT_SubCategory2__c=label.Talent_Technology_Case_Sub_Catogery2;
                h1bCase.Sub_Category_3__c=label.Talent_Technology_Case_Sub_Catogery3;
               h1bCase.Status='New';
               insert h1bCase;
               
               try{
                   if(fileName!=null && fileName!='')
                   {
                       Attachment tempAttachment = new Attachment();
                       tempAttachment.body=attach;
                       tempAttachment.Name=fileName;
                       tempAttachment.ParentId=h1bCase.id;
                       insert tempAttachment;
                       attach=null;
                   }
               }
               catch(Exception e)
               {
                   
                   
               }
               
               h1bCase = [Select Id, CaseNumber From Case where id =:h1bCase.id];
           }
           Catch(Exception e)
           {
               viewState=3;
           }
            viewState=2;
           
       }
       else
        {
            errorMessage='Subject & Description are mandotory';
        }
        attach=null;
        
    }
}