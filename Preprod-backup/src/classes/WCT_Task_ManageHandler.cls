/*
    Project                 : GM&I
    Associated Test Class   : 
    Description             : Common Task Handler for GM&I
    Author                  : Dinesh Kumar Rangasamy
 */

public class WCT_Task_ManageHandler {

    //Task Related Variables

    public String taskID { get; set; }
    public Task taskObj { get;set;}
    
    //Initializing Mobility Instance
    
    public WCT_Mobility__c mobilityRecord { get;set;}
    
    //Error Related Variables
    
    public string errorMessage {  get; set;}
    public boolean isError { get; set; }
    public static string NO_TASK_ID = 'NO TASK ID';
    public static string NO_TASK_INSTANCE = 'NO TASK INSTANCE';
    public static string NO_MOBILITY_INSTANCE = 'NO MOBILITY INSTANCE ';

    /* Initializing Constructor for Fetching Task and Mobility Details */

    public WCT_Task_ManageHandler() {
        getParameterInfo();
        if (!isError) {
            getTaskInstance();
        }

        if (!isError) {
            getMobilityDetails();
        }

    }

    /* Reset Error Method */

    public void resetError() {
       
        isError = false;
        errorMessage = '';
    }

    /* Retrieving URL Task Params */

    public String getParameterInfo() {

        resetError();

        try {
            taskID = ApexPages.currentPage().getParameters().get('taskid');
              
        } 
         catch (Exception e) {
            WCT_ExceptionUtility.logException('WCT_Task_ManageHandler', 'GM&I Task Handler', e.getMessage());
            isError = true;
            errorMessage = NO_TASK_ID;
        }
        
       return taskID;
    }

     /* Retrieving Task Instance */

    public void getTaskInstance() {

        resetError();

        try {
            taskObj = [SELECT Status, OwnerId, WhatId, WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c FROM Task WHERE Id = : taskID];
        } 
        catch (Exception e) {
           WCT_ExceptionUtility.logException('WCT_Task_ManageHandler', 'GM&I Task Handler', e.getMessage());
           isError = true;
           errorMessage = NO_TASK_INSTANCE;
        }
    }

    /* Retrieving Mobility Details */

    public void getMobilityDetails() {

        resetError();

        try {
            mobilityRecord = new WCT_Mobility__c();
            mobilityRecord = [SELECT Name, WCT_Mobility_Status__c,
                                     WCT_Visa_Type__c,
                                     Ownerid FROM WCT_Mobility__c WHERE id = : taskObj.WhatId
                             ];
        } 
        catch (Exception e) {
           WCT_ExceptionUtility.logException('WCT_Task_ManageHandler', 'GM&I Task Handler', e.getMessage());
           isError = true;
           errorMessage = NO_MOBILITY_INSTANCE;
        }
    }

    /* Upsert Task and Mobility Object */

    public pageReference saveTask() {

        resetError();

        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        
        try {

            taskObj.OwnerId = UserInfo.getUserId();
            upsert taskObj;

                if (string.valueOf(mobilityRecord.Ownerid).startsWith('00G')) {
                    taskObj.Ownerid = System.Label.GMI_User;
                } else {
                    taskObj.OwnerId = mobilityRecord.Ownerid;
                }

                if (taskObj.WCT_Auto_Close__c == true) {
                    taskObj.status = 'Completed';
                } else {
                    taskObj.status = 'Employee Replied';
                }

            taskObj.WCT_Is_Visible_in_TOD__c = false;
            upsert taskObj;
        } 
        catch (Exception e) {
           WCT_ExceptionUtility.logException('WCT_Task_ManageHandler', 'GM&I Task Handler', e.getMessage());
           isError = true;
           errorMessage = NO_MOBILITY_INSTANCE;
       }

     return null;
    }
}