/****************************************************************
 * Description : Test class for WCT_Accept_Or_DeclineEventHandler
 ****************************************************************/
 
@isTest

private class WCT_Accept_Or_DeclineEventHandler_Test

{
    static testMethod void testCase1() 
    {
        
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        //email.Subject = 'Acceptedid:EVE -test';
        email.plainTextBody = null;
        email.fromAddress='test@gmail.com';
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        
        Contact conRec = WCT_UtilTestDataCreation.createContactRec();
        insert conRec;
        Event e=new Event();
        //e.WCT_Event_type__c='EVE - 0004';
        e.DurationInMinutes=10;
        e.ActivityDateTime=System.today();
        insert e;
        EventRelation er=new EventRelation();
        er.IsInvitee = true;
        er.eventid=e.id;
        er.RelationId=conrec.id;
        insert er;
        Event e1=[select WCT_Event_type__c from event limit 1];
        email.Subject = 'Accepted [Id: '+String.valueOf(e1.WCT_Event_type__c)+']';
        
        WCT_Accept_Or_DeclineEventHandler eventHandler = new WCT_Accept_Or_DeclineEventHandler();     
                
        //run test
        Test.startTest();
        eventHandler.handleInboundEmail(email, envelope);
        email.Subject = 'Declined [Id: '+String.valueOf(e1.WCT_Event_type__c)+']';
        eventHandler.handleInboundEmail(email, envelope);
        Test.stopTest();
    }

}