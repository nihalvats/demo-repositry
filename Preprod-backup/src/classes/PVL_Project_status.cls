public class PVL_Project_status{

    public list<PVL_Database__c> pvls;
    string strPVLQuery = '';
    public static set<string> lst = new set<string> ();
    
    public integer getPVLStatus(list<PVL_Database__c> listPVL, string action){
       PVL_Database__c pvlitem ;
       pvlitem = listPVL[0];
       system.debug(' Object From Class: ' +pvlitem );
       if (action == 'Insert')
       {
           if (pvlitem.active__c == false)
           {
               return 0;
           }
           else if (pvlitem.active__c == true)
             {
              list<PVL_Database__c> pvls = [SELECT id, Active__c,Project__c,Name  FROM PVL_Database__c
                                            where project__c =: pvlitem.project__c  and Active__c =: pvlitem.active__c ];
              system.debug(' Object From Class - Insert: ' + pvls.size() );
                        return pvls.size();
              }
       }
       else if (action == 'Update')
       {
           if (pvlitem.active__c == false)
           {
               system.debug(' Object From Class:1 ' + pvlitem.active__c);
               return 0;
           }
           else if (pvlitem.active__c == true)
             {
                  system.debug(' Object From Class:2 ' + pvlitem.active__c);
                  list<PVL_Database__c> pvls = [SELECT id, Active__c,Project__c,Name  FROM PVL_Database__c
                                       where project__c =: pvlitem.project__c  and Active__c =:pvlitem.active__c and id <> :pvlitem.id ];
                  system.debug(' Object From Class - Update: ' + pvls.size() );
                            return pvls.size();
              }
       }
       return 0;
    }
    
    // Method counting the Same Service lines Records
     public static void Updtcount(list<PVL_Database__c> lstpvlrec){
    
    Integer FinalCount;
    for(PVL_Database__c pvl : lstpvlrec){
   
      if(pvl.ProjectLead_Engmt_Partner_Service_Line__c <> null && pvl.Project__c <> null){
        lst.add(pvl.ProjectLead_Engmt_Partner_Service_Line__c);
        
        }
    
    }
       
    if(!lst.isempty())  {   
    
   FinalCount = [select count() from PVL_Database__c where ProjectLead_Engmt_Partner_Service_Line__c =:lst ];
   
    }  
    system.debug('FinalCount******'+FinalCount);
   
    for(PVL_Database__c pv : lstpvlrec)
    {
     PVL_Database__c oldpvh = (PVL_Database__c)Trigger.oldMap.get(pv.ID);
     system.debug('oldvalue***'+oldpvh.ProjectLead_Engmt_Partner_Service_Line__c);
     system.debug('Newvalue***'+pv.ProjectLead_Engmt_Partner_Service_Line__c);
     if(oldpvh.ProjectLead_Engmt_Partner_Service_Line__c == pv.ProjectLead_Engmt_Partner_Service_Line__c){
     
     
    pv.WCT_Count__c = FinalCount ;
         
     }
     
     else{
     system.debug('FinalCount in else******'+FinalCount);
     FinalCount++;
     pv.WCT_Count__c = FinalCount;
     
     }
    
    }
    
       
    
  }
    
  // Method counting the Same Service lines Records while inserting  
    public static void Updatecuntinsert(list<PVL_Database__c> lstpvlrec){
       Integer FinalCount;
    for(PVL_Database__c pvl : lstpvlrec){
   
      if(pvl.ProjectLead_Engmt_Partner_Service_Line__c <> null && pvl.Project__c <> null){
        lst.add(pvl.ProjectLead_Engmt_Partner_Service_Line__c);
        
        }
    
    }
     
     if(!lst.isempty())  {
    
     FinalCount = [select count() from PVL_Database__c where ProjectLead_Engmt_Partner_Service_Line__c =:lst ];
    }
    
    for(PVL_Database__c pv : lstpvlrec)
    {
    
     if(FinalCount > 0){
     
     FinalCount++;
    pv.WCT_Count__c = FinalCount;
         
     }
     
     else{
     
     pv.WCT_Count__c = 1;
     
     }
    
    }
    
  }
   
   
}