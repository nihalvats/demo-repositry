public class contentController2 {

    public transient blob file { get; set; }
    public String InputFileName{get;set;}
    public boolean isUploaded{get; set;}
    
    public contentController2(ApexPages.StandardController ssc)
    {
        
          isUploaded=false;
    }
    public pagereference Cancel1()
    {
        string PVLId;
        PVLId = System.currentPagereference().getParameters().get('PVLID');
        return new PageReference('/' + PVLId );
    }    
    public PageReference go1() {
    
        //Id PVLId;
        string PVLId;
        PVLId = System.currentPagereference().getParameters().get('PVLID');

        //system.debug(' Received Parameters : ' +System.currentPagereference().getParameters().get('PVLID'));
        //Select Id, Name, Project__r.id,Project__r.Name, Project__r.PVL_Client_Name__c From PVL_Database__c Where id = 'a2iL00000004j0T'
         List<PVL_Database__c> xpvls=[Select Id, Name,Client_Name__c,Client_Name_Report__c,PVL_File_Nm__c,PVL_Project_Name__c,Project__r.PVL_Project_Name__c  From PVL_Database__c Where id =:PVLId]; 
         string pvlname='';
         string clientname='';

         string FileExtension = InputFileName.contains('.')?InputFileName.subString(InputFileName.indexOf('.'),InputFileName.length()):'';
         
         system.debug('Newly Uploaded InputFileName : ' + InputFileName);
         system.debug('Newly Uploaded InputFileName : ' + FileExtension );
         
         if(xpvls.size()>0)
        {
            pvlname = xpvls[0].Name ;
            clientname = xpvls[0].Client_Name_Report__c;
            //string fln = xpvls[0].Name+'-'+(xpvls.size()>0?xpvls[0].Client_Name_Report__c:'')+'-'+xpvls[0].Project__r.PVL_Project_Name__c+'-'+ System.today().format()+'-'+FileExtension;
            string fln = xpvls[0].Name+'-'+(xpvls.size()>0?xpvls[0].Client_Name_Report__c:'')+'-'+xpvls[0].Project__r.PVL_Project_Name__c+'-'+ Datetime.now().format('MM-dd-yyyy') +'-'+FileExtension;
    
            system.debug('Newly Uploaded InputFileName : ' + fln);

            ContentVersion NewContent = new ContentVersion();
            ContentVersion ContentToPublish = new ContentVersion();
            NewContent.versionData = file;

            NewContent.title = fln ;
            NewContent.pathOnClient = InputFileName;
            xpvls[0].PVL_File_Nm__c = fln;
            if (pvlid <> null)
            {
                NewContent.PVL_Database__c = PVLId;
            }
            insert NewContent;
            update xpvls[0];
            //system.debug('File.Name : ' + InputFileName);
        
            List<ContentWorkSpace> CWList = [SELECT Id, Name From ContentWorkspace WHERE Name =: Label.LibraryNameToPublish];
           
            ContentToPublish = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :NewContent.Id];
            
            ContentWorkspaceDoc cwd = new ContentWorkspaceDoc();
            cwd.ContentDocumentId = ContentToPublish.ContentDocumentId;
            cwd.ContentWorkspaceId = CWList.get(0).Id;
            insert cwd; 
            isUploaded=true;
            
        }
        
        return new PageReference('/' + PVLId );
    }
}