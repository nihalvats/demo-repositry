/**
 * Class Name  : WCT_SubmitAdHocCandidate_Contrl_test  
 * Description : This apex test class will use to test the WCT_SubmitAdHocCandidate_Controller

 */
@isTest
private class WCT_SubmitAdHocCandidate_Ctrl_TEST
{
    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
    /*
    TestMethod to test creation of Adhoc candidate
     */
    static testMethod void testCand()
    {

        User user =  WCT_UtilTestDataCreation.createUser('recoo',profileIdRecCoo,'ralaRecCo@deloitte.com','rala@deloitte.com');
        insert user;
        //To cover Util class method
        Id NonIEFRtypeIdTest = WCT_Util.getRecordTypeIdByDeveloperName('WCT_List_Of_Names__c', 'Non_IEF');
        //End
        Id NonIEFRtypeId = WCT_Util.getRecordTypeIdByLabel('WCT_List_Of_Names__c', 'Non-IEF');
        WCT_List_Of_Names__c eduInst = WCT_UtilTestDataCreation.createListOfNames('School',NonIEFRtypeId,'Education Institution');
        insert eduInst;
        WCT_List_Of_Names__c currEmp = WCT_UtilTestDataCreation.createListOfNames('Employer',NonIEFRtypeId,'Current Employer');
        insert currEmp;
        PageReference pgref = new PageReference('/apex/WCT_SubmitAdHocCandidate_Page');
        Test.setCurrentPage(pgref);      
        WCT_SubmitAdHocCandidate_Controller submitCon = new WCT_SubmitAdHocCandidate_Controller();
        submitCon.doc.body = Blob.valueOf('Unit Test Attachment Body');
        submitCon.doc.Name = 'test';
        submitCon.doc.ContentType = 'test';
        submitCon.TechnicalScreener = 'test';
        submitCon.TechnicalScreener1 = 'test';
        submitCon.TechnicalScreener2 = 'test';
        submitCon.TechnicalScreenerId = user.id;
        submitCon.TechnicalScreener1Id = user.id;
        submitCon.TechnicalScreener2Id = user.id;
        submitCon.InitialContact = 'test';
        submitCon.officeLocation = 'officeLocation';
        submitCon.emailAddress = 'emailAddress@deloitte.com' ;
        submitCon.phone = '12345';
        submitCon.school = 'school';
        submitCon.currentEmployer = 'test';
        submitCon.otherEmployer ='otherEmployer';
        submitCon.lastname = 'NoteNameWithEqualOrMoreThanEightyCharectors';
        submitCon.errorMsg = 'Invalid Phone,Invalid Email';

        // calling methods
        submitCon.can();
        submitCon.sub();
        submitCon.saveContact();
        submitCon.uploadAttachment();
        submitCon.nextPage();
        submitCon.backform(); 
        submitCon.nextPage();
       // submitCon.saveCandidate(); 
        submitCon.setAttachmentSection();
        submitCon.cancelAttachment();
        //submitCon.removeAttachment();
        submitCon.save();
        submitCon.schoolName  = 'school';
        submitCon.currentEmployerName  = 'test';
        submitCon.school = eduInst.id;
        submitCon.currentEmployer = currEmp.id;
        submitCon.nextPage();
        submitCon.save();
        submitCon.emailAddress = 'emailAddress2@deloitte.com' ; 
        submitCon.docIdList = new list<string>();
         submitCon.nextPage();
        submitCon.save();
        submitCon.emailAddress = 'emailAddress2@deloitte.com' ;
        submitCon.docIdList = new list<string>();
         submitCon.nextPage();
        submitCon.save();
        submitCon.backform();      
        submitCon.save();
    }

}