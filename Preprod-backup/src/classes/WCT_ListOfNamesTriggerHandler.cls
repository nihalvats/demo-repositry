public class WCT_ListOfNamesTriggerHandler{
    
    public Static void GetRelatedDocumentsOnInterviewForms(List<WCT_List_Of_Names__c> newInterviewForms){
        Set<String> setOfflineInterviewFormNames= new Set<String>();
        Set<String> setSampleInterviewFormNames= new Set<String>();
        Map<String,Document> mapSampleDocuments=new Map<String,Document>();
        Map<String,Document> mapWordDocuments=new Map<String,Document>();
        for(WCT_List_Of_Names__c newIEF:newInterviewForms){
              if(newIEF.recordtypeId==label.WCT_IEFLibraryRecordtypeId)
                   setOfflineInterviewFormNames.add(label.IEF_Blank_Doc_Prefix+newIEF.Name); 
                   setSampleInterviewFormNames.add(label.IEF_Sample_Doc_Prefix+newIEF.Name);
        }
        if(!setSampleInterviewFormNames.isEmpty()){
            for(Document sampleDoc:[select Name,id from Document where FolderID=:label.WCT_IEFSampleDocumentsFolder and Name in:setSampleInterviewFormNames ]){
              mapSampleDocuments.put(sampleDoc.Name,sampleDoc);
            }
        }
        if(!setOfflineInterviewFormNames.isEmpty()){
            for(Document wordDoc:[select Name,id from Document where FolderID= :label.WCT_IEFWordDocumentsFolder and Name in:setOfflineInterviewFormNames ]){
              mapWordDocuments.put(wordDoc.Name,wordDoc);
            }       
        }
        system.debug('map'+mapWordDocuments);
        for(WCT_List_Of_Names__c newIEF:newInterviewForms){
          if(mapSampleDocuments.get(label.IEF_Sample_Doc_Prefix+newIEF.Name)!= null)
            newIEF.WCT_Sample_Questions_URL__c=label.WCT_SalesforceContentDownloadURL+mapSampleDocuments.get(label.IEF_Sample_Doc_Prefix+newIEF.Name).id;
            system.debug('mapWord'+mapWordDocuments.get(newIEF.Name));
            if(mapWordDocuments.get(label.IEF_Blank_Doc_Prefix+newIEF.Name)!= null)
            newIEF.WCT_Blank_Document_URL__c=label.WCT_SalesforceContentDownloadURL+mapWordDocuments.get(label.IEF_Blank_Doc_Prefix+newIEF.Name).id;
        }
        
        
    }

}