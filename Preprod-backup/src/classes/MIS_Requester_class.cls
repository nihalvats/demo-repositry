/*****
ClassName:MIS_Requester_class
Description:Controller which handles the view for the requestor for its Request
Type:Controller
Test class:Test_MIS_batch_class

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01                 Deloitte                   27/02/2016          87%                         <Req / Case #>                  Original Version

*****/
global  class MIS_Requester_class extends SitesTodHeaderController{
    public list<Case_form_Extn__c> LONlist{get;set;} // holds the list of records of Case_form_Extn__c
    public string emailcon_1{get;set;}  //holds the contact id
    public list<Case_form_Extn__c> reportingList{get;set;}// holds the list of records of Case_form_Extn__c
    public string currentFeedBackId{get;set;}// to hold the feedback id
    public string count{get;set;}
    Public decimal promptness{get;set;}
    Public decimal accuracy{get;set;}
    Public decimal helpfullness{get;set;}
    Public string commentbox{get;set;}
    
    //constructor
    public MIS_Requester_class(){
        init();
    }

    /*Method to query the logged in contact details */
    public void init(){
        //getting the id of the logged in contact
        if(loggedInContact != null){
            emailcon_1=loggedInContact.id;
        }
    }
    /********************************************************************************************
*Method Name         : <getReportingInfo>
*Return Type         : <list<Case Form extension>>
*Param’s             : <None>
*Description         : <Method that returns list of CFE's that the requester raised.>

* -----------------------------------------------------------------------------------------------------------                 
* 01                 Original Version
*********************************************************************************************/
    
    public list<Case_form_Extn__c> getReportingInfo()
    {
        list<id> Liston = new list<id>();
        // List the pass the values in Apex Page
        // system.debug('loggedInContact111'  +loggedInContact);
        if(loggedInContact != null)
        {
            
            LONList=[select id,MIS_RptNumber__c,MIS_Name_of_the_Team__c,MIS_DL_Email_id__c from Case_form_Extn__c where RecordType.DeveloperName=:'MIS_Scheduled_Report' ];
            if(LONList.size()>0){
                for(Case_form_Extn__c lon :LONList){
                    string emailcon=lon.MIS_DL_Email_id__c;
                    if( emailcon != null){
                        if( emailcon.contains(loggedInContact.Email)   ){
                            Liston.add(lon.id);
                            //  system.debug('lon is'+Liston);  
                        }
                    }
                }
            }
            if( Liston != null){
                //  SObject[] obj = Database.query('SELECT Id FROM WCT_List_Of_Names__c WHERE loggedInContact.Email LIKE' + '%' + MIS_DL_Email_ID__c + '%');
                reportingList=[Select id,CreatedDate,GEN_Case__c,MIS_Report_StartDate__c,MIS_Calculation_Of_Weekdays__c,MIS_RptNumber__c,MIS_URL_File__c,MIS_RptName__c,MIS_RptFrequency__c,MIS_Name_of_the_Team__c,TRT_Requestor_Name__c,MIS_Status_of_Report__c,MIS_Archived_Reports__c ,TRT_Requestor_Name__r.name,MIS_date_report_closed__c,TRT_Assigned_Owner__c,(SELECT id,TRT_Feedback__c,MIS_No_of_times_viewed__c FROM Case_form_Extn__c.Case_Form_Extension__r where recordtype.name='MIS Feedback' and TRT_Requestor_Name__c=:loggedInContact.id),TRT_Feedback__c,(select id from attachments order by CreatedDate DESC) from Case_form_Extn__c where  RecordType.Name='MIS Scheduled Report' and  id IN : Liston and  MIS_Status_of_Report__c='CLOSED' and    MIS_Calculation_Of_Weekdays__c<20 order by CreatedDate DESC];
                
            } 
        }
        return  reportingList;  
    }
    /********************************************************************************************
*Method Name         : <countck>
*Return Type         : <page reference>
*Param’s             : <Requester name,record id>
*Description         : <This Method is used to know the requester view details
Getting the details of how many times the requester clicks on the report
and updating in the Cfe record >

* -----------------------------------------------------------------------------------------------------------                 
* 01                 Original Version
*********************************************************************************************/
    
    @RemoteAction
    global static pagereference countck(string countvalue,string newemail){
        try{
        list<Case_form_Extn__c> ctcfe = [Select id,GEN_Case__r.id,MIS_No_of_times_viewed__c from Case_form_Extn__c where MIS_Report_Detail_Object__c=:countvalue and TRT_Requestor_Name__c=:newemail LIMIT 1];
        if ( ctcfe != null){
            decimal num;
            ctcfe[0].MIS_No_of_times_viewed__c=+ctcfe[0].MIS_No_of_times_viewed__c+1;
        }
              update ctcfe;
              return null;
            //system.debug('mis reqview'+ctcfe);
        }
         catch(exception e){
                Exception_Log__c log = WCT_ExceptionUtility.logException('MIS Requester dashboard', 'MIS Requester page', e.getMessage());
              return null;  
            }
    }
    
    /*
@Name        : rateAsset
@description : The following method upserts the Rating record if the User rate an report
@parameters  : ReportingId(ID),rating(decimal)
@returns     : Boolean
*/
    
    // for submitting feedback
    @RemoteAction
    global static pagereference submit(String currentFeedBackId_1,decimal accuracy_1,decimal promptness_1,decimal helpfullness_1,decimal feedback,string commentbox_1,string email_1){
        try{
            
        list<Case_form_Extn__c> newcfe = [Select id,GEN_Case__r.id,TRT_Feedback__c,TRT_Feedback_Comments__c,TRT_Helpfulness__c,TRT_Promptness__c,TRT_Accuracy__c from Case_form_Extn__c where MIS_Report_Detail_Object__c=:currentFeedBackId_1 and TRT_Requestor_Name__c=:email_1 LIMIT 1];
        
        if ( newcfe != null){
            newcfe[0].TRT_Accuracy__c=accuracy_1;
            newcfe[0].TRT_Promptness__c=promptness_1;
            newcfe[0].TRT_Helpfulness__c=helpfullness_1;
            newcfe[0].TRT_Feedback_Comments__c=commentbox_1;
            newcfe[0].TRT_Feedback__c= feedback;
               
           }
            update newcfe;
            return null;
            }
         catch(exception e){
                Exception_Log__c log = WCT_ExceptionUtility.logException('MIS Requester dashboard', 'MIS Requester page', e.getMessage());
              return null;  
            }
    }
}