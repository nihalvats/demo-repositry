@isTest
private class ExitSurveyForm_Test {

    public static Account accountObj ;
    public static Contact contactObj; 
    /** 
            Method Name  : createAccount
            Return Type  : Account
            Type         : private
            Description  : Create temp Account record for data mapping         
        */
        private static Account createAccount()
        {
            Account accObj = new Account(name = 'Test Account');
            insert accObj;
            return accObj;
        }
    /** 

            Method Name  : createContact
            Return Type  : Contact
            Type         : private
            Description  : Create temp Contact record for data mapping         
        */
        private static Contact createContact(Account acc,string email)
        {
            Contact conObj = new Contact(firstname = 'Test',lastname = 'Contact',Accountid =acc.id,email=email,RecordTypeId = System.Label.Employee_Record_Type_ID,ELE_Access_Level__c = 'ITS',ELE_Contact__c=true);
            insert conObj;
            return conObj;
        }
        @isTest static void test_method_one()
        {
            Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');             
            User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
            insert u;
            System.runAs(u)
                //Running the test case with user u
            {              
                //accountObj = ExitSurveyForm_Test.createAccount();
                //contactObj = ExitSurveyForm_Test.createContact(accountObj);
                Test.startTest();    
                //Exit_Survey_Form_con esf = new Exit_Survey_Form_con();SURVEY-9203;a1y18000000ml6t
                //String id = ApexPages.currentPage().getParameters().get('em');0034000001dO8lj
                ELE_Separation__c esc = new ELE_Separation__c(ELE_Contact__c=con.ID);
                insert esc;
                Exit_Survey__c es = new Exit_Survey__c();
                insert es;
                Test.setCurrentPageReference(new PageReference('Page.Exit_Survey_Form'));
                //Changed the passing id as sid.
                System.currentPageReference().getParameters().put('sid',es.id);
                Exit_Survey_Form_con esf = new Exit_Survey_Form_con();
                Exit_Survey__c oexitsurvey = new Exit_Survey__c();
                
                
                
                //Exit_Survey__c oexitsurvey = [SELECT Id from Exit_Survey__c where Employee__c = 'KARTHIK GOLLAPALLI' limit 1];
                List<SelectOption> options;
                List<SelectOption> worklifebaloptions;
                List<SelectOption> feedbackoptions;
                List<SelectOption> alumnioptions;
                List<SelectOption> discussionwthoptions;
                List<SelectOption> Fssoptions;
                string selectedopt;
                Decimal Personnelno;
                esf.getoptions();
                esf.getworklifebaloptions();
                esf.getfeedbackoptions();
                esf.getalumnioptions();
                esf.getdicussionoptions();
                esf.getFssoptions();
                esf.submitsurvey();
                String[] temp  = esf.Discussedopt;
                temp  = esf.selectedItems;
                
                
                Test.stopTest();
            }
	}    
    
    /**  Method Name  : test_method_two
		Description  : To test the catch part of the submitsurvey method in the Exit_Survey_Form_con controller.      
		*/
    
    @isTest static void test_method_two(){
        
        Contact con = createContact(createAccount(),'jbahuguna@deloitte.com');             
        User u = WCT_UtilTestDataCreation.createUser('jbahuguna@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;
        System.runAs(u)
            //Running the test case with user u
        { 
            Test.startTest();
            
            ELE_Separation__c esc = new ELE_Separation__c(ELE_Contact__c=con.ID);
            insert esc;
            Exit_Survey__c es = new Exit_Survey__c();
            insert es;
            Test.setCurrentPageReference(new PageReference('Page.Exit_Survey_Form'));
            //Changed the passing id as sid.
            System.currentPageReference().getParameters().put('sid',es.id);
            Exit_Survey_Form_con esf = new Exit_Survey_Form_con();
            Exit_Survey__c oexitsurvey = new Exit_Survey__c();
            
            
            List<SelectOption> options;
            List<SelectOption> worklifebaloptions;
            List<SelectOption> feedbackoptions;
            List<SelectOption> alumnioptions;
            List<SelectOption> discussionwthoptions;
            List<SelectOption> Fssoptions;
            string selectedopt;
            Decimal Personnelno;
            esf.getoptions();
            esf.getworklifebaloptions();
            esf.getfeedbackoptions();
            esf.getalumnioptions();
            esf.getdicussionoptions();
            esf.getFssoptions();
            esf.submitsurvey();
            String[] temp  = esf.Discussedopt;
            temp  = esf.selectedItems;
            //To catch the exception in the submitsurvey method.
            esf.oexitsurvey=null;
            esf.submitsurvey();
            
            Test.stopTest();   
        }                
        
        
    }
    
}