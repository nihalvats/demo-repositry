/**  
    * Class Name  : WCT_UpdateCandTracker 
    * Description : This apex class will use to update Candidate Tracker's Interview Milestone
*/
public class WCT_CT_BulkUpload{

    //Variable Declaration
    //File Related
    public String nameFile{get;set;}
    public Blob contentFile{get;set;}
    //Number of Record related
    public Integer noOfRows{get;set;}
    public Integer noOfRowsProcessed{get;set;}
    public Integer noOfError{get;set;}
    public List<List<String>> fileLines = new List<List<String>>();
    //For processing
    public Set<Id> stagingIds=new Set<Id>();
    public List<Contact> addContact= new List<Contact>(); 
    public List<WCT_Candidate_Requisition__c> ctList{get;set;}
    public List<WCT_Candidate_Requisition__c> candtrack=new List<WCT_Candidate_Requisition__c>();
    public static final id AdhocCandRecTypeId = WCT_Util.getRecordTypeIdByLabel('Contact','Candidate Ad Hoc');
    // get date fields from candidate tracker 
    public WCT_Candidate_Requisition__c candreq{get;set;}
    
    //check if user is exporting current error list or error list with date range
    public boolean exportrngerr;
    
    public boolean dataErr = false;
    
    public List<WCT_sObject_Staging_Records__c> stagingRecordsList = new List<WCT_sObject_Staging_Records__c>();
    
    //map to show candidate trackers in downladed file
    public Map<String,id> candTrackMap = new Map<String,id>();
    //Variables for Validation of Blank Data
    public boolean blankEmails = false;
    public boolean blankReqIds = false;
    // Constructor to initialize the datefields
    public WCT_CT_BulkUpload()
    {
        candreq=new WCT_Candidate_Requisition__c();
        exportrngerr=false;
    }
    
    /** 
        Method Name  : readFile
        Return Type  : PageReference
        Description  : Read CSV and push records into staging table        
    */
    
    public Pagereference readFile()
    {
        stagingRecordsList = new List<WCT_sObject_Staging_Records__c>(); 
        stagingIds=new Set<Id>();
        noOfRowsProcessed=0;
        try{
            dataErr = false;
            stagingRecordsList = mapStagingRecords();
            if(blankEmails){
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Email is Mandatory in all rows of excel sheet');
                ApexPages.addMessage(errormsg);
                dataErr = true;
            }
            if(blankReqIds){
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Requisition Number is Mandatory in all rows of excel sheet');
                ApexPages.addMessage(errormsg);
                dataErr = true;
                
            }
            if(dataErr){
                return null;
            }
            
        }
        Catch(System.StringException stringException)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_String_Exception_Error+stringException.getMessage());
            ApexPages.addMessage(errormsg);    
        }
        Catch(System.ListException listException)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Columns_count_not_proper);
            ApexPages.addMessage(errormsg); 
        
        }
        
        Catch(Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_Exception);
            ApexPages.addMessage(errormsg);    
        
        }
        
        if(stagingRecordsList.size()>Limits.getLimitQueryRows())
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Error_Data_File_too_large + Limits.getLimitQueryRows());
            ApexPages.addMessage(errormsg);   
        }
        Database.SaveResult[] srList = Database.insert(stagingRecordsList, false);
        for(Database.SaveResult sr: srList)
        {
            if(sr.IsSuccess())
            {   
                stagingIds.add(sr.getId());
                noOfRowsProcessed=stagingIds.size();
            }
            else if(!sr.IsSuccess())
            {
                for(Database.Error err : sr.getErrors())
                {
                     ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.WCT_Data_Error+err.getMessage());
                     ApexPages.addMessage(errormsg);       
                }
            }
        }   
        if(!stagingIds.IsEmpty())
        {
        
            try{
                processStagingRecords(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
            }Catch(Exception e)
            {
                throw e;
            }
            
        }
          
        return null;
    }
    
    private List<WCT_sObject_Staging_Records__c> mapStagingRecords()
    {
        nameFile=contentFile.toString();
        filelines=parseCSV(nameFile, true);
        List<WCT_sObject_Staging_Records__c> stagingRecordsList=new List<WCT_sObject_Staging_Records__c>();
        noOfRows=fileLines.size();
        system.debug(filelines);
        set<string> uniqueIdSet = new set<string>();
        for(List<String> inputValues:filelines) 
        {   
            
            system.debug(inputValues.size()+'stringSize');
            WCT_sObject_Staging_Records__c stagingRecord = new WCT_sObject_Staging_Records__c();
           
            stagingRecord.Cand_First_Name__c=inputValues[0];
            stagingRecord.Cand_Last_Name__c=inputValues[1];
            stagingRecord.Cand_Phone__c=inputValues[2];
            if(inputValues[3] <> null && inputValues[3] <>''){
                stagingRecord.Cand_Email__c=inputValues[3];
            }else{
                blankEmails = true;
            }
            if(inputValues[4] <> null && inputValues[4] <>''){
                stagingRecord.Cand_Requisition_Number__c=inputValues[4];
            }else{
                blankReqIds = true;
            }
            stagingRecord.Cand_Hiring_Location__c=inputValues[5];
            stagingRecord.Type__c='Candidate Load';
            stagingRecord.WCT_status__c=WCT_UtilConstants.STAGING_STATUS_NOT_STARTED;
            stagingRecordsList.add(stagingRecord);
        }
        return stagingRecordsList;
    }

    /** 
        Method Name  : parseCSV
        Return Type  : String contents,Boolean skipHeaders
        Description  : Business logic for parsing the string         
    */
    private List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
        List<List<String>> allFields = new List<List<String>>();
    
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        // we are not attempting to handle fields with a newline inside of them
        // so, split on newline to get the spreadsheet rows
        List<String> lines = new List<String>();
        try {
            lines = contents.split('\r\n');
        } catch (System.ListException e) {
            System.debug('Limits exceeded?' + e.getMessage());
        }
        system.debug(lines+'lines');
        Integer num = 0;
        for(String line : lines) {
            system.debug('line'+line);
            // check for blank CSV lines (only commas)
            if (line.replaceAll(',','').trim().length() == 0) continue;
            
            List<String> fields = line.split(',');  
            system.debug(fields);
            List<String> cleanFields = new List<String>();
            String compositeField;
            Boolean makeCompositeField = false;
            for(String field : fields) {
                if (field.startsWith('"') && field.endsWith('"')) {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                } else if (field.startsWith('"')) {
                    makeCompositeField = true;
                    compositeField = field;
                } else if (field.endsWith('"')) {
                    compositeField += ',' + field;
                    cleanFields.add(compositeField.replaceAll('DBLQT','"'));
                    makeCompositeField = false;
                } else if (makeCompositeField) {
                    compositeField +=  ',' + field;
                } else {
                    cleanFields.add(field.replaceAll('DBLQT','"'));
                }
            }
            
            allFields.add(cleanFields);
        }
        if (skipHeaders) allFields.remove(0);
        return allFields;       
    }
    
    public void processStagingRecords(Set<Id> stagingIds, String stagingStatus){
        set<string> CTUniqueIdentifiesSet = new set<string>();
        Map<String,WCT_sObject_Staging_Records__c> ctIndentifierStgRecMap = new Map<String,WCT_sObject_Staging_Records__c>();
        List<WCT_Candidate_Requisition__c> candTrackListToUpdate = new List<WCT_Candidate_Requisition__c>();
        List<WCT_sObject_Staging_Records__c> stgRecToUpdate = new List<WCT_sObject_Staging_Records__c>();
        Map<Id,string> CTIdUniqueIdentifierMap = new Map<Id,string>();
        Set<string> stagingRecsEmail= new Set<string>();
        Map<String,Contact> conMap= new Map<String,Contact>();
        set<string> CTUniqueIdentifiesSetfromSys = new set<string>();
        List<Contact> updateCon= new List<Contact>();
        List<WCT_sObject_Staging_Records__c> stgList= new List<WCT_sObject_Staging_Records__c>();
        Contact tmpcon= new Contact();
        addContact= new List<Contact>(); 
        Set<String> stagingRecsReqNum= new Set<String>();
        Map<String, String> ReqNumMap= new Map<String, String>();
       
        Map<String, String> conReqMap= new Map<String, String>();
        Set<String> candTrackSet= new Set<String>();
        
        
        for(WCT_sObject_Staging_Records__c stgRec:[SELECT id,Cand_First_Name__c,Cand_Last_Name__c,Cand_Phone__c,
                                                    Cand_Requisition_Number__c,Cand_Email__c,Cand_Hiring_Location__c
                                                    FROM WCT_sObject_Staging_Records__c 
                                                    WHERE id IN :stagingIds AND WCT_Status__c = : stagingStatus]){
                                         
            stagingRecsEmail.add(stgRec.Cand_Email__c);
            stagingRecsReqNum.add(stgRec.Cand_Requisition_Number__c); //set of all the requisition numbers from staging recs object
            stgList.add(stgRec);
            System.debug('enter here'+stagingRecsEmail);
                    
        }
        //Getting all contacts based on emails in excel sheet
        for(Contact c:[Select id,FirstName,LastName,email,Phone,WCT_Requisition_Number__c from Contact where email IN : stagingRecsEmail])
            {
               
                conMap.put(c.email,c);
                System.debug('enter here 1'+conMap);
                
            }
        for(WCT_sObject_Staging_Records__c stgRec: stgList)
        {   
            if(conMap.containsKey(stgRec.Cand_Email__c))
            {
                Contact tmpcon1= new Contact();
                tmpcon1=conMap.get(stgRec.Cand_Email__c);
                tmpcon1.FirstName= stgRec.Cand_First_Name__c;
                tmpcon1.LastName= stgRec.Cand_Last_Name__c;
                tmpcon1.Phone= stgRec.Cand_Phone__c;
                tmpcon1.WCT_Hiring_Location__c=stgRec.Cand_Hiring_Location__c;
                //tmpcon1.RecordTypeId= AdhocCandRecTypeId;
                //tmpcon1.WCT_Requisition_Number__c= stgRec.Cand_Requisition_Number__c;
                updateCon.add(tmpcon1);
                System.debug('enter here 2'+updateCon);
            }
            else
            {
                Contact inserttmpcon= new Contact();
                inserttmpcon.FirstName= stgRec.Cand_First_Name__c;
                inserttmpcon.LastName= stgRec.Cand_Last_Name__c;
                inserttmpcon.Phone= stgRec.Cand_Phone__c;
                inserttmpcon.RecordTypeId= AdhocCandRecTypeId;
                inserttmpcon.email=stgRec.Cand_Email__c;
                inserttmpcon.WCT_Hiring_Location__c=stgRec.Cand_Hiring_Location__c;
                //inserttmpcon.WCT_Requisition_Number__c= stgRec.Cand_Requisition_Number__c;
                addContact.add(inserttmpcon);
            }
        }
        if(!addContact.isEmpty())
            { 
                System.debug('enter here 5'+addContact);
                insert addContact;
                System.debug('enter here 6'+addContact);
            }
        if(!updateCon.isEmpty())    
            {
                System.debug('enter here 7'+updateCon);
                update updateCon;
                System.debug('enter here 8'+updateCon);
            } 
        for(Contact c:[Select id,FirstName,LastName,email,Phone,WCT_Requisition_Number__c from Contact where id IN : addContact])
            {
               
                conMap.put(c.email,c);
                System.debug('enter here 1'+conMap);
                
            }       
        for(WCT_Requisition__c cr: [SELECT id,Name
                        FROM WCT_Requisition__c WHERE Name IN :stagingRecsReqNum])  
            {
                ReqNumMap.put(cr.Name,cr.id);
                System.debug('Req num map'+ReqNumMap);
            }
        for(WCT_Candidate_Requisition__c t: [SELECT id,WCT_Requisition__r.Name, WCT_Email__c, WCT_Candidate_Requisition_Status__c
                        FROM WCT_Candidate_Requisition__c WHERE WCT_Email__c IN :stagingRecsEmail AND WCT_Candidate_Requisition_Status__c IN ('New','In Progress')])
            {
                candTrackSet.add(t.WCT_Email__c+'-'+t.WCT_Requisition__r.Name);
                System.debug('enter here 12'+candTrackSet);
            }   
        for(WCT_sObject_Staging_Records__c stgRec1: stgList)
            {
                if((conMap.containsKey(stgRec1.Cand_Email__c)) && (ReqNumMap.containsKey(stgRec1.Cand_Requisition_Number__c)))  
                {
                    System.debug('enter here 11'+conMap);
                    WCT_Candidate_Requisition__c ct1= new WCT_Candidate_Requisition__c();
                    if(!(candTrackSet.contains(stgRec1.Cand_Email__c+'-'+stgRec1.Cand_Requisition_Number__c)))
                    {
                        System.debug('enter here 10'+candTrackSet);
                        //if(ct1.WCT_Candidate_Requisition_Status__c != 'New')
                        //{
                            ct1.WCT_Requisition__c = ReqNumMap.get(stgRec1.Cand_Requisition_Number__c);
                            ct1.WCT_Contact__c = conMap.get(stgRec1.Cand_Email__c).Id;
                            candtrack.add(ct1);
                        //}
                    }    
                }  
            }       
        if(!candtrack.isEmpty())
            {
                insert candtrack;
            } 

        for(WCT_Candidate_Requisition__c ct: [SELECT id,WCT_Requisition__r.Name, WCT_Email__c, WCT_Candidate_Requisition_Status__c
                        FROM WCT_Candidate_Requisition__c WHERE WCT_Email__c IN :stagingRecsEmail AND WCT_Candidate_Requisition_Status__c IN ('New','In Progress') order by Createddate desc])
            {
                if(!candTrackMap.containsKey(ct.WCT_Email__c)){
                    candTrackMap.put(ct.WCT_Email__c,ct.id);
                }
            }
            
        for(WCT_sObject_Staging_Records__c stgRec1: stgList){
            
            if(!ReqNumMap.containsKey(stgRec1.Cand_Requisition_Number__c)){
                WCT_sObject_Staging_Records__c tmpStgRec = new WCT_sObject_Staging_Records__c();
                tmpStgRec = stgRec1;
                tmpStgRec.WCT_Status__c = 'Failed';
                tmpStgRec.WCT_Error_Message__c = 'No Requisition present in system with this Number'+stgRec1.Cand_Requisition_Number__c;
                stgRecToUpdate.add(tmpStgRec);
            }else{
                WCT_sObject_Staging_Records__c tmpStgRec = new WCT_sObject_Staging_Records__c();
                tmpStgRec = stgRec1;
                tmpStgRec.WCT_Status__c = 'Completed';
                tmpStgRec.WCT_Error_Message__c = 'No Error';
                stgRecToUpdate.add(tmpStgRec);
            }
        }
        if(!stgRecToUpdate.isEmpty()){
            update stgRecToUpdate;
        }
       
    }
    public pageReference getErrorRecords(){
        exportrngerr=false;
        return Page.WCT_CT_BulkUploadPage_Errors;
    }
    //method to get upload errors with date range
   public pageReference getErrorRecordsRange(){
        exportrngerr=true;
        return Page.WCT_CT_BulkUploadPage_Errors;
    }
     public List<WCT_sObject_Staging_Records__c> getAllStagingRecords()
    {
        if(!exportrngerr)
        {
        if (stagingIds!= NULL)
            if (stagingIds.size() > 0)
            {
                return [Select id,Name,Cand_First_Name__c,Cand_Last_Name__c,Cand_Phone__c,
                                                    Cand_Requisition_Number__c,Cand_Email__c,Cand_Hiring_Location__c,WCT_Error_Message__c
                                                      from WCT_sObject_Staging_Records__c where Id In:stagingIds and WCT_Status__c='Failed'];
                
            }
            else
                return null;                   
        else
            return null;
        }
        else
        {
            return[Select id,Name,Cand_First_Name__c,Cand_Last_Name__c,Cand_Phone__c,
                   Cand_Requisition_Number__c,Cand_Email__c,Cand_Hiring_Location__c,WCT_Error_Message__c
                   from WCT_sObject_Staging_Records__c where Type__c='Candidate Load' and WCT_Status__c='Failed' and createdbyid=:system.UserInfo.getUserId() and createdDate >= :candreq.WCT_Time_to_Schedule_First_Interview__c and createdDate <= :candreq.WCT_Time_till_start_of_first_Interview__c.addminutes(1)  order by createddate asc limit 500];
        }
    } 
    public pageReference WCT_CT_BulkUpload1()
    {
        ctList= new List<WCT_Candidate_Requisition__c>();
        System.debug('candtrack###'+candtrack);
        if(!candTrackMap.isEmpty()){
            for(WCT_Candidate_Requisition__c    ct: [SELECT id, Name, WCT_Contact__c, WCT_Contact__r.Name from WCT_Candidate_Requisition__c where id IN :candTrackMap.values()])
            {
                            ctList.add(ct);
                            System.debug('%%%'+ctList);
            }
        }
          pageReference returnURL;
          returnURL= Page.CT_export;
          returnURL.getParameters().put('nooverride','1');
          returnURL.setRedirect(false);
          return returnURL;         
    }
    
        public pageReference WCT_CT_BulkExportRange()
    {
        ctList= new List<WCT_Candidate_Requisition__c>();
        System.debug('candtrack###'+candtrack);
        //if(!candTrackMap.isEmpty()){
            for(WCT_Candidate_Requisition__c    ct: [SELECT id, Name, WCT_Contact__c, WCT_Contact__r.Name from WCT_Candidate_Requisition__c where createdDate >= :candreq.WCT_Time_to_Schedule_First_Interview__c and createdDate <= :candreq.WCT_Time_till_start_of_first_Interview__c.addminutes(1) and createdbyid=:system.UserInfo.getUserId() order by createddate asc limit 500])
            {
                            ctList.add(ct);
                            System.debug('%%%'+ctList);
            }
       // }
          pageReference returnURL;
          returnURL= Page.CT_export;
          returnURL.getParameters().put('nooverride','1');
          returnURL.setRedirect(false);
          return returnURL;          
    }  
}