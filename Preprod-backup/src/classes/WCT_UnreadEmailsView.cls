/**
    * Class Name  : Wct_UnreadEmailsView   
    * Description : Show all unread emails which are attached to case 
*/

public with sharing class WCT_UnreadEmailsView {
    
    //Initialize variables
    private final String emailStatus='0';
    public List<ShowAllEmailWrapper> showAllEmailList{get;set;}
    public String selectedEmailStatus{get;set;}
    
    
    /** 
        Method Name  : getEmailMessages   
        Return Type  : List<ShowAllEmailWrapper>       
    */
    public List<ShowAllEmailWrapper> getEmailMessages(){
        String mainQuery='SELECT Parent.Status,Parent.Subject,Parent.CaseNumber,Parent.WCT_Category__c,Parent.Owner.Name,ActivityId,BccAddress,CcAddress,CreatedById,CreatedDate,FromAddress,FromName,HasAttachment,Headers, ' +
                                        +' HtmlBody,Id,Incoming,IsDeleted,LastModifiedById,LastModifiedDate,MessageDate,ParentId,ReplyToEmailMessageId, '+
                                         +'  Status,Subject,SystemModstamp,TextBody,ToAddress FROM EmailMessage ';
        String whereCondition='';
        String orderBy=' order by parentId,LastModifiedDate';
        String userEmail=UserInfo.getUserEmail();
        String userId=UserInfo.getUserId();
        if (selectedEmailStatus==null)
           selectedEmailStatus='5';
        if(selectedEmailStatus=='0' || selectedEmailStatus=='1' || selectedEmailStatus=='2' || selectedEmailStatus=='3' || selectedEmailStatus=='4')
            whereCondition=' where status=:selectedEmailStatus  and Parent.OwnerId=:userId';
        else if(selectedEmailStatus=='5') {
            selectedEmailStatus ='0';
            whereCondition=' where status=:selectedEmailStatus and FromAddress = : userEmail';
        }
        else if(selectedEmailStatus=='6') {
            selectedEmailStatus ='1';
            whereCondition=' where status=:selectedEmailStatus and FromAddress = : userEmail';
        }
        else if(selectedEmailStatus=='7') {
            selectedEmailStatus ='3';
            whereCondition=' where status=:selectedEmailStatus and FromAddress = : userEmail';
        }
        else if(selectedEmailStatus=='8') {
            selectedEmailStatus ='3';
            whereCondition=' where status=:selectedEmailStatus and FromAddress = : userEmail';
        }   
        else if(selectedEmailStatus=='9') {
            selectedEmailStatus ='4';
            whereCondition=' where status=:selectedEmailStatus and FromAddress = : userEmail';
        }
        mainQuery=mainQuery+whereCondition + orderBy;
        showAllEmailList=new List<ShowAllEmailWrapper>();
        for (EmailMessage emailMessage : database.query(mainQuery))
        {
            showAllEmailList.add(new ShowAllEmailWrapper(emailMessage,'Own'));
        }
        return showAllEmailList;
    }
    
    /** 
        Method Name  : getEmailStatuses   
        Return Type  : List<SelectOption>       
    */
    
    public List<SelectOption> getEmailStatuses()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('5','My Unread Emails'));
        options.add(new SelectOption('6','My Read Emails'));
        options.add(new SelectOption('7',' My Sent Emails '));
        //options.add(new SelectOption('8','My Sent Emails'));
        //options.add(new SelectOption('9','My Forwarded Emails'));
        //options.add(new SelectOption('0','My Own Case New Emails'));
       // options.add(new SelectOption('1','My Own Case Read Emails'));
       // options.add(new SelectOption('2','My Own Case Replied Emails'));
        //options.add(new SelectOption('3','My Own Case Sent Emails'));
       // options.add(new SelectOption('4','My Own Case Forwarded Emails'));
        return options;
    }
    
    /*
        Wrapper Class: ShowAllEmailWrapper
        Description  : Wrapper to show all unread emails.
    */
    public class ShowAllEmailWrapper{
        //Initialize variables
        public EmailMessage emailMessage {get;set;}
        public String ownership{get;set;}
        /*
        Constructor : ShowAllEmailWrapper
        Description : Assigning values to EmailMessgeList.
        */
        public showAllEmailWrapper(EmailMessage message,String owner)
        {
                emailMessage=message;
                ownerShip=owner;
                
        }
    }
}