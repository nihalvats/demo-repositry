@isTest
 private class WCT_H1bcap_SM_Notification_Test{
 Static testmethod void WCT_H1bcap_SM_Notification_TestMethod(){
 
         Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SM']; 
        User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','svalluru@deloitte.com');
        insert platuser;
  
  
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.WCT_Employee_Group__c = 'Active';
        insert con;
  
  list<WCT_H1BCAP__c> lst = new list<WCT_H1BCAP__c>();
        WCT_H1BCAP__c h1 = new WCT_H1BCAP__c();
        h1.WCT_H1BCAP_Practitioner_Name__c = con.id;
        h1.WCT_H1BCAP_Email_ID__c = 'svalluru@gmail.com';
        h1.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
        h1.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'abc@yahoo.com';
        h1.WCT_H1BCAP_Status__c  = 'Willing To Travel';
        h1.WCT_H1BCAP_USI_SM_Name_GDM__c = platuser.id;
        h1.recalculateFormulas(); 
        lst.add(h1);
        insert lst;   
    
     Date d = system.Today().addDays(-10);
     Test.setCreatedDate(lst[0].Id, d);
 
  //System.assertEquals(d,h1b.createdDate) ;  
  
         string orgmail =  Label.WCT_H1BCAP_Mailbox;
         OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
         Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'Notification_to_USI_SM_US_PPD' AND IsActive = true];
        
         List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
         list<string>  currentRMEmail = new list<string>();
         currentRMEmail.add( lst[0].WCT_H1BCAP_Resource_Manager_Email_ID__c);
         mail.SetTemplateid(et.id);
         mail.setSaveAsActivity(false);
         mail.setTargetObjectId(lst[0].WCT_H1BCAP_USI_SM_Name_GDM__c );
         //   mail.setCcAddresses(currentRMEmail);
         mail.setWhatid(lst[0].id);
         //   mail.setWhatid(con.id);
         mail.setOrgWideEmailAddressId(owe.id);   
         mailList.add(mail);     
    
    
        
     Test.StartTest();
     
    
      WCT_H1bcap_SM_Notification h1b1 = new WCT_H1bcap_SM_Notification();
    //  h1b.WCT_H1BCAP_Status__c = 'Ready for USI SM / US PPD approval';  
    //  h1b.WCT_H1BCAP_Capture_BeginingDate_SM__c = system.today(); 
      WCT_H1bcap_SM_Notification_Schedule createCon = new WCT_H1bcap_SM_Notification_Schedule ();
      system.schedule('New','0 0 2 1 * ?',createCon);  
      database.Executebatch(h1b1);
    
   
     
     Test.StopTest();   
    
   
     }
 }