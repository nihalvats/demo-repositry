global class WCT_UpdateUserFieldsFromContact implements Database.Batchable<sObject> {
   public String query;

   global WCT_UpdateUserFieldsFromContact (String queryStr)
   {
       query=queryStr;
   }
   
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<User> userList){
      Set<String> emailIds=new Set<String>();
      List<Contact> conList=new List<Contact>();
      Map<String,User> emailUserMap = new Map<String,User>();
      Map<String,Contact> emailContactMap = new Map<String,Contact>();
      List<User> userToUpdate = new List<User>();
      for(User obj:userList)
      {
        if(obj.Email!=null)
          emailIds.add(obj.Email);
          emailUserMap.put(obj.email,obj);
      }
      if(!emailIds.isEmpty()){
        for(Contact con :[Select id,Phone,WCT_Company_Code__c,email,WCT_Employee_Status__c from Contact
                           where recordTypeId = :WCT_Util.getRecordTypeIdByLabel('Contact', 'Employee')
                           AND Email IN:emailIds AND WCT_Employee_Status__c <> 'Withdrawn' AND WCT_Employee_Status__c <>'Retiree']){
                emailContactMap.put(con.email,con);
        }
     }
     
     for(User usr:userList){
        if(emailContactMap.containsKey(usr.email)){
            User tempUsr = new User();
            tempUsr = usr;
            boolean updated = false;
            if(usr.Direct_No__c == null || (usr.Direct_No__c <> null && emailContactMap.get(usr.email).phone<> null && usr.Direct_No__c <> emailContactMap.get(usr.email).phone)){
                tempUsr.Direct_No__c = emailContactMap.get(usr.email).phone;
                updated = true;
            }
            if(usr.Legal_Entity__c == null || (usr.Legal_Entity__c <> null && emailContactMap.get(usr.email).WCT_Company_Code__c<>null && usr.Legal_Entity__c <> emailContactMap.get(usr.email).WCT_Company_Code__c)){
                tempUsr.Legal_Entity__c = emailContactMap.get(usr.email).WCT_Company_Code__c;
                updated = true;
            }
            if(Updated){
                userToUpdate.add(tempUsr);
            }
        }    
     }
        
      try
      {  
        if(!userToUpdate.isEmpty()){
            update userToUpdate;
        }
      }Catch(Exception e)
      {
        throw e;
      }
      
   }

   global void finish(Database.BatchableContext BC){
   }
}