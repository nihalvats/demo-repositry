/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CTBulkDownloadSearch_Test {

    /*******Varibale Declaration********/
    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
    public static Id profileIdIntv=[Select id from Profile where Name=:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
    public static Id profileIdRecr=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
    public static User userRecRecCoo, userRecRecIntv, userRecRecr,userIntvtoAdd;
    public static Contact Candidate;
    public static WCT_Requisition__c Requisition;
    public static WCT_Candidate_Requisition__c CandidateRequisition;
    public static WCT_Interview__c Interview;
    public static WCT_Interview_Junction__c InterviewTracker;
    public static Event InterviewEvent;
    public static EventRelation InterviewEventRelation;
    public static WCT_List_Of_Names__c ClickToolForm;
    public static boolean isBlankIEF, isBlankSampleIEF;
    public static WCT_Candidate_Documents__c CandidateDocument,CandidateDocumentUpdated;
    public static Attachment attachment;
    public static Document document;
    /***************/

    /** 
        Method Name  : createUserRecCoo
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecCoo()
    {
        userRecRecCoo=WCT_UtilTestDataCreation.createUser('RecCoo', profileIdRecCoo, 'arunsharmaRecCoo@deloitte.com', 'arunsharma4@deloitte.com');
        insert userRecRecCoo;
        return  userRecRecCoo;
    }
    /** 
        Method Name  : createUserRecr
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecr()
    {
        userRecRecr=WCT_UtilTestDataCreation.createUser('Recr', profileIdRecr, 'arunsharmaRecr@deloitte.com', 'arunsharma4@deloitte.com');
        insert userRecRecr;
        return  userRecRecr;
    }
    /** 
        Method Name  : createUserIntv
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserIntv()
    {
        userRecRecIntv=WCT_UtilTestDataCreation.createUser('Intv', profileIdIntv, 'arunsharmaIntv@deloitte.com', 'arunsharma4@deloitte.com');
        insert userRecRecIntv;
        return  userRecRecIntv;
    }
    /** 
        Method Name  : createCandidate
        Return Type  : Contact
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createCandidate()
    {
        Candidate=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        insert Candidate;
        return  Candidate;
    }    
    /** 
        Method Name  : createRequisition
        Return Type  : WCT_Requisition__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Requisition__c createRequisition()
    {
        Requisition=WCT_UtilTestDataCreation.createRequisition(userRecRecr.Id);
        insert Requisition;
        return  Requisition;
    }
    /** 
        Method Name  : createCandidateRequisition
        Return Type  : WCT_Candidate_Requisition__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Candidate_Requisition__c createCandidateRequisition()
    {
        CandidateRequisition=WCT_UtilTestDataCreation.createCandidateRequisition(Candidate.ID,Requisition.Id);
        insert CandidateRequisition;
        return  CandidateRequisition;
    } 
    /** 
        Method Name  : createInterview
        Return Type  : WCT_Interview__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview__c createInterview()
    {
        Interview=WCT_UtilTestDataCreation.createInterview(ClickToolForm.Id);
        system.runAs(userRecRecCoo)
        {
            insert Interview;
        }
        return  Interview;
    }   
    /** 
        Method Name  : createInterviewTracker
        Return Type  : WCT_Interview_Junction__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Interview_Junction__c createInterviewTracker()
    {
        InterviewTracker=WCT_UtilTestDataCreation.createInterviewTracker(Interview.Id,CandidateRequisition.Id,userRecRecIntv.Id);
        insert InterviewTracker;
        return  InterviewTracker;
    }  
    /** 
        Method Name  : createEvent
        Return Type  : Event
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static Event createEvent()
    {
        InterviewEvent=WCT_UtilTestDataCreation.createEvent(Interview.Id);
        system.runAs(userRecRecCoo)
        {
            insert InterviewEvent;
        }
        return  InterviewEvent;
    }     
    /** 
        Method Name  : createEventRelation
        Return Type  : EventRelation
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static EventRelation createEventRelation()
    {
        InterviewEventRelation=WCT_UtilTestDataCreation.createEventRelation(InterviewEvent.Id,userRecRecIntv.Id);
        insert InterviewEventRelation;
        return  InterviewEventRelation;
    }   
    /** 
        Method Name  : cretaEmailBody
        Return Type  : string
        Type         : private
        Description  : Create temp Email body String         
    */   
    private static string cretaEmailBody()
    {
        return '<html><body><p>Email Body Top</p><p><ol><li>Email Body Center</li></ol></p><p>Email Body Bottom</p></body></html>';
    }  
    /** 
        Method Name  : createCandidateDocument
        Return Type  : WCT_Candidate_Documents__c
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static WCT_Candidate_Documents__c createCandidateDocument()
    {
        CandidateDocument=WCT_UtilTestDataCreation.createCandidateDocument(Candidate.Id);
        insert CandidateDocument;
        return  CandidateDocument;
    }
    /** 
        Method Name  : createAttachment
        Return Type  : Attachment
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static Attachment createAttachment()
    {
        attachment=WCT_UtilTestDataCreation.createAttachment(CandidateDocument.Id);
        insert attachment;
        return  attachment;
    } 
    /** 
        Method Name  : createDocument
        Return Type  : Document
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static Document createDocument()
    {
        document=WCT_UtilTestDataCreation.createDocument();
        insert document;
        return  document;
    }     
    /** 
        Method Name  : createClickToolForm
        Return Type  : WCT_List_Of_Names__c
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_List_Of_Names__c createClickToolForm()
    {
        ClickToolForm=WCT_UtilTestDataCreation.createClickToolForm(document.id);
        insert ClickToolForm; 
        return  ClickToolForm;
    }    
    
    static testMethod void myUnitTest() {
        userRecRecCoo=createUserRecCoo();
        userRecRecIntv=createUserIntv();
        userRecRecr=createUserRecr();
        Candidate=createCandidate();
        Requisition=createRequisition();
        CandidateRequisition=createCandidateRequisition();
        document=createDocument();
        ClickToolForm=createClickToolForm();
        Interview=createInterview();
        InterviewEvent=createEvent();
        InterviewEventRelation=createEventRelation();
        InterviewTracker=createInterviewTracker();
        CandidateDocument=createCandidateDocument();
        CandidateDocumentUpdated = createCandidateDocument();
        Attachment att = WCT_UtilTestDataCreation.createAdhocAttachment(Candidate.id);
        insert att;
        test.startTest();
        
        CTBulkDownloadSearch_Ctrl ctbs = new CTBulkDownloadSearch_Ctrl();
        
        ctbs.searchCandidates();
        ctbs.candTrackWrapList[0].isSelected = true;
        ctbs.getDocuments();
        ctbs.FSSValue = 'Tax';
        ctbs.SchoolValue = 'GGHS';
        ctbs.tempcontactforRecruiterLookup.NDR_Owner__c = UserInfo.getUserId();
        
        
     
        ctbs.tempCandTrackerFromDate.WCT_Time_till_start_of_first_Interview__c = system.now();
        ctbs.tempCandTrackerToDate.WCT_Time_till_start_of_first_Interview__c = system.now()+10;
        ctbs.searchCandidates();
        ctbs.getJobTypeValues();
      
     
        ctbs.tempCandTrackerToDate.WCT_Time_till_start_of_first_Interview__c = system.now()-10;
        ctbs.searchCandidates();

        /*Test coverage for new fields introduced as part of dec/jan enhancement(1/21/2014)*/
       
      
        ctbs.documentName='dummybulksearchname';
        ctbs.searchCandidates();
        ctbs.tempCandTrakerForDocDates.WCT_Time_to_Schedule_First_Interview__c=datetime.now();
        ctbs.tempCandTrakerForDocDates.WCT_Time_till_start_of_first_Interview__c=datetime.now()+1;
        
        ctbs.documentName='dummybulksearchname';
        ctbs.tempCandTrackerFromDate.WCT_Contact__r.lastname='BulksearchTest1212014';
        ctbs.searchCandidates();
        
        ctbs.tempCandTrakerForInterviewDate.WCT_Time_till_start_of_first_Interview__c=datetime.now();
        ctbs.tempCandTrakerForDocDates.WCT_Time_to_Schedule_First_Interview__c=datetime.now()+1;
        ctbs.tempCandTrakerForDocDates.WCT_Time_till_start_of_first_Interview__c=datetime.now();
        ctbs.documentName='dummybulksearchname';
        ctbs.searchCandidates();
       
        //ctbs.tempCandTrackerToDate.WCT_Requisition__r.WCT_Recruiting_Coordinator__c='sample';
        //ctbs.searchCandidates();
        ctbs.tempCandTrackerFromDate.WCT_Contact__r.lastname='Sample Name';
        ctbs.searchCandidates();
        ctbs.tempCandTrackerFromDate.WCT_Requisition__c=Requisition.id;
        ctbs.tempCandTrackerFromDate.WCT_Contact__r.lastname='BulksearchTest1212014'; 
        ctbs.searchCandidates();

        
       
        /*End of test class update*/
        CTBulkDownloadSearch_Ctrl.getAttachment(string.valueof(att.id));
        ctbs.zipFileName = 'testFile';
        ctbs.zipContent ='testBody';
        ctbs.uploadZip();
        
        ctbs.zipContent ='testBody';
        ctbs.uploadZip();
        test.stopTest();
    }
     
    public static testmethod void DocumentNameTest()
    {
        CTBulkDownloadSearch_Ctrl ctbs = new CTBulkDownloadSearch_Ctrl();
        ctbs.tempCandTrakerForDocDates.WCT_Time_to_Schedule_First_Interview__c=datetime.now();
        ctbs.tempCandTrakerForDocDates.WCT_Time_till_start_of_first_Interview__c=datetime.now()+1;
        ctbs.documentName='Attachment';
        ctbs.searchCandidates();
               
        ctbs.documentName='Attachment';
        ctbs.searchCandidates();
        
        ctbs.tempCandTrakerForInterviewDate.WCT_Time_till_start_of_first_Interview__c=datetime.now();
        ctbs.searchCandidates();
    }
    
    public static testmethod void test2()
    {
        CTBulkDownloadSearch_Ctrl ctbs = new CTBulkDownloadSearch_Ctrl();
       ctbs.candidateEmailId='test1212014@testing.com';
        ctbs.searchCandidates();
        ctbs.tempCandTrackerFromDate.WCT_Contact__r.lastname='bulklatesttest';
        ctbs.interviewJobtype='TestType';
        ctbs.tempcontactforUserLookup.NDR_Owner__c= UserInfo.getUserId();
        ctbs.searchCandidates();
        
    }
}