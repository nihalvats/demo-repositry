@isTest(SeeAllData=false) 
 private class WCT_H1bcap_SLL_Notification_Test{
  Static testmethod void WCT_H1bcap_SLL_Notification_TestMethod(){
 
 
  Date td = system.Today().addDays(-11);
 
  
   Profile ppd = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_PPD']; 
   User platuser1=WCT_UtilTestDataCreation.createUser( 'siva837',ppd.id,' svallurutest9@deloitte.com.preprod','svalluru@deloitte.com');
   insert platuser1;
  
   Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SLL']; 
   User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','svalluru@deloitte.com');
   insert platuser; 
  
  
    recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
    Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
    con.WCT_Employee_Group__c = 'Active';
    insert con;
  
    list<WCT_H1BCAP__c> lst = new list<WCT_H1BCAP__c>();
    WCT_H1BCAP__c h1 = new WCT_H1BCAP__c();
    h1.WCT_H1BCAP_Practitioner_Name__c = con.id;
    h1.WCT_H1BCAP_Email_ID__c = 'svalluru@gmail.com';
    h1.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
    h1.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'abc@yahoo.com';
    h1.WCT_H1BCAP_Status__c  = 'USI SM Approved';
    h1.WCT_H1BCAP_USI_SLL_Name__c = platuser.id;
    h1.WCT_H1BCAP_US_PPD_Name__c = platuser1.id;
    h1.WCT_H1BCAP_Capture_BeginingDate_SM__c = td;
    h1.recalculateFormulas(); 
    lst.add(h1);
    insert lst;   
     
  WCT_H1BCAP__c  h1b = [select id,WCT_H1BCAP_US_PPD_Name__c ,WCT_H1BCAP_After7thBusinessday_SM__c ,WCT_H1BCAP_Status__c,WCT_H1BCAP_USI_SLL_Name__c from WCT_H1BCAP__c where id =:lst[0].id   limit 1 ];         
   
  update h1b; 
  //System.assertEquals(td,h1b.WCT_H1BCAP_After7thBusinessday_SM__c ) ;  
  system.debug('7thday*******'+h1b.WCT_H1BCAP_After7thBusinessday_SM__c );
  system.debug('dlist****'+h1b);
  
         string orgmail =  Label.WCT_H1BCAP_Mailbox;
         OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
         Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'Notification_to_SLL_for_Nomination' AND IsActive = true];
        
         List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
         list<string>  currentRMEmail = new list<string>();
         currentRMEmail.add( lst[0].WCT_H1BCAP_Resource_Manager_Email_ID__c);
         mail.SetTemplateid(et.id);
         mail.setSaveAsActivity(false);
         mail.setTargetObjectId(h1b.WCT_H1BCAP_USI_SLL_Name__c);
         //   mail.setCcAddresses(currentRMEmail);
         mail.setWhatid(h1b.id);
         //   mail.setWhatid(con.id);
         mail.setOrgWideEmailAddressId(owe.id);   
         mailList.add(mail);     
    
       List<Messaging.SingleEmailMessage> mailList1 = new List<Messaging.SingleEmailMessage>();
       Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage(); 
       mail.SetTemplateid(et.id);
       mail.setSaveAsActivity(false);
       mail.setTargetObjectId(h1b.WCT_H1BCAP_US_PPD_Name__c);
       mail.setWhatid(h1b.id);
       mail.setOrgWideEmailAddressId(owe.id);   
       mailList1.add(mail1);     
    
  
        
     Test.StartTest();
     WCT_H1bcap_SLL_Notification h1b1 = new WCT_H1bcap_SLL_Notification ();
     h1b.WCT_H1BCAP_Status__c = 'Ready for SLL approval';  
     h1b.WCT_H1BCAP_Capture_BeginingDate_SLL__c = system.today(); 
     WCT_H1bcap_SLL_Notification_Schedule  createCon = new WCT_H1bcap_SLL_Notification_Schedule  ();
     system.schedule('New','0 0 2 1 * ?',createCon);
     database.Executebatch(h1b1);
     system.debug('Updatedtherecord***'+h1b); 
     Test.StopTest();   

        
   
     }
 }