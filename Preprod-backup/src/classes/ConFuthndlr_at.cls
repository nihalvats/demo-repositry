/*****************************************************************************************
    Name    : ConFuthndlr_at( Contact Future Handler)
    Desc    : This Future Class to handle asyncrhnos operations 
    Approach: crtConCoach (Cretae Contact Coaching record) is a method which is used to create contact coaching records based on new contact record insertion.
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Fazal Hussain                   14 Mar,2016           Created 
******************************************************************************************/

public class ConFuthndlr_at
{   
    //@future
    public static void crtConCoach(List<ID> recs)
    {
        List<Contact> ccon = new List<Contact>();
        ccon =[select id,name,DTA_Record_exists__c from contact where id in:recs];        
        for (Contact C : ccon)
        {
            c.DTA_Record_exists__c  = 'Yes';
            
                        
            
        }
        
        Contact_Coaching__c conCoach= New Contact_Coaching__c();
        List<Contact_Coaching__c> cc = new List<Contact_Coaching__c>();
        List<Contact_Coaching__c> ccs = new List<Contact_Coaching__c>();
        Map<Id,Id> Map_Id=new Map<Id,Id>();
        Contact ConNew;
        cc=[select id,contact__c from Contact_Coaching__c where Contact__c in : recs];
        if(cc.isempty()){
        for(Contact Cont : cCon)
        {  
            ConCoach.Status__c='Referred';
            ConCoach.Contact__c= Cont.id;
            ConCoach.RecordTypeId = '012190000008lcrAAA';
            ConCoach.Referral_Type__c = 'Involuntary';
            ConCoach.Client_Referred_Date__c = date.today();
            ConCoach.Referral_Source__c ='SAP/CRIS feed';
            ConCoach.DTA_Email__c= Cont.Email;
            cc.add(concoach);
            
            
        } 
            System.debug('Before Insert');
            insert cc;
             ccs=[select id,contact__c from Contact_Coaching__c where Contact__c in : recs];
            if(!ccs.isempty()){
                for(Contact_Coaching__c ct: ccs){
                  Map_Id.put(ct.contact__c,ct.id);  
                }   
            }
            for (Contact C : ccon)
           {
            c.Contact_Coaching__c=Map_Id.get(c.id);
           }
            System.debug('After Insert');            
            update ccon;

        }
    }
}