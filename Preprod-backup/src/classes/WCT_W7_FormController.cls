/********************************************************************************************************************************
Apex class         : <WCT_W7_FormController>
Description        : <Controller which allows to Update Task and Upload Attachments>
Type               :  Controller
Test Class         : <WCT_W7_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 23/05/2016          74%                          --                            License Cleanup Project
************************************************************************************************************************************/   

public  class WCT_W7_FormController extends SitesTodHeaderController {
    
     //Creating a Mobility Instance
     public WCT_Mobility__c mobilityRecord { get; set; }
     
     //Task Related Variables       
     public String taskID { get; set;} 
     public WCT_Task_ManageHandler taskInstance {get;set;} 

     //Error Related Variables
     public boolean pageError {get; set;}  
     public String pageErrorMessage {get;set; }   
     public String supportAreaErrorMesssage { get;set; }     
     public boolean validPage { get;set; }   
     
     //Attachment Related Variables
     
     public Document doc { get; set; }    
     public List <String> docIdList = new List <String> ();   
     public GBL_Attachments attachmentHelper { get;set; }
     public List <Attachment> listAttachments { get;set;}    
     public Map <Id,String> mapAttachmentSize {get;set; } 

     /* Invoking a Constructor with Task and Attachments  */

     public WCT_W7_FormController() {
        taskInstance = new WCT_Task_ManageHandler();
        attachmentHelper = new GBL_Attachments();
        init();
        if (taskInstance.isError) {
            validPage = false;
        } else {
            validPage = true;
        }

        getAttachmentInfo();

    }
        
    /********************************************************************************************
    *Method Name         : <init()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <Init() Used to Initializing Attachments and Mobility>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/          
    
     @TestVisible
     private void init() {

        mobilityRecord = new WCT_Mobility__c();
        doc = new Document();
        listAttachments = new List <Attachment> ();
        mapAttachmentSize = new Map <Id,String> ();

     }
        
    /********************************************************************************************
    *Method Name         : <getAttachmentInfo()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <GetAttachmentInfo() Used to Retrieving Attachment Details>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/  
  
     @TestVisible
     private void getAttachmentInfo() {
        listAttachments = [SELECT ParentId,
            Name,
            BodyLength,
            Id,
            CreatedDate
            FROM Attachment
            WHERE ParentId = : taskInstance.taskID
            ORDER BY CreatedDate DESC
            LIMIT 50
        ];

        for (Attachment a: listAttachments) {
            String size = null;

            if (1048576 < a.BodyLength) {
                //SIZE GREATAR THAN 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            } else if (1024 < a.BodyLength) {
                //SIZE GREATER THAN 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';
            } else {
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }

    /********************************************************************************************
    *Method Name         : <save()>
    *Return Type         : <Page Reference>
    *Param’s             : 
    *Description         : <Save() Used to Updating Task and Mobility>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/ 

      public pageReference save() {
      
      //Try and Catchblock for throw while any exception happens while task updates 
      
          try {
              taskInstance.saveTask();
              taskInstance.taskObj.OwnerId = UserInfo.getUserId();
              upsert taskInstance.taskObj;
          //Check whether file is attached
          if (attachmentHelper.docIdList.isEmpty()) {
              pageErrorMessage = 'Attachment is required to submit the form.';
              pageError = true;
              return null;
          }
          getAttachmentInfo();
          attachmentHelper.uploadRelatedAttachment(taskInstance.taskID);
          } 
          
          catch (Exception e) {
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_W7_FormController', 'W7 Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_W7_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
            
          }

          PageReference pageRef = new PageReference('/apex/WCT_W7_FormThankyou?taskid=' + taskInstance.taskID);
          pageRef.setRedirect(true);
          return pageRef;
    }

}