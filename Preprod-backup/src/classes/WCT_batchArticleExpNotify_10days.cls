/*******************************************************************
 APEX CLASS :WCT_batchArticleExpNotify_10days
 DESCRIPTION: This batch class is written to update the article to Draft if the article gets expired
 CREATED BY: Chandrasekhar Pinarouthu
 CREATED DATE:8/15/2013
********************************************************************/
global class WCT_batchArticleExpNotify_10days implements Database.batchable< KnowledgeArticleVersion >{

    /******************************************
    METHOD NAME: Start
    DESCRIPTION: This method will fetch all the article from KnowledgeArticleVersion. 
                 The output of this method is a query passed as an input to Execute method
    ******************************************/
global Iterable< KnowledgeArticleVersion > start(Database.batchableContext info){
        // Fetching all the Article which are Online
   //      List<query> = 'SELECT ArticleNumber,ArticleType,CreatedBy.email,KnowledgeArticleId,Title,UrlName FROM KnowledgeArticleVersion where PublishStatus = \'Online\' and Language = \'en_US\'';
        // Return the query results to execute method
    //    return Database.getQueryLocator([SELECT ArticleNumber,ArticleType,CreatedBy.email,KnowledgeArticleId,Title,UrlName FROM KnowledgeArticleVersion where PublishStatus = 'Online' and Language = 'en_US']);
       return new WCT_KAVExp();
    }

    global void execute(Database.BatchableContext BC, List < sObject > articles) {
    
    WCT_ArticleExp_EmailNotify_10days.kavNames();

        
    }
    global void finish(Database.BatchableContext BC ) {
    }

}