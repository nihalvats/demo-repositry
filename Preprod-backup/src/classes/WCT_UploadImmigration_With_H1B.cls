/**  
* Class Name  : WCT_UploadImmigration_With_H1B 
* Description : This apex class will use to upload CSV into staging table
*
*  04/10/2016 : Updated to supoport GSS Law Firm
                Adding the "law_Firm" to the page to differentate the process for GSS & Fragomen.
*
 
*/

public class WCT_UploadImmigration_With_H1B
{
    
    
    Transient List<WCT_Immigration_Stagging_Table__c> stagingRecordsList;
    public  Blob contentFile{get;set;}
    public Integer noOfRows{get;set;}
    public Integer noOfRowsProcessed{get;set;}
    Transient  Set<Id> stagingIds=new Set<Id>();     // Store all StagingIds after reading from file.
    public String nameFile{get;set;}
    public List<List<String>> fileLines = new List<List<String>>();
    Map<String,Integer> questionColOrderMap=new Map<String,Integer>();
    public List<RowWrapper> allRows{get; set;}
    public List<String> headerRow{get; set;}
    
    public String law_Firm_Name{get; set;}
    
    public WCT_UploadImmigration_With_H1B()
    {
        
        /*Check for Law Firm*/
        law_Firm_Name= ApexPages.currentPage().getParameters().get('law_Firm');
        
        if(law_Firm_Name==null)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Law Firm is required');
            ApexPages.addMessage(errormsg); 
        }
    }
    
    /*Get the id of the record type of Stagging named as "VTS Upload Record"*/
    public String VTS_Upload_Staging_RecordTypeId
    {
        get{
            if(VTS_Upload_Staging_RecordTypeId==null || VTS_Upload_Staging_RecordTypeId=='')
            {
                VTS_Upload_Staging_RecordTypeId= Schema.SObjectType.WCT_Immigration_Stagging_Table__c.getRecordTypeInfosByName().get('VTS Upload Record').getRecordTypeId();
                return VTS_Upload_Staging_RecordTypeId;
            }
            else
            {
                return VTS_Upload_Staging_RecordTypeId;
            }
            
        }
           set;
     }
    
    
    Set<String> validList;
    List<String> columnsNotPresent{get; set;}
    
    
    WCT_parseCSV parseCSVInstance = new WCT_parseCSV();
    
    List<WCT_List_Of_Names__c> validColumns;
    
    /** 
        Method Name  : readFile
        Return Type  : PageReference
        Description  : Read CSV and push records into staging table        
    */
    
    public Pagereference readFile()
    {
        stagingRecordsList = new List<WCT_Immigration_Stagging_Table__c>(); 
        noOfRowsProcessed=0;
        columnsNotPresent= new List<String>();
        validList= new Set<String>();
        stagingIds= new  Set<Id>();
        headerRow= new List<String>();
        //csvType = this.csvType; //Getting from VF
        System.debug('readFile Started');
        try
        {
            stagingRecordsList= mapStagingRecords();
        }
        Catch(System.StringException stringException)
        {
            System.debug(' listException '+stringException);
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_String_Exception_Error + stringException);
            ApexPages.addMessage(errormsg);    
            WCT_ExceptionUtility.logException('WCT_UploadImmigration_With_H1B','readFile',stringException.getMessage()+' Line # '+stringException.getLineNumber());
        }
       
        Catch(System.ListException listException)
        {
            System.debug(' listException '+listException);
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Columns_count_not_proper +listException);
            ApexPages.addMessage(errormsg); 
             WCT_ExceptionUtility.logException('WCT_UploadImmigration_With_H1B','readFile',listException.getMessage()+' Line # '+listException.getLineNumber());
            
        }
        Catch(Exception e)
        {
            System.debug(' listException '+e);
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_Exception +e );
            ApexPages.addMessage(errormsg);    
            WCT_ExceptionUtility.logException('WCT_UploadImmigration_With_H1B','readFile',e.getMessage()+' Line # '+e.getLineNumber());
            
        }
       
        
        if(stagingRecordsList.size()>Limits.getLimitQueryRows())
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Error_Data_File_too_large + Limits.getLimitQueryRows());
            ApexPages.addMessage(errormsg);   
        }
        Database.SaveResult[] srList = Database.insert(stagingRecordsList, false);
        for(Database.SaveResult sr: srList)
        {
            if(sr.IsSuccess())
            {   
                stagingIds.add(sr.getId());
                noOfRowsProcessed=stagingIds.size();
            }
            else if(!sr.IsSuccess())
            {
                for(Database.Error err : sr.getErrors())
                {
                    ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.WCT_Data_Error+err.getMessage());
                    ApexPages.addMessage(errormsg);       
                }
            }
        }   
        if(!stagingIds.IsEmpty())
        {            
            try{                
                VTS_ProcessStaggingImigrationRecords.processImmigrationRecords(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);               
            }Catch(Exception e)
            {
                throw e;
            }
            
        }
        contentFile=null;
        return null;
        
    }
    
    /** 
Method Name  : mapStagingRecords
Return Type  : List<WCT_Immigration_Stagging_Table__c>
Description  : Map fields names with CSV col names         
*/
    private List<WCT_Immigration_Stagging_Table__c> mapStagingRecords()
    {
        //System.debug('## contentFile '+contentFile);
        List<WCT_Immigration_Stagging_Table__c> stagingRecordsList=new List<WCT_Immigration_Stagging_Table__c>();
        
        
        /*New method to convert the Special Character blob to string.*/
        nameFile=WCT_parseCSV.blobToString(contentFile);

       System.debug('## '+nameFile);
        
        try
        {
            filelines = parseCSVInstance.parseCSV(nameFile, false);
        }
        catch(Exception e)
        {
            System.debug('Expection in CSV File '+e);
        }
        
        if(filelines.size()>0)
        {
            system.debug('filelines.size() started'+filelines.size());
            
            for(integer i=0; i<filelines.size();i++)
            {
                system.debug('## Size'+filelines[0]!=null?filelines[i].size():0);
                system.debug('filelines line started'+filelines[i]);
            }
            
            /*
            * Validates if all the configured header are present in the fragomen file.
            */
            headerRow=lowerCase(filelines[0]);
            System.debug('##  HeaderRow '+headerRow);
            boolean isValidFile= validateHeader(headerRow);
            
            if(!isValidFile)
            {
                /*
                 * Prompt if all header columns not present or  any field is mispelled in the file. 
                 */
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Missing needed Header '+columnsNotPresent);
                ApexPages.addMessage(errormsg);  
                
                return stagingRecordsList;
            }
            
            /*Pull the rows.*/
            
            /*
            * Get all the rows as a wrapper present in the file.
            */
            allRows=generateAllRows(filelines);
            
            if(allRows.size()==0)
            {
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'No Records to upload');
                ApexPages.addMessage(errormsg);  
                
                return stagingRecordsList;
            }
           // System.debug('### allRows '+allRows);
            
            /*From warpper build the Stagging records.
            * With validation. 
            * Insert the Stagging records.
            */
                       
                        /* 
            * Querying the contact related the email Id sent in fragome file.
            * 
            */
            noOfRows=fileLines.size()-1;
            Map<string,id> emailToIdMap_Emp = new Map<string,id>();
            Map<string,id> emailToIdMap_Cand = new Map<string,id>();
            Map<string,id> emailToIdMap_Adhoc = new Map<string,id>();
            set<string> empEmailsSet = new set<string>();
            
            for(RowWrapper wrapper : allRows)
            {
                if(wrapper.row.get('fn email address') <> null && wrapper.row.get('fn email address') <> '')
                {
                    empEmailsSet.add(wrapper.row.get('fn email address'));
                }
            }    
            
            if(!empEmailsSet.isEmpty())
            {
                /*Looking for Contacts with Email & Additional Email Id. */
                for(contact con : [SELECT id,Fragomen_Email__c, Email, AR_Deloitte_Email__c, AR_Personal_Email__c, WCT_ExternalEmail__c, RecordType.Name FROM contact WHERE (RecordType.name='Candidate' or RecordType.Name='Candidate Ad Hoc' or RecordType.Name='Employee' ) and  (Email in :empEmailsSet or AR_Deloitte_Email__c in :empEmailsSet or AR_Personal_Email__c in :empEmailsSet or WCT_ExternalEmail__c in :empEmailsSet)  order by RecordType.Name asc ])
                {
                    /*Adding both the Email & Additional Email into the Map of Email-Contact.*/
                    
                   if(con.RecordType.Name=='Employee')
                   {
                         if(con.Email!='' && con.Email!=null)
                        emailToIdMap_Emp.put(con.Email,con.id);
                         if(con.AR_Deloitte_Email__c!='' && con.AR_Deloitte_Email__c!=null)
                        emailToIdMap_Emp.put(con.AR_Deloitte_Email__c,con.id);
                         if(con.AR_Personal_Email__c!='' && con.AR_Personal_Email__c!=null)
                        emailToIdMap_Emp.put(con.AR_Personal_Email__c,con.id);
                         if(con.WCT_ExternalEmail__c!='' && con.WCT_ExternalEmail__c!=null)
                        emailToIdMap_Emp.put(con.WCT_ExternalEmail__c,con.id);
                   }
                   else if(con.RecordType.Name=='Candidate')
                    {
                         if(con.Email!='' && con.Email!=null)
                        emailToIdMap_Cand.put(con.Email,con.id);
                         if(con.AR_Deloitte_Email__c!='' && con.AR_Deloitte_Email__c!=null)
                        emailToIdMap_Cand.put(con.AR_Deloitte_Email__c,con.id);
                         if(con.AR_Personal_Email__c!='' && con.AR_Personal_Email__c!=null)
                        emailToIdMap_Cand.put(con.AR_Personal_Email__c,con.id);
                         if(con.WCT_ExternalEmail__c!='' && con.WCT_ExternalEmail__c!=null)
                        emailToIdMap_Cand.put(con.WCT_ExternalEmail__c,con.id);
                        
                    }
                    else if(con.RecordType.Name=='Candidate Ad Hoc')
                    {
                        if(con.Email!='' && con.Email!=null)
                        emailToIdMap_Adhoc.put(con.Email,con.id);
                         if(con.AR_Deloitte_Email__c!='' && con.AR_Deloitte_Email__c!=null)
                        emailToIdMap_Adhoc.put(con.AR_Deloitte_Email__c,con.id);
                         if(con.AR_Personal_Email__c!='' && con.AR_Personal_Email__c!=null)
                        emailToIdMap_Adhoc.put(con.AR_Personal_Email__c,con.id);
                         if(con.WCT_ExternalEmail__c!='' && con.WCT_ExternalEmail__c!=null)
                        emailToIdMap_Adhoc.put(con.WCT_ExternalEmail__c,con.id);
                        
                    }
                    
                }
               
            }
            
            
            for(RowWrapper wrapper : allRows)
            {
                WCT_Immigration_Stagging_Table__c stagingRecord = new WCT_Immigration_Stagging_Table__c();
                
                /*Checking if valid Email Id*/
                //System.debug('# Employee Email ');
              //  System.debug('## Selected Contact '+emailToIdMap_Emp.get(wrapper.row.get('FN Email Address')));
                
                /*Look First in Employee List*/
                String CurrentRowEmail=wrapper.row.get('fn email address')!=null ?wrapper.row.get('fn email address').trim().tolowercase():'';
                    if(emailToIdMap_Emp.get(CurrentRowEmail)!=null)
                    {
                        stagingRecord.Contact__c=emailToIdMap_Emp.get(CurrentRowEmail);
                        
                    }
                    else if(emailToIdMap_Cand.get(CurrentRowEmail)!=null)
                    {
                        /*Look Secound  in Candidate List*/
                        stagingRecord.Contact__c=emailToIdMap_Cand.get(CurrentRowEmail);
                    }
                    else if(emailToIdMap_Adhoc.get(CurrentRowEmail)!=null)
                    {
                        /*Look next in Adhoc List*/
                        stagingRecord.Contact__c=emailToIdMap_Adhoc.get(CurrentRowEmail);
                    }
                    else
                    {
                        /*Employee Or Candidate Or Adhoc Contact present. */
                         wrapper.errorMessage=wrapper.errorMessage!=null?wrapper.errorMessage+' \n No Contact found for specified Email Id':'No Contact found for specified Email Id';
                    }
             
               if(validColumns.size()>0)
                {
                    for(WCT_List_Of_Names__c temp : validColumns)
                    {
                        
                        /*To handle the case where the column name is empty*/
                        
                        if(temp.H1B_Column_Name__c!=null && temp.H1B_Column_Name__c!='')
                        {
                            
                            /*Triming Column Value if not null, or else have empty String.
                             * columnValue will always have empty or some value.
                             */
                            String columnValue=wrapper.row.get(temp.H1B_Column_Name__c)!=null?wrapper.row.get(temp.H1B_Column_Name__c).trim():'';
                            
                            /*If Column*/
                            if(temp.H1B_Data_Type_of_the_Field__c=='Date')
                            {
                                try
                                {
                                    if(columnValue!='')
                                    {                                    
                                        Date testDate = date.parse(columnValue);
                                        stagingRecord.put(temp.H1B_Stagging_API_Name__c,testDate);
                                    }
                                    else
                                    {
                                        if(temp.H1B_Is_Required__c==true)
                                        {
                                            wrapper.errorMessage+='\n'+temp.H1B_Column_Name__c+'Field is Blank';
                                        }
                                    }
                                }
                                catch(Exception e)
                                {
                                    wrapper.errorMessage=wrapper.errorMessage+'\n'+temp.H1B_Column_Name__c+' Invalid Date.'+columnValue;
                                   /* ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Invalid date '+temp.H1B_Column_Name__c);
                                    ApexPages.addMessage(errormsg);  
                                   */
                                    System.debug('## Name '+temp.H1B_Column_Name__c+'### H1B_Data_Type_of_the_Field__c '+temp.H1B_Data_Type_of_the_Field__c);
                                }
                            }
                            else
                            {
                                
                                if(columnValue!='')
                                {
                                    System.debug('### temp.H1B_Stagging_API_Name__c '+temp.H1B_Stagging_API_Name__c);
                                    System.debug('### columnValue '+columnValue);
                                  stagingRecord.put(temp.H1B_Stagging_API_Name__c,columnValue);
                                }
                                else
                                {
                                    if(temp.H1B_Is_Required__c==true)
                                        {
                                            wrapper.errorMessage+='\n'+temp.H1B_Column_Name__c+' - Field can not be Blank';
                                        }
                                }
                                
                                /* Setting Stagging record Contact with same email id passed in fragomen file.
    * 
    */
                             /*   if(temp.Staging_API_Name__c=='WCT_Employee_Email__c')
                                {
                                    if(columnValue!='' && emailToIdMap_Emp.get(columnValue)!=null) 
                                    {
                                        stagingRecord.put('Contact__c', emailToIdMap_Emp.get(columnValue));
                                    }
                                }
                 */
                            }
                        }
                        else if(temp.H1B_Static_Value__c != null && temp.H1B_Static_Value__c !='')
                        {
                            /*This block is ato handl ethe case where the columns name is empty and we have a static value cofigured for specific Law firm.*/
                            
                            stagingRecord.put(temp.H1B_Stagging_API_Name__c,temp.H1B_Static_Value__c);
                            
                        }
                            
                        
                        
                        
                        
                    }
                    
                    
                    /*Add the but case type and action fields  */
                    
                    stagingRecord.recordTypeId=VTS_Upload_Staging_RecordTypeId;
                    
                     if(wrapper.errorMessage!=null &&  wrapper.errorMessage!='')
                     {
                         stagingRecord.WCT_Error_Message__c=wrapper.errorMessage;
                         stagingRecord.Status__c='Failed';
                     }
                    else
                    {
                        stagingRecord.Status__c='Not Started';
                    }
                    stagingRecordsList.add(stagingRecord);
                }
            }
        }
        return stagingRecordsList;
    } 
    
    /*
        Validate the first column names in the file name with config configured as per the law firm.
        Return : 
            True : if all valid columns are present.
            False : if any valid columns are not present.
    */
    public boolean validateHeader(String[] header)
    {
        system.debug('### validateHeader started');
        validList= new Set<String>();
        columnsNotPresent= new List<String>();
        /*
        * Update the logic to pull base don law firm passed.
        */
        validColumns= [Select Id,H1B_Column_Name__c,H1B_Data_Type_of_the_Field__c, H1B_Stagging_API_Name__c,H1B_Immigration_API_Name__c, H1B_Is_Required__c,H1B_Static_Value__c,wct_type__c From WCT_List_Of_Names__c where wct_type__c=:law_Firm_Name and RecordType.Name='H1B Immigration Upload Settings'];
        
        System.debug('## validColumns Size'+validColumns.size());
        
        for(WCT_List_Of_Names__c temp : validColumns)
        {
            boolean isPresent=false;
            for(String temp2 : header)
            {
                
                if(temp.H1B_Column_Name__c!=null && temp.H1B_Column_Name__c!='')
                {
                    if(temp2!=null)
                    {
                        //system.debug('### validateHeader started');
                        string columnName=temp2.trim().tolowercase().replace('"','');
                        temp.H1B_Column_Name__c=temp.H1B_Column_Name__c.trim().tolowercase();
                        if(temp.H1B_Column_Name__c==columnName)
                        {
                            isPresent=true;
                            validList.add(columnName);
                        }
                    }
                }
                else
                {
                    /*Ignore if Column name in config is empty. Since the Static  */
                    isPresent=true;
                        
                }
            }
            
            if(isPresent==false)
            {
                
                columnsNotPresent.add(temp.H1B_Column_Name__c);
            }
        }
        return columnsNotPresent.size()==0;
    }
    
    public List<String> lowerCase(List<String> lists)
    {
        List<String> temp= new List<string>();
         for(String header:lists)
            {
                header=header!=null?header.trim().toLowercase():header;
                temp.add(header);
            }
        return temp;
        
    }
    
    
    /*
    *    Loop through the all lines of the files.
    *    
    */
    public List<RowWrapper> generateAllRows(List<List<String>> linesWithHeader)
    {
        List<RowWrapper> rowWrappers=new List<RowWrapper>();
        List<String> headerSeries=lowerCase(linesWithHeader[0]);
        //System.debug('## lower headerSeries '+headerSeries);
        //System.debug(' rowWrappers Started');
        
        
        
        for(integer index=1; index<linesWithHeader.size();index++)
        {
            List<String> CurrentRow=linesWithHeader[index];
            //System.debug('## CurrentRow '+' index '+index+' size '+CurrentRow.size());
           System.debug('## CurrentRow size '+CurrentRow.size()+' index '+index);
            
            RowWrapper rowWapper= new RowWrapper();
            try
            {
                
                /*
                 * Loop though all the Columns of Current Row. 
                 *      Check the Header of same Current column is a valid Header in ValidList.
                 *      If valid
                 *          Add it to the Wrapper. 
                 *      Else 
                 *          Ignore the current Column Value.
                 * 
                 */
                
                for(integer i=0;i<CurrentRow.size();i++)
                {
                    String CurrentRowHeader=headerSeries[i];
                    boolean isValidHeader=validList.contains(CurrentRowHeader);
                    
                    if(isValidHeader==true)
                    {
                        /*
                         * If valid
                         * Add it to the Wrapper. 
                         */
                         if(CurrentRow[i]!='' && CurrentRow[i]!=null)
                        {
                            String columnVal=CurrentRow[i];
                            System.debug('##  CurrentRowHeader '+CurrentRowHeader+' columnVal '+columnVal);
                            columnVal=columnVal.removeStart('"');
                            columnVal=columnVal.removeEnd('"');
                            rowWapper.row.put(CurrentRowHeader,columnVal);
                            if(CurrentRowHeader=='Database Title')
                            {
                                rowWapper.usOrUSI=columnVal;
                            }
                        }
                    }
                    else
                    {
                        System.debug('### Invalid Header'+CurrentRowHeader);
                    }
                }
                
                
                
             /*   
                for(integer i=0;i<validList.size();i++)
                {
                    System.debug('###value '+CurrentRow[i]);
                    
                    String header=headerSeries[i];
                    if(CurrentRow[i]!='' && CurrentRow[i]!=null)
                    {
                        String columnVal=CurrentRow[i];
                        System.debug('## columnVal header '+header+' Value '+columnVal);
                        columnVal=columnVal.removeStart('"');
                        columnVal=columnVal.removeEnd('"');
                        rowWapper.row.put(header,columnVal);
                        if(header=='Database Title')
                        {
                            rowWapper.usOrUSI=columnVal;
                        }
                    }
                */
                
            
            }
            Catch(ListException e)
            {
                rowWapper.isError=true;
                rowWapper.errorMessage='Invalid Data in this Row. Delete it and upload again after correcting.';
            }
            rowWrappers.add(rowWapper);
        }
        
        
        System.debug('#### rowWrappers '+rowWrappers);
        return rowWrappers;
        
    }
    public class RowWrapper
    {
        public Map<String, String> row{get; set;}
        public boolean isError{get; set;}
        public string errorMessage{get; set;}
        public string usOrUSI{get; set;}
        
        public RowWrapper()
        {
            row= new Map<String, string>();
            errorMessage='';
        }
        
    }
    
    /** 
Method Name  : getAllStagingRecords
Return Type  : List<WCT_Immigration_Stagging_Table__c>
Description  : Show all records into VF        
*/
    public List<WCT_Immigration_Stagging_Table__c> getAllStagingRecords()
    {
        if (stagingIds!= NULL)
            if (stagingIds.size() > 0)
        {
            return [Select id,Name, WCT_Initiatior__c,WCT_Emp_Number__c, WCT_Last_Name__c, WCT_First_Name__c, WCT_Employee_Email__c,WCT_Case_type__c, WCT_Entity__c, 
                    WCT_Service_Area__c, WCT_Service_Line__c, WCT_Deloitte_India_Office__c, WCT_Received_Request_for_Evidence__c, 
                    WCT_Request_for_Evidence_Due_Date__c, WCT_Response_to_Requst_for_Evidnce_Filed__c, WCT_Petition_Approved__c, 
                    WCT_Last_Action__c,WCT_Receivd_H_1B_LCA_Amendment_initiatin__c, WCT_Received_Confirmation_of_LCA_Posting__c, 
                    WCT_H_1B_LCA_filed_with_DOL__c, WCT_Copy_of_certified_H_1B_LCA_sent_date__c, Status__c, WCT_Date_Initiation_Received__c, 
                    WCT_Date_B_Package_Sent__c,WCT_CSV_Type__c,WCT_Error_Message__C from WCT_Immigration_Stagging_Table__c where Id In:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_ERROR];
            
        }
        else
            return null;                   
        else
            return null;
    }   
}