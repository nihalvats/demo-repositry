global class WCT_AssignRecordEmailSchedulableBatch implements Database.Batchable<sObject>,Schedulable, Database.Stateful{

    public string strSql{get;set;}
//-------------------------------------------------------------------------------------------------------------------------------------
//    Constructor
//-------------------------------------------------------------------------------------------------------------------------------------
    
    public WCT_AssignRecordEmailSchedulableBatch(){

    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Scheduler Execute 
//-------------------------------------------------------------------------------------------------------------------------------------
    global void execute(SchedulableContext SC) {
        WCT_AssignRecordEmailSchedulableBatch batch = new  WCT_AssignRecordEmailSchedulableBatch();
        ID batchprocessid = Database.executeBatch(batch,200);
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Set the query
//-------------------------------------------------------------------------------------------------------------------------------------
    global Database.QueryLocator start(Database.BatchableContext bc){
    
        string strSql = 'SELECT Id, WCT_Is_Travel_Start_Date_Alert__c ,(SELECT WCT_Initiation_Date__c from Assignments__r'+
                        ' WHERE WCT_Status__c=\'Active\' AND WCT_Initiation_Date__c >= '+ string.valueof(system.today()) +
                        ' AND WCT_Initiation_Date__c <= '+ string.valueof(system.today().addDays(5)) + ' )'+
                        ' FROM WCT_Mobility__c WHERE WCT_Mobility_Employee__r.RecordType.Name = \'Employee\'';
        return database.getQuerylocator(strSql);   
       
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Database execute method to Process Query results for email notification
//-------------------------------------------------------------------------------------------------------------------------------------
    global void execute(Database.batchableContext bc, List<sObject> scope){
    
        List<WCT_Mobility__c> lstMobility = new List<WCT_Mobility__c>();
        for(sObject tmp : scope){
            WCT_Mobility__c tmpMob = new WCT_Mobility__c();
            tmpMob = (WCT_Mobility__c)tmp;
            lstMobility.add(tmpMob);
            if(!(tmpMob.Assignments__r==null) && !(tmpMob.Assignments__r.isempty()))
                tmpMob.WCT_Is_Travel_Start_Date_Alert__c=true;
                 
        }
        Database.SaveResult[] sr = Database.update(lstMobility, false);
    }
    
    global void finish(Database.batchableContext info){     
   
    }     
}