@isTest
public class WCT_ToDToDoItemsManager_test
{
    @isTest public static void m1()
    {
        id idRT = [select Id from RecordType where SobjectType = 'Contact' and DeveloperName = 'WCT_Employee'].Id;
        Contact con1 = new Contact();
        con1.LastName='Testing Outcome Success';
        con1.email='test@test.com';
        con1.WCT_Visa_Type__c='temp';
        con1.MobilePhone='123';
        con1.recordtypeid=idRT;
        con1.WCT_Personnel_Number__c=12345;
        insert con1;

        Task t1 = new Task();
        t1.subject = 'aaaaaaa Success';
        t1.description = 'bbbbb';
        t1.whoId=con1.id;
        t1.ActivityDate=date.today().adddays(5);
        insert t1;

        Task t2 = new Task();
        t2.subject = 'aaaaaaa Error';
        t2.description = 'bbbbb';
        //t2.whoId=con2.id;
        t2.ActivityDate=date.today().adddays(5);
        insert t2;
        
        Test.setMock(HttpCalloutMock.class, new WCT_MockHttpResponseGenerator_Test());
        List<Id> tasklist= new List<Id>();
                        
        Test.starttest();
        // Test Success Handling
        taskList.add(t1.id);
   
        WCT_ToDToDoItemsManager.addToDo(taskList);
        WCT_ToD_Task_ToDo_Relation__c tasktod=new WCT_ToD_Task_ToDo_Relation__c();
        tasktod.SFDC_Task_Id__c=t1.id;
        tasktod.TOD_TODO_Id__c='33517';
        tasktod.Status__c = 'Created';
        tasktod.Status_Details__c = 'errorMessage';
        insert tasktod;       
        
        
        
        List<WCT_ToD_Task_ToDo_Relation__c> listRelationRecords = 
                              [
                                  SELECT
                                      Id, 
                                      SFDC_Task_Id__c, 
                                      ToD_TODO_Id__c, 
                                      Status__c,
                                      WCT_Next_Due_Date__c,
                                      UpdatedDate__c, 
                                      Status_Details__c
                                  FROM
                                      WCT_ToD_Task_ToDo_Relation__c
                                  WHERE
                                      SFDC_Task_Id__c =:t1.id
                                  LIMIT
                                      :taskList.size()
                              ];
    
        
         for(WCT_ToD_Task_ToDo_Relation__c relationRecord : listRelationRecords) {
            relationRecord.Status__c = 'Updated';
            relationRecord.Status_Details__c = '';
            relationRecord.Request__c = 'test request'; 
            relationRecord.Response__c  = 'test response';
            relationRecord.UpdatedDate__c = System.today();  
        }
        update listRelationRecords;   
        
        
            WCT_ToDToDoItemsManager.deleteToDo(taskList);      
        // Test Error Handling
        taskList.clear();
        taskList.add(t2.id);
        WCT_ToDToDoItemsManager.addToDo(taskList);
        WCT_ToDToDoItemsManager.deleteToDo(taskList);   
        WCT_ToDToDoItemsManager.updateToDo(taskList);      
        Test.stoptest();
    }
}