/**
    * Class Name  : WCT_EditAdhocContact_TEST
    * Description : This apex test class will use to test the WCT_EditAdhocContact_Controller
*/
@isTest
private class WCT_EditAdhocContact_TEST{
/** 
        Method Name  : editAdhocCandWithNotes
        Return Type  : void
        Type      : Test Method
        Description  : Test will check Updating Notes on Candidate
**/  
    static testMethod void editAdhocCandWithNotes(){
        Id NonIEFRtypeId = WCT_Util.getRecordTypeIdByLabel('WCT_List_Of_Names__c', 'Non-IEF');
        WCT_List_Of_Names__c eduInst = WCT_UtilTestDataCreation.createListOfNames('School',NonIEFRtypeId,'Education Institution');
        insert eduInst;
        WCT_List_Of_Names__c currEmp = WCT_UtilTestDataCreation.createListOfNames('Employer',NonIEFRtypeId,'Current Employer');
        insert currEmp;
        WCT_List_Of_Names__c otherIns = WCT_UtilTestDataCreation.createListOfNames('Other Employer',NonIEFRtypeId,'Current Employer');
        insert otherIns;
        Contact c = WCT_UtilTestDataCreation.createContactAsCandidate(SYSTEM.Label.AdhocCandidateRecordtypeID);
        insert c;
        PageReference pgref = new PageReference('/apex/WCT_EditAdhocContact_Page');
        Test.setCurrentPage(pgref);
        ApexPages.currentpage().getParameters().put('contactId',c.id);
        ApexPages.currentpage().getParameters().put('isAdhoc','Yes');
        WCT_EditAdhocContact_controller editContact = new WCT_EditAdhocContact_controller();
        editContact.strCurrentEmployerId = '';
        editContact.strSchoolNameId = '';
        editContact.strSchoolName = '';
        editContact.strCurrentEmployer = '';
        editContact.can();
        editContact.errorMsg = 'Invalid school,Invalid CurrEmployer';
        editContact.sub();
        editContact.strEmail = 'test@test.com';
        editContact.strLastName = '';
        editContact.updateAdHocCandidate();
        editContact.strLastName = 'test';
        editContact.strEmail = '';
        editContact.updateAdHocCandidate();
        editContact.strEmail = 'test@test.com';
        editContact.updateAdHocCandidate();
        editContact.strCurrentEmployerId = currEmp.id;
        editContact.strSchoolNameId = eduInst.id;
        editContact.updateAdHocCandidate();
        editContact.strCurrentEmployerId = otherIns.id;
        editContact.strOtherEmployer = '';
        delete c;
        editContact.updateAdHocCandidate();
    }
    
    
    /** 
        Method Name  : editAdhocCand
        Return Type  : void
        Type      : Test Method
        Description  : Test will check Updating Adhoc Candidate
**/  
    static testMethod void editAdhocCand(){
        Id NonIEFRtypeId = WCT_Util.getRecordTypeIdByLabel('WCT_List_Of_Names__c', 'Non-IEF');
        WCT_List_Of_Names__c eduInst = WCT_UtilTestDataCreation.createListOfNames('School',NonIEFRtypeId,'Education Institution');
        insert eduInst;
        WCT_List_Of_Names__c currEmp = WCT_UtilTestDataCreation.createListOfNames('Employer',NonIEFRtypeId,'Current Employer');
        insert currEmp;
        WCT_List_Of_Names__c otherIns = WCT_UtilTestDataCreation.createListOfNames('Other Employer',NonIEFRtypeId,'Current Employer');
        insert otherIns;
        Contact c = WCT_UtilTestDataCreation.createContactAsCandidate(SYSTEM.Label.AdhocCandidateRecordtypeID);
        insert c;
        PageReference pgref = new PageReference('/apex/WCT_EditAdhocContact_Page');
        Test.setCurrentPage(pgref);
        ApexPages.currentpage().getParameters().put('contactId',c.id);
        WCT_EditAdhocContact_controller editContact = new WCT_EditAdhocContact_controller();
        editContact.strCurrentEmployerId = '';
        editContact.strSchoolNameId = '';
        editContact.strSchoolName = '';
        editContact.strCurrentEmployer = '';
        editContact.can();
        editContact.errorMsg = 'Invalid school,Invalid CurrEmployer';
        editContact.sub();
        editContact.strEmail = 'test@test.com';
        editContact.strLastName = '';
        editContact.updateAdHocCandidate();
        editContact.strLastName = 'test';
        editContact.strEmail = '';
        editContact.updateAdHocCandidate();
        editContact.strEmail = 'test@test.com';
        editContact.updateAdHocCandidate();
        editContact.strCurrentEmployerId = currEmp.id;
        editContact.strSchoolNameId = eduInst.id;
        editContact.updateAdHocCandidate();
        editContact.strCurrentEmployerId = otherIns.id;
        editContact.strOtherEmployer = '';
        delete c;
        editContact.updateAdHocCandidate();
    }
}