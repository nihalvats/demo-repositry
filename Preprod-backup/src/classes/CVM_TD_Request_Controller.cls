public class CVM_TD_Request_Controller {

    // Contractor Request form code
    
    //Updated class to check the CI approach.
    public case caseObj{get;set;}
    public string strings{get;set;}
    public string requesttype{get;set;}
    public string vendorcontracttype{get;set;}
    public boolean ismissingReceiptsRequest{get; set;}
    public string LastName {get{
       system.debug('Getting LastName '+LastName ); 
        return LastName;
    } set;}
    public string emValue {get;set;}
    public string redirectparam{get;set;}
    public string fieldsetvar{get;set;}
    public List<Contact> Contractors{get;set;}
    public contact conObj{get;set;}
    public Contact ContractorInstance{get;set;}
    public boolean bCreateRecords{get;set;}
    public integer curEditingIndex{get;set;}
    
    /*Boolean to handle the render and rerender of thr popup, to avoid validation getting triggered in VF page */
    public boolean addOrEditContr{get; set;}
    
    public boolean isEditing{get{
       system.debug('Getting isEditing'+isEditing); 
        return isEditing;
    } set;}
    public boolean displayPopup {get; set;}
    public String amar {get; set;}
    public String HiddenVal {get;set;}
    public CVM_Contractor_Request__c TDRequestInstance{get;set;}
    public CVM_Contractor_Request__c cvmObj{get;set;}

    /* Cont Rec */
    
    //Attachments
    public GBL_Attachments attachmentHelper{get; set;}
    
    public blob jobDescFile{get; set;}
    public String jobDescFileName{get; set;}
    
    
    public List<String> docIdList = new List<string>();
    
    public boolean addAttachment{get; set;}
    public string setpopdisplay{get;set;}
    public List<Document> selectedDocumentList {get;set;}
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    
    wrapper objwrapper;
    public String appId;
    
    List < wrapper > lstwrapper = new List < wrapper > ();
    public boolean newRec;
    public integer rowIndex{get;set;}
    public Integer counter{get;set;}
    public List <wrapper> lst {get;set;}
    public string formname{get;set;}
    
    public integer contractorSize{get{
     
        if(Contractors!=null)
        {
        return Contractors.size();
        }
        return 0;
   
    } set;}
    
    public CVM_TD_Request_Controller()
    {
        emValue = ApexPages.currentPage().getParameters().get('em');
        string userEmail = cryptoHelper.decrypt(emValue);
        conObj=new contact();
        //strings = 'TD_Vendor_Request';
        attachmentHelper= new GBL_Attachments();
        ContractorInstance = new Contact();
        Contractors = new List<Contact>();
        bCreateRecords=false;
        fieldsetvar='Talent_Request';
        isEditing=false;
        addOrEditContr=true;
        formname ='';
        
        try {
            conObj = [select id,name, email from contact where email = : userEmail limit 1];
            if(conObj!=null){
                cvmObj= new CVM_Contractor_Request__c();

                caseObj = new Case();
                caseObj.ContactId = conObj.id;
                cvmObj.CVM_Requested_By__c=conObj.id;
                cvmObj.CVM_Requestor_Name__c = conObj.name;
                cvmObj.CVM_Request_Date__c= date.today();
            }
            
            /* Cont Rec*/
            newRec = false;

            lst = new list < wrapper > ();
            counter = 0;   
             getfields();
        }
        
        catch(Exception e)
        {
            System.debug('----Exception---'+e.getMessage()+'-----at Line #----'+e.getLineNumber());
        }    
    }
    
    
    
    Public void getfields(){
     
        if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('Param1'))){
          
           strings = apexpages.currentpage().getparameters().get('Param1');
           formname=strings ;
           strings = EncodingUtil.urlDecode(strings , 'UTF-8');
          // ocaseformextn.TDR_Request_Type__c = requesttype ;   
           
           if(strings != 'None'){
               strings =strings.replace(' ','_').replace('(' , '').replace(')' , '').replace(',' , '_').replace('-' , '_').replace('/' , '_').replace('.' , '');
           }
           
           if(strings.length() > 41){
               
             strings = strings.substring(0,40);
             system.debug('@@@@@@@@@@@@' + strings  );
             if(strings.endsWith('_')){
              strings = strings.removeEnd('_');
                              
             }
         }
       
       }
       
      }  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public void createContractor(){
    System.debug('createContractor called');
    }
    
    
    public PageReference createContractor1()
    {
    System.debug('createContractor called');
 
 //ContractorInstance.LastName='test';
    Contact temp =ContractorInstance.clone(false, true, false, false);
       temp.LastName=LastName ;
      
     if(isEditing)
     {
         if(curEditingIndex!=null)
         {
              Contractors.set(curEditingIndex, temp);
         }
     }
     else
     {
          Contractors.add(temp);
     }
     clearPopup();
     system.debug('## COntractor Added'+temp+':: whats supplied'+Contractors);
     return null;
    }
    
    public void clearPopup()
    {
        ContractorInstance= new Contact();  
        isEditing=false;
        curEditingIndex=null;
    }
    public void closePopup()
    {
        displayPopup = false;
        amar = HiddenVal;
    }
    
    public void showPopup()
    {
        HiddenVal ='';
        displayPopup = true;
    }
    
    public PageReference manageContractor()
    {
        System.debug('### manageContractor ');
        integer action=Integer.valueof(apexpages.currentpage().getparameters().get('action'));
        integer index=Integer.valueof(apexpages.currentpage().getparameters().get('rowNo'));
        System.debug('### manageContractor '+action+' :: '+index);
        if(action==1)
        {
            Contractors.remove(index);
        }
        else if(action==0)
        {
            ContractorInstance=Contractors.get(index).clone(false,true,false,false);
             LastName= ContractorInstance.LastName;
             system.debug(' ### LAST Name'+LastName);
            curEditingIndex=index;
            isEditing=true;
        }
        System.debug('### manageContractor ContractorInstance '+ContractorInstance);
         System.debug('### manageContractor isEditing '+isEditing+' :: curEditingIndex '+curEditingIndex);
        return null;
    }
    
    public void SetContractorRecord()
    {
        if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('bCreateRecords')))
        {
            string IsCreateContractRecs=apexpages.currentpage().getparameters().get('bCreateRecords');
            if(IsCreateContractRecs=='true')
            {
                bCreateRecords=true;
                AddRow();  
            }
            else{
                bCreateRecords=false;
                lst=new List<wrapper>();
            }
        }
    }
    
    /*
* @desc : Action method to update the record type based on the function string selected.

*/
    public void updateRecordType()
    {
        /*Get the Record Type id from the custom Setting we created. */
        
        string selectedval = apexpages.currentpage().getparameters().get('stringval') ;
        System.debug('352353245' + selectedval );    
        Map<String, RecordType_Function_Mapping__c> recordTypeMappings= RecordType_Function_Mapping__c.getAll();
        cvmObj.RecordTypeId=recordTypeMappings.get(strings).Record_Type_Id__c;
        
    }
    
    public PageReference saveAction() 
    {
        System.debug('#### saveAction');
        try
        {
            
            strings = strings.replace('_',' ');
            
            caseObj.Priority = ' 3- Medium';
            caseObj.status = 'New';
            caseObj.Origin = 'Phone';
            caseObj.WCT_Category__c = 'ELE';
            caseObj.subject = 'Contractor & Vendor Management';
            caseObj.description = 'Contractor & Vendor Management- Contractor Record Creation';
            caseObj.CVM_Request_Type__c = strings;
            
          //  caseObj.RecordTypeid   = System.Label.Case_RecordTypeId;
             
           // Assigning Case using Case-Assignment Rule.
             
             Database.DMLOptions dmlOpts = new Database.DMLOptions();
             dmlOpts.assignmentRuleHeader.assignmentRuleId= Label.Case_Assignment_Rule_Id ;
             dmlOpts.EmailHeader.TriggerUserEmail = true;
             caseObj.setOptions(dmlOpts); 
            
             Database.insert(caseObj, dmlOpts);
             
            
            
            if(caseObj.Id!=null)
            {
                Task objTask=new Task(
                    ActivityDate = Date.today(),
                    Subject='Contractor & Vendor Management',
                    OwnerId ='00540000002DsNU' ,//UserInfo.getUserId()
                    WhatId=caseObj.Id,
                    Status='In Progress');
                
                insert objTask;
            }
            system.debug('----CaseId---'+caseObj);
            cvmObj.CVM_Request_Type1__c= strings;
            cvmObj.CVM_Case__c= caseObj.id;
            insert cvmObj;
            
            
            
           if( cvmObj.CVM_Do_you_need_assistance_in_finding_id__c=='No')
           {
           
            system.debug('---------Contractors---------'+Contractors);
            for(Contact contactemp: Contractors)
                {

                    contactemp.CVM_Request_ID__c = cvmObj.id;
                    contactemp.recordTypeId='0121b0000008TuE';
                }
                if(Contractors.size()>0)
                {
                    insert Contractors;
                }
           }
           else if( cvmObj.CVM_Do_you_need_assistance_in_finding_id__c=='Yes')
           {
                /*Attaching the uploaded document*/
                
                if(jobDescFile!=null && jobDescFileName!=null)
                {
                    Attachment attach = new Attachment();
                    attach.body=jobDescFile;
                    attach.name=jobDescFileName;
                    attach.parentId=cvmObj.id;
                    
                    insert attach;
                }
           
           }
           
            
           
            PageReference objPageRef = new PageReference('/apex/CVM_ThankYou');
            objPageRef.setRedirect(true);
            return objPageRef;  
        }
        catch(Exception e)
        {
            System.debug('----Exception---'+e.getMessage()+'-----at Line #----'+e.getLineNumber());
            return null;
        }
        
    }
    
    public Pagereference gotorequestpage()
    {    
        system.debug('## gotorequestpage');
        PageReference MyRequestsPageRef = Page.CVM_ThankYou;
        MyRequestsPageRef.getParameters().put('em',ApexPages.currentPage().getParameters().get('em'));
        MyRequestsPageRef.setRedirect(true);
        return MyRequestsPageRef;
    }
    
    
    public pagereference delmethod() {
        if(rowIndex!=null){
            try{
                list < wrapper > lstWrapClone = new list < wrapper > ();
                
                lst.remove(rowIndex);
                lstWrapClone=lst;
                lst = new list < wrapper > ();
                integer lp=0;
                for(wrapper objWrap:lstWrapClone){
                    objWrap.rowNo=lp;
                    lst.add(objWrap);
                    lp++;
                }
            }
            catch(Exception ex){
            }
        }
        return null;
    }
    
    
    public PageReference AddRow() {
        
        objwrapper = new wrapper(new Contact());
        counter++;
        objwrapper.counterWrap = counter;
        objwrapper.isEdit = true;
        objwrapper.rowNo = lst.size();
        lst.add(objwrapper);
        newRec = true;
        return null;
    }
    
    public class wrapper {
        public Contact contractor {get;set;}
        public integer rowNo {get;set;}
        public boolean isEdit{get;set;}
        public Integer counterWrap{get;set;}
        public wrapper(Contact contractor) {
            this.contractor = contractor;
            
        }
    }
   
   
   public pagereference redirect()
    {
    PageReference pageRef= new PageReference('/apex/cvm_request_clone');
    pageRef.getParameters().put('em',ApexPages.currentPage().getParameters().get('em'));
    pageRef.setredirect(true);
    return pageRef;
    }
   
   public pagereference NavigateAudit()
    {
    PageReference pageRef= new PageReference('/apex/cvm_request_clone');
    pageRef.getParameters().put('em',ApexPages.currentPage().getParameters().get('em'));
    pageRef.setredirect(true);
    return pageRef;
    }
    
    /* Code for TD Request form */
    public void TDSubmit()
    {
        TDRequestInstance = new CVM_Contractor_Request__c();
        
        insert TDRequestInstance;
    }
    
}