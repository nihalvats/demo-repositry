@istest
public class Test_Custom_Authorization_Send_Email {
   static testmethod void sendemailtest(){
    Custom_Authorization_Send_Email a=new Custom_Authorization_Send_Email();
    a.userName=UserInfo.getUserName();
    a.activeUser= [Select Email From User where Username = : a.userName limit 1];
    a.userEmail = a.activeUser.Email;
    a.call();
    }
}