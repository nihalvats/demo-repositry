@isTest
public class H1B_Client_Location_Trigger_Test 
{
    public static testmethod void test1()
    {
        System.debug('TEST Started');
        Id projectId= H1B_Client_Location_Trigger_Test.dataCreation();
        
        /*Client Location 1 */
        List<Client_Location__c> locs= new List<Client_Location__c>();
        for(integer i=0; i<5;i++)
        {
            Client_Location__c temploc= new Client_Location__c();
            temploc.H1B_Street__c='Steert '+i;
            temploc.H1B_State__c='State '+i;
            temploc.Zip_Code__c='1312';
            temploc.H1B_City__c='AZ';
            temploc.Project_Name__c=projectId;
            if(i==4)
            {
                /*Handle the Empty Project null.*/
                temploc.Project_Name__c=null;
            }
            locs.add(temploc);
        }
        upsert locs;
        
        delete locs;
        
      }
    
    public static Id dataCreation()
    {
        /*Create the Client Project.*/
        Project_H1B__c tempProject= new Project_H1B__c();
        
        insert tempProject;
        return tempProject.Id;
    }
}