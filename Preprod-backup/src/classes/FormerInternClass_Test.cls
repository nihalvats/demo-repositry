@isTest
public class FormerInternClass_Test {
public static testmethod void FormerInternClasstest()
    {
        Event__c evt=new Event__c();
        evt.Name='EventP0304215';
        evt.Start_Date_Time__c=system.Datetime.now();
        evt.End_Date_Time__c=system.Datetime.now()+1;
        Insert evt;

        string temptext='testP342015fi@invalid.com';
        string email=EncodingUtil.urlDecode(CryptoHelper.encrypt(temptext), 'UTF-8');
        PageReference pageRef = Page.FormerInternRSVP;
		Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',string.valueof(evt.id));
        ApexPages.currentPage().getParameters().put('em',email);
        contact cnt=new contact();
        cnt.lastname='prasant';
        cnt.Email=CryptoHelper.decrypt(email);
        cnt.RecordTypeId=label.FormerInternRecordtypeID;
        cnt.WCT_Is_Separated__c=true;
        insert cnt;
        Former_Intern__c frm=new Former_Intern__c();
        frm.Bank_Name__c='ICICI3415';
        frm.Account_Number__c='A123453415';
        frm.Routing_Number__c='12343415';
        frm.Account_Type__c='Savings';
        frm.Deloitte_Event__c=evt.id;
        frm.Former_Intern_Emp__c=cnt.id;
        insert frm;
        FormerInternClass frmIclss=new FormerInternClass();
        //frmIclss.EmailAddress=CryptoHelper.decrypt(email);
        frmIclss.FirstName='Test342015';
        frmIclss.LastName='TestLast342015';
        frmIclss.EmailAddress=temptext;
        frmIclss.Phone='9437488878';
        frmIclss.RSVPStatus='Attending';
        frmIclss.Address1='Address1';
        frmIclss.Address2='Address2';
        frmIclss.City='City';
        frmIclss.State='State';
        frmIclss.Cityna='Cityna';
        frmIclss.Statena='Statena';
        frmIclss.Zipna='Zipna';
        frmIclss.test='test';
        frmIclss.PaymentMode='My pay check should be deposited into the following account:';
        frmIclss.getItems();
        frmIclss.getPaymentMode();
        frmIclss.getItemsRSVP();
        frmIclss.getRSVPStatus();
        frmIclss.getStatelist();
        frmIclss.next();
        frmIclss.back();
        frmIclss.disableEnable();
        frmIclss.getItemsAcptdec();
        frmIclss.getAcptdec();
        frmIclss.submit();
        PageReference pageRef1 = Page.FormerInternRSVP;
		Test.setCurrentPage(pageRef1);
        email=EncodingUtil.urlDecode(CryptoHelper.encrypt('temptex432015@invalid.com'), 'UTF-8');
        ApexPages.currentPage().getParameters().put('id',string.valueof(evt.id));
        ApexPages.currentPage().getParameters().put('em',email);
        
        FormerInternClass frmIclss1=new FormerInternClass();
        frmIclss1.password='temp4712';
        frmIclss1.login();
        //frmIclss.EmailAddress=CryptoHelper.decrypt(email);
        frmIclss1.FirstName='Test342015';
        frmIclss1.LastName='TestLast342015';
       // frmIclss1.EmailAddress='temptex432015@invalid.com';
        frmIclss1.Phone='9437488878';
        frmIclss1.RSVPStatus='Attending';
        frmIclss1.PaymentMode='My pay check should be deposited into the following account:';
        frmIclss1.getItems();
        frmIclss1.getPaymentMode();
        frmIclss1.getItemsRSVP();
        frmIclss1.getRSVPStatus();
        frmIclss1.getStatelist();
        frmIclss1.next();
        frmIclss1.back();
        frmIclss1.disableEnable();
        frmIclss1.getItemsAcptdec();
        frmIclss1.getAcptdec();
        //frmIclss.setPaymentMode();
        frmIclss1.submit();
    }
}