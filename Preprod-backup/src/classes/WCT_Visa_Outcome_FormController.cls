/********************************************************************************************************************************
Apex class         : <WCT_Visa_Outcome_FormController>
Description        : <Controller which allows to Update Task and Mobility>
Type               :  Controller
Test Class         : <WCT_Visa_Outcome_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 24/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/   

public  class WCT_Visa_Outcome_FormController extends SitesTodHeaderController 
{
   
   //PUBLIC VARIABLES
   public WCT_Immigration__c ImmigrationRecord{get;set;}
   public String dropDownValue {get;set;}  
   public GBL_Attachments attachmentHelper{get; set;}
     
   //TASK RELATED VARIABLES
   public task t{get;set;}
   public boolean displayVisaDenial{get;set;}
   public String taskid{get;set;}
    
    
    //ATTACHMENT RELATED VARIABLES
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public List<Attachment> listAttachments {get; set;}
    public Map<Id, String> mapAttachmentSize {get; set;} 
        
       
    //ERROR RELATED VARIABLES
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}


    //DEFINING A CONSTRUCTOR 
    public WCT_Visa_Outcome_FormController()
    {
        
        init();
        attachmentHelper= new GBL_Attachments();   
        getParameterInfo();  

        if(taskid=='' || taskid==null)
        {
           invalidEmployee=true;
           return;
        }

        getTaskInstance();
        getAttachmentInfo();
        getImmigrationDetails();
        getVisaDecisionDropDownValues();
        
    }

/********************************************************************************************
*Method Name         : <init()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Init() Used for loading Immigration,Attachments>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/      
   
     private void init(){

        ImmigrationRecord= new WCT_Immigration__c (); 
        doc = new Document();
        listAttachments = new List<Attachment>();
        mapAttachmentSize = new Map<Id, String>();
     
    }   

/********************************************************************************************
*Method Name         : <getParameterInfo()>
*Return Type         : <Null>
*Param’s             : URL
*Description         : <GetParameterInfo() Used to get URL Params for TaskID>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     
 
   private void getParameterInfo(){
        taskid = ApexPages.currentPage().getParameters().get('taskid');
    }

/********************************************************************************************
*Method Name         : <getTaskInstance()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetTaskInstance() Used for Querying Task Instance>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     
    
    private void getTaskInstance(){
        t=[SELECT Status, OwnerId, WhatId, WCT_Auto_Close__c, WCT_Is_Visible_in_TOD__c  FROM Task WHERE Id =: taskid];
    }

/********************************************************************************************
*Method Name         : <getAttachmentInfo()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetAttachmentInfo() Used for Retrieving Attachment Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/   
 
    @TestVisible
   private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId , 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId = :taskid
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50 ];    
              
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }

/********************************************************************************************
*Method Name         : <getImmigrationDetails()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetMobilityDetails() Used for Fetching ImmigrationDetails>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/    
   
   public pagereference getImmigrationDetails()
   {
       try {
       ImmigrationRecord = [SELECT   Id,
                                     Name,
                                     WCT_Immigration_Status__c ,
                                     WCT_Visa_Type__c ,
                                     ownerid,
                                     WCT_Visa_Interview_Q_A__c,
                                     WCT_Reason_for_Denial__c,
                                     WCT_Visa_Status__c
                                     FROM WCT_Immigration__c 
                                     where id=:t.WhatId ];
          if(ImmigrationRecord.WCT_Immigration_Status__c=='Visa Denied')
          displayVisaDenial = true;
   }
   
        catch (Exception e) {
           
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Visa_Outcome_FormController', 'Visa Outcome Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_VOF_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
            
        }       
     return null;  
   }

/********************************************************************************************
*Method Name         : <insertorupdate()>
*Return Type         : <PageReference>
*Param’s             : 
*Description         : <Insertorupdate() Used for Updating Task and Immigration>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/    
   public pagereference insertorupdate()
   {
       if( (ImmigrationRecord.WCT_Immigration_Status__c==null) ||(ImmigrationRecord.WCT_Visa_Interview_Q_A__c==null )||
           (ImmigrationRecord.WCT_Immigration_Status__c=='') ||(ImmigrationRecord.WCT_Visa_Interview_Q_A__c=='' )||
           (dropDownValue=='None'))  {
                        pageErrorMessage = 'Please fill in all the required fields on the form.';
                        pageError = true;
                        return null;
                    }
         
        if(displayVisaDenial ==true)
        {
             if( (ImmigrationRecord.WCT_Reason_for_Denial__c==null) ||(ImmigrationRecord.WCT_Reason_for_Denial__c=='') )
                   {
                    pageErrorMessage = 'Please fill in all the required fields on the form.';
                    pageError = true;
                    return null;
                    }
        }
       ImmigrationRecord.WCT_Immigration_Status__c = dropDownValue;
       ImmigrationRecord.WCT_Visa_Status__c=ImmigrationRecord.WCT_Immigration_Status__c;
       
       //Changing  Owner Inorder To Avoid Integrity Exception
       try{
        t.OwnerId=UserInfo.getUserId();
        upsert t;
        
        if(attachmentHelper.docIdList.isEmpty()) {
            pageErrorMessage = 'Attachment is required to submit the form.';
            pageError = true;
            return null;

        }
         attachmentHelper.uploadRelatedAttachment(t.id);    
        upsert ImmigrationRecord ;
     
        
        If (t.WCT_Auto_Close__c == true){   
            t.status = 'completed';
        }else{
            t.status = 'Employee Replied';  
        }
        
        if(string.valueOf(ImmigrationRecord.Ownerid).startsWith('00G')){
            t.Ownerid = System.Label.GMI_User; 

        }else{
             t.OwnerId = ImmigrationRecord.Ownerid;
          
        }

        t.WCT_Is_Visible_in_TOD__c=false; 
        upsert t;
        
        
        getAttachmentInfo();
        }
        catch (Exception e) {
           
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Visa_Outcome_FormController', 'Visa Outcome Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_VOF_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
            
        }
        //REDIRECTING TO THANK YOU PAGE PAGE
        PageReference pageRef = new PageReference('/apex/WCT_Visa_Outcome_FormThankYou?taskid='+taskid);
        pageRef.setRedirect(true);
        return pageRef;  
     
   }

/********************************************************************************************
*Method Name         : <getVisaDecisionDropDownValues()>
*Return Type         : <List>
*Param’s             : 
*Description         : <GetVisaDecisionDropDownValues() Used to Populate Dropdown Values>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/    

    public list<SelectOption> getVisaDecisionDropDownValues(){
        list<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('None', 'None'));

        list<Schema.PickListEntry> values = WCT_Immigration__c.WCT_Immigration_Status__c.getDescribe().getPickListValues();

        for(Schema.PicklistEntry v : values){
            if(v.getLabel() == 'Administrative Processing' || v.getLabel() == 'Visa Approved' || v.getLabel() == 'Visa Denied'){
                options.add(new SelectOption(v.getLabel(), v.getValue()));
            }
        }
        return options;
    }
}