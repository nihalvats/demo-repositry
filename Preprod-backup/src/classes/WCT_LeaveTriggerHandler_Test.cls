@isTest
public class WCT_LeaveTriggerHandler_Test {
    
    public static testMethod void ChildLeaveRecordUpdate() {
        Test.starttest();
        Profile prof = [select id from profile where name='system Administrator'];
        
        User usr = new User(alias = 'usr', email='us.name@vmail.com',
                   emailencodingkey='UTF-8', lastname='lstname',
                   timezonesidkey='America/Los_Angeles',
                   languagelocalekey='en_US',
                   localesidkey='en_US', profileid = prof.Id,
                   username='testuser128@testorg.com');
        insert usr;
        
        Account acc = new Account();
        acc.Name='INDIA';
        acc.phone='123456790';
        Insert acc;
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con = WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email = 'test@deloitte.com';
        con.Accountid= acc.id;
        insert con;
        
        recordtype rt1 =[select id from recordtype where DeveloperName = 'LOA_Code'];
        
        WCT_Leave__c leav = new WCT_Leave__c();
         leav.Recordtypeid = rt1.id;
         leav.WCT_Employee__c = con.id;
         //leav.WCT_Related_Record__c = '';
         leav.WCT_Leave_Start_Date__c = system.today();
         leav.WCT_Leave_End_Date__c = system.today()+1;
         leav.WCT_Leave_Category__c = 'Adoption';
         leav.WCT_Leave_Reason__c = 'own serious health condition';
        insert leav;
       
       list<WCT_Leave__c> ls1 = new list <WCT_Leave__c> ();
       WCT_Leave__c leav1 = new WCT_Leave__c();
        leav1.WCT_Leave_Start_Date__c = system.today();
        leav1.WCT_Leave_End_Date__c = system.today()+1;
        leav1.WCT_Leave_Category__c = 'Adoption';
        leav1.WCT_Employee__c = con.id;
        leav1.WCT_Related_Record__c = leav.id;
        leav1.WCT_Leave_Reason__c = '';
       ls1.add(leav1);
       insert ls1;
       
       leav1.WCT_Leave_Category__c  =  leav.WCT_Leave_Category__c;
       leav1.WCT_Leave_Reason__c  =  leav.WCT_Leave_Reason__c;
       update leav1;
       Test.stoptest();
    }
}