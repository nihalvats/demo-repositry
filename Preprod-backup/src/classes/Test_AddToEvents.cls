@isTest
private class Test_AddToEvents{

static testMethod void addCampaignMembers(){
    
    Campaign c = new Campaign(Name = 'Test Campaign');
    insert c;
    
    List<Contact> conList = new List<Contact>();
    List<Lead> ldList = new List<Lead>();
    //List<CampaignMember> cmLst = new List<CampaignMember>();
    
    Contact con = new Contact(LastName = 'Test Campaign Contact');
    Contact con1 = new Contact(LastName = 'Test Contact');
    
    conList.add(con);
    conList.add(con1);
    
    insert conList;
    
    Lead ld=new Lead(LastName='Test Lead',Company='Test Company');
    Lead ld1=new Lead(LastName='Test Lead 1',Company='Test Company');
    ldList.add(ld);
    ldList.add(ld1);
    insert ldList;
   
    //CampaignMember cm = new CampaignMember(CampaignId=c.id,ContactId=conList[0].id);
    //CampaignMember cm1 = new CampaignMember(CampaignId=c.id,LeadId=ld.id);
    //cmLst.add(cm);
//    cmLst.add(cm1);
   // insert cmLst;
    
   // system.assertEquals(cmLst[0].campaignId,c.id);
    
    ApexPages.StandardSetController setCon = new ApexPages.StandardSetController(conList);
    ApexPages.StandardSetController setCon1 = new ApexPages.StandardSetController(ldList);
    
    AddToEvents campn = new AddToEvents(setCon);
        
    setCon.setSelected(conList);
    
    
    AddToEvents camp = new AddToEvents(setCon);
    camp.eventRegObj.Event__c=c.id;
    camp.selectedOption='2';
    camp.selectedMemberStatus = 'Sent';
   pagereference p = camp.AddeventRegistrations();
    
    
    
    camp.eventRegObj.Event__c= null;
    pagereference p1=camp.AddeventRegistrations();
    
    AddToEvents camp1 = new AddToEvents(setCon1);
    setCon1.setSelected(ldList);
    camp1.eventRegObj.Event__c=c.id;
    camp1.selectedOption='2';
    camp1.selectedMemberStatus = 'Sent';
    pagereference p2 = camp1.AddeventRegistrations();
    
 }
}