/**
 * Description : Test class for WCTInterviewExtension_AC
 */
 
@isTest
private class WCTInterviewExtension_AC_test {
    static ApexPages.StandardController sc;        
    static Id candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();

//---------------------------------------------------------
//    Description : Test method for doSearch method
//---------------------------------------------------------
    
    static testMethod void doSearchTest() {
        // TO DO: implement unit test
        Test.startTest();
        
        Contact con = WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        insert con;

        WCT_Candidate_Documents__c  doc = WCT_UtilTestDataCreation.createCandidateDocument(con.id);
        insert doc;
        
        WCT_List_Of_Names__c clickToolForm = WCT_UtilTestDataCreation.createClickToolForm(doc.id);
        insert clickToolForm;
        
        WCT_Interview__c interview = WCT_UtilTestDataCreation.createInterview(clickToolForm.id);
        insert interview;

        Profile p = [Select id from Profile where Name = 'System Administrator'];
        User u = WCT_UtilTestDataCreation.createUser('ua',p.id,'ua@ua.com','ua@ua.com');
        insert u;
        
        WCT_Requisition__c  requisition = WCT_UtilTestDataCreation.createRequisition(u.id);
        insert requisition ;
        
        WCT_Candidate_Requisition__c candRequisition = WCT_UtilTestDataCreation.createCandidateRequisition(con.id,requisition.id);
        insert candRequisition;
        
        WCT_Interview_Junction__c intvTracker = WCT_UtilTestDataCreation.createInterviewTracker(interview.id,candRequisition.id,u.id);
        insert intvTracker;
        
        sc = new ApexPages.StandardController(interview); 
        WCTInterviewExtension_AC  objUpsertInterview = new WCTInterviewExtension_AC (sc) ;
        PageReference pageRef = Page.WCT_Upsert_Interview;
        
        Test.setCurrentPage(pageRef);
        objUpsertInterview.contactName='test';
        objUpsertInterview.contactPhone='1';
        objUpsertInterview.contactEmail='ua';
        objUpsertInterview.reqNum = 'r';
        objUpsertInterview.rmsID = '1234';
        objUpsertInterview.doSearch();
        
        Test.stopTest();                
    }

//---------------------------------------------------------
//    Description : Test method for addCandidate method
//---------------------------------------------------------
    static testMethod void addCandidateTest() {
        // TO DO: implement unit test
        Test.startTest();
        
        Contact con = WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        insert con;

        WCT_Candidate_Documents__c  doc = WCT_UtilTestDataCreation.createCandidateDocument(con.id);
        insert doc;
        
        WCT_List_Of_Names__c clickToolForm = WCT_UtilTestDataCreation.createClickToolForm(doc.id);
        insert clickToolForm;
        
        WCT_Interview__c interview = WCT_UtilTestDataCreation.createInterview(clickToolForm.id);
        interview.WCT_Interview_Status__c = 'Open';

        WCT_Requisition__c  requisition = WCT_UtilTestDataCreation.createRequisition(con.id);
        WCT_Candidate_Requisition__c candRequisition = WCT_UtilTestDataCreation.createCandidateRequisition(con.id,requisition.id);
        candRequisition.WCT_Select_Candidate_For_Interview__c = true;

        sc = new ApexPages.StandardController(interview); 
        WCTInterviewExtension_AC  objUpsertInterview = new WCTInterviewExtension_AC (sc) ;
        objUpsertInterview.ctList = new List<WCT_Candidate_Requisition__c>();
        objUpsertInterview.ctList.add(candRequisition);
        objUpsertInterview.addCandidate();
        Test.stopTest();                    
    }    

//---------------------------------------------------------
//    Description : Test method for removeCandidate method
//---------------------------------------------------------
    static testMethod void removeCandidateTest() {
        // TO DO: implement unit test
        Test.startTest();
        
        Contact con = WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        insert con;

        WCT_Candidate_Documents__c  doc = WCT_UtilTestDataCreation.createCandidateDocument(con.id);
        insert doc;
        
        WCT_List_Of_Names__c clickToolForm = WCT_UtilTestDataCreation.createClickToolForm(doc.id);
        insert clickToolForm;
        
        WCT_Interview__c interview = WCT_UtilTestDataCreation.createInterview(clickToolForm.id);

        WCT_Requisition__c  requisition = WCT_UtilTestDataCreation.createRequisition(con.id);
        WCT_Candidate_Requisition__c candRequisition = WCT_UtilTestDataCreation.createCandidateRequisition(con.id,requisition.id);
        candRequisition.WCT_Select_Candidate_For_Interview__c = true;

        sc = new ApexPages.StandardController(interview); 
        WCTInterviewExtension_AC  objUpsertInterview = new WCTInterviewExtension_AC (sc) ;
        objUpsertInterview.ctSelectedList = new List<WCT_Candidate_Requisition__c>();
        objUpsertInterview.ctSelectedList.add(candRequisition);
        objUpsertInterview.removeCandidate();
        Test.stopTest();                            
    }    

//---------------------------------------------------------
//    Description : Test method for saveInterview method - Insert
//---------------------------------------------------------
    static testMethod void saveInterviewInsertTest() {
        Test.startTest();
        
        Contact con = WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        insert con;

        WCT_Candidate_Documents__c  doc = WCT_UtilTestDataCreation.createCandidateDocument(con.id);
        insert doc;
        
        WCT_List_Of_Names__c clickToolForm = WCT_UtilTestDataCreation.createClickToolForm(doc.id);
        insert clickToolForm;
        
        WCT_Interview__c interview = WCT_UtilTestDataCreation.createInterview(clickToolForm.id);
        interview.WCT_Interview_Status__c = 'Open';

        Profile p = [Select id from Profile where Name = 'System Administrator'];
        User u = WCT_UtilTestDataCreation.createUser('ua',p.id,'ua@ua.com','ua@ua.com');
        insert u;
        
        WCT_Requisition__c  requisition = WCT_UtilTestDataCreation.createRequisition(u.id);
        insert requisition ;
        
        WCT_Candidate_Requisition__c candRequisition = WCT_UtilTestDataCreation.createCandidateRequisition(con.id,requisition.id);
        candRequisition.WCT_Select_Candidate_For_Interview__c = true;
        insert candRequisition;
                
        sc = new ApexPages.StandardController(interview); 
        WCTInterviewExtension_AC  objUpsertInterview = new WCTInterviewExtension_AC (sc) ;
        PageReference pageRef = Page.WCT_Upsert_Interview;
        objUpsertInterview.ctList.add(candRequisition);
        objUpsertInterview.addCandidate();
        objUpsertInterview.ctSelectedList.add(candRequisition);
        objUpsertInterview.saveInterview();
        Event e =  WCT_UtilTestDataCreation.createEvent(interview.id);
        insert e;
        Contact contactRec=new Contact(lastName='LastName1', firstName='FirstName1',Email='candidate1@deloitte.com',RecordTypeId=candidateRecordTypeId,WCT_Taleo_Id__c='123456');
        insert contactRec;
        candRequisition = WCT_UtilTestDataCreation.createCandidateRequisition(contactRec.id,requisition.id);
        
        insert candRequisition;
        objUpsertInterview.ctList.add(candRequisition);
        objUpsertInterview.addCandidate();
        objUpsertInterview.saveInterview();
        objUpsertInterview.ctSelectedList.clear();
        objUpsertInterview.saveInterview();
        //Test.setCurrentPage(pageRef);
        interview.WCT_Case_Type__c='Group Case';
        objUpsertInterview.saveInterview();
        interview=null;
        objUpsertInterview.ctSelectedList.add(candRequisition);
        objUpsertInterview.addCandidate();
        objUpsertInterview.saveInterview();
        objUpsertInterview.removeCandidate();
        Test.stopTest();         
        
    }    
//---------------------------------------------------------
//    Description : Test method for saveInterview method - Edit
//---------------------------------------------------------
    static testMethod void saveInterviewEditTest() {
        Test.startTest();
        
        Contact con = WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        insert con;

        WCT_Candidate_Documents__c  doc = WCT_UtilTestDataCreation.createCandidateDocument(con.id);
        insert doc;
        
        WCT_List_Of_Names__c clickToolForm = WCT_UtilTestDataCreation.createClickToolForm(doc.id);
        insert clickToolForm;
        
        WCT_Interview__c interview = WCT_UtilTestDataCreation.createInterview(clickToolForm.id);
        insert interview;

        Profile p = [Select id from Profile where Name = 'System Administrator'];
        User u = WCT_UtilTestDataCreation.createUser('ua',p.id,'ua@ua.com','ua@ua.com');
        insert u;
        
        WCT_Requisition__c  requisition = WCT_UtilTestDataCreation.createRequisition(u.id);
        insert requisition ;
        
        WCT_Candidate_Requisition__c candRequisition = WCT_UtilTestDataCreation.createCandidateRequisition(con.id,requisition.id);
        insert candRequisition;
                
        sc = new ApexPages.StandardController(interview); 
        WCTInterviewExtension_AC  objUpsertInterview = new WCTInterviewExtension_AC (sc) ;
        PageReference pageRef = Page.WCT_Upsert_Interview;
        objUpsertInterview.ctList.add(candRequisition);
        objUpsertInterview.getCandidateTrackers();
        objUpsertInterview.getSelectedCandidates();
        objUpsertInterview.addCandidate();
        objUpsertInterview.ctSelectedList.add(candRequisition);
        objUpsertInterview.saveInterview();
        Test.setCurrentPage(pageRef);
        Test.stopTest();         
        
    }        
}