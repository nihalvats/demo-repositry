@istest
public class Wct_Workers_Compensation_form_test{
public static testmethod void wc(){
Contact contactRec = WCT_UtilTestDataCreation.createContact();
         contactRec.Email='candidatedeloitte.1001@deloitte.com';
         insert contactRec;
Workers_Compensation__c wc = new Workers_Compensation__c();
wc.Employer_s_Insured_s_Name_Use_Applica__c = 'Deloitte CF';
wc.Street_Address_Assigned_Office_for_Empl__c='candidatedeloitte.1001@deloitte.com';
wc.Suite_Floor_Room_No__c ='candidatedeloitte.1001@deloitte.com';
wc.City_State_Zip_Code__c='candidatedeloitte.1001@deloitte.com';
wc.Location_Code__c='candidatedeloitte.1001@deloitte.com';
wc.US_workers_compensation_coordinator_Nam__c=contactRec.id;
wc.US_workers_compensation_coordinator_Pho__c='candidatedeloitte.1001@deloitte.com';
wc.US_workers_compensation_coordinator_Fax__c='candidatedeloitte.1001@deloitte.com';
wc.Location_of_Human_Resource_Contact_If_D__c=contactRec.id;
wc.Street_Address__c='candidatedeloitte.1001@deloitte.com';
wc.HR_Suite_Floor_Room_No__c ='candidatedeloitte.1001@deloitte.com';
wc.HR_City_State_Zip_Code__c='candidatedeloitte.1001@deloitte.com';
wc.US_workers_compensation_coordinator_Ema__c=contactRec.id;
wc.Status__c='Closed';
wc.Employee_Name__c=contactRec.id;
wc.SAP_ID__c='candidatedeloitte.1001@deloitte.com';

wc.Location__c='candidatedeloitte.1001@deloitte.com';
wc.Date_Notification_Received__c=system.today();
wc.Home_Address__c='candidatedeloitte.1001@deloitte.com';
wc.Home_City_State_Zip_Code__c='candidatedeloitte.1001@deloitte.com';
wc.Home_Phone_Including_Area_Code__c='candidatedeloitte.1001@deloitte.com';

wc.Marital_Status__c='Single';
wc.Number_of_Dependents__c='1';
wc.Did_the_employee_have_prior_accidents_i__c='yes';
wc.Are_there_any_known_medical_conditions_o__c='yes';
wc.Pre_existing_Disabilities__c='yes';
wc.If_yes_Explain__c='candidatedeloitte.1001@deloitte.com';
wc.Employee_Hire_Date__c=system.today();
wc.Employee_Job_Title__c='candidatedeloitte.1001@deloitte.com';
wc.Work_Phone__c='candidatedeloitte.1001@deloitte.com';
wc.Number_of_Hours_Per_Day__c=1;
wc.Name_Days_of_Week_Usually_Worked__c=1;
wc.Employment_Status__c='active';
wc.Employee_Wage_Rate_per_year__c=1;
wc.Amount__c=1;
wc.Per_Year__c=1;
wc.Did_the_employee_receive_full_pay_for_th__c='yes';
wc.Did_or_will_the_employee_s_salary_contin__c='yes';
wc.Date_of_Injury_Illness__c=system.today();
wc.Date_Employer_Notified__c=system.today();
wc.Date_Disability_Began__c=system.today();
wc.Did_injury_illness_occur_on_employer_s_p__c='yes';
wc.Loss_Location_Name_Address_If_Different__c='yes';
wc.Nature_of_Injury_Brief_Description__c='yes';
wc.Part_of_Body_Affected__c='yes';
wc.Source_Cause_of_Injury__c='yes';
wc.Specific_Activity_the_Employee_Was_Engag__c='yes';
wc.How_did_this_accident_injury_occur__c='yes';
wc.Additional_Comments__c='yes';
wc.Initial_Treatment_Describe__c='yes';
wc.Was_Surgery_Required__c='yes';
wc.Has_the_claimant_been_directed_to_a_medi__c='yes';
wc.Physician_s_Name__c='yes';
wc.Address__c='yes';
wc.Treatment_City_State_Zip_Code__c='yes';
wc.Physician_Phone__c='yes';
wc.Hospital_Name__c='yes';
wc.Hospital_Phone__c='yes';
wc.Type_of_Facility__c='yes';
wc.date_Report_Prepared__c=system.today();
wc.Name_of_Person_Preparing_Report__c=contactRec.id;

wc.Report_Address__c='yes';
wc.Report_City_State_Zip_Code__c='yes';
wc.State_of_Hire__c='yes';
wc.Were_there_witnesses__c='yes';
wc.Witness_1_Name__c='yes';
wc.Witness_1_Address__c='yes';
wc.Witness_1_Phone__c='yes';
wc.Witness_2_Name__c='yes';
wc.Witness_2_Address__c='yes';
wc.Witness_2_Phone__c='yes';
wc.Has_the_employee_lost_time_from_work_due__c='yes';
wc.Did_the_injury_occur_in_the_employee_s_d__c ='yes';
wc.Is_the_employee_receiving_any_pay_from_e__c='yes';
wc.Date_Last_Worked__c=system.today();
wc.Has_the_employee_returned_to_work__c='yes';
wc.If_returned_to_work_are_there_any_restr__c='yes';
wc.Date_Returned_to_Work__c=system.today();
wc.FRF_send_to_Chartis_AIG__c=system.today();
wc.Reference_Number__c='yes';
wc.Claim_Number__c='yes';
wc.Adjuster_Name__c='yes';
wc.Adjuster_Contact_details__c='yes';
insert wc;
update wc;
PageReference pageRef = Page.Wct_Workers_Compensation_form;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('wid',string.valueof(wc.id));
        
Wct_Workers_Compensation_form wwcf= new Wct_Workers_Compensation_form();
wwcf.pg();
}
}