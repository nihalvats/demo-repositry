global class WCT_Batch_EmailFieldsUpdated implements Schedulable{
     global void execute(SchedulableContext sc) {
        
        WCT_Batch_EmailFieldsUpdate1 b = new WCT_Batch_EmailFieldsUpdate1(); 
        
        database.executebatch(b,1);
     }
  }