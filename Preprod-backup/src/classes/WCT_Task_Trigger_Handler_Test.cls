@isTest

Private class WCT_Task_Trigger_Handler_Test
{
    public static testmethod void m1()
    {
        
        List<Profile> proflist1 = [Select ID,Name from profile where Name = 'System Administrator'  limit 1];
        User userRec1 = WCT_UtilTestDataCreation.createUser('alias',proflist1[0].id,'userName@acme.com','test@deloitte.com');
        insert userRec1;
        
        Contact conRec1 =  WCT_UtilTestDataCreation.createContact();
        conRec1.OwnerId = userRec1.id; 
        insert conRec1;
        
        Case casRec1 = WCT_UtilTestDataCreation.createCase(conRec1.ID);
        casRec1.OwnerId = userRec1.id; 
        insert casRec1;
        
        Task tempTaskgmi1 = WCT_UtilTestDataCreation.createTask(casRec1.id);
        insert tempTaskgmi1;
        
        WCT_Task_Reference_Table__c taskRefParent1 = new WCT_Task_Reference_Table__c();
        taskRefParent1.WCT_Assigned_to__c = 'Employee';
      //  taskRefParent1.Name = 'IT 000';
        taskRefParent1.WCT_Auto_Create__c  = true;
        insert taskRefParent1;
        
        Id lstRecType1 = WCT_Util.getRecordTypeIdByLabel('Task','Immigration');
        
        Id lstRecType3 = WCT_Util.getRecordTypeIdByLabel('Task','LCA');
        
        WCT_Task_Reference_Table__c taskRef1 = WCT_UtilTestDataCreation.CreateTaskRefTableforTrigger(taskRefParent1.id);
        insert taskRef1;
        test.startTest();
        
        WCT_Immigration__c immigRec = new WCT_Immigration__c();
        immigRec.WCT_Immigration_Status__c = 'New';
        insert immigRec;
        immigRec.WCT_Immigration_Status__c = 'Petition Expired';
        update immigRec;
        WCT_LCA__c lcaRec = new WCT_LCA__c();
        insert lcaRec;        
        List<Task> taskToInsert = new List<Task>();
        Task tempTask1 = WCT_UtilTestDataCreation.createTaskCompleted(immigRec.id);
        tempTask1.WCT_Task_Reference_Table_ID__c = taskRefParent1.id;
        tempTask1.RecordTypeId = lstRecType1;
        tempTask1.OwnerId = casRec1.OwnerID;
        taskToInsert.add(tempTask1);
        //insert tempTask1;
        Task tempTask3 = WCT_UtilTestDataCreation.createTaskCompleted(lcaRec.id);
        tempTask3.WCT_Task_Reference_Table_ID__c = taskRefParent1.id;
        tempTask3.RecordTypeId = lstRecType3;
        tempTask3.OwnerId = casRec1.OwnerID;
        //insert tempTask3;
        taskToInsert.add(tempTask3);
        test.stopTest();
        WCT_Task_Trigger_Handler class1 = new WCT_Task_Trigger_Handler ();
        
        
    }
    
    
    public static testmethod void m2()
    {
    
        List<Profile> proflist2 = [Select ID,Name from profile where Name = 'System Administrator'  limit 1];
        User userRec2 = WCT_UtilTestDataCreation.createUser('alias',proflist2[0].id,'userName@acme.com','test@deloitte.com');
        insert userRec2;
        
        Id lstRecTypeCon2 = WCT_Util.getRecordTypeIdByLabel('Contact','Employee');
        Id lstRecType2 = WCT_Util.getRecordTypeIdByLabel('Task','Mobility');
        Contact conRec2 =  WCT_UtilTestDataCreation.createContact();
        conRec2.RecordTypeId = lstRecTypeCon2;
        conRec2.OwnerId = userRec2.id; 
        insert conRec2;
        
        Case casRec2 = WCT_UtilTestDataCreation.createCase(conRec2.ID);
        casRec2.OwnerId = userRec2.id; 
        insert casRec2;
        
        Task tempTaskgmi2 = WCT_UtilTestDataCreation.createTask(casRec2.id);
        insert tempTaskgmi2;
        test.starttest();
        WCT_Mobility__c  mobRec = WCT_UtilTestDataCreation.createMobility(conRec2.id);
        mobRec.WCT_Assignment_Type__c = 'Short Term';
        mobRec.WCT_Mobility_Status__c = 'New';
        insert mobRec;
        mobRec.WCT_Mobility_Status__c = 'Initiate Onboarding';
        update mobRec;
        
        WCT_Task_Reference_Table__c taskRefParent2 = new WCT_Task_Reference_Table__c();
        taskRefParent2.WCT_Assigned_to__c = 'Employee';
      //  taskRefParent2.Name = 'IT 001';
        taskRefParent2.WCT_Auto_Create__c  = true;
        insert taskRefParent2;
        
        
        
     //   WCT_Task_Reference_Table__c taskRef2 = WCT_UtilTestDataCreation.CreateTaskRefTableforTrigger(taskRefParent2.id);
     //   insert taskRef2;
        
        Task tempTask2 = WCT_UtilTestDataCreation.createTaskCompleted(mobRec.id);
        tempTask2.WCT_Task_Reference_Table_ID__c = taskRefParent2.id;
        tempTask2.RecordTypeId = lstRecType2;
        tempTask2.OwnerId = conRec2.OwnerID;
        insert tempTask2; 

        List<Task> lstTask2 = new List<Task>();
        lstTask2.add(tempTask2);
        Map<id,Task> mapTask2 = new Map<id,Task>();
        mapTask2.put(tempTask2.id,tempTask2);
        WCT_Task_Trigger_Handler.createGMIDependentTasks(mapTask2,lstTask2); 
        test.stoptest();     
    }  
}