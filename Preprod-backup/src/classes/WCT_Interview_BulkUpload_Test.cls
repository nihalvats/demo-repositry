@isTest
public class WCT_Interview_BulkUpload_Test {

    static testMethod void TestFileUploader2 (){
        test.startTest();
        
        Contact Cont = new Contact(FirstName='rakuten12345',LastName='sri', Email= 'abc@gmail.com');
        insert Cont;
        
        WCT_List_Of_Names__c LON = new WCT_List_Of_Names__c();
        LON.Name = 'NEW-India_Experienced_Consulting_SAP MM-WM-SRM IEF';
        LON.WCT_Type__c = 'IEF';
        LON.WCT_Job_Type__c = 'India-Experienced';
        LON.WCT_User_Group__c = 'India';
        LON.WCT_FSS__c = 'Consulting';
        insert LON;
        
        WCT_Requisition__c req = new WCT_Requisition__c();
        req.Name = 'Test REQ#01';
        insert req;
        
        WCT_Candidate_Requisition__c ct = new WCT_Candidate_Requisition__c();
        ct.WCT_Contact__c = Cont.id;
        ct.WCT_Requisition__c = req.id;
        insert ct;                         
        
        List<WCT_Candidate_Requisition__c> cr = [select id, name from WCT_Candidate_Requisition__c where id=:ct.id];
        DateTime xEventDate  = DateTime.now();
        xEventDate  = xEventDate.addDays(5);
        //string csvcontent ='Interview Form Name,Candidate Tracker ID,Event Start Date,Event End Date,Interview Subject'+ '\r\n' +  'NEW-India_Experienced_Consulting_SAP MM-WM-SRM IEF,'+cr[0].Name+',3/27/2015 9:41 PM,3/27/2015 9:41 PM, Test event 1';
        string csvcontent ='Interview Form Name,Candidate Tracker ID,Event Start Date,Event End Date,Interview Subject'+ '\r\n' +  'NEW-India_Experienced_Consulting_SAP MM-WM-SRM IEF,'+cr[0].Name+','+ xEventDate +',' + xEventDate +', Test event 1'+',abc1062015@deloitte.com';
        

        WCT_Interview_BulkUpload intBulkupload = new WCT_Interview_BulkUpload();
        intBulkupload.candreq.WCT_Time_to_Schedule_First_Interview__c=system.now()-1;
        intBulkupload.candreq.WCT_Time_till_start_of_first_Interview__c=system.now()+1;
        intBulkupload.contentFile = Blob.valueof(csvContent);
        intBulkupload.ReadFile();
        intBulkupload.getInterviewRecords();
        intBulkupload.getErrorRecords();
        intBulkupload.getAllInterviewCreated();
        intBulkupload.getAllStagingRecords();
        
        intBulkupload.exportrngInt=true;
        intBulkupload.exportrngerr=true;
        intBulkupload.getAllInterviewCreated();
        intBulkupload.getAllStagingRecords();
        intBulkupload.getInterviewRecordsRange();
        intBulkupload.getErrorRecordsRange();
        test.stopTest(); 
    }
}