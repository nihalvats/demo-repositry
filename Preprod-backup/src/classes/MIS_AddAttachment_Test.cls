@istest

public class MIS_AddAttachment_Test {
    
    
    static testmethod void trtAddAttachment(){
        
        case cs = new Case();
        cs.Status='New';
        cs.Origin='web';
        insert cs;
        Case_form_Extn__c cfsid = new Case_form_Extn__c();
        list<Case_form_Extn__c> c = new list<Case_form_Extn__c>();
        cfsid.GEN_Case__c=cs.id;
        insert cfsid;
        ApexPages.StandardController sc = new ApexPages.StandardController(cfsid);
        MIS_Add_SOP misattach =new MIS_Add_SOP(sc);
        
        ApexPages.CurrentPage().getParameters().put('id',cfsid.id);
        misattach.Cancel();
        misattach.getmyfile();
        misattach.cfsid = cfsid.id;
        Blob bodyBlobs=Blob.valueOf('Unit Test Attachment Body');
        misattach.myfile.name='test';
        misattach.myfile.body=bodyBlobs;
        misattach.Savedoc(); 
        
    }
}