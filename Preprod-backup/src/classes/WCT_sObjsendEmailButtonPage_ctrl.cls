global with sharing class WCT_sObjsendEmailButtonPage_ctrl {

 
// *********************************************************************************************
// *************** variable declariation********************************************************
    public list<Document> cTempDocToinsert= new list<Document>();
    
    global Document cDoc{get;set;}
    public String ToFieldEmail { get; set; }
    public list<string> ShowAttachmentlist{get;set;} 
    public String RelatedToFieldId { get; set; }
    public String RelatedToField { get; set; }
    public boolean IsAttachmentRetrive= false;
    /*public list < SelectOption > RelatedToSelectOptionValues { 
    get
    {
     list<SelectOption> tempOptions= new list<SelectOption>();
     tempOptions.add(new selectOption('case','Case'));
     return  tempOptions;       
    } 
    set; }*/
    
    public list <SelectOption> RelatedToSelectOptionValues {get;set;}

    public String SelectedRetaedTovalue { get; set; }
    public list<string> ccid   = new list<String>(); 
    public list<string> bccId = new list<String>();  
    public list<String>    toEmailAddress= new list<string>();     
    User usr = [Select Id, Name, Profile.Name from User where Id = :UserInfo.getUserId()];  
   list<string> DocIds=new list<string>();
   public string templateId='';
   public String sEmailTemplateID {get;set;}
   public Boolean ShowselectTemplatepopup{get;set;}
   public Boolean ShowAttchPopUp{get;set;}
   public String fileName {get; set;}
   public Blob attachedFile {get; set;} 
   public List<EmailTemplate> Templatelist { get; set; }
   public list<selectOption> Folders { 
     get
    {
             list<selectOption> option= new list<selectoption>();
             option.add(new selectOption('','--Select--'));
             option.add(new SelectOption(UserInfo.getOrganizationId(), 'Unfiled Public Email Templates'));
                    for(folder f:[SELECT Id,Name,Type FROM Folder where Type='Email'])
                    {
                            if(f.name !='CIC Reports')
                            option.add(new selectOption(f.id,f.name));
                    }
             return option;
    }
     set; 
    }
    public String folderid { get; set; }
    public String BodyHiddenContentvalue { get; set; }
    public String BodyContent { get; set; }
    public String subjectLine { get; set; }
    public String BCcIdFieldValue { get; set; }
    public String BCcFieldValue { get; set; }
    public String CcIdFieldValue { get; set; }
    public String CcFieldValue { get; set; }
    public String AdditionalToFieldValue { get; set; }
    public String ShowAdditionalFieldValue { get; set; }
    public String ToFieldId { get; set; }
    public String ToField { get; set; }
    public string SelectedFromId{get;set;}
    public String caseid;
    public EmailTemplate emailTemp {get;set;}
    public string caseThreadId {get;set;}
    public Set<String> selTempdocIdList;
    
    public boolean clearToField {get;set;}
    public EmailMessage em {get;set;}
    public void clearToField(){
        ToFieldId = '';
        ToField = '';
        ToFieldEmail = '';
        clearToField = false;
    }
    //variables related to Leaves
    public list<String> relLeaveIds {get;set;} 
    public list<LeaveTaskWrapper> taskFromLeave {get;set;}
    
    public boolean emailBodyfldHTMLRender {get;set;}
    
    public string currentObject {get;set;}
    public List<String> AccessibleFields = new List<String>();
    private static Map<String, Schema.sObjectType> sObjectByKeyPrefix {
        get {
            if (sObjectByKeyPrefix == null) {
                sObjectByKeyPrefix = new Map<String, Schema.sObjectType>();
                for (Schema.SObjectType sObj : Schema.getGlobalDescribe().values()) {
                    sObjectByKeyPrefix.put(sObj.getDescribe().getKeyPrefix(), sObj);
                }
            }
            return sObjectByKeyPrefix;
        }
        
        private set;
    } 
    
    public list < SelectOption > fromAddressValues{
        get {
            list < SelectOption > tempOption = new list < SelectOption > ();
                     
            
            list<OrgWideEmailAddress> owea = [SELECT Address,DisplayName,Id FROM OrgWideEmailAddress ];
           
            if((usr.Profile.Name == Label.CIC_agent_Profile)||(usr.Profile.Name == Label.CIC_Manager_Profile))
             {
               tempOption.add(new selectOption(Label.TalentCICInbox,Label.TalentCICInbox));
            }
            else
            {
                tempOption.add(new selectOption(UserInfo.getUserEmail(),UserInfo.getUserEmail()));
            }
            for(OrgWideEmailAddress eslist :owea) {
                  tempOption.add(new selectOption(eslist.Address,eslist.DisplayName+'<'+eslist.Address+'>'));
               }

            return tempOption;
        }
        set;
    }
    
    //EmailMessage related variables
    public boolean ShowLVEpopup{get;set;}
    
 //****************************************************************************************************************
 //********************** send email method to send email**********************************************************   
    public PageReference SendEmail() {
    
         String s= ToFieldId;
         boolean edited = false; 
         Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
         toEmailAddress= new list<string>();
         list<String> AdditionalTo= new list<String>();
         List<Contact> conList = new List<Contact>();
         //message.setOrgWideEmailAddressId(SelectedFromId);
         
         //**********************  Setting TO Address . ************************************
         set<String> TempToEmails= new  set<string>();
         if(ShowAdditionalFieldValue.length()>0)
         {
         AdditionalTo= ShowAdditionalFieldValue.split(',');
          if(AdditionalTo.size()>0){
              TempToEmails.addAll(AdditionalTo);
             }
             toEmailAddress.AddAll(TempToEmails);
             //message.setToAddresses(toEmailAddress);
         }
         
   //**********************  Adding custom error message if To field is blank. ************************************   
         if(ToField =='' || ToField == null)
         { 
          ToFieldId='';
         }   
         
         if(ToFieldId=='' && AdditionalTo.isEmpty()){
             ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Select at least one contact from either of To OR Additional to Lookup');
              ApexPages.addMessage(myMsg); 
              return null;
         
         }
    
        
    //**********************  Setting BCC Address . ************************************
         
         if(BCcFieldValue.length()>0)
             {
                list<String> BCCField=BCcFieldValue.split(',');
                if(BCCField.size()>0){  
                        message.setBccAddresses(BCCField);
                  }
              }
   //**********************  Setting CC Address . ************************************
               
         if(CcFieldValue.length()>0)
             {
               list<String> CCField=CcFieldValue.split(',');
               if(CCField.size()>0){
                       message.setCcAddresses(CCField);
                   }
               }
   
     
   //**********************  Adding Custom message if there is no subject with the email. ******************
         
         if((templateId.length()==0)&&(subjectLine.length()==0))
         {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendEmail_subject_fieldErrorMessage);
              ApexPages.addMessage(myMsg); 
              return null;
         }
   
   //***************** Setting From Address ************************
   
        if(!fromAddressValues.isEmpty()){
            //message.setSenderDisplayName(SelectedFromId);
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: SelectedFromId];
            if ( owea.size() > 0 ) {
                message.setOrgWideEmailAddressId(owea.get(0).Id);
            }
        }
        
   
  //**********************  Setting subject, body if user don't select a template. ************************************
           //message.setWhatId(RelatedToFieldId);
           if(ToFieldId!=null && ToFieldId!=''){
           message.setTargetObjectId(ToFieldId);
           message.setWhatId(RelatedToFieldId);  
            } else{
               // message.setTargetObjectId(userInfo.getuserId());
                //message.setSaveAsActivity(false);
            }
           if(currentObject == 'Case'){
           subjectLine = subjectLine+' '+caseThreadId;
           }
           message.setSubject(subjectLine);
           if(currentObject == 'Case'){
           BodyContent = BodyContent +'\n\n'+ caseThreadId;
           }
           if(!emailBodyfldHTMLRender){
            message.setPlainTextBody(BodyContent);
           }else{
            message.setHtmlBody(BodyContent);
           }
             if(BodyContent.length()==0)
             {
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendEmail_body_fieldErrorMessage);
                  ApexPages.addMessage(myMsg); 
                  return null;
              }
           //}
         
 //**********************  adding attachments to the email ************************************
        for(AttachmentList ListSelectedAttachment:ListAttachment )
        {
        if(ListSelectedAttachment.checkbox==true)
             {
                DocIds.add(ListSelectedAttachment.AttachmentId);
             }
        }
        if(DocIds.size()>0)
        {
            
           message.setDocumentAttachments(DocIds);
        }
     try{
     
 //**********************  Sending email ************************************
        message.setToAddresses(toEmailAddress);
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {message});
                if(tofieldId==''){
                system.debug('entered1');
                task t = new task();
                t.subject = subjectLine;
                t.OwnerId = userinfo.getuserId();
                t.status = 'Completed';
                t.WhatId = RelatedToFieldId;
                t.ActivityDate = system.today();
                //t.recordtypeid = '01240000000DuEb';
                //insert t;
                system.debug('entered2'+t);
               
               
                if(message.getHtmlbody()!=null){
                string html=message.getHtmlbody();
                string result = html.replaceAll('<br/>', '\n');
                result = result.replaceAll('<br />', '\n');
                result = result.replaceAll('&nbsp;', '');
                result = result.replaceAll('&gt;', '');
                result = result.replaceAll('&lt;', '');
                //string HTML_TAG_PATTERN = '<.*?>';
                string HTML_TAG_PATTERN = '<[^>]+>|&nbsp;';
                pattern myPattern = pattern.compile(HTML_TAG_PATTERN);
                matcher myMatcher = myPattern.matcher(result);
                result = myMatcher.replaceAll('');
                t.description = result;
                }
                if(message.getPlainTextbody() != null){
                t.description = message.getPlainTextbody();}
                
                insert t;
            if(message.getDocumentAttachments()!=null){
                 
                List<Attachment> attOnEmail = new list<Attachment>();
                list<Document> DocToAtt=[select  id,name,ContentType,Type,Body FROM Document where id IN :message.getDocumentAttachments()];
                for(Document doc : DocToAtt){
                    Attachment newAtt = new Attachment();
                    newAtt.body = doc.body;
                    newAtt.ContentType = doc.ContentType;
                    newAtt.Name= doc.Name;
                    newAtt.parentid = t.id;
                    attOnEmail.add(newAtt);
                }
                if(!attOnEmail.isEmpty()){
                     system.debug('entered5'+attOnEmail.size());
                    insert attOnEmail;
                    system.debug('entered5'+attOnEmail);
                    //em.HasAttachment = true;
                    //update em;
                }
            }
           
          }
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, r+'>Message Id>'+message));   
     
         }catch(Exception e )
         {
          if(e.getMessage().contains('INVALID_EMAIL_ADDRESS'))
          {
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendEmail_Invalid_Email_Id);
                  ApexPages.addMessage(myMsg); 
                  return null;
          }else{
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
           return null;
           }           
         }  
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Support Request submitted successfully.')); 
        list<Document> DocToDelete=[select id from Document where id In : DocIds];
        delete DocToDelete;  
        pagereference PageRef = new pagereference('/'+caseid);
        PageRef.setRedirect(true);
        return PageRef;
    
    }
//**********************  Cancel method  ************************************
  

    public PageReference cancel() {
        pagereference PageRef = new pagereference('/'+caseid);
        PageRef.setRedirect(true);
        return PageRef;
    }

//**********************  Method to close Attachment Popup  ************************************
 
    public PageReference CloseAttachmentPopup() {
        ShowAttchPopUp=false;
        return null;
    }
     public PageReference CloseTempPopup() {
        ShowselectTemplatepopup=false;
        return null;
    }
    public PageReference CloseLVEpopup() {
        ShowLVEpopup=false;
        return null;
    }
//**********************  Method to open Attachment Popup  ************************************


    public PageReference OpenAttachmentPopup() {
        ShowAttchPopUp=true;
        return null;
    }
//**********************  Method to attach a file/ createing document  ************************************

    public PageReference save() {
      
    if(cDoc.Name!=null){
        cDoc.FolderId=Label.Attachment_Zip_Document_Folder_Id;
        insert cDoc;
        ShowAttachmentlist.add(cDoc.Name);
       // DocIds.add(cDoc.id);
        ListAttachment.add(new AttachmentList(true,cDoc.Name,cDoc.id));
        cDoc= new Document();
      }
    return null;
    }


//**********************  Method to attach a file/ createing document  ************************************
  public PageReference OpenSelectTemplatePopup() {
        ShowselectTemplatepopup=true;
        return null;
    }
    
  public PageReference OpenLVEPopup() {
        ShowLVEpopup=true;
        return null;
    }
 //**********************  Setting selected template on VF page  ************************************
  
   public PageReference SelectTemplate() {
            
            emailTemp = new EmailTemplate();
         for(EmailTemplate temp : [SELECT Id,Name,Body,subject,FolderId,HtmlValue,Encoding,TemplateType FROM EmailTemplate where id=: sEmailTemplateID ]){
              
               //Set<String> selTempdocIdList;
               system.debug('entered set'+selTempdocIdList);
               if(selTempdocIdList!=null && !selTempdocIdList.isEmpty()){
               system.debug('entered '+ListAttachment.size());
               if(ListAttachment.size()>0){
               system.debug('entered one');
               
               list<AttachmentList> TempAttList = new list<AttachmentList>();
                for(integer i=0;i<ListAttachment.size();i++){
                    system.debug('entered 2'+ListAttachment.size());
                    if(!selTempdocIdList.contains(ListAttachment[i].AttachmentId)){
                        TempAttList.add(ListAttachment[i]);
                        //ListAttachment.remove(i);
                        system.debug('entered 3'+ListAttachment.size());
                    }
                }
                ListAttachment.clear();
                if(!TempAttList.isEmpty()){
                    //ListAttachment.clear();
                    ListAttachment.addAll(TempAttList);
                }
                
               }
               selTempdocIdList.clear();
               }
               selTempdocIdList = new Set<String>();
               for(Attachment temAtt : [select id,body,ContentType,Name,parentid from Attachment where parentid =:temp.id]){
                //selTempdocIdList = new Set<String>();
                Document doc= new Document();
                doc.body = temAtt.body;
                doc.ContentType = temAtt.ContentType;
                doc.Name= temAtt.Name;
                doc.FolderId=Label.Attachment_Zip_Document_Folder_Id;
                insert doc;
                selTempdocIdList.add(doc.id);
                ListAttachment.add(new AttachmentList(true, doc.Name,doc.id));
                doc= new Document();               
               }
               Messaging.SingleEmailMessage tempMessage = new Messaging.SingleEmailMessage();
               tempMessage.setTemplateId(temp.id);
               //tempMessage.setTargetObjectId('003f000000DcI49'); 
               //tempMessage.setTargetObjectId('003f000000E15tM');  
               tempMessage.setWhatId(RelatedToFieldId);
               Savepoint sp = Database.setSavepoint();
               if(ToFieldId==''){
               contact c = new contact(recordtypeid='01240000000DuEW',lastname='.');
               c.email='test@gmail.com';
               //Savepoint sp = Database.setSavepoint();
               insert c;
               tempMessage.setTargetObjectId(c.id);
               }else{
               //templateid = ToFieldId.substring(0,15);
               system.debug('tofield'+ToFieldId.substring(0,15)+'&&'+ToFieldId);
               tempMessage.setTargetObjectId(ToFieldId.substring(0,15));}
               Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {tempMessage});
               
               //body = tempMessage.gethtmlbody();
               //subject=tempMessage.subject;
               //database.rollback(sp);
               
               
               if(temp.TemplateType=='HTML'){ BodyContent=tempMessage.gethtmlbody();
                    emailBodyfldHTMLRender = true;
                //BodyContent=BodyContent.replaceAll(']]>','\n\n\n');
                //string tempBodyContent=EncodingUtil.urlEncode(temp.HtmlValue, temp.Encoding);
                //BodyContent='<html>'+EncodingUtil.urlDecode(tempBodyContent, temp.Encoding)+'</html>';
                subjectLine=tempMessage.subject;
                //templateId=temp.Id;
                //emailTemp.HtmlValue = temp.HtmlValue;
                //emailTemp.body = temp.body;
                }else if(temp.TemplateType=='Text')
                {
                BodyContent=tempMessage.getPlaintextBody();
                subjectLine=tempMessage.subject;
                emailBodyfldHTMLRender = false;
                //templateId=temp.Id;
                //emailTemp.body = temp.body;
                }else if(temp.TemplateType=='VISUALFORCE'){ 
                    if(tempMessage.gethtmlbody() <> null){
                        BodyContent=tempMessage.gethtmlbody();
                        emailBodyfldHTMLRender = true;
                    }else if(tempMessage.getPlaintextBody() <> null){
                        BodyContent=tempMessage.getPlaintextBody();
                        emailBodyfldHTMLRender = false;
                    }
                    subjectLine=tempMessage.subject;
                }
                 database.rollback(sp);          
                }
                ShowselectTemplatepopup=false;
                return null;
    }


//********************** Searching templates  ************************************
 

    public void Searchtemplfiles() {
        Templatelist = new list<EmailTemplate>();
                if(folderid!= null && folderid!='')
                    {
                       for(EmailTemplate et:[ SELECT Id,Name,Body,FolderId,Description,HtmlValue,TemplateType FROM EmailTemplate where FolderId=:folderid AND IsActive = true])
                       {
                         Templatelist.add(et); 
                        } 
                
                    }
          // return null;
        }


  
public list<AttachmentList> ListAttachment {get; set;}

 //********************** Default constructor  ************************************
   
 public WCT_sObjsendEmailButtonPage_ctrl()
 { 
    BCcFieldValue='';
    subjectLine='';
    CcFieldValue='';
    ShowAdditionalFieldValue='';
    ToFieldId='';
    ToField='';
    ToFieldEmail='';
    templateId='';
    cDoc= new Document();
    emailBodyfldHTMLRender = true;
    ShowselectTemplatepopup= false;
    ShowAttchPopUp=false;
    ShowLVEpopup=false;
    ShowAttachmentlist= new list<String>();
    ListAttachment = new List<AttachmentList>();
    RelatedToSelectOptionValues = new List<SelectOption>();
    caseid=ApexPages.currentPage().getParameters().get('id') ; 
    string currKeyPrefix = ApexPages.currentpage().getParameters().get('id').subString(0,3);
    Schema.sObjectType currsObjectType = sObjectByKeyPrefix.get(currKeyPrefix);
    RelatedToSelectOptionValues.add(new selectOption(currsObjectType.getDescribe().getName(),currsObjectType.getDescribe().getLabel()));
       
    currentObject =  currsObjectType.getDescribe().getLabel();
    //Dynamic reference of all fields
        Map<String, Schema.SobjectField> fields =  currsObjectType.getDescribe().fields.getMap();
        for (String s : fields.keySet()) {
            if ((s != 'id') && (fields.get(s).getDescribe().isAccessible())) {
                AccessibleFields.add(s);
                
            }
        }
    
     String Query = 'SELECT ' + joinList(AccessibleFields, ', ') + 
                          ' FROM ' + currsObjectType.getDescribe().getName() + 
                          ' WHERE Id = :caseid';
    sObject sobj = database.query(Query);
    RelatedToField = (string)sobj.get('name');
    
    if(currentObject == 'Leave'){
        relLeaveIds = new list<string>();
        taskFromLeave = new list<LeaveTaskWrapper>();
        if((string)sobj.get('WCT_Employee__c') <> null){
            ToField= getContactName((string)sobj.get('WCT_Employee__c'), 'Name');
            ToFieldId=(string)sobj.get('WCT_Employee__c');
            ToFieldEmail= getContactName((string)sobj.get('WCT_Employee__c'), 'Email');
        }  
        relLeaveIds.add(caseid);
        for(WCT_Leave__c lve:[SELECT id FROM WCT_Leave__c WHERE WCT_Related_Record__c = :caseid]){
            relLeaveIds.add(lve.id);        
        }
        for(Task ta:[select id,Subject,ActivityDate,Owner.name,What.name,Description,status,priority from task 
                        where whatId IN :relLeaveIds]){
            taskFromLeave.add(new LeaveTaskWrapper(false,ta.subject,ta.status,ta.id,string.valueOf(ta.ActivityDate)));
        }
    }
    /*
    SelectedRetaedTovalue='case';
    RelatedToField=cCurrentCase[0].CaseNumber;
    */
    RelatedToFieldId=caseid;
    folderid = UserInfo.getOrganizationId();
    Searchtemplfiles();
 }
 
 private static String joinList(List<String> theList, String separator) {

        if (theList == null)   { return null; }
        if (separator == null) { separator = ''; }

        String joined = '';
        Boolean firstItem = true;
        for (String item : theList) {
            if(null != item) {
                if(firstItem){ firstItem = false; }
                else { joined += separator; }
                joined += item;
            }
        }
        return joined;
    }
  
 //********************** Method to get the contact name  ************************************
 
 public string getContactName(String contactId, String RetParameter)
     {
      list<Contact> contactTowork=[select name, id,Email from contact where id =:contactId];
      if(RetParameter=='Name'){
          return contactTowork[0].Name;
      }else
      {
       return contactTowork[0].Email;
      }
     }
     
     
     //Convert List of strings to string
     public string conListOfStr(list<string> strList){
        string retString = '';
        for(string s : strList){
            retString += s + ';'+'\n';
        }   
        return retString;
     }
     
     public class AttachmentList
     {
        public boolean checkbox{get;set;}
        public String  AttachmentName{get;set;}
        public String  AttachmentId{get;set;}
        
        public AttachmentList(Boolean isSelected, String Name,String id)
        {
             checkbox= isSelected;
             AttachmentName=name;
             AttachmentId= id;
             
        }
        
     } 
     
     //getting attachments from current object
     public void AttachementListFormCurrObj()
     {
         if(!IsAttachmentRetrive){
         List<String> parentIdsForAtt = new List<String>();
         parentIdsForAtt.add(caseid);
         //getting child leave Ids for getting attachments
         if(currentObject == 'Leave'){
            for(WCT_Leave__c lve:[SELECT id FROM WCT_Leave__c WHERE WCT_Related_Record__c = :caseid]){
                parentIdsForAtt.add(lve.Id);
            }
         }
          
          for(Attachment at : [select id, Name, Body  from Attachment where Attachment.ParentId IN :parentIdsForAtt]){
           //  ShowAttachmentlist.add(at.Name);
           if(at.body!=null){
             Document doc= new Document();
             doc.Name=at.Name;
             doc.body=at.body;
             doc.FolderId=Label.Attachment_Zip_Document_Folder_Id;
             cTempDocToinsert.add(doc);
             }
          }
          for(Note note:[SELECT id,Title,body from NOTE where ParentId IN :parentIdsForAtt]){
            if(note.body <> null){
             Document doc= new Document();
             doc.Name=note.title;
             doc.body=Blob.valueof(note.body);
             doc.FolderId=Label.Attachment_Zip_Document_Folder_Id;
             doc.ContentType='text/plain';
             doc.type = 'txt';
             cTempDocToinsert.add(doc);
            }
          }          
          
          insert cTempDocToInsert;
          for(Document d: cTempDocToInsert)
          {
                //DocIds.add(d.id);
             ListAttachment.add(new AttachmentList(false, d.Name,d.id));
           
          }
         cTempDocToInsert.clear();
         IsAttachmentRetrive=true;
         }
         
     }
     
     public class LeaveTaskWrapper{
        public boolean isSelected {get;set;}
        public string subject {get;set;}
        public string status {get;set;}
        public string tskId {get;set;}
        public string duedate {get;set;}
     
        public LeaveTaskWrapper(Boolean selected, String sub,String sta,string id,string dt){
            isSelected = selected;
            subject = sub;
            status = sta;
            tskId = id; 
            duedate = dt;
        }    
     }
     
    public pagereference InsertLveTasks(){
        for(LeaveTaskWrapper lvWrap:taskFromLeave){
            if(lvWrap.isSelected){
            pageReference pdf = Page.WCT_TaskPDF;
            pdf.getParameters().put('id',lvWrap.tskId);
            Blob body;
            try {
                body =  pdf.getContent();
            } catch (VisualforceException e) {
                body = Blob.valueOf('Some Text');
            }
            Document doc= new Document();
            doc.Name=lvWrap.subject+'.pdf';
            doc.body=body;
            doc.FolderId=Label.Attachment_Zip_Document_Folder_Id;
            insert doc;
            ListAttachment.add(new AttachmentList(true, doc.Name,doc.id));  
            doc = new Document();
            }
        }
        ShowLVEpopup=false;
        return null;
    }
     
     
}