/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class SOnboardingInitiationFormV1 

{
    public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        recordtype mobRecType = [select id from recordtype where DeveloperName = 'Employment_Visa'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        String employeeType = 'Employee';
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        mob.WCT_India_Cellphone__c='12345678';
        mob.WCT_Accompanied_Status__c=true;
        mob.WCT_Number_Of_Children__c=1;
        mob.WCT_Accompanying_Dependents__c=2;
        mob.WCT_US_Project_Mngr__c='Testing@mobility.com';
        mob.WCT_Regional_Dest__c='Testing';
        mob.WCT_USI_RCCode__c='Testing';
        mob.WCT_Project_Controller__c = 'test@mobility.com';
        mob.RecordTypeId = mobRecType.Id;
        insert mob;
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=false;
        insert t;
        datetime startdate=date.Today().adddays(10);
        datetime assignstartdate=date.Today().adddays(2);
        datetime assignenddate=date.Today().adddays(5);
        
        Test.starttest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_OnboardingInitiationForm;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_OnboardingInitiationForm controller=new WCT_OnboardingInitiationForm();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_OnboardingInitiationForm();
        controller.save(); 
        controller.dateOfArrival=startdate.format('MM/dd/yyyy');
        controller.assignmentRec.WCT_Client_Office_Name__c='test';
        controller.assignmentRec.WCT_Client_Office_City__c='test';
        controller.assignmentRec.WCT_Client_Office_State__c='test';
        controller.assignmentstartdate = assignstartdate.format('MM/dd/yyyy');
        controller.assignmentenddate = assignenddate.format('MM/dd/yyyy');
        controller.employeeType = 'Employee';
        controller.doc=WCT_UtilTestDataCreation.createDocument();
        //controller.uploadAttachment();
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
       // controller.uploadAttachment();
    }  
    
    public static testmethod void m2()
    {
        //recordtype rt=[select id from recordtype where sObjectType = 'WCT_Mobility__c'];
        recordtype rt=[select id,Name from recordtype where DeveloperName = 'WCT_Employee'];
        recordtype mobRecType = [select id from recordtype where DeveloperName = 'Employment_Visa'];
        recordtype mobRecTypeB = [select id from recordtype where DeveloperName = 'Business_Visa'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        String employeeType = 'Employee';
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        mob.WCT_India_Cellphone__c='12345678';
        mob.WCT_Accompanied_Status__c=true;
        mob.WCT_Number_Of_Children__c=1;
        mob.WCT_Accompanying_Dependents__c=2;
        mob.WCT_US_Project_Mngr__c='Testing@mobility.com';
        mob.WCT_Regional_Dest__c='Testing';
        mob.WCT_USI_RCCode__c='Testing';
        mob.WCT_Project_Controller__c = 'test@mobility.com';
        mob.RecordTypeId = mobRecTypeB.Id;
        mob.WCT_Date_of_Arrival_in_US__c=system.today();
        insert mob;
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=false;
        insert t;
        datetime startdate=date.Today().adddays(10);
        datetime assignstartdate=date.Today().adddays(2);
        datetime assignenddate=date.Today().adddays(5);
        
        Test.starttest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_OnboardingInitiationForm;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_OnboardingInitiationForm controller=new WCT_OnboardingInitiationForm();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_OnboardingInitiationForm();
       
        controller.employeeType='employeeType';
        controller.getMobilityTypeDropDownValues();
        controller.mobilityRecord.WCT_Date_of_Arrival_in_US__c=system.today()+10;
        controller.save(); 
        controller.dateOfArrival=startdate.format('MM/dd/yyyy');
        controller.assignmentRec.WCT_Client_Office_Name__c='test';
        controller.assignmentRec.WCT_Client_Office_City__c='test';
        controller.assignmentRec.WCT_Client_Office_State__c='test';
        controller.assignmentstartdate = assignstartdate.format('MM/dd/yyyy');
        controller.assignmentenddate = assignenddate.format('MM/dd/yyyy');
        controller.assignmentRec.WCT_Regional_Dest__c='test';
        controller.dateOfArrival='06/27/2016';
        controller.employeeType = 'Employee';
          controller.assignmentRec.WCT_Initiation_Date__c=Date.newInstance(2016,5,23);
        controller.assignmentRec.WCT_End_Date__c=Date.newInstance(2016,5,20); 
        //controller.listAttachments=Test_Data_Utility.createAttachmentsForSpecificObject(t);       
        controller.doc=WCT_UtilTestDataCreation.createDocument();
       // controller.getAttachmentInfo();
        //controller.uploadAttachment();
        controller.save(); 
        datetime assignstartdate1=date.Today().adddays(-2);
        datetime assignenddate1=date.Today().adddays(-5);
        controller.assignmentstartdate = assignstartdate1.format('MM/dd/yyyy');
        controller.assignmentenddate = assignenddate1.format('MM/dd/yyyy');
        controller.save(); 
        datetime assignstartdate2=date.Today().adddays(-2);
        controller.assignmentstartdate = assignstartdate2.format('MM/dd/yyyy');
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
       // controller.uploadAttachment();
    }
     public static testmethod void m3()
    {
      recordtype rt=[select id,Name from recordtype where DeveloperName = 'WCT_Employee'];
      recordtype mobRecType = [select id from recordtype where DeveloperName = 'Employment_Visa'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        String employeeType = 'Employee';
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        mob.WCT_India_Cellphone__c='12345678';
        mob.WCT_Accompanied_Status__c=true;
        mob.WCT_Number_Of_Children__c=1;
        mob.WCT_Accompanying_Dependents__c=2;
        mob.WCT_US_Project_Mngr__c='Testing@mobility.com';
        mob.WCT_Regional_Dest__c='Testing';
        mob.WCT_USI_RCCode__c='Testing';
        mob.WCT_Project_Controller__c = 'test@mobility.com';
        mob.RecordTypeId = mobRecType.Id;
        insert mob;
         system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@MOBILI'+mob); 
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        Insert t;
        system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@t'+t);
    
      PageReference pageRef = Page.WCT_OnboardingInitiationForm;
      Test.setCurrentPage(pageRef); 
      WCT_OnboardingInitiationForm controller=new WCT_OnboardingInitiationForm(); 
      GBL_Attachments attachmentHelper = new GBL_Attachments();      
        attachmentHelper.doc=WCT_UtilTestDataCreation.createDocument();
        controller.doc= WCT_UtilTestDataCreation.createDocument();
        attachmentHelper.uploadDocument();
        
      
      controller.taskid =t.id;
      List<Attachment> a= Test_Data_Utility.createAttachmentsForSpecificObject(t);
      insert a;
      controller.listAttachments=a;
      controller.getAttachmentInfo(); 
      //controller.init(); 
      //controller.getParameterInfo(); 
      controller.employeeType='Employee';
      controller.getMobilityTypeDropDownValues(); 
      recordtype mobRecType1 = [select id from recordtype where DeveloperName = 'Business_Visa'];
      controller.employeeType='Business';
      controller.getMobilityTypeDropDownValues(); 
       system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@tttt'+t); 
       controller.t=t;
      controller.getMobilityDetails(); 
      controller.getTaskInstance();
      controller.save(); 
        
        
        controller.assignmentRec.WCT_Client_Office_Name__c='test';
        controller.assignmentRec.WCT_Client_Office_City__c='test';
        controller.assignmentRec.WCT_Client_Office_State__c='test';
        controller.assignmentstartdate = '05/23/2016';
        controller.assignmentenddate = '05/22/2016';
        controller.assignmentRec.WCT_Regional_Dest__c='test';
        controller.dateOfArrival='06/27/2016';
        controller.employeeType = 'Employee';
        //controller.assignmentRec.WCT_Initiation_Date__c=Date.newInstance(2016,5,23);
        //controller.assignmentRec.WCT_End_Date__c=Date.newInstance(2016,5,20);   
        controller.save(); 
         controller.assignmentRec.WCT_Initiation_Date__c=Date.newInstance(2016,5,23);
        controller.assignmentRec.WCT_End_Date__c=Date.newInstance(2016,5,20); 
     controller.save(); 
             
    }
    
    
    public static testmethod void m4()
    {
       //recordtype rt=[select id from recordtype where sObjectType = 'WCT_Mobility__c'];
        recordtype rt=[select id,Name from recordtype where DeveloperName = 'WCT_Employee'];
        recordtype mobRecType = [select id from recordtype where DeveloperName = 'Employment_Visa'];
         recordtype mobRecTypeB = [select id from recordtype where DeveloperName = 'Business_Visa'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        String employeeType = 'Employee';
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        mob.WCT_India_Cellphone__c='12345678';
        mob.WCT_Accompanied_Status__c=true;
        mob.WCT_Number_Of_Children__c=1;
        mob.WCT_Accompanying_Dependents__c=2;
        mob.WCT_US_Project_Mngr__c='Testing@mobility.com';
        mob.WCT_Regional_Dest__c='Testing';
        mob.WCT_USI_RCCode__c='Testing';
        mob.WCT_Project_Controller__c = 'test@mobility.com';
        mob.RecordTypeId = mobRecType.Id;
        
        insert mob;
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=true;
        insert t;
        datetime startdate=date.Today().adddays(10);
        datetime assignstartdate=date.Today().adddays(2);
        datetime assignenddate=date.Today().adddays(5);
        
        Test.starttest();
        //String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_OnboardingInitiationForm;
        Test.setCurrentPage(pageRef); 
        //ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_OnboardingInitiationForm controller=new WCT_OnboardingInitiationForm();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_OnboardingInitiationForm();
         List<Attachment> a= Test_Data_Utility.createAttachmentsForSpecificObject(t);
          insert a;
          GBL_Attachments attachmentHelper = new GBL_Attachments();      
        attachmentHelper.doc=WCT_UtilTestDataCreation.createDocument();
        attachmentHelper.uploadDocument();
        attachmentHelper.uploadRelatedAttachment(mob.id);
        controller.getAttachmentInfo();           
        controller.save();
        controller.listAttachments=a;
        controller.employeeType='employeeType';
        controller.employeeType='Business';
        controller.getMobilityTypeDropDownValues();
        //controller.employeeType='Employee';
       // controller.getMobilityTypeDropDownValues();
        controller.save(); 
        controller.dateOfArrival=startdate.format('MM/dd/yyyy');
        controller.assignmentRec.WCT_Client_Office_Name__c='test';
        controller.assignmentRec.WCT_Client_Office_City__c='test';
        controller.assignmentRec.WCT_Client_Office_State__c='test';
        controller.assignmentstartdate = assignstartdate.format('MM/dd/yyyy');
        controller.assignmentenddate = assignenddate.format('MM/dd/yyyy');
        controller.assignmentRec.WCT_Regional_Dest__c='test';
        controller.dateOfArrival='06/27/2016';
        controller.employeeType = 'Employee';
          controller.assignmentRec.WCT_Initiation_Date__c=Date.newInstance(2016,5,23);
        controller.assignmentRec.WCT_End_Date__c=Date.newInstance(2016,5,20); 
        //controller.listAttachments=Test_Data_Utility.createAttachmentsForSpecificObject(t);       
        controller.doc=WCT_UtilTestDataCreation.createDocument();
       // controller.getAttachmentInfo();
        //controller.uploadAttachment();
        controller.save(); 
        datetime assignstartdate1=date.Today().adddays(-2);
        datetime assignenddate1=date.Today().adddays(-5);
        controller.assignmentstartdate = assignstartdate1.format('MM/dd/yyyy');
        controller.assignmentenddate = assignenddate1.format('MM/dd/yyyy');
        controller.mobilityRecord.WCT_Date_of_Arrival_in_US__c=date.today()+10;
        
        controller.save(); 
        
        controller.mobilityRecord.recordTypeId=mobRecType.Id;
        // controller.mobilityRecord.RecordType.Name = 'Business_Visa';
        //controller.dateOfArrival=String.Valueof(date.today());
        //controller.assignmentRec.WCT_Initiation_Date__c=system.today();
       // controller.mobilityRecord.WCT_Date_of_Arrival_in_US__c=date.today();
        
       // attachmentHelper.docIdList= null ;
        Document doc= new Document();
            doc.Name='test';
            doc.Body=Blob.valueOf('test');
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert doc;
            
            controller.attachmentHelper.docIdList.add(doc.Id);
            controller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'20'));
       
        datetime assignstartdate2=date.Today().adddays(-2);
        controller.assignmentstartdate = assignstartdate2.format('MM/dd/yyyy');
        controller.save(); 
        
       
         
         
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
    }
    
     public static testmethod void m5()
    {
       //recordtype rt=[select id from recordtype where sObjectType = 'WCT_Mobility__c'];
        recordtype rt=[select id,Name from recordtype where DeveloperName = 'WCT_Employee'];
        recordtype mobRecType = [select id,name,DeveloperName  from recordtype where DeveloperName = 'Employment_Visa'];
         recordtype mobRecTypeB = [select id,name,DeveloperName from recordtype where DeveloperName = 'Business_Visa'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        String employeeType = 'Employee';
        
   
        
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        mob.WCT_India_Cellphone__c='12345678';
        mob.WCT_Accompanied_Status__c=true;
        mob.WCT_Number_Of_Children__c=1;
        mob.WCT_Accompanying_Dependents__c=2;
        mob.WCT_US_Project_Mngr__c='Testing@mobility.com';
        //mob.WCT_Regional_Dest__c= assign.WCT_Regional_Dest__c ;
        mob.WCT_USI_RCCode__c='Testing';
        mob.WCT_Project_Controller__c = 'test@mobility.com';
        mob.WCT_Mobility_Employee__c = con.id;
        mob.RecordTypeId = mobRecType.Id;
        
        insert mob;
        
        WCT_Assignment__c assign = new WCT_Assignment__c ();
        assign.WCT_Client_Office_Name__c='test';
        assign.WCT_Client_Office_City__c='test';
      //  assign.WCT_US_Project_Coordinator__c='test';
        assign.WCT_Client_Office_State__c='test';
        assign.WCT_Regional_Dest__c = 'sample';
        assign.WCT_Employee__c = con.id;
        assign.WCT_Mobility__c=mob.id;
        insert assign;
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=true;
        t.WCT_ToD_Task_Reference__c = 'sample';
        insert t;
        datetime startdate=date.Today().adddays(10);
        datetime assignstartdate=date.Today().adddays(2);
        datetime assignenddate=date.Today().adddays(5);
        
        Test.starttest();
        //String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_OnboardingInitiationForm;
        Test.setCurrentPage(pageRef); 
        //ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_OnboardingInitiationForm controller=new WCT_OnboardingInitiationForm();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_OnboardingInitiationForm();
         List<Attachment> a= Test_Data_Utility.createAttachmentsForSpecificObject(t);
          insert a;
          
          Document dvar = WCT_UtilTestDataCreation.createDocument();
          
          List<String> lsvar = new List<String>();
          
          lsvar.add(dvar.id);
          
          GBL_Attachments attachmentHelper = new GBL_Attachments();      
        attachmentHelper.doc=WCT_UtilTestDataCreation.createDocument();
        //attachmentHelper.docIdList = lsvar;
        attachmentHelper.uploadDocument();
        attachmentHelper.uploadRelatedAttachment(mob.id);
        controller.getAttachmentInfo();           
        controller.save();
        controller.listAttachments=a;
        controller.employeeType='employeeType';
        controller.employeeType='Business';
        controller.getMobilityTypeDropDownValues();
        //controller.employeeType='Employee';
       // controller.getMobilityTypeDropDownValues();
        controller.save(); 
        controller.dateOfArrival=startdate.format('MM/dd/yyyy');
        controller.assignmentRec.WCT_Client_Office_Name__c='test';
        controller.assignmentRec.WCT_Client_Office_City__c='test';
        controller.assignmentRec.WCT_Client_Office_State__c='test';
        controller.assignmentstartdate = assignstartdate.format('MM/dd/yyyy');
        controller.assignmentenddate = assignenddate.format('MM/dd/yyyy');
        controller.assignmentRec.WCT_Regional_Dest__c='Testing';
        mob.WCT_Regional_Dest__c = controller.assignmentRec.WCT_Regional_Dest__c;
        
        //controller.dateOfArrival='06/27/2016';
        controller.employeeType = 'Employee';
          controller.assignmentRec.WCT_Initiation_Date__c= Date.newInstance(2015,5,23);
        controller.assignmentRec.WCT_End_Date__c=Date.newInstance(2015,5,20);
        
        
         controller.assignmentRec.WCT_Initiation_Date__c= system.Today()-1;
        controller.assignmentRec.WCT_End_Date__c= date.Today();
        
         
        //controller.listAttachments=Test_Data_Utility.createAttachmentsForSpecificObject(t);       
        controller.doc=WCT_UtilTestDataCreation.createDocument();
       // controller.getAttachmentInfo();
        //controller.uploadAttachment();
        controller.save(); 
        datetime assignstartdate1=date.Today().adddays(-2);
        datetime assignenddate1=date.Today().adddays(-5);
        controller.assignmentstartdate = assignstartdate1.format('MM/dd/yyyy');
        controller.assignmentenddate = assignenddate1.format('MM/dd/yyyy');
        controller.mobilityRecord.WCT_Date_of_Arrival_in_US__c=system.today()+10;
        
        controller.save(); 
        
        controller.mobilityRecord.recordTypeId=mobRecTypeB.Id;
      //  controller.mobilityRecord.recordType.Name = mobRecTypeB.Name;
        //controller.dateOfArrival=String.Valueof(date.today());
        //controller.assignmentRec.WCT_Initiation_Date__c=system.today();
       // controller.mobilityRecord.WCT_Date_of_Arrival_in_US__c=date.today();
        
       // attachmentHelper.docIdList= null ;
        Document doc= new Document();
            doc.Name='test';
            doc.Body=Blob.valueOf('test');
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert doc;
            
            controller.attachmentHelper.docIdList.add(doc.Id);
            controller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'20'));
       
        datetime assignstartdate2=date.Today().adddays(-2);
        controller.assignmentstartdate = assignstartdate2.format('MM/dd/yyyy');
        controller.save(); 
        
       
         
         
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
    }
    
}