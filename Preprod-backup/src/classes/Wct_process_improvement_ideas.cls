public class Wct_process_improvement_ideas extends SitesTodHeaderController  {
  
    /* Public Variables */
    public Case caseRecord {get; set;}
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
  //  public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
    
    // Error Message related variables
    public boolean CPI {get; set;}
    public boolean CPIty {get; set;}
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}    
    
    private String urlTodCat = null;
    private String urlTodSupportArea = null;
    
     public GBL_Attachments attachmentHelper{get; set;}
    
   
    public Wct_process_improvement_ideas() {
    CPI=true;
    CPIty=false;
        if(invalidEmployee) {
            return;
        }

        //Create a New Case Record
        caseRecord = new Case();

        //Populate Email        
        caseRecord.suppliedEmail = loggedInContact.email;  
        
        doc = new Document();
       // UploadedDocumentList = new List<AttachmentsWrapper>();
                
        //pageError = false;
        
          attachmentHelper= new GBL_Attachments();
     
    }

    

    public PageReference saveCase() {
        /* Set Origin */        
        system.debug('cjkasgda'+loggedincontact);
        caseRecord.origin = 'Web';

        /* Set Contact */
        caseRecord.ContactID = loggedInContact.id;
        caseRecord.WCT_Category__c='CPI Process Improvement';
        caseRecord.recordtypeid=SYSTEM.LABEL.CPI_case_recordtype;
       
        

        /*Insert Case Record*/
        insert caseRecord;
        CPIty=true;
        CPI=false;
        /* Upload Documents as Related Attachments to Case After the Case Record is created */
        attachmentHelper.uploadRelatedAttachment(caseRecord.id);
        
        /*Set Page Reference After Save*/
        return null;/*new PageReference('/apex/todCaseUS?id=' + caseRecord.id + '&em=' + CryptoHelper.encrypt(loggedInContact.Email)
                            + '&sa=' + urlTodSupportArea);*/
    }
    
  
}