global class  WCT_H1bcap_Imm_Notification Implements Database.Batchable <sObject> {
    global Database.queryLocator start(Database.BatchableContext bc) {
        
    Date d= system.today();
            
        String SOQL = 'SELECT Id,WCT_H1BCAP_Status__c,WCT_H1BCAP_Resource_Manager_Email_ID__c ,WCT_H1BCAP_After7thBusinessday_SLL__c  FROM WCT_H1BCAP__c WHERE WCT_H1BCAP_Status__c =  \'USI SLL Approved\'  AND (WCT_H1BCAP_After7thBusinessday_SLL__c =:d)' ;
         return Database.getQueryLocator(SOQL);
       
    }

    global void execute(Database.BatchableContext bc, List<WCT_H1BCAP__c> H1bcap) {
    
 // Get the Resp. RM Emails for their Practitioners   
     list<string>  currentRMEmail = new list<string>();   
     set<string>   SingleRMmail = new set<string>();
     string orgmail = Label.WCT_H1BCAP_Mailbox;
     List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
     OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
     Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'Notification_to_GM_I_Team' AND IsActive = true];
    
             
         for(WCT_H1BCAP__c w: H1bcap) {
          System.debug('Entering ind loop*******');   
           if(w.WCT_H1BCAP_Status__c  == 'USI SLL Approved') 
         {    
            w.WCT_H1BCAP_Status__c = 'Sent to GM & I Team';
             currentRMEmail.add(w.WCT_H1BCAP_Resource_Manager_Email_ID__c);
            
         }   
     
        }   
        
       //Adding list into Set
        for(string s :currentRMEmail)
        {
         SingleRMmail.add(s);
        }
       // Convert list into Set ....  
      List<string> fnlRMmail = new list<string>(SingleRMmail);  
        
        
       if(H1bcap.size()>0)
       { 
        
        update H1bcap;
        system.debug('RM EMails *********'+currentRMEmail);
         system.debug('RM EMails size *********'+currentRMEmail.size());
         
      //Sending mails to GM&I Team
      
       contact u = [select id, Email from contact where Email = :label.WCT_Santhosh_Mailbox limit 1];
   
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
        mail.SetTemplateid(et.id);
         mail.setSaveAsActivity(false);
          mail.setTargetObjectId(u.id);
         //mail.setCcAddresses(currentRMEmail);
           mail.setCcAddresses(fnlRMmail); 
            mail.setWhatid(H1bcap[0].id);
             mail.setOrgWideEmailAddressId(owe.id);        
              mailList.add(mail);   
          
       }
        if(mailList.size() >0)
      {  
         try{
         System.debug('Mail Has been sent*******');
         Messaging.sendEmail(mailList);   
          }   
          catch(Exception npe) {
          System.debug('The following exception has occurred: ' + npe.getMessage());
             }            
      } 
       
    }
    
     global void finish(Database.BatchableContext bc) {
    }
  }