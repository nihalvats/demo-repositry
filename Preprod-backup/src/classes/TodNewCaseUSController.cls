/*

Project : General Enquiry Form
Associated VF Page : todNewCase
Description:Controller which allows to create case in salesforce for deloitte employees
Author : Gunasekaran Lakshmanan

*/

public  class TodNewCaseUSController extends SitesTodHeaderController {
    
    /* Public Variables */
    public Case caseRecord {get; set;}
    public GBL_Attachments attachmentHelper{get; set;}
    public Case_form_Extn__c caseExt{get;set;}
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public List<WCT_List_of_Names__c> listOfNames {get; set;}
    public Boolean isSaIndia {get;set;}
    public String ElecomQ1 {get; set;}
    public String ElecomQ1other {get; set;}
    public String ElecomQ2A {get; set;}
    public String ElecomQ2B {get; set;}
    
    
    // Error Message related variables
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String AttachmentErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}  
    
    //Category Fields
    public String urlTodCat {get;set;}
    public String todNewCat {get;set;}
    
    private String urlTodSupportArea = null;
    public string CompCat{get;set;}
    
    /*
        Getter Setter for Contact Sensitive Field to show up in Form witout providing the access to the main Field. 
    */
    
    public string getContactPhone()
    {
        return loggedInContact==null?'':loggedInContact.Phone;
        
    }
    
    public string getContactPersonalId()
    {
        return loggedInContact==null?'': loggedInContact.WCT_Person_Id__c;
        
    }
    
    public string getContactOffice()
    {
    return loggedInContact==null?'': loggedInContact.WCT_Home_Office__c;
    
    }
    
     public string getContactEntity()
    {
    return loggedInContact==null?'': loggedInContact.WCT_Entity__c;
    
    }
    
     public string getContactJobLevel()
    {
    return loggedInContact==null?'': loggedInContact.WCT_Job_Level_Text__c;
    
    }
    
     public string getContactFunction()
    {
    return loggedInContact==null?'': loggedInContact.WCT_Function__c;
    
    }
    
      public string getContactCostCenter()
    {
    return loggedInContact==null?'':loggedInContact.WCT_Cost_Center__c;
    
    }
    


    public TodNewCaseUSController() {
    attachmentHelper= new GBL_Attachments();   
        if(invalidEmployee) {
            return;
        }

        //Set Support Area - India to be false
        isSaIndia = false;
        CompCat ='--None--';
        
        //Create a New Case Record
        caseRecord = new Case();
        caseRecord.RecordTypeId= Schema.SObjectType.Case.getRecordTypeInfosByName().get('Case').getRecordTypeId();
        caseExt = new Case_form_Extn__c();
        //Populate Email        
        caseRecord.suppliedEmail = loggedInContact.email;  
        
        /*Below method is used to process all the values from Parameter and store appropriate Info into Case Record*/
        getParameterInfo();        
        
        /*Below method is used to get all the Category Names(Short Form) and get related Info for each Category */
        getCategoryList();
        
        // Set Case Category
        if(String.isNotEmpty(urlTodCat)){
            
            if(todNewCat!=null){
                todNewCat=caseRecord.ToD_Case_Category__c;
                
                    }else{
        
                        for(WCT_List_of_Names__c ln : listOfNames){
                            
                            if(urlTodCat.equals(ln.Name)) {
                                //Set Initial TOD Case Category when passed through Parameters
                                //caseRecord.ToD_Case_Category__c = ln.ToD_Case_Category__c;
                                todNewCat=ln.ToD_Case_Category__c;
                                // caseRecord.ToD_Case_Category__c = 'SAmple';
                                break;
                            }
            			}
                	}
            	}
        
        // Set Support Area from URL of the page
        if(String.isNotEmpty(urlTodSupportArea))
        {
            if(urlTodSupportArea == 'IN'){
                System.debug('Setting up India');
                caseRecord.WCT_Support_Area__c = 'INDIA';
            }

            if(urlTodSupportArea == 'US'){
                System.debug('Setting up US');
                caseRecord.WCT_Support_Area__c = 'US';
            }

            if(urlTodSupportArea == 'PSN'){
                System.debug('Setting up PSN');
                caseRecord.WCT_Support_Area__c = 'US-PSN';
            }
            if(urlTodSupportArea == 'FIN'){
                System.debug('Setting up FIN');
                caseRecord.WCT_Support_Area__c = 'FIN';
            }
        }        
        
        if(loggedInContact!=null)
        {
            List<Contact> contacts= [Select Id,Name, Phone, Email, WCT_Person_Id__c,WCT_Function__c, WCT_Job_Level_Text__c, WCT_Cost_Center__c,WCT_Home_Office__c ,WCT_Entity__c  From Contact Where Id = :loggedInContact.Id  ];
            if(contacts.size()>0)
            {
                loggedInContact=contacts[0];
            }
        
        }
        
       System.debug('Case loaded');
    }
    
   /* For displaying instruction on the vf page, we are getting values from the list of names record based on selected Tod Category */
    public void getCategoryList(){
        listOfNames = new List<WCT_List_of_Names__c>();
        String query = 'SELECT Name, ToD_Case_Category__c, ToD_Case_Category_Instructions__c, Sub_Category_1__c, ' +
                       'WCT_Category__c, TOD_Service_Area__c FROM WCT_List_of_Names__c ' +
                       'WHERE WCT_Type__c = \'ToD\'';
        if(String.isNotEmpty(urlTodSupportArea)) {
            query = query + ' AND TOD_Service_Area__c = :urlTodSupportArea';
        }
        listOfNames = Database.query(query);
        system.debug('list of names'+listOfNames);
        for(WCT_List_of_Names__c ln : listOfNames) {
        system.debug('list of namesetc'+ln);
            if(null != ln.ToD_Case_Category_Instructions__c) {
            system.debug('beforeln.ToD_Case_Category_Instructions__c'+ln.ToD_Case_Category_Instructions__c);
                ln.ToD_Case_Category_Instructions__c = ln.ToD_Case_Category_Instructions__c.replaceAll('\r', ' ');
                ln.ToD_Case_Category_Instructions__c = ln.ToD_Case_Category_Instructions__c.replaceAll('\n', ' ');
                system.debug('ln.ToD_Case_Category_Instructions__c'+ln.ToD_Case_Category_Instructions__c);
            }
        }
    }
    
    /* method is used to process all the values from Parameter and store appropriate Info into Case Record  */
    public void getParameterInfo(){        
        // Get Category Values from 'cat'
        urlTodCat = ApexPages.currentPage().getParameters().get('cat');  
         
        // Get Support Area Value from 'sa'
        urlTodSupportArea = ApexPages.currentPage().getParameters().get('sa');
     }

     public PageReference saveCase() 
       {
            system.debug('<< '+caseRecord); 
           system.debug('>> '+caseRecord.request_for_a_conversation_with_the_Loca__c); 
        /* Handle Validation Rules */
           
           if(urlTodCat!=null || String.isNotEmpty(urlTodCat)){
               
               caseRecord.ToD_Case_Category__c=todNewCat;
           }          
           
        if( ('' == caseRecord.WCT_ContactChannel__c)||(null == caseRecord.WCT_ContactChannel__c) ||(null == caseRecord.ToD_Case_Category__c) ||
            ((caseRecord.ToD_Case_Category__c!='USI Tax Professionals - Compensation Query')&&(null == caseRecord.subject)) || ((caseRecord.ToD_Case_Category__c!='USI Tax Professionals - Compensation Query')&&('' == caseRecord.subject)) ||
            ((caseRecord.ToD_Case_Category__c!='USI Tax Professionals - Compensation Query')&&(null == caseRecord.description)) || ((caseRecord.ToD_Case_Category__c!='USI Tax Professionals - Compensation Query')&&('' == caseRecord.description)) || (caseRecord.ToD_Case_Category__c!='Submit an Alert or To Know Request')) 
            {
             if(caseRecord.ToD_Case_Category__c!='Submit an Alert or To Know Request' && null == caseRecord.subject && null == caseRecord.description)
              {
                                  
                pageErrorMessage = 'Please fill in all the required fields in the form.';
                pageError = true;
                return null;
               }
             else
              {
                System.debug('-----SUBJ------'+caserecord.ToD_Case_Category__c);
                pageError = false;
              }
            
            }
                
        
        /* Set Origin */        
        caseRecord.origin = 'Web';
        caseRecord.ELE_Compliance_Case_Status__c = 'Submitted';
           if(caseRecord.ToD_Case_Category__c == 'USI Tax Professionals - Compensation Query')
           {
               caseRecord.Questions_on_my_compensation__c=ElecomQ1;
        		caseRecord.request_for_a_conversation_with_the_Loca__c= ElecomQ2A;
           }
        
        /* Set Contact id to case record */
        caseRecord.ContactID = loggedInContact.id;

        /* Set Salesforce Case Categories from List of names record */
        for(WCT_List_of_Names__c ln : listOfNames){
            if(caseRecord.ToD_Case_Category__c == ln.ToD_Case_Category__c) {

                //Set Salesforce Category
                caseRecord.WCT_Category__c = ln.WCT_Category__c;
                
                //set Salesforce Sub Category
                caseRecord.WCT_SubCategory1__c = ln.Sub_Category_1__c;
                
                break;
            }
         }
         
        /*Using Assignment Rules for Cases. This is used for Applying corresponding Queue Values*/
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId = System.Label.Case_Assignment_Rule_Id;
        caseRecord.setOptions(dmlOpts);
        
        if(caseRecord.ToD_Case_Category__c == system.label.ELEC_OE_ACTEMP_LABEL || caseRecord.ToD_Case_Category__c == 'Outside Employment and Activities (APR218) – New Hire Candidates'  || caseRecord.ToD_Case_Category__c == system.label.ELEC_OE_GEN_INQ_LABEL )
        { 
           caseRecord.WCT_Category__c =  system.label.ELE_Compliance_Case_Category; //'ELE: Compliance'
           caseRecord.WCT_SubCategory1__c =  caseRecord.ToD_Case_Category__c;
           caseRecord.WCT_SubCategory2__c = CompCat;
           caseRecord.RecordTypeId = system.label.ELE_Compliance_Common_Record_Type_Label;
           if(caseRecord.ToD_Case_Category__c == system.label.ELEC_OE_ACTEMP_LABEL)
           {
               System.debug('----------------------------CASE CATEGORY IS : -----------------------'+caseRecord.ToD_Case_Category__c);
               caseExt.RecordTypeId = system.label.CFEOEActiveRecdTyp; //ELE_Compliance_Outside_Employment_Active_Employment_Label //CFEOEActiveRecdTyp //ELE_Compliance_Outside_Employment_Active_Employment_Label //ELE_Compliance_OE_Active
           }
           if(caseRecord.ToD_Case_Category__c == 'Outside Employment and Activities (APR218) – New Hire Candidates')
           {
               caseExt.RecordTypeId =system.label.CFEOENewHireRecdTyp;
           }
           if(caseRecord.ToD_Case_Category__c == system.label.ELEC_OE_GEN_INQ_LABEL)
           {
               caseExt.RecordTypeId =system.label.ELE_Compliance_Outside_Employment_General_Inquiries_Label;
           }
         }
        
        if(caseRecord.ToD_Case_Category__c == 'Submit an Alert or To Know Request')
        {
            caseRecord.WCT_Category__c =  system.label.Knowledge_Management_Case_Category;
            
        }
        
        if(caseRecord.ToD_Case_Category__c == 'Nepotism - Active Personnel' || caseRecord.ToD_Case_Category__c == 'Nepotism - New hires')
        {
            caseRecord.WCT_Category__c =  system.label.ELE_Compliance_Case_Category; //'ELE: Compliance'
            caseRecord.WCT_SubCategory1__c =  caseRecord.ToD_Case_Category__c;
            caseRecord.RecordTypeId =system.label.ELE_Compliance_Common_Record_Type_Label;
            caseExt.RecordTypeId =system.label.ELEC_Nepotism_NewHire_Label;
        }
        
        else if(caseRecord.ToD_Case_Category__c == system.label.ELEC_MTC_GEN_INQ_LABEL || caseRecord.ToD_Case_Category__c == 'Military Time Compliance - Orders' || caseRecord.ToD_Case_Category__c == 'Nepotism - Active Personnel' || caseRecord.ToD_Case_Category__c == 'Nepotism - New hires' || caseRecord.ToD_Case_Category__c == 'PDA Violations US' || caseRecord.ToD_Case_Category__c == 'PDA Violations USI')
        {
            caseRecord.WCT_Category__c =  system.label.ELE_Compliance_Case_Category; //'ELE: Compliance'
            caseRecord.WCT_SubCategory1__c =  caseRecord.ToD_Case_Category__c;
            caseRecord.RecordTypeId =system.label.ELE_Compliance_Common_Record_Type_Label;
        }
               
         /*Insert Case Record*/
        try { 
          insert caseRecord;
          }
        catch (Exception e) {
            WCT_ExceptionUtility.logException('TodNewCaseUSController', 'General Enquiry Form', e.getMessage());
        }  
            
        if(caseRecord.ToD_Case_Category__c == system.label.ELEC_MTC_GEN_INQ_LABEL || caseRecord.ToD_Case_Category__c == 'Military Time Compliance - Orders' ||  caseRecord.ToD_Case_Category__c == 'PDA Violations US' || caseRecord.ToD_Case_Category__c == 'PDA Violations USI' || caseRecord.ToD_Case_Category__c == 'Submit an Alert or To Know Request')
        {
             caseExt.RecordTypeId = system.label.ELE_Compliance_Common_Record_Type_Label_For_Case_Form_Extension; //'0121b0000008TwK'; //CFEOEActiveRecdTyp 0121b0000008TwK
             //caseRecord.RecordType = system.label.ELE_Compliance_Case_Record_Type;
        }
        
        try {            
         caseExt.GEN_Case__c=caseRecord.id;
         insert caseExt;
         }
        catch (Exception e) {
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('TodNewCaseUSController', 'General Enquiry Form', e.getMessage());
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=HAQ_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
        }  
     
        /* Upload Documents as Related Attachments to Case After the Case Record is created */    
        attachmentHelper.uploadRelatedAttachment(caseRecord.id);
      
        /*Set Page Reference After Save*/
        return new PageReference('/apex/todCaseUS?id=' + caseRecord.id + '&sa=' + urlTodSupportArea);
    }
    

    // ELE Compensation form question 1
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Components in my compensation structure ','Components in my compensation structure ')); 
        options.add(new SelectOption('The variable bonus program and/or how my variable bonus has been computed','The variable bonus program and/or how my variable bonus has been computed'));
        options.add(new SelectOption('Both of the above','Both of the above'));
        options.add(new SelectOption('Thank you, I don\'t have any questions','Thank you, I don\'t have any questions')); 
        options.add(new SelectOption('Any other, please provide details here','Any other, please provide details here')); return options; 
    }
    
    // ELE Compensation form question 2A
    public List<SelectOption> getItems2A() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Yes','Yes')); 
        options.add(new SelectOption('No','No')); return options; 
    } 
    
    public List<SelectOption> getUSIBLItems()
    {
        List<SelectOption> opt = new List<SelectOption>();
        opt.add(new SelectOption('Yes','1. Yes, request for leadership discussion'));
        opt.add(new SelectOption('No','2. No')); 
        return opt;
    }
    
     //USI Consulting Professional - Request a compensation discussion with Business Leader
    
    Public void renderoutempfields(){
        system.debug('123124' +caseExt.Name_of_Entity__c);
        
               
        if(!(caseRecord.ToD_Case_Category__c == 'Outside Employment – Active')){
            caseExt = new Case_form_Extn__c();  
            
            system.debug('111111111' +caseExt.Name_of_Entity__c);
             CompCat ='';
        }
        
        
        
        
    }
   
    
}