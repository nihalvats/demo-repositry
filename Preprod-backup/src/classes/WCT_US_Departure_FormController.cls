/********************************************************************************************************************************
Apex class         : <WCT_US_Departure_FormController>
Description        : <Controller which allows to Update Task and Mobility>
Type               :  Controller
Test Class         : <WCT_US_Departure_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 24/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/   

public class WCT_US_Departure_FormController  extends SitesTodHeaderController{

    //Upload Related Variables
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public task taskObj {get;set;}
    public String taskID{get;set;}
    public List<Attachment> listAttachments {get; set;}
    public Map<Id, String> mapAttachmentSize {get; set;}  
    public GBL_Attachments attachmentHelper{get; set;}

   //Error Related Variables
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    
    //Date Fields
    public String departureDayUS {get;set;}
    public String arrivalDayUSI {get;set;}
    public String fromDate {get;set;}
    public String toDate {get;set;}  
    public List<SelectOption> takePTOOptions{set; get;}
    public String takePTO{set; get;}  
    
    //Mobility Instance Variables
    public WCT_Mobility__c mobilityRecord{get;set;} 
         
    /* Invoking a Constructor with Task and Attachments  */
     
    public WCT_US_Departure_FormController() {
      init();
      attachmentHelper = new GBL_Attachments();
      getParameterInfo();
    
      if (taskID == '' || taskID == null) {
       invalidEmployee = true;
       return;
      }
      getTaskInstance();
      getAttachmentInfo();
      getMobilityDetails();
    
     }

    /********************************************************************************************
    *Method Name         : <init()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <Init() Used for loading Mobility,Attachments>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/     
   
     private void init() {
    
      mobilityRecord = new WCT_Mobility__c();
      takePTOOptions = new List < SelectOption > ();
      takePTOOptions.add(new SelectOption('Yes', 'Yes'));
      takePTOOptions.add(new SelectOption('No', 'No'));
      takePTO = 'No';
      doc = new Document();
      listAttachments = new List <Attachment> ();
      mapAttachmentSize = new Map <Id,String> ();
    
     }

    /********************************************************************************************
    *Method Name         : <getParameterInfo()>
    *Return Type         : <Null>
    *Param’s             : URL
    *Description         : <GetParameterInfo() Used to get URL Params for TaskID>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/    
   
     private void getParameterInfo() {
      taskID = ApexPages.currentPage().getParameters().get('taskid');
     }

    /********************************************************************************************
    *Method Name         : <getTaskInstance()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <GetTaskInstance() Used for Querying Task Instance>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/    
   
     private void getTaskInstance() {
      taskObj = [SELECT Status, OwnerId, WhatId, WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c FROM Task WHERE Id = : taskID];
     }
    
    /********************************************************************************************
    *Method Name         : <getAttachmentInfo()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <GetAttachmentInfo() Used for Retrieving Attachment Details>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/       
 
     @TestVisible
     private void getAttachmentInfo() {
      listAttachments = [SELECT ParentId,
                               Name,
                               BodyLength,
                               Id,
                               CreatedDate
                               FROM Attachment
                               WHERE ParentId = : taskid
                               ORDER BY CreatedDate DESC
                               LIMIT 50
      ];
    
      for (Attachment a: listAttachments) {
       String size = null;
    
       if (1048576 < a.BodyLength) {
        size = '' + (a.BodyLength / 1048576) + ' MB';
       } else if (1024 < a.BodyLength) {
        size = '' + (a.BodyLength / 1024) + ' KB';
       } else {
        size = '' + a.BodyLength + ' bytes';
       }
       mapAttachmentSize.put(a.id, size);
      }
     }
    
    /********************************************************************************************
    *Method Name         : <getMobilityDetails()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <GetMobilityDetails() Used for Fetching Mobility Details>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/      

     public void getMobilityDetails() {
     
      mobilityRecord =     [SELECT id, WCT_Departure_From_US__c, WCT_Arrive_In_USI__c, 
                            WCT_From_Date__c, WCT_To_Date__c, OwnerId FROM WCT_Mobility__c
                            where id = : taskObj.WhatId
                          ];
    
     }
    
    /********************************************************************************************
    *Method Name         : <save()>
    *Return Type         : <PageReference>
    *Param’s             : 
    *Description         : <Save() Used for for Updating Task and Mobility>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/ 
   
     public pageReference save() {
         
        //Validation for Required Fields
        
         if ((null == departureDayUS) || ('' == departureDayUS) || (null == arrivalDayUSI) || ('' == arrivalDayUSI)) {
               pageErrorMessage = 'Please fill in all the required fields on the form.';
               pageError = true;
               return null;
           }
        
       //Date Parsing from String to Apex Date Format
           
        if (String.isNotBlank(departureDayUS)) {
               mobilityRecord.WCT_Departure_From_US__c = Date.parse(departureDayUS);
          }
          
        if (String.isNotBlank(arrivalDayUSI)) {
               mobilityRecord.WCT_Arrive_In_USI__c = Date.parse(arrivalDayUSI);
          }
          
       if (mobilityRecord.WCT_Arrive_In_USI__c < mobilityRecord.WCT_Departure_From_US__c) {
               pageErrorMessage = 'Arrival date cannot be less than departure date';
               pageError = true;
               return null;
          }
      
       if (takePTO == 'Yes') {
              
               if ((null == fromDate) || ('' == fromDate) || (null == toDate) || ('' == toDate)) {
                    pageErrorMessage = 'Please fill in all the required fields on the form.';
                    pageError = true;
                    return null;
               }
               
               if (String.isNotBlank(fromDate)) {
                    mobilityRecord.WCT_From_Date__c = Date.parse(fromDate);
               }
              
               if (String.isNotBlank(toDate)) {
                    mobilityRecord.WCT_To_Date__c = Date.parse(toDate);
               }
               
               if (mobilityRecord.WCT_To_Date__c < mobilityRecord.WCT_From_Date__c) {
                    pageErrorMessage = 'To date cannot be less tha from date';
                    pageError = true;
                    return null;
               }
      }
    
      //Changing  Owner Inorder To Avoid Integrity Exception
      try {
      taskObj.OwnerId = UserInfo.getUserId();
      upsert taskObj;
    
      update mobilityRecord;
      
      //Check whether file is attached
      
      if (attachmentHelper.docIdList.isEmpty()) {
       pageErrorMessage = 'Attachment is required to submit the form.';
       pageError = true;
       return null;
    
      }
      attachmentHelper.uploadRelatedAttachment(taskObj.id);
    
      if (String.valueOf(mobilityRecord.Ownerid).startsWith('00G')) {
       taskObj.Ownerid = System.Label.GMI_User;
      } else {
       taskObj.OwnerId = mobilityRecord.Ownerid;
      }
    
      //Upserting Task Record
      
      if (taskObj.WCT_Auto_Close__c == true) {
       taskObj.status = 'completed';
      } else {
       taskObj.status = 'Employee Replied';
      }
    
      taskObj.WCT_Is_Visible_in_TOD__c = false;
      upsert taskObj;
    
      pageError = false;
    
      getAttachmentInfo();
    }
      catch (Exception e) {

            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_US_Departure_FormController', 'US Departure Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_USDEP_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
            
     }
       
        PageReference pageRef = new PageReference('/apex/WCT_US_Departure_Form_Thankyou?taskid=' + taskID);
        pageRef.setRedirect(true);
        return pageRef; 

   }
 }