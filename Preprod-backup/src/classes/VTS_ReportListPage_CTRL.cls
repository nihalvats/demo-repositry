public class VTS_ReportListPage_CTRL {
    public Set<String> searchType{get; set;}
    public List<ReportLinksWrapper> wrappers{get; set;}
    public VTS_ReportListPage_CTRL()
    {
        
        
        /*
         *   Logic to pull the correct report link fo rthe logged in User.
         *  Step 1 : find logged in Users Role. 
         *  Step 2 : find the Group where this Role is a member. In general when we role is added, a new group is created.
         *   Step 3 : find all the groups with 
         *         User as a memeber 
         *         Group (due to added role ) as a member.
         * Step 4 : loop though all the groups and add appropiate Report Type by referting to the Custom Settings. 
         * 
         * Step 5 : Query the List of name for list of reports and Dashbord based on the various report type identified above.
         * 
     */
        searchType= new Set<String>();
               
        /*find the group with role as member*/
        List<Id> userOrgroupWithRoleId= new List<Id>();
        
        /**  Step 1 : find logged in Users Role. */
        if(UserInfo.getUserRoleId()!=null)
        {
            /*
             *   Step 2 : find the Group where this Role is a member. In general when we role is added, a new group is created.
      */
            List<Group> groupWithRole=[Select Id, RelatedId From Group where RelatedId=:UserInfo.getUserRoleId()];
            for(Group tempGp :  groupWithRole)
            {
                userOrgroupWithRoleId.add(tempGp.Id);
            }
        }
    
    /*Add the User Id to the above list */
    userOrgroupWithRoleId.add(UserInfo.getUserId());
        
        /*Pulling the setting with the Report Links Type ans Group name. */
        List<VTS_Report_Link_Group_Mapping__c> reporyLinksMappings= VTS_Report_Link_Group_Mapping__c.getAll().values();
        
        Map<String, string> mapGroupReportType= new Map<String, string>();
    
       for(VTS_Report_Link_Group_Mapping__c reporyLinksMapping : reporyLinksMappings)
       {
           
           if(reporyLinksMapping.Name!=null && reporyLinksMapping.Name!='' && reporyLinksMapping.Report_Link_Type__c!=null && reporyLinksMapping.Report_Link_Type__c!='')
           {
              mapGroupReportType.put(reporyLinksMapping.Name, reporyLinksMapping.Report_Link_Type__c);
           }
       }
        
        /*
         *  Step 3 : find all the groups with 
         *         User as a memeber 
         *         Group (due to added role ) as a member.
    */
               
        
        for (GroupMember gm : [SELECT Id, group.id, group.name, group.type FROM GroupMember where (UserOrGroupId in :userOrgroupWithRoleId)  AND group.type='Regular'])
             { 
                 
                 if(mapGroupReportType.get(gm.group.name)!=null)
                 {
                     searchType.add(mapGroupReportType.get(gm.group.name));
                 }
          
             }
                        
        //searchType.add(ApexPages.CurrentPage().getParameters().get('type'));
        
        List<WCT_List_of_Names__C> reportLinks= [Select Id, Name, WCT_Type__c, VTS_Record_Type_ID__c, VTS_IsReport__c From WCT_List_Of_Names__c Where RecordType.Name='VTS Report Links' and WCT_Type__C in :searchType order by Name];
        
    wrappers = new List<ReportLinksWrapper>();
        for(WCT_List_of_Names__C temp : reportLinks)
    {
      
      ReportLinksWrapper tempItem = new ReportLinksWrapper(temp.Name, temp.VTS_Record_Type_ID__c, temp.VTS_IsReport__c);
      wrappers.add(tempItem);
    }
    
    }
    
    public class ReportLinksWrapper
    {
        public String Name{get; set;}
        public String RecordId{get; set;}
        public boolean isReport{get; set;}
        public ReportLinksWrapper(String nameTemp, String recordIdTemp, boolean isreportTemp  )
        {
            Name=nameTemp;
            RecordId=recordIdTemp;
            isReport=isreportTemp;
        }
    }
    
}