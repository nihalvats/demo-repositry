global class StageTableBatchDelete implements Database.Batchable<sObject> {
   public String query;

   global StageTableBatchDelete (String queryStr)
   {
       query=queryStr;
   }
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
      delete scope;
      DataBase.emptyRecycleBin(scope);
   }

   global void finish(Database.BatchableContext BC){
   }
}