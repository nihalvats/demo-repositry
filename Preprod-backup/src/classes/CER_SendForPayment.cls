public class CER_SendForPayment {
    
    public string requestId{get; set;}
    public CER_Expense_Reimbursement__c currentRequest{get; set;}
    public List<CER_Expense_Line_Item__c> expenseLineItems{get; set;}
    public List<Attachment> expenseAttachments{get; set;}
    public boolean isError{get; set;}
    
    
    public CER_SendForPayment()
    {
        isError=true;
     requestId= ApexPages.currentPage().getParameters().get('id');   
     if(requestId!=null && requestId!='')
     {
         List<CER_Expense_Reimbursement__c> request=[Select id, Name,CER_Name_for_Check__c, CER_Check_Address_Street_1__c,CER_Check_Address_Street_2__c, CER_Check_Address_City__c, CER_Check_Address_Zip__c,  CER_Check_Address_State__c, CER_Expense_Grand_Total__c, CER_Recruiter_Coordinator__r.Name, CER_Request_Status__c, CER_WBS_Element__c,CER_NON_US_Address__c, CER_Is_US_Address__c,CER_Travel_To_City_State__c,CER_Travel_Return_Date__c, CER_Travel_Departure_Date__c   from  CER_Expense_Reimbursement__c where id =:requestId ];
         if(request.size()>0)
         {
             currentRequest=request[0];
             /*Query all the line Items which are 
              * If flagged then status should not be declined.
              * If not flagged any status is ok.
              */
             expenseLineItems= [Select Id,  Name, CER_Expense_Amount__c, CER_Expense_Type__c, CER_Expense_Explanation__c From CER_Expense_Line_Item__c where RelatedTo__c=:currentRequest.id and ( CER_Approval_Needed__c=false or CER_Approval_Status__c!='Declined')];
             expenseAttachments= [Select Id, Name, CreatedDate, CreatedBy.Name From Attachment Where parentId= :currentRequest.Id];
             isError=false;
         }
     }
    }
    
    public PageReference updateRequest()
    {
        currentRequest.CER_Request_Status__c='Sent for Payment';
        currentRequest.CER_Sent_For_Payment_Date__c=datetime.now();
        update currentRequest;
        return new PageReference('/'+currentRequest.id);
        
    }

}