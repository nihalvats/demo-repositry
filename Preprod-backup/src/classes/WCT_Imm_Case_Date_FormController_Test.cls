@isTest
public class WCT_Imm_Case_Date_FormController_Test 

{
  public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        recordtype immRecType = [select id from recordtype where DeveloperName = 'L1_Visa'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Immigration__c imm=WCT_UtilTestDataCreation.createImmigration(con.id);
        imm.RecordTypeId = immRecType.Id;
        imm.WCT_Immigration_Status__c='Drop Box';
        imm.WCT_Visa_Type__c='B1/B2';
        insert imm;
        WCT_Task_Reference_Table__c taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.Form_Verbiage__c = 'Hi. This is Test.';
        insert taskRef;
        task t=WCT_UtilTestDataCreation.createTask(imm.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=false;
        t.WCT_Task_Reference_Table_ID__c = taskRef.Id;
        insert t; 
        
        Test.starttest();
        
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_Imm_CaseSubmission_Date_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        //datetime startdate=date.Today().adddays(10);
        //datetime enddate=date.Today().adddays(20);
        WCT_Imm_Case_Date_FormController controller=new WCT_Imm_Case_Date_FormController();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        datetime startdate=date.Today().adddays(12);
        controller.updateTaskFlags();
        controller=new WCT_Imm_Case_Date_FormController();
        controller.updateTaskFlags();
         
        controller.caseSubmissionDate = string.valueOf(date.today().adddays(10));
        controller.save();
        system.debug('@case2'+controller.caseSubmissionDate);
        controller.caseDate = Date.newInstance(2016, 2, 17);
        controller.save();
        system.debug('@case1'+controller.caseDate);
        controller.caseSubmissionLocation = 'Test Location';
        controller.save();
        system.debug('@case'+controller.caseSubmissionLocation );
        controller.ImmigrationRec.WCT_Visa_Interview_Date__c = datetime.newInstance(2016, 9, 15, 12, 30, 0);
        system.debug('@case100'+controller.ImmigrationRec.WCT_Visa_Interview_Date__c);
        controller.ImmigrationRec.WCT_OFC_Appointment_Date__c = datetime.newInstance(2016, 9, 15, 12, 30, 0);
        system.debug('@case101'+controller.ImmigrationRec.WCT_OFC_Appointment_Date__c);
        controller.ImmigrationRec.WCT_OFC_Location__c = controller.caseSubmissionLocation;
        controller.ImmigrationRec.WCT_Visa_Interview_Location__c = controller.caseSubmissionLocation;
        controller.save();
        controller.taskRecord.WCT_Auto_Close__c = true;
        controller.taskRecord.status = 'Completed';
        controller.taskRecord.status = 'Employee Replied';
        controller.taskRecord.WCT_Is_Visible_in_TOD__c = false; 
        upsert controller.taskRecord;
        controller.display=true;
        controller.taskSubject = t.Subject;
        controller.taskVerbiage = taskRef.Form_Verbiage__c;
        controller.supportAreaErrorMesssage = 'Error Meassage';
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        Test.stoptest();
    }
   
    public static testmethod void m2()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        recordtype immRecType = [select id from recordtype where DeveloperName = 'L1_Visa'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Immigration__c imm=WCT_UtilTestDataCreation.createImmigration(con.id);
        imm.RecordTypeId = immRecType.Id;
        insert imm;
        WCT_Task_Reference_Table__c taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.Form_Verbiage__c = 'Hi. This is Test.';
        insert taskRef;
        task t=WCT_UtilTestDataCreation.createTask(imm.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=false;
        t.WCT_Task_Reference_Table_ID__c = taskRef.Id;
        insert t; 
        
        Test.starttest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_Imm_CaseSubmission_Date_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_Imm_Case_Date_FormController controller=new WCT_Imm_Case_Date_FormController();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller.updateTaskFlags();
        controller=new WCT_Imm_Case_Date_FormController();
        controller.updateTaskFlags();
        controller.save(); 
        
        controller.caseSubmissionDate = '03/06/2016';
        controller.caseSubmissionLocation = 'Test Location';
        controller.save(); 
        // controller.checked = true;
        controller.display=true;
        controller.taskSubject = t.Subject;
        controller.taskVerbiage = taskRef.Form_Verbiage__c;

        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        
    }        
        
        
        }