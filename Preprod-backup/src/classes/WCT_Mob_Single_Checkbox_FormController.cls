/********************************************************************************************************************************
Apex class         : <WCT_Mob_Single_Checkbox_FormController>
Description        : <Controller which allows to Update Task and Mobility>
Type               :  Controller
Test Class         : <WCT_Mob_Single_Checkbox_Form_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 25/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/ 
public class WCT_Mob_Single_Checkbox_FormController extends SitesTodHeaderController {

    // TASK RELATED VARIABLES
    public task taskRecord{get;set;}
    public WCT_Task_Reference_Table__c taskRefRecord{get;set;}
    public String taskSubject {get;set;}
    public String taskid{get;set;}
    public String taskVerbiage{get;set;}
    public boolean display{get;set;}
    public boolean checked{get;set;}
    
     // PUBLIC VARIABLES
    public WCT_Mobility__c MobilityRec {get;set;}

    // ERROR RELATED VARIABLES
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    
    // DEFINING A CONSTRUCTOR 
    public WCT_Mob_Single_Checkbox_FormController()
    {
        init();
        taskid = ApexPages.currentPage().getParameters().get('taskid');
        display=true;
        checked=false;
        taskSubject = '';
        taskVerbiage = '';
        updateTaskFlags();
    }
    
    /********************************************************************************************
    *Method Name         : <init()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <Init() Used for loading Mobility>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/     
    //INIT METHOD FOR INITIALISING OBJECTS AND ATTACHMENTS 
    private void init(){
        MobilityRec = new WCT_Mobility__c();

    } 

    /********************************************************************************************
    *Method Name         : <updateTaskFlags()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <UpdateTaskFlags() Used for fetching TaskReference and Mobility>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/    

    public void updateTaskFlags()
    {
        if(taskid==''|| taskid==null){
            display=false;
            return;
        }

        taskRecord=[SELECT id, Subject, WhatId, Ownerid, Status, WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c, WCT_Task_Reference_Table_ID__c FROM Task WHERE Id =: taskid];
        taskRefRecord = [SELECT Id, Name, WCT_Task_for_Object__c, WCT_Task_Subject__c, Form_Verbiage__c, ownerId FROM WCT_Task_Reference_Table__c Where id=:taskRecord.WCT_Task_Reference_Table_ID__c];
        MobilityRec = [SELECT Id, Name, OwnerId FROM WCT_Mobility__c WHERE Id=:taskRecord.WhatId];

        taskSubject = taskRecord.Subject;
        taskVerbiage = taskRefRecord.Form_Verbiage__c;

    }
    
    /********************************************************************************************
    *Method Name         : <save()>
    *Return Type         : <PageReference>
    *Param’s             : 
    *Description         : <Save() Used for for Updating Task and Mobility>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/     
        
    public pageReference save()
    {
      if(checked != true)
        {
            pageErrorMessage = 'Please check the checkbox in order to submit the form.';
            pageError = true;
            return null;
        }
        try{
        taskRecord.OwnerId=UserInfo.getUserId();
        upsert taskRecord;

        if(string.valueOf(MobilityRec.OwnerId).startsWith('00G')){
            taskRecord.OwnerId = System.Label.GMI_User;
        }else{
            taskRecord.OwnerId = MobilityRec.Ownerid;
        }
        
        if(taskRecord.WCT_Auto_Close__c == true){   
            taskRecord.status = 'Completed';
        }else{
            taskRecord.status = 'Employee Replied';  
        }

        taskRecord.WCT_Is_Visible_in_TOD__c = false; 
        upsert taskRecord;
        pageError = false;
        }
        catch (Exception e) {
            
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Mob_Single_Checkbox_FormController', 'Mob Single Checkbox Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_MOB_SCHX_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
            
        }
        PageReference pageRef = new PageReference('/apex/WCT_Mob_Task_Completion_ThankYou?taskid='+taskid);  
        pageRef.setRedirect(true);
        return pageRef;
        
    }
}