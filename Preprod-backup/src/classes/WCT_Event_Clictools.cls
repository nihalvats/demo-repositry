public class WCT_Event_Clictools {
    
    Public Contact con{get;set;}
    Public Event_Registration_Attendance__c ERA{get;set;}
    // Public Event__c DE{get;set;}
    Public String Eventid{get;set;}
    Public String Sname{get;set;}
    Public String Status{get;set;}  
    Public String SchoolId{get;set;}  
    
    public integer errorStatus{get; set;}
    Public WCT_Event_Clictools(){ 
        errorStatus=0;
        init();  
    }
    
    public void init()
    { 
        Eventid=apexpages.currentPage().getparameters().get('q1');
        Sname=apexpages.currentPage().getparameters().get('q3');
        SchoolId=apexpages.currentPage().getparameters().get('q4');
        system.debug('# Sname'+Sname);
        if(Eventid==null|| EventId==''|| Sname==null || Sname=='')
        {
             errorStatus=1;
        }
        
        /*This code is to handle the old Event URL shared where the q4 paramter of School Id is not present. Here, It will query the school from name and update the schoolId with the queried Id,
		*/
        if(SchoolId==''||SchoolId==null)
        {
			List<School__c> school= [Select Id, Name from School__C Where Name=:Sname ];
            
            if(school.size()>0)
            {
                SchoolId=school[0].Id;
            }
            
        }
        con = new contact();
        ERA= new Event_Registration_Attendance__c();
    }
    Public Pagereference submit()
    {
        List<Contact> Conup=[Select id from Contact where Email=:con.Email order by createddate];
        List<School__c> Sid= [Select id, Name from School__c where Id=:SchoolId order by createddate];
        System.debug('## Contact '+con);
        system.debug('# Sid'+Sid);
        if(conup.size() == 0)
        {
            
            try
            {
                if(!Sname.containsIgnoreCase('Multi School Event'))
                {
                 if(SchoolId!=null && SchoolId!='')   
                     con.WCT_School_Name__c=SchoolId;
                    con.RecordTypeId='01240000000QLGc';    
                    insert con;
                }
                else
                {
                    System.debug('### Multi School Event');
                    con.RecordTypeId='01240000000QLGc';    
                    insert con;
                }
            }
            catch(Exception e)
            {
                System.debug('Exception '+e);
                errorStatus=2;
               WCT_ExceptionUtility.logException('WCT_Event_Clictools','General_RSVP_Form_Release',e.getMessage()+' Line # '+e.getLineNumber()); 
            }
        }  
        else    
        {
            try
            {
                if(!Sname.containsIgnoreCase('Multi School Event')){
                    if(SchoolId!=null && SchoolId!='')   
                     con.WCT_School_Name__c=SchoolId;
                    con.id=conup[0].id;   
                    update con; 
                }
                else{
                    con.id=conup[0].id;   
                    update con; 
                }
            }
            catch(Exception e)
            {
                System.debug('Exception '+e);
                errorStatus=2;
               WCT_ExceptionUtility.logException('WCT_Event_Clictools','General_RSVP_Form_Release',e.getMessage()+' Line # '+e.getLineNumber()); 
            }

        }
        
        try
        {
        List<Event_Registration_Attendance__c> EventDetails =[Select id,Contact__r.Email,Not_Attending__c,RecordTypeId,Attending__c,Contact__r.ID,Contact__r.Firstname,Contact__r.Lastname,Contact__r.WCT_School_Name__r.Name,Accept__c from Event_Registration_Attendance__c Where Event__c =: Eventid and Contact__c=:con.id order by createddate];
        
        
        if(EventDetails.size()>0) 
        {
            
            
            System.debug('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&'+EventDetails[0]);
            //EventDetails[0].Accept__c=   ERA.Accept__c;
            if (Status=='Attending'){
                EventDetails[0].Attending__c=True;
                EventDetails[0].Not_Attending__c=False;
                
            }
            Else {
                EventDetails[0].Attending__c=False;
                EventDetails[0].Not_Attending__c=True;
            }
            
            System.debug('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&^^^^^^^^^^^^^^^^^^^^^^^^^'+EventDetails[0]);
            
            update EventDetails;
            system.debug('########################################'+ con); 
            system.debug('**************************'+ Eventdetails);
            
        }
        
        
        
        else
        {
            
            
            
            ERA.RecordTypeId='01240000000QMJB';
            ERA.Contact__c=con.id;
            ERA.Event__c=Eventid;
            if (Status=='Attending'){
                ERA.Attending__c=True;
                ERA.Not_Attending__c=False;
                
            }
            Else {
                ERA.Attending__c=False;
                ERA.Not_Attending__c=True;
            }
            Insert ERA;
            
        }
            
        }
        catch (Exception e)
        {
            System.debug('Exception '+e);
            errorStatus=2;
            WCT_ExceptionUtility.logException('WCT_Event_Clictools','General_RSVP_Form_Release',e.getMessage()+' Line # '+e.getLineNumber()); 
        }
        
        if(errorStatus!=0)
        {
            return null;
        }
        else
        {
        	return page.WCT_General_Thank_You; 
        }
    }
}