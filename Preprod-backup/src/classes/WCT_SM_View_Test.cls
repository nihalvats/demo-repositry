@isTest
public class WCT_SM_View_Test {

static testMethod void myUnitTest() {

    Test.startTest();
     Recordtype rt=[select id from Recordtype where DeveloperName = 'WCT_Employee'];
   Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
   insert con;
  system.debug('con_id::'+con.id);
  
   WCT_H1BCAP__c capobj= new WCT_H1BCAP__c();
   capobj.Current_FY_Nomination__c = true;
   capobj.WCT_H1BCAP_Email_ID__c ='test1@gmail.com';
   capobj.WCT_H1BCAP_Practitioner_Name__c =con.id;
   capobj.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'test@deloitte.com';
   insert capobj;
   
   system.debug('cap_id::'+capobj.id);
  
    WCT_SM_View smview = new WCT_SM_View();
    smview.realignComments='Change to other projects';
    smview.getRecordId = capobj.id;
    smview.rejectblock=false; 
    smview.realignblock=false;
    smview.WCTH1BCAP = [SELECT WCT_Realigned_Reason_from_SLL__c,WCT_Realigned_Reason_from_SM__c,WCT_H1BCAP_Status__c,WCT_Rejection_Reason_from_RM__c,WCT_H1BCAP_prac_name__c,OwnerId,WCT_Custom_Modified_Date__c,CreatedDate,Name,Id,WCT_H1BCAP_Case_status__c FROM WCT_H1BCAP__c where id=:capobj.id];

    smview.rejectAction();
 smview.cancelAction();
 smview.realignAction();
 smview.cancelRealignAction();  
  
 smview.loggedName = '20_H1BCAP_SM';
 smview.WCTH1BCAP.WCT_H1BCAP_Status__c = 'USI SM Approved';
 smview.StatusUpdate = 'USI SM Approved';
 smview.approveRecord();
 
 
 smview.loggedName = '20_H1BCAP_SLL';
 smview.WCTH1BCAP.WCT_H1BCAP_Status__c = 'USI SLL Approved';
 
 smview.StatusUpdate = 'USI SLL Approved';
 smview.approveRecord();
 smview.WCTH1BCAP.WCT_H1BCAP_Status__c = '';
 smview.approveRecord();
 
 
 smview.loggedName = '20_H1BCAP_SM';
 smview.WCTH1BCAP.WCT_H1BCAP_Status__c = 'USI SM Realigned';
 smview.realignComments ='change';
 smview.StatusUpdate = 'USI SM Realigned';
 smview.WCTH1BCAP.WCT_Realigned_Reason_from_SM__c = 'change to other project';
 smview.realignRecord();
 
 smview.realignComments ='';
 smview.realignRecord();
 
 smview.loggedName = '20_H1BCAP_SLL';
 smview.realignComments ='change';
 smview.StatusUpdate = 'USI SLL Realigned';
 smview.WCTH1BCAP.WCT_H1BCAP_Status__c='USI SLL Realigned';
 smview.WCTH1BCAP.WCT_Realigned_Reason_from_SLL__c = 'change to other project';
 smview.realignRecord();
 
 smview.realignComments ='';
 smview.realignRecord();
 
 
 
 smview.loggedName = '20_H1BCAP_SLL';
 smview.WCTH1BCAP.WCT_H1BCAP_Status__c = 'USI SLL Rejected';
 smview.StatusUpdate = 'USI SLL Rejected';
  smview.WCTH1BCAP.WCT_Rejected_Reason_from_SLL__c = 'change to other project';
 smview.SMComments ='Test';
 smview.rejectRecord();
 smview.SMComments ='';
 smview.rejectRecord();
 
 
 smview.loggedName = '20_H1BCAP_SM';
 smview.WCTH1BCAP.WCT_H1BCAP_Status__c = 'USI SM Rejected';
 smview.StatusUpdate = 'USI SM Rejected';
  smview.SMComments ='Test';
 smview.WCTH1BCAP.WCT_H1BCAP_Rejected_Reason_from_SM__c ='Not fit';
 smview.rejectRecord();
 
 smview.SMComments ='';
 smview.rejectRecord();
    Test.stopTest();
    }

}