/*=========================================Test Class=================================================

***************************************************************************************************************************************** 
 * Class Name   : Test_TodELEBenefitFormsController
 * Description  : test Class for TodELEBenefitFormsController
 
 *****************************************************************************************************************************************/
 
 @istest(SeeAllData = false)
public class Test_TodELEBenefitFormsController{

    static testmethod void EleBenifits() {
      
        Id consrty;
        Id lsnrt;
        
      
        Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        consrty =rtMapByName.get('Consulting').getRecordTypeId();//particular RecordId by  Name
        
        Schema.DescribeSObjectResult Rep1 = WCT_List_Of_Names__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName1 = Rep1.getRecordTypeInfosByName();// getting the record Type Info
        lsnrt =rtMapByName1.get('ELEB Category Instruction').getRecordTypeId();//particular RecordId by  Name
        
                
       WCT_List_Of_Names__c olistofnames = new WCT_List_Of_Names__c();
       olistofnames.WCT_Type__c = 'Maternity Leave';
       olistofnames.ToD_Case_Category_Instructions__c = 'ELE Car Lease Program';
       olistofnames.Name = 'Car Lease Program';
       olistofnames.RecordTypeId = lsnrt;
       insert olistofnames;
        
       WCT_List_Of_Names__c olistofnames1 = new WCT_List_Of_Names__c();
       olistofnames1.WCT_Type__c = 'Paternity Leave';
       olistofnames1.ToD_Case_Category_Instructions__c = 'ELE Paternity Leave';
       olistofnames1.Name = 'Leaves';
       olistofnames1.RecordTypeId = lsnrt;
       insert olistofnames1; 
   
   
   
       recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='test.ELE.Ben@deloitte.com';
        con.WCT_PS_Group__c ='I660';
        con.WCT_Gender__c ='Female';
        con.WCT_Function__c ='Talent';
        con.WCT_Most_Recent_Rehire__c = date.parse('6/25/2013');
        con.WCT_Original_Hire_Date__c = date.parse('6/25/2013');
        
        insert con;
     
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
       String profile = System.label.Label_for_Employee_Profile_Name;   
       User u = WCT_UtilTestDataCreation.createUser('test.ELE.Ben@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.ELE.Ben@deloitte.com');   
       insert u;
        
       System.runAs(u)
        {
            apexpages.currentpage().getparameters().put('Param1','Paternity Leave'); 
          
           Test.startTest();
         
           TodELEBenefitFormsController testcontroller = new TodELEBenefitFormsController();
          
            //testcontroller.doc=WCT_UtilTestDataCreation.createDocument();
            //testcontroller.uploadAttachment();
            testcontroller.getplaces();
            testcontroller.ProfessionalJoinDate = con.WCT_Most_Recent_Rehire__c;
            testcontroller.requesttypedef();
            testcontroller.LoggedContactJoinDate();
            testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Paternity Leave';
            testcontroller.ocaseformextn.ELE_Expected_Date_of_Delivery__c=System.today()+1;
            testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=system.today();
            
             Document doc= new Document();
            doc.Name='test';
            doc.Body=Blob.valueOf('test');
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert doc;
            
            testcontroller.attachmentHelper.docIdList.add(doc.Id);
            testcontroller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'20'));
           
            testcontroller.saveRequest();
            
            testcontroller = new TodELEBenefitFormsController();
          
            //testcontroller.doc=WCT_UtilTestDataCreation.createDocument();
            //testcontroller.uploadAttachment();
            testcontroller.getplaces();
            testcontroller.ProfessionalJoinDate = con.WCT_Most_Recent_Rehire__c;
            testcontroller.requesttypedef();
            testcontroller.LoggedContactJoinDate();
            testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Paternity Leave';
            testcontroller.ocaseformextn.ELE_Expected_Date_of_Delivery__c=System.today();
            testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=system.today()+63;
            
              doc= new Document();
            doc.Name='test';
            doc.Body=Blob.valueOf('test');
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert doc;
            
            testcontroller.attachmentHelper.docIdList.add(doc.Id);
            testcontroller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'20'));
            
           
            testcontroller.saveRequest();
            
            testcontroller = new TodELEBenefitFormsController();
          
            //testcontroller.doc=WCT_UtilTestDataCreation.createDocument();
            //testcontroller.uploadAttachment();
            testcontroller.getplaces();
            testcontroller.ProfessionalJoinDate = con.WCT_Most_Recent_Rehire__c;
            testcontroller.requesttypedef();
            testcontroller.LoggedContactJoinDate();
            testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Maternity Leave';
            testcontroller.ocaseformextn.ELE_Expected_Date_of_Delivery__c=System.today()-30;
            testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=system.today()+32;
            testcontroller.ocaseformextn.ELE_Leave_End_Date__c=system.today()-32;
            
              doc= new Document();
            doc.Name='test';
            doc.Body=Blob.valueOf('test');
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert doc;
            
            testcontroller.attachmentHelper.docIdList.add(doc.Id);
            testcontroller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'20'));
            
            
           
            testcontroller.saveRequest();
            
            
            
             Test.stopTest();
        }
         
   
    }
    
    static testmethod void positiveSubmission()
    {
        
        Id consrty;
        Id lsnrt;
        
      
        Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        consrty =rtMapByName.get('Consulting').getRecordTypeId();//particular RecordId by  Name
        
        Schema.DescribeSObjectResult Rep1 = WCT_List_Of_Names__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName1 = Rep1.getRecordTypeInfosByName();// getting the record Type Info
        lsnrt =rtMapByName1.get('ELEB Category Instruction').getRecordTypeId();//particular RecordId by  Name
        
                
       WCT_List_Of_Names__c olistofnames = new WCT_List_Of_Names__c();
       olistofnames.WCT_Type__c = 'Car Lease Program';
       olistofnames.ToD_Case_Category_Instructions__c = 'ELE Car Lease Program';
       olistofnames.Name = 'Car Lease Program';
       olistofnames.RecordTypeId = lsnrt;
       insert olistofnames;
        
       WCT_List_Of_Names__c olistofnames1 = new WCT_List_Of_Names__c();
       olistofnames1.WCT_Type__c = 'Paternity Leave';
       olistofnames1.ToD_Case_Category_Instructions__c = 'ELE Paternity Leave';
       olistofnames1.Name = 'Leaves';
       olistofnames1.RecordTypeId = lsnrt;
       insert olistofnames1; 
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='test2.ELE.Ben@deloitte.com';
        con.WCT_PS_Group__c ='I660';
        con.WCT_Gender__c ='Male';
        insert con;

       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
       String profile = System.label.Label_for_Employee_Profile_Name;   
       User u = WCT_UtilTestDataCreation.createUser('test2.ELE.Ben@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test2.ELE.Ben@deloitte.com');   
       insert u;

        System.runAs(u)
        {        
           
            apexpages.currentpage().getparameters().put('Param1','Car Lease Program');
            Test.startTest();
            TodELEBenefitFormsController testcontroller = new TodELEBenefitFormsController();
            testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c='Paternity Leave';
            testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=System.today()+20;
            testcontroller.ocaseformextn.ELE_Expected_Date_of_Delivery__c=System.today();
            testcontroller.ocaseformextn.ELE_Leave_End_Date__c=System.today()+23;
            testcontroller.ocaseformextn.ELE_EMP_Prefered_Contact_channel__c='test';
            testcontroller.ProfessionalJoinDate=null;
            
            Document doc= new Document();
            doc.Name='test';
            doc.Body=Blob.valueOf('test');
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert doc;
            
            testcontroller.attachmentHelper.docIdList.add(doc.Id);
            testcontroller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'20'));
            testcontroller.saveRequest();
             Test.stopTest();
        }
    }
    
    static testmethod void ElebenifitsCLP() {
    
      recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='test2.ELE.Ben@deloitte.com';
        con.WCT_PS_Group__c ='I660';
        con.WCT_Gender__c ='Male';
        insert con;

       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
       String profile = System.label.Label_for_Employee_Profile_Name;   
       User u = WCT_UtilTestDataCreation.createUser('test2.ELE.Ben@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test2.ELE.Ben@deloitte.com');   
       insert u;

        System.runAs(u)
        {        
           
            apexpages.currentpage().getparameters().put('Param1','Car Lease Program');
            Test.startTest();
            TodELEBenefitFormsController testcontroller = new TodELEBenefitFormsController();
             Test.stopTest();
        }
    }
    static testmethod void ElebenifitsnewSA() 
	{
    
      recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
		con.Email='test3.ELE.Ben@deloitte.com';
         con.WCT_PS_Group__c ='I660';
        con.WCT_Gender__c ='Unknown';
        insert con;
        
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
       String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test3.ELE.Ben@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test3.ELE.Ben@deloitte.com');   
      insert u;

        System.runAs(u)
        {         
            
            apexpages.currentpage().getparameters().put('Param1','Salary Advance');
            
            Test.startTest();
            TodELEBenefitFormsController testcontroller = new TodELEBenefitFormsController();
            Test.stopTest();
        }
   }
   
     static testmethod void ElebenifitsPEL() {
     
     recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
		con.Email='test4.ELE.Ben@deloitte.com';
         con.WCT_PS_Group__c ='I660';
        con.WCT_Gender__c ='Unknown';
        con.WCT_Most_Recent_Rehire__c =date.parse('5/15/2015');
        insert con;
        
		  List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
       String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test4.ELE.Ben@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test4.ELE.Ben@deloitte.com');   
      insert u;

        System.runAs(u)
        { 
		
     
      TodELEBenefitFormsController testcontroller = new TodELEBenefitFormsController();
       testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Personal Emergency Loan';
       testcontroller.ocaseformextn.ELE_Loan_Availed_Before__c  ='Yes';
       testcontroller.ocaseformextn.ELE_Prev_Availed_Loan_Month__c  =null;
        testcontroller.ocaseformextn.ELE_Prev_Availed_Loan_Year__c =null;
        testcontroller.ProfessionalJoinDate=date.parse('6/12/2015');
        
       
        testcontroller.saveRequest();
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Personal Emergency Loan';
        testcontroller.ocaseformextn.ELE_Loan_Availed_Before__c  ='Yes';
        testcontroller.ocaseformextn.ELE_Prev_Availed_Loan_Month__c  ='March';
        testcontroller.ocaseformextn.ELE_Prev_Availed_Loan_Year__c ='2015';
        testcontroller.ocaseformextn.ELE_Prev_Availed_Loan_Type__c ='Medical Loan';
        testcontroller.ocaseformextn.ELE_Amount__c =50000;
        testcontroller.ocaseformextn.ELE_EMP_Prefered_Contact_channel__c =null;
        testcontroller.ocaseformextn.ELE_I_agree_to_the_policy_T_C__c =false;
        
         
         Case c = new Case(Status = 'New',Origin = 'Email'); 
         insert c;
    
        testcontroller.saveRequest();
        
        
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Paternity Leave';
        testcontroller.ocaseformextn.ELE_Expected_Date_of_Delivery__c=date.parse('9/16/2015');
        testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=date.parse('6/21/2015');
        testcontroller.ocaseformextn.ELE_Leave_End_Date__c=date.parse('6/15/2015');
        testcontroller.ocaseformextn.ELE_EMP_Prefered_Contact_channel__c=null;
        testcontroller.ProfessionalJoinDate=date.parse('6/12/2015');
        testcontroller.saveRequest();
        
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Paternity Leave';
        testcontroller.ocaseformextn.ELE_Expected_Date_of_Delivery__c=date.parse('9/16/2015');
        testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=date.parse('6/17/2014');
        testcontroller.ocaseformextn.ELE_Leave_End_Date__c=date.parse('6/25/2015');
        testcontroller.ocaseformextn.ELE_EMP_Prefered_Contact_channel__c=null;
        testcontroller.ProfessionalJoinDate=date.parse('6/12/2015');
        testcontroller.saveRequest();
        
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Paternity Leave';
        testcontroller.ocaseformextn.ELE_Expected_Date_of_Delivery__c=date.parse('12/16/2015');
        testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=date.parse('7/17/2015');
        testcontroller.ocaseformextn.ELE_Leave_End_Date__c=date.parse('6/25/2015');
        testcontroller.saveRequest();
        
        
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Maternity Leave';
        testcontroller.ocaseformextn.ELE_Expected_Date_of_Delivery__c=date.parse('9/16/2015');
        testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=date.parse('6/21/2015');
        testcontroller.ocaseformextn.ELE_Leave_End_Date__c=date.parse('6/15/2015');
        testcontroller.ocaseformextn.ELE_EMP_Prefered_Contact_channel__c=null;
        testcontroller.ProfessionalJoinDate=date.parse('6/12/2015');
        testcontroller.saveRequest();
        
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Maternity Leave';
        testcontroller.ocaseformextn.ELE_Expected_Date_of_Delivery__c=date.parse('6/16/2015');
        testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=date.parse('6/20/2015');
        testcontroller.ocaseformextn.ELE_Leave_End_Date__c=date.parse('11/25/2015');
        testcontroller.ocaseformextn.ELE_EMP_Prefered_Contact_channel__c=null;
        testcontroller.ProfessionalJoinDate=date.parse('6/12/2015');
        testcontroller.saveRequest();
        
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Medical Paid Leave';
        
        testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=date.parse('6/25/2015');
        testcontroller.ocaseformextn.ELE_Leave_End_Date__c=date.parse('6/20/2015');
        testcontroller.ocaseformextn.ELE_EMP_Prefered_Contact_channel__c=null;
        testcontroller.ProfessionalJoinDate=date.parse('6/12/2015');
        testcontroller.saveRequest();
        
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Medical Paid Leave';
        testcontroller.ocaseformextn.ELE_Medical_Leave_Type__c='Medical Termination';
        testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=date.parse('6/15/2015');
        testcontroller.ocaseformextn.ELE_Leave_End_Date__c=date.parse('9/20/2015');
        testcontroller.ocaseformextn.ELE_EMP_Prefered_Contact_channel__c=null;
        testcontroller.ProfessionalJoinDate=date.parse('6/12/2015');
        testcontroller.saveRequest();
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Medical Paid Leave';
        testcontroller.ocaseformextn.ELE_Medical_Leave_Type__c='Miscarriage';
        testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=date.parse('6/15/2015');
        testcontroller.ocaseformextn.ELE_Leave_End_Date__c=date.parse('9/20/2015');
        testcontroller.saveRequest();
     
        
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='CPA Leave';
        testcontroller.ocaseformextn.ELE_No_of_Papers_to_be_attempted__c='1';
        testcontroller.ocaseformextn.ELE_Do_you_want_to_avail_advance_leave__c='Yes';
        testcontroller.ocaseformextn.ELE_No_of_Advance_Leaves_Days__c=null;
        testcontroller.ocaseformextn.ELE_Leave_End_Date__c=date.parse('6/15/2015');
        testcontroller.ocaseformextn.ELE_Return_Date_from_Leave__c=date.parse('6/15/2015');
        testcontroller.ocaseformextn.ELE_Exam_Date_1__c=date.parse('6/17/2015');
        testcontroller.saveRequest();
        
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='CPA Leave';
        testcontroller.ocaseformextn.ELE_No_of_Papers_to_be_attempted__c='1';
        testcontroller.ocaseformextn.ELE_Exam_Date_1__c=null;
        testcontroller.saveRequest();
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='CPA Leave';
        testcontroller.ocaseformextn.ELE_No_of_Papers_to_be_attempted__c=null;
        testcontroller.saveRequest();
        
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Adoption Leave';
        testcontroller.ocaseformextn.ELE_Legal_Adoption_Date__c=date.parse('6/15/2015');
        testcontroller.ocaseformextn.ELE_Leave_End_Date__c=date.parse('10/15/2015');
         testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=date.parse('9/15/2015');
        testcontroller.ocaseformextn.ELE_CareTaker_Type__c='Primary Caretaker';
        testcontroller.ProfessionalJoinDate=date.parse('9/14/2015');
        testcontroller.saveRequest();
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Adoption Leave';
        testcontroller.ocaseformextn.ELE_Legal_Adoption_Date__c=date.parse('6/15/2015');
        testcontroller.ocaseformextn.ELE_Leave_End_Date__c=date.parse('10/15/2015');
        testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=date.parse('9/15/2015');
        testcontroller.ocaseformextn.ELE_CareTaker_Type__c='Secondary Caretaker';
        testcontroller.ProfessionalJoinDate=date.parse('9/14/2015');
        testcontroller.saveRequest();
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Adoption Leave';
        testcontroller.ocaseformextn.ELE_Legal_Adoption_Date__c=date.parse('6/15/2015');
        testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=date.parse('6/13/2015');
        testcontroller.saveRequest();
        
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Rewards and Recognition Client';
        testcontroller.ocaseformextn.ELE_RR_Nominee_Email__c='test@deloi.com';
        testcontroller.saveRequest();
        
        
        testcontroller.ocaseformextn.ELE_ToD_Sub_Category2__c ='Unpaid Leave';
        testcontroller.ocaseformextn.ELE_Leave_Start_Date__c=date.parse('6/15/2015');
        testcontroller.ocaseformextn.ELE_Leave_End_Date__c=date.parse('6/17/2015');
        testcontroller.saveRequest();
        
        }
    }
   
    static testmethod void ElebenifitsCPA() {
    
      recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
         Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
		 con.Email='test5.ELE.Ben@deloitte.com';
         con.WCT_PS_Group__c ='I660';
         con.WCT_Gender__c ='Unknown';
         con.WCT_Function__c= 'Talent';
         insert con;
         
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
       String profile = System.label.Label_for_Employee_Profile_Name;   
       User u = WCT_UtilTestDataCreation.createUser('test5.ELE.Ben@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test5.ELE.Ben@deloitte.com');   
       insert u;
        
       System.runAs(u)
        {         
           
            apexpages.currentpage().getparameters().put('Param1','CPA Leave');
            
            Test.startTest();
            TodELEBenefitFormsController testcontroller = new TodELEBenefitFormsController();
            Test.stopTest();
        }
    }
    static testmethod void ElebenifitsCPAV() 
	{
    
      recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
         Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
		 con.Email='test6.ELE.Ben@deloitte.com';
         con.WCT_PS_Group__c ='I660';
         con.WCT_Gender__c ='Unknown';
         
         insert con;
        
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
       String profile = System.label.Label_for_Employee_Profile_Name;   
       User u = WCT_UtilTestDataCreation.createUser('test6.ELE.Ben@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test6.ELE.Ben@deloitte.com');   
       insert u;
        
       System.runAs(u)
        {        
            
            apexpages.currentpage().getparameters().put('Param1','CPA Leave');
            
            Test.startTest();
            TodELEBenefitFormsController testcontroller = new TodELEBenefitFormsController();
            Test.stopTest();
        }
    }
    
    static testmethod void ElebenifitsCLPV() 
	{
    
      recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
		con.Email='test7.ELE.Ben@deloitte.com';
        con.WCT_Gender__c ='Male';
        insert con;
        
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
       String profile = System.label.Label_for_Employee_Profile_Name;   
       User u = WCT_UtilTestDataCreation.createUser('test7.ELE.Ben@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test7.ELE.Ben@deloitte.com');   
       insert u;
        
       System.runAs(u)
        {
                
            
            apexpages.currentpage().getparameters().put('Param1','Car Lease Program');
            
            Test.startTest();
            TodELEBenefitFormsController testcontroller = new TodELEBenefitFormsController();
            Test.stopTest();
        }
    }
     static testmethod void ElebenifitsML() {
    
      recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
		con.Email='test8.ELE.Ben@deloitte.com';
        con.WCT_Gender__c ='Male';
        insert con;
        
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
       String profile = System.label.Label_for_Employee_Profile_Name;   
       User u = WCT_UtilTestDataCreation.createUser('test8.ELE.Ben@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test8.ELE.Ben@deloitte.com');   
       insert u;
        
       System.runAs(u)
        {        
        
            String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
            ApexPages.CurrentPage().getParameters().put('em',encrypt);
            apexpages.currentpage().getparameters().put('Param1','Maternity Leave');
            
            Test.startTest();
            TodELEBenefitFormsController testcontroller = new TodELEBenefitFormsController();
            Test.stopTest();
        }
            
    }
    static testmethod void ElebenifitsMEL() {
    
      recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.WCT_Gender__c ='';
        con.WCT_PS_Group__c=null;
        con.WCT_Most_Recent_Rehire__c =date.parse('5/15/2015');
        con.WCT_Original_Hire_Date__c=date.parse('5/15/2015');
		con.Email='test9.ELE.Ben@deloitte.com';
        insert con;
        
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
       String profile = System.label.Label_for_Employee_Profile_Name;   
       User u = WCT_UtilTestDataCreation.createUser('test9.ELE.Ben@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test9.ELE.Ben@deloitte.com');   
       insert u;
        
       System.runAs(u)
        {        
        //String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        //ApexPages.CurrentPage().getParameters().put('em',encrypt);
            apexpages.currentpage().getparameters().put('Param1','Medical Emergency Loan');
            
            Test.startTest();
            TodELEBenefitFormsController testcontroller = new TodELEBenefitFormsController();
            Test.stopTest();
            testcontroller.pageErrorMessageall = 'Error message';
            testcontroller.LoggedInProfBaseDataVal();
            testcontroller.LoggedContactJoinDate();
            testcontroller.pageError =true;
            testcontroller.requesttype ='None';
            testcontroller.salaryadvcondition=false;
        }
    }
    
   
    
}