/**
 * Description : Test class for WCT_CandidateAquisitionTrigerHandler
 */
@isTest
private class WCT_CandidateAquisitionTrigerHandlerTest {
    static Id candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();

    static testMethod void populateFieldsonCreationTest() {
        Profile p = [Select id from Profile where Name = 'System Administrator'];
        User u = WCT_UtilTestDataCreation.createUser('ua',p.id,'ua@ua.com','ua@ua.com');
        insert u;
        
        WCT_Requisition__c  requisition = WCT_UtilTestDataCreation.createRequisition(u.id);
        requisition.WCT_Requisition_Hiring_Level__c = 'Consultant';
        insert requisition ;

        Contact con = WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        insert con;        
        WCT_Candidate_Requisition__c candRequisition = WCT_UtilTestDataCreation.createCandidateRequisition(con.id,requisition.id);
        candRequisition.WCT_Select_Candidate_For_Interview__c = true;
        candRequisition.WCT_Recommended_Level__c = null;
        insert candRequisition;		
    }

}