@isTest
public class WCT_Business_Tax_FormController_Test
{
     public static List<ID> Doclist = new List<ID>();
     public static Document docs;
  /*
     public static testmethod void m1()
    {
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        insert mob;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        taskRef.Form_Verbiage__c = 'Submit';
        insert taskRef;
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        Test.starttest();
       
        PageReference pageRef = Page.WCT_Business_TAX_Form;
        Test.setCurrentPage(pageRef); 
      
        WCT_Business_Tax_FormController controller=new WCT_Business_Tax_FormController();
               
       // WCT_Task_ManageHandler taskInstance=new WCT_Task_ManageHandler();
       GBL_AttachmentController gblattcon = new GBL_AttachmentController ();
        GBL_Attachments attachmentHelp = new GBL_Attachments();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
       
        controller=new WCT_Business_Tax_FormController();
       controller.attachmentHelper = new GBL_Attachments();
   
    docs =WCT_UtilTestDataCreation.createDocument();
    string s = 'test';
    string s1 = '1';
    Boolean sel = true;
    gblattcon.getMaxLimit();
     gblattcon.setMaxLimit(s1);
      List<Folder> lstFolder = [Select Id From Folder Where Name = 'IEF Documents Zip' limit 1];
      
      Document doc = new Document(FolderId = lstFolder.get(0).Id, Name='Test Name',Keywords = 'Test',Body = Blob.valueOf('Some Text'),ContentType = 'application/pdf');
     
     insert doc;
      ID i = doc.id;
      Doclist.add(i);
      Attachment a = new Attachment();
      a.body = doc.body;
      a.Name= doc.Name;
      a.parentid = t.id; 
      insert a;  
       
       attachmentHelp.doc = doc;  
        attachmentHelp.uploadDocument();
       // GBL_Attachments.AttachmentsWrapper aawrap = new GBL_Attachments.AttachmentsWrapper(sel,s,i,s1);
       system.debug('Doc Testfolderid******'+attachmentHelp.doc.folderId);
         system.debug('Doc Testbody******'+attachmentHelp.doc.body);
          system.debug('Doc TestDetails******'+attachmentHelp.doc);
          
          system.debug('Doc TestListsize******'+attachmentHelp.UploadedDocumentList.size());
      //   system.assertequals(doc.body,attachmentHelp.doc.body);
        system.debug('Doclst******'+attachmentHelp.docIdList.size());
        system.debug('Doclist Isempty Testcls******'+attachmentHelp.docIdList.isEmpty());
        attachmentHelp.uploadRelatedAttachment(t.id);
        attachmentHelp.deleteAttachment();
        controller.init();
        controller.getParameterInfo();
        controller.getTaskInstance();
        controller.getMobilityDetails();
       
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        //controller.uploadAttachment();
    }
    
 */   
    static testmethod void m()
    {
        
         Profile ppd = [SELECT Id FROM Profile WHERE Name='Standard Platform User']; 
         User platuser=WCT_UtilTestDataCreation.createUser( 'siva837',ppd.id,'svallurutest9@deloitte.com.preprod','test@deloitte.com');
         insert platuser;
         
         
    Group  testGroup = new Group(Name='QUEUE NAME', Type='Queue');
        insert testGroup;
        
       QueuesObject  testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'WCT_Mobility__c');
        insert testQueue;
        
     ID   gr = [SELECT Id FROM Group WHERE Name = 'QUEUE NAME'].Id; 
      ID  us = [SELECT Id FROM User WHERE Username = 'svallurutest9@deloitte.com.preprod' LIMIT 1].Id; 
     GroupMember   gm = new GroupMember(GroupId = gr, UserOrGroupId = us); 
        insert gm;
    
         
        system.runas(platuser){
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email = 'test@deloitte.com';
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        mob.ownerid = gr;
        insert mob;
        system.debug('MOBID*****'+mob.ownerid);
        system.assertequals(mob.ownerid,gr);
              
        
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef;
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=platuser.id;
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=true;
        insert t;
        
         
      
    Test.starttest();
       
        PageReference pageRef = Page.WCT_Business_TAX_Form;
        Test.setCurrentPage(pageRef); 
      
        WCT_Business_Tax_FormController controller=new WCT_Business_Tax_FormController();
               
       // WCT_Task_ManageHandler taskInstance=new WCT_Task_ManageHandler();
       // GBL_AttachmentController gblattcon = new GBL_AttachmentController ();
       // GBL_Attachments attachmentHelp = new GBL_Attachments();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_Business_Tax_FormController();
        controller.attachmentHelper = new GBL_Attachments();
   
         
   
      List<Folder> lstFolder = [Select Id From Folder Where Name = 'IEF Documents Zip' limit 1];
      
      Document d = new Document(FolderId = lstFolder.get(0).Id, Name='Test Name',Keywords = 'Test',Body = Blob.valueOf('Some Text'),ContentType = 'application/pdf');
    // Document d = new Document ();
    
      
       controller.attachmentHelper.doc = d;  
     
       controller.attachmentHelper.doc.body = Blob.valueOf('Selected Document');
       controller.attachmentHelper.doc.Name = 'My Document';
       controller.attachmentHelper.doc.FolderId = lstFolder.get(0).Id;
      
     
      
       controller.attachmentHelper.uploadDocument();   
            
            
     /*  List<Attachment> lstattinsert = new List<Attachment>();
       Attachment a = new Attachment();
       a.body = controller.attachmentHelper.doc.body;
       a.Name= controller.attachmentHelper.doc.Name;
       a.parentid = t.id; 
       lstattinsert.add(a);
       insert lstattinsert ;  
  */
        
          system.debug('Doc1 Testfolderid******'+controller.attachmentHelper.doc.folderId);
          system.debug('Doc1 Testbody******'+controller.attachmentHelper.doc.body);
          system.debug('Do1c TestName******'+controller.attachmentHelper.doc.Name);
          system.debug('Doc1 TestListsize******'+controller.attachmentHelper.UploadedDocumentList.size());
          system.debug('Doc1 Listdata******'+controller.attachmentHelper.UploadedDocumentList);
          system.debug('Doclst1******'+controller.attachmentHelper.docIdList.size());
          system.debug('Doclist1 Isempty Testcls******'+controller.attachmentHelper.docIdList.isEmpty());
       //   system.debug('SELECTEDDOC******'+attachmentHelp.attachmentsToInsertList);
        system.assertequals(string.valueOf(gr).startsWith('00G'),true); 
        
     //   controller.attachmentHelper.uploadRelatedAttachment(t.id);
       
        controller.init();
        controller.getParameterInfo();
        controller.getTaskInstance();
        controller.getMobilityDetails();
        t.Ownerid = System.Label.GMI_User;
        t.status = 'completed';
        upsert t;
        controller.save(); 
            
            controller.save(); 
        controller.pageError=false;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        //controller.uploadAttachment();
    }
    
    
    }
    static testmethod void m1()
    {
        
        task t = new task();
        t.WCT_Auto_Close__c= true;
        insert t;
        
        WCT_Business_Tax_FormController cont = new WCT_Business_Tax_FormController();
        cont.save();
    }
    
}