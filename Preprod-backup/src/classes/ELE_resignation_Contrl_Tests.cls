/**************************************************************************************
Apex class       : ELE_resignation_Contrl_Tests
Created Date     : 25 April 2016
Type			 : Test Class
Function         : Test class to cover US resignation form controller code 

* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   25/04/2016            Original Version
*************************************************************************************/
@isTest
private class ELE_resignation_Contrl_Tests {
    
    public static Case casesep;
    
    Public static Contact resgncontact ;    
    
    @istest
    private static void testClass()
    {
        
        /*Covering the negative Scenario of SideTodHeader*/
        
        //ApexPages.CurrentPage().getParameters().put('em','');
        Emp_ResignationFormController controllerDummy = new Emp_ResignationFormController() ;
        
        
        resgncontact = new Contact();
        resgncontact.Email = 'test@deloitte.com';
        resgncontact.LastName = 'Madgikar';
        resgncontact.WCT_Function__c='TALENT';
        resgncontact.WCT_Job_Level_Text__c='test level';
        resgncontact.WCT_Hiring_location__c='Hyderabad';
        resgncontact.WCT_Office_City_Personnel_Subarea__c='Madhapur';
        insert resgncontact;
        
        User u = WCT_UtilTestDataCreation.createUser('amadgikar@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','jbahugunat@deloitte.com');
        insert u;
        System.runAs(u)
            //Running the test case with user u
        {
            Emp_ResignationFormController controller = new Emp_ResignationFormController();
            controller.loggedInContact=resgncontact;           
           // resgncontact = [select id, WCT_Hiring_location__c,WCT_Office_City_Personnel_Subarea__c from contact where ID=:resgncontact.id];
            controller.next1();
            controller.next2();
            controller.next3();
            controller.Savecase();
            casesep = new Case();
            casesep.ELE_Resignation_Submitted_on_Behalf_US__c ='No';
            casesep.RecordTypeId =Schema.SObjectType.Case.getRecordTypeInfosByName().get('Employee Lifecycle Events - Voluntary Separations').getRecordTypeId();
            
            casesep.Status='New';
            casesep.Priority='3-Medium';
            casesep.Origin='Web';
            casesep.WCT_Category__c='Separations';
            casesep.WCT_SubCategory1__c='Voluntary Separation';
            casesep.Subject='Voluntary Separation via webform';
            casesep.Description='Voluntary Separation via webform';
            insert casesep;
            
            Exit_Survey__c es = new Exit_Survey__c();
            es.ELE_Question_1__c = 'Yes';
            es.ELE_Q1_Additional_Comments__c = 'Test comments';
            insert es;                
            controller.Updateques();
            
        } 
        
    }
    
    
    
    
    
}