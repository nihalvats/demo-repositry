@isTest
private class WCT_Mobility_Request_FormController_Test {

    static testMethod void myUnitTest() {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        rt=[select id from recordtype where DeveloperName = 'Business_Visa'];
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');

        PageReference pageRef = Page.WCT_Mobility_Request_Form;
        Test.setCurrentPage(pageRef); 
        //ApexPages.CurrentPage().getParameters().put('em',encrypt);     
        ApexPages.currentPage().getParameters().put('type','Employee');
       
       // datetime startdate=date.Today().adddays(10);
       // datetime enddate=date.Today().adddays(20);
        
        Test.starttest();
        /*GBL_Attachments attachmentHelper= new GBL_Attachments();
        ApexPages.CurrentPage().getParameters().put('MobilityRecord.id',String.valueof(t.id));
        attachmentHelper.doc=WCT_UtilTestDataCreation.createDocument();
        attachmentHelper.uploadDocument();
        attachmentHelper.uploadRelatedAttachment(t.id);*/
        WCT_Mobility_Request_FormController controller=new WCT_Mobility_Request_FormController();
        ApexPages.currentPage().getParameters().put('type','Business');
        datetime startdate=date.Today().adddays(10);
        datetime enddate=date.Today().adddays(20);
        controller=new WCT_Mobility_Request_FormController();
        controller.employeeEmail='';
        controller.employeeType='Employee';
        controller.visastart=enddate.format('MM/dd/yyyy'); 
        controller.visaexpiration=startdate.format('MM/dd/yyyy');
       
        controller.saveMobilityRecord();
        controller.MobilityRecord.WCT_US_Project_Mngr__c='test@testing.com';
        controller.MobilityRecord.WCT_Notes__c = 'Hi this is test.';
        controller.MobilityRecord.WCT_USI_Resource_Manager__c='test@testing.com';
        controller.MobilityRecord.WCT_Purpose_of_Travel__c='Business Meetings/Trainings';
       // controller.BusinessVisa='Yes';
        controller.MobilityRecord.WCT_Existing_Business_Visa__c = 'Yes';
        controller.L2withEAD='Yes';
        controller.selectedDropDownValue=String.valueOf(rt.id);
        controller.firstworkingday =enddate.format('MM/dd/yyyy');
        controller.travelstartdate =enddate.format('MM/dd/yyyy');
        controller.lastworkingday=startdate.format('MM/dd/yyyy');
        controller.travelenddate=startdate.format('MM/dd/yyyy');
        controller.saveMobilityRecord();
        controller.visastart=enddate.format('MM/dd/yyyy'); 
        controller.visaexpiration=startdate.format('MM/dd/yyyy');
        controller.saveMobilityRecord();
        controller.visastart=startdate.format('MM/dd/yyyy');
        controller.visaexpiration=enddate.format('MM/dd/yyyy');
        controller.saveMobilityRecord();
        controller.firstworkingday =startdate.format('MM/dd/yyyy');
        controller.travelstartdate=startdate.format('MM/dd/yyyy');
        controller.lastworkingday=enddate.format('MM/dd/yyyy');
        controller.travelenddate =enddate.format('MM/dd/yyyy');
        //controller.doc=WCT_UtilTestDataCreation.createDocument();
        //controller.uploadAttachment();
        controller.getEmployeeDetails();
        controller.employeeEmail=con.email;
        controller.getEmployeeDetails();
       
        controller.saveMobilityRecord(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        controller.supportAreaErrorMesssage = 'Error Meassage';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        //controller.getAttachmentInfo();
        controller.employeeEmail='testingmobility@test.com';
        try{
             controller.getEmployeeDetails();
        }
        catch(Exception e)
        {}
        controller.invalidEmployee=true;
        controller=new WCT_Mobility_Request_FormController();
        Test.stoptest();
    }
    
    static testMethod void myUnitTest2() {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        rt=[select id from recordtype where DeveloperName = 'Business_Visa'];
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        mob.WCT_Existing_Business_Visa__c='BusinessVisa';
        mob.WCT_Existing_Business_Visa__c = 'Yes';
        mob.WCT_USI_RCCode__c='';
        insert mob;
        
        PageReference pageRef = Page.WCT_Mobility_Request_Form;
        Test.setCurrentPage(pageRef); 
             
        ApexPages.currentPage().getParameters().put('type','Employee');
       
        datetime startdate=date.Today().adddays(10);
        datetime enddate=date.Today().adddays(20);
        
        Test.starttest();
        /*GBL_Attachments attachmentHelper= new GBL_Attachments();
        ApexPages.CurrentPage().getParameters().put('MobilityRecord.id',String.valueof(t.id));
        attachmentHelper.doc=WCT_UtilTestDataCreation.createDocument();
        attachmentHelper.uploadDocument();
        attachmentHelper.uploadRelatedAttachment(t.id);*/
        WCT_Mobility_Request_FormController controller=new WCT_Mobility_Request_FormController();
        ApexPages.currentPage().getParameters().put('type','Business');
        controller=new WCT_Mobility_Request_FormController();
         controller.visastart=string.valueOf(date.today());
        controller.saveMobilityRecord();
        controller.employeeEmail='';
         controller.employeeType='Employee';
        controller.getMobilityTypeDropDownValues();
        
        controller.visastart=string.valueOf(date.today());
        controller.saveMobilityRecord();
        controller.MobilityRecord.WCT_US_Project_Mngr__c='test@testing.com';
        controller.MobilityRecord.WCT_Notes__c = 'Hi this is test.';
        controller.MobilityRecord.WCT_USI_Resource_Manager__c='test@testing.com';
        controller.MobilityRecord.WCT_Purpose_of_Travel__c='Business Meetings/Trainings';
        controller.mobilityRecord.WCT_USI_RCCode__c='sample';
        controller.MobilityRecord.WCT_USI_Resource_Manager__c='sample';
        controller.MobilityRecord.WCT_USI_Report_Mngr__c='sample';
      //  controller.BusinessVisa='Yes';
        controller.MobilityRecord.WCT_Existing_Business_Visa__c = 'Yes';
        controller.L2withEAD='Yes';
        controller.selectedDropDownValue=String.valueOf(rt.id);
        controller.firstworkingday =enddate.format('MM/dd/yyyy');
        controller.travelstartdate =enddate.format('MM/dd/yyyy');
        controller.lastworkingday=startdate.format('MM/dd/yyyy');
        controller.travelenddate=startdate.format('MM/dd/yyyy');
        controller.saveMobilityRecord();
        controller.visastart=enddate.format('MM/dd/yyyy');
        controller.visaexpiration=startdate.format('MM/dd/yyyy');
        controller.saveMobilityRecord();
        controller.visastart=startdate.format('MM/dd/yyyy');
        controller.visaexpiration=enddate.format('MM/dd/yyyy');
        controller.saveMobilityRecord();
        controller.firstworkingday =startdate.format('MM/dd/yyyy');
        controller.travelstartdate=startdate.format('MM/dd/yyyy');
        controller.lastworkingday=enddate.format('MM/dd/yyyy');
        controller.travelenddate =enddate.format('MM/dd/yyyy');
        //controller.doc=WCT_UtilTestDataCreation.createDocument();
        //controller.uploadAttachment();
        controller.getEmployeeDetails();
        controller.employeeEmail=con.email;
        controller.getEmployeeDetails();
       
        controller.saveMobilityRecord(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        controller.supportAreaErrorMesssage = 'Error Meassage';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        //controller.getAttachmentInfo();
        controller.employeeEmail='testingmobility@test.com';
        try{
             controller.getEmployeeDetails();
        }
        catch(Exception e)
        {}
        controller.invalidEmployee=true;
        controller=new WCT_Mobility_Request_FormController();
        Test.stoptest();
    }
     static testMethod void myUnitTest3() {
    WCT_Mobility_Request_FormController controller=new WCT_Mobility_Request_FormController();
       
        controller=new WCT_Mobility_Request_FormController();
         
         controller.mobilityRecord.WCT_USI_RCCode__c='sample';
        controller.MobilityRecord.WCT_USI_Resource_Manager__c='sample';
        controller.MobilityRecord.WCT_USI_Report_Mngr__c='sample';
         controller.MobilityRecord.WCT_Purpose_of_Travel__c='sample';
         controller.visastart=string.valueOf(date.today());
        controller.visaexpiration=string.valueOf(date.today().adddays(10));
         
        controller.saveMobilityRecord();
     }
}