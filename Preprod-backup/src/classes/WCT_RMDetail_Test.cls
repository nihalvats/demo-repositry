@isTest
private class WCT_RMDetail_Test {

static testMethod void myUnitTest() {

    Test.startTest();
   Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_RM']; 
  User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','svalluru@deloitte.com');
  insert platuser;
  
  system.runas(platuser){
   Recordtype rt=[select id from Recordtype where DeveloperName = 'WCT_Employee'];
   Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
   insert con;
  system.debug('con_id::'+con.id);
  
   WCT_H1BCAP__c capobj= new WCT_H1BCAP__c();
   capobj.Current_FY_Nomination__c = true;
   capobj.WCT_H1BCAP_Email_ID__c ='test1@gmail.com';
   capobj.WCT_H1BCAP_Practitioner_Name__c =con.id;
   capobj.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'testRM@deloitte.com';
   capobj.WCT_H1BCAP_RM_comments__c = 'Test';
   insert capobj;
   system.debug('cap_id::'+capobj.id);
  string getRecordId;
      PageReference pageRef = Page.WCT_RM_Detail;
      pageRef .getParameters().put('id',capobj.id);
      Test.setCurrentPageReference(pageRef);

   WCT_RM_DetailView  detailobj =new WCT_RM_DetailView();
    // ApexPages.CurrentPage().getParameters().put('id',String.valueof(capobj.id));      
  detailobj.getRecordId =capobj.id;
   detailobj.pageError = false;
  detailobj.rejectblock = false;
  detailobj.WCTH1BCAP = [SELECT WCT_Rejection_Reason_from_RM__c,WCT_H1BCAP_prac_name__c,OwnerId,WCT_Custom_Modified_Date__c,CreatedDate,Name,Id,WCT_H1BCAP_Case_status__c FROM WCT_H1BCAP__c where id=:capobj.id];

  
     detailobj.WCTH1BCAP.WCT_Rejection_Reason_from_RM__c='Change zipcode'; 
      detailobj.RMComments = 'Test';
      detailobj.Submitvalues();
      detailobj.Editvalues();
      detailobj.Cancel();
      detailobj.rejectblock =true;   
      detailobj.rejectRecord();
      detailobj.RejectCall();
      detailobj.cancelAction();
Test.stopTest();
        }
    }
    
    static testMethod void myUnitTest1() {

    Test.startTest();
   Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SUPERRM']; 
  User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','svalluru@deloitte.com');
  insert platuser;
  
  system.runas(platuser){
   Recordtype rt=[select id from Recordtype where DeveloperName = 'WCT_Employee'];
   Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
   insert con;
  system.debug('con_id::'+con.id);
  
   WCT_H1BCAP__c capobj= new WCT_H1BCAP__c();
   capobj.Current_FY_Nomination__c = true;
   capobj.WCT_H1BCAP_Email_ID__c ='test1@gmail.com';
   capobj.WCT_H1BCAP_Practitioner_Name__c =con.id;
   capobj.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'testSRM@deloitte.com';
   capobj.Super_RM_Comments__c = 'Test';
   insert capobj;
   system.debug('cap_id::'+capobj.id);
  string getRecordId;
  
      PageReference pageRef = Page.WCT_RM_Detail;
      pageRef.getParameters().put('id',capobj.id);
      Test.setCurrentPageReference(pageRef);

   WCT_RM_DetailView  detailobj =new WCT_RM_DetailView();
    // ApexPages.CurrentPage().getParameters().put('id',String.valueof(capobj.id));      
  detailobj.getRecordId =capobj.id;
   detailobj.pageError = false;
  detailobj.rejectblock = false;
  detailobj.WCTH1BCAP = [SELECT WCT_Rejection_Reason_from_RM__c,WCT_H1BCAP_prac_name__c,OwnerId,WCT_Custom_Modified_Date__c,CreatedDate,Name,Id,WCT_H1BCAP_Case_status__c FROM WCT_H1BCAP__c where id=:capobj.id];

  
     detailobj.WCTH1BCAP.WCT_Rejection_Reason_from_RM__c='Change zipcode'; 
      detailobj.RMComments = 'Test';
      detailobj.Submitvalues();
      detailobj.Editvalues();
      detailobj.Cancel();
      detailobj.rejectblock =true;   
      detailobj.rejectRecord();
      detailobj.RejectCall();
      detailobj.cancelAction();
Test.stopTest();
        }
    }
    
    
    
}