/*=========================================Test Class=================================================

***************************************************************************************************************************************** 
 * Class Name   : Test_ProjectsByInvolvedTeamsController
 * Description  : test Class for ProjectsByInvolvedTeamsController
 * Created By   : Deloitte India.
 *
 *****************************************************************************************************************************************/
 
 @istest(SeeAllData = false)
public class Test_ProjectsByInvolvedTeamsController {
 static testmethod void ProjectsByInvolvedteamsControllernew() {
         
         
         Test.startTest();
      
       Project__c prj = new Project__c();
       prj.Status__c = 'In Progress';
       prj.Talent_Channel__c = 'test';
       prj.Project_Category__c = 'test';
     prj.Involved_Teams__c='test';
       insert prj;
      
       
        ProjectsByInvolvedTeamsController testcontroller = new ProjectsByInvolvedTeamsController();
        
        testcontroller.SelectedPS ='';
        testcontroller.SelectedPCat=null;
        testcontroller.IsShowTConly = false;
        testcontroller.ProcessData();
        
        testcontroller.SelectedPS ='In Progress';
        testcontroller.SelectedPCat='test';
        testcontroller.IsShowTConly = false;
        testcontroller.ProcessData();
        
        
        
        testcontroller.SelectedPS ='';
        testcontroller.SelectedPCat=null;
       testcontroller.IsShowTConly = true;
        testcontroller.ProcessData();
        
        testcontroller.SelectedPS ='In Progress';
        testcontroller.SelectedPCat='test';
        testcontroller.IsShowTConly = true;
        testcontroller.ProcessData();
        apexpages.currentpage().getparameters().put('SelectedPS','In Progress');
        apexpages.currentpage().getparameters().put('SelectedPCat','test');
        apexpages.currentpage().getparameters().put('IsShowTConly','false');
        apexpages.currentpage().getparameters().put('SelectedTC','');
        apexpages.currentpage().getparameters().put('SelectedIT','test');
        testcontroller.FilterResult();
        apexpages.currentpage().getparameters().put('SelectedPS','In Progress');
        apexpages.currentpage().getparameters().put('SelectedPCat','test');
        apexpages.currentpage().getparameters().put('IsShowTConly','true');
       apexpages.currentpage().getparameters().put('SelectedTC','test');
     apexpages.currentpage().getparameters().put('SelectedIT','test');

        testcontroller.FilterResult();

        
        
        
         Test.stopTest();
         }

}