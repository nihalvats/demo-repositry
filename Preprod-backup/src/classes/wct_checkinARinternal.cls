public class wct_checkinARinternal {
    public boolean attendingno{get;set;}
    public datetime siteenddate {get;set;}
    public datetime siteenddate1 {get;set;}
    public datetime sitestartdate {get;set;}
    public datetime sitestartdate1 {get;set;}
    public integer countsubschedule{get;set;}
    public boolean thankyoumess { get; set; }
    public string confromaddress{get;set;}
    public map<string, string> ARimages = new map<string, string>();
    public boolean EventRegistrationClosed { get; set; }
    public boolean AReventended{get;set;}
    public String EventAttendance { get; set; }
    public String targetField { get; set; }
    public String eid = ApexPages.currentPage().getParameters().get('eid');
    public string confp{get;set;}
    public List<Event_Registration_Attendance__c> erlist = new List<Event_Registration_Attendance__c> ();
    public Event__c event{get;set;}
    public boolean ARpage1{get;set;}
    public boolean ARpage1error{get;set;}
    Public list<contact> AR_con;
    public contact con{get;set;}
    public Event_Registration_Attendance__c eve{get;set;}
    public Event__c eventAR{get;set;}
    public string Subschedule {get;set;}
    public wct_checkinARinternal(){
        
        EventAttendance='Yes';
        ARpage1error= false;
        con = new contact();
        eve= new  Event_Registration_Attendance__c ();
        AR_con = new list<contact>(); 
        thankyoumess = false;
        event=[select id,Time_Zone_short_form__c,AR_Event_Option_1_Start_Time__c,AR_Event_Option_1_End_Time__c,AR_Event_Option_2_Start_Time__c,AR_Event_Option_2_End_Time__c,AR_Event_Option_3_Start_Time__c,AR_Event_Option_3_End_Time__c,name,School__c,Time_Zone__c,NIR_venue_address__c,AR_Start_Date_Time_Text__c,AR_End_Date_Time_Text__c,AR_Subschedule_Time_zone__c,Venue_City_AR__c,Venue_Zip_AR__c,Venue_state_AR__c,AR_image_for_registration_page__c,Subschedule_2_Title__c,Subschedule_2_Start_Time__c,Subschedule_2_end_Time__c,Subschedule_1_Title__c,Subschedule_1_Start_Time__c,Subschedule_1_end_Time__c,Subschedule_3_Title__c,Subschedule_3_Start_Time__c,Subschedule_3_end_Time__c,End_Date_Time__c,Start_Date_time__c,NIR_Venue__c,AR_RSVP_Deadline__c,recordtype.name from event__c where id=:eid];
        List <String> stringParts = event.AR_Start_Date_Time_Text__c.split(' ');
        sitestartdate=date.parse(stringParts[0]);
        List <String> stringParts1 = event.AR_end_Date_Time_Text__c.split(' ');
        siteenddate=date.parse(stringParts1[0]);
        
        //text to Date time conversion
        string Dtt = event.AR_Start_Date_Time_Text__c;
        String[] str = dtt.split(' ');
        String[] dts = str[0].split('/');
        String[] dtss = str[1].split(':');
        Datetime incorrectstartGMT= datetime.newinstanceGMT(Integer.valueOf(dts[2]), Integer.valueOf(dts[0]), Integer.valueOf(dts[1]),Integer.valueOf(dtss[0]),Integer.valueOf(dtss[1]),Integer.valueOf(dtss[2]));
        Map<String,AR_timezones__c> allzones = AR_timezones__c.getAll();
        Map<String,Ar_time_zones_standard__c> allzones1 = Ar_time_zones_standard__c.getAll();
        
        system.debug('***************'+dts[0]);
        if(event.Time_Zone__c <>''&&(dts[0]=='03'||dts[0]=='04'||dts[0]=='05'||dts[0]=='06'||dts[0]=='07'||dts[0]=='08'||dts[0]=='09'||dts[0]=='10'))
            sitestartdate1= incorrectstartGMT.addhours((integer)allzones.get(event.Time_Zone__c).hours__c);
        
        else if(event.Time_Zone__c <>''&&(dts[0]=='11'||dts[0]=='12'||dts[0]=='01'||dts[0]=='02'))
            sitestartdate1= incorrectstartGMT.addhours((integer)allzones1.get(event.Time_Zone__c).hours_S__c);
        
        string Dtte = event.AR_End_Date_Time_Text__c;
        String[] stre = dtte.split(' ');
        String[] dtse = stre[0].split('/');
        String[] dtsse = stre[1].split(':');
        Datetime incorrectendGMT = datetime.newinstanceGMT(Integer.valueOf(dtse[2]), Integer.valueOf(dtse[0]), Integer.valueOf(dtse[1]),Integer.valueOf(dtsse[0]),Integer.valueOf(dtsse[1]),Integer.valueOf(dtsse[2]));
        
        if(event.Time_Zone__c <>''&&(dtse[0]=='03'||dtse[0]=='04'||dtse[0]=='05'||dtse[0]=='06'||dtse[0]=='07'||dtse[0]=='08'||dtse[0]=='09'||dtse[0]=='10'))
            siteenddate1= incorrectendGMT.addhours((integer)allzones.get(event.Time_Zone__c).hours__c);
        
        else if(event.Time_Zone__c <>''&&(dtse[0]=='11'||dtse[0]=='12'||dtse[0]=='01'||dtse[0]=='02'))
            siteenddate1= incorrectendGMT.addhours((integer)allzones1.get(event.Time_Zone__c).hours_S__c);
        
        DateTime dT = siteenddate;
        Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
        Integer noOfDays = myDate.daysBetween(system.today());
        
        
        if(noOfDays >= 1 && noOfDays <14)
        {
            EventRegistrationClosed = true;
            ARpage1error= false;
            AReventended = false;
            ARpage1= false;
        }
        
        else   if(noOfDays>= 14){
            // 14 days Closed
            AReventended = true;
            ARpage1error= false;
            ARpage1= false;
            EventRegistrationClosed = false;
        }
        
        
        
        else if(noOfDays< 1  )//if(noOfDays< 14 && noOfDays<>1  )
        {
            EventRegistrationClosed = false;
            ARpage1error= false;
            AReventended = false;
            ARpage1= true;
            
            
            
            
        }
        getSubscheduledet();
        countsubschedule =getSubscheduledet().size();
    }
    
    public list<SelectOption> getnirfss(){
        List<SelectOption> optns = new List<Selectoption>();
        List<eEvent_AR_FSS__c> statelist = new List<eEvent_AR_FSS__c>();
        Map<String,eEvent_AR_FSS__c> allpos = eEvent_AR_FSS__c.getAll();// custom setting
        Statelist = allpos.values();
        List<SelectOption> options = new List<SelectOption>();
        for (Integer i = 0; i < Statelist.size(); i++) {
            optns.add(new SelectOption(Statelist[i].name, Statelist[i].name));
        }
        optns.sort();
        return optns;
    }
    
    
    public PageReference AR_event_insert_or_update() {
        
        RecordType RecordTypeARid = new RecordType();
        RecordTypeARid =[select id,name from RecordType where sobjecttype = 'Event_Registration_Attendance__c' and name = 'Alumni Relation' ];
        for(List<Contact> ObjContact :[select Id,wct_function__c,Name,title,AR_ExternalWork_Email__c,AR_Personal_Email__c,AR_Preferred_Email__c,AR_Current_Title__c,firstname,Email,WCT_School_Name__c,LastName,mobilephone,Location_Preference__c,WCT_CN_Other_Institution__c,Degree_Type__c,CR_Undergrad_Major__c,CR_Expected_Grad_Month__c,CR_Expected_Grad_Year__c,CR_Undergraduate_Overall_GPA_4_00_Scale__c,TRM_serviceArea__c,TRM_correspondingFSS__c,WCT_Employee_Status__c from Contact  where email=:con.email and lastname=:con.lastname and WCT_Employee_Status__c='Active' and WCT_Is_Merged__c = false and AR_Receive_Communications__c!='Duplicate' limit 1])
        {
            for(Contact c : ObjContact) {
                AR_con.add(c);
            }
        }
        if(AR_con.size()>0)
        {
            erlist=[select id,name,Current_Employer_ID__c,registration_date__c,Which_portion_of_the_event__c,attending__c,What_is_your_primary_reason__c,What_is_your_primary_reason_other__c,Do_you_consent_to_having_your_name_and_e__c,preferred_email_type__c,preferred_email__c,Special_dietary_needs__c,Special_dietary_needs_other__c,first_nickname_for_nametag__c,curent_employer__c from Event_Registration_Attendance__c where event__c=:eid and Contact__c=:AR_con[0].id];
            contact ARcon = new contact();
            ARcon.id=AR_con[0].id;
            if(confp=='--None--')
                confp='';
            ARcon.wct_function__c=confp; 
            
            update ARcon;
            confromaddress=ARcon.id;
            
            
            if(erlist.size()>0)
            {
                
                Event_Registration_Attendance__c everegtoupdat = new Event_Registration_Attendance__c();
                everegtoupdat.id=erlist[0].id;
                if(EventAttendance == 'Yes')
                {everegtoupdat.attending__c=true;
                 everegtoupdat.not_attending__c=false;
                }
                if(EventAttendance == 'No')
                {everegtoupdat.attending__c=False;
                 everegtoupdat.not_attending__c=true;
                }
                //everegtoupdat.Current_Title__c=con.title;//targetfield;
                
                everegtoupdat.first_nickname_for_nametag__c=eve.first_nickname_for_nametag__c;
                everegtoupdat.Special_dietary_needs__c=eve.Special_dietary_needs__c;
                everegtoupdat.Special_dietary_needs_other__c=eve.Special_dietary_needs_other__c;
                everegtoupdat.Do_you_consent_to_having_your_name_and_e__c=eve.Do_you_consent_to_having_your_name_and_e__c;
                everegtoupdat.What_is_your_primary_reason__c=eve.What_is_your_primary_reason__c;
                everegtoupdat.What_is_your_primary_reason_other__c=eve.What_is_your_primary_reason_other__c;
                everegtoupdat.Curent_Employer__c='Deloitte';
                if(subschedule =='--None--')
                    subschedule=null;
                everegtoupdat.Which_portion_of_the_event__c=subschedule;
                
                System.debug('## Before '+erlist[0].registration_date__c);
                if(erlist[0].registration_date__c ==null)
                    everegtoupdat.registration_date__c= system.today();
                
                System.debug('## Before '+erlist[0].registration_date__c);
                
                update everegtoupdat;
                if(everegtoupdat.not_attending__c==true)
                {
                 attendingno=true;
                 thankyoumess= false;
                 ARpage1= false;
                }
                
                if(everegtoupdat.attending__c==true)
                {
                 attendingno=false;
                 thankyoumess= true;
                 ARpage1= false;
                 sendinvite();
                }
                
                
            }
            else 
            {
                Event_Registration_Attendance__c everegtinsert = new Event_Registration_Attendance__c();
                everegtinsert.Contact__c=AR_con[0].id;
                everegtinsert.Event__c=eid;
                if(EventAttendance == 'Yes')
                {
                    everegtinsert.attending__c=true;
                    everegtinsert.not_attending__c=false;
                }
                if(EventAttendance == 'No')
                {
                    everegtinsert.attending__c=False;
                    everegtinsert.not_attending__c=true;
                }
                everegtinsert.recordtypeid=RecordTypeARid.id;
                everegtinsert.Curent_Employer__c='Deloitte';
                everegtinsert.first_nickname_for_nametag__c=eve.first_nickname_for_nametag__c;
                everegtinsert.Special_dietary_needs__c=eve.Special_dietary_needs__c;
                everegtinsert.Special_dietary_needs_other__c=eve.Special_dietary_needs_other__c;
                everegtinsert.What_is_your_primary_reason__c=eve.What_is_your_primary_reason__c;
                everegtinsert.What_is_your_primary_reason_other__c=eve.What_is_your_primary_reason_other__c;
                everegtinsert.Do_you_consent_to_having_your_name_and_e__c=eve.Do_you_consent_to_having_your_name_and_e__c;
                if(subschedule =='--None--')
                    subschedule=null;
                everegtinsert.Which_portion_of_the_event__c=subschedule;
                
                /*Creating the Event Registration hence can update the registering date witout any check.*/
                everegtinsert.registration_date__c= system.today();
                
                insert everegtinsert;
                if(everegtinsert.not_attending__c==true)
                {
                 attendingno=true;
                 thankyoumess= false;
                 ARpage1= false;
                }
                
                if(everegtinsert.attending__c==true)
                {
                 attendingno=false;
                 thankyoumess= true;
                 ARpage1= false;
                 sendinvite();
                }
                
            }
        }
        
        else if(AR_con.size()==0 ) {ARpage1error= true;} 
        return null;
    }
    
    public list<SelectOption> getARattendance(){
        List<SelectOption> optns = new List<SelectOption>();
        optns.add(new SelectOption('--None--', '--None--'));
        optns.add(new SelectOption('Yes', 'Yes'));
        optns.add(new SelectOption('No','No'));
        return optns;
    }
    
    public boolean displayPopup {get; set;}
    
    public void closePopup() {
        displayPopup = false;
    }
    
    public void showPopup() {
        displayPopup = true;
    }
    public list<SelectOption> getSubscheduledet(){
        List<SelectOption> optns = new List<SelectOption>();
        optns.add(new SelectOption('--None--', '--None--'));
        if(event.Subschedule_1_Title__c<>null)
            optns.add(new SelectOption(event.Subschedule_1_Title__c+ ' ('+event.AR_Event_Option_1_Start_Time__c +' - ' + event.AR_Event_Option_1_End_Time__c+')', event.Subschedule_1_Title__c+ ' ('+event.AR_Event_Option_1_Start_Time__c +' - ' + event.AR_Event_Option_1_End_Time__c+' '+ event.Time_Zone_short_form__c+')'));
        if(event.Subschedule_2_Title__c<>null)
            optns.add(new SelectOption(event.Subschedule_2_Title__c+' ('+event.AR_Event_Option_2_Start_Time__c +' - ' +event.AR_Event_Option_2_End_Time__c+')',event.Subschedule_2_Title__c+' ('+event.AR_Event_Option_2_Start_Time__c +' - ' +event.AR_Event_Option_2_End_Time__c+' '+ event.Time_Zone_short_form__c+')'));
        if(event.Subschedule_3_Title__c<>null)
            optns.add(new SelectOption(event.Subschedule_3_Title__c+ ' ('+event.AR_Event_Option_3_Start_Time__c +' - ' +event.AR_Event_Option_3_End_Time__c+')',event.Subschedule_3_Title__c+ ' ('+event.AR_Event_Option_3_Start_Time__c +' - ' +event.AR_Event_Option_3_End_Time__c+' '+ event.Time_Zone_short_form__c+')'));
        return optns;
    }
    
    public string getDocumentImageUrl()
    {
        Map<String,eEvent_AR_Images__c> allStates = eEvent_AR_Images__c.getAll();
        
        if(event.AR_image_for_registration_page__c<>null)
        {List<Document> lstDocument = [Select Id,Name from Document where id =: allStates.get(event.AR_image_for_registration_page__c).eEvent_AR_Images_ID__c limit 1];
         
         string strOrgId = UserInfo.getOrganizationId();
         string strDocUrl = '/servlet/servlet.ImageServer?oid=' + strOrgId + '&id=';
         return strDocUrl + lstDocument[0].Id;
        }
        
        else{
            string strOrgId = UserInfo.getOrganizationId();
            string strDocUrl = '/servlet/servlet.ImageServer?oid=' + strOrgId + '&id=';
            return strDocUrl + allStates.get('Tree branches growing.jpeg').eEvent_AR_Images_ID__c;}
    }
    
    
    public PageReference sendinvite()  
    {      
        if(eve.What_is_your_primary_reason__c ==null)
            eve.What_is_your_primary_reason__c='';
        if(eve.Special_Dietary_Needs__c ==null)
            eve.Special_Dietary_Needs__c='';
        contact confrmaddree = [select id, email from contact where id=:confromaddress]; 
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'alumni2@deloitte.com'];
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        String[] toAddresses = new String[] {confrmaddree.email}; 
            mail.setToAddresses(toAddresses);
        mail.setSubject(event.name);
        // mail.setHtmlBody('<html><body>This is your registration confirmation for the '+ event.name +' ('+sitestartdate1.format('MMMM dd, YYYY')+' / '+sitestartdate1.format('hh:mm a')+' ('+event.AR_Subschedule_Time_zone__c+')'+' / '+event.NIR_venue_address__c+', '+event.Venue_City_AR__c+', '+event.Venue_state_AR__c+', '+event.Venue_Zip_AR__c+'). <br/><table style=" width:100%;" align="left" ><tr ><td style ="text-align:left; width:50%" vAlign="top"><div></div><b>Personal Profile</b></td></tr><tr ><td style ="text-align:left; width:50%" vAlign="top">First Name </td><td  style ="text-align:left; width:50%;" width="70%" vAlign="top">'+ con.firstname+' </td></tr><tr ><td style ="text-align:left; width:50%;" vAlign="top">Last Name </td><td  style ="text-align:left; width:50%;" vAlign="top">'+ con.lastname +'</td></tr><tr><td style ="text-align:left; width:50%" vAlign="top">Nickname for Nametag </td><td style ="text-align:left; width:50%" vAlign="top">'+  eve.First_Nickname_for_Nametag__c+' </td></tr><tr><td style ="text-align:left; width:50%" vAlign="top">Primary Email </td><td style ="text-align:left; width:50%" vAlign="top">'+con.email+'</td></tr><tr ><td style ="text-align:left; width:50%" width="30%" vAlign="top"><div> </div><b>Additional Information</b></td></tr><tr><td style ="text-align:left; width:50%" vAlign="top">Function </td><td style ="text-align:left; width:50%" vAlign="top"> '+confp+'</td></tr><tr><td style ="text-align:left; width:50%" vAlign="top">Primary reason for attending this event </td><td style ="text-align:left; width:50%" vAlign="top"> '+eve.What_is_your_primary_reason__c+'</td></tr><tr><td style ="text-align:left; width:50%" vAlign="top">Special Dietary Needs </td><td style ="text-align:left; width:50% " vAlign="top">'+eve.Special_Dietary_Needs__c+'</td><td > </td></tr></table></body></html>');
        //Sekhar Updated Text fields based on the discussion with Devarshi
        mail.setHtmlBody('<html><body>This is your registration confirmation for the '+ event.name +' ('+event.AR_Start_Date_Time_Text__c+' - '+event.AR_End_Date_Time_Text__c+' ('+event.Time_Zone__c+')'+' / '+event.NIR_venue_address__c+', '+event.Venue_City_AR__c+', '+event.Venue_state_AR__c+', '+event.Venue_Zip_AR__c+'). <br/><table style=" width:100%;" align="left" ><tr ><td style ="text-align:left; width:50%" vAlign="top"><div></div><b>Personal Profile</b></td></tr><tr ><td style ="text-align:left; width:50%" vAlign="top">First Name </td><td  style ="text-align:left; width:50%;" width="70%" vAlign="top">'+ con.firstname+' </td></tr><tr ><td style ="text-align:left; width:50%;" vAlign="top">Last Name </td><td  style ="text-align:left; width:50%;" vAlign="top">'+ con.lastname +'</td></tr><tr><td style ="text-align:left; width:50%" vAlign="top">Nickname for Nametag </td><td style ="text-align:left; width:50%" vAlign="top">'+  eve.First_Nickname_for_Nametag__c+' </td></tr><tr><td style ="text-align:left; width:50%" vAlign="top">Primary Email </td><td style ="text-align:left; width:50%" vAlign="top">'+con.email+'</td></tr><tr ><td style ="text-align:left; width:50%" width="30%" vAlign="top"><div> </div><b>Additional Information</b></td></tr><tr><td style ="text-align:left; width:50%" vAlign="top">Function </td><td style ="text-align:left; width:50%" vAlign="top"> '+confp+'</td></tr><tr><td style ="text-align:left; width:50%" vAlign="top">Primary reason for attending this event </td><td style ="text-align:left; width:50%" vAlign="top"> '+eve.What_is_your_primary_reason__c+'</td></tr><tr><td style ="text-align:left; width:50%" vAlign="top">Special Dietary Needs </td><td style ="text-align:left; width:50% " vAlign="top">'+eve.Special_Dietary_Needs__c+'</td><td > </td></tr></table></body></html>');
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();   
        attach.filename = 'quarter1meeting.ics'; 
        attach.ContentType = 'text/calendar';     
        attach.inline = true;     
        attach.body = invite();   
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {attach});   
        Messaging.SendEmailResult[] er = Messaging.sendEmail(new Messaging.Email[] { mail });   
        return null;    }
    
    private Blob invite() {
        string eventstartdate= sitestartdate1.formatGMT('yyyyMMdd\'T\'HHmmss\'Z\'');
        string eventenddate= siteenddate1.formatGMT('yyyyMMdd\'T\'HHmmss\'Z\'');
        String txtInvite = '';
        txtInvite += 'BEGIN:VCALENDAR\n';
        txtInvite += 'PRODID:-//Force.com Labs//iCalendar Export//EN\n';
        txtInvite += 'VERSION:2.0\n';
        txtInvite += 'METHOD:REQUEST\n';
        txtInvite += 'X-MS-OLK-FORCEINSPECTOROPEN:TRUE\n';
        txtInvite += 'BEGIN:VEVENT\n';
        txtInvite += 'CLASS:PUBLIC\n';
        txtInvite += 'UID:test@gmail.com\n';//erlist[0].id+'\n';
        txtInvite += 'SEQUENCE:0\n';
        txtInvite += 'ORGANIZER:alumni2@deloitte.com\n';
        txtInvite += 'DTSTAMP:20100804T120000Z\n';
        txtInvite += 'CREATED:20100804T120000Z\n';
        txtInvite += 'DTSTART:'+eventstartdate+'\n';
        txtInvite += 'DTEND:'+eventenddate+'\n';
        txtInvite += 'LOCATION:'+event.nir_Venue__c+', '+event.NIR_venue_address__c+', '+event.Venue_City_AR__c+', '+event.Venue_state_AR__c+', '+event.Venue_Zip_AR__c+'\n';
        txtInvite += 'PRIORITY:5\n';
        txtInvite += 'SUMMARY;';
        txtInvite += 'LANGUAGE=en-us:'+event.name+'\n';
        txtInvite += 'TRANSP:OPAQUE\n';
        txtInvite += 'X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN"><HTML><HEAD><META NAME="Generator" CONTENT="MS Exchange Server version 08.00.0681.000"><TITLE></TITLE></HEAD><BODY><!-- Converted from text/plain format --></BODY></HTML>\n';
        txtInvite += 'X-MICROSOFT-CDO-BUSYSTATUS:BUSY\n';
        txtInvite += 'X-MICROSOFT-CDO-IMPORTANCE:1\n';
        txtInvite += 'END:VEVENT\n';
        txtInvite += 'END:VCALENDAR';
        return Blob.valueOf(txtInvite);
    } 
    
    public PageReference backtocheckin() {
        
        pagereference pg = new pagereference(system.label.AR_eEvents_Internal_Checkin_URL+event.id);
        pg.setredirect(true);
        return pg;}
}