public class CompanyTriggerHandler_AT {
    
    /*
     * @desc This methods assign the company lookup value using the company text value. This is indedted to be use in before insert.
     * @input : List of the companies. 
    */
 public void assignCompanyName(List<Company__c> comtList){
        Map<String,Company__c> cmpNameIdMap = new Map<String,Company__c>();
         set<string> empString = new set<string>();
            for(Company__c com:comtList)
            {
                if(com.MDR_Parent_Company__c  <>null && com.MDR_Parent_Company__c <>'')
                {
                    empString.add(com.MDR_Parent_Company__c);
                    empString.add(com.MDR_Parent_Company__c.toLowerCase());
                    empString.add(com.MDR_Parent_Company__c.toUpperCase());
                }
            }
     
     /*Adding the Unkown & notInList name query parameters*/
     
     empString.add(system.Label.NotInListSchoolName);
     empString.add(system.Label.unknowCompanyName);
     system.debug('%%Company: '+empString);
     for(Company__c cmp:[select id,name,MDR_Client_Number__c,Industry_SIC__c,Region__c,Industry_Sector_Desc__c from Company__c where name in :empString and Parent__c = True and Status__c='Approved' ])
        {
                cmpNameIdMap.put(cmp.Name,cmp);
        }
       
        for(Company__c com:comtList)
        {
            
            Company__c tempCompany= null;
            /*If Parent Company text field is not empty */
            if(com.MDR_Parent_Company__c  <>null && com.MDR_Parent_Company__c <>'')
                {
                    /*If Parent Company Text value has valid company record.*/
                    if(cmpNameIdMap.containsKey(com.MDR_Parent_Company__c))
                    {
                       tempCompany= cmpNameIdMap.get(com.MDR_Parent_Company__c);
                    }
                    else if(cmpNameIdMap.containsKey(com.MDR_Parent_Company__c.tolowercase()))
                    {
                       tempCompany= cmpNameIdMap.get(com.MDR_Parent_Company__c.tolowercase());
                    }
                    else if(cmpNameIdMap.containsKey(com.MDR_Parent_Company__c.touppercase())){
                        
                        tempCompany= cmpNameIdMap.get(com.MDR_Parent_Company__c.touppercase());
                    }
                    else
                    {
                        /*If Parent Company Text value has no  valid company record. so update it with Not In list record.*/
                        if(cmpNameIdMap.get(system.Label.NotInListSchoolName)!=null)
                        {
                         tempCompany= cmpNameIdMap.get(system.Label.NotInListSchoolName);
                        }
                        
                    }
               }
           
            if(tempCompany!=null)
            {
                        com.Parent_Company__c= tempCompany.id;   
            }
            else
            {
                /*This handle the cases such as Parent Company Text empty & Unkown Company is deleted. */
            }
        }
    }
}