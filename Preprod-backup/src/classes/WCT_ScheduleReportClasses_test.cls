@isTest(seealldata = true)
public class WCT_ScheduleReportClasses_test{

    static Report r = [SELECT id,name FROM Report limit 1];
    static Schedule_Report__c sr = new Schedule_Report__c(ReportId__c = r.id,ReportName__c = r.name,ReportTrigger__c = 1);
       
    static testMethod void testSchClass(){
        insert sr; 
        test.starttest();
        IncrementReport ir = new IncrementReport(r.name);
        system.schedule('Report Extract','0 0 2 1 * ?',ir);
        test.stoptest();
    
    }
    
    static testMethod void testCSVStream(){
        test.starttest();
        ReportCSVStream  rcs = new ReportCSVStream ();
        rcs.strRptname = r.id;
        rcs.getCSVStream();
        test.stoptest();
    }
    
}