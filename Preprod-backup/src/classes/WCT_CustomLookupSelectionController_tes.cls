@isTest
private class WCT_CustomLookupSelectionController_tes{

    static testMethod void myUnitTest() {
        
        list<Case> caseList = new list<Case>();
    	Test_Data_Utility.createCase();
    	caseList = [select Id from Case limit 1];
    	test.setCurrentPage(page.WCT_ContactLookupSelectionPage);
    	ApexPages.currentPage().getParameters().put('ObjType', 'contact');
         ApexPages.currentPage().getParameters().put('lksrch', 'a');
         WCT_CustomLookupSelectionController controller1= new  WCT_CustomLookupSelectionController();
        
        ApexPages.currentPage().getParameters().put('ObjType', 'case');
        ApexPages.currentPage().getParameters().put('lksrch', 'a');
        ApexPages.currentPage().getParameters().put('frm','test');
        ApexPages.currentPage().getParameters().put('txt','test');
        
        WCT_CustomLookupSelectionController controller= new  WCT_CustomLookupSelectionController();
        system.assertEquals(controller.sObjectType,'case');
        controller.getFormTag();
        controller.search();
        Contact c= new Contact(LastName='test');
        controller.account= c;
        controller.saveAccount();
        controller.getTextBox();
         
    }
}