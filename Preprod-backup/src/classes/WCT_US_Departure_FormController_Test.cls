@isTest
public class WCT_US_Departure_FormController_Test
{
    public static testmethod void m()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        insert mob;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef; 
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
           datetime startdate=date.Today().adddays(10);
        datetime enddate=date.Today().adddays(-1);
        
        datetime Depdte=date.Today().adddays(-10);
        datetime Arrdte=date.Today().adddays(1);
        Test.starttest();
       
        PageReference pageRef = Page.WCT_US_Departure_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        WCT_US_Departure_FormController controller=new WCT_US_Departure_FormController();
              Document doc= new Document();
            doc.Name='test';
            doc.Body=Blob.valueOf('test');
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert doc;
            
            controller.attachmentHelper.docIdList.add(doc.Id);
            controller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'20'));
       controller.takePTO='Yes';
       
        controller.departureDayUS=string.valueof(Depdte.format('MM/dd/yyyy'));
        controller.arrivalDayUSI=string.valueof(Arrdte.format('MM/dd/yyyy'));
        controller.fromDate=enddate.format('MM/dd/yyyy');
        controller.toDate=startdate.format('MM/dd/yyyy');
      controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
      
    }
   
       public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        insert mob;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef; 
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
                 datetime startdate=date.Today().adddays(10);
        datetime enddate=date.Today().adddays(-1);
        
        datetime Depdte=date.Today().adddays(-10);
        datetime Arrdte=date.Today().adddays(1);
              

      
        
        Test.starttest();
       // String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_US_Departure_Form;
        Test.setCurrentPage(pageRef); 
       // ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_US_Departure_FormController controller=new WCT_US_Departure_FormController();
        WCT_Task_ManageHandler taskInstance=new WCT_Task_ManageHandler();
        GBL_Attachments attachmentHelper = new GBL_Attachments();
       ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_US_Departure_FormController();
         Document doc= new Document();
            doc.Name='test';
            doc.Body=Blob.valueOf('test');
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert doc;
            
            controller.attachmentHelper.docIdList.add(doc.Id);
            controller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'20'));
 
        controller.getAttachmentInfo();
        //controller.init();
           controller.takePTO='No';
       
        controller.departureDayUS=string.valueof(Depdte.format('MM/dd/yyyy'));
        controller.arrivalDayUSI=string.valueof(Arrdte.format('MM/dd/yyyy'));
        controller.fromDate=enddate.format('MM/dd/yyyy');
        controller.toDate=startdate.format('MM/dd/yyyy');
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        controller.supportAreaErrorMesssage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
       // controller.uploadAttachment();
    } 
   
}