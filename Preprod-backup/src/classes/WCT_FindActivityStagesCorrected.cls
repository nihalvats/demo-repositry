/**
    * Class Name  : WCT_FindActivityStagesCorrected
    * Description : This apex class will use to Create cases. 
*/

global class WCT_FindActivityStagesCorrected {
  
   /** 
        Method Name  : findStagingRecords
        Return Type  : Void
        Description  : Find all records which needs to treated as cases.         
    */
  
    WebService static String findStagingRecords(String stagingStatus)
    {
      Set<Id> validatedIds=new Set<Id>();
      List<WCT_Outreach_Activity_Staging_Table__c> ListStageRecords = new List<WCT_Outreach_Activity_Staging_Table__c>();
      String returnStatement=Label.WCT_Message_Processing_Starting;
    for(WCT_Outreach_Activity_Staging_Table__c preBIStageRecord: [Select Id,Name, WCT_Associated_Outreach__c,WCT_Status__c,WCT_Employee__c, WCT_Outreach_Contact_Email__c,
                                                                    WCT_Personnel_Number__c, WCT_Mobile__c, WCT_Phone__c, WCT_Medical_Summary_for_Office_Location__c, WCT_Admission_Date__c,
                                                                    WCT_Insert_Zip_Postal_Code_in_SAP__c, WCT_Insert_Carriers__c, WCT_Insert_Address_in_SAP__c, WCT_Insert_City_in_SAP__c, 
                                                                    WCT_Insert_State_in_SAP__c, WCT_Insert_Date__c, WCT_Effective_Date__c, WCT_Transition_Date__c, WCT_Payroll_Date__c,
                                                                    WCT_Dependent_First_Name__c, WCT_Dependent_Last_Name__c, WCT_Enrollment_Date__c, WCT_Analyst_Name__c,
                                                                    WCT_Analyst_Phone__c, WCT_Deduction_Amount__c, WCT_Deduction_Type__c, WCT_Coverage_Level__c, WCT_Coverage_Name__c,
                                                                    WCT_Year_to_Date_Amount__c, WCT_Coverage_Amount__c, WCT_Date_Mailed__c, WCT_Date_Forms_Due__c,WCT_Insert_Street_Address_Line2_in_SAP__c,
                                                                    WCT_End_Date__c, WCT_Analyst_Email__c, WCT_Enrollment_Deadline__c
                                                                     from WCT_Outreach_Activity_Staging_Table__c where WCT_Status__c = : stagingStatus limit:Limits.getLimitQueryRows()])
    {
      ListStageRecords.add(preBIStageRecord);
    
    }
    
    if(!ListStageRecords.IsEmpty())
      {
        
        try{
         List<WCT_Outreach_Activity_Staging_Table__c> ValidatedListStageRecords = WCT_UploadOutreachActivity.validateStagingRecords(ListStageRecords); 
         update ValidatedListStageRecords ;
         for( WCT_Outreach_Activity_Staging_Table__c test:ValidatedListStageRecords ){
         validatedIds.add(test.id);
         }
         WCT_UploadOutreachActivity.CreateOutreachActivity(validatedIds);
          }Catch(Exception e)
          {
            WCT_ExceptionUtility.logException('WCT_FindActivityStagesCorrected','Create Outreach Activity',e.getMessage()); 
            throw e;
          }
      }
      else
      {
        returnStatement=WCT_UtilConstants.PROCESS_NONE;
      
      }
      return returnStatement;
    
    }

}