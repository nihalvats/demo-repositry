global class GBL_MultiSelectLookup_CTRL {

    public GBL_MultiSelectLookup_CTRL()
    {
        
        
    }
    
     @RemoteAction
    global static  List<WCT_List_Of_Names__c> getItemsList(String fieldName)
    {
        List<WCT_List_Of_Names__c> listItems;
        
        listItems= [Select Id, Name from WCT_List_Of_Names__c where RecordTypeId =:Label.MultiSelectPicklist and MultiSelectIdentifier__c=:fieldName  ORDER BY Name ASC ];
        return listItems;
        
    }
    
     public GBL_MultiSelectLookup_CTRL (ApexPages.StandardController controller) 
     {

 
    }
    
}