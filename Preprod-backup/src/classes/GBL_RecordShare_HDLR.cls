public class GBL_RecordShare_HDLR 
{

    public static boolean shareWithRead(Id SourceId, Id USerId, String ObjectName)
    {
        
       return GBL_RecordShare_HDLR.createShareRecord(SourceId, USerId, ObjectName, 'Read');
       
    }
    
    public static boolean createShareRecord(Id SourceId, Id USerId, String ObjectName, String AccessLevel)
    {
        try
        {
            if(ObjectName!=null && ObjectName!='' )
            {
                String shareObjName='';
                if(ObjectName.contains('__c')||ObjectName.contains('__C'))
                {
                    shareObjName=ObjectName.contains('__c')==true?ObjectName.replace('__c', '__share'):ObjectName.replace('__C', '__share');
                }
                else if(ObjectName.trim()!='')
                {
                   shareObjName= ObjectName+'Share';
                }
                
                if(shareObjName!='')
                {
                      Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
                      Schema.sObjectType sobjType = globalDescription.get('objectAPIName');
                      sObject shareObject=sobjType.newSObject();
                     shareObject.put('UserOrGroupId', USerId);
                    shareObject.put('UserOrGroupId', USerId);
                   
                }
                
            }
            
        }
        catch(Exception e)
        {
            return false;
        }
        return true;
    }
    
}