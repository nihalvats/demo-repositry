public class WCT_BatchToExtendDueDate implements Database.Batchable<sObject>, Database.AllowsCallouts { 
    
    // Relation Records Failure Statuses
    private static final String[] DUEDATE_STATUSES = new String[] {'Created', 'Updated'};    
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        // Pull the relation records with failure statuses
        String query = 'SELECT ' +
                            'Id, SFDC_Task_Id__c, ToD_TODO_Id__c, Status__c, Status_Details__c, WCT_Next_Due_Date__c, UpdatedDate__c ' +
                        'FROM ' +
                            'WCT_ToD_Task_ToDo_Relation__c ' +
                        'WHERE ' +
                            'Status__c in :DUEDATE_STATUSES';
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<WCT_ToD_Task_ToDo_Relation__c> listFailedRecords) { 
        WCT_ToD_Task_ToDo_Relation__c ToDTaskStatus = new WCT_ToD_Task_ToDo_Relation__c();
        for(WCT_ToD_Task_ToDo_Relation__c dueRecords : listFailedRecords){
            if((dueRecords.Status__c == 'Created' || dueRecords.Status__c == 'Updated') && dueRecords.UpdatedDate__c <= System.today().addDays(-20)){
                dueRecords.Status__c = 'Ready for Update';
                dueRecords.WCT_Next_Due_Date__c = System.today().addDays(25);
            }
        }
        update listFailedRecords; 
        
    }
    
    public void finish(Database.BatchableContext BC){    
    }
}