@isTest
private class WCT_GMI_CreateTasks_Test{

    static testmethod void updateImmEmailoncon(){
        WCT_GMI_CreateTasks.doNotRunTaskTrig = false;
        recordtype recType = [select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact assignmentOwner = WCT_UtilTestDataCreation.createEmployee(recType.id);
          assignmentOwner.Fragomen_Email__c = 'test.deloitte.com';
          insert assignmentOwner;
       list<WCT_Immigration_Stagging_Table__c> lst = new list<WCT_Immigration_Stagging_Table__c>();
        WCT_Immigration_Stagging_Table__c immstg = new WCT_Immigration_Stagging_Table__c();
        immstg.WCT_Employee_Email__c = 'test.deloitte.com';  
        immstg.Status__c = 'Corrected';
        immstg.Contact__c = assignmentOwner.id;
        lst.add(immstg);
        insert lst;
       immstg.Status__c = 'Corrected';
       update lst; 
        Test.starttest();
        WCT_GMI_CreateTasks imm = new WCT_GMI_CreateTasks();
        WCT_GMI_CreateTasks.updateImmigrationEmailOnContact(lst);
        assignmentOwner.Fragomen_Email__c = immstg.WCT_Employee_Email__c;
        update assignmentOwner;
        Test.stoptest();
        
    }
    
    static testmethod void createImmigtsk(){
    Date t = system.today();
    WCT_GMI_CreateTasks.doNotRunTaskTrig = false;
    WCT_Task_Reference_Table__c  taskRefParent = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
          taskRefParent.WCT_Status__c = 'Visa Approved';
          taskRefParent.WCT_Assigned_to__c='Employee';
          taskRefParent.WCT_Criteria_Of_Due_Date__c= string.valueof(t);
          taskRefParent.WCT_Visa_Type__c = 'H1 Visa';
          insert taskRefParent;
    
    recordtype recType = [select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact assignmentOwner = WCT_UtilTestDataCreation.createEmployee(recType.id);
          insert assignmentOwner;
    Contact rm = WCT_UtilTestDataCreation.createEmployee(recType.id);
          insert rm;
    ID immigRecType = [SELECT Id,name FROM RecordType WHERE NAME = 'H1 Visa' and SobjectType = 'WCT_Immigration__c'].ID;
    ID taskRecType = [SELECT Id,name FROM RecordType WHERE NAME  = 'Immigration' and SobjectType = 'Task'].ID;
    WCT_Immigration__c  immigRec = WCT_UtilTestDataCreation.createImmigration(assignmentOwner.id);
    immigRec.WCT_Immigration_Status__c = 'Petition Collected';  
    immigRec.H1B_Is_USI_Upload__c = true;
    immigRec.RecordTypeId =immigRecType;
    immigRec.WCT_Resource_Manager__c = rm.id;
    immigRec.WCT_Visa_Interview_Date__c = t;
    insert immigRec;
   immigRec.WCT_Immigration_Status__c = 'Visa Approved';
   update  immigRec;
  
  taskRefParent.WCT_Task_for_Object__c = immigRec.id;  
  update taskRefParent;
    Task taskRec = WCT_UtilTestDataCreation.createTaskwithRef(immigRec.id,taskRefParent.id);
          taskRec.RecordTypeId = taskRecType;
          taskRec.whatid = immigRec.id;
          taskRec.task_type__c = 'H1 Visa';
          taskRec.whoId = assignmentOwner.id;
          insert taskRec;
    
    Test.Starttest();
   WCT_GMI_CreateTasks imm = new WCT_GMI_CreateTasks(); 
    Test.StopTest();
    
    }

    static testmethod void newvis(){
    
    recordtype recType = [select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact assignmentOwner = WCT_UtilTestDataCreation.createEmployee(recType.id);
          assignmentOwner.Email = 'test@deloitte.com';
         assignmentOwner.WCT_Green_Card_Status__c = 'Active';
          insert assignmentOwner;
    ID immigRecType = [SELECT Id,name FROM RecordType WHERE NAME = 'H1 Visa' and SobjectType = 'WCT_Immigration__c'].ID;
    ID taskRecType = [SELECT Id,name FROM RecordType WHERE NAME  = 'Immigration' and SobjectType = 'Task'].ID;
    ID mobRecType = [SELECT Id,name FROM RecordType WHERE NAME  = 'Business Visa' and SobjectType = 'WCT_Mobility__c'].ID;
        ID taskRecType1 = [SELECT Id,name FROM RecordType WHERE NAME  = 'Mobility' and SobjectType = 'Task'].ID;
    WCT_Immigration__c  immigRec = WCT_UtilTestDataCreation.createImmigration(assignmentOwner.id);
    immigRec.WCT_Immigration_Status__c = 'Petition Collected';  
    immigRec.H1B_Is_USI_Upload__c = true;
    immigRec.RecordTypeId =immigRecType;
      
    insert immigRec;    
    WCT_Task_Reference_Table__c  taskRefParent = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
        taskRefParent.WCT_Status__c = 'New';
        taskRefParent.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRefParent.WCT_Visa_Type__c = 'Business Visa';
        insert taskRefParent;
      
    WCT_Mobility__c mob = new WCT_Mobility__c ();
    mob.Immigration__c = immigRec.id;
    mob.WCT_Mobility_Employee__c = assignmentOwner.id;
    mob.RecordTypeId = mobRecType;
    mob.WCT_Mobility_Status__c = 'New';
    insert mob;
    
    Task taskRec = WCT_UtilTestDataCreation.createTaskwithRef(mob.id,taskRefParent.id);
        taskRec.RecordTypeId = taskRecType1;
        insert taskRec;
      
      Test.Starttest();
      WCT_GMI_CreateTasks gt = new WCT_GMI_CreateTasks();
      update mob;
      Test.StopTest();  
    
    }
    
  static testmethod void newLCA(){
    
    recordtype recType = [select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact assignmentOwner = WCT_UtilTestDataCreation.createEmployee(recType.id);
          assignmentOwner.Email = 'test@deloitte.com';
          insert assignmentOwner;  
          
 ID immigRecType = [SELECT Id,name FROM RecordType WHERE NAME = 'H1 Visa' and SobjectType = 'WCT_Immigration__c'].ID; 
 ID lcaRecType = [SELECT Id,name FROM RecordType WHERE NAME = 'LCA' and SobjectType = 'Task'].ID;  
 ID lcaRecTy = [SELECT Id,name FROM RecordType WHERE NAME = 'LCA Record type' and SobjectType = 'WCT_LCA__c'].ID;  
 
WCT_Immigration__c  immigRec = WCT_UtilTestDataCreation.createImmigration(assignmentOwner.id);
    immigRec.WCT_Immigration_Status__c = 'Petition Collected';  
    immigRec.H1B_Is_USI_Upload__c = true;
    immigRec.RecordTypeId =immigRecType;
      
    insert immigRec;   
  
  WCT_Task_Reference_Table__c  taskRefParent = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
        taskRefParent.WCT_Status__c = 'New';
        taskRefParent.WCT_Task_for_Object__c = 'WCT_LCA__c';
        taskRefParent.WCT_Visa_Type__c = 'LCA Record type';
        taskRefParent.WCT_Assigned_to__c='Employee';
        insert taskRefParent;
    
    WCT_LCA__c lcare = new WCT_LCA__c();
    lcare.WCT_Assignment_Owner__c =  assignmentOwner.id;
    lcare.WCT_Immigration__c = immigRec.id;
    lcare.WCT_Status__c = 'New'; 
    lcare.RecordTypeId = lcaRecTy;
    insert lcare;
    
 Task taskRec = WCT_UtilTestDataCreation.createTaskwithRef(lcare.id,taskRefParent.id);
        taskRec.RecordTypeId = lcaRecType;
        taskRec.whoid = assignmentOwner.id;
        taskRec.whatid = lcare.id;
        taskRec.task_type__c = 'LCA Approved';
        insert taskRec;   
    Test.StartTest();
    WCT_GMI_CreateTasks gt = new WCT_GMI_CreateTasks();
    update lcare;
    Test.StopTest();
    }

    
}