@isTest
private class Test_AR_Event_rsvplinks
{
static testMethod void AR_Eventcheckin(){

    Contact conar = new Contact();
     conar.LastName = 'abcAR_Eventcheckin';
     conar.FirstName = 'xyzAR_Eventcheckin';
     conar.Email = 'abcAR_Eventcheckin@invalid.com';
     conar.Teams__c = 'Campus';
     conar.WCT_Candidate_School__c =  'School of Arts';
     conar.Degree_program__c = 'BA';
     insert conar;
    Contact conarint = new Contact();
     conarint.LastName = 'abcAR_Eventcheckinint';
     conarint.FirstName = 'xyzAR_Eventcheckinint';
     conarint.Email = 'abcAR_Eventcheckinint@invalid.com';
     conarint.Teams__c = 'Campus';
     conarint.WCT_Candidate_School__c =  'School of Arts';
     conarint.Degree_program__c = 'BA';
     insert conarint;   
     
     Contact conarints = new Contact();
     conarints.LastName = 'abcAR_Eventcheckinint';
     conarints.FirstName = 'xyzAR_Eventcheckinint';
     conarints.Email = 'abcAR_Eventcheckinint@invalid.com';
     conarints.Teams__c = 'Campus';
     conarints.WCT_Candidate_School__c =  'School of Arts';
     conarints.Degree_program__c = 'BA';
     conarints.recordTypeId= Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
     insert conarints; 
     

    Account acc=new Account(name='test321AReventcheckin');                  
     insert acc;
    School__c school = new School__c(name = 'test321AReventcheckin');
     insert school;
    AR_timezones__c setting = new AR_timezones__c();
     setting.Name = 'Central';
     setting.hours__c = 8.0;
     insert setting;
    Event__c event = new Event__c();
     event.School__c = school.id;
     event.Name = 'Test eventssss1 AReventcheckin';
     event.Event_Cap__c = 1000;
     event.Account__c = acc.id;
     event.Start_Date_Time__c=system.now();
     event.end_date_time__c=system.now();
     event.AR_image_for_registration_page__c='Chairs';
     event.Subschedule_1_Title__c='ARTESTSCH1';
     event.Subschedule_1_Start_Time__c=system.now();
     event.Subschedule_1_End_Time__c=system.now();
     event.Subschedule_2_Title__c='ARTESTSCH1';
     event.Subschedule_2_Start_Time__c=system.now();
     event.Subschedule_2_End_Time__c=system.now();
     event.Subschedule_3_Title__c='ARTESTSCH1';
     event.Subschedule_3_Start_Time__c=system.now();
     event.Subschedule_3_End_Time__c=system.now();
     event.AR_RSVP_Deadline__c=system.now();
     event.AR_Attire__c='dat';
     event.AR_Start_Date_Time_Text__c='07/07/2015 19:00:00';
     event.AR_end_Date_Time_Text__c='07/07/2015 19:00:00';
     event.time_zone__c='Central';
     event.PPD_Sponsor__c=conarints.id;
     insert event;
     
     
    String strRecordTypeId = [Select Id From RecordType Where SobjectType = 'Company__c' and Name = 'client'].Id;
    Company__c com= new Company__c();
     com.recordtypeid=strRecordTypeId;
     com.DUNS_Number__c='997655321';
     com.name='XXXYYYZZZ';
     insert com;
     
    Event_Registration_Attendance__c ObjEventReg = new  Event_Registration_Attendance__c();
     ObjEventReg.Contact__c = conar.id;
     ObjEventReg.Event__c = event.id;       
     ObjEventReg.Attended__C = false;    
     ObjEventReg.Attending__C = true;  
     ObjEventReg.Attendee_Type__c = 'Alumni';
     ObjEventReg.invited__C=true;
     ObjEventReg.Curent_Employer__c=com.id;
     insert ObjEventReg;
  List<Event_Registration_Attendance__c>  lers = new list<Event_Registration_Attendance__c>();
  lers.add(ObjEventReg);      
    document d =new document();
     Blob DocBody = Blob.valueof('Test23456 test321AReventcheckin.');
     d.name='ARIMAGEDOC test321AReventcheckin';
     d.body=docbody;
     d.FolderId=Label.Attachment_Zip_Document_Folder_Id;
     insert d;
     
     eEvent_AR_FSS__c arfss= new eEvent_AR_FSS__c();
     arfss.name='Audit';
     insert arfss;
     
    eEvent_AR_Images__c ARimages= new eEvent_AR_Images__c();
     ARimages.name= 'Chairs';
     ARimages.eEvent_AR_Images_ID__c=d.id;
     insert Arimages;

/////////////////////////AR event checkin /////////////////////////////////////////////////////////// 
   PageReference pageRef = Page.Wct_eEvents_AR_Eventcheckin;
     Test.setCurrentPage(pageRef);
     ApexPages.currentPage().getParameters().put('eid',string.valueof(event.id));
     Wct_eEvents_AR_Eventcheckin ARL= new Wct_eEvents_AR_Eventcheckin(); 
     ARL.backb();
     ARL.searchlastname=conar.LastName;
     ARL.ers();
     ARL.getERs();
     ARL.getSelected();
     ARL.GetSelectedErs();
     ARL.getDocumentImageUrl();
     
/////////////////////////RSVP External ///////////////////////////////////////////////////////////        
   PageReference pageRefe = Page.checkinAR;
     Test.setCurrentPage(pageRefe);
     ApexPages.currentPage().getParameters().put('eid',string.valueof(event.id));       
     wct_checkinAR ARe= new wct_checkinAR();
     are.sitestartdate1=system.now(); 
     are.siteenddate1=system.now();
     are.searchlastname='abcAR_Eventcheckin';
     are.searchemail='abcAR_Eventcheckin@invalid.com';
     are.targetfield=ObjEventReg.Curent_Employer__c;
     are.search();
     are.AR_event_insert_or_update();
     are.getARattendance();
     are.getSubscheduledet();
     are.closePopup();
     are.showPopup();
     are.getDocumentImageUrl();
     are.sendregdetails();
     wct_checkinAR.populateImageURL(lers);
   //are.backtocheckin();
/////////////////////////Arlist page///////////////////////////////////////////////////////////////////
PageReference pageRefar = Page.AR_listofattending;
        Test.setCurrentPage(pageRefar);
        ApexPages.currentPage().getParameters().put('eid',string.valueof(event.id));
        Ar_listofattending ARLi= new Ar_listofattending(); 
       ARLi.getlstatt();
       ARLi.getDocumentImageUrl();
     
/////////////////////////Rsvp Internal Else ///////////////////////////////////////////////////////////        
    PageReference pageRefeint = Page.checkinAR;
     Test.setCurrentPage(pageRefeint);
     ApexPages.currentPage().getParameters().put('eid',string.valueof(event.id));       
     wct_checkinARinternal ARint= new wct_checkinARinternal();
     arint.con=conarint;
     arint.AR_event_insert_or_update();
     arint.getnirfss();
     arint.getARattendance();
     arint.getSubscheduledet();
     arint.closePopup();
     arint.showPopup();
     arint.getDocumentImageUrl();   
       
}
/////////////////////////RSVP Internal ///////////////////////////////////////////////////////////        
 public static testMethod void AR_Eventcheckininternal(){

    Contact conarint = new Contact();
     conarint.LastName = 'abcAR_Eventcheckinint';
     conarint.FirstName = 'xyzAR_Eventcheckinint';
     conarint.Email = 'abcAR_Eventcheckinint@invalid.com';
     conarint.Teams__c = 'Campus';
     conarint.WCT_Candidate_School__c =  'School of Arts';
     conarint.Degree_program__c = 'BA';
     conarint.WCT_Employee_Status__c = 'Active';
     insert conarint;
     
     Contact conarints = new Contact();
     conarints.LastName = 'abcAR_Eventcheckinint';
     conarints.FirstName = 'xyzAR_Eventcheckinint';
     conarints.Email = 'abcAR_Eventcheckinint@invalid.com';
     conarints.Teams__c = 'Campus';
     conarints.WCT_Candidate_School__c =  'School of Arts';
     conarints.Degree_program__c = 'BA';
     conarints.recordTypeId= Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
     insert conarints; 
     

     eEvent_AR_FSS__c arfss= new eEvent_AR_FSS__c();
     arfss.name='Audit';
     insert arfss; 
     
    Account accint=new Account(name='test321AReventcheckinint');                  
     insert accint;
    School__c schoolint = new School__c(name = 'test321AReventcheckinint');
     insert schoolint;
     AR_timezones__c settingint = new AR_timezones__c();
     settingint.Name = 'Central';
     settingint.hours__c = 8.0;
     insert settingint;
    Event__c eventint = new Event__c();
     eventint.School__c = schoolint.id;
     eventint.Name = 'Test eventssss1 AReventcheckinint';
     eventint.Event_Cap__c = 1000;
     eventint.Account__c = accint.id;
     eventint.Start_Date_Time__c=system.now();
     eventint.end_date_time__c=system.now();
     eventint.AR_image_for_registration_page__c='Chairs';
     eventint.Subschedule_1_Title__c='ARTESTSCH1';
     eventint.Subschedule_1_Start_Time__c=system.now();
     eventint.Subschedule_1_End_Time__c=system.now();
     eventint.Subschedule_2_Title__c='ARTESTSCH1';
     eventint.Subschedule_2_Start_Time__c=system.now();
     eventint.Subschedule_2_End_Time__c=system.now();
     eventint.Subschedule_3_Title__c='ARTESTSCH1';
     eventint.Subschedule_3_Start_Time__c=system.now();
     eventint.Subschedule_3_End_Time__c=system.now();
     eventint.AR_RSVP_Deadline__c=system.now();
     eventint.AR_Attire__c='dat';
     eventint.AR_Start_Date_Time_Text__c='07/07/2015 19:00:00';
     eventint.AR_end_Date_Time_Text__c='07/07/2015 19:00:00';
     eventint.time_zone__c='Central';
     eventint.PPD_Sponsor__c=conarints.id;
     insert eventint;
     
     
    String strRecordTypeIdint = [Select Id From RecordType Where SobjectType = 'Company__c' and Name = 'client'].Id;
    Company__c comint= new Company__c();
     comint.recordtypeid=strRecordTypeIdint;
     comint.DUNS_Number__c='997655321';
     comint.name='XXXYYYZZZint';
     insert comint;
     
    Event_Registration_Attendance__c ObjEventRegint = new  Event_Registration_Attendance__c();
     ObjEventRegint.Contact__c = conarint.id;
     ObjEventRegint.Event__c = eventint.id;       
     ObjEventRegint.Attended__C = false;    
     ObjEventRegint.Attending__C = true;  
     ObjEventRegint.Attendee_Type__c = 'Alumni';
     ObjEventRegint.invited__C=true;
     ObjEventRegint.Curent_Employer__c=comint.id;
     insert ObjEventRegint;
  List<Event_Registration_Attendance__c>  lers = new list<Event_Registration_Attendance__c>();
  lers.add(ObjEventRegint);      
    document d =new document();
     Blob DocBody = Blob.valueof('Test23456 test321AReventcheckin.');
     d.name='ARIMAGEDOC test321AReventcheckin';
     d.body=docbody;
     d.FolderId=Label.Attachment_Zip_Document_Folder_Id;
     insert d;
     
    eEvent_AR_Images__c ARimages= new eEvent_AR_Images__c();
     ARimages.name= 'Chairs';
     ARimages.eEvent_AR_Images_ID__c=d.id;
     insert Arimages;
  PageReference pageRefei = Page.checkinAR;
     Test.setCurrentPage(pageRefei);
     ApexPages.currentPage().getParameters().put('eid',string.valueof(eventint.id));       
     wct_checkinARinternal ARi= new wct_checkinARinternal();
     ari.con=conarint;
     ari.AR_event_insert_or_update();
     ari.getnirfss();
     ari.getARattendance();
     ari.getSubscheduledet();
     ari.closePopup();
     ari.showPopup();
     ari.getDocumentImageUrl(); 
     //ari.backtocheckin();
     }
}