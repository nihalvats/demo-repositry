/********************************************************************************************************************************
Apex class         : <WCT_No_Input_FormController>
Description        : <Controller which allows to Update Task and Upload Attachments>
Type               :  Controller
Test Class         : <WCT_No_Input_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 23/05/2016          81%                          --                            License Cleanup Project
************************************************************************************************************************************/   

public class WCT_No_Input_FormController{
    public task taskRecord{get;set;}
    public WCT_Task_Reference_Table__c taskRefRecord{get;set;}
    public String taskSubject {get;set;}
    public String taskid{get;set;}
    public boolean display{get;set;}
    public String taskVerbiage{get;set;}
    
    public WCT_No_Input_FormController()
    {
        taskid = ApexPages.currentPage().getParameters().get('taskid');
        display=true;
        taskSubject = '';
        taskVerbiage = '';
    }

/********************************************************************************************
*Method Name         : <updateTaskFlags()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <updateTaskFlags() Used to update TaskFlags in task Object>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 
    public pageReference updateTaskFlags()
    {
     try{
        if(taskid==''|| taskid==null){
            display=false;
            return null;
        }

        taskRecord=[SELECT id, Subject,Ownerid, Status,WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c, WCT_Task_Reference_Table_ID__c FROM Task WHERE Id =: taskid];
        taskRefRecord = [SELECT Id, Name, WCT_Task_Subject__c, Form_Verbiage__c, ownerId FROM WCT_Task_Reference_Table__c Where id=:taskRecord.WCT_Task_Reference_Table_ID__c];

        if(taskRecord != null){

            //Get Task Subject
            
            taskSubject = taskRecord.Subject;
            taskVerbiage = taskRefRecord.Form_Verbiage__c;
            

        }
        
    }
    catch (Exception e) {
        Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_No_Input_FormController', 'No Input Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
        Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_NOINP_EXP&expCode='+errLog.Name);
        pg.setRedirect(true);
        return pg; 
    }
        return null;
    }
}