public with sharing class WCT_H1BCAP_TriggerHandler {
  
    //static method
  public static List<WCT_H1BCAP__c> sendEmail(List<WCT_H1BCAP__c> h1bcapList) {
  string orgmail = Label.WCT_H1BCAP_Mailbox;
        
  //query on template object
   
   OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
   Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'WCT_Action_Required_H1BCAP_Nomination_Professional' AND IsActive = true];
        //list of emails
   List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
   list<String> ToCcAddress =  new List<String>();
   List<String> getContact =  new List<String>();
 /*
 //storing the id's of RM  
   Map<id,WCT_H1BCAP__c> MapRMrecord = new Map<id,WCT_H1BCAP__c>();
     list<WCT_H1BCAP__c> ls = new list<WCT_H1BCAP__c>(); 
   List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();   
  for(WCT_H1BCAP__c w : h1bcapList){
    MapRMrecord.put(w.WCT_H1BCAP_RM_Name__c, w); 
   }
   
   for(id key: MapRMrecord.keyset())
    {
          for(WCT_H1BCAP__c h1 :MapRMrecord.values() )
          
        {
           
           System.debug('H1bcap value *******'+h1); 
                   ls.add(h1);
             
        }
      
        Messaging.SingleEmailMessage singleMail1 = new Messaging.SingleEmailMessage();
     
        singleMail1.setTargetObjectId(key);
        singleMail1.setSaveAsActivity(false);
        singleMail1.setTemplateId(et.Id);
        singleMail1.setWhatid(ls[0].id);
        singleMail1.setOrgWideEmailAddressId(owe.id);
    
      mailList.add(singleMail1);
     
   }
   if(mailList.size() >0){
    
    Messaging.sendEmail(emails);
    return h1bcapList;
   }   
 */      
   
 //My code ended by here
   
    string recordid;
      List<Contact> getContactlist =  new List<Contact>();
        //loop
   for(WCT_H1BCAP__c capList : h1bcapList){
      
         if(capList.WCT_H1BCAP_Email_ID__c != null && capList.WCT_H1BCAP_Practitioner_Personal_Number__c !='' )
        {
       getContact.add(capList.WCT_H1BCAP_Practitioner_Personal_Number__c);
       
        system.debug('per no::'+capList.WCT_H1BCAP_Practitioner_Personal_Number__c);
      
        system.debug('contact id::'+recordid);
        Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
         ToCcAddress.add(capList.WCT_H1BCAP_Resource_Manager_Email_ID__c);
        // singleMail.setTargetObjectId('00354000001QfPWAA0');
        singleMail.setTargetObjectId(capList.WCT_Contact_Record_ID__c);
     //singleMail.setTargetObjectId(capList.id);
           singleMail.setSaveAsActivity(false);
      singleMail.setTemplateId(et.Id);
      singleMail.setWhatid(capList.id);
      singleMail.setCcAddresses(ToCcAddress);
      singleMail.setOrgWideEmailAddressId(owe.id);
    
      emails.add(singleMail);
     System.debug('emaillist::'+emails);

 
 

 }


            }
        
  Messaging.sendEmail(emails);
      
       return h1bcapList;
    }
    
  /*  
   
   public static void  sendNotificationtoSLL(List<WCT_H1BCAP__c> h1bcapList){
    
    system.debug('Entering the loop*********'+h1bcapList);
    Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'Notification_to_SLL_for_Nomination' AND IsActive = true];
    string orgmail = 'replicastatelabor@gmail.com';
        
    OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
    List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
   list<String> ToCcAddress =  new List<String>();
        for(WCT_H1BCAP__c capList : h1bcapList){
   
         if(capList.WCT_H1BCAP_Notification_sent_to_SLL__c == true && capList.id <> null && capList.WC_H1BCAP_USI_SLL_email_id__c <> null && capList.WCT_H1BCAP_US_PPD_mail_Id__c<> null)
        {
       
       system.debug('Entering the loop*********');
       system.debug('SLL ID*********' +capList.WCT_H1BCAP_USI_SLL_Name__c);
        Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
        ToCcAddress.add(capList.WCT_H1BCAP_Resource_Manager_Email_ID__c);
        ToCcAddress.add(capList.WCT_H1BCAP_US_PPD_mail_Id__c);
        singleMail.setTargetObjectId(capList.WCT_H1BCAP_USI_SLL_Name__c);
        singleMail.setSaveAsActivity(false);
        singleMail.setTemplateId(et.Id);
        singleMail.setWhatid(capList.id);
        singleMail.setCcAddresses(ToCcAddress);
        singleMail.setOrgWideEmailAddressId(owe.id);
    
      emails.add(singleMail);
     System.debug('emaillist::'+emails);
    
    //Clear the Cc address for not repeating again
    ToCcAddress.clear();
    capList.WCT_H1BCAP_Notification_sent_to_SLL__c = false;
    
    }
    
  }
  if(emails.size()>0){
    Messaging.sendEmail(emails);      
    }
 }    
   */  
 
   public static void  sendNotificationtoRM_MissingDetails(List<WCT_H1BCAP__c> h1bcapList){
    
    system.debug('Entering the loop*********'+h1bcapList);
    Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'Notification_to_RM_MissingDetails' AND IsActive = true];
    string orgmail = Label.WCT_H1BCAP_Mailbox;
        
    OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
    List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
   list<String> ToCcAddress =  new List<String>();
        for(WCT_H1BCAP__c capList : h1bcapList){
   
         if(capList.WCT_H1BCAP_Flagged_Off_by_GMI__c && capList.WCT_H1BCAP_RM_Name__c <> null && capList.WCT_H1BCAP_Noti_sent_to_RM_Misseddetails__c )
        {
       
       system.debug('RecordId*********'+capList.id);
    
        Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
      //  ToCcAddress.add(capList.WCT_H1BCAP_Resource_Manager_Email_ID__c);
        singleMail.setTargetObjectId(capList.WCT_H1BCAP_RM_Name__c);
        singleMail.setSaveAsActivity(false);
        singleMail.setTemplateId(et.Id);
        singleMail.setWhatid(capList.id);
       // singleMail.setCcAddresses(ToCcAddress);
        singleMail.setOrgWideEmailAddressId(owe.id);
    
      emails.add(singleMail);
     System.debug('emaillist::'+emails);
    
    
    capList.WCT_H1BCAP_Noti_sent_to_RM_Misseddetails__c = false;
    }
    
    
  }
  if(emails.size()>0){
    Messaging.sendEmail(emails);      
    }
 } 
    
    

    
    
}