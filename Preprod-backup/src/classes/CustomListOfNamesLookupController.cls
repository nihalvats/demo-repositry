public with sharing class CustomListOfNamesLookupController {
    
 // public Account account {get;set;} // new account to create
  public List<WCT_List_Of_Names__c> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
    public string label{get; set;}  

    public List<string> filterQuery{get; set;}  
  public CustomListOfNamesLookupController() {
      results= new List<WCT_List_Of_Names__c>();
      filterQuery= new List<string>();
    //account = new Account();
    // get the current search string
    searchString = System.currentPageReference().getParameters().get('lksrch');
         JSONParser parser;
      parser = JSON.createParser(System.currentPageReference().getParameters().get('serachFilters'));
      
      /*Advance Next Token needed*/
      parser.nextToken();
      /*To skip the null for new Interview */
      parser.nextToken();
      
     while(parser.nextToken()!=null)
     {
         filterQuery.add(parser.getText());
     }

      
         //filterQuery.add(parser.getText()+'123'+System.currentPageReference().getParameters().get('serachFilters'));
           
       
     // label='Recently viewed :';
    runSearch();
  }
   
  // performs the keyword search
  public PageReference search() {
    runSearch();

    return null;
  }
  
  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
        if(searchString != '' && searchString != null)
        {
            results = performSearch(searchString); 
    }else
    {
           results = recentQuery();
    }              
  } 
  
  // run the search and return the records found. 
  private List<WCT_List_Of_Names__c> performSearch(string searchString) {
 
       label='Search Results :';
    String soql = 'select id,WCT_Record_Name__c,WCT_FSS__c,WCT_Job_Type__c,WCT_User_Group__c, name, WCT_Blank_Document_URL__c from WCT_List_Of_Names__c';
    if(searchString != '' && searchString != null)
      soql = soql +  ' where name LIKE \'%' + searchString +'%\'';
      soql = soql +  ' and WCT_FSS__c =\'' + filterQuery[0]+'\'';
      soql = soql +  ' and WCT_User_Group__c =\'' + filterQuery[1]+'\'';
      soql = soql +  ' and WCT_Job_Type__c =\'' + filterQuery[2]+'\'';
      
    soql = soql + ' limit 25';
    System.debug(soql);
    return database.query(soql); 
 
  }
    
 private List<WCT_List_Of_Names__c> recentQuery() {
 
       label='Recently Viewed :';
       
    String soql = 'Select id,WCT_Record_Name__c,WCT_FSS__c,WCT_Job_Type__c,WCT_User_Group__c, name, WCT_Blank_Document_URL__c From WCT_List_Of_Names__c  where LastViewedDate!=null' ;
    soql = soql +  ' and WCT_User_Group__c =\'' + filterQuery[0]+'\'';
      soql = soql +  ' and WCT_Job_Type__c =\'' + filterQuery[1]+'\'';
      soql = soql +  ' and WCT_FSS__c =\'' + filterQuery[2]+'\'';
    soql = soql + ' order by  LastViewedDate desc limit 10';
    return database.query(soql); 
   }
    

  
  // save the new account record
 /* public PageReference saveAccount() {
    insert account;
    // reset the account
    account = new Account();
    return null;
  }*/
  
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
    
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }
 
}