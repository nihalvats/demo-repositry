global class  WCT_H1bcap_SM_Notification Implements Database.Batchable <sObject> {
    global Database.queryLocator start(Database.BatchableContext bc) {
        
    Date d= system.today();
            
        String SOQL = 'SELECT Id,WCT_H1BCAP_Status__c,WCT_H1BCAP_Resource_Manager_Email_ID__c,WC_H1BCAP_USI_SLL_email_id__c,WCT_H1BCAP_USI_SM_Name_GDM__c FROM WCT_H1BCAP__c WHERE WCT_H1BCAP_Status__c =  \'Willing To Travel\' AND (WCT_H1BCAP_After7thday__c =:d)' ;
    
        return Database.getQueryLocator(SOQL);
       
    }

    global void execute(Database.BatchableContext bc, List<WCT_H1BCAP__c> H1bcap) {
    
       
        string orgmail =  Label.WCT_H1BCAP_Mailbox;
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        
        OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
       Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'Notification_to_USI_SM_US_PPD' AND IsActive = true];
    
       Set<id> SMids = new set<id> ();
       Map<id,WCT_H1BCAP__c> Maprecord = new Map<id,WCT_H1BCAP__c>();
       System.debug('Start the execute*******'+H1bcap.size());   
      
       for(WCT_H1BCAP__c w: H1bcap) 
  {
       System.debug('Entering ind loop*******');   
          if(w.WCT_H1BCAP_Status__c  == 'Willing To Travel') 
     {    
          System.debug('Entering ind loop2*******'); 
            
          w.WCT_H1BCAP_Status__c = 'Ready for USI SM / US PPD approval';  
          w.WCT_H1BCAP_Capture_BeginingDate_SM__c = system.today(); // capturing SM action date
     }         
     
   }    
           
         for(WCT_H1BCAP__c w: H1bcap) 
     {
       System.debug('Entering ind loop*******');   
          
       SMids.add(w.WCT_H1BCAP_USI_SM_Name_GDM__c);
        Maprecord.put(w.WCT_H1BCAP_USI_SM_Name_GDM__c, w); 
    }
    
    System.debug('loop outside*******'+SMids);
    System.debug('Map Data*******'+Maprecord);
    System.debug('Map values*******'+Maprecord.values());
    System.debug('Map values size*******'+Maprecord.values().size());
    System.debug('Map Key*******'+Maprecord.keyset());
    System.debug('Map Key size*******'+Maprecord.keyset().size());
   list<WCT_H1BCAP__c> ls = new list<WCT_H1BCAP__c>();
    
           for(id key: Maprecord.keyset())
    {
          for(WCT_H1BCAP__c h1 :Maprecord.values() )
          
        {
           
       ls.add(h1);
        }  
           
           system.debug('%%%%%%%%%'+ls.size());
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
            mail.SetTemplateid(et.id);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(key);
        //   mail.setCcAddresses(s);
            mail.setWhatid(ls[0].id);
            mail.setOrgWideEmailAddressId(owe.id);
           
           system.debug('IDDDDDDDDDDD*******' +owe.id);
           system.debug('IDDDDDDDDDDD*******' +et.id);
           mailList.add(mail); 
           ls.clear();  
           system.debug('length of Maillist*******'+mailList.size()); 
       }
        if(mailList.size() >0)
      {  
         try{
             update H1bcap;
              System.debug('Mail Has been sent*******');
             Messaging.sendEmail(mailList); 
             }   
          catch(Exception npe) {
          System.debug('The following exception has occurred: ' + npe.getMessage());
             }          
      } 
      
    }
    
     global void finish(Database.BatchableContext bc) {
    }
  }