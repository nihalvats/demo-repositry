@isTest
public class Wct_OfferPendingforAnalyst_Test{
        
        
    static testMethod void Wct_OfferPendingforAnalyst_TestMethod1 (){
     Profile prof = [select id from profile where name='system Administrator'];
     User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
    
       Contact con = new Contact ();
       con.LastName='tec';
       con.Email='user@gmail.com';
       con.WCT_Internship_Start_Date__c = date.today()-1;
       Insert con;
       system.debug('con values ----'+con);
       
        string rc1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Campus Hire Offer').getRecordTypeId();
        
        BusinessHours bhs=[select id from BusinessHours where IsDefault=true];
        integer mins = 358;
       
        List<WCT_Offer__c> lstOffer = new List<WCT_Offer__c>(); 
        WCT_Offer__c  objCusWct= new WCT_Offer__c();
        
        objCusWct.RecordTypeId = rc1;
        //objCusWct.recordtype = rc1.id;
        objCusWct.WCT_status__c = 'Draft in Progress';
        objCusWct.WCT_Candidate__c = con.Id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.OwnerId = usr.id;
        
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.WCT_Type_of_Hire__c = 'Full Time';
        
        objCusWct.WCT_AgentState_Time__c = Date.today();
        objCusWct.WCT_Basic__c = '200';
        objCusWct.WCT_Annual_LTA__c='200';
        objCusWct.WCT_Annual_Provident_Fund__c='1200';
        objCusWct.WCT_Annual_Spcl_Allowance__c='800';
        objCusWct.WCT_Lookup_Code__c='1442';
        objCusWct.WCT_Internship_Start_Date__c=Date.today();
        objCusWct.WCT_Internship_End_Date__c=Date.today();
        objCusWct.WCT_Last_Name_INTERN__c='';
        objCusWct.ownerId = usr.Id;
        objCusWct.WCT_Team_Mailbox__c = 'us.name@vmail.com';
       //objCusWct.WCT_Team_Mailbox__c = usr.email;
        lstOffer.add(objCusWct);
        insert lstOffer;
        
        objCusWct.recalculateformulas();  
        
        Datetime yesterday = Datetime.now().addminutes(-490);
        Test.setCreatedDate(objCusWct.id, yesterday);
        system.debug('yesterday1------'+yesterday);
        //system.debug('Test.setCreatedDate----'+Test.setCreatedDate);
        
        system.debug('objCusWct values1--------'+objCusWct);
        system.debug('objCusWct.WCT_Created_Date_Time__c1-------'+objCusWct.WCT_Created_Date_Time__c);
        
        Decimal  seconds = BusinessHours.diff(bhs.id, yesterday, System.now())/ 1000;
        Datetime startdate = Datetime.newInstance(2013, 5, 28, 1, 6, 8);
        system.debug('seconds1------'+seconds);
        Decimal  min = Integer.valueOf(seconds / 60) - 692;
        system.debug('min1------'+min);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();   
        
        Test.StartTest();
        
        Wct_OfferPendingforAnalyst objBatch = new Wct_OfferPendingforAnalyst();
        
        Map<ID, Schema.RecordTypeInfo> rtMap1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosById();
        system.assertequals(rtMap1.get(lstOffer[0].RecordTypeId).getName(), 'US Campus Hire Offer' ) ; 
        system.schedule('New','0 0 2 1 * ?',objBatch );
        ID batchprocessid = Database.executeBatch(objBatch);
        
        Test.StopTest();
    }
    
     static testMethod void Wct_OfferPendingforAnalyst_TestMethod2 (){
     Profile prof = [select id from profile where name='system Administrator'];
     User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
    
       Contact con = new Contact ();
       con.LastName='tec';
       con.Email='user@gmail.com';
       con.WCT_Internship_Start_Date__c = date.today()-1;
       Insert con;
       system.debug('con values2------'+con);
       
       
        string rc1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Experienced Hire Offer').getRecordTypeId();
        
        BusinessHours bhs=[select id from BusinessHours where IsDefault=true];
        integer mins = 358;
        system.debug('mins2------'+mins);
        
        List<WCT_Offer__c> lstOffer = new List<WCT_Offer__c>(); 
        WCT_Offer__c  objCusWct= new WCT_Offer__c();
        
        objCusWct.RecordTypeId = rc1;
        //objCusWct.recordtype = rc1.id;
        objCusWct.WCT_status__c = 'Returned for Revisions';
        objCusWct.WCT_Candidate__c = con.Id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.OwnerId = usr.id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.WCT_Type_of_Hire__c = 'Full Time';
        objCusWct.WCT_AgentState_Time__c = Date.today();
        objCusWct.WCT_Basic__c = '200';
        objCusWct.WCT_Annual_LTA__c='200';
        objCusWct.WCT_Annual_Provident_Fund__c='1200';
        objCusWct.WCT_Annual_Spcl_Allowance__c='800';
        objCusWct.WCT_Lookup_Code__c='1442';
        objCusWct.WCT_Internship_Start_Date__c=Date.today();
        objCusWct.WCT_Internship_End_Date__c=Date.today();
        objCusWct.WCT_Last_Name_INTERN__c='';
        objCusWct.ownerId = usr.Id;
        objCusWct.WCT_Team_Mailbox__c = 'us.name@vmail.com';
       
        lstOffer.add(objCusWct);
        insert lstOffer;
        
        objCusWct.recalculateformulas();  
        
        Datetime yesterday = Datetime.now().addminutes(-250);
        Test.setCreatedDate(objCusWct.id, yesterday);
        system.debug('yesterday2----'+yesterday);
        //system.debug('Test.setCreatedDate----'+Test.setCreatedDate);
        
        system.debug('objCusWct values2--------'+objCusWct);
        system.debug('objCusWct.WCT_Created_Date_Time__c2-------'+objCusWct.WCT_Created_Date_Time__c);
        
        
        Test.StartTest();
        
        Wct_OfferPendingforAnalyst objBatch = new Wct_OfferPendingforAnalyst();
        Map<ID, Schema.RecordTypeInfo> rtMap1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosById();
        system.assertequals(rtMap1.get(lstOffer[0].RecordTypeId).getName(), 'US Experienced Hire Offer' ) ; 
        system.schedule('New','0 0 2 1 * ?',objBatch );
        ID batchprocessid = Database.executeBatch(objBatch);
        
        Test.StopTest();
    }
    
    static testMethod void Wct_OfferPendingforAnalyst_TestMethod3 (){
     Profile prof = [select id from profile where name='system Administrator'];
     User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
    
       Contact con = new Contact ();
       con.LastName='tec';
       con.Email='user@gmail.com';
       con.WCT_Internship_Start_Date__c = date.today()-1;
       Insert con;
       system.debug('con values3-------'+con);
       
       
        string rc1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Experienced Hire Offer').getRecordTypeId();
        
        BusinessHours bhs=[select id from BusinessHours where IsDefault=true];
        integer mins = 358;
        system.debug('mins3------'+mins);
       
        List<WCT_Offer__c> lstOffer = new List<WCT_Offer__c>(); 
        WCT_Offer__c  objCusWct= new WCT_Offer__c();
        
        objCusWct.RecordTypeId = rc1;
        //objCusWct.recordtype = rc1.id;
        objCusWct.WCT_status__c = 'Returned for Revisions';
        objCusWct.WCT_Candidate__c = con.Id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.OwnerId = usr.id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.WCT_Type_of_Hire__c = 'Full Time';
        objCusWct.WCT_AgentState_Time__c = Date.today();
        objCusWct.WCT_Basic__c = '200';
        objCusWct.WCT_Annual_LTA__c='200';
        objCusWct.WCT_Annual_Provident_Fund__c='1200';
        objCusWct.WCT_Annual_Spcl_Allowance__c='800';
        objCusWct.WCT_Lookup_Code__c='1442';
        objCusWct.WCT_Internship_Start_Date__c=Date.today();
        objCusWct.WCT_Internship_End_Date__c=Date.today();
        objCusWct.WCT_Last_Name_INTERN__c='';
        objCusWct.ownerId = usr.Id;
        objCusWct.WCT_Team_Mailbox__c = 'us.name@vmail.com';
        objCusWct.WCT_Iteration_count__c = 0;
        lstOffer.add(objCusWct);
        insert lstOffer;
        
        lstOffer[0].WCT_Iteration_count__c = 1;
        update lstOffer;
        
        objCusWct.recalculateformulas();  
        
        Datetime yesterday = Datetime.now().addminutes(-370);
        Test.setCreatedDate(objCusWct.id, yesterday);
        system.debug('yesterday3-------'+yesterday);
        //system.debug('Test.setCreatedDate----'+Test.setCreatedDate);
        
        system.debug('objCusWct values3------'+objCusWct);
        system.debug('objCusWct.WCT_Created_Date_Time__c3-------'+objCusWct.WCT_Created_Date_Time__c);
        
        
        Test.StartTest();
        
        Wct_OfferPendingforAnalyst objBatch = new Wct_OfferPendingforAnalyst();
        Map<ID, Schema.RecordTypeInfo> rtMap1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosById();
        system.assertequals(rtMap1.get(lstOffer[0].RecordTypeId).getName(), 'US Experienced Hire Offer' ) ; 
        system.schedule('New','0 0 2 1 * ?',objBatch );
        ID batchprocessid = Database.executeBatch(objBatch);
        
        Test.StopTest();
    }
    
    static testMethod void Wct_OfferPendingforAnalyst_TestMethod4 (){
     Profile prof = [select id from profile where name='system Administrator'];
     User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
    
       Contact con = new Contact ();
       con.LastName='tec';
       con.Email='user@gmail.com';
       con.WCT_Internship_Start_Date__c = date.today()-1;
       Insert con;
       system.debug('con values3-------'+con);
       
       
        string rc1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Experienced Hire Offer').getRecordTypeId();
        
        BusinessHours bhs=[select id from BusinessHours where IsDefault=true];
        integer mins = 358;
        system.debug('mins3------'+mins);
       
        List<WCT_Offer__c> lstOffer = new List<WCT_Offer__c>(); 
        WCT_Offer__c  objCusWct= new WCT_Offer__c();
        
        objCusWct.RecordTypeId = rc1;
        //objCusWct.recordtype = rc1.id;
        objCusWct.WCT_status__c = 'Returned for Revisions';
        objCusWct.WCT_Candidate__c = con.Id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.OwnerId = usr.id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.WCT_Type_of_Hire__c = 'Full Time';
        objCusWct.WCT_AgentState_Time__c = Date.today();
        objCusWct.WCT_Basic__c = '200';
        objCusWct.WCT_Annual_LTA__c='200';
        objCusWct.WCT_Annual_Provident_Fund__c='1200';
        objCusWct.WCT_Annual_Spcl_Allowance__c='800';
        objCusWct.WCT_Lookup_Code__c='1442';
        objCusWct.WCT_Internship_Start_Date__c=Date.today();
        objCusWct.WCT_Internship_End_Date__c=Date.today();
        objCusWct.WCT_Last_Name_INTERN__c='';
        objCusWct.ownerId = usr.Id;
        objCusWct.WCT_Team_Mailbox__c = 'us.name@vmail.com';
        objCusWct.WCT_Iteration_count__c = 1;
        lstOffer.add(objCusWct);
        insert lstOffer;
        
        lstOffer[0].WCT_Iteration_count__c = 2;
        update lstOffer;
        
        objCusWct.recalculateformulas();  
        
        Datetime yesterday = Datetime.now().addminutes(-490);
        Test.setCreatedDate(objCusWct.id, yesterday);
        system.debug('yesterday3-------'+yesterday);
        //system.debug('Test.setCreatedDate----'+Test.setCreatedDate);
        
        system.debug('objCusWct values3------'+objCusWct);
        system.debug('objCusWct.WCT_Created_Date_Time__c3-------'+objCusWct.WCT_Created_Date_Time__c);
        
        
        Test.StartTest();
        
        Wct_OfferPendingforAnalyst objBatch = new Wct_OfferPendingforAnalyst();
        Map<ID, Schema.RecordTypeInfo> rtMap1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosById();
        system.assertequals(rtMap1.get(lstOffer[0].RecordTypeId).getName(), 'US Experienced Hire Offer' ) ; 
        system.schedule('New','0 0 2 1 * ?',objBatch );
        ID batchprocessid = Database.executeBatch(objBatch);
        
        Test.StopTest();
    }
    
    
    
}