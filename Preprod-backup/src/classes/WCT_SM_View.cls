public class WCT_SM_View extends SitesTodHeaderController 
{

        /*Public Variables*/
    

    Public WCT_H1BCAP__c WCTH1BCAP {get;set;}
    public String loggedName{get; set;}
    public String StatusUpdate{get; set;}
    public List<Profile> loggedProfile = new List<Profile>();
    public String getRecordId;
    public boolean reject_flag{get; set;}
    public string LoggedInUserEmail {get; set;}
    public boolean pageError {get; set;}
       public boolean pageload {get; set;}
    public String pageErrorMessage {get; set;}
     public String SMComments {
        get;
        set;
    }
   public String realignComments {
        get;
        set;
    }
 public boolean rejectblock {
        get;
        set;
    }
    public boolean realignblock {
        get;
        set;
    }
    /* Error Message related variables */

    
    public contact Employee = new contact();

     public WCT_SM_View ()
    {
       LoggedInUserEmail = UserInfo.getUserEmail();

       getRecordId = ApexPages.currentPage().getParameters().get('id'); 
       loggedProfile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
       loggedName =loggedProfile[0].Name;
       
       if(getRecordId == '' || getRecordId == null) {
             
            pageErrorMessage = 'Not valid';
            pageError = true;
            //return null;
             
             }
  else {
            WCTH1BCAP =  [SELECT WCT_Sl_No__c,OwnerId,WCT_Custom_Modified_Date__c,CreatedDate,Name,Id,WCT_H1BCAP_Case_status__c, WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c, 
        WCT_H1BCAP_Client_location_Building__c, WCT_H1BCAP_Client_Location_City_State__c, WCT_H1BCAP_Client_Location_Flat_Number__c, 
        WCT_H1BCAP_Client_Location_Zip_code__c, WCT_H1BCAP_Client_Name__c, WCT_H1BCAP_Comments__c, WCT_H1BCAP_Confirmation_US_Eng_Manager__c, 
        WCT_H1BCAP_CPA_Certification__c, WCT_H1BCAP_Delegate_for_USI_SM_GDM__c, WCT_H1BCAP_Delegate_USI_SM_GDM_Email__c, WCT_H1BCAP_Deloitte_Entity__c, WCT_H1BCAP_Deloitte_Tenure_Years__c, WCT_H1BCAP_Deloitte_Tenure_Year__c, WCT_H1BCAP_Deployment_Advisor_Email_id__c, 
        WCT_H1BCAP_Deployment_Advisor_Name__c, WCT_H1BCAP_Educational_Background__c, WCT_H1BCAP_Employment_Status__c, WCT_Rejection_Reason_from_RM__c ,
        WCT_H1BCAP_FSS_Leader_Email_Id__c, WCT_H1BCAP_FSS_Leader_Name__c, WCT_H1BCAP_FY15_AERS_US_level__c, WCT_H1BCAP_H1B_Status__c,WCT_H1BCAP_Capability__c, 
        WCT_H1BCAP_Industry__c, WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Practitioner_Level__c,WCT_H1BCAP_Practitioner_DOJ__c, 
         WCT_H1BCAP_Practitioner_Name__c, WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Skill_set__c, WCT_H1BCAP_Processing_Type__c, 
         WCT_H1BCAP_Project_Manager_Email_Id__c, WCT_H1BCAP_Project_Manager_Name__c, WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_Notes_Given_GM_I__c, 
         WCT_H1BCAP_Rationale_project_alignment__c, WCT_H1BCAP_Region__c, WCT_H1BCAP_Resource_Manager_Email_ID__c, WCT_H1BCAP_RM_Name__c, 
         WCT_H1BCAP_RM_comments__c, WCT_H1BCAP_Service_Area_Leader_email_Id__c, WCT_H1BCAP_Service_Leader_Name__c, 
         WCT_H1BCAP_Status__c, WCT_H1BCAP_Total_experience__c, WCT_H1BCAP_Deloitte_Reporting_Office_Loc__c, 
         WCT_H1BCAP_Willingness_Options__c,WCT_H1BCAP_Willingness_Status__c,WCT_H1BCAP_Rejected_Reason_from_SM__c ,WCT_H1BCAP_Practitioner_Comments__c,WCT_H1BCAP_Willing_to_Travel__c ,WCT_H1BCAP_Business_email__c, WCT_H1BCAP_Business_immigration_champ__c, WCT_H1BCAP_USI_Expat__c, WCT_H1BCAP_USI_Expat_email_id__c, 
         Super_RM_Comments__c,WCT_H1BCAP_USI_PPD_mail_Id__c, WC_H1BCAP_USI_SLL_email_id__c, WCT_H1BCAP_USI_SLL_Name__c, WCT_H1BCAP_GDM_Email_id__c, 
        WCT_Realigned_Reason_from_SM__c, WCT_Realigned_Reason_from_SLL__c,WCT_Rejected_Reason_from_SLL__c,WCT_H1BCAP_Practitioner_Service_Area__c,WCT_H1BCAP_Practitioner_Service_Line__c,Multi_checkbox__c,WCT_H1BCAP_USI_SM_Name_GDM__c, WCT_H1BCAP_US_PPD_mail_Id__c, WCT_H1BCAP_US_PPD_Name__c, WCT_H1BCAP_Year_At_Level__c, WCT_H1BCAP_YE_Rating__c from WCT_H1BCAP__c where id = : getRecordId limit 1];
      rejectblock = false;
      realignblock=false;
      pageload=false;
    }
    }
    
        public PageReference rejectAction() {
        rejectblock = true;
    
        return null;
    }
    public PageReference cancelAction() {
        rejectblock = false;
  
        pageReference  page  = new pageReference('/apex/WCT_Approval_View?id='+getRecordId );
         page.setRedirect(true);
        return page;
    }
        public PageReference realignAction() {
        realignblock = true;
    
        return null;
    }
    public PageReference cancelRealignAction() {
        realignblock = false;
  
        pageReference  page  = new pageReference('/apex/WCT_Approval_View?id='+getRecordId );
         page.setRedirect(true);
        return page;
    }
    
     public pageReference approveRecord()
    {
 
  if(loggedName == '20_H1BCAP_SLL'){
   StatusUpdate='USI SLL Approved';
   }
 else if(loggedName == '20_H1BCAP_SM'){
   StatusUpdate='USI SM Approved';
   }
   
    try {
    
       if ((null == WCTH1BCAP.WCT_H1BCAP_Status__c)  || ('' == WCTH1BCAP.WCT_H1BCAP_Status__c))
         {
           pageErrorMessage = 'Please fill Status.';
            pageError = true;
            pageload=false;
            return null;
        }
    
        else {
    
        WCT_H1BCAP__c updateStatus = [SELECT Id,WCT_Rejected_Reason_from_SLL__c,WCT_H1BCAP_Rejected_Reason_from_SM__c,WCT_Realigned_Reason_from_SLL__c,WCT_Realigned_Reason_from_SM__c,WCT_Custom_Modified_Date__c,WCT_H1BCAP_Willingness_Options__c,WCT_H1BCAP_Willingness_Status__c,WCT_H1BCAP_Status__c,WCT_H1BCAP_Business_Days__c,WCT_H1BCAP_Willing_to_Travel__c,WCT_H1BCAP_Practitioner_Comments__c
                                                  FROM WCT_H1BCAP__c WHERE  id = :getRecordId limit 1];
     
      updateStatus.WCT_H1BCAP_Status__c= StatusUpdate;
        update updateStatus;
    pageload = true;
        pageError = false;
     }
    
    
   
} catch(Exception e) {
    System.debug('An unexpected error has occurred: ' + e.getMessage());
}
return null;

}

 
 
 public pageReference realignRecord()
    {

   if(loggedName == '20_H1BCAP_SLL'){
   StatusUpdate='USI SLL Realigned';
   }
    else if(loggedName == '20_H1BCAP_SM'){
   StatusUpdate='USI SM Realigned';
   }
   
    try {
    
    if(loggedName == '20_H1BCAP_SM') {
       if (realignComments == '')
         {
           pageErrorMessage = 'Please fill the Re-aligned Reason';
            pageError = true;
            pageload=false;
            return null;
        }
        }
    
         if(loggedName == '20_H1BCAP_SLL') {
      if (realignComments == '')
         {
         pageload=false;
           pageErrorMessage = 'Please fill the Re-aligned Reason';
            pageError = true;
            return null;
        }
    }
    
       
        WCT_H1BCAP__c updateStatus = [SELECT Id,WCT_Realigned_Reason_from_SLL__c,WCT_Realigned_Reason_from_SM__c,WCT_Custom_Modified_Date__c,WCT_H1BCAP_Willingness_Options__c,WCT_H1BCAP_Willingness_Status__c,WCT_H1BCAP_Status__c,WCT_H1BCAP_Business_Days__c,WCT_H1BCAP_Willing_to_Travel__c,WCT_H1BCAP_Practitioner_Comments__c
                                                  FROM WCT_H1BCAP__c WHERE  id = :getRecordId limit 1];
     
      
       updateStatus.WCT_H1BCAP_Status__c= StatusUpdate;
       if(loggedName == '20_H1BCAP_SM') {
         updateStatus.Wct_H1bcap_SM_Realigned_Date__c = system.today();
         updateStatus.WCT_Realigned_Reason_from_SM__c= realignComments;
       }
          if(loggedName == '20_H1BCAP_SLL') {
          updateStatus.Wct_H1bcap_SLL_RealignedDate__c = system.today();
          updateStatus.WCT_Realigned_Reason_from_SLL__c= realignComments;
          }
    
      
        update updateStatus;
        pageError = false;
       realignblock = false;
       cancelRealignAction();
 pageReference pageRef= ApexPages.currentPage(); 
       Id id = pageRef.getParameters().get('id');
       pageRef.getParameters().clear();
       pageRef.getParameters().put('id', updateStatus.id);  
       pageRef.setRedirect(true);
       return pageRef;  
   
} catch(Exception e) {
    System.debug('An unexpected error has occurred: ' + e.getMessage());
}

return null;
    
}
 public pageReference rejectRecord()
    {
      
    if(loggedName == '20_H1BCAP_SLL'){
   StatusUpdate='USI SLL Rejected';
   }
    else if(loggedName == '20_H1BCAP_SM'){
   StatusUpdate='USI SM Rejected';
   }
    
    try {
    system.debug('code::'+WCTH1BCAP.WCT_H1BCAP_Rejected_Reason_from_SM__c);

    if(loggedName == '20_H1BCAP_SM') {
       if (SMComments == '')
         {
           pageErrorMessage = 'Please fill the Rejection Reason ';
            pageError = true;
            pageload=false;
            return null;
        }
        }
         if(loggedName == '20_H1BCAP_SLL') {
      if (SMComments == '')
         {
           pageErrorMessage = 'Please fill the Rejection Reason';
            pageError = true;
            pageload=false;
            return null;
        }
    }
    
        WCT_H1BCAP__c updateStatus = [SELECT Id,WCT_Rejected_Reason_from_SLL__c,WCT_H1BCAP_Rejected_Reason_from_SM__c,WCT_Custom_Modified_Date__c,WCT_H1BCAP_Willingness_Options__c,WCT_H1BCAP_Willingness_Status__c,WCT_H1BCAP_Status__c,WCT_H1BCAP_Business_Days__c,WCT_H1BCAP_Willing_to_Travel__c,WCT_H1BCAP_Practitioner_Comments__c
                                                  FROM WCT_H1BCAP__c WHERE  id = :getRecordId limit 1];
     
   
 
      updateStatus.WCT_H1BCAP_Status__c= StatusUpdate;
       if(loggedName == '20_H1BCAP_SM') {
        updateStatus.Wct_H1bCap_SM_RejectedDate__c = system.today();
         updateStatus.WCT_H1BCAP_Rejected_Reason_from_SM__c= SMComments;
       }
          if(loggedName == '20_H1BCAP_SLL') {
      updateStatus.Wct_H1bcap_SLL_RejectDate__c = system.today();    
      updateStatus.WCT_Rejected_Reason_from_SLL__c= SMComments;
          }
    
        update updateStatus;
         pageError = false;
       rejectblock = false;
       cancelAction();
     

 pageReference pageRef= ApexPages.currentPage(); 
       Id id = pageRef.getParameters().get('id');
       pageRef.getParameters().clear();
       pageRef.getParameters().put('id', updateStatus.id);  
       pageRef.setRedirect(true);
       return pageRef;  
   
   
} catch(Exception e) {
    System.debug('An unexpected error has occurred: ' + e.getMessage());
}

return null;
    
}
}