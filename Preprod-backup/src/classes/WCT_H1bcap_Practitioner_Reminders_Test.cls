@isTest
 public class WCT_H1bcap_Practitioner_Reminders_Test{
 Static testmethod void WCT_H1bcap_Practitioner_Reminders_TestMethod(){
   
      
       Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_RM']; 
       User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','svalluru@deloitte.com');
       insert platuser;
 
   
       Recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
       Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
       con.WCT_Employee_Group__c = 'Active';
       insert con;
  
  list<WCT_H1BCAP__c> lst = new list<WCT_H1BCAP__c>();
        WCT_H1BCAP__c h1 = new WCT_H1BCAP__c();
        h1.WCT_H1BCAP_Practitioner_Name__c = con.id;
        h1.WCT_H1BCAP_Email_ID__c = 'svalluru@gmail.com';
        h1.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
        h1.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'abc@yahoo.com';
        h1.WCT_H1BCAP_Status__c  = null;
       // h1.WCT_H1BCAP_USI_SM_Name_GDM__c = platuser.id;
       h1.recalculateFormulas(); 
        lst.add(h1);
        insert lst;   
  
  
       Date d = system.today().adddays(-4); 
       Test.setCreatedDate(lst[0].Id, d);
  system.debug('BusinesssDays********'+lst[0].WCT_H1BCAP_Business_Days__c );
       string orgmail =  Label.WCT_H1BCAP_Mailbox;
       OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
       Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'WCT_Reminder_1_notification_to_practitioner_for_Willing_to_travel' AND IsActive = true];
      
     
       List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
     
            mail.SetTemplateid(et.id);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(lst[0].WCT_H1BCAP_Practitioner_Name__c);
            mail.setWhatid(lst[0].id);
            mail.setOrgWideEmailAddressId(owe.id);   
 
    Test.StartTest(); 
    WCT_H1bcap_Practitioner_Reminders h1bs = new WCT_H1bcap_Practitioner_Reminders ();
    WCT_H1bcap_Practitioner_Schedul_Remnd  createCon = new WCT_H1bcap_Practitioner_Schedul_Remnd ();
    system.schedule('New','0 0 2 1 * ?',createCon); 
    database.Executebatch(h1bs); 
    Test.StopTest(); 
}   
    
 Static testmethod void WCT_H1bcap_Practitioner_Reminders_TestMethod1(){
 
       Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_RM']; 
       User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','svalluru@deloitte.com');
       insert platuser;
 
   
        Recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.WCT_Employee_Group__c = 'Active';
        insert con;
 
       list<WCT_H1BCAP__c> ls = new list<WCT_H1BCAP__c>();
        WCT_H1BCAP__c h = new WCT_H1BCAP__c();
        h.WCT_H1BCAP_Practitioner_Name__c = con.id;
        h.WCT_H1BCAP_Email_ID__c = 'svalluru@gmail.com';
        h.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
        h.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'abc@yahoo.com';
        h.WCT_H1BCAP_Status__c  = null;
   //     h.WCT_H1BCAP_USI_SM_Name_GDM__c = platuser.id;
       h.recalculateFormulas(); 
        ls.add(h);
        insert ls;   
   
  
       Date d1 = system.today().adddays(-5); 
       Test.setCreatedDate(ls[0].Id, d1);
       
        system.debug('BusinesssDays1********'+ls[0].WCT_H1BCAP_Business_Days__c );
       string orgmail =  Label.WCT_H1BCAP_Mailbox;
       OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
       Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'WCT_Reminder_1_notification_to_practitioner_for_Willing_to_travel' AND IsActive = true];
       Emailtemplate et1 = [select id, developername , IsActive from Emailtemplate where developername = 'WCT_Reminder_2_notification_to_practitioner_for_Willing_to_travel' AND IsActive = true]; 
     List<Messaging.SingleEmailMessage> mailList1 = new List<Messaging.SingleEmailMessage>();
    Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage(); 
     
            mail1.SetTemplateid(et1.id);
            mail1.setSaveAsActivity(false);
            mail1.setTargetObjectId(ls[0].WCT_H1BCAP_Practitioner_Name__c);
            mail1.setWhatid(ls[0].id);
            mail1.setOrgWideEmailAddressId(owe.id);   
            mailList1.add(mail1);  
  
    Test.StartTest(); 
    WCT_H1bcap_Practitioner_Reminders h1bs = new WCT_H1bcap_Practitioner_Reminders ();
    WCT_H1bcap_Practitioner_Schedul_Remnd  createCon = new WCT_H1bcap_Practitioner_Schedul_Remnd ();
    system.schedule('New','0 0 2 1 * ?',createCon); 
    database.Executebatch(h1bs); 
    Test.StopTest(); 
    
    
    
   }
  }