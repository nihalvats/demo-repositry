/*****************************************************************************************
    Name    : WCT_CreateCandidateTracker_TEST
    Desc    : Test class for WCT_CreateCandidateTracker_Controller                

    Modification Log : 
---------------------------------------------------------------------------
 Developer                  Date            Description
---------------------------------------------------------------------------
  Deloitte                 15 Jan 2014         Created 
 ******************************************************************************************/

@isTest
private class  WCT_CreateCandidateTracker_TEST{
    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
    static testMethod void createCandTracker(){
        Contact adhocCand = WCT_UtilTestDataCreation.createContactAsCandidate(SYSTEM.Label.AdhocCandidateRecordtypeID);
        Contact Candidate = WCT_UtilTestDataCreation.createContactAsCandidate(SYSTEM.Label.CandidateRecordtypeId);
        insert adhocCand;
        insert Candidate;
        User user =  WCT_UtilTestDataCreation.createUser('recoo',profileIdRecCoo,'ralaRecCo@deloitte.com','rala@deloitte.com');
        insert user;
        WCT_Requisition__c requisition = WCT_UtilTestDataCreation.createRequisition(user.id);
        insert requisition;

        Test.startTest();
        PageReference pageRef = Page.WCT_CreateCandidateTracker_Page;      
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', Candidate.id);
        WCT_CreateCandidateTracker_Controller ctrl = new WCT_CreateCandidateTracker_Controller();
        ctrl.can();
        system.Assert(ctrl.booCand == true);
        ctrl.reqId = requisition.id;
        ctrl.saveCandTracker();
        ctrl.candidateReq.WCT_Contact__c = null;
        ctrl.saveCandTracker();  
        PageReference AdhocpageRef = Page.WCT_CreateCandidateTracker_Page;      
        Test.setCurrentPage(AdhocpageRef);
        ApexPages.currentPage().getParameters().put('id', adhocCand.id);
        WCT_CreateCandidateTracker_Controller AdhocCtrl = new WCT_CreateCandidateTracker_Controller();
        system.Assert(AdhocCtrl.booAdhocCand == true);
        AdhocCtrl.candidateReq.WCT_Requisition__c = requisition.id;
        AdhocCtrl.saveCandTracker();
        Test.stopTest();
    }
}