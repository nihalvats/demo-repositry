public class CiTemplt {
    
       public Id caseId {get;set;}
        Id recdtype = Schema.SObjectType.Exit_Survey__c.getRecordTypeInfosByName().get('CI Questionnaire').getRecordTypeId();
     public List<Exit_Survey__c> getCiQ()
    {
                
        List<Exit_Survey__c> sy;
       sy = [SELECT Name, id,ELE_Question_1__c,ELE_Q1_Additional_Comments__c,ELE_Question_2__c,ELE_Question_2_approver_name__c,ELE_Q2_Additional_Comments__c,
             ELE_Question_3__c,ELE_Question_3_Detail_steps__c,ELE_Question3_PPD_Name__c,ELE_Q3_Additional_Comments__c,ELE_Question_4__c,
             ELE_Question_4_Type_of_Data__c,ELE_Question_4_Type_of_Media__c,ELE_Q4_Additional_Comments__c,ELE_Question_5__c ,
             ELE_Q5_Additional_Comments__c,ELE_Question_6__c,ELE_Question_6_Details__c,ELE_Q6_Additional_Comments__c,ELE_Question_7__c,ELE_Q7_Additional_Comments__c
             
             FROM Exit_Survey__c WHERE Case__c =: caseId and recordtypeId=:recdtype LIMIT 1];
        return sy;
    }
}