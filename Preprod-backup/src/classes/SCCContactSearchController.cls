/*****************************************************************************************
    Name    : SCCContactSearchController
    Desc    : Class used to Search Contact from Service Cloud Console Component and VF Pages
    		  'SCCSearchContactNotFound' Page
    		  'SCCSearchContact' Page
    Approach: 
                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
 Sreenivasa Munnangi            20 Aug, 2013         Created 
******************************************************************************************/
public with sharing class SCCContactSearchController {

    // the soql without the order and limit
    private String soql {get;set;}
    // the collection of contacts to display
    public List<Contact> contacts {get;set;}
    // format the soql for display on the visualforce page
    public String debugSoql {
        get { return soql + ' limit 500'; }
        set;
    }
    // init the controller and display some sample data when the page loads
    public SCCContactSearchController () {
    }


    // runs the actual query
    public void runQuery() {
	    try {
	       contacts = Database.query(soql + 'ORDER BY Name limit 200');
	    } catch (Exception e) {
	    	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error occured. Please contact System Administrator!'));
	    }
    
    }

    // runs the search with parameters passed via Javascript
    public PageReference runSearch() {
        
        String firstName = Apexpages.currentPage().getParameters().get('firstname');
        String lastName = Apexpages.currentPage().getParameters().get('lastname');
        String personnelNumber = Apexpages.currentPage().getParameters().get('personnelNumber');
        String prefferedName= Apexpages.currentPage().getParameters().get('prefferedName');
        String priorName= Apexpages.currentPage().getParameters().get('priorName');
        String facilityName= Apexpages.currentPage().getParameters().get('facilityName');
        String ssnLast4digits= Apexpages.currentPage().getParameters().get('ssnLast4digits');

        system.debug('firstName ==>'+firstName );
        system.debug('lastName ==>'+lastName );
        system.debug('personnelNumber ==>'+personnelNumber );
        system.debug('prefferedName==>'+prefferedName);
        system.debug('priorName==>'+priorName);
        system.debug('ssnLast4digits==>'+ssnLast4digits);
        
        soql = 'SELECT Firstname, Lastname,WCT_Employee_Status__c, WCT_Preferred_Name__c,'+
                                'WCT_Prior_Name__c,WCT_SSN__c, WCT_Facility_Name__c,WCT_Personnel_Number__c,'+
                                'Phone, Email, WCT_Most_Recent_Rehire__c'+
                                ' from Contact ';
		if (!firstName.equals('')){
		        soql += ' where firstname LIKE \''+String.escapeSingleQuotes(firstName)+'%\'';
		}
		if (!lastName.equals('')){
		        if(soql.contains('where')){
		                soql += ' and lastname LIKE \''+String.escapeSingleQuotes(lastName)+'%\'';
		        }else{
		                soql += ' where lastname LIKE \''+String.escapeSingleQuotes(lastName)+'%\'';
		        }
		} 
		if (!personnelNumber.equals('')){
		        if(soql.contains('where')){
		                soql += ' and WCT_Personnel_Number__c = '+String.escapeSingleQuotes(personnelNumber );
		        }else{
		                soql += ' where WCT_Personnel_Number__c = '+String.escapeSingleQuotes(personnelNumber );
		        }
		}
		if (!facilityName.equals('')){
		        if(soql.contains('where')){
		                soql += ' and WCT_Facility_Name__c LIKE \''+String.escapeSingleQuotes(facilityName)+'%\'';
		        }else{
		                soql += ' where WCT_Facility_Name__c LIKE \''+String.escapeSingleQuotes(facilityName )+'%\'';
		        }
		}
		
		if (!prefferedName.equals('')){
		        if(soql.contains('where')){
		                soql += ' and WCT_Preferred_Name__c LIKE \''+String.escapeSingleQuotes(prefferedName)+'%\'';
		        }else{
		                soql += ' where WCT_Preferred_Name__c LIKE \''+String.escapeSingleQuotes(prefferedName )+'%\'';
		        }
		}
		if (!priorName.equals('')){
		        if(soql.contains('where')){
		                soql += ' and WCT_Prior_Name__c LIKE \''+String.escapeSingleQuotes(priorName)+'%\'';
		        }else{
		                soql += ' where WCT_Prior_Name__c LIKE \''+String.escapeSingleQuotes(priorName )+'%\'';
		        }
		}
		if (!ssnLast4digits.equals('')){
		        if(soql.contains('where')){
		                soql += ' and WCT_SSN__c LIKE \''+String.escapeSingleQuotes(ssnLast4digits)+'%\'';
		        }else{
		                soql += ' where WCT_SSN__c LIKE \''+String.escapeSingleQuotes(ssnLast4digits )+'%\'';
		        }
		}

        // run the query again
        runQuery();
        
        return null;
	}

}