public class importDataFromCSVController1
{
    public integer totalrecords { get; set; }
    public integer totalsuccessrec { get; set; }
    public integer totalsuccessreccount { get; set; }
    public integer totalunsuccessrec { get; set; }
    public String nameFile { get; set; }
    
    public list<contact> lcon = new list<contact> ();
   transient public list<id> listids= new list<id>();
   public integer size{get; set;}
   
public static final id candRecTypeId = WCT_Util.getRecordTypeIdByLabel('Contact','Candidate');
public static final id AdhocCandRecTypeId = WCT_Util.getRecordTypeIdByLabel('Contact','Candidate Ad Hoc');

    
  public List<MyWrapper> wrapper {get; set;}
  public boolean b {get{
          if(wrapper!=null)
          {
          return wrapper.size()>0?true:false;
          }
          else 
          {
          return false;
          }
  }
  }
   //  public string csvAsString{get;set;}
  transient public Map<string, contact> conNamesIdMap = new Map<string, contact>();
  transient public Blob contentFile { get; set; }
    String[] filelines = new String[]{};
    List<Contact> Conlist;
    public Contact UpdateCon;
    List<String> conNames;
    Set<String> conNamesf;
    Set<String> conNamesr;
    public  list<string> emaillist= new list<string> (); 
    Map<String, Contact> newlistmap = new Map<String, Contact>();
    List<Contact> newCons = new List<Contact>();
    
    
    
   
    public Pagereference ReadFile()
    {
       conNamesIdMap =new Map<string, contact>();
        
        /*Validtion if no file selected */
        if(contentFile!=null)
        {
            nameFile =contentFile.toString();
        }
        else
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please choose a file to upload !'));
            return null;
        }
        filelines = nameFile.split('\n');
        Conlist= new List<Contact>();
        conNames = new List<String>();
        conNamesf = new set<String>();
        conNamesr= new set<String>();
        wrapper = new List<MyWrapper>() ;
        listids= new list<id>();
        newlistmap= new Map<String, Contact>();
        totalunsuccessrec =0;
        totalsuccessrec =0;
        totalrecords=0;
        totalsuccessreccount=0;
        
  /*Step 1 :: Processing the uploaded file and fetching the various details into  the contact object List newlistmap.*/       
  for (Integer i=1;i<filelines.size();i++)
        {
        totalrecords++;
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');
            Contact c = new Contact();
            c.FirstName = inputvalues[0];
            c.LastName= inputvalues[1];       
            c.Email= inputvalues[2];
            c.CR_School_Recruiter__c=inputvalues[3];
            c.school_program__c = inputvalues[4];
            c.CR_Dropped_Resume__c=Boolean.valueOf(inputvalues[5]);
            c.Position_Applied_for__c=inputvalues[6];
            c.Service_Area_Applied_for__c=inputvalues[7];
            c.CR_Undergrad_Other_Major__c=inputvalues[8];
            if(inputvalues[9]!=null?inputvalues[9].trim()!='':false){
            c.CR_Undergraduate_Major_GPA_4_00_Scale__c=decimal.valueOf(inputvalues[9]!=null?inputvalues[9].trim():inputvalues[9]);}
            c.Office_Recruiter__c=inputvalues[10];
            c.Teams__c=inputvalues[11];
            if(inputvalues[12]!=null?inputvalues[12].trim()!='':false){
            c.CR_Graduate_GPA__c=decimal.valueOf(inputvalues[12]!=null?inputvalues[12].trim():inputvalues[12]);}
            c.CR_Other_GPA__c=inputvalues[13];
            c.SAT_Total__c=inputvalues[14];
            if(inputvalues[15]!=null?inputvalues[15].trim()!='':false){
            c.SAT_Quantitative__c=integer.valueOf(inputvalues[15]!=null?inputvalues[15].trim():inputvalues[15]);}
            if(inputvalues[16]!=null?inputvalues[16].trim()!='':false){
            c.SAT_Verbal__c=integer.valueOf(inputvalues[16]!=null?inputvalues[16].trim():inputvalues[16]);}
            if(inputvalues[17]!=null?inputvalues[17].trim()!='':false){
            c.SAT_Writing__c =integer.valueOf(inputvalues[17]!=null?inputvalues[17].trim():inputvalues[17]);}
            if(inputvalues[18]!=null?inputvalues[18].trim()!='':false){
            c.CR_ACT__c=integer.valueOf(inputvalues[18]!=null?inputvalues[18].trim():inputvalues[18]);}
            if(inputvalues[19]!=null?inputvalues[19].trim()!='':false){
            c.CR_GMAT__c=integer.valueOf(inputvalues[19]!=null?inputvalues[19].trim():inputvalues[19]);}
            if(inputvalues[20]!=null?inputvalues[20].trim()!='':false){
                c.GRE_Pre_8_1_2011__c=integer.valueOf((inputvalues[20]!=null && inputvalues[20]!='')?inputvalues[20].trim():inputvalues[20]);
            }
            
            conNames.add(c.Email);
            newlistmap.put(c.Email,c);
            conNamesf.add(c.FirstName);
            Conlist.add(c);
            emaillist.add(inputvalues[2]);

            system.debug('@@@@@@@@@@@@@@@@@@@@@@'+emaillist);   
        }
        
      //  totalsuccessreccount=totalsuccessrec-totalunsuccessrec;
        // existingConts = [SELECT Id,FirstName,LastName,SAT_Writing__c,CR_Dropped_Resume__c,CR_ACT__c,SAT_Verbal__c,SAT_Quantitative__c,WCT_Candidate_School__c,GRE_Pre_8_1_2011__c,Position_Applied_for__c, Email,Service_Area_Applied_for__c,CR_School_Recruiter__c,Office_Recruiter__c,Teams__c,CR_Undergrad_Other_Major__c,CR_Undergraduate_Major_GPA_4_00_Scale__c,CR_Other_GPA__c,CR_GMAT__c,SAT_Total__c FROM Contact where Email in :conNames limit 50000];
        //create a map with names as key
        // load the map - this will help you find out if an Contact name exists already
     
        /*Step 2 : Searching for the contacts with the emails ids from the Uploaded file in the SFDC. 
        */
     List<Contact> existingConts = [SELECT Id,FirstName,LastName,school_program__c,SAT_Writing__c,CR_Dropped_Resume__c,CR_ACT__c,SAT_Verbal__c,SAT_Quantitative__c,WCT_Candidate_School__c,GRE_Pre_8_1_2011__c,Position_Applied_for__c, Email,Service_Area_Applied_for__c,CR_School_Recruiter__c,Office_Recruiter__c,Teams__c,CR_Undergrad_Other_Major__c,CR_Undergraduate_Major_GPA_4_00_Scale__c,CR_Other_GPA__c,CR_GMAT__c,SAT_Total__c FROM Contact where Email in :conNames  limit 5000];
     
        /*Step 3 : By this step 'existingConts' has contacts record present in SFDC. 
                  A. Now, Modifying the existing contacts (existingConts) with new values from 'newlistmap' (from uploaded file)
                  B. At the same time remove the record which are been existing from the 'newlistmap' */
      for (Contact cont: existingConts)
      {
        If(newlistmap.containsKey(cont.Email))
        {
              UpdateCon = newlistmap.get(cont.Email);
              //Cont.FirstName=UpdateCon.FirstName;
              //Cont.LastName=UpdateCon.LastName;
              if(UpdateCon.SAT_Writing__c!=null ){
              Cont.SAT_Writing__c=UpdateCon.SAT_Writing__c;}
              if(UpdateCon.CR_Dropped_Resume__c!=null ){
              Cont.CR_Dropped_Resume__c=UpdateCon.CR_Dropped_Resume__c;}
              if(UpdateCon.CR_ACT__c!=null ){
              Cont.CR_ACT__c=UpdateCon.CR_ACT__c;}
              if(UpdateCon.SAT_Verbal__c!=null ){
              Cont.SAT_Verbal__c=UpdateCon.SAT_Verbal__c;}
              if(UpdateCon.SAT_Quantitative__c!=null ){
              Cont.SAT_Quantitative__c=UpdateCon.SAT_Quantitative__c;}
              if(UpdateCon.school_program__c!='' ){
              Cont.school_program__c=UpdateCon.school_program__c;}
              if(UpdateCon.GRE_Pre_8_1_2011__c!=null ){
              Cont.GRE_Pre_8_1_2011__c=UpdateCon.GRE_Pre_8_1_2011__c;}
              if(UpdateCon.Position_Applied_for__c!='' ){
              Cont.Position_Applied_for__c=UpdateCon.Position_Applied_for__c;}
              if(UpdateCon.Service_Area_Applied_for__c!='' ){
              Cont.Service_Area_Applied_for__c=UpdateCon.Service_Area_Applied_for__c;}
              if(UpdateCon.CR_School_Recruiter__c!='' ){
              Cont.CR_School_Recruiter__c=UpdateCon.CR_School_Recruiter__c;}
              if(UpdateCon.Office_Recruiter__c!='' ){
              Cont.Office_Recruiter__c=UpdateCon.Office_Recruiter__c;}
              if(UpdateCon.Teams__c!='' ){
              Cont.Teams__c=UpdateCon.Teams__c;}
              if(UpdateCon.CR_Undergrad_Other_Major__c!='' ){
              Cont.CR_Undergrad_Other_Major__c=UpdateCon.CR_Undergrad_Other_Major__c;}
              if(UpdateCon.CR_Undergraduate_Major_GPA_4_00_Scale__c!=null ){
              Cont.CR_Undergraduate_Major_GPA_4_00_Scale__c=UpdateCon.CR_Undergraduate_Major_GPA_4_00_Scale__c;}
              if(UpdateCon.CR_Other_GPA__c!='' ){
              Cont.CR_Other_GPA__c=UpdateCon.CR_Other_GPA__c;}
               if(UpdateCon.CR_Graduate_GPA__c!=null ){
              Cont.CR_Graduate_GPA__c=UpdateCon.CR_Graduate_GPA__c;}
              if(UpdateCon.CR_GMAT__c!=null ){
              Cont.CR_GMAT__c=UpdateCon.CR_GMAT__c;}
              if(UpdateCon.SAT_Total__c!='' ){
              Cont.SAT_Total__c=UpdateCon.SAT_Total__c;}
        /*Step 3 B : Deleting the Contact of the existing contact from the newlistmap. By this at the end of this loop, newlistmap will contain only the contact not present in the SFDC.*/
            newlistmap.remove(cont.Email);
         }     
      }
        
        /*Step 4 : Now 'existingConts' has contacts record present in SFDC with modified values from the uploaded files. Now update these contact into SFDC.*/
           if(existingConts.size()>0)
           {
            update existingConts;
           }
        /*Step 5 : Generating the report for End User.
         *        1. Records do not exist : Size of newlistmap. Hint :: Since all existing records are been removed from newlistmap.
         *        2. Records successfully updated : 'totalrecords' (total records in Uploaded file) - (Records do not exist).
         *        3. Update the wrapper class with non existing contact details.
        */
        totalunsuccessrec=newlistmap.size();
        totalsuccessreccount=existingConts.size();
        Set<String> emails=newlistmap.keySet();
        for(String email : emails)
        {
            Contact c=newlistmap.get(email);
            wrapper.add(new MyWrapper(c.FirstName,c.LastName,c.Email,c.CR_School_Recruiter__c,c.school_program__c,String.valueOf(c.CR_Dropped_Resume__c),c.Position_Applied_for__c,c.Service_Area_Applied_for__c,String.valueOf(c.CR_Undergrad_Other_Major__c),String.valueOf(c.CR_Undergraduate_Major_GPA_4_00_Scale__c),c.Office_Recruiter__c,c.Teams__c,String.valueOf(c.CR_Graduate_GPA__c),String.valueOf(c.CR_Other_GPA__c),String.valueOf(c.SAT_Total__c),String.valueOf(c.SAT_Quantitative__c),String.valueOf(c.SAT_Verbal__c) ,String.valueOf(c.SAT_Writing__c),String.valueOf(c.CR_ACT__c),String.valueOf(c.CR_GMAT__c),String.valueOf(c.GRE_Pre_8_1_2011__c)));
        }
     
                        
                        
try
 {
     for(string s:conNamesr)
        {
            if(conNamesf.contains(s))
            {
              conNamesf.remove(s);     
            }
        }

    // Update Cont;
    /* ApexPages.Message msg = new ApexPages.Message(ApexPages.severity.info,'ContactName with Contact Id are the new contact created.Related Contact and Opportunity also created');
     ApexPages.Message msg1 = new ApexPages.Message(ApexPages.severity.info,'ContactName without Contact Id are the existing Contacts');
     ApexPages.addMessage(msg);
     ApexPages.addMessage(msg1);*/

    }
     catch (Exception e)
    {

    ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Contact Email already exist, change Contact Email and try again');
      ApexPages.addMessage(errormsg);

    }
     return null;   
   }     
    
    
      public List<Contact> getuploadedAccounts()
    {
   
    list<contact> lcon1;
        if (Conlist!= NULL)
        
            if (Conlist.size() > 0){
            conlist.clear();
return conlist;}
               
            else
             return null;               
            else
            return null;
    }  
    
    public pagereference pg()
    {
    
    
    return page.ViewUploadedContactsInUI;}
    
    
    public class MyWrapper
    {
        public string Firstname1 {get; set;}
        public string Lastname1 {get; set;}
         public string Email1 {get; set;}
         public String SATWriting1 {get; set;}
         public String CRDroppedResume1 {get; set;}
         public String CRACT1 {get; set;}
         public String SATVerbal1 {get; set;}
         public String SATQuantitative1 {get; set;}
         public String Schoolprogram1 {get; set;}
         public String GREPre1 {get; set;}
         public String PositionApplied1  {get; set;}
         public String ServiceAreaApplied1 {get; set;}
         public String SchoolRecruiter1 {get; set;}
         public String OfficeRecruiter1 {get; set;}
         public String Teams1 {get; set;}
         public String OtherMajor1 {get; set;}
         public String UndergradMajorGPA1 {get; set;}
         public String OtherGPA1 {get; set;}
         public String GraduateGPA1 {get; set;}
         public String GMAT1 {get; set;}
         public String SATTotal1 {get; set;}
        public MyWrapper(string firstname , string lastname, string email, String SchoolRecruiter,String Schoolprogram,String CRDroppedResume, String PositionApplied,String ServiceAreaApplied, String OtherMajor,String UndergradMajorGPA,String OfficeRecruiter,String Teams,String GraduateGPA,String OtherGPA, String SATTotal, String SATQuantitative, String SATVerbal, String SATWriting, String CRACT, String GMAT, String GREPre  )
        {
            firstname1 = firstname ;
            lastname1 = lastname ;
            Email1 = email;
            SATWriting1 = SATWriting;
            CRDroppedResume1 = CRDroppedResume;
            CRACT1 =CRACT;
            SATVerbal1 = SATVerbal;
            SATQuantitative1 = SATQuantitative;
            Schoolprogram1 = Schoolprogram;
            GREPre1 = GREPre;
            PositionApplied1 = PositionApplied;
            ServiceAreaApplied1 = ServiceAreaApplied;
            SchoolRecruiter1=SchoolRecruiter;
            OfficeRecruiter1 = OfficeRecruiter;
            Teams1 = Teams;
            OtherMajor1 = OtherMajor;
            UndergradMajorGPA1 = UndergradMajorGPA;
            OtherGPA1 = OtherGPA;
            GraduateGPA1 =GraduateGPA;
            GMAT1 = GMAT;
            SATTotal1 = SATTotal;
        }
    }
    
    
     }