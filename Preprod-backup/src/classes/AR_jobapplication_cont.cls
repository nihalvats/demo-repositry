public class AR_jobapplication_cont{
public Contact con{get; set;}
public Invalid_Submission__c InvalidSubmission{get;set;}
public Contact_Job_Association__c conjob = new Contact_Job_Association__c();
public List<Contact> objContact= new List<Contact>();
public job__c job = new job__c();
public String parentId = '';
public string pid='';
String companyjobp='';

public Attachment attachment
{
get
{ 
if (attachment == null) attachment = new Attachment(); 
return attachment;
} 
set;
}
public AR_jobapplication_cont(ApexPages.StandardController controller) {
con = new contact();
pid=System.currentPagereference().getParameters().get('pid');
job=[select id,Name,Account_Contact_Name__c,Account__r.Name,Active__c,CCL_Alumnet_ID__c,Attest_Client__c,CCL_Attest_Client__c,Career_Level__c,CCL_Client__c,Client_Type__c,Close_Date__c,Comments__c,CCL_Company_posting_name_communications__c,job__c.CCL_Contact_for_company_if_applicable__c,Compensation_Details__c,Confidential__c,Deloitte_Client_Type__c,Description__c,Email__c,End_Date__c,Industry__c,Industry_Sector__c,Instructions_for_Contacting__c,AR_Job_Application_Link__c,Job_Category__c,AR_Job_Details__c,Job_ID__c,Job_Owners__c,CCL_Job_Type__c,CCL_Lead_from_the_Front_Client__c,CCL_Link_to_Job__c,Location__c,Position__c,Position_Code__c,Post_Date__c,Preferred_Phone__c,Region__c,CCL_Required_Skills__c,AR_Requirements__c,Service_Area_CCL__c,Source__c,Source_Detail_Name__c,Status__c,Target_Audience__c,Total_Compensation__c from job__c where id=:pid];
companyjobp=Job.CCL_Company_posting_name_communications__c;
}
/****************************************************************************************
* @author - Rajasekhara Reddy Yeturi
* @date - 21 March, 2014 
* @description - 
* @Usage - When user clicks on the Submit and Send an email button from JobApplicationlive VF Page.
*****************************************************************************************/ 
public PageReference upload() 
{ Invalid_Submission__c InvalidSubmission = new Invalid_Submission__c();
job=[select id,Name,Account_Contact_Name__c,Account__r.Name,Active__c,CCL_Alumnet_ID__c,Attest_Client__c,CCL_Attest_Client__c,Career_Level__c,CCL_Client__c,Client_Type__c,Close_Date__c,Comments__c,CCL_Company_posting_name_communications__c,job__c.CCL_Contact_for_company_if_applicable__c,Compensation_Details__c,Confidential__c,Deloitte_Client_Type__c,Description__c,Email__c,End_Date__c,Industry__c,Industry_Sector__c,Instructions_for_Contacting__c,AR_Job_Application_Link__c,Job_Category__c,AR_Job_Details__c,Job_ID__c,Job_Owners__c,CCL_Job_Type__c,CCL_Lead_from_the_Front_Client__c,CCL_Link_to_Job__c,Location__c,Position__c,Position_Code__c,Post_Date__c,Preferred_Phone__c,Region__c,CCL_Required_Skills__c,AR_Requirements__c,Service_Area_CCL__c,Source__c,Source_Detail_Name__c,Status__c,Target_Audience__c,Total_Compensation__c from job__c where id=:pid];
objContact=[select id,AR_Personal_Email__C  from contact where (AR_Personal_Email__C =:con.email and WCT_Type__c='Employee') or (AR_Deloitte_Email__c=:con.email and WCT_Type__c='Separated') limit 1];
if(ObjContact.size()<=0){
InvalidSubmission.First_Name__c=Con.FirstName;
InvalidSubmission.Last_Name__c=con.LastName;
InvalidSubmission.Email__c=con.Email;
InvalidSubmission.Job__c=Job.Id;
insert InvalidSubmission;
parentId = InvalidSubmission.id;
}
else
{
//insert con;
conjob.contact__c=Objcontact[0].id;
system.debug(Objcontact[0].id+'objcontactxxx');
conjob.Job__c=job.id;
insert conjob;
parentId = conjob.id;
}

attachment.OwnerId = UserInfo.getUserId();
attachment.ParentId = parentId;
attachment.IsPrivate = false;
try
{
if( attachment <> null) 
{ 
//This if() condition is for code coverage 
if (!Test.isRunningTest()){
insert attachment;
}

Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
String[] toAddresses = new String[]{job.email__c};
String fromaddress= 'marketplacejobs@deloitte.com'; 
String lineBreak = ''; // Line break
List<String> liObjects = new List<String>();
List<OrgWideEmailAddress> lstOrgWideEmailId = [Select id from OrgWideEmailAddress where Address = 'marketplacejobs@deloitte.com' limit 1];
if(Job.Confidential__C)
{
companyjobp='Confidential Company';
}
//mail.setReplyTo(fromaddress);
//mail.setSenderDisplayName(fromaddress); 
mail.setToAddresses(toAddresses); 
mail.setOrgWideEmailAddressId(lstOrgWideEmailId[0].id);
mail.setSubject('Candidate resume from Deloitte alumni marketplace jobs bank-Jobid:'+job.Job_ID__c);
liObjects.add(label.JobApplicationlive_EMail_Header+'</br></br></br></br></br></br>'); 
liObjects.add('Dear '+Job.CCL_Contact_for_company_if_applicable__c+','+'</br></br></br>'); 
liObjects.add('Please find attached resume of interested candidate for the '); liObjects.add(''+Job.Name+'</br>'); 
liObjects.add('position at '); liObjects.add(''+companyjobp+''); liObjects.add(' you posted on Deloitte’s marketplace job bank,'+'</br>'); 
liObjects.add('targeting Deloitte alumni, employees and friends. If you are interested, please '+'</br>');
liObjects.add('contact this candidate directly with next steps and mention you received his/her '+'</br>'); 
liObjects.add('resume from the Deloitte marketplace job bank.</br></br>');
liObjects.add('Your feedback is important to us. <a href="mailto:marketplacejobs@deloitte.com?Subject=Feedback" target="_top"> Please let us know </a> if '+'</br>');
liObjects.add('you hire a candidate from this source. We thank you for valuing Deloitte '+'</br>');
liObjects.add('experience and considering our alumni for your hiring needs.</br></br>');
liObjects.add('Sincerely,</br></br>');
liObjects.add('Deloitte Alumni Relations </br></br>');
liObjects.add('PS – Have another job to post? Provide us the information via our <a href="http://www.clicktools.com/survey?iv=177k5367jj84" >Job Submission Form</a>.</br></br>');
liObjects.add('<h6>This is a free service intended for informational purposes only. Candidates will apply directly to '+'</br>'); 
liObjects.add('the hiring party. Deloitte will not screen resumes nor advocate or negotiate on behalf of either '+'</br>');
liObjects.add('the former employee or the client. Deloitte will also continue to follow our internal policies on '+'</br>');
liObjects.add('what type of references or additional information is provided to prospective employers regarding '+'</br>');
liObjects.add('a former employee</h6>'+'</br></br>'); 
liObjects.add(''+'</br></br>'); 
liObjects.add(label.Deloitte_Footer_logo+'</br>');
String strMailBody = String.join(liObjects, lineBreak);
// mail.setPlainTextBody(strMailBody);
mail.setHTMLBody(strMailBody); 
//Set email file attachments
List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
// Add to attachment file list
Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
efa.setFileName(attachment.Name);
efa.setBody(attachment.Body);
fileAttachments.add(efa);
//create attachment for object
Attachment att = new Attachment(name = attachment.name, body = attachment.body, parentid = parentId);
//insertAttList.add(att);
mail.setFileAttachments(fileAttachments);
//Send email
If(parentId ==conjob.id){
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
}
//END : Send Email with Attachment
}
PageReference page = new PageReference('/AR_Jobs_ThankYou');
return page;
} 
catch (DMLException e) 
{
apexpages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please attach resume.'));
return null;
}
finally
{
attachment = new Attachment(); 
}
}
}