global  class GBL_BulkMergeBatch implements Database.Batchable<sObject>
{
    public List<WCT_sObject_Staging_Records__c> recordstohandle;
     //Constructor to prepare Query to fetch Case Records
    global GBL_BulkMergeBatch(List<WCT_sObject_Staging_Records__c> recordstohandle)
    {
        this.recordstohandle = recordstohandle;
    }

    //Start Method ,it returns a list of cases which needs to be updated to Execute method
    global List<WCT_sObject_Staging_Records__c> start(Database.BatchableContext BC)
    {
            return this.recordstohandle;
    }

    //Execute method to update case's
    global void execute(Database.BatchableContext BC,List<WCT_sObject_Staging_Records__c> recordstohandle)
    {
        for(WCT_sObject_Staging_Records__c stagging: recordstohandle)
        {
            GBL_Merge_Record_Bulk_Handler handler= new GBL_Merge_Record_Bulk_Handler(stagging.MR_Object_Type__c, stagging.MR_Source_Record_Name__c , stagging.MR_Source_Record_Id__c, stagging.MR_Destination_Record_Name__c, stagging.MR_Destination_Record_Id__c, stagging.MR_Merge_Config_Name__c, stagging.id);
        }
    }
    
    //Finish method to send email and do post execution task's on completion of batch
    global void finish(Database.BatchableContext BC ) 
    {
        // Get the ID of the AsyncApexJob representing this batch job
       // from Database.BatchableContext.
       // Query the AsyncApexJob object to retrieve the current job's information.
       AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
          TotalJobItems, CreatedBy.Email
          FROM AsyncApexJob WHERE Id =
          :BC.getJobId()];
       // Send an email to the Apex job's submitter notifying of job completion.
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       String[] toAddresses = new String[] {a.CreatedBy.Email};
       mail.setToAddresses(toAddresses);
       mail.setSubject('Bulk Merge Completed :' + a.Status);
       mail.setPlainTextBody
       ('Bulk merge batch is completed.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }

}