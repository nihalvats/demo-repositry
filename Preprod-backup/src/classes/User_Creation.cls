/********************************************************************************************************************************
Apex class         : User Creation
Description        : Creating User for the Contacts Created and assigning the User with a Basic Profile and Role 
Type               : Batch
Test Class         : Test_UserCreation

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01                 Deloitte                   27/05/2016          88%                         <Req / Case #>                  Original Version
************************************************************************************************************************************/

global class User_Creation implements Database.Batchable<sObject>,Database.Stateful

    {
        List<Exception_Log__c> Userlog=new List<Exception_Log__c>();//holds the exception log records
		global integer UsersCreated=0;  // holds the count of newly created users
		global integer UsersUpd=0;   // holds the count of  updated users
		//global list<user> CreatedUsers=new List<user>(); // holds the newly created users records for sending an email
	//	global list<user> UsersUpdated=new List<user>(); // holds the updated users records for sending an email 
		global boolean NewUser=false;
		global boolean PS=false;
		global boolean PSLicense=false;
        global boolean UpdtdUser=False;
        global Id UserLogId;
        global Id ExceptionId;
		global Id ContId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();//querying the record type 'employee' of contact 
		// global List<PermissionSetAssignment> psadata=new List<PermissionSetAssignment>(); //holds the permission set records for sending an email in finish method.
		global final String Query;
       //passing the query of the batch from scheduler in constructerr
        global User_Creation(string q)
		{
        Query=q;
        }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
 	{
        //querying the custom label to log the success in exception log object.
        string Logs=System.Label.User_Mangement_Report;
        //system.debug('dheuheuw'+Logs);
        //
       //holds the record type id of exception object to log the success
        Schema.DescribeSObjectResult Cas = Exception_Log__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames = Cas.getRecordTypeInfosByName();// getting the record Type Info
        UserLogId =rtMapByNames.get('User Management').getRecordTypeId();//particular RecordId by  Name
        //holds the record type id of exception object to log the failures.
        Schema.DescribeSObjectResult Cas1 = Exception_Log__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames1 = Cas1.getRecordTypeInfosByName();// getting the record Type Info
        ExceptionId =rtMapByNames1.get('Exception').getRecordTypeId();//particular RecordId by  Name
        
        list<string> strs=new List<string>();
        if(Logs != null)
         {
           strs =Logs.split(','); 
         }
        if(strs.size()>0)
         {
        for(string s:strs)
         {
            if(s=='NewUser')
             {
                NewUser=true;
             }
             else if(s=='UpdateUser')
			 {
                UpdtdUser=true;
             }
            else if(s=='PermissionSet')
			 {
                PS=true;
             }
            else if(s=='PSLicense')
			 {
             PSLicense=true;   
             }
        }
        }
        return Database.getQueryLocator(Query);
    } 
/********************************************************************************************
*Method Name         : Execute
*Return Type         : None
*Param’s             : None
*Description         : creation of users based on contact
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01                 Original Version
*********************************************************************************************/

    global void execute(Database.BatchableContext BC,List<contact> Names) 
	{
        try
            
        {
        List<User> ActiveUserId=new List<User>();
        list<string> ActiveUsers=new list<string>();//holds the emails of all active employees
        Map<string,id> EmailToUserMap = new Map<string,id>();//Map for validation of duplication of users
        List<Id> IdforPS=new List<Id>();//holds all the Ids of the active users
        Set<Id> IdforProfile=new Set<Id>();//holds the profile Ids of the active users
        Map<string,User> Inactive_Emails_Map=new Map<string,User>();//holds the emails of inactive users
        Map<string,User> Active_Emails_Map=new Map<string,User>();
        List<User> UserList=new List<User>();//holds the new users sobject
        List<User> UsrUpdate=new List<User>();//holds the users that needs to update
        List<User> UsrUpdate_Contact=new List<User>();//holds the users that needs to update the contact ID field in User
        List<PermissionSetAssignment> Psalst=new List<PermissionSetAssignment>();//holds the newly created permission set assignments
        list<id> New_Usrs_Id=new list<id>();//holds the Ids of newly created users 
        List<id> UpdatedUserId=new List<id>();
        List<PermissionSetAssignment> DltPSA=New List<PermissionSetAssignment>();
		Map<List<string>,PermissionSetAssignment> PermSetforDelete=New Map<list<string>,PermissionSetAssignment>();
	    List<PermissionSetLicenseAssign> PSL=new list<PermissionSetLicenseAssign>(); //permission set license assignment to handle the extra permissions 
        List<PermissionSetLicenseAssign> PSLcs=new list<PermissionSetLicenseAssign>();
		List<PermissionSetAssignment> PSforDelete=new List<permissionSetAssignment>();
		List<permissionsetAssignment> CreatedPS=new List<permissionsetAssignment>();
        
		string pr = System.Label.Default_Profile_For_Employees; //basic profile that is common to all the newly created Users
        string PermSt=System.Label.Default_PS_For_Employees; //basic permission set that is common to all the newly created Users
        string RoleId=System.Label.Default_Role_For_Employees;//basic role
        string PSLc=system.label.Permission_set_license_Id;//holds the permission set license Id
        
        
            
        system.debug('exception id is'+ExceptionId);
        system.debug('exception id is'+UserLogId);
        Map<String,String> timeZoneForUser=new Map<String,String>(); // Region , TimeZonSidCode
        timeZoneForUser.put('EAST','America/New_York');
        timeZoneForUser.put('CENTRAL','America/Chicago');
        timeZoneForUser.put('WEST','America/Los_Angeles');
        timeZoneForUser.put('USI','Asia/Kolkata');
        /*looping through each contact and adding the email to a list */
        For(Contact em:Names)
		{
            if(em.Email!=null){
            string conemail=em.Email;
            ActiveUsers.add(conemail);
            }
        }
        
        //querying the active users based on emails of contact
        ActiveUserId=[select id,Email,UserName,Contact_ID__c,isActive,profileId,FederationIdentifier,CommunityNickName from User where email IN :ActiveUsers Order by createddate ASC];
       
        //if the user is active add to a map,if inactive add to another map.
        For(User u:ActiveUserId)
		{
            if(u.IsActive==true)
			 {
                IdforProfile.add(u.profileId);
                IdforPS.add(u.id);
                EmailToUserMap.put(u.Email,u.id);
                Active_Emails_Map.put(u.email,u);
             }
            else
			 {
                Inactive_Emails_Map.put(u.email,u);
             }
        }
        
/********************************************************************************************
This below section is used to identify the transit user for a contact and will update them.      
********************************************************************************************/    
        For(contact ct: Names)
		{
            if(EmailToUserMap.containskey(ct.email))
			{
                //system.debug('user already exists 1');
                
               User UsrMap=Active_Emails_Map.get(ct.email); 
                //updating the contactId feild in User if that field is null.
                if(UsrMap.Contact_ID__c == null){
               UsrMap.Contact_ID__c=ct.id;
               UsrUpdate_Contact.add(UsrMap);
                }
            }
            else if(Inactive_Emails_Map.containskey(ct.email))
			{
/********************************************************************************************
Checking if the employee is in transit
by comparing the person Id and personnel number
If both the values are same,then the contact is a transit contact 
********************************************************************************************/
            string PId=ct.WCT_Person_Id__c;
            string PersonId=PId!=null?PId.remove(','):'';
            string PersonNum=ct.WCT_Personnel_Number1__c;
             system.debug('person id'+PersonId);
              system.debug('person munber'+PersonNum);
            
                /*
                 * Checking the personal Id or personal number is empty. 
                 * This is to cover the case where only Persond Id is coming from SAP not Personal Number. In this case PersonalId != personal Number, but it is not transit case.
                 * 
				*/
                
            if( (PersonId != PersonNum) && PersonNum!='' && PersonId!='' && PersonId!=null && PersonNum!=null  )
			{ 
                system.debug('user is1'+ct.email);
                 if(ct.WCT_Employee_Status__c=='Active'){
                        /*if the employee is in transit then activate the already existing de-activated user */
                        User UsrMap=Inactive_Emails_Map.get(ct.email);
                        string Nam=UsrMap.UserName;
                        if(UsrMap.IsActive==false)
                        {
                            UsrMap.IsActive=true;
                            if(UsrMap.Contact_ID__c == null){
                            UsrMap.Contact_ID__c=ct.id;
                            }
                            UsrUpdate.add(UsrMap);
                            IdforPS.add(UsrMap.id);
                            IdforProfile.add(UsrMap.profileId);
                            EmailToUserMap.put(UsrMap.email,UsrMap.id);
                        }
                    }
                }
            
            
/********************************************************************************************
If the employee is not in transit 
then update the already existed de-activated user with '.ex' 
*********************************************************************************************/
            else
            {
                system.debug('user is2'+ct.email);
                User UsrMap=Inactive_Emails_Map.get(ct.email);
                    string Nam=UsrMap.UserName;
                    if(!Nam.contains('prd.ex'))
                    {
                        system.debug('Nam'+Nam);
                        UsrMap.UserName=UsrMap.UserName+'.ex';
                        UsrMap.FederationIdentifier=UsrMap.FederationIdentifier+'.ex';
                        UsrMap.CommunityNickName=UsrMap.CommunityNickName+'.ex';
                        // This will link the existing User (might be of old sapeareted contact) as well with the current contact Id.
                        // This will give wrong data set. 
                        
                      /*  if(UsrMap.Contact_ID__c == null){
                        UsrMap.Contact_ID__c=ct.id;
                        }
						*/
                        UsrUpdate.add(UsrMap);
                    }
                }
            }
           
        }
/******************************************************************************************** 
Updating the list of already existing users of status 'in-active'
if he is a transit user or already assigned Old users with same email.
******************************************************************************************/
        if (UsrUpdate.size()>0)
		{
            //System.debug('Updating the existing user');
            Database.DMLOptions dlo = new Database.DMLOptions();
            dlo.EmailHeader.triggerUserEmail = false;
            Database.SaveResult[] UpUserList = Database.update(UsrUpdate, dlo);
            for (Integer i=0;i<UpUserList.size();i++) 
			{
                if(UpUserList.get(i).isSuccess())
				{
                 UpdatedUserId.add(UpUserList.get(i).getId());
                // UsersUpdated.add(UsrUpdate.get(i));
                 string Us_name=UsrUpdate.get(i).Username;
                //logging the result
                 if(UpdtdUser==true)
					{
                        if(Us_name.contains('prd.ex')){      
                Userlog.add(New Exception_Log__c(Exception_Date_and_Time__c=system.now(),Action__c='Update mocked User',User_Description__c='Updating the already existing user'+UsrUpdate.get(i).Email,RecordTypeId= UserLogId,Running_User__c = UserInfo.getUserId(),Status__c='Success') );    
                    }else{      
                Userlog.add(New Exception_Log__c(Exception_Date_and_Time__c=system.now(),Action__c='Update Transit User',User_Description__c='Updating the transit user '+UsrUpdate.get(i).Email,RecordTypeId= UserLogId,Running_User__c = UserInfo.getUserId(),Status__c='Success') );    
                    }
                    }
                }
                else 
				{
                    for (Database.Error err : UpUserList.get(i).getErrors())
					{
                      string msg='Error while updating the already existing user : '+UsrUpdate.get(i)+':'+ err.getStatusCode() + ' :' + err.getMessage();
                      Userlog.add(New Exception_Log__c(className__c='User Creation',pageName__c='Batch Class',Exception_Date_and_Time__c=system.now(),Detailed_Exception__c=msg,RecordTypeId= ExceptionId,Running_User__c = UserInfo.getUserId()) );    
                   
                    }
                }
            }
        } 
/******************************************************************************************** 
Updating the list of already existing users with contact Id feild.
******************************************************************************************/
        
         if (UsrUpdate_Contact.size()>0)
		{
            //System.debug('Updating the existing user');
            
            Database.SaveResult[] UpUserList_Contact = Database.update(UsrUpdate_Contact,false);
            for (Integer i=0;i<UpUserList_Contact.size();i++) 
			{
                if(UpUserList_Contact.get(i).isSuccess())
				{
                }
                else 
				{
                    for (Database.Error err : UpUserList_Contact.get(i).getErrors())
					{
                      string msg='Error while updating the already existing user with contact ID feild : '+UsrUpdate_Contact.get(i)+':'+ err.getStatusCode() + ' :' + err.getMessage();
                      Userlog.add(New Exception_Log__c(className__c='User Creation',pageName__c='Batch Class',Exception_Date_and_Time__c=system.now(),Detailed_Exception__c=msg,RecordTypeId= ExceptionId,Running_User__c = UserInfo.getUserId()) );    
                   
                    }
                }
            }
        } 
        //count of the number of users that are updated by the batch
        UsersUpd=UsersUpd+UpdatedUserId.size();
        
       /********************************************************************************************
        This below section will create new users if the user doesn't exist for a contact.
        *******************************************************************************************/
        
        For(Contact c:Names)
		{
            //to avoid user creation for non deloitte users.
            if(c.Email!=null && c.Email.containsIgnoreCase('@deloitte.com')){
            if(!EmailToUserMap.containskey(c.email))
			{
                //creating new users
                //System.debug('Case::User Does not Exist');
                //system.debug('map is'+c.email);
                string cusernm=c.Email;
                string frstnm=c.FirstName;
                User u = new User ();
                u.FirstName = c.FirstName;
                u.LastName = c.LastName;
                u.UserName=c.Email+'.wct.prd';
                u.ProfileId=pr;
                u.UserRoleId=RoleId;
                u.Email=c.Email;
                u.FederationIdentifier=(c.Email).split('@')[0];
                
                /*Alias Calculation :
                 * 1. Firt Character of First Name. 
                 * 2. Three Characters of Last Name, if atleast 3 characters Or 2 or 1
                 * 
				*/
                u.Alias='';
                if(c.FirstName!=null && c.FirstName!='')
                {
                    u.Alias=(c.FirstName).substring(0,1);
                }
                if(c.LastName.length()>=3)
                {
                   u.Alias=u.Alias+ c.LastName.substring(0,3);
                }
                if(c.LastName.length()==2)
                {
                   u.Alias=u.Alias+ c.LastName.substring(0,2);
                }
                if(c.LastName.length()==1)
                {
                   u.Alias=u.Alias+ c.LastName.substring(0,1);
                }
                
                u.CommunityNickName=(c.Email).split('@')[0];
                u.IsActive=true;
                if(c.WCT_Region__c == null || c.WCT_Region__c==''){
                u.TimeZoneSidKey='America/El_Salvador';
                }else{
                string region=c.WCT_Region__c;
                    if(region.containsIgnoreCase('USI')){
                u.TimeZoneSidKey=timeZoneForUser.get('USI');
                    }else  if(region.containsIgnoreCase('WEST')){
                u.TimeZoneSidKey=timeZoneForUser.get('WEST');
                    }else  if(region.containsIgnoreCase('EAST')){
                u.TimeZoneSidKey=timeZoneForUser.get('EAST');
                    }else  if(region.containsIgnoreCase('CENTRAL')){
                u.TimeZoneSidKey=timeZoneForUser.get('CENTRAL');
                    }else{
                   u.TimeZoneSidKey='America/El_Salvador';     
                    }
                }
                u.LanguageLocaleKey='en_US';
                u.LocaleSidKey='en_US';
                u.Contact_ID__c=c.id;
                u.EmailEncodingKey ='ISO-8859-1';
                u.EmailPreferencesAutoBcc=false;
                u.EmailPreferencesAutoBccStayInTouch=false;
                u.EmailPreferencesStayInTouchReminder=true;
                u.UserPreferencesTaskRemindersCheckboxDefault=true;
                u.ReceivesAdminInfoEmails=false;
                u.ReceivesInfoEmails=false;
                u.UserPreferencesEventRemindersCheckboxDefault=true;
                u.Title=c.Email;
                //adding to the list
                IdforProfile.add(pr);
                UserList.add(u);
                
            }
            }
        }
         
        
        /********************************************************************************************
        This below section is about the insertion operation for the new users
         and upon creation will assign Permission Set license 
        *******************************************************************************************/
        if (UserList.size()>0)
		{
            //System.debug('Creating New User');
            Database.DMLOptions dlo = new Database.DMLOptions();
            dlo.EmailHeader.triggerUserEmail = false;
            Database.SaveResult[] NewUserList = Database.insert(UserList, dlo);
            for (Integer i=0;i<NewUserList.size();i++ )
			{
                if(NewUserList.get(i).isSuccess())
				{
                    New_Usrs_Id.add(NewUserList.get(i).getId());
                    IdforPS.add(NewUserList.get(i).getId());
                    EmailToUserMap.put(UserList.get(i).Email,NewUserList.get(i).getId());
                  //  CreatedUsers.add(UserList.get(i));
                    if(NewUser==true)
					{
                     Userlog.add(New Exception_Log__c(Exception_Date_and_Time__c=system.now(),Action__c='NewUser Creation',User_Description__c='Creating a new user'+UserList.get(i).Email,RecordTypeId= UserLogId,Running_User__c = UserInfo.getUserId(),Status__c='Success') );    
                    }
                }
                else 
				{
                    for (Database.Error err : NewUserList.get(i).getErrors())
					{
                       string exmsg=err.getMessage();
                       string msg='Error while creating the new user at : '+ UserList.get(i)+':'+ err.getStatusCode() + ' :' + err.getMessage();
                       Userlog.add(New Exception_Log__c(className__c='User Creation',pageName__c='Batch Class',Exception_Date_and_Time__c=system.now(),Detailed_Exception__c=msg,RecordTypeId= ExceptionId,Running_User__c = UserInfo.getUserId()) );    
                    }   
                }
            }
        } 
        //count of the number of users created by the batch
        UsersCreated=UsersCreated+New_Usrs_Id.size();
        //system.debug('newly created users are new'+CreatedUsers);

/********************************************************************************************
This below section will be used to delete a permission set that got assigned through the restricted apps
feilds in the contact.
*******************************************************************************************/
               
        //custom settings of rescricted Apps.
        Map<String,User_Permission_sets__c> AllCodes = User_Permission_sets__c.getAll();
        list<string> Ids=new list<string>();
        for(User_Permission_sets__c m: AllCodes.values())
		{
            Ids.add(m.PS_For_Salesforce_User__c);
            Ids.add(m.PS_For_SalesforcePlatform_User__c);
        }
        //querying all the permission sets of active users
        DltPSA=[select id,AssigneeId,PermissionSetId from PermissionSetAssignment where PermissionSetId IN :Ids and AssigneeId In :IdforPS];  

        For(PermissionSetAssignment d:DltPSA)
		{
          List<string> dl=new List<string>();
          dl.add(d.AssigneeId);
          dl.add(d.PermissionSetId);
          PermSetforDelete.put(dl,d);
        }
        
        
/********************************************************************************************
This below section will be used to identify which user is assigned to which salesforce license.
        
*******************************************************************************************/
        Map<Id,string>  prof_lic_map  = new map<id,string>();
        for(profile prf:[select Id, userlicenseId,UserLicense.Name from profile where id IN :IdforProfile])
		{
            // system.debug(''+prf.UserLicense.Name);
            prof_lic_map.put(prf.id,prf.UserLicense.Name);
        }
        
        //Mapping of userIds with Userlicense Name
        Map<id,string>  Licence_Names = new map<id,string>(); 
        for(User us:[select id,profileId from user where id IN :IdforPS])
		{
            string licId = prof_lic_map.get(us.profileId);
            if(licId !=null)
			{
                Licence_Names.put(us.id,licId);  
            }
        }

         
/********************************************************************************************
Now every contact will have a user.
*******************************************************************************************/
/********************************************************************************************
This below section is used to assign the default 'permission set' to all the 
existing employees based on the license type for that user.
If that user is a salesforce licensed type,it will just assign only permission set.If not,then assign 'Permission Set License'.
*******************************************************************************************/
           
        For(contact c :Names)
		   {
              //system.debug('perm set loop'+c.name);
              string psrecd=Licence_Names.get(EmailToUserMap.get(c.Email));
               //system.debug('license type is'+psrecd);
              if(psrecd=='salesforce')
			  {
                        PermissionSetAssignment psa = new PermissionSetAssignment();
                        psa.PermissionSetId=PermSt;
                        psa.AssigneeId=EmailToUserMap.get(c.Email);
                        Psalst.add(psa);
              }
              else if(psrecd=='salesforce platform')
			  {
                        //permission set license assignment to handle the extra permissions
                        system.debug('enter the loop');
                        PermissionSetLicenseAssign p = new PermissionSetLicenseAssign();
                        p.PermissionSetLicenseId=PSLc;
                        p.AssigneeId=EmailToUserMap.get(c.Email);
                        PSLcs.add(p);
                       
              }     
/********************************************************************************************
This below section will insert the permission sets to the users based on the restricted App.
*******************************************************************************************/
            if(c.App_Assignment__c!= null)
			{
                string psvalue=(string) c.get('App_Assignment__c'); 
                list<string> strs =psvalue.split(';');
                // system.debug('list of ps in contact is'+strs); 
                for(string st:strs)
				{
                    User_Permission_sets__c strec = AllCodes.get(st);
                    string psrec=Licence_Names.get(EmailToUserMap.get(c.Email));
/********************************************************************************************
Assigning permission sets based on the profile of that User.
If the employee has salesforce liscence,then assign the permission set that is of salesforce
else assign platform liscence 
********************************************************************************************/
                    if(psrec=='salesforce')
					{
                        PermissionSetAssignment psa = new PermissionSetAssignment();
                        psa.PermissionSetId=strec.PS_For_Salesforce_User__c;
                        psa.AssigneeId=EmailToUserMap.get(c.Email);
                        Psalst.add(psa);
                    }
                    else if(psrec=='salesforce platform')
					{
                        
                        PermissionSetAssignment psa = new PermissionSetAssignment();
                        psa.PermissionSetId=strec.PS_For_SalesforcePlatform_User__c;
                        psa.AssigneeId=EmailToUserMap.get(c.Email);
                        Psalst.add(psa);
                    }
                }
            }
        } 
         if(PSLcs.size()>0)
		 {
            Database.SaveResult[] PermListSetl = Database.insert(PSLcs, false);
            for (Integer i=0;i<PermListSetl.size();i++)
			{
                if (PermListSetl.get(i).isSuccess())
				{
                    PermissionSetAssignment psa = new PermissionSetAssignment();
                    psa.PermissionSetId=PermSt;
                    psa.AssigneeId=PSLcs.get(i).AssigneeId;
                    Psalst.add(psa);
                    if(PSLicense==true)
					{
                     Userlog.add(New Exception_Log__c(Exception_Date_and_Time__c=system.now(),Action__c='PSL Creation',User_Description__c='Creating a new Permission set license'+PSLcs.get(i).AssigneeId,RecordTypeId= UserLogId,Running_User__c = UserInfo.getUserId(),Status__c='Success') );    
                    } 
                  }
                    else
					{
                    for (Database.Error err : PermListSetl.get(i).getErrors())
					{
                        string exmsg=err.getMessage();
                        if(exmsg.containsIgnoreCase('duplicate value found'))
						{
                            //system.debug('duplicate permission license assignment found');
                             PermissionSetAssignment psa = new PermissionSetAssignment();
                             psa.PermissionSetId=PermSt;
                             psa.AssigneeId=PSLcs.get(i).AssigneeId;
                             Psalst.add(psa);
                        }
                        else
						{
                            String msg='Error while assigning the permission set license assignment at : '+ PSLcs.get(i)+':'+ err.getStatusCode() + ' :' + err.getMessage();
                            Userlog.add(New Exception_Log__c(className__c='User Creation',pageName__c='Batch Class',Exception_Date_and_Time__c=system.now(),Detailed_Exception__c=msg,RecordTypeId= ExceptionId,Running_User__c = UserInfo.getUserId()) );    
                        }
                           
                     }
                   }
                }
            }
        
        
        Map<List<string>,Integer> deltps=new Map<list<string>,Integer>();
        if(psalst.size()>0)
		{
           for (Integer i=0;i<psalst.size();i++ )
		   { 
             List<string> plist =new List<string>();
             plist.add(psalst.get(i).AssigneeId);
             plist.add(psalst.get(i).PermissionSetId);
             deltps.put(plist,i);
           }
        }
        
        For(List<string>  dlt:PermSetforDelete.keySet())
		{
            // system.debug('123'+dlt);
            if(!deltps.containsKey(dlt))
			{
             PSforDelete.add(PermSetforDelete.get(dlt));  
            } 
        }
/********************************************************************************************
Deleting the permission set for a user if that permission doesn't exist in the Restricted App field in the contact
        
*******************************************************************************************/
        if(PSforDelete.size()>0)
		{
            Database.DeleteResult[] DltPSAList = Database.delete(PSforDelete, false);
            for (Integer i=0;i<DltPSAList.size();i++)
			{
                if (!DltPSAList.get(i).isSuccess())
				{
                    for (Database.Error err : DltPSAList.get(i).getErrors())
					{
                        String msg='Error while deleting the permission set at: '+ PSforDelete.get(i)+':'+ err.getStatusCode() + ' :' + err.getMessage();
                        Userlog.add(New Exception_Log__c(className__c='User Creation',pageName__c='Batch Class',Exception_Date_and_Time__c=system.now(),Detailed_Exception__c=msg,RecordTypeId= ExceptionId,Running_User__c = UserInfo.getUserId()) );    
                    } 
                }
            }
        }
        
        List<id> psdata=new List<id>();
        /********************************************************************************************
        Insertion Operation for the permission sets.
        *******************************************************************************************/
        if(psalst.size()>0)
		{
            Database.SaveResult[] permList = Database.insert(psalst, false); 
            for (Integer i=0;i<permList.size();i++ )
			{
                if(permList.get(i).isSuccess())
				{
                  psdata.add(permList.get(i).getId()); 
                    if(PS==true)
					{  
                  Userlog.add(New Exception_Log__c(Exception_Date_and_Time__c=system.now(),Action__c='PSCreation',User_Description__c='Creating:Permission set for User Id'+psalst.get(i).AssigneeId+'and PS id'+psalst.get(i).PermissionSetId,RecordTypeId= UserLogId,Running_User__c = UserInfo.getUserId(),Status__c='Success') );    
                    }     
                }
                else 
				{
                    for (Database.Error err : permList.get(i).getErrors())
					{
                         string exmsg=err.getMessage();
                        if(exmsg.containsIgnoreCase('duplicate PermissionSetAssignment'))
						{
                            //system.debug('duplicate permission set assignment found');
                        } 
                        else
						{
                        String msg='Error while assigning the permission set at: '+ psalst.get(i)+':'+ err.getStatusCode() + ' :' + err.getMessage();
                        Userlog.add(New Exception_Log__c(className__c='User Creation',pageName__c='Batch Class',Exception_Date_and_Time__c=system.now(),Detailed_Exception__c=msg,RecordTypeId= ExceptionId,Running_User__c = UserInfo.getUserId()) );    
                        }        
                    }
                }
            }
    }
     //querying the permissionsetAssignment to know who are assigning to which permission set for the talent administrator.
     /*   if(psdata.size()>0)
		{   
           CreatedPS=[select Assignee.Name,PermissionSet.Name from PermissionSetAssignment where id in :psdata];
           //system.debug('perm test'+CreatedPS[0].Assignee.Name);
           For(PermissionSetAssignment p:CreatedPS)
		   {
            psadata.add(p);
           }
        }
*/
            
        }
        Catch(Exception e)
        {
            System.debug('Final catch of Exception');
             Userlog.add(New Exception_Log__c(className__c='User Creation',pageName__c='Batch Class',Exception_Date_and_Time__c=system.now(),Detailed_Exception__c=e.getMessage(),RecordTypeId= ExceptionId,Running_User__c = UserInfo.getUserId()) );    
          
            
        }
    }
    
      
    global void finish(Database.BatchableContext BC)
	{
        String userName ='';
        string email='';
        string upduserName='';
        string updemail='';
        string sendemail=System.Label.User_Management_Emails;
        List<string> Sending=new List<String>();
        if(sendemail != null){
          Sending= sendemail.split(','); 
        }
        //inserting the exceptions into exceptions log
        
        AsyncApexJob a =[SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems,CreatedBy.Email,ExtendedStatus FROM AsyncApexJob WHERE Id =:BC.getJobId()];        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toadd=new String[]{'swchundu@deloitte.com','dginde@deloitte.com'};
            
        string td1='"border:1px solid green; width=200px;"';
        string td2='"width=200px; border:1px solid green; background-color:red; color:white; font-weight:bold;"';
        string tdHead='"border:1px solid green; width=200px; color:white; background-color:green; font-weight:bold;"';
        string htmlBody = '<div style="border:2px solid green; border-radius:15px;"><p>Hi,</p><p><span style="color:brown; font-weight:bolder;">Salesforce</span> completed running <b>Apex Batch Code</b>.</p>'
								+'<p>The batch Apex job processed '+ a.TotalJobItems +
												  ' batches with '+ a.NumberOfErrors + ' failures.</p>'
								+'<p>The  users information is below:</p>'+
								'<p>The number of  users created are '+UsersCreated+'</p>'+
								'<p>The number of  users updated are '+UsersUpd+'</p>';
        /*
								 '<p>The detailed information about the newly created users is below:</p>'
								+'<center><table style="border:3px solid green; border-collapse:collapse;">'
								+'<th style='+tdHead+'>userName</th><th style='+tdHead+'>Email</th>';
        //system.debug('newly created users are'+CreatedUsers);         
        for (User u : CreatedUsers)
          {
            htmlbody +='<tr><td style='+td1+'>'+u.userName+'</td><td style='+td1+'>'+u.email+'</td></tr>'; 
          }
            htmlbody +='</table></centre>';
             htmlbody +='<p>The detailed updated information about the existing users is below:</p>'
                      +'<center><table style="border:3px solid green; border-collapse:collapse;">'
                      +'<th style='+tdHead+'>userName</th><th style='+tdHead+'>Email</th><th style='+tdHead+'>IsActive</th>';
       // System.debug('Debug---'+ userName);
        for(user up:UsersUpdated)
		{
          htmlbody +='<tr><td style='+td1+'>'+up.userName+'</td><td style='+td1+'>'+up.email+'</td><td style='+td1+'>'+up.IsActive+'</td></tr>';   
        }
        htmlbody +='</table></centre>';
        htmlbody +='<p>The detailed information about the existing users permissions is as mentioned below:</p>'
                    +'<center><table style="border:3px solid green; border-collapse:collapse;">'
                    +'<th style='+tdHead+'>Name of the user</th><th style='+tdHead+'>Permission Name</th>';
        for(PermissionSetAssignment psd :psadata)
		{
          htmlbody +='<tr><td style='+td1+'>'+psd.Assignee.Name+'</td><td style='+td1+'>'+psd.PermissionSet.Name+'</td>';   
        }
        
        */
         htmlbody +='</table></centre></div>';
        mail.setToAddresses(toadd);
        mail.setHtmlBody(htmlBody);
        mail.setSubject('License Management Status: ' + a.Status);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail}); 
		
        //logging the exceptions through the batch
       database.executeBatch(new User_Login_Batch(Userlog)); 
	   
        /********************************************************************************************
         Calling another batch apex in finish method 
         This Logic is used to update the feild 'User_Management_category__c' of type checkbox in contact record  
         This logic will update the feild to false
         This is used to limit the number of records returned by the query
        
        *******************************************************************************************/
       
         database.executeBatch(new Contact_Update_Batch(Query)); 
        
    }
}