@isTest
private class WCT_Mobility_Onboard_FormController_Test {

public static testMethod void m2()
{
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.email='test@deloitte.com';
        insert con;
        
        Recordtype rtImm=[select id,name from recordtype where DeveloperName = 'B1_Visa'];
        Recordtype rt2=[select id,name from recordtype where DeveloperName = 'Business_Visa'];
        //String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');

        PageReference pageRef = Page.WCT_Travel_Request_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('type','Employee');
       
      
        datetime startdate=date.Today().adddays(10);
        datetime enddate=date.Today().adddays(20);
        
        contact con1=[SELECT Id,email FROM contact where Id =:con.id];
        
        WCT_Immigration__c immRec1  = new WCT_Immigration__c();
        immRec1.RecordTypeId = rtImm.id;
        immRec1.WCT_Immigration_Status__c= 'New';
        immRec1.WCT_Immigration_Closed__c= false; 
        immRec1.WCT_Visa_Type__c= 'B1/B2'; 
        immRec1.WCT_Assignment_Owner__c = con1.id;
        insert immRec1;
        
        WCT_Mobility__c mobRec1  = new WCT_Mobility__c();
        mobRec1.WCT_Mobility_Status__c = 'New';
        mobRec1.RecordTypeId = rt2.id;
        mobRec1.WCT_Assignment_Type__c = 'Short Term';
        mobRec1.WCT_Existing_Business_Visa__c='Yes';
        mobRec1.WCT_Previous_Employer_Visa__c=true;
        mobRec1.WCT_First_Working_Day_in_US__c = startdate.date(); 
        mobRec1.WCT_Last_Working_Day_in_US__c = enddate.date(); 
        mobRec1.WCT_Mobility_Employee__c = con1.id;
        
        mobRec1.WCT_USI_Resource_Manager__c= con1.email;
        mobRec1.WCT_Travel_Start_Date__c =startdate.date(); 
        mobRec1.WCT_Travel_End_Date__c = enddate.date(); 
        mobRec1.WCT_Purpose_of_Travel__c='Client Travel';
        mobRec1.WCT_India_Cellphone__c='999999999';
        insert mobRec1;
        
        WCT_Port_of_Entry__c portRec1  = new WCT_Port_of_Entry__c();
        portRec1.WCT_MobilityRel_ID__c= mobRec1.id;
        portRec1.WCT_Mobility_ID__c= mobRec1.name;
        portRec1.WCT_Contact_ID__c= con1.id;
        insert portRec1;
        
        WCT_Task_Reference_Table__c taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.Form_Verbiage__c = 'Hi. This is Test.';
        insert taskRef;
       
        Task t=WCT_UtilTestDataCreation.createTask(mobRec1.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=false;
        t.WCT_Task_Reference_Table_ID__c = taskRef.Id;
        insert t; 
                
        Test.starttest();
        
        
        WCT_Mobility_Onboarding_FormController  controller=new WCT_Mobility_Onboarding_FormController();
        controller=new WCT_Mobility_Onboarding_FormController();
        controller.urlEmployeeId=mobRec1.id;
        controller.employeeEmail=con1.email;
        controller.BusinessVisa='Yes';
        controller.SSNValue='Yes';
        controller.employeePrevious=true;
        
        controller.taskid=t.id;
        
        
        
        controller.init();
        controller.redirectBusiness();
        controller.getTaskInstance();
        controller.getEmployeeDetails();
        controller.getMobilityDetails();
        controller.BusinessVisa='Green Card/ US Citizen';
        controller.ggsValue = 'Yes';
        controller.SSNValue='Yes'; 
        controller.employeePrevious=true; 
        
        controller.poeResult = true;
        
        controller.VisaStart=''; 
        controller.VisaExpiration = ''; 
       
        controller.dateofArrival=enddate.format('MM/dd/yyyy'); 
        controller.StartDate1 =startdate.format('MM/dd/yyyy'); 
        controller.EndDate1 =enddate.format('MM/dd/yyyy'); 
        
        controller.StartDate2 =startdate.format('MM/dd/yyyy'); 
        controller.EndDate2 =enddate.format('MM/dd/yyyy'); 
        
        
            controller.StartDate3 =startdate.format('MM/dd/yyyy'); 
        controller.EndDate3 =enddate.format('MM/dd/yyyy'); 
        
            controller.StartDate4 =startdate.format('MM/dd/yyyy'); 
        controller.EndDate4 =enddate.format('MM/dd/yyyy'); 
        
            controller.StartDate5 =startdate.format('MM/dd/yyyy'); 
        controller.EndDate5 =enddate.format('MM/dd/yyyy');     
        
        
             controller.DateArrival =startdate.format('MM/dd/yyyy'); 
        controller.DateDeparture =enddate.format('MM/dd/yyyy'); 
        
            controller.previousstartdate=startdate.format('MM/dd/yyyy'); 
        controller.previousenddate=enddate.format('MM/dd/yyyy');
        
        controller.travelstartdate=startdate.format('MM/dd/yyyy'); 
        controller.travelenddate=enddate.format('MM/dd/yyyy');
        controller.lastWorkingDayUS=enddate.format('MM/dd/yyyy');
        controller.TravelExtendedDuration = 30;
        controller.portRecord.WCT_Email_Address__c ='test@deloitte.com';
    
        controller.employeeEmail='test@deloitte.com';
        controller.portRecord.WCT_USI_Resource_Manager__c ='test@deloitte.com';
        controller.PortRecord.WCT_Purpose__c= 'Test';
        controller.PortRecord.WCT_Port_of_Entry_City__c= 'chennai';
        controller.PortRecord.WCT_Port_of_Entry_State__c='TN';
        controller.portRecord.WCT_Email_Address__c ='test@deloitte.com';
        controller.PortRecord.WCT_Travel_Start_Date__c = startdate.date();
        controller.PortRecord.WCT_Travel_End_Date__c = enddate.date();
           controller.PortRecord.WCT_Start_Date__c=startdate.date();
        controller.PortRecord.WCT_End_Date__c=enddate.date();
        controller.PortRecord.WCT_Start_Date2__c=startdate.date();
        controller.PortRecord.WCT_End_Date2__c=enddate.date();
        
        controller.PortRecord.WCT_Start_Date3__c=startdate.date();
        controller.PortRecord.WCT_End_Date3__c = enddate.date();
        controller.PortRecord.WCT_Start_Date4__c=startdate.date();
        controller.PortRecord.WCT_End_Date4__c=enddate.date();
        controller.PortRecord.WCT_Start_Date5__c=startdate.date();
        controller.PortRecord.WCT_End_Date5__c = enddate.date();
        
        
        
        controller.MobilityRecord.WCT_Start_Date1__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date1__c=enddate.date();     
        controller.MobilityRecord.WCT_Start_Date2__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date2__c=enddate.date();
        
        controller.MobilityRecord.WCT_Start_Date3__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date3__c = enddate.date();
        controller.MobilityRecord.WCT_Start_Date4__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date4__c=enddate.date();
        controller.MobilityRecord.WCT_Start_Date5__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date5__c = enddate.date();
        controller.saveMobilityRecord();
        controller.saveogcdetails();
    
    controller.VisaExpiration='test';
    controller.VisaStart='test';
   
    
    controller.MobilityRecord.WCT_Existing_Business_Visa__c='Yes';
    controller.MobilityRecord.WCT_Travel_Start_Date__c=System.today()+20;
    controller.MobilityRecord.WCT_Travel_End_Date__c=System.today();
    controller.MobilityRecord.WCT_Visa_expiration_date__c=System.today();
    controller.MobilityRecord.WCT_Visa_start_date__c=System.today()+15;
    controller.saveMobilityRecord();
    
    controller.ggsValue=null;
    controller.saveMobilityRecord();
    
    
    
    
    
    
    
    
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        
        Test.stopTest();

}
public static testMethod void m3()
{
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.email='test@deloitte.com';
        insert con;
        
        Recordtype rtImm=[select id,name from recordtype where DeveloperName = 'B1_Visa'];
        Recordtype rt2=[select id,name from recordtype where DeveloperName = 'Business_Visa'];
        //String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');


        PageReference pageRef = Page.WCT_Travel_Request_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('type','Employee');
        
        datetime startdate=date.Today().adddays(10);
        datetime enddate=date.Today().adddays(20);
        
        contact con1=[SELECT Id,email FROM contact where Id =:con.id];
        
        WCT_Immigration__c immRec1  = new WCT_Immigration__c();
        immRec1.RecordTypeId = rtImm.id;
        immRec1.WCT_Immigration_Status__c= 'New';
        immRec1.WCT_Immigration_Closed__c= false; 
        immRec1.WCT_Visa_Type__c= 'B1/B2'; 
        immRec1.WCT_Assignment_Owner__c = con1.id;
        insert immRec1;
        
        WCT_Mobility__c mobRec1  = new WCT_Mobility__c();
        mobRec1.WCT_Mobility_Status__c = 'New';
        mobRec1.RecordTypeId = rt2.id;
        mobRec1.WCT_Assignment_Type__c = 'Short Term';
        mobRec1.WCT_Existing_Business_Visa__c='Yes';
        mobRec1.WCT_Previous_Employer_Visa__c=true;
        mobRec1.WCT_First_Working_Day_in_US__c = startdate.date(); 
        mobRec1.WCT_Last_Working_Day_in_US__c = enddate.date(); 
        mobRec1.WCT_Mobility_Employee__c = con1.id;
        
        mobRec1.WCT_USI_Resource_Manager__c= con1.email;
        mobRec1.WCT_Travel_Start_Date__c =startdate.date(); 
        mobRec1.WCT_Travel_End_Date__c = enddate.date(); 
        mobRec1.WCT_Purpose_of_Travel__c='Client Travel';
        mobRec1.WCT_India_Cellphone__c='999999999';
     
        insert mobRec1;
        
        WCT_Port_of_Entry__c portRec1  = new WCT_Port_of_Entry__c();
        portRec1.WCT_MobilityRel_ID__c= mobRec1.id;
        portRec1.WCT_Mobility_ID__c= mobRec1.name;
        portRec1.WCT_Contact_ID__c= con1.id;
        insert portRec1;
        

        WCT_Task_Reference_Table__c taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.Form_Verbiage__c = 'Hi. This is Test.';
        insert taskRef;
       
        Task t=WCT_UtilTestDataCreation.createTask(mobRec1.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=false;
        t.WCT_Task_Reference_Table_ID__c = taskRef.Id;
        insert t;                 
        Test.starttest();
        
        
        WCT_Mobility_Onboarding_FormController  controller=new WCT_Mobility_Onboarding_FormController();
        controller=new WCT_Mobility_Onboarding_FormController();
        ApexPages.currentPage().getParameters().put('taskid',t.id);
        controller.taskid=ApexPages.currentPage().getParameters().get('taskid');
      
        controller.urlEmployeeId=mobRec1.id;
        controller.employeeEmail=con1.email;
        controller.BusinessVisa='Yes';
        controller.SSNValue='Yes';
        controller.employeePrevious=true;
        
      controller.init();
        controller.redirectBusiness();
      //    controller.taskid=t.id;
        controller.getTaskInstance();
        controller.getEmployeeDetails();
        controller.getMobilityDetails();
        controller.BusinessVisa='Yes';
        controller.ggsValue = 'Yes';
        controller.SSNValue='Yes'; 
        controller.employeePrevious=true; 
        
        controller.poeResult = true;
        controller.VisaStart=startdate.format('MM/dd/yyyy'); 
        controller.VisaExpiration = enddate.format('MM/dd/yyyy'); 
       
        controller.dateofArrival=enddate.format('MM/dd/yyyy'); 
        controller.StartDate1 =startdate.format('MM/dd/yyyy'); 
        controller.EndDate1 =enddate.format('MM/dd/yyyy'); 
        
        controller.StartDate2 =startdate.format('MM/dd/yyyy'); 
        controller.EndDate2 =enddate.format('MM/dd/yyyy'); 
        
        
            controller.StartDate3 =startdate.format('MM/dd/yyyy'); 
        controller.EndDate3 =enddate.format('MM/dd/yyyy'); 
        
            controller.StartDate4 =startdate.format('MM/dd/yyyy'); 
        controller.EndDate4 =enddate.format('MM/dd/yyyy'); 
        
            controller.StartDate5 =startdate.format('MM/dd/yyyy'); 
        controller.EndDate5 =enddate.format('MM/dd/yyyy');     
        
        
             controller.DateArrival =startdate.format('MM/dd/yyyy'); 
        controller.DateDeparture =enddate.format('MM/dd/yyyy'); 
        
            controller.previousstartdate=startdate.format('MM/dd/yyyy'); 
        controller.previousenddate=enddate.format('MM/dd/yyyy');
        
        controller.travelstartdate=startdate.format('MM/dd/yyyy'); 
        controller.travelenddate=enddate.format('MM/dd/yyyy');
        controller.lastWorkingDayUS=enddate.format('MM/dd/yyyy');
        controller.TravelExtendedDuration = 30;
        controller.portRecord.WCT_Email_Address__c ='test@deloitte.com';
    
        controller.employeeEmail='test@deloitte.com';
        controller.portRecord.WCT_USI_Resource_Manager__c ='test@deloitte.com';
        controller.PortRecord.WCT_Purpose__c= 'Test';
        controller.PortRecord.WCT_Port_of_Entry_City__c= 'chennai';
        controller.PortRecord.WCT_Port_of_Entry_State__c='TN';
        controller.portRecord.WCT_Email_Address__c ='test@deloitte.com';
        controller.PortRecord.WCT_Travel_Start_Date__c = startdate.date();
        controller.PortRecord.WCT_Travel_End_Date__c = enddate.date();
           controller.PortRecord.WCT_Start_Date__c=startdate.date();
        controller.PortRecord.WCT_End_Date__c=enddate.date();
        controller.PortRecord.WCT_Start_Date2__c=startdate.date();
        controller.PortRecord.WCT_End_Date2__c=enddate.date();
        
        controller.PortRecord.WCT_Start_Date3__c=startdate.date();
        controller.PortRecord.WCT_End_Date3__c = enddate.date();
        controller.PortRecord.WCT_Start_Date4__c=startdate.date();
        controller.PortRecord.WCT_End_Date4__c=enddate.date();
        controller.PortRecord.WCT_Start_Date5__c=startdate.date();
        controller.PortRecord.WCT_End_Date5__c = enddate.date();
        
        
        
        controller.MobilityRecord.WCT_Start_Date1__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date1__c=enddate.date();     
        controller.MobilityRecord.WCT_Start_Date2__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date2__c=enddate.date();
        
        controller.MobilityRecord.WCT_Start_Date3__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date3__c = enddate.date();
        controller.MobilityRecord.WCT_Start_Date4__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date4__c=enddate.date();
        controller.MobilityRecord.WCT_Start_Date5__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date5__c = enddate.date();
        controller.MobilityRecord.WCT_Port_of_Entry_City__c='Texas';
        controller.MobilityRecord.WCT_Port_of_Entry_State__c='Texas';
        controller.saveMobilityRecord();
        controller.saveogcdetails();
    
        
    
        controller.MobilityRecord.WCT_USI_Point_of_Contact__c=con.id;
        controller.MobilityRecord.WCT_US_point_of_contact__c=con.id;
        controller.previousstartdate=(System.now()).format('MM/dd/yyyy');
        controller.previousenddate=(System.now()).format('MM/dd/yyyy');
        controller.saveogcdetails();
    
        controller.MobilityRecord.WCT_Travel_Start_Date__c= System.today()+1;
        controller.MobilityRecord.WCT_Travel_End_Date__c= System.today();
        controller.saveMobilityRecord();
    
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        
        Test.stopTest();

}

public static testMethod void m4()
{
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.email='test@deloitte.com';
        insert con;
        
        Recordtype rtImm=[select id,name from recordtype where DeveloperName = 'B1_Visa'];
        Recordtype rt2=[select id,name from recordtype where DeveloperName = 'Business_Visa'];
        //String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');

        
        PageReference pageRef = Page.WCT_Travel_Request_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('type','Employee');
        
        datetime startdate=date.Today().adddays(10);
        datetime enddate=date.Today().adddays(20);
        
        contact con1=[SELECT Id,email FROM contact where Id =:con.id];
        
        WCT_Immigration__c immRec1  = new WCT_Immigration__c();
        immRec1.RecordTypeId = rtImm.id;
        immRec1.WCT_Immigration_Status__c= 'New';
        immRec1.WCT_Immigration_Closed__c= false; 
        immRec1.WCT_Visa_Type__c= 'B1/B2'; 
        immRec1.WCT_Assignment_Owner__c = con1.id;
        insert immRec1;
        
        WCT_Mobility__c mobRec1  = new WCT_Mobility__c();
        mobRec1.WCT_Mobility_Status__c = 'New';
        mobRec1.RecordTypeId = rt2.id;
        mobRec1.WCT_Assignment_Type__c = 'Short Term';
        mobRec1.WCT_Existing_Business_Visa__c='Yes';
        mobRec1.WCT_Previous_Employer_Visa__c=true;
        mobRec1.WCT_First_Working_Day_in_US__c = startdate.date(); 
        mobRec1.WCT_Last_Working_Day_in_US__c = enddate.date(); 
        mobRec1.WCT_Mobility_Employee__c = con1.id;
        mobRec1.WCT_US_Point_of_Contact__c= con1.id;
        mobRec1.WCT_USI_Point_of_Contact__c= con1.id;
        
        mobRec1.WCT_USI_Resource_Manager__c= con1.email;
        mobRec1.WCT_Travel_Start_Date__c =startdate.date(); 
        mobRec1.WCT_Travel_End_Date__c = enddate.date(); 
        mobRec1.WCT_Purpose_of_Travel__c='Client Travel';
        mobRec1.WCT_India_Cellphone__c='999999999';
     
        insert mobRec1;
        
        WCT_Port_of_Entry__c portRec1  = new WCT_Port_of_Entry__c();
        portRec1.WCT_MobilityRel_ID__c= mobRec1.id;
        portRec1.WCT_Mobility_ID__c= mobRec1.name;
        portRec1.WCT_Contact_ID__c= con1.id;
        insert portRec1;
        

        WCT_Task_Reference_Table__c taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.Form_Verbiage__c = 'Hi. This is Test.';
        insert taskRef;
       
        Task t=WCT_UtilTestDataCreation.createTask(mobRec1.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=false;
        t.WCT_Task_Reference_Table_ID__c = taskRef.Id;
        insert t;                 
        Test.starttest();
        
        
        WCT_Mobility_Onboarding_FormController  controller=new WCT_Mobility_Onboarding_FormController();
        controller=new WCT_Mobility_Onboarding_FormController();
        controller.employeeRefresh=true;
        controller.beforeRefresh=true;
        controller.mobilityid=mobRec1.id;
        controller.DateTravelInsurance=enddate.format('MM/dd/yyyy');
        ApexPages.currentPage().getParameters().put('taskid',t.id);
        controller.taskid=ApexPages.currentPage().getParameters().get('taskid');
      
        controller.urlEmployeeId=mobRec1.id;
        controller.employeeEmail=con1.email;
        controller.BusinessVisa='Yes';
        controller.SSNValue='Yes';
        controller.employeePrevious=true;
          controller.saveogcdetails();
      controller.init();
        controller.redirectBusiness();
      //    controller.taskid=t.id;
        controller.getTaskInstance();
        controller.getEmployeeDetails();
        controller.getMobilityDetails();
        controller.BusinessVisa='Yes';
        controller.ggsValue = 'Yes';
        controller.SSNValue='Yes'; 
        controller.employeePrevious=true; 
        
        controller.poeResult = true;
        controller.VisaStart='';
        controller.VisaExpiration ='';
       
        controller.dateofArrival='';
        controller.StartDate1 ='';
        controller.EndDate1 =''; 
        
        controller.StartDate2 ='';
        controller.EndDate2 =''; 
        
        
            controller.StartDate3 ='';
        controller.EndDate3 ='';
        
            controller.StartDate4 ='';
        controller.EndDate4 ='';
        
            controller.StartDate5 ='';
        controller.EndDate5 ='';    
        
        
             controller.DateArrival ='';
        controller.DateDeparture =''; 
            controller.previousstartdate=startdate.format('MM/dd/yyyy'); 
        controller.previousenddate=enddate.format('MM/dd/yyyy');
        
        controller.travelstartdate='';
        controller.travelenddate='';
        controller.lastWorkingDayUS=enddate.format('MM/dd/yyyy');
        controller.TravelExtendedDuration = 30;
        controller.portRecord.WCT_Email_Address__c ='test@deloitte.com';
    
        controller.employeeEmail='test@deloitte.com';
        controller.portRecord.WCT_USI_Resource_Manager__c ='test@deloitte.com';
        controller.PortRecord.WCT_Purpose__c= 'Test';
        controller.PortRecord.WCT_Port_of_Entry_City__c= 'chennai';
        controller.PortRecord.WCT_Port_of_Entry_State__c='TN';
        controller.portRecord.WCT_Email_Address__c ='test@deloitte.com';
        controller.PortRecord.WCT_Travel_Start_Date__c = startdate.date();
        controller.PortRecord.WCT_Travel_End_Date__c = enddate.date();
           controller.PortRecord.WCT_Start_Date__c=startdate.date();
        controller.PortRecord.WCT_End_Date__c=enddate.date();
        controller.PortRecord.WCT_Start_Date2__c=startdate.date();
        controller.PortRecord.WCT_End_Date2__c=enddate.date();
        
        controller.PortRecord.WCT_Start_Date3__c=startdate.date();
        controller.PortRecord.WCT_End_Date3__c = enddate.date();
        controller.PortRecord.WCT_Start_Date4__c=startdate.date();
        controller.PortRecord.WCT_End_Date4__c=enddate.date();
        controller.PortRecord.WCT_Start_Date5__c=startdate.date();
        controller.PortRecord.WCT_End_Date5__c = enddate.date();
        
        
        
        controller.MobilityRecord.WCT_Start_Date1__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date1__c=enddate.date();     
        controller.MobilityRecord.WCT_Start_Date2__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date2__c=enddate.date();
        
        controller.MobilityRecord.WCT_Start_Date3__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date3__c = enddate.date();
        controller.MobilityRecord.WCT_Start_Date4__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date4__c=enddate.date();
        controller.MobilityRecord.WCT_Start_Date5__c=startdate.date();
        controller.MobilityRecord.WCT_End_Date5__c = enddate.date();
        controller.MobilityRecord.WCT_Port_of_Entry_City__c='Texas';
        controller.MobilityRecord.WCT_Port_of_Entry_State__c='Texas';

        controller.saveMobilityRecord();
    
    
        controller.employeeEmail='';
        controller.getEmployeeDetails();
      
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        controller.supportAreaErrorMesssage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        
        Test.stopTest();

} 

}