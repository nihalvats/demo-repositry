public class WCT_LeaveTriggerHandler{

    public static boolean isAlreadyRun = false;
   
       
   public static void HolidayCalculation(List<WCT_Leave__c> lea){
       for(WCT_Leave__c lv : lea){
           Date StartDate = lv.WCT_Leave_Start_Date__c;
           Date EndDate = lv.WCT_Leave_End_Date__c;
           
           list<US_Holidays__c> trtd = US_Holidays__c.getall().values();//custom setting all values
           list<USI_Holidays__c> USIHOL = USI_Holidays__c.getall().values();
           integer i =0;
           if(lv.WCT_User_Group__c == 'United States'){
                for(US_Holidays__c  h : trtd){
                    if(h.Holiday_Date__c >= StartDate && h.Holiday_Date__c <= EndDate)
                    {
                      i ++;
                    }
                }
            }
            else if(lv.WCT_User_Group__c == 'US GLS India'){
                for(USI_Holidays__c  USI : USIHOL){
                    if(USI.Holiday_Date__c >= StartDate && USI.Holiday_Date__c <= EndDate)
                    {
                      i ++;
                    }
                }
            }
            lv.WCT_Leave_Holidays__c = i;
       }
   }
    
    
}