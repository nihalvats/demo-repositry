public class Alum_VF_Tab_Controller {

    public List<Contact> contacts = new List<Contact>();
    Id recTypeId;
    public string ctype;
    public Alum_VF_Tab_Controller(ApexPages.StandardController controller) {
       
       recTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
       contacts = [Select Name,WCT_Person_Id__c,CreatedDate,WCT_Region__c,WCT_Start_Date2__c,Adopted_Ada_c__c,WCT_Type__c,WCT_Terminated_Date__c,WCT_Employee_Status__c,AR_Current_Company__c 
                    From Contact
                    Where WCT_Type__c =:'Separated' limit 1000];
       
      
    }
    
    public List<Contact> getContacts(){
        return contacts;
    }
     public pageReference redToDetail()      
    {           
        PageReference viewPage = new PageReference('/'+ApexPages.currentPage().getParameters().get('cid'));
        viewPage.setRedirect(true);           
        return viewPage;      
    }
    
     public PageReference createNew()
    {
        
        PageReference newEditPage = new PageReference('/'+'003/e'+'?RecordType='+recTypeId+'&ent=Contact');
        newEditPage.setRedirect(true);           
        return newEditPage; 
    
    }

}