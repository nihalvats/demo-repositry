public class AdA_validatePractitioner{
    private Invalid_Practitioner__c practitioner;
    public contact con{get;set;}
    public Boolean showValidated{get;set;}
     public string showexcep{get;set;}
     public id pracRecTypeId;
         public AdA_validatePractitioner(ApexPages.StandardController stdController) {
        pracRecTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Practitioner').getRecordTypeId();
        this.practitioner= (Invalid_Practitioner__c)stdController.getRecord();
        practitioner=[select id,Employee_ID__c,First_Name__c,Last_Name__c,Validated__c ,email__c,Function__c,Office__c from Invalid_Practitioner__c where id=:practitioner.id];
        con= new contact();
        try{
            con=[select id,name,WCT_Person_Id__c from contact where WCT_Person_Id__c=:practitioner.Employee_ID__c and recordtypeId=:pracRecTypeId and AR_Deloitte_Email__c=:practitioner.Email__c limit 1];
            showValidated=true;
        }
        catch(exception e){
        
            showValidated=false;
            
        }
    }
    
    public pagereference validate(){
        if(!showValidated){
            
            con.firstName=practitioner.First_Name__c;
            con.LastName=practitioner.Last_Name__c;
            con.WCT_Person_Id__c=practitioner.Employee_ID__c;
            con.WCT_Function__c=practitioner.Function__c;
            con.WCT_Employee_Status__c  ='Active';
            con.WCT_Office_City_Personnel_Subarea__c=practitioner.Office__c;
            con.AR_Deloitte_Email__c=practitioner.Email__c;
            con.recordtypeId=pracRecTypeId;
            insert con;
        }
        if(!practitioner.validated__c){
            practitioner.validated__c=true;
                       
        }
        practitioner.Practitioner__c=con.id;
        update practitioner; 
        return null;
    }
     
    public pagereference conDetailPage(){
        return (new  pagereference('/'+con.id));
    }
}