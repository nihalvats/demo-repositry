public class ProjectTrigger_handler {
   public static boolean isSecoundRun=false; 
   
   

    public static void  Pipeline(list<project__c> pro)
    {
            List<Project__c> pjt=new List<Project__c>();

    Schema.DescribeSObjectResult projt = Project__c.SObjectType.getDescribe();// getting Sobject Type
    Map<String,Schema.RecordTypeInfo> rtMapByNames = projt.getRecordTypeInfosByName();// getting the record Type Info
    Id caseRtId =rtMapByNames.get('Analytics Refresh Scheduler').getRecordTypeId();//particular RecordId by  Name
            
        For(Project__c pa : pro)
            {
                system.debug('(pa.Status__c  '+pa.Status__c );
                System.debug('# pa.Status__c check '+pa.Status__c == 'Completed (Move to Refresher)');
                    if (pa.Status__c == 'Completed (Move to Refresher)')
                        {
                            Project__c p = new Project__c ();
                            p=pa.clone();
                            p.recordtypeid =caseRtId;
                            p.Status__c='Not Started';
                            pjt.add(p);
                        }
            }
        insert pjt;
        system.debug('The newly created record id is -------------->'+pjt);
        }
        
            
}