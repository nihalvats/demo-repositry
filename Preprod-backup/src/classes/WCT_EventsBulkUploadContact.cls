public class WCT_EventsBulkUploadContact {
    
    public integer totalrecords { get; set; }
    public integer totalsuccessrec { get; set; }
    public integer totalsuccessreccount { get; set; }
    public integer totalunsuccessrec { get; set; }
    public integer totalalreadypresent { get; set; }
    
    transient public String nameFile { get; set; }
    
    public String eventid{get;set;}
    public String RecTypeName{get;set;}
    public String eventRecTypeId{get;set;}
    public String Event_RegistrationRecTypeId{get;set;}
    
    transient public list<contact> lcon = new list<contact> ();
    transient public list<id> listids= new list<id>();
    public integer size{get; set;}
   
        
   
   public List<MyWrapper> wrapper {get; set;}
    public List<MyWrapper> excelWrapper {get; set;}
    
    public boolean b {
        get{
                if(wrapper!=null)
                {
                    return wrapper.size()>0?true:false;
                }
                else 
                {
                    return false;
                }
            }
  }
  
  transient public Map<string, Event_Registration_Attendance__c> conNamesIdMap = new Map<string, Event_Registration_Attendance__c>();
  transient public Blob contentFile { get; set; }
  transient public List<List<String>> fileLines = new List<List<String>>();
  
  List<Event_Registration_Attendance__c> Conlist;
  public Event_Registration_Attendance__c UpdateCon;
  
  transient List<String> conNames;
  Set<String> conNamesf;
  Set<String> conNamesr;
  
  
  transient Map<String, Event_Registration_Attendance__c> newlistmap = new Map<String, Event_Registration_Attendance__c>();
  List<Contact> newCons = new List<Contact>();
  
  Map<String,Event_Registration_Attendance__c> oldEventRegistrationRecords=new Map<String,Event_Registration_Attendance__c>();
  public Map<String,string> oldEventRegistrationRecords1=new Map<String,string>();
  public Map<String,string> oldEventRegistrationRecordsError=new Map<String,string>();
  
  Set<String> employeeContactIds=new Set<String>();
  Map<Id,contact> employeeIdsMap=new Map<Id,contact>();
  
  public WCT_EventsBulkUploadContact() {
     
     eventid = ApexPages.currentPage().getParameters().get('id');
     eventRecTypeId = ApexPages.currentPage().getParameters().get('RecordType');
     
     RecTypeName = Schema.SObjectType.Event__c.getRecordTypeInfosById().get(eventRecTypeId).getName();
    // system.debug('RecTypeName'+RecTypeName);
     Event_RegistrationRecTypeId = Schema.SObjectType.Event_Registration_Attendance__c.getRecordTypeInfosByName().get('Alumni Relation').getRecordTypeId();
     
    
  }
      
  WCT_parseCSV parseCSVInstance = new WCT_parseCSV();  
  
  
   
  public Pagereference ReadFile()
  {
        conNamesIdMap =new Map<string, Event_Registration_Attendance__c>();
      /*  try {
        /*Validtion if no file selected */
      /*  if(contentFile!=null)
        {
            nameFile = contentFile.toString();
            filelines = parseCSVInstance.parseCSV(nameFile, true);
        }
        else
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please choose a file to upload !'));
            return null;
        }
     }
        Catch(System.StringException stringException)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_String_Exception_Error + stringException);
            ApexPages.addMessage(errormsg);    
        }
       
        Catch(Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_Exception +e );
            ApexPages.addMessage(errormsg);    
        
        }*/
        System.debug('filelines'+filelines);
        Conlist= new List<Event_Registration_Attendance__c>();
        conNames = new List<String>();
        conNamesf = new set<String>();
        conNamesr= new set<String>();
        wrapper = new List<MyWrapper>() ;
        listids= new list<id>();
        newlistmap= new Map<String, Event_Registration_Attendance__c>();
        totalunsuccessrec =0;
        totalsuccessrec =0;
        totalrecords=0;
        totalsuccessreccount=0;
        totalalreadypresent=0;
         
        List<String> recruiterEmailIDs = new List<String>();        
                
        /*Step 1 :: Processing the uploaded file and fetching the various details into  the contact object List newlistmap.*/       
        try {
        
        nameFile = contentFile.toString();
        filelines = parseCSVInstance.parseCSV(nameFile, true);
        
        for(List<String> inputValues:filelines) 
        {   
            totalrecords++;
             
         /*   for (integer j=0; j<inputvalues.size(); j++) {
                //System.debug('inputvalues['+j+']'+inputvalues[j]);
            }*/
            
            Event_Registration_Attendance__c eveReg = new Event_Registration_Attendance__c(); 
         
            try{    
            
            if(inputValues[0] != null){
                eveReg.Contact__c = inputValues[0];
            }
            if(inputValues[1] != null){
                eveReg.Curent_Employer__c = inputValues[1];
            }
            if(inputValues[2] != null){
                eveReg.Current_Title__c = inputValues[2];
            }
            if(inputValues[3] != null){
                eveReg.Contact_Email_for_Comm__c = inputValues[3];
            }
            if(inputValues[4] != null){
                eveReg.Preferred_Email_Type__c = inputValues[4];
            }
            eveReg.RecordTypeId = Schema.SObjectType.Event_Registration_Attendance__c.getRecordTypeInfosByName().get('Alumni Relation').getRecordTypeId();
            eveReg.Event__c = eventid;
            
           
            conNames.add(eveReg.Contact__c);
            employeeContactIds.add(eveReg.Contact__c);
            newlistmap.put(eveReg.Contact__c,eveReg);
                        
            }
            catch (Exception e)
            {
                oldEventRegistrationRecords1.put(inputValues[0],eventid);
                oldEventRegistrationRecordsError.put(inputValues[0],e.getMessage()); 
                
            } 
            
            }
           
        }
        
         Catch(System.StringException stringException)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_String_Exception_Error + stringException);
            ApexPages.addMessage(errormsg);    
        }
         Catch(System.ListException listException)
        {    
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Columns_count_not_proper);
            ApexPages.addMessage(errormsg); 
        }
        
            catch (Exception e)
            {
               ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_Exception);
            ApexPages.addMessage(errormsg);    
            }  
        System.debug('filelines1'+filelines);
        Map<Id,String> employeeIdsMap=new Map<Id,String>();
        if(!employeeContactIds.IsEmpty())
                {   
                     for(Contact candidateRecord: [Select id,Email from Contact where (id IN:employeeContactIds)])
                    {
                
                            employeeIdsMap.put(candidateRecord.Id,eventid);
                    }
                }
                
         if(!newlistmap.IsEmpty())
        {
            
            for(String str:newlistmap.keySet())
            {
                str=(str);
                if(!employeeIdsMap.ContainsKey(newlistmap.get(str).Contact__c))
                    {
                        
                       oldEventRegistrationRecords1.put(newlistmap.get(str).Contact__c,eventid);
                       newlistmap.remove(newlistmap.get(str).Contact__c); 
                    }
            } 
        }         
                    
        
         /*Step 2 : Searching for the contacts with the emails ids from the Uploaded file in the SFDC. */
        
        List<Event_Registration_Attendance__c> existingConts = [SELECT recordtypeId,Contact__r.Email,Accept_Date__c,Accept__c,Applied_Date__c,Applied__c,Attended__c,Attendee_Type__c,Attending__c,
                                                                                Campus_Event__c,City__c,Communicating_with_Candidate__c,
                                                                                Contact__c,Convention_Activity_Notes__c,Convention_Activity__c,Convention_Rating_Category__c,
                                                                                CreatedById,CreatedDate,Decline_Date__c,Decline__c,Deloitte_Professional_Met_Interviewer__c,
                                                                                Event_End_Date__c,Event_ID__c,Event__c,Feedback_Form__c,FSS__c,Full_Event_Name__c,Id,
                                                                                Inclusion_Ambassador__c,Interview_1_Date__c,Interview_1__c,Interview_2_Date__c,Interview_2__c,
                                                                                Invited__c,IsDeleted,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Level__c,
                                                                                Local_Recruiter_Notes__c,Name,Not_Attending__c,No_Response__c,No_Show__c,Offer_Date__c,Offer__c,
                                                                                Other_Offers_Detail__c,Post_Contvention_Interviewer_Title__c,Post_Convention_Interviewer_Title__c,
                                                                                Post_Convention_Interview_Format__c,Prior_Interview_Notes__c,Recruiter_Local__c,Role__c,Service_Area__c,
                                                                                Status__c,SystemModstamp,Target__c,UserResponse__c,Waitlisted__c 
                                                                        FROM Event_Registration_Attendance__c
                                                                        where Contact__c IN: employeeContactIds AND Event__c =: eventid LIMIT:Limits.getLimitQueryRows()];
        
         /*Step 3 : By this step 'existingConts' has contacts record present in SFDC. 
                  A. Now, Modifying the existing contacts (existingConts) with new values from 'newlistmap' (from uploaded file)
                  B. At the same time remove the record which are been existing from the 'newlistmap' */
      for (Event_Registration_Attendance__c oldeveReg: existingConts)
      {
        If(newlistmap.containsKey(oldeveReg.Contact__c))
        {
         
             UpdateCon = newlistmap.get(oldeveReg.Contact__c);
             oldeveReg.Contact__c = UpdateCon.Contact__c;
             oldeveReg.RecordTypeId = UpdateCon.RecordTypeId;
             oldeveReg.Event__c = UpdateCon.Event__c;
            
             oldeveReg.Curent_Employer__c = UpdateCon.Curent_Employer__c;
             oldeveReg.Current_Title__c = UpdateCon.Current_Title__c;
             oldeveReg.Contact_Email_for_Comm__c = UpdateCon.Contact_Email_for_Comm__c;
             oldeveReg.Preferred_Email_Type__c = UpdateCon.Preferred_Email_Type__c;
             
            
            /*Step 3 B : Deleting the Contact of the existing contact from the newlistmap. By this at the end of this loop, newlistmap will contain only the contact not present in the SFDC.*/
            newlistmap.remove(oldeveReg.Contact__c);
            oldEventRegistrationRecords.put(oldeveReg.Contact__c,oldeveReg);
            
         }     
      } 
      
      /*Step 4 : Now 'existingConts' has contacts record present in SFDC with modified values from the uploaded files. Now update these contact into SFDC.*/
           if(newlistmap.size()>0)
           {
                Set<String> conIds=newlistmap.keySet();
                list<Event_Registration_Attendance__c> insList = new list<Event_Registration_Attendance__c>();
                for(String Id : conIds)
                {
                    insList.add(newlistmap.get(Id));
            
                }
                
                
                if(!insList.isEmpty()) {
                    //Using the JSON.serializePretty we will serialize the list of account.
                    string jsonString = JSON.serializePretty(insList);
                    handleinsertion(jsonString);
                    //insert insList;
                }   
           }
      /*Step 5 : Generating the report for End User.
         *        1. Records do not exist : Size of newlistmap. Hint :: Since all existing records are been removed from newlistmap.
         *        2. Records successfully updated : 'totalrecords' (total records in Uploaded file) - (Records do not exist).
         *        3. Update the wrapper class with non existing contact details.
        */
        
        totalunsuccessrec=oldEventRegistrationRecords1.size();
        totalalreadypresent = oldEventRegistrationRecords.size(); 
        totalsuccessreccount=newlistmap.size();
        Set<String> Ids=oldEventRegistrationRecords.keySet();
        Set<String> Ids1=oldEventRegistrationRecords1.keySet();
        
             
        
    /*     for(String Id : Ids)
        {
        /*  private final integer listLimit;
            integer counter = 0;
            integer loopCount = 0;
            Event_Registration_Attendance__c[] tmpcase = new Event_Registration_Attendance__c[]{};
            
            Event_Registration_Attendance__c c=oldEventRegistrationRecords.get(Id);
            wrapper.add(new MyWrapper(c.Contact__c, c.Event__c, 'Contact Already exists'));
        }*/
        if(Ids1.size() < 1000) {
        for(String Id1 : Ids1)
        {
           // Event_Registration_Attendance__c c=oldEventRegistrationRecords1.get(Id1);
            wrapper.add(new MyWrapper(Id1, oldEventRegistrationRecords1.get(Id1), oldEventRegistrationRecordsError.get(Id1)));
        }
        }
        oldEventRegistrationRecords1.clear();
        oldEventRegistrationRecords.clear();
        oldEventRegistrationRecordsError.clear();                
          excelWrapper = wrapper;
        try
        {
            for(string s:conNamesr)
            {
                if(conNamesf.contains(s))
                {
                  conNamesf.remove(s);     
                }
            }
        }
        catch (Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Contact Email already exist, change Contact Email and try again');
            ApexPages.addMessage(errormsg);
        }
    
        return null; 
   }     
        
    public List<Event_Registration_Attendance__c> getuploadedAccounts()
    {
        list<Event_Registration_Attendance__c> lcon1;
            if (Conlist!= NULL)    
                if (Conlist.size() > 0) { conlist.clear(); return conlist; }
                
                else
                 return null;               
                else
                return null;
    }  
    
    public pagereference pg()
    {
        
        return page.Events_ViewFailedContacts;
    }
    
    public class MyWrapper
    {
         public string Contactc {get; set;}
         public string Eventc {get; set;} 
         public string Errormsgc {get; set;} 
         
         public MyWrapper(string Contact, String Event, String Errormsg) {
         
                Contactc = Contact;
                Eventc = Event;
                Errormsgc = Errormsg;
                
        }
    }    
   /*@future(limits='3xDMLRows')  */
   @future
    private static void handleinsertion(string eventString) {
        list<Event_Registration_Attendance__c> inserteventList =  (list<Event_Registration_Attendance__c>)JSON.deserializeStrict(eventString, list<Event_Registration_Attendance__c>.class);
        system.debug('inserteventList'+inserteventList);
        if(!inserteventList.isEmpty())
            insert inserteventList; 
    }   
   
}