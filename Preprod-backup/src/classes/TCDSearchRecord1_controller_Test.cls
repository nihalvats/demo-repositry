@isTest
private class TCDSearchRecord1_controller_Test{
    
    
    static testmethod void method(){
        
        TCDSearchRecord1_controller TCD = new TCDSearchRecord1_controller ();
        
        list<Talent_Delivery_Contact_Database__c> lsttcd = new list<Talent_Delivery_Contact_Database__c>();
        
        
        
        Talent_Delivery_Contact_Database__c TDCD = new Talent_Delivery_Contact_Database__c();
        
        
        TDCD.name='123456';
        TDCD.HR_Function__c='Test HR Function';
        TDCD.HR_Service_Area__c ='Test HR Service Area  ';
        TDCD.HR_Service_Line__c = 'Test HR Service Line';
        TDCD.India_Talent_Contact_1__c = 'TestIndiaTalentContact1@gmail.com';
        TDCD.India_Talent_Contact_2__c  = 'TestIndiaTalentContact2@gmail.com ';
        TDCD.Payroll_Contact_Email__c = 'TestPayrollContactEmail@gmail.com ';
        TDCD.US_Talent_Contact_1__c =' TestUSTalentContact1@gmail.com';
        TDCD.US_Talent_Contact_2__c = 'TestUSTalentContact2@gmail.com ';
        TDCD.US_Talent_Contact_3__c = 'TestUSTalentContact3@gmail.com ';
        TDCD.US_Talent_Contact_4__c = 'TestUSTalentContact4@gmail.com ';
        TDCD.Workers_Comp_Contact_Email__c = 'TestWorkersCompContactEmail@gmail.com ';
        lsttcd.add(TDCD);
        
        insert TDCD; 
        
        test.starttest();
        
        
        TCD.TCDid = TDCD.id;
        TCD.CostCenterNumber =TDCD.name;
        TCD.HRfunction = TDCD.HR_Function__c;
        TCD.HRServiceArea  = TDCD.HR_Service_Area__c ;
        TCD.TDCD = lsttcd;
        TCD.MAXTCD = lsttcd;
        lsttcd= TCD.maxTCD;
        TCD.SearchRecord();
        TCD.SearchById();
        TCD.RefreshTCDlist();
      	TCD.DownloadToExcel();
        TCD.reset();
        TCD.closePopup();
        
        test.stoptest();
        
    }
}