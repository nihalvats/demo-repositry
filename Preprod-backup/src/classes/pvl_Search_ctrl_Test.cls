@isTest
public class pvl_Search_ctrl_Test{

public static integer  limitToDisplay= 1000;


    /*
    static testmethod void Test4RemoteAction()
    {
        H1B_Client_Company__c h1b = new H1B_Client_Company__c ();
        h1b.Company_Name__c = 'Test';
        insert h1b;
        string callingvar ='Test';
        //pvl_Search_ctrl_Test1.Test4RemoteAction(callingvar);
    }
    */
 public static testMethod void test() {
 
      date myDate = date.today();
 
     PageReference pgRef = Page.PVLSearch;
     Test.setCurrentPageReference (pgRef);
    
     Project_H1B__c proj = New Project_H1B__c ();
     proj.USI_Resources__c = 'Yes'; 
     proj.USI_Landed_Resources__c = 'Yes';
     proj.H1B_Project_WBS__c = 'Test';
     proj.PVL_Project_Service_Area__c = 'Technology';
     proj.PVL_Project_Service_Line__c = 'Deloitte Digital';
     proj.Industry__c = 'Industry1';
     proj.Vendor__c = 'Test vendor';
     proj.Capability__c = 'Capability';
     proj.H1B_Project_Skills__c = 'Yes';
     proj.Sub_Capability__c= 'Test Sub';
     proj.PVL_Module__c = 'Test Module';
     Insert proj;

     Project_H1B__c proj1 = New Project_H1B__c ();
     proj1.USI_Resources__c = 'Yes'; 
     proj1.USI_Landed_Resources__c = 'No';
     proj1.H1B_Project_WBS__c = 'Test';
     proj1.PVL_Project_Service_Area__c = 'Technology';
     proj1.PVL_Project_Service_Line__c = 'Deloitte Digital';
     proj1.Industry__c = 'Industry1';
     proj1.Vendor__c = 'Test vendor';
     proj1.Capability__c = 'Capability';
     proj1.H1B_Project_Skills__c = 'Yes';
     proj1.Sub_Capability__c= 'Test Sub';
     proj1.PVL_Module__c = 'Test Module';
     Insert proj1;
     
     PVL_Database__c pvl = New PVL_Database__c();
     pvl.Project__c = proj.id;
     pvl.Active__c = true;
     pvl.PVL_Expiration_Date__c = myDate.addDays(50) ;
     pvl.PVL_Date_PVL_Received__c= myDate ;
     pvl.Project__c=proj1.id;
     Insert pvl ;
     
     Client_Location__c cl = new Client_Location__c ();
     cl.Project_Name__c = proj.id;
     cl.H1B_State__c = 'IL';
     cl.H1B_Street__c = 'Streat';
     cl.Zip_Code__c= '12345';     
     insert cl;
     
     Client_Location__c cl1 = new Client_Location__c ();
     cl1.Project_Name__c = proj.id;
     cl1.H1B_State__c = 'IL';
     cl1.H1B_City__c='Hyderabad';
     cl1.H1B_Street__c = 'Streat';
     cl1.Zip_Code__c= '12345';     
     insert cl1;     
     
    H1B_Client_Company__c objClntCmpny = new H1B_Client_Company__c(Company_Name__c='SYSCO CORPORATION');
    insert objClntCmpny; 
    
    H1B_Client_Company__c objCltCny = new H1B_Client_Company__c(Company_Name__c='TECO ENERGY INC');
    insert objCltCny; 
    

    String fldName = 'SYSCO CORPORATION';
    String fldNames = 'TECO ENERGY INC';  
    pvl_Search_ctrl.getExistingCompanies(fldName);
     pvl_Search_ctrl.getItemsList(fldNames);    
     Test.starttest();       
     ApexPages.StandardController sc = new ApexPages.standardController(proj);
     string item = 'test'; 
     string mainValue = 'testvalue';
     pvl_Search_ctrl con = new pvl_Search_ctrl();
     con.ClearControls();
     con.topList=con.topList;
     con.tempProject = proj1;
     con.search();
     
     
     con.tempClientLocation.H1B_City__c='Hyderabad';
     con.ppe='Technology';
     con.pvlstate='AZ';
     con.search();
     
     con.tempDatabase.PVL_Expiration_Date__c=System.today();
     con.tempDatabase.PVL_Date_PVL_Received__c=System.today();
     con.search();
     
     con.compareString('val1', 'val1,val2');
     
     pvl_Search_ctrl con1 = new pvl_Search_ctrl(sc);
     
     //con.compareString();
     con.export();
     

     
     Test.stoptest();
     
     
     
 }

}