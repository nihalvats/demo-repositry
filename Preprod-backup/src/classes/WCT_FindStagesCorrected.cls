/**
    * Class Name  : WCT_FindStagesCorrected 
    * Description : This apex class will use to Create cases. 
*/

global class WCT_FindStagesCorrected {
    
     /** 
        Method Name  : findStagingRecords
        Return Type  : Void
        Description  : Find all records which needs to treated as cases.         
    */
  
    WebService static String findStagingRecords(String stagingStatus, String versionNo)
    {
        Set<Id> stagingIds = new Set<id>();
        String returnStatement=Label.WCT_Message_Processing_Starting;

        if(versionNo != null && versionNo != '') {
            for(WCT_PreBIStageTable__c preBIStageRecord: [SELECT id FROM WCT_PreBIStageTable__c WHERE WCT_Status__c=:stagingStatus AND WCT_Case_Creation__c=true AND WCT_Version_Number__c=:versionNo LIMIT:Limits.getLimitQueryRows()])
            {
                stagingIds.add(preBIStageRecord.Id);
            }
            if(!stagingIds.IsEmpty())
            {
                try{
                    WCT_CreateCasesHelper.createCases(stagingIds, stagingStatus);
                }Catch(Exception e)
                {
                    WCT_ExceptionUtility.logException('WCT_FindStagesCorrected','Create Cases',e.getMessage()); 
                    throw e;
                }
            }
            else
            {
                returnStatement=WCT_UtilConstants.PROCESS_NONE;
            }
        } else {
            returnStatement='Please enter the valid version no';
        }

        return returnStatement;
    }
}