@isTest
global class WCT_MockHttpResBulkNoticeGenerator_Test implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        /* Create a JSON parser
        JSONParser parser = JSON.createParser(req.getBody());
        String taskId = null;
        while (parser.nextToken() != null) {
            taskId = null;            
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'Extra')) {               
                // Parse the taskId
                parser.nextToken();
                taskId = parser.getText(); 
                break;
            }                    
        }
 
*/
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        /*String responseBody = '[{' +
        '    "tokenEndpoint": "https://test.salesforce.com/services/oauth2/token",' +
        '    "Username": "svelusamy@deloitte.com.preprod",' +
        '    "Password": "Sundari@25",' +
        '    "Clientkey": "3MVG90D5vR7UtjbqVOk9wq35o8dwsUWF9X3cFVFiS9x1344_7a8S2Qep1N4g0XVsGFu03nb9xFtcFWgi1c2mZ",'+
        '    "Oathsecretkey": 3195122133755905575,' +
        '}]';*/
        
       //String responseBody =  '{"access_token":"00D180000001abB!AQEAQFL0TUWa.oNDb5gvbujIoSfo.LIc62LUqjxTY2zA_r1Suv27UL5Vb.9qECxJpj5j4t1_k0C2ijR0D2rhu6Ik7U0nkzs.","instance_url":"https://talent--PreProd.cs23.my.salesforce.com","id":"https://test.salesforce.com/id/00D180000001abBEAQ/00518000000lAKaAAM","token_type":"Bearer","issued_at":"1452601513719","signature":"ZFso/v1zPmxHUHWsIdtjGHUueztIhZPe2PG9SsKVGAM="}';
        
        res.setBody('{"access_token":"B!AQEAQFL0TUWa.oNDb5gvbujIoSfo.LIc62LUqjxTY2zA_r1Suv27UL5Vb.9qECxJpj5j4t1_k0C2ijR0D2rhu6Ik7U0nkzs.","clientId":"3MVG90D5vR7UtjbqVOk9wq35o8dwsUWF9X3cFVFiS9x1344_7a8S2Qep1N4g0XVsGFu03nb9xFtcFWgi1c2mZ", "clientSecret": "3195122133755905575", "password":"Sundari@25", "tokenEndpoint":"https://test.salesforce.com/services/oauth2/token", "username":"svelusamy@deloitte.com.preprod"}');
     system.debug('ResBody***********'+res)  ; 
        res.setStatusCode(200);
        return res;
    }
    
    global HTTPResponse respond1(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');        
        res.setBody('error:error');
        res.setStatusCode(400);
        return res;
    }    
}