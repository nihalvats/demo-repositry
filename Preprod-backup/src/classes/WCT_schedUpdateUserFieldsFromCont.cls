global class WCT_schedUpdateUserFieldsFromCont implements Schedulable {
    
   String query='Select id,Email,Direct_No__c,Legal_Entity__c,Profile.UserLicense.name from User where IsActive = true AND Profile.UserLicense.name = \'Salesforce\' ';
   
   global void execute(SchedulableContext sc) {
      WCT_UpdateUserFieldsFromContact b = new WCT_UpdateUserFieldsFromContact(query); 
      database.executebatch(b,2000);
   }
}