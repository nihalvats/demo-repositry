@isTest
public class Wct_RevisedOfferNotification_Test{
    
    static testMethod void Wct_RevisedOfferNotification_TestMethod(){
         Profile prof = [select id from profile where name='system Administrator'];
         User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
    
         Contact con = new Contact ();
         con.LastName='tec';
         con.Email='user@gmail.com';
         con.WCT_Internship_Start_Date__c = date.today()-1;
         Insert con;
         system.debug('con values ----'+con);
         
         string rc1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Experienced Hire Offer').getRecordTypeId();
         
       
        List<WCT_Offer__c> lstOffer = new List<WCT_Offer__c>(); 
        WCT_Offer__c  objCusWct= new WCT_Offer__c();
        
        
        objCusWct.RecordTypeId = rc1;
        objCusWct.WCT_status__c = 'Draft in Progress';
        objCusWct.WCT_Candidate__c = con.Id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.OwnerId = usr.id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.WCT_Type_of_Hire__c = 'Full Time';
        objCusWct.ownerId = usr.Id;
        objCusWct.WCT_Team_Mailbox__c = 'us.name@vmail.com';
        
        objCusWct.Offer_Sent_Track__c = True;
        lstOffer.add(objCusWct);
        insert lstOffer; 
        
        Datetime yesterday = Datetime.now().addminutes(-120);
        Test.setCreatedDate(objCusWct.id, yesterday);
        system.debug('yesterday----'+yesterday); 
        
        lstOffer[0].WCT_Revised_Offer_CreatedDate__c = yesterday;
        lstOffer[0].WCT_Offer_Approved_Date__c = yesterday;
        update lstOffer;
        system.debug('revised date----'+objCusWct.WCT_Revised_Offer_CreatedDate__c);
        
        objCusWct.recalculateformulas();  
        
        BusinessHours bhs=[select id from BusinessHours where IsDefault=true];
        Decimal  seconds = BusinessHours.diff(bhs.id, yesterday, System.now())/ 1000;
        Datetime startdate = Datetime.newInstance(2013, 5, 28, 1, 6, 8);
        system.debug('seconds----'+seconds);
        Decimal  min = Integer.valueOf(seconds / 60);
        system.debug('min ----'+min);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        
        Test.StartTest();
        
        Wct_RevisedOfferNotification objBatch = new Wct_RevisedOfferNotification();
        
        Map<ID, Schema.RecordTypeInfo> rtMap1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosById();
        system.assertequals(rtMap1.get(lstOffer[0].RecordTypeId).getName(), 'US Experienced Hire Offer' ) ; 
        system.schedule('New','0 0 2 1 * ?',objBatch );
        ID batchprocessid = Database.executeBatch(objBatch);
        
        Test.StopTest();
        
    }
    
    static testMethod void Wct_RevisedOfferNotification_TestMethod2(){
         Profile prof = [select id from profile where name='system Administrator'];
         User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
    
         Contact con = new Contact ();
         con.LastName='tec';
         con.Email='user@gmail.com';
         con.WCT_Internship_Start_Date__c = date.today()-1;
         Insert con;
         system.debug('con values ----'+con);
         
         string rc1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Experienced Hire Offer').getRecordTypeId();
         
       
        List<WCT_Offer__c> lstOffer = new List<WCT_Offer__c>(); 
        WCT_Offer__c  objCusWct= new WCT_Offer__c();
        
        
        objCusWct.RecordTypeId = rc1;
        objCusWct.WCT_status__c = 'Draft in Progress';
        objCusWct.WCT_Candidate__c = con.Id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.OwnerId = usr.id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.WCT_Type_of_Hire__c = 'Full Time';
        objCusWct.ownerId = usr.Id;
        objCusWct.WCT_Team_Mailbox__c = 'us.name@vmail.com';
        
        objCusWct.Offer_Sent_Track__c = True;
        lstOffer.add(objCusWct);
        insert lstOffer; 
        
        Datetime yesterday = Datetime.now().addminutes(-180);
        Test.setCreatedDate(objCusWct.id, yesterday);
        system.debug('yesterday----'+yesterday); 
        
        lstOffer[0].WCT_Revised_Offer_CreatedDate__c = yesterday;
        lstOffer[0].WCT_Offer_Approved_Date__c = yesterday;
        update lstOffer;
        system.debug('revised date----'+objCusWct.WCT_Revised_Offer_CreatedDate__c);
        
        objCusWct.recalculateformulas();  
        
        BusinessHours bhs=[select id from BusinessHours where IsDefault=true];
        Decimal  seconds = BusinessHours.diff(bhs.id, yesterday, System.now())/ 1000;
        Datetime startdate = Datetime.newInstance(2013, 5, 28, 1, 6, 8);
        system.debug('seconds----'+seconds);
        Decimal  min = Integer.valueOf(seconds / 60);
        system.debug('min ----'+min);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        
        Test.StartTest();
        
        Wct_RevisedOfferNotification objBatch = new Wct_RevisedOfferNotification();
        
        Map<ID, Schema.RecordTypeInfo> rtMap1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosById();
        system.assertequals(rtMap1.get(lstOffer[0].RecordTypeId).getName(), 'US Experienced Hire Offer' ) ; 
        system.schedule('New','0 0 2 1 * ?',objBatch );
        ID batchprocessid = Database.executeBatch(objBatch);
        
        Test.StopTest();
        
    }
    
    
    static testMethod void Wct_RevisedOfferNotification_TestMethod3(){
         Profile prof = [select id from profile where name='system Administrator'];
         User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
    
         Contact con = new Contact ();
         con.LastName='tec';
         con.Email='user@gmail.com';
         con.WCT_Internship_Start_Date__c = date.today()-1;
         Insert con;
         system.debug('con values ----'+con);
         
         string rc1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Experienced Hire Offer').getRecordTypeId();
         
       
        List<WCT_Offer__c> lstOffer = new List<WCT_Offer__c>(); 
        WCT_Offer__c  objCusWct= new WCT_Offer__c();
        
        
        objCusWct.RecordTypeId = rc1;
        objCusWct.WCT_status__c = 'Draft in Progress';
        objCusWct.WCT_Candidate__c = con.Id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.OwnerId = usr.id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.WCT_Type_of_Hire__c = 'Full Time';
        objCusWct.ownerId = usr.Id;
        objCusWct.WCT_Team_Mailbox__c = 'us.name@vmail.com';
        
        objCusWct.Offer_Sent_Track__c = True;
        lstOffer.add(objCusWct);
        insert lstOffer; 
        
        Datetime yesterday = Datetime.now().addminutes(-240);
        Test.setCreatedDate(objCusWct.id, yesterday);
        system.debug('yesterday----'+yesterday); 
        
        lstOffer[0].WCT_Revised_Offer_CreatedDate__c = yesterday;
        lstOffer[0].WCT_Offer_Approved_Date__c = yesterday;
        update lstOffer;
        system.debug('revised date----'+objCusWct.WCT_Revised_Offer_CreatedDate__c);
        
        objCusWct.recalculateformulas();  
        
        BusinessHours bhs=[select id from BusinessHours where IsDefault=true];
        Decimal  seconds = BusinessHours.diff(bhs.id, yesterday, System.now())/ 1000;
        Datetime startdate = Datetime.newInstance(2013, 5, 28, 1, 6, 8);
        system.debug('seconds----'+seconds);
        Decimal  min = Integer.valueOf(seconds / 60);
        system.debug('min ----'+min);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        
        Test.StartTest();
        
        Wct_RevisedOfferNotification objBatch = new Wct_RevisedOfferNotification();
        
        Map<ID, Schema.RecordTypeInfo> rtMap1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosById();
        system.assertequals(rtMap1.get(lstOffer[0].RecordTypeId).getName(), 'US Experienced Hire Offer' ) ; 
        system.schedule('New','0 0 2 1 * ?',objBatch );
        ID batchprocessid = Database.executeBatch(objBatch);
        
        Test.StopTest();
        
    }
    
    static testMethod void Wct_RevisedOfferNotification_TestMethod4(){
         Profile prof = [select id from profile where name='system Administrator'];
         User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
    
         Contact con = new Contact ();
         con.LastName='tec';
         con.Email='user@gmail.com';
         con.WCT_Internship_Start_Date__c = date.today()-1;
         Insert con;
         system.debug('con values ----'+con);
         
         string rc1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Campus Hire Offer').getRecordTypeId();
         
       
        List<WCT_Offer__c> lstOffer = new List<WCT_Offer__c>(); 
        WCT_Offer__c  objCusWct= new WCT_Offer__c();
        
        
        objCusWct.RecordTypeId = rc1;
        objCusWct.WCT_status__c = 'Draft in Progress';
        objCusWct.WCT_Candidate__c = con.Id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.OwnerId = usr.id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.WCT_Type_of_Hire__c = 'Full Time';
        objCusWct.ownerId = usr.Id;
        objCusWct.WCT_Team_Mailbox__c = 'us.name@vmail.com';
        
        objCusWct.Offer_Sent_Track__c = True;
        lstOffer.add(objCusWct);
        insert lstOffer; 
        
        Datetime yesterday = Datetime.now().addminutes(-120);
        Test.setCreatedDate(objCusWct.id, yesterday);
        system.debug('yesterday----'+yesterday); 
        
        lstOffer[0].WCT_Revised_Offer_CreatedDate__c = yesterday;
        lstOffer[0].WCT_Offer_Approved_Date__c = yesterday;
        update lstOffer;
        system.debug('revised date----'+objCusWct.WCT_Revised_Offer_CreatedDate__c);
        
        objCusWct.recalculateformulas();  
        
        BusinessHours bhs=[select id from BusinessHours where IsDefault=true];
        Decimal  seconds = BusinessHours.diff(bhs.id, yesterday, System.now())/ 1000;
        Datetime startdate = Datetime.newInstance(2013, 5, 28, 1, 6, 8);
        system.debug('seconds----'+seconds);
        Decimal  min = Integer.valueOf(seconds / 60);
        system.debug('min ----'+min);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        
        Test.StartTest();
        
        Wct_RevisedOfferNotification objBatch = new Wct_RevisedOfferNotification();
        
        Map<ID, Schema.RecordTypeInfo> rtMap1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosById();
        system.assertequals(rtMap1.get(lstOffer[0].RecordTypeId).getName(), 'US Campus Hire Offer' ) ; 
        system.schedule('New','0 0 2 1 * ?',objBatch );
        ID batchprocessid = Database.executeBatch(objBatch);
        
        Test.StopTest();
        
    }
    
    static testMethod void Wct_RevisedOfferNotification_TestMethod5(){
         Profile prof = [select id from profile where name='system Administrator'];
         User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
    
         Contact con = new Contact ();
         con.LastName='tec';
         con.Email='user@gmail.com';
         con.WCT_Internship_Start_Date__c = date.today()-1;
         Insert con;
         system.debug('con values ----'+con);
         
         string rc1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Campus Hire Offer').getRecordTypeId();
         
       
        List<WCT_Offer__c> lstOffer = new List<WCT_Offer__c>(); 
        WCT_Offer__c  objCusWct= new WCT_Offer__c();
        
        
        objCusWct.RecordTypeId = rc1;
        objCusWct.WCT_status__c = 'Draft in Progress';
        objCusWct.WCT_Candidate__c = con.Id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.OwnerId = usr.id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.WCT_Type_of_Hire__c = 'Full Time';
        objCusWct.ownerId = usr.Id;
        objCusWct.WCT_Team_Mailbox__c = 'us.name@vmail.com';
        
        objCusWct.Offer_Sent_Track__c = True;
        lstOffer.add(objCusWct);
        insert lstOffer; 
        
        Datetime yesterday = Datetime.now().addminutes(-180);
        Test.setCreatedDate(objCusWct.id, yesterday);
        system.debug('yesterday----'+yesterday); 
        
        lstOffer[0].WCT_Revised_Offer_CreatedDate__c = yesterday;
        lstOffer[0].WCT_Offer_Approved_Date__c = yesterday;
        update lstOffer;
        system.debug('revised date----'+objCusWct.WCT_Revised_Offer_CreatedDate__c);
        
        objCusWct.recalculateformulas();  
        
        BusinessHours bhs=[select id from BusinessHours where IsDefault=true];
        Decimal  seconds = BusinessHours.diff(bhs.id, yesterday, System.now())/ 1000;
        Datetime startdate = Datetime.newInstance(2013, 5, 28, 1, 6, 8);
        system.debug('seconds----'+seconds);
        Decimal  min = Integer.valueOf(seconds / 60);
        system.debug('min ----'+min);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        
        Test.StartTest();
        
        Wct_RevisedOfferNotification objBatch = new Wct_RevisedOfferNotification();
        
        Map<ID, Schema.RecordTypeInfo> rtMap1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosById();
        system.assertequals(rtMap1.get(lstOffer[0].RecordTypeId).getName(), 'US Campus Hire Offer' ) ; 
        system.schedule('New','0 0 2 1 * ?',objBatch );
        ID batchprocessid = Database.executeBatch(objBatch);
        
        Test.StopTest();
        
    }
    
    static testMethod void Wct_RevisedOfferNotification_TestMethod6(){
         Profile prof = [select id from profile where name='system Administrator'];
         User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
    
         Contact con = new Contact ();
         con.LastName='tec';
         con.Email='user@gmail.com';
         con.WCT_Internship_Start_Date__c = date.today()-1;
         Insert con;
         system.debug('con values ----'+con);
         
         string rc1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Campus Hire Offer').getRecordTypeId();
         
       
        List<WCT_Offer__c> lstOffer = new List<WCT_Offer__c>(); 
        WCT_Offer__c  objCusWct= new WCT_Offer__c();
        
        
        objCusWct.RecordTypeId = rc1;
        objCusWct.WCT_status__c = 'Draft in Progress';
        objCusWct.WCT_Candidate__c = con.Id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.OwnerId = usr.id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.WCT_Type_of_Hire__c = 'Full Time';
        objCusWct.ownerId = usr.Id;
        objCusWct.WCT_Team_Mailbox__c = 'us.name@vmail.com';
        
        objCusWct.Offer_Sent_Track__c = True;
        lstOffer.add(objCusWct);
        insert lstOffer; 
        
        Datetime yesterday = Datetime.now().addminutes(-240);
        Test.setCreatedDate(objCusWct.id, yesterday);
        system.debug('yesterday----'+yesterday); 
        
        lstOffer[0].WCT_Revised_Offer_CreatedDate__c = yesterday;
        lstOffer[0].WCT_Offer_Approved_Date__c = yesterday;
        update lstOffer;
        system.debug('revised date----'+objCusWct.WCT_Revised_Offer_CreatedDate__c);
        
        objCusWct.recalculateformulas();  
        
        BusinessHours bhs=[select id from BusinessHours where IsDefault=true];
        Decimal  seconds = BusinessHours.diff(bhs.id, yesterday, System.now())/ 1000;
        Datetime startdate = Datetime.newInstance(2013, 5, 28, 1, 6, 8);
        system.debug('seconds----'+seconds);
        Decimal  min = Integer.valueOf(seconds / 60);
        system.debug('min ----'+min);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        
        Test.StartTest();
        
        Wct_RevisedOfferNotification objBatch = new Wct_RevisedOfferNotification();
        
        Map<ID, Schema.RecordTypeInfo> rtMap1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosById();
        system.assertequals(rtMap1.get(lstOffer[0].RecordTypeId).getName(), 'US Campus Hire Offer' ) ; 
        system.schedule('New','0 0 2 1 * ?',objBatch );
        ID batchprocessid = Database.executeBatch(objBatch);
        
        Test.StopTest();
        
    }
    
    static testMethod void Wct_RevisedOfferNotification_TestMethod7(){
         Profile prof = [select id from profile where name='system Administrator'];
         User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
    
         Contact con = new Contact ();
         con.LastName='tec';
         con.Email='user@gmail.com';
         con.WCT_Internship_Start_Date__c = date.today()-1;
         con.WCT_User_Group__c = 'United States'; 
         Insert con;
         system.debug('con values ----'+con);
         
         string rc1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Campus Hire Offer').getRecordTypeId();
         
       
        List<WCT_Offer__c> lstOffer = new List<WCT_Offer__c>(); 
        WCT_Offer__c  objCusWct= new WCT_Offer__c();
        
        objCusWct.RecordTypeId = rc1;
        objCusWct.WCT_status__c = 'Offer Approved';
        objCusWct.WCT_Candidate__c = con.Id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.OwnerId = usr.id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.WCT_Type_of_Hire__c = 'Full Time';
        objCusWct.ownerId = usr.Id;
        objCusWct.WCT_Team_Mailbox__c = 'us.name@vmail.com';
        
        objCusWct.Offer_Sent_Track__c = True;
        lstOffer.add(objCusWct);
        insert lstOffer; 
        
        Datetime yesterday = Datetime.now().addminutes(-120);
        Test.setCreatedDate(objCusWct.id, yesterday);
        system.debug('yesterday----'+yesterday); 
        
        lstOffer[0].WCT_Revised_Offer_CreatedDate__c = yesterday;
        lstOffer[0].WCT_Offer_Approved_Date__c = yesterday;
        update lstOffer;
        system.debug('revised date----'+objCusWct.WCT_Revised_Offer_CreatedDate__c);
        
        
        WCT_Offer_Status__c tempState = new WCT_Offer_Status__c();
        tempState.WCT_Enter_State_Date_Time__c = system.Now();
        tempState.WCT_Offer_Current_Status__c = objCusWct.WCT_status__c;
        tempState.WCT_Related_Offer__c = objCusWct.id;
        tempState.WCT_Status__c = 'Open';
        tempState.WCT_Leave_State_Date_Time__c = system.Now();
        
        insert tempState;
        
        objCusWct.recalculateformulas();  
        
        BusinessHours bhs=[select id from BusinessHours where IsDefault=true];
        
        Decimal  sec = BusinessHours.diff(bhs.id, tempState.WCT_Enter_State_Date_Time__c, tempState.WCT_Leave_State_Date_Time__c)/ 1000;
        Decimal  mins = Integer.valueOf(sec / 60);
        Decimal  hrs = Integer.valueOf(mins / 60);
        system.debug('hrs******'+hrs);
        tempState.WCT_SLA_TIme_in_Status__c = hrs.setScale(3);
        update tempState;
        
        Decimal  seconds = BusinessHours.diff(bhs.id, yesterday, System.now())/ 1000;
        Datetime startdate = Datetime.newInstance(2013, 5, 28, 1, 6, 8);
        system.debug('seconds----'+seconds);
        Decimal  min = Integer.valueOf(seconds / 60);
        system.debug('min ----'+min);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
        
        Test.StartTest();
        
        Wct_RevisedOfferNotification objBatch = new Wct_RevisedOfferNotification();
        
        Map<ID, Schema.RecordTypeInfo> rtMap1 = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosById();
        system.assertequals(rtMap1.get(lstOffer[0].RecordTypeId).getName(), 'US Campus Hire Offer' ) ; 
        system.schedule('New','0 0 2 1 * ?',objBatch );
        ID batchprocessid = Database.executeBatch(objBatch);
        
        Test.StopTest();
        
    }
    
}