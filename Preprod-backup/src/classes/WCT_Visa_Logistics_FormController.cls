/********************************************************************************************************************************
Apex class         : <WCT_Visa_Logistics_FormController>
Description        : <Controller which allows to Update Task and Immigration>
Type               :  Controller
Test Class         : <WCT_Visa_Logistics_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 24/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/  
public class WCT_Visa_Logistics_FormController extends SitesTodHeaderController{

     //PUBLIC VARIABLES
     public WCT_Immigration__c ImmigrationRecord{get;set;}
     public String siteURL{get;set;}
     
     // UPLOAD RELATED VARIABLES
     public Document doc {get;set;}
     public List<String> docIdList = new List<string>();
     public GBL_Attachments attachmentHelper{get; set;}
     public task t{get;set;}
     public String taskid{get;set;}
     public List<Attachment> listAttachments {get; set;}
     public Map<Id, String> mapAttachmentSize {get; set;}  
     
    
     //ERROR RELATED VARIABLES 
     public boolean pageError {get; set;}
     public String pageErrorMessage {get; set;}
     public String supportAreaErrorMesssage {get; set;}   
     
     //DEFINING A CONSTRUCTOR 
     public WCT_Visa_Logistics_FormController()
     {  
            /*Initialize all Variables*/
            init();
            attachmentHelper= new GBL_Attachments();
            /*Get Task ID from Parameter*/
            getParameterInfo();  
            /*Task ID Null Check*/
            if(taskid=='' || taskid==null)
            {
               invalidEmployee=true;
               return;
            }
            /*Get Task Instance*/
            getTaskInstance();
            /*Get Attachment List and Size*/
            getAttachmentInfo();
            /*Query to get Immigration Record*/  
            getImmigrationDetails();
            
     }
/********************************************************************************************
*Method Name         : <init()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Init() Used for loading Immigration,Attachments>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 

     private void init(){
           //Custom Object Instances
           ImmigrationRecord= new WCT_Immigration__c (); 
           siteURL = Label.Partial_Site_URL;
                 
           //Document Related Init
           doc = new Document();
           listAttachments = new List<Attachment>();
           mapAttachmentSize = new Map<Id, String>();
    
           
     }   

/********************************************************************************************
*Method Name         : <getParameterInfo()>
*Return Type         : <Null>
*Param’s             : URL
*Description         : <GetParameterInfo() Used to get URL Params for TaskID>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 
 
     private void getParameterInfo(){
        taskid = ApexPages.currentPage().getParameters().get('taskid');
     }

/********************************************************************************************
*Method Name         : <getTaskInstance()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetTaskInstance() Used for Querying Task Instance>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/   
 
     private void getTaskInstance(){
          t=[SELECT Status, OwnerId, WhatId, WCT_Auto_Close__c, WCT_Is_Visible_in_TOD__c  FROM Task WHERE Id =: taskid];
     }

/********************************************************************************************
*Method Name         : <getAttachmentInfo()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetAttachmentInfo() Used for Retrieving Attachment Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     
   
    @TestVisible
    //GETTING ATTACHMENT DETAILS
    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId , 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId = :taskid
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50 ];    
              
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }

/********************************************************************************************
*Method Name         : <getImmigrationDetails()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetImmigrationDetails() Used for Fetching Immigration Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     
 
     public void getImmigrationDetails()
     {
        ImmigrationRecord = [SELECT   Id,
                                         Name,
                                         RecordType.Name, 
                                         WCT_First_Name_As_on_passport__c,
                                         WCT_Last_Name_As_on_passport__c,
                                         WCT_Middle_Name_As_on_passport__c,
                                         WCT_Pref_US_Location_for_Pass_Pickup__c,
                                         UID_Number__c,
                                         WCT_Attached_DS160_online_visa_form__c,
                                         WCT_Attached_Petition_Copy__c,
                                         WCT_Attached_Visa_Fee_Payment_Conf__c,
                                         WCT_Attached_Passport_Bio_Pages_copy__c, 
                                         ownerId
                                         FROM WCT_Immigration__c 
                                         where id=:t.WhatId ];
      
     }

/********************************************************************************************
*Method Name         : <save()>
*Return Type         : <PageReference>
*Param’s             : 
*Description         : <Save() Used for for Updating Task and Immigration>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 

     public pageReference save()
     { 
        /*Custom Validation Rules are handled here*/
                if( (null == ImmigrationRecord.WCT_First_Name_As_on_passport__c) ||
                ('' == ImmigrationRecord.WCT_First_Name_As_on_passport__c) ||
                (null == ImmigrationRecord.WCT_Last_Name_As_on_passport__c) ||
                ('' == ImmigrationRecord.WCT_Last_Name_As_on_passport__c) ||
                (null == ImmigrationRecord.WCT_Pref_US_Location_for_Pass_Pickup__c) ||
                ('' == ImmigrationRecord.WCT_Pref_US_Location_for_Pass_Pickup__c) ||
                ('' == ImmigrationRecord.UID_Number__c) ||
                (null == ImmigrationRecord.UID_Number__c) ||
                (false == ImmigrationRecord.WCT_Attached_DS160_online_visa_form__c) ||
                (false == ImmigrationRecord.WCT_Attached_Visa_Fee_Payment_Conf__c) ||
                (false == ImmigrationRecord.WCT_Attached_Passport_Bio_Pages_copy__c) )
                {
                pageErrorMessage = 'Please fill in all the required fields on the form.';
                pageError = true;
                return null;
                }
               
                if(ImmigrationRecord.UID_Number__c.length() != 8 || (!ImmigrationRecord.UID_Number__c.isNumeric()))
                {
                   pageErrorMessage = 'Please enter a 8 digit UID Number.';
                   pageError = true;
                   return null;
                }
               
                if((ImmigrationRecord.RecordType.Name == 'H1 Visa' || ImmigrationRecord.RecordType.Name == 'L1 Visa') && (false == ImmigrationRecord.WCT_Attached_Petition_Copy__c))
                {
                   pageErrorMessage = 'Please attach a petition copy for L1 Visa or H1 Visa.';
                   pageError = true;
                   return null;
                }
              
                try{
                /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
                t.OwnerId=UserInfo.getUserId();
                upsert t;
                        
                /*update ImmigrationRecord Record*/
                ImmigrationRecord.WCT_Visa_Logistics_Info_Received__c = true;
                ImmigrationRecord.WCT_Immigration_Status__c = 'Yet to schedule visa appointment';
                update ImmigrationRecord ;
           
                if(attachmentHelper.docIdList.isEmpty()) {
                pageErrorMessage = 'Attachment is required to submit the form.';
                pageError = true;
                return null;
                }
                attachmentHelper.uploadRelatedAttachment(t.id);
                //updating task record
                If (t.WCT_Auto_Close__c == true){   
                    t.status = 'Completed';
                }else{
                    t.status = 'Employee Replied';  
                }
                if(string.valueOf(ImmigrationRecord.Ownerid).startsWith('00G')){
                    t.Ownerid = System.Label.GMI_User;
                }else{
                    t.OwnerId = ImmigrationRecord.Ownerid;
                }
                t.WCT_Is_Visible_in_TOD__c=false; 
                upsert t;
    
                getAttachmentInfo();
                pageError = false;
                }
                catch (Exception e) {
                    Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Visa_Logistics_FormController', 'Visa Logistics Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            
                Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_VALS_EXP&expCode='+errLog.Name);
                pg.setRedirect(true);
                return pg;
                }
                return new PageReference('/apex/WCT_Visa_Logistics_FormThankYou?taskid='+taskid) ;  
        
        }

}