@isTest
public class Test_H1B_ImgToPVL {

    public static testmethod void addImmigrations()         
    {
        List<Contact> conList = new List<Contact>();
        List<WCT_Immigration__c> ImList = new List<WCT_Immigration__c>();
        
        Contact con = new Contact(LastName = 'Test Immigration Contact');
        conList.add(con);            
        insert conList;
        
        WCT_Immigration__c  Im = new WCT_Immigration__c();
        Im.WCT_Assignment_Owner__c = con.id;
        Im.WCT_Immigration_Status__c = 'New';
        Im.WCT_Visa_Type__c = 'H1B CAP';
        ImList.add(Im);  
        insert ImList; 
        
        ApexPages.StandardSetController setImm = new ApexPages.StandardSetController(ImList);
        H1B_ImgToPVL Imm = new H1B_ImgToPVL(setImm);
        setImm.setSelected(ImList);
        H1B_ImgToPVL add = new H1B_ImgToPVL(setImm);
        pagereference p = add.Addimigtopvl();
    
        
    }
}