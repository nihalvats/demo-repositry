@isTest
public class WCT_parentRecordUpdateBasedOnChild{

 public static testmethod void WCT_parentRecordUpdateBasedOnChild_Test()
    {
    
    recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
  
   recordtype rt1=[select id from recordtype where DeveloperName = 'Parental']; 
     WCT_Leave__c leav = new WCT_Leave__c();
        leav.recordtypeId = rt1.id;
        leav.WCT_Leave_Start_Date__c = system.today();
        leav.WCT_Leave_End_Date__c = system.today()+1;
        leav.WCT_Employee__c = con.id;
        leav.Wct_FMLA_Continuous__c = false;
        leav.Wct_FMLA_Intermittent_Leave__c = false;
         leav.WCT_Return_to_work_date__c = null;
         
        insert leav;    
   list<WCT_Leave__c> ls = new list <WCT_Leave__c> ();
     WCT_Leave__c leav1 = new WCT_Leave__c();
         leav1.recordtypeId = rt1.id;
        leav1.WCT_Leave_Start_Date__c = system.today();
        leav1.WCT_Leave_End_Date__c = system.today()+1;
        leav1.WCT_Leave_Category__c = 'Adoption';
        leav1.WCT_Employee__c = con.id;
        leav1.WCT_Parental_Leave_Exhausted__c = true;
        leav1.WCT_Related_Record__c = leav.id;
        leav1.WCT_Return_to_work_date__c = Date.valueof(system.now());
        ls.add(leav1);
      insert ls;  
     leav.WCT_Return_to_work_date__c  =  leav1.WCT_Return_to_work_date__c;
       update leav;
    
   }

}