/********************************************************************************************************************************
Apex class         : RequestorView_CTLR
Description        : Controller which handles the view for the requestor for its Request
Type               : Controller
Test Class         : test_TRT_AddAttachment

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 02                 Deloitte                   27/02/2015          86%                         <Req / Case #>                  Original Version
************************************************************************************************************************************/
global class TRT_RequestorView_ctrl extends SitesTodHeaderController
{
    public String setBtnValue{set;get;}//holds the record id
    public Case_form_Extn__c treq{get;set;} // holds the record value of Case_form_Extn__c
    public list<Case_form_Extn__c> newValue{get;set;}//holds the list of records of Case_form_Extn__c
    public list<Case_form_Extn__c> reportingList;// holds the list of records of Case_form_Extn__c
    public string comment {get;set;}// holds the reopen comment value
    ApexPages.StandardController controller;// standard controller
    public List<caseFormWrapper> caseFormWrapperList{get;set;}
    public list<case> caseQuestionsList{get;set;}// holds the case question/suggestions list
    public string currentFeedBackId{get;set;}// to hold the feedback id
    Public decimal promptness{get;set;}
    Public decimal accuracy{get;set;}
    Public decimal helpfullness{get;set;}
    Public string commentbox{get;set;}
    public string csename{get;set;}
    public string pagename{get;set;}
    // constructor 
    public  TRT_RequestorView_ctrl()
    {
        //method to Initializing the Reporting object.
        init();
        // Method which returns the requested requests by the requestor 
        getReportingInfo();
        getCaseFormWrapper();
        
    }
    
    /********************************************************************************************
*Method Name             : Init
*Return Type             : None
*Param’s                 : None
*Description             : method to Initializing the Reporting object.
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 02                 Original Version
*********************************************************************************************/
    public Void Init()
    {
        
        //Initializing the Reporting object
        Treq = new Case_form_Extn__c();
        // Initilizing the list for holding reporting values
        newValue = new list<Case_form_Extn__c> ();
        setBtnValue = null;
    }
    /********************************************************************************************
*Method Name             : clonerequest
*Return Type             :Page reference
*Param’s                 : None
*Description             : Method that is used to clone the already existing Report.
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 02                 Original Version
*********************************************************************************************/
    
    public Pagereference clonerequest()
    {
        csename = apexPages.currentPage().getParameters().get('csename');
        system.debug('record name'+csename);
        reportingList = [select id, name, RecordType.Name from Case_form_Extn__c where id=:csename];
         system.debug('record name'+reportingList[0].RecordType.Name);
        TRT_Record_Type_Details__c trtd = TRT_Record_Type_Details__c.getInstance(reportingList[0].RecordType.Name);
        system.debug('clone'+trtd);
        if(reportingList[0].RecordType.Name == trtd.Record_Type__c){
            //  string encodeid = EncodingUtil.urlEncode(currentid , 'UTF-8');
            string encodepagename = EncodingUtil.urlEncode(trtd.TRT_Page_Name__c, 'UTF-8');
            string encodeparamname = EncodingUtil.urlEncode(trtd.TRT_Parameter_Value__c, 'UTF-8');
            string encodecsename = EncodingUtil.urlEncode(csename, 'UTF-8');
            PageReference pr = new PageReference('/apex/'+encodepagename+'?cse='+encodecsename+'&Reqn='+encodeparamname);  
            pr.setRedirect(true);  
            
            return pr;
        }
        return null;
    }
    /********************************************************************************************
*Method Name             : getReportingInfo
*Return Type             :Page reference
*Param’s                 : None
*Description             : Method which returns the requested requests by the requestor
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 02                 Original Version
*********************************************************************************************/
    
    public list<Case_form_Extn__c> getReportingInfo()
    {
        // List the pass the values in Apex Page
        system.debug('loggedInContact111'  +loggedInContact);
        if(loggedInContact != null)
        {
            reportingList=[Select id,Name,TRT_Requestor_Name__c,TRT_Feedback__c,Calculate_reopen_days__c,GEN_Case__c,TRT_Report_Title_TA__c,CreatedDate,TRT_Request_Status__c,TRT_ReOpen_Comment__c,TRT_Business_need_for_the_data_request__c,Case_Form_App__c from Case_form_Extn__c where TRT_Requestor_Name__c=:loggedInContact.id and Case_Form_App__c ='TRT Reporting' order by TRT_Sort_Order__c ASC];
        }
        return reportingList;
        
    }
    
    //Wrapper class
    public class CaseFormWrapper
    {
        public Case_form_Extn__c caseform {get;set;}
        public Id attachmentId{get;set;}
        public caseformWrapper(Case_form_Extn__c caseform,Id attachmentId)
        {
            this.caseform=caseform;
            this.attachmentId=attachmentId;
        }
    }
    
    //wrapper for attachments
    public List<caseFormWrapper> getCaseFormWrapper()
    {
        
        list<Case_form_Extn__c> caseFromList = new list<Case_form_Extn__c>();
        caseFormWrapperList = new List<caseFormWrapper>();
        list<id> caseid = new list<id>();
        map<id,id> mapAttachments = new map<id,id>();
        id mapAttachedId;
        
        if(loggedInContact!= null)
        {
            reportingList=[Select id,Name,TRT_Report_Title_TA__c,Calculate_reopen_days__c,CreatedDate,TRT_Requestor_Name__c,TRT_Requestor_Name__r.name,TRT_Assigned_Owner__c,TRT_Feedback__c,GEN_Case__c,TRT_Request_Status__c,TRT_ReOpen_Comment__c,TRT_Business_need_for_the_data_request__c from Case_form_Extn__c where TRT_Requestor_Name__c=:loggedInContact.id and Case_Form_App__c ='TRT Reporting' order by TRT_Sort_Order__c ASC];
            
            if(!reportingList.isEmpty())
            {
                for(Case_form_Extn__c cfe :reportingList)
                {
                    caseid.add(cfe.GEN_Case__c);
                }
                
                List<Attachment> attachedFiles = [select Id,parentId from Attachment where parentId =:caseid order By CreatedDate ASC];
                for(Attachment attach:attachedFiles)
                {
                    mapAttachments.put(attach.parentId,attach.id);
                }
                for(Case_form_Extn__c cfe :reportingList)
                {
                    if(mapAttachments != null && !mapAttachments.isEmpty())
                    {
                        mapAttachedId=mapAttachments.get(cfe.GEN_Case__c);
                    }
                    caseFormWrapperList.add(new caseformWrapper(cfe,mapAttachedId));
                }
            }
        }
        return caseFormWrapperList;
    }
    
    public void Hidecomment()
    {
        //String to set the value of the particular Id based on Param value
        setBtnValue = apexPages.currentPage().getParameters().get('acctidvalue');
        
        //Query to get value based on the new param value from Report Object
        newvalue =[Select id,Name,TRT_Requestor_Name__c,TRT_Report_Title_TA__c,CreatedDate,TRT_Requestor_Name__r.name,TRT_Request_Status__c,TRT_ReOpen_Comment__c,TRT_Feedback__c from Case_form_Extn__c where id=:setBtnValue limit 1];
        
    }
    /********************************************************************************************
*Method Name             : sendRequest
*Return Type             :Page reference
*Param’s                 : None
*Description             :  Method which is called to reopen the case
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 02                 Original Version
*********************************************************************************************/
    // Method which is called to reopen the case
    public Pagereference sendRequest()
    {
         String pagename = ApexPages.currentPage().getUrl();
        if(pagename != null){
        pagename = pagename.split('apex/')[1];
        }
        system.debug('################################setbutton'+setBtnValue);
        treq.id=setBtnValue;
        if(setBtnValue != null && (string.isBlank(comment)  ))
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Please enter comments'));
        }else if(setBtnValue != null && ( string.isNotBlank(comment)  ))
        {
            treq.TRT_ReOpen_Comment__c=comment;
            treq.TRT_Sap_ReOpened__c=true;
            treq.TRT_Request_Status__c='ReOpened';
            treq.TRT_Delivery_Date__c=null;//deepthi added
            treq.TRT_Delivery_Time__c=null;// deepthi added
            treq.TRT_Renegotiate_Delivery_Date__c=null;// deepthi added
            treq.TRT_Renegotiate_Delivery_Time__c=null;// deepthi added
            treq.TRT_Renegotiate_Comments__c='';// deepthi added
            treq.TRT_Status_Checkbox__c=false;//deepthi added
            Database.update(treq,false);
            if(pagename !=null){
            if(pagename.containsIgnoreCase('TRT_Requestorview_frame')){
              PageReference pr = new PageReference('https://talent.secure.force.com/internal/Frontdoor?key=trtform');  
            pr.setRedirect(true);  
                return pr;
            }
            else{
            PageReference pr = new PageReference('https://talent.secure.force.com/internal/Frontdoor?key=trtview');  
            pr.setRedirect(true); 
                return pr;
                }
            }   
        }
        return null;
    }
    
    
    
    /*
@Name        : rateAsset
@description : The following method upserts the Rating record if the User rate an report
@parameters  : ReportingId(ID),rating(decimal)
@returns     : Boolean
*/
    
    // for submitting feedback
    @RemoteAction
    global static pagereference submit(String currentFeedBackId_1,decimal accuracy_1,decimal promptness_1,decimal helpfullness_1, decimal feedback,  string commentbox_1 ){
        
        Case_form_Extn__c treq= new Case_form_Extn__c();
        treq.id=currentFeedBackId_1;
        treq.TRT_Accuracy__c=accuracy_1;
        treq.TRT_Promptness__c=promptness_1;
        treq.TRT_Helpfulness__c=helpfullness_1;
        treq.TRT_Feedback_Comments__c=commentbox_1;
        treq.TRT_Feedback__c= feedback;
        try{
            update treq;
        }
        catch(Exception e){
            system.debug('An exception occurred'+e);
            Exception_Log__c log = WCT_ExceptionUtility.logException('TRT RequesterView controller', 'TRT RequesterView page', e.getMessage());
        }
        return null;
        
    }
    
    
    // Method which returns list of case who raised suggestrion on TRT reporting tool
    public list<Case> getQuestionsInfo()
    {  
        if(loggedInContact != null){
            // List the pass the values in Apex Page 
            caseQuestionsList=[Select id,TRT_Requestor_Suggestions__c,status,WCT_ResolutionNotes__c,
                               CaseNumber  from Case where  TRT_Questions__c=true and Contact.id=:loggedInContact.id];
        }
        return caseQuestionsList;
        
    }
    
}