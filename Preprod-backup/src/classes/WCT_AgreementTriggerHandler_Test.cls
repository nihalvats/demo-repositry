@isTest 
private class WCT_AgreementTriggerHandler_Test
{
    public static testmethod void test1()
    {
       
        
        Contact con=WCT_UtilTestDataCreation.createContact();
        con.WCT_Taleo_Status__c='Offer Details Updated';    
        con.WCT_User_Group__c = 'United States';
        con.email='test@deloitte.com';       
        insert con;
        
        con.WCT_Taleo_Status__c='Offer - To be Extended';
        update con;
        WCT_Offer__c offer = [select id, WCT_status__c,WCT_Candidate__c,WCT_Address1_INTERN__c,WCT_Candidate_Tracker__c,WCT_Type_of_Hire__c from WCT_Offer__c where WCT_Candidate__c=:con.id]; 
        
        echosign_dev1__SIGN_Agreement__c agreement = new echosign_dev1__SIGN_Agreement__c();
        agreement.WCT_offer__c = offer.id;
        
        /*Mobility Sample data */
        WCT_Mobility__c testMobility= new  WCT_Mobility__c();
        testMobility.WCT_Mobility_Employee__c=con.id;
        testMobility.WCT_Assignment_Type__c='Short Term';
        testMobility.WCT_Mobility_Status__c='New';
        
        insert testMobility;
        
        system.assert(testMobility.id!=null);
               
        echosign_dev1__SIGN_Agreement__c agreementMobility = new echosign_dev1__SIGN_Agreement__c();
        agreementMobility.WCT_Mobility__c = testMobility.id;
        
        OrgWideEmailAddress owa = [select Id, Address from OrgWideEmailAddress where DisplayName= 'Deloitte US Offers' limit 1];
        
       Test.StartTest();  
           insert agreement;
           agreement.echosign_dev1__Status__c = 'Out for Signature';
           agreement.WCT_Declined_Reason__c=null;
           update agreement;
           
           agreement.echosign_dev1__Status__c = 'Cancelled / Declined';
           agreement.WCT_Declined_Reason__c='Test Reason';
           update agreement;
           
           
           /*Mobility Agreement*/
      /*     insert agreementMobility ;
           agreementMobility.echosign_dev1__Status__c = 'Out for Signature';
           update agreementMobility ;
           */
        Test.StopTest();
        
    }
    
    public static testmethod void test2()
    {
        Test.StartTest();
        Contact con=WCT_UtilTestDataCreation.createContact();
        con.WCT_Taleo_Status__c='Offer Details Updated';   
        con.WCT_User_Group__c = 'United States'; 
        con.email='test@deloitte.com';       
        insert con;
        
        con.WCT_Taleo_Status__c='Offer - To be Extended';
        update con;
        WCT_Offer__c offer = [select id, WCT_status__c,WCT_Candidate__c,WCT_Address1_INTERN__c,WCT_Candidate_Tracker__c,WCT_Type_of_Hire__c from WCT_Offer__c where WCT_Candidate__c=:con.id]; 
        offer.WCT_isOfferSentByBatch__c = true;
        echosign_dev1__SIGN_Agreement__c agreement = new echosign_dev1__SIGN_Agreement__c();
        agreement.WCT_offer__c = offer.id;
        
        
          insert agreement;
         
          agreement.echosign_dev1__Status__c = 'Signed';
          update agreement;
        Test.StopTest();
        
     }
    
    public static testmethod void test3()
    {
        Test.StartTest();
 
        Contact con=WCT_UtilTestDataCreation.createContact();
        WCT_AgreementTriggerHandler agreeObj = new WCT_AgreementTriggerHandler();
        
        con.WCT_User_Group__c = 'United States';
        con.WCT_Taleo_Status__c='Offer Details Updated';    
        con.email='test@deloitte.com';       
        insert con;
        
        con.WCT_Taleo_Status__c='Offer - To be Extended';
        update con;
       
        WCT_Offer__c offer = [select id, WCT_status__c,WCT_Candidate__c,WCT_Address1_INTERN__c,WCT_Candidate_Tracker__c,WCT_Type_of_Hire__c from WCT_Offer__c where WCT_Candidate__c=:con.id]; 
        
        offer.WCT_isOfferSentByBatch__c = true;
        echosign_dev1__SIGN_Agreement__c agreement = new echosign_dev1__SIGN_Agreement__c();

    
           
           agreement.WCT_offer__c = offer.id;
          insert agreement;
          agreement.echosign_dev1__Status__c = 'Cancelled / Declined';
          agreement.WCT_Declined_Reason__c='Test declined';
          update agreement;
               
        Test.StopTest();
        
     }
     public static testmethod void test4()
    {
        Test.startTest();
              
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        Integer countowa = [select count() from OrgWideEmailAddress];
       system.debug('::@@@@@@@owa'+countowa);
        
        WCT_Mobility__c mobRec = new WCT_Mobility__c();
        recordtype rt1=[select id from recordtype where name='Employment Visa'];
        mobRec.RecordTypeId = rt1.id;
        mobRec.WCT_Mobility_Employee__c = con.Id;
        mobRec.WCT_Mobility_Status__c = 'New';
        mobRec.WCT_Assignment_Type__c='Short Term';
        INSERT mobRec;
        
        echosign_dev1__SIGN_Agreement__c agreement = new echosign_dev1__SIGN_Agreement__c();
        agreement.WCT_Mobility__c = mobRec.id;
        
        insert agreement;
        
        agreement.echosign_dev1__Status__c = 'Draft';
         
        agreement.echosign_dev1__Status__c = 'Out for Signature';
        update agreement;
        
         Test.StopTest();
        }
        
      public static testmethod void test5(){
      Test.startTest();
      Contact con=WCT_UtilTestDataCreation.createContact();
        con.WCT_Taleo_Status__c='Offer Details Updated';    
        con.WCT_User_Group__c = 'United States';
        con.email='test@deloitte.com';       
        insert con;
      
      WTPAA_Wage_Notice__c wageRec= new WTPAA_Wage_Notice__c();
       wageRec.WTPAA_Personnel_Number__c = string.valueof(12345);
       wageRec.WTPAA_Related_To__c = con.id;
       wageRec.WTPAA_Status__c = 'New';
       wageRec.WTPAA_Notice_Type__c = 'At Hire';
       INSERT wageRec;
      
      echosign_dev1__SIGN_Agreement__c agreement = new echosign_dev1__SIGN_Agreement__c();
        agreement.WageNotice__c = wageRec.id;
        
        insert agreement;
        
        agreement.echosign_dev1__Status__c = 'Draft';
        agreement.echosign_dev1__Status__c = 'Out for Signature';
        update agreement;
         Test.StopTest();
      
      }
       
      public static testmethod void test6(){
      Test.startTest();
      Contact con=WCT_UtilTestDataCreation.createContact();
        con.WCT_Taleo_Status__c='Offer Details Updated';    
        con.WCT_User_Group__c = 'United States';
        con.email='test@deloitte.com';       
        insert con;
      
      ELE_Separation__c eleRec= new ELE_Separation__c();
       eleRec.ELE_Contact__c = con.id;
       eleRec.ELE_Case_status__c= 'open';
       INSERT eleRec;
      
      echosign_dev1__SIGN_Agreement__c agreement = new echosign_dev1__SIGN_Agreement__c();
        agreement.WCT_ELE_Separation__c = eleRec.id;
        
        insert agreement;
        
        agreement.echosign_dev1__Status__c = 'Draft';
        agreement.echosign_dev1__Status__c = 'Out for Signature';
        update agreement;
         Test.StopTest();
      
      }  
    
      public static testmethod void A()
       {
        Test.startTest();
       system.debug('@a');
       Contact con=WCT_UtilTestDataCreation.createContact();
        con.WCT_Taleo_Status__c='Offer Details Updated';    
        con.WCT_User_Group__c = 'United States';
        con.email='test@deloitte.com';       
        insert con;
        system.debug('@a1'+con);
        WCT_AgreementTriggerHandler agreeTrig = new WCT_AgreementTriggerHandler();
        Profile profileID = [SELECT Id FROM Profile WHERE Name='12_GM_&_I_RO'];    
        system.debug('::@@@@@@@pro'+profileID); 
        User userObj= new User(Alias = 'deloitte', Email='deloittetest@deloitte.com', LastName='Market Place Jobs', EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = profileID.Id, TimeZoneSidKey='America/Los_Angeles', UserName='tnax@deloitte.com');
        insert userObj;
        system.debug('::@@@@@@@user'+userObj);
        OrgWideEmailAddress countowa = [select Id, DisplayName from OrgWideEmailAddress limit 1];
        system.debug('::@@@@@@@cwa'+countowa);
        echosign_dev1__SIGN_Agreement__c agreement = new echosign_dev1__SIGN_Agreement__c();
        agreement.echosign_dev1__Cc__c = 'test@deloitte.com';
        agreement.echosign_dev1__Sender_User__c = userObj.id;
        insert agreement;
      
        echosign_dev1__SIGN_Agreement__c agr = [select id,echosign_dev1__Cc__c ,echosign_dev1__Sender_User__c,WCT_Display_Name__c  from echosign_dev1__SIGN_Agreement__c where id=:agreement.id];
        system.debug('::@@@@@@@agr'+agr);
      
        agreeTrig.sendBouncedEmail(agreement);
        system.debug('::@@@@@@@agr1'+agreeTrig);
         Test.StopTest();
       }
}