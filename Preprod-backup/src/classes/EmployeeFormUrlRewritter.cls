/**************************************************************************************
Apex class       : EmployeeFormUrlRewritter
Version          : 1.0 
Created Date     : 07/06/2016
Function         : URL Rewriter Class,Rewrites the Url accesed from the site comparing the already stored vf pages.

* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Mayur Kondra              07/06/2016            Original Version
*************************************************************************************/

global with sharing class EmployeeFormUrlRewritter implements Site.UrlRewriter {
    
    global PageReference mapRequestUrl(PageReference yourFriendlyUrl)
    {
    
    try
    {
        // Get the Url from the page and trim till last /.
        String trimmedPageName=yourFriendlyUrl.getUrl().substringAfterLast('/');
        //To store the parameters from the URL of the page.
        String parameters;
        
        //Checking for the Parameters in the URL and passing the visualforce page only.
        if(trimmedPageName.contains('?'))
        {
            //To store the parameters for passing to the New URL.
            parameters=trimmedPageName.substringAfter('?');            
            //To get only Visualforce page.
            trimmedPageName=trimmedPageName.substringBefore('?');     
        }
        
        //Getting the Rewritter Url and the visualforce page from the custom object.
        list<urlForRedirect__c> urllist =[select Redirect_URL__c,fullVfPage__c,Pass_parameter__c from urlForRedirect__c where fullVfPage__c=:trimmedPageName];
        
        //Checking the Url from the page with Visualforce page in the custom object.
        if(urllist.size()>0)
        {
            if(urllist[0].fullVfPage__c!=null && urllist[0].fullVfPage__c==trimmedPageName)
            {
                system.debug('The checkbox value is-'+urllist[0].Pass_parameter__c);
                //To generate a new URL with Parameters
                if(urllist[0].Pass_parameter__c=true && parameters!=null)
                {
                    String finalReDirectUrl;
                    //Checking for the redirect URL with the ? symbol,if not present appending the symbol.
                    if(urllist[0].Redirect_URL__c.contains('?')){
                        finalReDirectUrl=urllist[0].Redirect_URL__c+'&';
                    }
                    else
                    {
                        finalReDirectUrl=urllist[0].Redirect_URL__c+'?'; 
                    }
                    yourFriendlyUrl= new PageReference(finalReDirectUrl+parameters);
                    //system.debug('the friendly url'+yourFriendlyUrl);
                }
                //To generate a new URL without Parameters.
                else{
                    //Assigning the new URl as a page reference.
                    yourFriendlyUrl= new PageReference(urllist[0].Redirect_URL__c);
                    
                    system.debug('the friendly url'+yourFriendlyUrl);
                }
            }
         }   
        
         system.debug('the friendly url final '+yourFriendlyUrl);
         
         }
         
         Catch(Exception e)
         {
             System.debug('##### Exception '+e+'Line # '+e.getLineNumber());
             WCT_ExceptionUtility.logException('EmployeeFormUrlRewritter','NA','Line #'+e.getLineNumber()+' Error'+e.getMessage() );
             
         }
        
        return yourFriendlyUrl;
    }     
    
    global List<PageReference> generateUrlFor(List<PageReference> mySalesforceUrls)
    {
        mySalesforceUrls = new List<PageReference>();
        return null;
    }
          
}