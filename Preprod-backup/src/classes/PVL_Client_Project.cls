public class PVL_Client_Project extends SitesTodHeaderController 
{

 /****************************************************************************************************************************************** 
    * Class Name   : PVLDataTemplateController 
    * Description  : This Class is for access the PVL Data Template Form.  
       
  *****************************************************************************************************************************************/

    /*Public Variables*/
    

    Public Client_Location__c ClientLocation {get;set;}
    Public Project_H1B__c ClientProject {get;set;}
    Public string PLEngagementPrtnr_Email {get;set;}
    Public string PLEngagementPrtnr_Name {get;set;}
    public string projectId{get; set;}
    public boolean Bool_PLE_Partner {get; set;}
    
    public String selectedValues{get; set;}
    public string searchemail{get;set;}
    
    public String selectedProjectCapabilityValues {get; set;}
    public String selectedVendorValues {get; set;}
    public String selectedIndustryValues{get; set;}
    public string selectedClientId {get; set;}
    public string newClientName {get; set;}
    
    //Public List<Client_Location__c> oClientLocations{get;set;}
    //Public List<Project_H1B__c> oClientProjects;
    public integer RowIndex {get; set;}

    public boolean showalert{get;set;}
    public boolean showresults{get;set;}
    public String recordType {get;set;}
    public Boolean  renderflag {get;set;}
    public List<SelectOption> orecordtypes {get; set;}
    public Contact partnerContact{get; set;}

    // Content File Related:
    
    public blob file { get; set; }
    public String InputFileName{get;set;}
    public boolean isUploaded{get; set; }   
    public string employeeEmail {get; set;}
    public boolean employeePresent  {get; set;}
    public string strEmployeeSOQL ;
    public boolean pageError {get; set;}
    public Contact PLEngagementPrtnr_Contact  {get; set;}
    public string PLEngagementPrtnr_Service_Area {get; set;}
    public string PLEngagementPrtnr_Service_Line {get; set;}
    public List <SelectOption> BusinessVisaOptions {set;get;}     
    public String BusinessVisa {set;get;}     
    ContentVersion NewContent = new ContentVersion();
    ContentVersion ContentToPublish = new ContentVersion();
    
    public Id Selected_Client_Project_Id;
    public string LoggedInUser {get; set;}
    public string LoggedInUserName {get; set;}
    public string LoggedInUserEmail {get; set;}
    
    /* Error Message related variables */

    public String pageErrorMessage {get; set;}
    public String pageErrorMessageAll {get; set;}
    public List<Client_Location__c> locList {get;set;}
    public List<Client_Location__c> locAddList{get;set;}
    public contact Employee = new contact();

    public PVL_Client_Project(ApexPages.StandardController controller)
    {
       LoggedInUserEmail = UserInfo.getUserEmail();
       LoggedInUserName = UserInfo.getName();
       Selected_Client_Project_Id = ApexPages.currentPage().getParameters().get('id'); 
       system.debug('Selected_Client_Project_Id .........1 :' + Selected_Client_Project_Id);
       ClientProject = new Project_H1B__c();
       Bool_PLE_Partner  = true;
       if(Selected_Client_Project_Id != null)
       {
            Bool_PLE_Partner = false;
            ClientLocation = new Client_Location__c();
            //oClientLocations = new List<Client_Location__c>();
            //oClientProjects = new List<Project_H1B__c>();
            ClientProject = new Project_H1B__c();
            partnerContact= new Contact();
            
            renderflag=false;
            orecordtypes = new List<selectoption>();
            List<RecordType> RTNone= new List<RecordType>();
            List<RecordType> RTcombine= new List<RecordType>();
            
            //MultirowsController();
            
            ClientProject =  [SELECT Are_you_the_PLE_Partner__c,H1B_Project_WBS__c, PVL_Module__c,Capability__c,H1B_Client_Company__c,
                            ConnectionReceivedId,ConnectionSentId,CreatedById,CreatedDate,H1B_Project_LEP_Service_Line__c,
                            H1B_Project_Skills__c,Id,Industry__c,IsDeleted,Name,OwnerId,PL_Engagement_Prtnr_Email__c,
                            Project_Contact_1__c,Project_Contact_2__c,Project_Contact_3__c,Project_LEP_Service_Area__c,
                            PVL_PL_Engagement_Partner__c,PVL_Primary_Visa_Contact__c,PVL_Project_End_Date__c,PVL_Project_Name__c,
                            PVL_Project_Service_Area__c,PVL_Project_Service_Line__c,PVL_Project_Start_Date__c,
                            PVL_Submitted_By_Email_Id__c,PVL_Submitted_By__c,PVL_Your_role_in_the_project__c,Sub_Capability__c,
                            SystemModstamp,USI_Landed_Resources__c,USI_Resources__c,
                            Vendor__c FROM Project_H1B__c where id = : Selected_Client_Project_Id limit 1];
            //system.debug('Selected_Client_Project_Id + ClientProject .........3 :' + ClientProject );
            
            selectedClientId=ClientProject.H1B_Client_Company__c;
            updatePartnerDetails();
       }
        else
        {
            pageError=true;
            pageErrorMessage='Invalid Request';
        }
    }
    public pageReference getEmployeeDetails() {
        try {
                system.debug( 'From getEmployeeDetails....: ' );
                
                recordtype rt = [select id,Name from recordtype where DeveloperName = 'WCT_Employee'];
                employeeEmail = PLEngagementPrtnr_Email.trim();
                if ((employeeEmail == null) || (employeeEmail == '')) {
                    pageErrorMessage = 'Please enter a email ID';
                    pageError = true;
                    employeePresent = false;
                    PLEngagementPrtnr_Name = 'Employee Not Found';
                    return null;
                } else {
                    system.debug( 'From getEmployeeDetails....: ' + employeeEmail );
                    Employee = [select id, Name, Phone, WCT_Service_Area__c,WCT_Service_Line__c, Email from contact where recordtypeid = : rt.id and email = : employeeEmail limit 1];
                    employeePresent = true;
                    PLEngagementPrtnr_Name = Employee.name;
                    
                    PLEngagementPrtnr_Service_Area = Employee.WCT_Service_Area__c;
                    PLEngagementPrtnr_Service_Line = Employee.WCT_Service_Line__c;

                    PLEngagementPrtnr_Contact  = Employee;
                    system.debug( 'From getEmployeeDetails....: ' + Employee  );
                }
            }
         catch (Exception e) {
            
            pageErrorMessage = 'Email Id is not associated with any employee';
            pageError = true;
            employeePresent = false;
            return null;
        }
        pageError = false;
        return null;
    }    

     public PageReference hideSectionOnChange()
    {
      if(ClientProject.Are_you_the_PLE_Partner__c == 'Yes')
            renderflag= true;
        if(ClientProject.Are_you_the_PLE_Partner__c== 'No')
            renderflag= false;
            return null;
    }
    
    public void updatePartnerDetails()
    {
       string selectedId=ClientProject.PVL_PL_Engagement_Partner__c;
        System.debug('####Adasdasd '+selectedId);
        if(selectedId!=null || selectedId!='')
        {
           List<COntact> contacts = [Select Id,Name,Email , WCT_Service_Area__c, WCT_Service_Line__c From Contact Where id=:selectedId] ;
            partnerContact= (contacts.size()>0?contacts[0]:new Contact() );
            //ClientProject.PL_Engagement_Prtnr_Email__c = partnerContact.email;
        }
        else
        {
            partnerContact= new Contact();
        }
    }
     
     /*public void RemoveRowFromLocList()
     {
       Integer indexToRemove=Integer.valueOf(ApexPages.currentPage().getParameters().get('rowNo'));
         system.debug('## rowNo'+indexToRemove);
         if(indexToRemove<locAddList.size())
         {
             locAddList.remove(indexToRemove);             
         }
     }*/
     public PageReference DiscardSave1()
     {

         string xid ;
         xid = ClientProject.Id;
         
         //if (ClientProject.Id != null || ClientProject.Id != '')
         if (xId != null )
            {
               PageReference pageRef = new PageReference('/' + ClientProject.Id);
               pageRef.setRedirect(true);
               return pageRef;
          }
          else 
          {
              return new PageReference('/a2o/o');            
         }       
     }
     public PageReference SavePvl()
     {
         if(selectedClientId=='new' || selectedClientId=='')
         {
             H1B_Client_Company__c company = new H1B_Client_Company__c();
             company.Company_Name__c=newClientName;
           //  company.No_DUNS_Number__c='No DUNS';
             insert company;
             selectedClientId=company.id;
         }

        //ClientProject.PVL_Submitted_By__c=loggedInContact.Id;
        ClientProject.H1B_Client_Company__c = selectedClientId;
        upsert ClientProject;
        PageReference nextPage = new PageReference('/' + ClientProject.Id);
        nextPage.setRedirect(true);
        return nextPage;        
     }
    
  @RemoteAction
    public static  List<H1B_Client_Company__c> getExistingCompanies(String fieldName)
    {
        List<H1B_Client_Company__c> listItems;
        
        listItems= [Select Id, Company_Name__c from H1B_Client_Company__c ORDER BY Company_Name__c ASC limit 50000 ];
        return listItems;
     }  
    
    /*ADDED BY DEEPU */
    
     @RemoteAction
    public static  List<WCT_List_Of_Names__c> getItemsList(String fieldName)
    {
        List<WCT_List_Of_Names__c> listItems;
        
        listItems= [Select Id, Name from WCT_List_Of_Names__c where RecordTypeId =:Label.MultiSelectPicklist and MultiSelectIdentifier__c=:fieldName ORDER BY Name ASC ];
        system.debug(' List Items ............1 : ' + listItems);
        return listItems;
    }
    
    public pageReference newProject()
    {
        ClientProject= new  Project_H1B__c();
        return null;
        
    }
}