/**
    * Class Name  : WCT_StageTableBatchDelete_Test
    * Description : This apex test class will use to test the StageTableBatchDelete
*/
@isTest
private class WCT_StageTableBatchDelete_Test {

    
    public static List<WCT_PreBIStageTable__c> preBiStageList=new List<WCT_PreBIStageTable__c>();
    /** 
        Method Name  : createPreBIStageTable
        Return Type  : List<WCT_PreBIStageTable__c>
        Type 		 : private
        Description  : Create Pre-Bi Stage Table Records.         
    */
    private Static List<WCT_PreBIStageTable__c> createPreBIStageTable()
    {
    	preBiStageList=WCT_UtilTestDataCreation.createPreBIStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
    	insert preBiStageList;
    	return  preBiStageList;
    }
   
    /** 
        Method Name  : deleteStagingTableRecords
        Return Type  : void
        Type 		 : private
        Description  : Delete Pre-Bi Stage Table Records.         
    */
    static testMethod void deleteStagingTableRecords() {
        Test.startTest();
        preBiStageList=createPreBIStageTable();
        String query='Select id from WCT_PreBIStageTable__c';
        StageTableBatchDelete stageTableDelIns=new StageTableBatchDelete(query);
        database.executebatch(stageTableDelIns,200);
        Test.stopTest();
        
        
    }
}