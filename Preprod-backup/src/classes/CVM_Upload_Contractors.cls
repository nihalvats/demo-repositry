/*

Project : VRM Contractor Management
Associated VF Page : CVM_My_Request
Description:Controller which allows requestors to upload contractors in bulk.
Test Class : CVM_Class_Test
Author : Karthik Raju Gollapalli

*/


public class CVM_Upload_Contractors
{
    public integer totalrecords { get; set; }
    public integer totalsuccessrec { get; set; }
    public integer totalunsuccessrec { get; set; }
    public String nameFile { get; set; }
    public String fileName { get; set; }
    public integer size{get; set;}
       
    transient public Blob contentFile { get; set; }
    transient public List<List<String>> fileLines = new List<List<String>>();
    
    transient List<contact> contactList;
    List<String> rows;
    List<String> failedLines;
    WCT_parseCSV parseCSVInstance = new WCT_parseCSV();  
    
    public boolean b {get{
       if(rows != null && rows.size() > 1)
       {
          return true;
       }
       else 
       {
          return false;
       }
     }
    }  
      
    public Pagereference ReadFile()
    {   
        contactList = new List<contact>();
        rows = new List<String>();
        fileLines = new List<List<String>>();
                
        totalrecords = 0;
        totalsuccessrec = 0;
        totalunsuccessrec = 0;
        
        try {
            contactList = uploadProcess();            
            
        } 
       
        
        Catch(Exception e)
        {     
            System.debug('e.getCause'+e.getCause());
            System.debug('e.getLinenumber'+e.getLinenumber());
            System.debug('e.getMessage'+e.getMessage());
            
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_Exception);
           ApexPages.addMessage(errormsg);    
        }  
         if(contactList==null) { return null;}
       
        
        totalrecords = contactList.size();
        Database.SaveResult[] srList = Database.insert(contactList, false);
        System.debug('srList'+srList);
        
        for(Database.SaveResult sr: srList)
        {
            if(!sr.IsSuccess())
            {    
                for(Database.Error err : sr.getErrors())
                {
                     ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.WCT_Data_Error+err.getMessage());
                     ApexPages.addMessage(errormsg);       
                }
            }
        }           
        
        return null; 
    }     
    
    public List<contact> uploadProcess() {

      string ReqId=ApexPages.currentpage().getParameters().get('rid'); 
         
        /*Validation if no file selected */
        if(contentFile!=null)
        {
            fileName = nameFile;
            nameFile = contentFile.toString();
            System.debug('nameFile'+nameFile);
            filelines = parseCSVInstance.parseCSV(nameFile, true);
            System.debug('nameFile1'+filelines);
            failedLines = nameFile.split('\r\n');
            System.debug('failedLines**********'+failedLines);
           
        }       
        else
        {
           ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please select a file before clicking on "Submit file."'));
           return null;
        } 
      
        rows.add(failedLines[0]);
        Integer counter = 1;        
        
        for(List<String> inputvalues:filelines) 
        {       
            totalrecords++;
                 
                        if(inputvalues[0]=='' || inputvalues[1]=='' || inputvalues[2]=='') {
                
                totalunsuccessrec++;
                rows.add(failedLines[counter]);
            } 
           
                         else {       
               
                contact w = new contact();
                 w.RecordTypeId=Label.ContractorRecordType;
                 w.CVM_Request_ID__c=ReqId;
                 // system.debug('RequestId***********'+w.CVM_Request_ID__c);                 
                 w.LastName = (inputvalues.size()>0 && inputvalues[0]!='')?inputvalues[0]:'';              
                 w.FirstName = (inputvalues.size()>1 && inputvalues[1]!='')?inputvalues[1]:'';
                 w.WCT_Project_Name__c = (inputvalues.size()>2 && inputvalues[2]!='')?inputvalues[2]:'';
                 
                 if((inputvalues.size()>3 && inputvalues[3]!=null)?inputvalues[3].trim()!='':false){
                 w.CVM_Project_Start_Date__c =date.parse( (inputvalues.size()>3 && inputvalues[3]!='')?inputvalues[3]:'');
                 }
                 if((inputvalues.size()>4 && inputvalues[4]!=null)?inputvalues[4].trim()!='':false){
                 w.CVM_Project_End_Date__c =date.parse( (inputvalues.size()>4 && inputvalues[4]!='')?inputvalues[4]:''); 
                 }
                 w.CVM_Project_Location__c = (inputvalues.size()>5 && inputvalues[5]!='')?inputvalues[5]:'';
                 w.CVM_Project_New_Current__c = (inputvalues.size()>6 && inputvalues[6]!='')?inputvalues[6]:'';
                 w.CVM_Project_Language__c = (inputvalues.size()>7 && inputvalues[7]!='')?inputvalues[7]:'';
                 w.CVM_Contractor_New_Returning__c = (inputvalues.size()>8 && inputvalues[8]!='')?inputvalues[8]:'';
                 w.CVM_Contractor_EID_Number_in_eRaptor__c = (inputvalues.size()>9 && inputvalues[9]!='')?inputvalues[9]:'';
                 w.CVM_Supplier_Name__c = (inputvalues.size()>10 && inputvalues[10]!='')?inputvalues[10]:'';                  
                 if((inputvalues.size()>11 && inputvalues[11]!=null)?inputvalues[11].trim()!='':false){
                     
                     String formatedDecimal=inputvalues[11].replace('"','').replace(',','').removeStart('$').trim();                     
                     w.CVM_Supplier_Pay_Rate__c =Decimal.valueOf(formatedDecimal);
                 }
                 
                 
                 if((inputvalues.size()>12 && inputvalues[12]!=null)?inputvalues[12].trim()!='':false)
                 {
                     String formatedDecimal1=inputvalues[12].replace('"','').replace(',','').removeStart('$').trim();                         
                     w.CVM_Supplier_Bill_Rate_to_Deloitte__c =Decimal.valueOf(formatedDecimal1);   
                     
                 }
                 w.CVM_Project_Set_Lower_Bill_Rate__c = (inputvalues.size()>13 && inputvalues[13]!='')?inputvalues[13]:'';                
                //14 is empty column in template               
                 w.CVM_Alias__c = (inputvalues.size()>15 && inputvalues[15]!='')?inputvalues[15]:'';                
                 w.CVM_US_Access_Services_Complete__c = (inputvalues.size()>16 && inputvalues[16]!='')?inputvalues[16]:'';                
                 w.CVM_TC_Account_Tested__c = (inputvalues.size()>17 && inputvalues[17]!='')?inputvalues[17]:'';                
                 w.CVM_Contractor_Display_Name_in_Outlook__c = (inputvalues.size()>18 && inputvalues[18]!='')?inputvalues[18]:''; 
                
                 if((inputvalues.size()>19 && inputvalues[19]!=null)?inputvalues[19].trim()!='':false){
                 w.WCT_Personnel_Number__c = Decimal.valueOf((inputvalues.size()>19 && inputvalues[19]!='')?inputvalues[19]:''); 
                 }
                              
                 if((inputvalues.size()>20 && inputvalues[20]!=null)?inputvalues[20].trim()!='':false){
                 w.CVM_Expiration_Date_Of_SAP_Account__c = date.parse((inputvalues.size()>20 && inputvalues[20]!='')?inputvalues[20]:'');                
                 }
                 w.CVM_Ariba_DO_if_applicable__c = (inputvalues.size()>21 && inputvalues[21]!='')?inputvalues[21]:'';                
                 w.WCT_Comments__c = (inputvalues.size()>22 && inputvalues[22]!='')?inputvalues[22]:'';                
                 w.CVM_Company_Entity__c = (inputvalues.size()>23 && inputvalues[23]!='')?inputvalues[23]:'';                
                 w.CVM_Project_WBS__c = (inputvalues.size()>24 && inputvalues[24]!='')?inputvalues[24]:'';                
                 w.CVM_TimeSheet_Approval_Contact__c = (inputvalues.size()>25 && inputvalues[25]!='')?inputvalues[25]:'';                
                 w.CVM_Sr_DRS_contact__c = (inputvalues.size()>26 && inputvalues[26]!='')?inputvalues[26]:'';                
                  
                contactList.add(w);  
                        
                         totalsuccessrec++;
                 
                        }
        counter++;  
                }
       //System.debug('rows####'+rows);

        return contactList;      
    }
      
        
    public pagereference pg()
    {
        return page.ViewFailedContractorsInUI;
    }
    
    //Method to catch records failed to insert
    public String getFailedRows() {
        String result='';
        result = rows.remove(0);
        while(!rows.isEmpty()) {
            result += '\r\n' + rows[0];
            rows.remove(0);
        }
        
        return result;
    }
}