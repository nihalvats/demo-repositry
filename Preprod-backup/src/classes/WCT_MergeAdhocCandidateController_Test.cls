@isTest

private class WCT_MergeAdhocCandidateController_Test{  


static testMethod void Test(){
    test.startTest();

    Test_Data_Utility.createAccount();    

    
    Contact AdhocContact= WCT_UtilTestDataCreation.createAdhocContactsbyEmail('testAdhoc@deloitte.com');
    insert AdhocContact;
    Id AdhocID=  AdhocContact.id;   
    Profile p = [SELECT Id FROM Profile WHERE Name=:WCT_UtilConstants.RECRUITER_COMPANY]; 
    User recruitingCoordinator = WCT_UtilTestDataCreation.createUser('testest',p.id,'gjdfas@test.com','email7152@testing.com');
    insert recruitingCoordinator;
    WCT_Requisition__c Requisition_1= WCT_UtilTestDataCreation.createRequisitionbyUserEmails(null,recruitingCoordinator.Email);
    insert Requisition_1;
    insert WCT_UtilTestDataCreation.createCandidateRequisition(AdhocId,Requisition_1.id);   
    insert WCT_UtilTestDataCreation.createAdhocCase(AdhocID);
    insert WCT_UtilTestDataCreation.createAdhocTask(AdhocID);
    insert WCT_UtilTestDataCreation.createAdhocCandidateDocument(AdhocID);
    insert WCT_UtilTestDataCreation.createAdhocNotes(AdhocID);
    insert WCT_UtilTestDataCreation.createAdhocAttachment(AdhocID);
    Contact MasterContact = WCT_UtilTestDataCreation.createContactbyEmail('testContact@deloitte.com');
    insert MasterContact ;
        
    ApexPages.currentPage().getParameters().put('id',MasterContact.id);
    WCT_MergeAdhocCandidateController newMergeAdhocController= new  WCT_MergeAdhocCandidateController();
    newMergeAdhocController.MergeContactId= AdhocID;
    newMergeAdhocController.MergeAdhoc();
    newMergeAdhocController.Cancel(); 
    test.stopTest();
    }
    
}