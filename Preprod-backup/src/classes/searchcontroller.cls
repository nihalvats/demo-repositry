public with sharing class searchcontroller {

    public String PageSize { get; set; }
    public String ResultSize { get; set; }
    public String PageNumber { get; set; }
    public string namevar {get;set;}
    public string funcvar {get;set;}
    public string companyvar {get;set;}
   
    public List<Contact> lconvar{get;set;}
    transient public List<Contact> lconvarc{get;set;}
    public List<Company__c> lcompanyvar{get;set;}
    public List<Contact> lconvar1{get;set;}
    transient public List<Contact> lconvar1c{get;set;}
    public List<Contact> lconvar3{get;set;}
    transient public List<Contact> lconvar3c{get;set;}
    public List<String> lstrvar{get;set;}
    public decimal decivar {get;set;}
   
    public String val1 = null;
    public String namevarlike;
    public String funcvarlike;
    public String contactTypevar = null;
    
    private integer totalRecs = 0;
    private integer OffsetSize = 0;
    private integer LimitSize= 100;
    public integer countvar{get;set;}
    
public void searchcontroller(){
 
    totalRecs = [select count() from Contact]; 
    decivar=0;
    
}
 
    public void previous(){
        OffsetSize = OffsetSize-LimitSize;
            if(lconvar3!=Null)
                SearchbyFunction();
            if(lconvar!=Null) 
                Search();
            if(OffsetSize == 0)
                getprev();
            if(lconvar1!=Null)
                conlist1(); 
    }
    
    public void next(){
        OffsetSize = OffsetSize + LimitSize;
            if(lconvar1!=Null)
                conlist1();
            if(lconvar3!=Null)
                SearchbyFunction();
            if(lconvar!=Null) 
                Search();
            if(lconvar1!=Null)
                conlist1();

    }
    public boolean getprev(){
            if(OffsetSize == 0)
                return true;
            else
            return false;
    }
    Public void Search(){
        string Query='';
        namevarlike = null;
        contactTypevar = 'Separated';
        decivar = 1;
        namevarlike = '%'+namevar+'%';
        countvar = 0;
            if(!test.isrunningtest()){
                Query='SELECT id,Name,FirstName,LastName,WCT_Type__c,AR_Personal_Email__c,MobilePhone FROM Contact WHERE WCT_Type__c =: contactTypevar AND Name Like:namevarlike LIMIT :LimitSize OFFSET :OffsetSize';
                    system.debug('Query---'+Query);
                lconvar=Database.query(Query);
                Query='SELECT id,Name,FirstName,LastName,WCT_Type__c,AR_Personal_Email__c,MobilePhone FROM Contact WHERE WCT_Type__c =: contactTypevar AND Name Like:namevarlike';
                lconvarc=Database.query(Query);
                countvar = lconvarc.size();
            }
     
    } 
    Public void SearchbyCompany(){
        string Query='';
        namevarlike= null;
        namevarlike = '%'+companyvar+'%';
        
        if(!test.isrunningtest()){
            Query='SELECT id,Name FROM Company__c WHERE Name Like:namevarlike ';
                system.debug('Query---'+Query);
            lcompanyvar=Database.query(Query);
        }
    
    }    
    public List<SelectOption> getName(){
        List<SelectOption> lNames = new List<SelectOption>();
            if(lconvar !=Null) {
                for(Contact convar :lconvar){
                    lNames.add(new SelectOption(convar.id,convar.Name));
                }
            }
        return lNames;
    }
    public List<SelectOption> getCompanyName(){
        List<SelectOption> lNames = new List<SelectOption>();
            if(lcompanyvar !=Null){
                for(Company__c companyvar:lcompanyvar){
                    lNames.add(new SelectOption(companyvar.id,companyvar.Name));
                }
            }
    return lNames;
     }
    public String getVal1(){
    return val1;
    }
    public void setval1(String val1) { this.val1 = val1;}
       
    public void conlist1(){
        decivar=3;
        string comvar = (string)val1;
        string Query='';
        funcvarlike = null;
        contactTypevar = 'Separated';
        countvar = 0;
            if(!test.isrunningtest()){
                Query='SELECT id, FirstName, LastName,AR_Current_Company__c,AR_Current_Company__r.Name,WCT_Type__c FROM Contact WHERE WCT_Type__c =: contactTypevar AND AR_Current_Company__c =:comvar LIMIT :LimitSize OFFSET :OffsetSize';
                    system.debug('Query---'+Query);
                lconvar1=Database.query(Query);
                    system.debug('lconvar1---'+lconvar1);
                Query='SELECT id, FirstName, LastName,AR_Current_Company__c,AR_Current_Company__r.Name,WCT_Type__c FROM Contact WHERE WCT_Type__c =: contactTypevar AND AR_Current_Company__c =:comvar';
                lconvar1c=Database.query(Query);
                countvar = lconvar1c.size();
                
            }
    } 
    Public void SearchbyFunction(){
        string Query='';
        funcvarlike = null;
        contactTypevar = 'Separated';
        decivar = 5;
        funcvarlike = '%'+funcvar+'%';
        countvar = 0;
        
            if(!test.isrunningtest()){
                Query='SELECT id,Name,FirstName,LastName,WCT_Type__c,WCT_Function__c,AR_Personal_Email__c,MobilePhone FROM Contact WHERE WCT_Type__c =: contactTypevar AND WCT_Function__c Like:funcvarlike LIMIT :LimitSize OFFSET :OffsetSize';
                    system.debug('Query---'+Query);
            lconvar3=Database.query(Query);
            Query='SELECT id,Name,FirstName,LastName,WCT_Type__c,WCT_Function__c,AR_Personal_Email__c,MobilePhone FROM Contact WHERE WCT_Type__c =: contactTypevar AND WCT_Function__c Like:funcvarlike';
            lconvar3c=Database.query(Query);
            countvar = lconvar3c.size();
            }
    } 
    Public void func1(){
        if(decivar == 3 ||decivar==5)
            decivar = 0;
   
   }
}