/********************************************************************************************************************************
Apex class         : <WCT_I_129_FormController>
Description        : <Controller which allows to Update Mobility and Task>
Type               :  Controller
Test Class         : <WCT_I_129_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 25/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/ 

public class WCT_I_129_FormController extends SitesTodHeaderController{

   //Mobility Instance Variables
   
   public WCT_Mobility__c mobilityRecord{get;set;}
  
    //Upload Related Variables
    
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public List<Attachment> listAttachments {get; set;}
    public Map<Id, String> mapAttachmentSize {get; set;}  
    public WCT_Task_ManageHandler taskInstance {get;set;}
    public GBL_Attachments attachmentHelper{get; set;}
    
    //Error message Related Variables
    
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    public boolean validPage {get;set;}
    
    //Task Related Variables
    public Task taskObj{get;set;}
    public String taskID{get;set;}
     
    /* Initializing Constructor for Task and Attachments */
   
    public WCT_I_129_FormController()
    {
        taskInstance = new WCT_Task_ManageHandler();
        attachmentHelper= new GBL_Attachments();   
        init();
        if(taskInstance.isError){
           validPage = false;
          }
          else {
           validPage = true;
        }

     getAttachmentInfo();
       
    }

/********************************************************************************************
*Method Name         : <init()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Init() Used for loading Mobility and Attachments>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/   
    private void init(){

       mobilityRecord = new  WCT_Mobility__c();
       doc = new Document();
       listAttachments = new List<Attachment>();
       mapAttachmentSize = new Map<Id, String>();

    }   

/********************************************************************************************
*Method Name         : <getAttachmentInfo()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetAttachmentInfo() Used for Retrieving Attachment Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/    
    
    @TestVisible
    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId , 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId = :taskInstance.taskID 
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50 ];    
              
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                //SIZE GREATAR THAN 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                //SIZE GREATER THAN 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }

/********************************************************************************************
*Method Name         : <save()>
*Return Type         : <PageReference>
*Param’s             : 
*Description         : <Save() Used for for Updating Task and Mobility>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 
    public pageReference save()  {

       try {
       
       taskInstance.saveTask();
       taskInstance.taskObj.OwnerId=UserInfo.getUserId();
       upsert taskInstance.taskObj;
       
       if(attachmentHelper.docIdList.isEmpty()) {
            pageErrorMessage = 'Attachment is required to submit the form.';
            pageError = true;
            return null;

        }

        attachmentHelper.uploadRelatedAttachment(taskInstance.taskID);
        }
        catch (Exception e) {
         Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_I_129_FormController', 'I 129 Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            
         Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_I129_EXP&expCode='+errLog.Name);
         pg.setRedirect(true);
         return pg;
         }
        
        PageReference pageRef = new PageReference('/apex/WCT_I_129_FormThankyou?taskid='+taskInstance.taskID);
        pageRef.setRedirect(true);
        return pageRef;  
     }
}