@isTest
public class Test_MISBatchclass {
    //test method for MIS Scheduler
    static testMethod void myTest(){ 
    date dt=date.today();
    
    
    Id devRecordTypeId = Schema.SObjectType.WCT_List_Of_Names__c.getRecordTypeInfosByName().get('MIS_Report_Number').getRecordTypeId(); 
    list<WCT_List_Of_Names__c> names=new list<WCT_List_Of_Names__c>();
    WCT_List_Of_Names__c listnames=new WCT_List_Of_Names__c();
    listnames.name='sampledata';
    listnames.WCT_Type__c='testdata';
    listnames.recordtypeid=devRecordTypeId;
    listnames.MIS_Report_End_Date__c=dt;
    listnames.MIS_Report_Start_Date__c=dt;
    listnames.MIS_Frequency__c='Weekly';
    listnames.MIS_Day_Of_Report__c='FRIDAY';
    names.add(listnames);
    
     WCT_List_Of_Names__c listname=new WCT_List_Of_Names__c();
    listname.name='sampledata';
    listname.WCT_Type__c='testdata';
    listname.recordtypeid=devRecordTypeId;
    listname.MIS_Report_End_Date__c=dt;
    listname.MIS_Report_Start_Date__c=dt;
    listname.MIS_Frequency__c='DAILY';
    listname.MIS_Day_Of_Report__c='FRIDAY';
    names.add(listname);
    insert names;
         
     Test.StartTest();
        
     MISScheduler sc1 = new MISScheduler();
     String sch = '0 15 15 * * ?';
    string jobID= system.schedule('Schedule Report class',sch,sc1);
     CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
         FROM CronTrigger WHERE id = :jobId];
          System.assertEquals(sch,ct.CronExpression);
          System.assertEquals(0, ct.TimesTriggered);
        
          

     Test.stopTest();
     }
   //test method for MIS_batchclass
    static testmethod void test() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'sample12', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='sample12@testorg.com');
        insert u;

        // The query used by the batch job.
           WCT_List_Of_Names__c listn=new WCT_List_Of_Names__c();
        
        Schema.DescribeSObjectResult lt = WCT_List_Of_Names__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> ltname = lt.getRecordTypeInfosByName();// getting the record Type Info
        Id RtId =ltname.get('MIS_Report_Number').getRecordTypeId();//particular RecordId by  Name
        
        
        
        listn.name='test name';
        listn.recordtypeid=RtId;
        listn.WCT_Type__c='test';
        listn.MIS_Report_End_Date__c=Date.today();
        listn.MIS_Report_Start_Date__c=Date.today();
        listn.Status__c='active';
        listn.MIS_DL_Email_id__c='st@test.com';
        listn.MIS_Assigned_Analyst_Report__c='standarduser@testorg.com';
        insert listn;
        string query ='SELECT Id, MIS_Analyst__c,MIS_Assigned_Analyst_Report__c,MIS_Team_Name__c,MIS_Quality_Check_Analyst_Email__c,MIS_Backup_Analyst__c,MIS_Frequency__c,MIS_DL_Email_ID__c,MIS_Report_Number__c,MIS_Report_End_Date__c,MIS_Day_Of_Report__c FROM WCT_List_Of_Names__c WHERE ((RecordType.Name=\'MIS_Report_Number\')'
            +'and (MIS_Report_End_Date__c>=TODAY) and (MIS_Report_End_Date__c!=null) and (MIS_Report_Start_Date__c!=null) and (MIS_Report_Start_Date__c<=TODAY)'
            +'and (Status__c=\'active\')  and  (MIS_Report_Start_Date__c=TODAY))';
       
        string rec=listn.id;
       
    if(rec!=null){
        
        MISCreateCase.create(rec);
    }
        
        Test.startTest();
        MIS_Batchclass c = new MIS_Batchclass();
        Database.executeBatch(c);
        
        Test.stopTest();
        
        
    }
   
    //test method for emailpopup class
    static testmethod void emailpopup(){	
   
    Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'sample12', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='sample1234@testorg.com');
        insert u;
        
         WCT_List_Of_Names__c listn=new WCT_List_Of_Names__c();
        
        Schema.DescribeSObjectResult lt1 = WCT_List_Of_Names__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> ltname1 = lt1.getRecordTypeInfosByName();// getting the record Type Info
        Id RtId1 =ltname1.get('MIS_Report_Number').getRecordTypeId();//particular RecordId by  Name
        
        
        
        listn.name='test name';
        listn.recordtypeid=RtId1;
        listn.WCT_Type__c='test';
        listn.MIS_Report_End_Date__c=Date.today();
        listn.MIS_Report_Start_Date__c=Date.today();
        listn.Status__c='active';
        listn.MIS_DL_Email_id__c='st@test.com';
        listn.MIS_Report_Number__c='1234';
        listn.MIS_Name_of_the_Report__c='reportname';
        listn.MIS_Assigned_Analyst_Report__c='standarduser@testorg.com';
        insert listn;
        
        
        
       //getting the record type
         Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        Id rtId =rtMapByName.get('MIS Scheduled Report').getRecordTypeId();//particular RecordId by  Name
                
        Schema.DescribeSObjectResult Cas = Case.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames = Cas.getRecordTypeInfosByName();// getting the record Type Info
        Id caseRtId =rtMapByNames.get('Case Mail Consolidation').getRecordTypeId();//particular RecordId by  Name
        case objCase=new case();
        objCase.Status='New';
            objCase.RecordTypeId=caseRtId;
            objCase.WCT_Category__c='MIS Scheduled Report';  
            objCase.Origin = 'Web';
            objCase.Gen_Request_Type__c ='';
            objCase.Priority='3 - Medium';
            objCase.MIS_ReportsList__c=listn.id;
        
        insert objCase;
        Case_form_Extn__c MISreport=new Case_form_Extn__c();
        //inserting case form extension test record
        MISreport.RecordTypeId=rtId;
        MISreport.MIS_Status_of_Report__c='Closed';
        MISreport.GEN_Case__c=objCase.id;
        
       
        
        insert MISreport;
        
        
      
       PageReference pageRef = Page.MISEmailpopup;
       Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getParameters().put('id',String.valueOf(MISreport.id));
      
      ApexPages.StandardController sc = new ApexPages.StandardController(MISreport);
        MIS_Email_Popup em=new MIS_Email_Popup(sc);
        
         
       
          em.init();
       
       } 
    
    //test method for MIS_Requester_class
     static testmethod void validatecfe(){
        
         recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con1=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con1;
         
        MIS_Requester_class c=new MIS_Requester_class();
        
        try{
            c.loggedInContact = con1;
            c.emailcon_1=con1.id;
            c.count='2';
            c.promptness=2;
            c.accuracy=2;
            c.helpfullness=2;
            c.commentbox='test comment';
        }catch(exception e){
            system.debug('an exception occured'+e);
        }
        
        
         Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        Id rtId =rtMapByName.get('MIS Scheduled Report').getRecordTypeId();//particular RecordId by  Name
                
        Schema.DescribeSObjectResult Cas = Case.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames = Cas.getRecordTypeInfosByName();// getting the record Type Info
        Id caseRtId =rtMapByNames.get('Case Mail Consolidation').getRecordTypeId();//particular RecordId by  Name
        
        case objCase=new case();
        objCase.Status='New';
            objCase.RecordTypeId=caseRtId;
            objCase.WCT_Category__c='MIS Scheduled Report';  
            objCase.Origin = 'Web';
            objCase.Gen_Request_Type__c ='';
            objCase.Priority='3 - Medium';
        
        insert objCase;
        Case_form_Extn__c MISreport=new Case_form_Extn__c();
        //inserting case form extension
        MISreport.RecordTypeId=rtId;
       
        MISreport.GEN_Case__c=objCase.id;
        MISreport.TRT_Requestor_Name__c=con1.id;
       // MISreport.TRT_Requestor_Function__c=contact[0].WCT_Job_Level_Text__c;
      //  MISreport.MIS_Report_Uploaded__c=true;
        insert MISreport;
        
         c.getReportingInfo();
        
         Schema.DescribeSObjectResult R = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtName = R.getRecordTypeInfosByName();// getting the record Type Info
        Id rcd =rtName.get('MIS Feedback').getRecordTypeId();
        //particular RecordId by  Name
        
        Case_form_Extn__c childcfe=new  Case_form_Extn__c();
        childcfe.GEN_Case__c=objCase.id;
        childcfe.RecordTypeId=rcd;
        childcfe.MIS_Report_Detail_Object__c= MISreport.id;
        childcfe.TRT_Requestor_Name__c=con1.id;
         insert childcfe;
        
       
         MIS_Requester_class.submit(MISreport.id,2,2,2,2,'test',con1.id);
     
        
        MIS_Requester_class.countck(MISreport.id,con1.id);
       
       
    }
    //test method for AddAttachment
     static testmethod void trtAddAttachment(){
        
        case cs = new Case();
        cs.Status='New';
        cs.Origin='web';
        insert cs;
        Case_form_Extn__c cfsid = new Case_form_Extn__c();
        list<Case_form_Extn__c> c = new list<Case_form_Extn__c>();
        cfsid.GEN_Case__c=cs.id;
        insert cfsid;
        ApexPages.StandardController sc = new ApexPages.StandardController(cfsid);
        MIS_AddAttachmentclass misattach =new MIS_AddAttachmentclass(sc);
        
        ApexPages.CurrentPage().getParameters().put('id',cfsid.id);
        misattach.Cancel();
        misattach.getmyfile();
        misattach.cfsid = cfsid.id;
        Blob bodyBlobs=Blob.valueOf('Unit Test Attachment Body');
        misattach.myfile.name='test';
        misattach.myfile.body=bodyBlobs;
        misattach.Savedoc(); 
        
    }
    // test method for MIScreatecfe(add a dl)
    static testmethod void addnewdl(){
 Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        Id rtId =rtMapByName.get('MIS Scheduled Report').getRecordTypeId();//particular RecordId by  Name
        
         Schema.DescribeSObjectResult Cas = Case.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames = Cas.getRecordTypeInfosByName();// getting the record Type Info
        Id caseRtId =rtMapByNames.get('Case Mail Consolidation').getRecordTypeId();//particular RecordId by  Name
        
        case objCase=new case();
        objCase.Status='New';
            objCase.RecordTypeId=caseRtId;
            objCase.WCT_Category__c='MIS Scheduled Report';  
            objCase.Origin = 'Web';
            objCase.Gen_Request_Type__c ='';
            objCase.Priority='3 - Medium';
        
        insert objCase;
        Case_form_Extn__c MISreport=new Case_form_Extn__c();
        //inserting case form extension
        MISreport.RecordTypeId=rtId;
        MISreport.GEN_Case__c=objCase.id;
        insert MISreport;
        string recid1=MISreport.id;
        ApexPages.CurrentPage().getParameters().put('id',recid1);
        ApexPages.StandardController s = new ApexPages.StandardController(MISreport);
        MIS_Create_CFE cf=new MIS_Create_CFE(s);
        
        
         Schema.DescribeSObjectResult Con = Contact.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames1 = Con.getRecordTypeInfosByName();// getting the record Type Info
        Id caseRtId1 =rtMapByNames1.get('Employee').getRecordTypeId();//particular RecordId by  Name
        
        contact ct=new contact();
        ct.email='samplemail@example.com';
        ct.WCT_Employee_Status__c='active';
        ct.RecordTypeId=caseRtId1;
        ct.LastName='testcontact';
        insert ct;
        
        cf.emaildl='samplemail@example.com';
        cf.validate();
        
    }
}