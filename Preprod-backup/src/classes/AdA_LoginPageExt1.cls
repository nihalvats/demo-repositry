public class AdA_LoginPageExt1 extends SitesTodHeaderController{
 private Contact con;
    public Boolean practNotFound{get;set;}
    public Boolean practFound{get;set;}
    public Boolean displayAlumSearchResults{get;set;}
    public Boolean displayAlumNotFoundMsg{get;set;}
    //public Boolean searchWithinFunc{get;set;}
    public Boolean searchInAllFunc{get;set;}
    public Boolean searchInIndia{get;set;}
    public Boolean showCongrats{get;set;}
    public Boolean showUnlink{get;set;}
    public Boolean showFPXEmailSent{get;set;}
    public Boolean showAdoptUpdate{get;set;}
    public Boolean showUpdateError{get;set;}
    public Boolean showFPXChargeButton{get;set;}
    public Alumni_History_Ada__c InsertAlumnusHistory{get;set;}
    public Invalid_Practitioner__c InsertInvalidAlumnus{get;set;}
    public string adminEmail{get;set;}
    public string selTab{get;set;}
    public string AdopterNames; 
    public contact alumnus{get;set;} 
    public contact updateAndAdoptalum{get;set;} 
    public list<contact> alumniSearchList{get;set;}
   
    public string name{set;get;}
    
    public map<id,Alumni_Adopter_Associations__c> adopterId;
    public list<Alumni_Adopter_Associations__c> adoptedAlum{get;set;}
    public list<string> adopterEmailAddress;
    public string alumId{get;set;}
    public string SecurityToken{get;set;}
    public string selectedAlumId{get;set;}
    id pracRecTypeId;  
    string alumRecTypeId;
    list<FPX_Charge_AdA__c> chargecode=new list<FPX_Charge_AdA__c>();
    public Current_Employer_AdA__c currentEmp{get;set;}
    public Boolean showCurrentEmployer{get;set;}
    public Document document {
    get {
      if (document == null)
        document = new Document();
      return document;
    }
    set;
  }
    public List<relAdA> relatedAdA{get;set;}
       
    
    
    public List<Document> docLst = new List<Document>();
    public AdA_LoginPageExt1(){
       alumnus=new contact();
      //  this.con = (Contact)stdController.getRecord();
        practNotFound = FALSE;
        practFound=FALSE;
        displayAlumSearchResults=false;
        displayAlumNotFoundMsg=false;
        showCongrats=false;
        showFPXEmailSent=false;
        showAdoptUpdate=false;
        showFPXChargeButton=false;
        showCurrentEmployer=false;
        showUpdateError=false;
        showUnlink=false;
        selTab='CreateAlum';
        pracRecTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
        alumRecTypeId='Separated';
        InsertInvalidAlumnus=new Invalid_Practitioner__c();
        relatedAdA=new list<relAdA>();
        AdA_Admin_Email__c AdAAdminEmail=AdA_Admin_Email__c.getall().values();
        adminEmail=AdAAdminEmail.AdA_Admin_Email__c;
        searchInAllFunc=true;
        searchInIndia=false; 
        if(LoggedInContact != null){
        practFound=TRUE; 
            
         Contact tempContact=  [SELECT Id, WCT_Prior_Name__c,  (SELECT Id, Name , alumnus__r.name, alumnus__r.WCT_Prior_Name__c, alumnus__r.WCT_Function__c , alumnus__r.WCT_Service_Area__c, alumnus__r.Adopted_Ada_c__c  
   FROM alumnii__r where Active_Ada__c=:true) FROM Contact where Id = :loggedInContact.id ];
           // [Select Id, alumnii__r from Contact where Id = :loggedInContact.Id ];
            
            for(Alumni_Adopter_Associations__c al:tempContact.alumnii__r)
                    relatedAdA.add(new relAdA(al)); 
                chargecode=[select id,Active__c,Function__c,name from FPX_Charge_AdA__c where Function__c=:loggedInContact.WCT_Function__c and Active__c=true limit 1];
                if(chargecode.size()>0)
                    showFPXChargeButton=true; 
        }
        
    }
    public PageReference searchPract() {
        try{    
       // system.debug('&&&&&enter'+con);    
         
              system.debug('&&&&&enter2'+LoggedInContact);
            
                        if(loggedInContact<>null)
            {  
                practFound=TRUE;
               // system.debug('&&&&&enter2'+con);
                for(Alumni_Adopter_Associations__c al:loggedInContact.alumnii__r)
                    relatedAdA.add(new relAdA(al)); 
                chargecode=[select id,Active__c,Function__c,name from FPX_Charge_AdA__c where Function__c=:loggedInContact.WCT_Function__c and Active__c=true limit 1];
                if(chargecode.size()>0)
                    showFPXChargeButton=true;
                
                               return Page.AdA_TabbedPage;
                
                  
            }            
            else
            practNotFound = TRUE;
           
        
        }
        catch(Exception searchEx){
            practNotFound = TRUE;
            return null;
        }
        return null;
    }
    public Void searchAlum(){
        system.debug('sdv'+Loggedincontact);
        if(loggedInContact.id<>null){
            alumniSearchList=new list<contact>();
             String queryfirstName = '%' + alumnus.firstName + '%';
             String querylastName = '%' + alumnus.lastName + '%';
             if(SearchInIndia)
                alumniSearchList=[select id,firstName,WCT_Employee_Status__c,LastName,AR_Nickname__c,WCT_Prior_Name__c ,Previous_Last_Name_AdA__c,Email
                                ,Current_Employer__c ,name,Adopted_Ada_c__c ,WCT_Office_City_Personnel_Subarea__c ,WCT_Service_Area__c,
                                WCT_Function__c,WCT_Region__c,WCT_Terminated_Date__c from contact where WCT_Type__c=:alumRecTypeId 
                                and ((firstName like :queryfirstName) or (AR_Nickname__c like :queryfirstName)) and 
                                ((lastName like :querylastName) or (Previous_Last_Name_AdA__c like :querylastName))  limit 300];
              else if(searchInAllFunc)
                 alumniSearchList=[select id,firstName,WCT_Employee_Status__c,LastName,AR_Nickname__c,WCT_Prior_Name__c ,Previous_Last_Name_AdA__c,Email
                                ,Current_Employer__c ,name,Adopted_Ada_c__c ,WCT_Office_City_Personnel_Subarea__c ,WCT_Service_Area__c,
                                WCT_Function__c,WCT_Region__c,WCT_Terminated_Date__c from contact where WCT_Type__c=:alumRecTypeId and
                          ((firstName like :queryfirstName) or (AR_Nickname__c like :queryfirstName)) and 
                           ((lastName like :querylastName) or (Previous_Last_Name_AdA__c like :querylastName))  limit 300];
              
              else
                alumniSearchList=[select id,firstName,WCT_Employee_Status__c,LastName,AR_Nickname__c,WCT_Prior_Name__c ,Previous_Last_Name_AdA__c,Email
                                ,Current_Employer__c ,name,Adopted_Ada_c__c ,WCT_Office_City_Personnel_Subarea__c ,WCT_Service_Area__c,
                                WCT_Function__c,WCT_Region__c,WCT_Terminated_Date__c from contact where WCT_Type__c=:alumRecTypeId and WCT_Function__c =:alumnus.WCT_Function__c
                                and ((firstName like :queryfirstName) or (AR_Nickname__c like :queryfirstName)) and 
                                ((lastName like :querylastName) or (Previous_Last_Name_AdA__c like :querylastName))  limit 300];
              
                  

             
            if(alumniSearchList.size()>0){      
                displayAlumSearchResults=true;
                displayAlumNotFoundMsg=false;
            }
            else{
                displayAlumNotFoundMsg=true;
                displayAlumSearchResults=false;
                }
           
        }
        else{
           
        }
    }
      public contact queryUpdatecon(string alumId){
        
        contact updateCon=[select id,firstName,WCT_Employee_Status__c,LastName,name,WCT_Prior_Name__c,WCT_Function__c,WCT_Service_Area__c,AR_Personal_Email__c,WCT_Region__c,Current_Employer__c,
                                AR_Current_Title__c ,WCT_Office_City_Personnel_Subarea__c,WCT_Terminated_Date__c,Adopted_Ada_c__c,(select name,Adopter__r.name,Active_AdA__c ,adopter__c,id,CreatedDate,Adopter__r.Email,Adopter__r.WCT_Employee_Status__c,Adopter__r.WCT_Terminated_Date__c from adopter__r),
                                 (select id,alumnus__r.name from alumnii__r),(Select Id, Updated_By__c,Current_Employer__c,Title__c,Email__c ,Comments__c,How_well_you_know_the_alumns__c,
                                  City_AdA__c,State_AdA__c,zip_code__c From Alumni_History__r where Updated_By__c=:loggedInContact.id ) From Contact where id=:alumId];
        
        return updateCon;
    }
    public pagereference adoptAndUpdate(){
        adopterId=new map<id,Alumni_Adopter_Associations__c>();
        adopterEmailAddress=new list<string>();
        updateAndAdoptalum=new contact();
        InsertAlumnusHistory=new Alumni_History_Ada__c();
        showAdoptUpdate=false;
        showCurrentEmployer=false;
        
        if(alumId<>null)
         updateAndAdoptalum=queryUpdatecon(alumId);
     
        adoptedAlum=new list<Alumni_Adopter_Associations__c>();
        AdopterNames='<ul>';
        for( Alumni_Adopter_Associations__c ad: updateAndAdoptalum.adopter__r){
            
            adopterId.put(ad.adopter__c,ad);
            if(ad.Active_AdA__c==true && ad.Adopter__r.WCT_Employee_Status__c!='Inactive'){
                adoptedAlum.add(ad);
                adopterEmailAddress.add(ad.Adopter__r.Email);
                AdopterNames=AdopterNames+'<li>'+ad.Adopter__r.Name+'</li>';
            }
        }
        
        if(updateAndAdoptalum.Alumni_History__r.size()>0)
                InsertAlumnusHistory=updateAndAdoptalum.Alumni_History__r[0];
        
        return Page.AdA_UpdateAdoptAlum;
       
    }
  public pagereference yes(){
    showAdoptUpdate=true;
    return null;
    }
    public pagereference navigateUpdateAlum(){
        showFPXEmailSent=false;
        updateAndAdoptalum=new contact();
        InsertAlumnusHistory=new Alumni_History_Ada__c(); 
        id updateAlumId;    
        for(relAdA a: relatedAdA) {     
            if(a.selected == true){
                updateAlumId=a.AdA.alumnus__c;
            
            }
         
        } 
        if(updateAlumId<>null){
            showUpdateError=false;
           updateAndAdoptalum=queryUpdatecon(updateAlumId);
             if(updateAndAdoptalum.Alumni_History__r.size()>0)
                InsertAlumnusHistory=updateAndAdoptalum.Alumni_History__r[0];
            return page.AdA_UpdateAlum;
             return null;
        }
        else{
            showUpdateError=true;
            return null;
        }
        
    }
     public pagereference updateAlum(){
       try{
            inserAlumHis();
            selTab='CurrentAlum';
            return page.AdA_TabbedPage;
           }
       catch(DmlException ex){
            ApexPages.addMessages(ex);
            return null;
                    
        }
     }
    
     public pagereference cancel(){
        showCongrats=false;
        showCurrentEmployer=false;
        showFPXEmailSent=false;
        showUnlink=false;
        return page.AdA_TabbedPage;
     }
     public pagereference newCurrentEmp(){
        currentEmp=new Current_Employer_AdA__c();
        showCurrentEmployer=true;
        return null;
        
     }
     public void inserAlumHis(){
        if(InsertAlumnusHistory.Email__c<>null||InsertAlumnusHistory.How_well_you_know_the_alumns__c<>null||(InsertAlumnusHistory.current_employer__c<>null)||
        InsertAlumnusHistory.Location__c<>null||InsertAlumnusHistory.Title__c<>null||InsertAlumnusHistory.Comments__c<>null){
            if(currentEmp<>null&&currentEmp.name<>null){
                insert currentEmp;
                showCurrentEmployer=false;
                InsertAlumnusHistory.Current_Employer__c=currentEmp.id;
            }
            if(updateAndAdoptalum.Alumni_History__r.size()>0){               
              update InsertAlumnusHistory;

                
            }
            
            else{
                InsertAlumnusHistory.Alumnus__c =updateAndAdoptalum.id; 
                InsertAlumnusHistory.Updated_By__c=loggedInContact.id;
                insert InsertAlumnusHistory;

            }
        }
     }
     public pagereference adoptAlum(){
      
        try{
            inserAlumHis();
        
            Alumni_Adopter_Associations__c adoptAlum=new Alumni_Adopter_Associations__c();

            system.debug('adopterId' +adopterId);
            if(!adopterId.keyset().contains(loggedInContact.id)){ 
                 system.debug('qwrewyrd'+adopterEmailAddress);          
                
                adoptAlum.Adopter__c=loggedInContact.id;
                
                adoptAlum.Alumnus__r=updateAndAdoptalum;
                adoptAlum.Alumnus__c=updateAndAdoptalum.id;
                adoptAlum.active_AdA__c=true;
                adoptAlum.Send_Email__c=true;
                adoptAlum.Date_Adopted__c=system.today();
                insert adoptAlum;  
                relatedAdA.add(new relAdA(adoptAlum));         
            }
            else 
            {
                if(adopterId.get(loggedInContact.id).active_ada__c==false){
                    adoptAlum=adopterId.get(loggedInContact.id);
                    adoptAlum.Alumnus__r=updateAndAdoptalum;
                    adoptAlum.active_AdA__c=true;
                    adoptAlum.Send_Email__c=true;
                    adoptAlum.Date_Adopted__c=system.today();
                    update adoptAlum;
                    relatedAdA.add(new relAdA(adoptAlum));
                }
            }
             
             if(adopterEmailAddress.size()>0){
                 
                try{
                     
                     AdopterNames=AdopterNames+'<li>'+loggedInContact.name+'</li></ul>';
                    
                    Messaging.singleEmailMessage mail = new Messaging.singleEmailMessage ();
                    adopterEmailAddress.add(LoggedInContact.Email);
                    mail.setToAddresses(adopterEmailAddress);
                    mail.setSubject('Adopt-an-Alum – your adopted alum has also been adopted by another practitioner');
                    mail.setreplyTo('usadoptanalum@deloitte.com');
                    mail.setSenderDisplayName('US Adopt an Alum');
                    mail.setHtmlBody('Adopt-an-Alum<br/>Your Adopted Alum has also been adopted by another practitioner <br/><br/>Greetings,<br/>Your adopted alum, <b>' + updateAndAdoptalum.name +' </b>has also been adopted by <p><b>' +AdopterNames +
                                    '<br/></b>Together with these practitioners, you are an adopter for this alum. Please continue to maintain the<br/>relationship, and feel free to coordinate outreach with these other practitioner adopters.<br/><br/> Please don’t hesitate to contact us with any questions.<br/><br/>Thank you for participating in Adopt-an-Alum.<br/><br/>National Alumni Relations Team');
                    Messaging.sendEmail(new Messaging.singleEmailMessage[] { mail },false);
                   }
                catch(exception e){
                    
                } 
            }
            showCongrats=true;
         }
         
         catch(DmlException ex){
               ApexPages.addMessages(ex);
                    
         }
            return null;
     }
      public pagereference showSearch(){
      
          showCongrats=false;
          return null;
      
      }
      
      public pagereference unlink(){
          showUnlink=true;
          return null;
      }
      
      public pagereference delAlumAdoptAssociation(){
           showUnlink=false;
           showFPXEmailSent=false;
           Alumni_Adopter_Associations__c delAlum=new Alumni_Adopter_Associations__c();
           system.debug('&&&&&&&'+relatedAdA);
           list<relAdA> relatedUndelectedAdA= new List<relAdA>();

           for(relAdA a: relatedAdA) { 
            if(Test.IsRunningTest()){
            a.selected = true;
            }    
                if(a.selected == true){
                    delAlum=a.AdA;
                    delAlum.active_AdA__c=false;
                    delAlum.Send_Email__c=false;
                    delAlum.Date_Unlinked__c=system.today();
                    update delAlum; 
                }
                else
                relatedUndelectedAdA.add(a);
               system.debug('*****'+relatedUndelectedAdA);
                
           } 
            relatedAdA.clear();
            relatedAdA.addall(relatedUndelectedAdA); 
             system.debug('*****'+relatedAdA);  
            //return null;
         
          return page.AdA_TabbedPage;
         
         
          
      
      }

      
      public List<Document> getdocList(){
        Id foldId = [Select Id From Folder Where Name = 'AdA Documents'].Id;
        docLst = [Select Name,Id,LastModifiedDate From Document Where FolderId = :foldId];
        return docLst;
    }
    

     
     
     public PageReference setSecurityKey() {
     
         return(new pagereference('/apex/AdA_PractitionerRegistration'));
     
     }
     public PageReference logout() {
     
         return(new pagereference('/apex/AdA_LoginPage'));
     
     }
     
     public pagereference requestChargeCode(){ 
        if(chargecode.size()>0){ 
            if(loggedInContact.Request_Charge_Code_Ad__c)
                loggedInContact.Request_Charge_Code_Ad__c=false;
            else
                loggedInContact.Request_Charge_Code_Ad__c=true;
            loggedInContact.Charge_Code_AdA__c=chargecode[0].name;
            update loggedInContact;
            showFPXEmailSent=true;
         }
         return null;
     
     }
     public pagereference addAlumnus(){
        return (new pagereference('/apex/Ada_AlumRegistration'));
     }
     
     public pagereference registerAlumnus(){
        if(currentEmp<>null&&currentEmp.name<>null){
                insert currentEmp;
                showCurrentEmployer=false;
                InsertInvalidAlumnus.Current_Employer__c=currentEmp.id;
        }
        
        InsertInvalidAlumnus.recordTypeId=Schema.SObjectType.Invalid_Practitioner__c.getRecordTypeInfosByName().get('Invalid Alumnus').getRecordTypeId();
        InsertInvalidAlumnus.Admin_EmailId__c=adminEmail;
        InsertInvalidAlumnus.Practitioner__c=loggedInContact.id;
        if(!Test.IsRunningTest()){
        insert InsertInvalidAlumnus;
        }
        showCongrats=true;
        InsertInvalidAlumnus=new Invalid_Practitioner__c();
        return null;
        
        
     }
     public class relAdA{
        public Alumni_Adopter_Associations__c AdA {get; set;}
        public Boolean selected {get; set;}
        public relAdA(Alumni_Adopter_Associations__c a) {
        AdA = a;
        selected = false;
        }
    }
}