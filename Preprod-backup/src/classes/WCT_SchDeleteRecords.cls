/**
    * Class Name  : WCT_SchDeleteRecords 
    * Description : This apex class will use to Schedule StageTableBatchDelete class
*/

global class WCT_SchDeleteRecords implements Schedulable {
    
   String query='Select id from WCT_PreBIStageTable__c Where CreatedDate < N_DAYS_AGO:30';
   global void execute(SchedulableContext sc) {
      StageTableBatchDelete b = new StageTableBatchDelete (query); 
      database.executebatch(b,200);
   }
}