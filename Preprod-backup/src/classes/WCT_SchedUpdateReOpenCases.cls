/**************************************************************************************
Apex Class Name: WCT_schedUpdateReOpenCases
Version          : 1.0 
Created Date     : 12 Dec 2013
Function         : This class is a schedule class to execute batch class WCT_batchUpdateReOpenCases
                    This class will pass Id's of case's which need's to be updated
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                    12/11/2013              Original Version
*************************************************************************************/
global class WCT_SchedUpdateReOpenCases implements Schedulable {
    //Variable Declaration
    
    String query='SELECT id,Unread_Email_on_Closed_Case__c,(SELECT id,status FROM EmailMessages ) from Case where Unread_Email_on_Closed_Case__c = true ';
    /*
     * Method name  : Execute of schedule class
     * Description  : Gets the Case Id's of cases which needs to be updated if there are no emails which are unRead 
     * Return Type  : Nil
     * Parameter    : SchedulableContext
     */
    
    global void execute(SchedulableContext SC){
               
        WCT_batchUpdateReOpenCases b=new WCT_BatchUpdateReOpenCases(query);
        Database.executeBatch(b,2000);
        
    }
     
}