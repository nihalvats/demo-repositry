public class massUploadOfferNotes {
    
    transient public Blob contentFile { get; set; }
    public integer totalrecords { get; set; }
    public integer totalErrorrecords { get; set; }
    public String nameFile { get; set; }
    @TestVisible public String strName { get; set; }
    public List<list<String>> fileLines = new List<list<String>>();
    public List<String> str = new List<String>();
    public  List<WCT_Offer_Note__c> OfferNoteList = new List<WCT_Offer_Note__c>();
    public  List<WCT_Offer_Note__c> OfferNoteErrorList {get; set;}
    WCT_parseCSV parseCSVInstance = new WCT_parseCSV();  
    
   @TestVisible public boolean b 
  {
        get
        {
        if(OfferNoteErrorList!=null)
        {
        return OfferNoteErrorList.size()>0?true:false;
        }
        else 
        {
        return false;
        }
        }
  }
  
    

 public PageReference Save() 
 {
     OfferNoteErrorList  = new List<WCT_Offer_Note__c>();
  
     List<String>  nameList = new List<String>();
     totalrecords = 0;
     totalErrorrecords =0;
     list<string> offname = new list<string>();
     list<WCT_Offer__c>  offid = new list<WCT_Offer__c>();
     List<string>  listid = new List<string>();
     Map<integer,string> mapvar = new Map<integer,string>();
  
         if(contentFile!=null)
        {
         system.debug('Enter'); 
        
            nameFile = contentFile.toString();
            filelines = parseCSVInstance.parseCSV(nameFile, true);
             system.debug('FileLines11--'+filelines); 
             system.debug('FileLines11.size()--'+filelines.size()); 
        }
        else
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please choose a file to upload !'));
            return null;
        }
        system.debug('nameList11--'+filelines);
      for( list<string> inputname :filelines) 
      { //to collect all the offer name from excel file 
            nameList.add(inputname[0]);
            system.debug('nameList--'+nameList);
      }
 
     
        system.debug('nameList--'+nameList);
        system.debug('nameList.size()--'+nameList.size());
          
        offid = [select id,name from WCT_Offer__c where name IN : nameList]; // collecting respective offer record based on the name
        
        system.debug('offid.size() --'+ offid.size());
        system.debug('offid --'+ offid);
        
      for( WCT_Offer__c off_id  :offid) 
      {
           
           listid.add(off_id.id);
          
     }
     
     
     Map<String, Id> conIdPersonnelNoMap = new Map<String, Id>();
     
     for(WCT_Offer__c  o :offid ) {
           conIdPersonnelNoMap.put(String.valueOf(o.name), o.Id);
           System.debug('conIdPersonnelNoMap'+conIdPersonnelNoMap);
        }
        
     system.debug('conIdPersonnelNoMap' +conIdPersonnelNoMap);
     //integer i = -1;
      for(List<String> inputValues : filelines) { 
      
          system.debug('filelines.size()--'+inputValues.size());  
           
       /*     if(i<listid.size()) {   
                          i++;
                          system.debug('i--'+ i); 
                          system.debug('listid.size()--'+ listid.size());
                          system.debug('listid--'+ listid);
              }   
              
           WCT_Offer_Note__c w = new WCT_Offer_Note__c();     
                      if(listid.size()>0) {
      system.debug('listid.get(i)--'+listid.get(i)); */
          
           
   
   // w.WCT_Offer__c =      (inputvalues.size()>0 && inputvalues[0]!='')?listid.get(i):''; 
    WCT_Offer_Note__c w = new WCT_Offer_Note__c();     
       w.WCT_Offer__c =  conIdPersonnelNoMap.get(inputValues[0])   ; 
       
       system.debug('w.WCT_Offer__c' +  w.WCT_Offer__c);
    
    w.WCT_Offer_Status__c =   (inputvalues.size()>1 && inputvalues[1]!='')?inputvalues[1]:'';
    w.WCT_Offer_Reason__c = (inputvalues.size()>2 && inputvalues[2]!='')?inputvalues[2]:'';
    w.WCT_Subject__c =   (inputvalues.size()>3 && inputvalues[3]!='')?inputvalues[3]:'';
    w.WCT_Description__c =  (inputvalues.size()>4 && inputvalues[4]!='')?inputvalues[4]:''; 
    
       
    
    
      if( w.WCT_Offer__c == Null || w.WCT_Offer_Status__c  == '' ||  w.WCT_Offer_Reason__c  == '' || w.WCT_Subject__c  == '' || w.WCT_Description__c  == '') {
    
      OfferNoteErrorList.add(w); 
      totalErrorrecords++;
     }  
      else     {
      OfferNoteList.add(w);  
      totalrecords++;
       }
    }
    
    system.debug('OfferNoteList'+ OfferNoteList);
    system.debug('OfferNoteListsize--'+ OfferNoteList.size());
   Database.SaveResult[] offnoteresult = database.insert(OfferNoteList,false);
   
   system.debug('offnoteresult***' + offnoteresult);
   
  /* for(Database.SaveResult sr: offnoteresult)
        {
            if(!sr.IsSuccess())
            {    
                for(Database.Error err : sr.getErrors())
                {
                     ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.WCT_Data_Error+err.getMessage());
                     ApexPages.addMessage(errormsg);       
                }
            }
        } */
    return null;
}
public pagereference pg(){
        return page.ViewFailedOfferNotesInUI;
    }
}