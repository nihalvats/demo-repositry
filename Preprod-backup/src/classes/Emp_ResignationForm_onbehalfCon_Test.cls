@istest
public class Emp_ResignationForm_onbehalfCon_Test {
 
    public static Id Emprecdtype = Schema.SObjectType.contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
    @istest
    public static  void coverage()        
    {        
        Account accObj = new Account(name = 'Test Account');
        insert accObj;
        contact conObj = new Contact(lastname='test name',WCT_Office_City_Personnel_Subarea__c='Seattle');
        insert conObj; 
        
        case cs = new case();
        cs.ContactId=conObj.Id;
        cs.Status='New';
        cs.Priority='3 - Medium';
        cs.Origin='Email';
        cs.WCT_Category__c='Separations';
        cs.Subject='test';
        cs.Description='Test description';
        cs.ELE_Resignation_Date__c= date.newInstance(2016, 4, 14);
        cs.ELE_Are_you_an_ORCA_Card_holder__c='No';
        cs.ELE_Office_Address__c='100 S. Kings St.';
        cs.ELE_Other_Employer__c='NTT Data';
        cs.ELE_Reason_for_Leaving__c='No Return from LOA';
        cs.WCT_Country_IEF__c='Oman';
        cs.WCT_LastWorkDate__c=date.newInstance(2016, 5, 16);
     
       // cs.WCT_Future_Employer_Job_Title__c='Analyst';
      //  cs.WCT_FutureEmployerStartDate__c=datetime.newInstance(2016, 6, 17, 12, 30, 0);
        cs.ELE_Are_you_Retiring__c='No';
        cs.ELE_Comments__c='Comments';
       
        insert cs;
        Test.setCurrentPageReference(new PageReference('Page.EMP_onBehalf_ResignationForm_new'));
        
       
        cs= [Select id,contact.name,contact.WCT_Personnel_Number__c,contact.WCT_Hiring_location__c,ELE_Resignation_Date__c,
             ELE_Are_you_an_ORCA_Card_holder__c,ELE_Office_Address__c,ELE_Other_Employer__c,ELE_Reason_for_Leaving__c,
             WCT_Country_IEF__c,WCT_LastWorkDate__c,ELE_Name_of_Future_Employer__c,
             WCT_Future_Employer_Job_Title__c,WCT_FutureEmployerStartDate__c,ELE_Are_you_Retiring__c,
             WCT_PayrollTermDate__c,ELE_Comments__c,contact.WCT_Office_City_Personnel_Subarea__c,ELE_Count_of_Hours__c,ELE_Updated_by_Employee__c from case WHERE ID=:cs.Id ];
         System.currentPageReference().getParameters().put('rid', cs.id); 
        Emp_ResignationForm_onbehalfCon  ctrlr= new Emp_ResignationForm_onbehalfCon();       
      
       
        ctrlr.saverecord();
        
        Emp_ResignationForm_onbehalfCon  ctrlr1= new Emp_ResignationForm_onbehalfCon();  
        cs.ELE_Updated_by_Employee__c=true;
        update cs;
        ctrlr1.saverecord();
    } 
    
}