global class WCT_UpdateMobilityRecordsFlags {
    
    WebService static  String updateFlags(String recordId)
    {
        
        WCT_Mobility__c rec;
        try{
            rec=new WCT_Mobility__c(id=recordId);
            rec.WCT_Email_Received__c=false;
            update rec;
        
        }Catch(Exception e)
        {
            WCT_ExceptionUtility.logException('WCT_UpdateMobilityRecordsFlags','Update Records Flags',e.getMessage());
        }
        
        return 'Thanks for reviewing the Mobility record.';
        
    }

}