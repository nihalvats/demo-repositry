/************************************************************
Class : WCT_GMI_Manual_taskTest
Description : Test Class for WCT_GMI_Manual_task
*************************************************************/

@isTest

private class WCT_GMI_Manual_taskTest
{
    static testMethod void testCase1()
    {
       
       Id rtId = WCT_Util.getRecordTypeIdByLabel('Contact','Employee');
       Contact EmpContact = WCT_UtilTestDataCreation.createEmployee(rtId);
       insert EmpContact;
       WCT_Task_Reference_Table__c taskRefRecord = WCT_UtilTestDataCreation.CreateTaskRefTable();
       insert taskRefRecord;
       
       WCT_Task_Reference_Table__c taskRefRecordTwo = WCT_UtilTestDataCreation.CreateTaskRefTable();
       insert taskRefRecordTwo;
       
       WCT_Immigration__c immigRecord = WCT_UtilTestDataCreation.createImmigration(EmpContact.id);
       insert immigRecord;
        
        Task taskRecOne = WCT_UtilTestDataCreation.createTaskwithRef(immigRecord.id,taskRefRecord.id);
        insert taskRecOne;
        
        //Task taskRecTwo = WCT_UtilTestDataCreation.createTask(immigRecord.id);
        //insert taskRecTwo;
        
        PageReference pageRef = Page.WCT_GMI_Immigration_Manual_task;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',immigRecord.id);
        ApexPages.currentPage().getParameters().put('objectType','WCT_Immigration__c');
        ApexPages.currentPage().getParameters().put('rtype','WCT_Immigration__c.RecordType');
        WCT_GMI_Manual_task.Select_Task_Wrapper wrapperClass = new WCT_GMI_Manual_task.Select_Task_Wrapper(taskRefRecord);
        WCT_GMI_Manual_task controller = new WCT_GMI_Manual_task();
        
       // WCT_GMI_Manual_task.Select_Task_Wrapper wrapperClass = WCT_GMI_Manual_task.Select_Task_Wrapper(taskRefRecord);
        wrapperClass.selected = true;
        List<WCT_GMI_Manual_task.Select_Task_Wrapper> tempList = new List<WCT_GMI_Manual_task.Select_Task_Wrapper>();
        tempList.add(wrapperClass);
        controller.STW_List=tempList;
        
        controller.Save();
        controller.Cancel();
    }
    static testMethod void testCase2()
    {
        WCT_Mobility__c mobRecord = WCT_UtilTestDataCreation.CreateMobility();
        insert mobRecord;
        
        ApexPages.currentPage().getParameters().put('id',mobRecord.id);
        ApexPages.currentPage().getParameters().put('objectType','WCT_Mobility__c');
        ApexPages.currentPage().getParameters().put('rtype','WCT_Mobility__c.RecordType');
        WCT_GMI_Manual_task controller1 = new WCT_GMI_Manual_task();
        controller1.Save();
        
    }
    
   static testMethod void testCase3()
    {
       
        WCT_Task_Reference_Table__c taskRefRecordTwo = WCT_UtilTestDataCreation.CreateTaskRefTable();
        insert taskRefRecordTwo;
        
        Id rtId = WCT_Util.getRecordTypeIdByLabel('Contact','Employee');
        Contact EmpContact = WCT_UtilTestDataCreation.createEmployee(rtId);
        insert EmpContact;
       
        WCT_LCA__c lcaRec = WCT_UtilTestDataCreation.CreateLCA(EmpContact.id );
        insert lcaRec;      
        
        
        ApexPages.currentPage().getParameters().put('id',lcaRec.id);
        ApexPages.currentPage().getParameters().put('objectType','WCT_LCA__c');
        ApexPages.currentPage().getParameters().put('rtype','WCT_LCA__c.RecordType');
        WCT_GMI_Manual_task controller3 = new WCT_GMI_Manual_task();
        controller3.Save();
        
    }
}