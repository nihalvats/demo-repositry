global class WCT_ContactDependentRecordBatch implements Database.Batchable<sObject>,Schedulable, Database.Stateful{

    public string strSql{get;set;}
//-------------------------------------------------------------------------------------------------------------------------------------
//    Constructor
//-------------------------------------------------------------------------------------------------------------------------------------
    
    public WCT_ContactDependentRecordBatch(){

    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Scheduler Execute 
//-------------------------------------------------------------------------------------------------------------------------------------
    global void execute(SchedulableContext SC) {
        WCT_ContactDependentRecordBatch batch = new WCT_ContactDependentRecordBatch();
        ID batchprocessid = Database.executeBatch(batch,200);
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Set the query
//-------------------------------------------------------------------------------------------------------------------------------------
    global Database.QueryLocator start(Database.BatchableContext bc){
    
        string strSql = 'SELECT Id, WCT_Visa_Status__c, WCT_Visa_End_Date__c, WCT_GMI_Record__c, WCT_Visa_Start_Date__c, RecordType.Name FROM Contact WHERE RecordType.Name = \'Dependant\' AND WCT_GMI_Record__c = true AND WCT_Visa_End_Date__c != null AND WCT_Visa_Start_Date__c != null';
                        
        return database.getQuerylocator(strSql);   
       
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Database execute method to Process Query results for email notification
//-------------------------------------------------------------------------------------------------------------------------------------
    global void execute(Database.batchableContext bc, List<Contact> ConList){
    
        List<Contact> lstContact = new List<Contact>();
        for(Contact tmpCon : ConList){
            if(tmpCon.WCT_Visa_End_Date__c < system.today()) 
                tmpCon.WCT_Visa_Status__c = 'Visa Expired';
            else if(tmpCon.WCT_Visa_Start_Date__c <= system.today() && tmpCon.WCT_Visa_End_Date__c >= system.today())
                tmpCon.WCT_Visa_Status__c = 'Visa Approved';
            lstContact.add(tmpCon);                
        }
        if(!lstContact.isEmpty())
            Database.SaveResult[] srListCon = Database.update(lstContact, false);
    }
    
    global void finish(Database.batchableContext info){     
   
    }     
}