/********************************************************************************************************************************
Apex class         : PVL_Intake_Form 
Description        : COntroller for the PVL intake form.
Type               :  Controller 
Test Class         :  PVL_Intake_Form_Test 

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01               Deepu                 N/A                     80%                   H1B poject                        Original Version
* 02               Deepu                  05/31/2016                                   Compliance project                Added comment as part of license project.
************************************************************************************************************************************/



public without sharing class PVL_Intake_Form extends SitesTodHeaderController 
{



    /*Public Variables*/
    
    
   
    Public string PVL_Notes {get; set;}
    
        
    Public PVL_Database__c oPvlData {get;set;}
    Public Client_Location__c ClientLocation {get;set;}
    Public Project_H1B__c ClientProject {get;set;}
       
    public String selectedValues{get; set;}
    public string searchemail{get;set;}
    
    /*Logged in Details */
    public string loggedName{get; set;}
    public string loggedId{get; set;}
    public string loggedEmail{get; set;}
    public string loggedServiceArea{get; set;}
    public string loggedServiceLine{get; set;}
    
    /*Client Related Field */
    public string selectedClientId {get; set;}
    public string newClientName {get; set;}
    
    Public List<Client_Location__c> oClientLocations{get;set;}
    Public List<Project_H1B__c> oClientProjects;// {get;set;}
    public integer RowIndex {get; set;}
    
    public String recordType {get;set;}
    public String requesttype {get;set;}
    public String reqtype {get;set;}
    public Boolean  renderflag {get;set;}
    
    public List<SelectOption> orecordtypes {get; set;}
    public Document doc {get;set;}
    
    /*Partner COntact Details*/
    public Contact partnerContact{get; set;}
    
    /*Invalid Error Boolean*/
    
    public boolean pageError{get; set;}
    public string pageErrorMessage {get; set;}

    // Content File Related:
    
    public blob file { get; set; }
    public String InputFileName{get;set;}
    public boolean isUploaded{get; set; }   
       
      
    public List<Client_Location__c> locList {get;set;}
    public List<Client_Location__c> locAddList{get;set;}
    //public String memberName {get;set;}  
    
    
    public class locwrapper
    {
        public boolean check {get; set;}
        
    }
    
    public void MultirowsController()
    {
        //State, City, Street,  Zip code 
        string sql = 'select H1B_City__c,H1B_State__c,H1B_Street__c,Zip_Code__c from Client_Location__c';
        locList = Database.Query(sql); 
        locAddList = new List<Client_Location__c>();
        locAddList.add(new Client_Location__c());
     
        
    }
   
    public void AddRow()
    {
        locAddList.add(new Client_Location__c());
        system.debug('## '+loggedInContact);
    } 
    public void DeleteRow()
    {
        //locAddList.remove(new Client_Location__c());
    }     
    public PVL_Intake_Form(ApexPages.StandardController controller)
    {
        
       if(loggedInContact!=null)
       {
        oPvlData = new PVL_Database__c();
        ClientLocation = new Client_Location__c();
        oClientLocations = new List<Client_Location__c>();
        oClientProjects = new List<Project_H1B__c>();
        ClientProject = new Project_H1B__c();
        partnerContact= new Contact();
 
        renderflag=false;
        orecordtypes = new List<selectoption>();
        List<RecordType> RTNone= new List<RecordType>();
        List<RecordType> RTcombine= new List<RecordType>();
        MultirowsController();
        
        /*Loogged IN USER */
        
        List<Contact> contacts = [Select Id, Name,Email, WCT_Service_Area__c, WCT_Service_Line__c From Contact Where id =:loggedInContact.Id];
        if(contacts.size()>0)
        {
                loggedName=contacts[0].Name;
                loggedId=contacts[0].Id;
                loggedEmail=contacts[0].Email;
                loggedServiceArea=contacts[0].WCT_Service_Area__c;
                loggedServiceLine=contacts[0].WCT_Service_Line__c;
            
        }
        
        
        
       }
        else
        {
            pageError=true;
            pageErrorMessage='Invalid Request';
        }
    }
    
    
    
       public PageReference hideSectionOnChange()
     {
      if(ClientProject.Are_you_the_PLE_Partner__c == 'Yes')
            renderflag= true;
        if(ClientProject.Are_you_the_PLE_Partner__c== 'No')
            renderflag= false;
            return null;
            
    }
    
    
    
/********************************************************************************************
*Method Name         : updatePartnerDetails
*Return Type         : Void
*Param’s             : None
*Description         : Prepopulate the Contact details
*Version               Description
* -----------------------------------------------------------------------------------------------------------                 
* 01                 Original Version
*********************************************************************************************/

    public void updatePartnerDetails()
    {
        
       string selectedId=ClientProject.PVL_PL_Engagement_Partner__c;
        System.debug('####Adasdasd '+selectedId);
        
        if(selectedId!=null || selectedId!='')
        {
            List<COntact> contacts = [Select Id,Name,Email , WCT_Service_Area__c, WCT_Service_Line__c From Contact Where id=:selectedId] ;
            partnerContact= (contacts.size()>0?contacts[0]:new Contact() );
        }
        else
        {
            partnerContact= new Contact();
        }
      
    }
     
     public void RemoveRowFromLocList()
     {
        Integer indexToRemove=Integer.valueOf(ApexPages.currentPage().getParameters().get('rowNo'));
         system.debug('## rowNo'+indexToRemove);
         if(indexToRemove<locAddList.size())
         {
             locAddList.remove(indexToRemove);             
         }
     }
     public PageReference SavePvl()
     {
         
        
         
         
         if(selectedClientId=='new' || selectedClientId=='')
         {
             
            /*Check if any client exist with same name if so, the attach to that client rather than creating a new one, as compnay name is unique in nature. */ 
            
            List<H1B_Client_Company__c> existClient = [Select Id, Company_Name__c from H1B_Client_Company__c Where Company_Name__c=:newClientName];
            
            if(existClient.size()>0)
            {
             selectedClientId=existClient[0].Id;
            }
            else
            {
             H1B_Client_Company__c company = new H1B_Client_Company__c();
             company.Company_Name__c=newClientName;
             insert company;
             selectedClientId=company.id;
            }
       
             
         }
         
        system.debug('From SavePVL selectedClientId - 3 :' + selectedClientId);
         
         if(ClientProject.Are_you_the_PLE_Partner__c=='Yes')
         {
             ClientProject.PVL_Your_role_in_the_project__c='Partner';
         }
         
        ClientProject.PVL_Submitted_By__c=loggedInContact.Id;
        ClientProject.H1B_Client_Company__c=selectedClientId; 
        insert ClientProject;

           List<Client_Location__c> locToAddList = new List<Client_Location__c>();
         if (locAddList.size() > 0 )
         {
           
             for ( Client_Location__c loc : locAddList)
             {
                 loc.Project_Name__c = ClientProject.id;
                 
                 locToAddList.add (loc);
             }
             if(!locToAddList.isEmpty()) {
             upsert locToAddList;
             }
             
             
         }       
     //}
     
     ///=============================

        Id PVLId;
        //PVLId = System.currentPagereference().getParameters().get('PVLID');
        
        PVL_Database__c xpvl = new PVL_Database__c();
        
        xpvl.PVL_Date_PVL_Received__c =system.today()  ; 
       
        xpvl.PVL_Expiration_Date__c = system.today().addYears(1); 
        xpvl.Project__c=ClientProject.id;
        xpvl.Active__c=true;        
        
        insert xpvl;
        List<PVL_Database__c> xpvls=[Select Id, Name, PVL_File_Name__c,PVL_File_Nm__c From PVL_Database__c Where id =:xpvl.id]; 
         
        if(xpvls.size()>0)
        {
            
          /*Upload Attachments*/
          Attachment attachment = new Attachment();
          attachment.Name=xpvls[0].Name+'-'+xpvls[0].PVL_File_Name__c;
          attachment.body=file;
          attachment.ParentId=xpvls[0].id;
            
            insert attachment;
            
            isUploaded=true;
            string fln='';
            string extension = InputFileName.contains('.')?InputFileName.subString(InputFileName.indexOf('.'),InputFileName.length()):'';
            
            List<H1B_Client_Company__c> company = [Select Id, Company_Name__c from H1B_Client_Company__c Where id=:selectedClientId];
            //uploadContent(attachment.id, xpvls[0].Name+'-'+(company.size()>0?company[0].Company_Name__c:'')+'-'+ClientProject.PVL_Project_Name__c+'-'+System.today().format()+'-'+extension);            
            fln = xpvls[0].Name+'-'+(company.size()>0?company[0].Company_Name__c:'')+'-'+ClientProject.PVL_Project_Name__c+'-'+  Datetime.now().format('MM-dd-yyyy') +'-'+extension;
            uploadContent(attachment.id, fln);
            xpvls[0].PVL_File_Nm__c = fln;
            update xpvls[0];
         }
         
         string Name='';
         if(xpvls.size()>0)
         {
            
             Name=xpvls[0].Name;
         }
        
        // return new PageReference('/PVL_ThankYouPage?id='+xpvl.id);    
            return new PageReference('/apex/GBL_Page_Notification?key=PVL_Intake&name='+Name);  

     }
    
  @RemoteAction
    public static  List<H1B_Client_Company__c> getExistingCompanies(String fieldName)
    {
        List<H1B_Client_Company__c> listItems;
        
        listItems= [Select Id, Company_Name__c from H1B_Client_Company__c order by Company_Name__c limit 50000 ];
        return listItems;
     } 
    
    
    /*ADDED BY DEEPU */
    
     @RemoteAction
    public static  List<WCT_List_Of_Names__c> getItemsList(String fieldName)
    {
        List<WCT_List_Of_Names__c> listItems;
        
        listItems= [Select Id, Name from WCT_List_Of_Names__c where RecordTypeId =:Label.MultiSelectPicklist and MultiSelectIdentifier__c=:fieldName ORDER BY NAME ASC];
        return listItems;
        
    }
    public pageReference newProject()
    {
        ClientProject= new  Project_H1B__c();
        return null;
        
    }
    
    @future
    public static void uploadContent( String AttachmentId, string fileName)
    {
        try
        {
    System.debug('uploadContent stareted '+AttachmentId);
        
        List<Attachment> pvlAttach= [Select Id, ParentId, Name, Body From Attachment Where id =:AttachmentId];
        
        if(pvlAttach.size()>0)
        {
         ContentVersion NewContent = new ContentVersion();
         ContentVersion ContentToPublish = new ContentVersion();
        
            NewContent.title = fileName;
            NewContent.versionData = pvlAttach[0].Body;
            NewContent.pathOnClient = fileName;
            NewContent.PVL_Database__c = pvlAttach[0].ParentId;
           insert NewContent;
           System.debug('#### NewContent'+NewContent);
         List<ContentWorkSpace> CWList = [SELECT Id, Name From ContentWorkspace WHERE Name =: Label.LibraryNameToPublish];
            
            System.debug('#### CWList'+CWList);
        //    if(CWList.size()>0)
         //   {
         
         ContentToPublish = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :NewContent.Id];
            
            ContentWorkspaceDoc cwd = new ContentWorkspaceDoc();
            cwd.ContentDocumentId = ContentToPublish.ContentDocumentId;
            cwd.ContentWorkspaceId = '05840000000XcEFAA0';
            //cwd.ContentVersionId = CWList.get(0).Id;
            
           insert cwd; 
            System.debug('#### cwd'+cwd);
            
           if(cwd.id!=null)
           {
               delete pvlAttach[0];
           }
      //  }
           
        }

        
      }
        Catch(Exception e)
        {
            	
			Exception_Log__c errLog=WCT_ExceptionUtility.logException('PVL Intake Form', 'Publish to content ', e.getMessage()+' :: Line #'+e.getLineNumber()+' :: '+e.getStackTraceString());            
        }
    }
}