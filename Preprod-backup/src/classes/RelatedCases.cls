public class RelatedCases {
 public Case caseId;
 public Case caseRec;
 public List<Case> Related_Cases {get; set;}
    public RelatedCases (ApexPages.StandardController controller){
        this.caseId = (Case)controller.getRecord();
        this.caseRec = [Select ContactId, Id, ParentId, WCT_Duplicate_Of__c, WCT_RelatedCase__c from Case where Id =:caseId.Id limit 1];
        Related_Cases = new list<Case>();
        if(caseRec.WCT_RelatedCase__c != null){
            for(Case conCase: [Select Id, CaseNumber, Subject, Priority, CreatedDate, Status, ContactId  from Case where Id =: caseRec.WCT_RelatedCase__c]){
                Related_Cases.add(conCase);
            }
        }
        if(caseRec.ParentId != null){
            for(Case parentCase: [Select Id, CaseNumber, Subject, Priority, CreatedDate, Status  from Case where Id =: caseRec.ParentId]){
                Related_Cases.add(parentCase);
            }
        }
        if(caseRec.Id != null){
            for(Case childCase: [Select Id, CaseNumber, Subject, Priority, CreatedDate, Status, ParentId  from Case where ParentId =: caseRec.Id]){
                Related_Cases.add(childCase);
            }
            for(Case childCase: [Select Id, CaseNumber, Subject, Priority, CreatedDate, Status, ParentId  from Case where WCT_RelatedCase__c =: caseRec.Id]){
                Related_Cases.add(childCase);
            }
            for(Case childCase: [Select Id, CaseNumber, Subject, Priority, CreatedDate, Status, ParentId  from Case where WCT_Duplicate_Of__c =: caseRec.Id]){
                Related_Cases.add(childCase);
            }
        }
        if(caseRec.WCT_Duplicate_Of__c != null){
            for(Case dupCase: [Select Id, CaseNumber, Subject, Priority, CreatedDate, Status  from Case where Id =: caseRec.WCT_Duplicate_Of__c]){
                Related_Cases.add(dupCase);
            }
        }
    }

}