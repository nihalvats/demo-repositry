/**===============================================================================
 Class Name   : H1B_ImgToPVL
 ===============================================================================
PURPOSE:       This class is invoked when user clicks on Add To PVL
               button on the Immigration list view. This class adds the Immigration records to PVL Database.
              
                
CHANGE HISTORY
===============================================================================
DATE              NAME                         DESC

12/12/2013        Rajasekhara Reddy Yeturi     Created

===============================================================================*/

public class H1B_ImgToPVL{

     //Variable Declaration
    ApexPages.StandardSetController setCon;
    public WCT_Immigration__c imigobj {get;set;}
    public Integer selectedSize {get;set;}
    Set<Id> sObjectIds = new Set<Id>();        
    List<WCT_Immigration__c> eRTC = new List<WCT_Immigration__c>();
    
    public H1B_ImgToPVL(ApexPages.StandardSetController controller) {
       setCon = controller;
       imigobj = new WCT_Immigration__c();
       selectedSize = setCon.getSelected().size();
    
       //If no immigrations are selected, display an error message.
       if(selectedSize==0){  
        if(setCon.getRecord().getsObjectType() == WCT_Immigration__c.sObjectType) 
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one Immigration'));        
        
       } 
      for(sObject s:setCon.getSelected()) {
       sObjectIds.add(s.id);
       }
    }
    
    //This method adds the selected immigration records to PVL record.
    public pagereference Addimigtopvl(){ 
        
      for(WCT_Immigration__c er: [Select Id,PVL_Database__c from WCT_Immigration__c where id in:sObjectIds])
      {
             er.PVL_Database__c=imigobj.PVL_Database__c;
              eRTC.add(er);
      }
     
      try{ 
       update eRTC;       
      }catch(Exception e){
        ApexPages.addMessages(e);
        return null;
      } 
      
      //Redirect to the campaign detail page.
      Pagereference p= new Pagereference('/'+imigobj.PVL_Database__c);
      p.setRedirect(true);
      return p;
    }

}