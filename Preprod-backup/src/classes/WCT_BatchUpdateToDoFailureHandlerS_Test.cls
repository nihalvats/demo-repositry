@isTest
public class WCT_BatchUpdateToDoFailureHandlerS_Test
{
    @isTest public static void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con1=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con1;

        Task t1 = new Task();
        t1.subject = 'aaaaaaa Error';
        t1.description = 'bbbbb';
        t1.whoId=con1.id;
        t1.WCT_Is_Visible_in_TOD__c=true;
        t1.ActivityDate=date.today().adddays(5);
        insert t1;      
        
        WCT_ToD_Task_ToDo_Relation__c tasktod1=new WCT_ToD_Task_ToDo_Relation__c();
        tasktod1.SFDC_Task_Id__c=t1.id;
        tasktod1.TOD_TODO_Id__c='3357';
        tasktod1.Status__c = 'Update Failed';
        tasktod1.Status_Details__c = 'errorMessage';
        insert tasktod1;               
        
        Test.setMock(HttpCalloutMock.class, new WCT_MockHttpResponseGenerator_Test());
                        
        Test.starttest();
        System.schedule('ScheduleApexClassTestDelete',
                        '0 0 0 15 3 ? 2022', 
                        new WCT_BatchUpdateToDToDoFailureHandlerS());
        Test.stoptest();
    }
}