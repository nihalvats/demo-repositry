public Class checkRecursive{
    private static boolean runCandidate = true;
    private static boolean runEmployee = true;
    private static boolean runMerge = true;
    
    public static boolean runOnceCandidate(){
    if(runCandidate){
     runCandidate=false;
     return true;
    }else{
        return runCandidate;
    }
    }
    
    public static boolean runOnceEmployee(){
    if(runEmployee){
     runEmployee=false;
     return true;
    }else{
        return runEmployee;
    }
    }
    
    public static boolean runOnceMerge(){
    if(runMerge){
     runMerge=false;
     return true;
    }else{
        return runMerge;
    }
    }
    
}