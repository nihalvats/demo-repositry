@isTest(SeeAllData=false)
Public class AdATestLoginExt{
    private static testMethod void loginExt() {
       
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m1@deloitte.com';
          insert con;
         String profile = System.label.Label_for_Employee_Profile_Name; 
  User u = WCT_UtilTestDataCreation.createUser('test.m1@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m1@deloitte.com');   
      insert u;
        
         
       
         
    system.runas(u){
         AdA_Admin_Email__c ad=new AdA_Admin_Email__c();
        ad.name='AdA Admin Email';
        ad.AdA_Admin_Email__c='usadoptanalum@deloitte.com';
        insert ad;
    
        id pracRecTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
        id alumRecTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Alumni').getRecordTypeId();
         contact pract=new contact(Lastname='test', Security_Key_AdA__c='deloitte1',WCT_Employee_Group__c='Employee',AR_Deloitte_Email__c='test@test.com',WCT_Person_Id__c='756858');
       
     
        AdA_LoginPageExt1 adalogin=new AdA_LoginPageExt1();
       adalogin.name='sample';
          adalogin.searchPract();
        insert pract;
         contact pract2=new contact(Lastname='test3', Security_Key_AdA__c='deloitte1',WCT_Employee_Group__c='Employee',AR_Deloitte_Email__c='test3@test.com',WCT_Person_Id__c='756854');
        insert pract2;       
        contact alum=new contact(Lastname='test',WCT_Employee_Status__c='seperated',firstName='test',WCT_Employee_Group__c='Separated',AR_Deloitte_Email__c='test1@test.com',WCT_Person_Id__c='756958');
        insert alum;
        contact alum1=new contact(Lastname='test2',WCT_Employee_Status__c='seperated',firstName='test2',WCT_Employee_Group__c='Separated',AR_Deloitte_Email__c='test2@test.com',WCT_Person_Id__c='706958');
        insert alum1;
        
        Alumni_Adopter_Associations__c aa=new Alumni_Adopter_Associations__c(Active_AdA__c=true,Alumnus__c=alum1.id,adopter__c=pract.id);
        insert aa;

        Alumni_Adopter_Associations__c aa1=new Alumni_Adopter_Associations__c(Active_AdA__c=true,Alumnus__c=alum1.id,adopter__c=pract2.id);
        insert aa1;
        adalogin.alumnus=alum;
        adalogin.newCurrentEmp();
        adalogin.searchPract();
        adalogin.searchInAllFunc=false;
        adalogin.searchAlum();
        adalogin.searchInAllFunc=true;
        adalogin.searchAlum();
        adalogin.alumId=alum1.id;
        Current_Employer_AdA__c c=new Current_Employer_AdA__c(name='test');
        adalogin.currentEmp=c;
        adalogin.adoptAndUpdate();
        adalogin.yes();
       
        adalogin.navigateUpdateAlum();
        adalogin.updateAlum();
        //adalogin.inserAlumHis.Zip_Code__c='iii';
        adalogin.updateAlum();
        //adalogin.updateAlumId=alum.id;

        adalogin.navigateUpdateAlum();

        adalogin.cancel();
        List<string> st=new List<string>();
        st.add('test@deloitte.com');
         adalogin.adopterEmailAddress=st;
        
        adalogin.adoptAlum();
        adalogin.showSearch();
        adalogin.delAlumAdoptAssociation();
        adalogin.getdocList();
        adalogin.setSecurityKey();
        adalogin.requestChargeCode();
        adalogin.addAlumnus();
        adalogin.unlink();
        adalogin.logout();
        adalogin.registerAlumnus();
        adalogin.document=null;
        adalogin.SecurityToken='test';
        adalogin.selectedAlumId='test';
        adalogin.SearchInIndia=true;
        adalogin.searchAlum();
        
    }
        
    }
  /*  private static testMethod void loginExt3(){
        AdA_LoginPageExt1 ada = new AdA_LoginPageExt1();
        ada.searchPract();
        
    }*/
      private static testMethod void loginExt1() {
       
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m1@deloitte.com';
          con.WCT_Function__c='USA';
          insert con;
         String profile = System.label.Label_for_Employee_Profile_Name; 
  User u = WCT_UtilTestDataCreation.createUser('test.m1@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m1@deloitte.com');   
      insert u;
        
   
     /*       
        ad.adoptAlum(); */
         
    system.runas(u){
        
        AdA_Admin_Email__c ads=new AdA_Admin_Email__c();
        ads.name='AdA Admin Email';
        ads.AdA_Admin_Email__c='usadoptanalum@deloitte.com';
        insert ads;
           AdA_LoginPageExt1 ad=new AdA_LoginPageExt1();
        
        
        FPX_Charge_AdA__c f=new FPX_Charge_AdA__c();
        f.Active__c=true;
        f.Function__c='USA';         
       ad.searchPract();
        ad.adoptAndUpdate();
         List<string> st=new List<string>();
        st.add('test@deloitte.com');
        st.add('test2@deloitte.com');
        ad.adopterEmailAddress=st;
        ad.adoptAlum();
        ad.navigateUpdateAlum();
        ad.updateAlum();
       
        ad.requestChargeCode();
        map<id,Alumni_Adopter_Associations__c> adopterId=new map<id,Alumni_Adopter_Associations__c>();
        ad.adopterId.clear();
        adopterId.put(con.id,new Alumni_Adopter_Associations__c());
        ad.adopterId= adopterId;
         system.debug('test '+ ad.adopterId);
          ad.adoptAlum();
        list<string> adopterEmailAddress = new list<string>() ;
        adopterEmailAddress.add('test@test.com');
        adopterEmailAddress.add('test1@test.com');
        adopterEmailAddress.add('test2@test.com');
        ad.adopterEmailAddress = adopterEmailAddress;
         ad.adoptAlum();
    }
    }
     
}