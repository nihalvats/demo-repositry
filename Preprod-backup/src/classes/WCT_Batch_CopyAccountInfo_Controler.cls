global class WCT_Batch_CopyAccountInfo_Controler implements Database.Batchable<sObject>{

   /*  global void execute(SchedulableContext SC) {
        WCT_Batch_CopyContactContrler batch = new WCT_Batch_CopyContactContrler();
            ID batchprocessid = Database.executeBatch(batch,100); 
    }*/

     global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        String query = 'SELECT id, Name, AccountId, Account.Name, Old_Accout_Id__c, Old_Account_Name__c FROM  Contact where accountid != null and Old_Account_Name__c = null limit 45000'; 
        system.debug('test query: '+query);               
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Contact> listContactRecords) 
    { 
        System.debug(' List of Contact Records: ' + listContactRecords.size());
        for(Contact ContactRecord : listContactRecords) 
           {
                System.debug(' Contact Record : ' + ContactRecord.Id + ' - ' + ContactRecord.Name);
                ContactRecord.Old_Accout_Id__c = ContactRecord.AccountId;
                ContactRecord.Old_Account_Name__c = ContactRecord.Account.Name;
           }
            update listContactRecords;
    } 
    global void finish(Database.BatchableContext BC){    
     //   system.debug('Contact Records Processed : '  + lstContacts.size()); 
     WCT_Util.sendMailToLoginUser('Emergency Contact Inactivate Job Ran Successfully', 'Emergency Contact Inactivate Job Ran Successfully On - '+Datetime.Now());


        list<Contact> ContactsList1 = [SELECT id, Name, AccountId, Account.Name, Old_Accout_Id__c, Old_Account_Name__c FROM  Contact where accountid != null and Old_Account_Name__c = null limit 45000]; 
        if(ContactsList1.size()>0)
        {
            WCT_Batch_CopyAccountInfo_Controler batch = new WCT_Batch_CopyAccountInfo_Controler();
            ID batchprocessid = Database.executeBatch(batch,50);
        }     
    }
    
      
}