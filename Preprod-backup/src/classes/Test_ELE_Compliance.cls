/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class Test_ELE_Compliance
{
    
   /* 
    public static testMethod void testAll()
    {
          Contact con=WCT_UtilTestDataCreation.createContact();
         con.email='test@deloitte.com';       
         insert con;
         
         String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('CVM_Intake_Request')); 
         System.currentPageReference().getParameters().put('em', strEncryptEmail);
        System.currentPageReference().getParameters().put('sa', 'US');
        TodNewCaseUSController tod=new TodNewCaseUSController();
        WCT_List_Of_Names__c ln = new WCT_List_Of_Names__c();
        ln.ToD_Case_Category_Instructions__c = 'Test Data';
        ln.ToD_Case_Category__c = 'Outside Employment and Activities (APR218) – Active Employees (excludes Partners and Principals)';
        ln.Sub_Category_1__c = 'Outside Employment and Activities - Governmental/Political/University/College';
        tod.caseRecord.WCT_Support_Area__c = 'US';
        tod.caseRecord.WCT_ContactChannel__c='Phone';
        tod.caseRecord.ToD_Case_Category__c='Outside Employment and Activities (APR218) – Active Employees (excludes Partners and Principals)';
        tod.caseRecord.subject='Test Sub';
        tod.caseRecord.WCT_SubCategory1__c = 'Outside Employment and Activities - Governmental/Political/University/College';        
        tod.caseRecord.description='Test description';        
        tod.saveCase();
        
       
    }
    
    public static testMethod void TestALL2()
    {
        
        Contact con=WCT_UtilTestDataCreation.createContact();
         con.email='test@deloitte.com';       
         insert con;
         
         String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('CVM_Intake_Request')); 
         System.currentPageReference().getParameters().put('em', strEncryptEmail);
        System.currentPageReference().getParameters().put('sa', 'IN');
        TodNewCaseUSController tod=new TodNewCaseUSController();
        tod.caseRecord.WCT_Support_Area__c = 'IN';
        tod.caseRecord.WCT_ContactChannel__c='Phone';
        tod.caseRecord.ToD_Case_Category__c='Military Time Compliance - General Inquires';
        tod.caseRecord.subject='Test Sub';
        tod.caseRecord.description='Test description';
        tod.saveCase();
        //TodNewCaseUSController.AttachmentsWrapper tempWrapper= new TodNewCaseUSController.AttachmentsWrapper(true, 'Test', 'sample', '34');
        
    }
    
     public static testMethod void TestALL3()
    {
        
        Contact con=WCT_UtilTestDataCreation.createContact();
         con.email='test@deloitte.com';       
         insert con;
         
         String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('CVM_Intake_Request')); 
         System.currentPageReference().getParameters().put('em', strEncryptEmail);
        System.currentPageReference().getParameters().put('sa', 'US-PSN');
        TodNewCaseUSController tod=new TodNewCaseUSController();        
        tod.caseRecord.WCT_Support_Area__c = 'US-PSN';
        tod.caseRecord.WCT_ContactChannel__c='Phone';
        tod.caseRecord.ToD_Case_Category__c='Nepotism - Active Personnel';
        tod.caseRecord.subject='Test Sub';
        tod.caseRecord.description='Test description';
        tod.saveCase();
        
        
            if(tod.caseExt.id!=null)
            {
                GENERATE_PDF_OF_RECORD.generateCFEPDF(tod.caseExt.id);
            }
        tod.doc.body=Blob.valueOf('Test Sample ');
        tod.doc.name='Test Name';
        //tod.uploadAttachment();
        tod.getParameterInfo();
        //newTodRequest.privatemethods();
        tod.getItems();
        tod.getItems2A();
        
        tod.getCategoryList();
       // tod.uploadRelatedAttachment(); 
    }
    
      public static testMethod void ToDAttachmentsDisplayControllerTEST()
    {
       // ToDAttachmentsDisplayController  temp= new ToDAttachmentsDisplayController (null);
        
    }
    
      public static testMethod void PDFTEST()
    {
       Contact con=WCT_UtilTestDataCreation.createContact();
         con.email='test@deloitte.com';       
         insert con;
        
       List<Case> cases = new List<Case>{};
       List<Case_form_Extn__c> caseformextensions = new List<Case_form_Extn__c>{};
       Case c1 = new Case(contactId=con.Id,Status = 'New', Priority = '1 - Critical',ELE_Compliance_Case_Status__c = 'Submitted', Subject = 'Sample Subject', Description = 'Sample Description', WCT_ReportedBy__c = con.Id);
       insert c1;
        
       Case_form_Extn__c CFEInsert = new Case_form_Extn__c(GEN_Case__c = c1.Id);
       caseformextensions.add(CFEInsert);
       insert caseformextensions;
       
       
       
       ApexPages.StandardController sc = new ApexPages.StandardController(CFEInsert);
       todgeneratepdf_cfe CFERecord = new todgeneratepdf_cfe(sc);

        
       PageReference pageRef = Page.TOD_RECORD_TO_PDF;
       pageRef.getParameters().put('id', String.valueOf(CFEInsert.Id));
       Test.setCurrentPage(pageRef);        
        
    }
    */
       
}