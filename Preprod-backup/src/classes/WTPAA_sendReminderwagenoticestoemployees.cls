global class  WTPAA_sendReminderwagenoticestoemployees Implements Database.Batchable <sObject> {
    global Database.queryLocator start(Database.BatchableContext bc) {
         
        
        Date d = Date.today();
      
      if(Test.isRunningTest()){

            d = Date.today().addDays(7);

        }
    if(Test.isRunningTest()){

            d = Date.today().addDays(14);

        }
     if(Test.isRunningTest()){

            d = Date.today().addDays(21);

        }   
      if(Test.isRunningTest()){

            d = Date.today().addDays(28);

        }    
        
      if(Test.isRunningTest()){

            d = Date.today().addDays(35);

        }        
        
     if(Test.isRunningTest()){

            d = Date.today().addDays(42);

        }  
        
      if(Test.isRunningTest()){

            d = Date.today().addDays(49);

        }   
        
        if(Test.isRunningTest()){

            d = Date.today().addDays(56);

        }      
        
        if(Test.isRunningTest()){

            d = Date.today().addDays(61);

        }  
        
        
         if(Test.isRunningTest()){

            d = Date.today().addDays(68);

        }     
        
         if(Test.isRunningTest()){

            d = Date.today().addDays(75);

        }      
        
        if(Test.isRunningTest()){

            d = Date.today().addDays(82);

        }      
        
        if(Test.isRunningTest()){

            d = Date.today().addDays(89);

        }         
        
        if(Test.isRunningTest()){

            d = Date.today().addDays(96);

        
         }
    if(Test.isRunningTest()){

            d = Date.today().addDays(105);

        
         }
      if(Test.isRunningTest()){

            d = Date.today().addDays(123);

        
         }  
         
       if(Test.isRunningTest()){

            d = Date.today().addDays(140);

        
         }  
         
       if(Test.isRunningTest()){

            d = Date.today().addDays(165);

        
         }  
         
        if(Test.isRunningTest()){

            d = Date.today().addDays(180);

        
         }   
         if(Test.isRunningTest()){

            d = Date.today().addDays(194);

        
         }  
         
          if(Test.isRunningTest()){

            d = Date.today().addDays(205);

        
         } 
         
         string idvar = 'a2g40000003Myo8'; 
     String SOQL = 'SELECT Id,WTPAA_Wage_Notice__c.OwnerId, WTPAA_Employee_Email__c,WTPAA_Related_To__r.id,WTPAA_Status__c,WTPAA_Employee_Group__c,WTPAA_Is_Notice_Sent__c ,WTPAA_Reminder_for_2nd_week__c,WTPAA_Reminder_for_3rd_week__c,WTPAA_Reminder_for_4nd_week__c FROM WTPAA_Wage_Notice__c WHERE id =: idvar';
       
         
        return Database.getQueryLocator(SOQL);
       
    }

    global void execute(Database.BatchableContext bc, List<WTPAA_Wage_Notice__c> wagenotice) {
       string orgmail = 'hrcompliance@deloitte.com';
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        
       OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
       Emailtemplate et = [select id,Body, developername , IsActive from Emailtemplate where developername = 'WTPAA_Reminder_for_2_weeks' AND IsActive = true];
 
        for(WTPAA_Wage_Notice__c w: wagenotice) {
            if(w.WTPAA_Employee_Group__c <> 'Separated' && w.WTPAA_Employee_Group__c <> 'Salary continuation')
     {
           system.debug('size of the list' +wagenotice.size());
           system.debug('ID**********' +w.id);
          //  List<String> toAddresses = new List<String>{w.WTPAA_Employee_Email__c};           
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          //  mail.setToAddresses(toAddresses);
          
            mail.SetTemplateid(et.id);
            mail.setSaveAsActivity(true);
          
           mail.setTargetObjectId(w.WTPAA_Related_To__r.id);
           system.debug('ID of Contact' +w.WTPAA_Related_To__r.id);
           mail.setWhatid(w.id);
        

          
           mail.setOrgWideEmailAddressId(owe.id);
           
           
           
           system.debug('IDDDDDDDDDDD*******' +owe.id);
  
            system.debug('IDDDDDDDDDDD*******' +et.id);
            
            
   
            mailList.add(mail); 
                
           } 
              
        } 
        Messaging.sendEmail(mailList); 
         
    }

    global void finish(Database.BatchableContext bc) {
    }
 
}