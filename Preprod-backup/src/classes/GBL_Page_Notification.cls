/*****************************************************************************************************************************
Apex class         : GBL_Page_Notification
Description        : By using this controller we are going to display the Thank you or Error message notificaation
                       based on the key and some parameters..
Type               : Controller 
Test Class         : GBL_Page_Notification_Test
------------------------------------------------------------------------------------------------------------------------------
*Version         Developer                   Date          Code Coverage              Case/Req #            Description     
* ----------------------------------------------------------------------------------------------------------------------------            
* 01             Anji Reddy K              05/02/2016          80%                    <Req / Case #>        Original Version


*****************************************************************************************************************************/

public class GBL_Page_Notification {

    //Initialize Thank you page Verbiage Related
        
      
        public string Verbiagename {get; set;} // To get the Verbiage id from the command link ...
       
        public  string final_VRBG1{get;set;}
        public  string final_VRBG2 {get;set;}
        public boolean Display {get; set;}
        Map<String,String> allParams= apexpages.currentpage().getparameters();
        public string homeurl{get;set;}
        public boolean home{get;set;}
        public string labelname{get;set;}
        //Initialize custom settings-ThankYou Page
        
       
         public WCT_List_Of_Names__c lstt;
        
        //Constructor for calling getVerbiage method
        
        public GBL_Page_Notification() 
        {
            Verbiagename = Apexpages.currentPage().getParameters().get('key');
       
            if(Verbiagename != '' && Verbiagename != null)
                {
                    WCT_List_Of_Names__c lst = [select id, key__c, Verbiage_1__c, Verbiage_2__c, Is_Exception__c,Display_Home_Button__c from WCT_List_Of_Names__c where key__c =:Verbiagename];
                     if(lst.Is_Exception__c==TRUE)
                     {
                         Display =false;
                     }
                    else
                    {
                        Display = true;
                    }
                    
                    if(lst.Verbiage_1__c != '')
                    {
                        string VBG1= lst.Verbiage_1__c;
                        for(string key : allParams.keySet() )
                        {
                            if(VBG1.contains('{%'+key+'}'))
                            {
                                 VBG1=VBG1.replace('{%'+key+'}', allParams.get(key));
                            }
                            
                            else
                            {
                                final_VRBG1=VBG1;
                            }
                            
                        }
                        final_VRBG1= VBG1;
                    }
                    
                     if(lst.Verbiage_2__c != '')
                    {
                        string VBG2= lst.Verbiage_2__c;
                        for(string key : allParams.keySet() )
                        {
                            if(VBG2.contains('{%'+key+'}'))
                            {
                                 VBG2=VBG2.replace('{%'+key+'}', allParams.get(key));
                            }
                            
                            else
                            {
                                final_VRBG2=VBG2;
                            }
                            
                        }
                        final_VRBG2= VBG2;
                    }
                    if(lst.Display_Home_Button__c == true){
                       
                        
                        homeurl= Apexpages.currentPage().getParameters().get('url');
                        labelname=Apexpages.currentPage().getParameters().get('label');
                        if(homeurl !=null && labelname !=null){
                            home=true;
                        }
                        
                    }
                    
                }
                Else 
                { 
                        final_VRBG1= 'Invalid Page';
                }
         } 

}