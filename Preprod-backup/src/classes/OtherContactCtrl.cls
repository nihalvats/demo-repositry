/*****************************************************************************************
    Name    : OtherContactCtrl 
    Desc    : This class is user to provide edit page for Other Contact Layout. 
    Approach: 
                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Ashish Shishodiya               20 Aug, 2013    Created
Ashish Shishodiya               11 Sept, 2013   Modified  
******************************************************************************************/

public without sharing class OtherContactCtrl {

    public String taleoId { get; set; }

    public String des { get; set; }

    public String contactAddress { get; set; }

    public String type { get; set; }

    public String externalEmail { get; set; }

    public String Lead { get; set; }

    public Account accntName { get; set; }

    public String ssn { get; set; }

    public Contact empRelated { get; set; }

    public Date birthdate { get; set; }

    public String lastname { get; set; }

    public String dependentType { get; set; }

    public String firstname { get; set; }

    public String contactTitle { get; set; }
    
    public String errorMsg{get;set;}
    
    public boolean showError{get;set;}

public Id contactId{get;set;}
public Contact con{get;set;}
public String phone{get;set;}
public Contact otherContact{get;set;}
public PageReference pg;


public OtherContactCtrl(){
contactId = apexpages.currentpage().getparameters().get('id');
System.debug('******ContactId********'+contactId);
con = [Select c.WCT_Type_of_Caller__c, c.WCT_Taleo_Id__c,c.title, c.WCT_ExternalEmail__c, c.Phone,c.WCT_Contact_Address__c, c.LastName, c.FirstName, WCT_Dercription_of_caller__c, c.AccountId From Contact c where id =: contactId]; 
contactTitle = con.title;
firstname = con.firstName;
lastName = con.LastName;
phone = con.phone;
externalEmail = con.WCT_ExternalEmail__c;

type = con.WCT_Type_of_Caller__c;
contactAddress = con.WCT_Contact_Address__c;
taleoId = con.WCT_Taleo_Id__c;
des = con.WCT_Dercription_of_caller__c;
}



public PageReference saveContact(){
System.debug('******ContactId********'+contactId);

con.WCT_Type_of_Caller__c = type;
con.WCT_Taleo_Id__c = taleoId;
con.title = contactTitle;
con.WCT_ExternalEmail__c = externalEmail;
con.WCT_Dercription_of_caller__c = des;
con.Phone = phone;
con.LastName = lastName;
con.FirstName = firstName;
con.WCT_Contact_Address__c = contactAddress;
con.WCT_Dercription_of_caller__c = des;

update con;
return new PageReference('/'+con.id);
}
public PageReference can(){

return new PageReference('/'+con.id);
}

Public void sub(){
if(errorMsg != null){
String[] arrError = errorMsg.split(',');
showError = true;
for(String s:arrError){
system.debug('Value of String is  ???????????????????'+ string.valueof(s));
if(s != 'Test')
ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,s));
}
}

}
}