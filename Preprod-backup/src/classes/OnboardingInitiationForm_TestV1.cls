@isTest
public class OnboardingInitiationForm_TestV1 

{
 
   /*  public static testmethod void m3()
    {
      
      recordtype rt=[select id,Name from recordtype where DeveloperName = 'WCT_Employee'];
      recordtype mobRecType = [select id from recordtype where DeveloperName = 'Employment_Visa'];
      
      Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
      String employeeType = 'Employee';
      
      WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
      
        mob.WCT_India_Cellphone__c='12345678';
        mob.WCT_Accompanied_Status__c=true;
        mob.WCT_Number_Of_Children__c=1;
        mob.WCT_Accompanying_Dependents__c=2;
        mob.WCT_US_Project_Mngr__c='Testing@mobility.com';
        mob.WCT_Regional_Dest__c='Testing';
        mob.WCT_USI_RCCode__c='Testing';
        mob.WCT_Project_Controller__c = 'test@mobility.com';
        mob.RecordTypeId = mobRecType.Id;
      insert mob;
        
      WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
      
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
      insert taskRef; 
      
      task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        t.Whatid=mob.id;
        insert t;
       
     PageReference pageRef = Page.WCT_OnboardingInitiationForm;
     
        Test.setCurrentPage(pageRef);
         
     WCT_OnboardingInitiationForm controller=new WCT_OnboardingInitiationForm(); 
     GBL_Attachments attachmentHelper = new GBL_Attachments();
          
     datetime assignstartdate=date.Today().adddays(2);
     datetime assignenddate=date.Today().adddays(5);
       
         controller.init(); 
         controller.getParameterInfo(); 
         controller.employeeType='Employee';
         controller.getMobilityTypeDropDownValues(); 
         controller.employeeType='Employee';
         controller.getMobilityTypeDropDownValues(); 
         controller.taskid=t.id;
         controller.getTaskInstance();
         controller.getMobilityDetails();
         
     Document doc= new Document();
         doc.Name='test';
         doc.Body=Blob.valueOf('test');
         doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
         insert doc;
            
         controller.attachmentHelper.docIdList.add(doc.Id);
         controller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'20'));
         controller.getAttachmentInfo(); 
         controller.save(); 
         
         controller.assignmentRec.WCT_Client_Office_Name__c='test';
         controller.assignmentRec.WCT_Client_Office_City__c='test';
         controller.assignmentRec.WCT_Client_Office_State__c='test';
         controller.assignmentstartdate = assignstartdate.format('MM/dd/yyyy');
         controller.assignmentenddate = assignenddate.format('MM/dd/yyyy');
         controller.assignmentRec.WCT_Regional_Dest__c='test';
         controller.employeeType = 'Employee';
         controller.save(); 
         
         controller.assignmentRec.WCT_Initiation_Date__c=Date.newInstance(2016,5,23);
         controller.assignmentRec.WCT_End_Date__c=Date.newInstance(2016,5,20); 
         controller.save(); 
       
    }
    
    */
    
    public static testmethod void m4()
    {
       
        recordtype rt=[select id,Name from recordtype where DeveloperName = 'WCT_Employee'];
        recordtype mobRecType = [select id from recordtype where DeveloperName = 'Employment_Visa'];
        recordtype mobRecTypeB = [select id from recordtype where DeveloperName = 'Business_Visa'];
        
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
            insert con;
        String employeeType = 'Employee';
        
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
            mob.WCT_India_Cellphone__c='12345678';
            mob.WCT_Accompanied_Status__c=true;
            mob.WCT_Number_Of_Children__c=1;
            mob.WCT_Accompanying_Dependents__c=2;
            mob.WCT_US_Project_Mngr__c='Testing@mobility.com';
            mob.WCT_Regional_Dest__c='Testing';
            mob.WCT_USI_RCCode__c='Testing';
            mob.WCT_Project_Controller__c = 'test@mobility.com';
            mob.RecordTypeId = mobRecType.Id;
        
        insert mob;
        
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
            taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
            taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef; 
        
       task t=WCT_UtilTestDataCreation.createTask(mob.id);
            t.OwnerId=UserInfo.getUserId();
            t.WCT_Task_Reference_Table_ID__c= taskRef.id;
            t.WCT_Auto_Close__c=false;
            t.Whatid=mob.id;
        insert t;
        
        datetime startdate=date.Today().adddays(10);
        datetime assignstartdate=date.Today().adddays(2);
        datetime assignenddate=date.Today().adddays(5);
        
        Test.starttest();
       
        PageReference pageRef = Page.WCT_OnboardingInitiationForm;
        Test.setCurrentPage(pageRef); 
       
        WCT_OnboardingInitiationForm controller=new WCT_OnboardingInitiationForm();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
            controller=new WCT_OnboardingInitiationForm();
            controller.mobilityRecord.recordTypeId=mobRecType.Id;
            
            
            controller.getAttachmentInfo();           
            controller.save();
            
            controller.employeeType='Employee';
            controller.getMobilityTypeDropDownValues();
            controller.employeeType='Employee';
            controller.save();
             
            controller.dateOfArrival=startdate.format('MM/dd/yyyy');
            controller.assignmentRec.WCT_Client_Office_Name__c='test';
            controller.assignmentRec.WCT_Client_Office_City__c='test';
            controller.assignmentRec.WCT_Client_Office_State__c='test';
            controller.assignmentstartdate = assignstartdate.format('MM/dd/yyyy');
            controller.assignmentenddate = assignenddate.format('MM/dd/yyyy');
            controller.assignmentRec.WCT_Regional_Dest__c='test';
            controller.employeeType = 'Employee';
            controller.assignmentRec.WCT_Initiation_Date__c=system.today()-5;
            controller.assignmentRec.WCT_End_Date__c=system.today()-2; 
            controller.save();
             
            datetime assignstartdate1=date.Today().adddays(-2);
            datetime assignenddate1=date.Today().adddays(-5);
            controller.assignmentstartdate = assignstartdate1.format('MM/dd/yyyy');
            controller.assignmentenddate = assignenddate1.format('MM/dd/yyyy');
            controller.mobilityRecord.WCT_Date_of_Arrival_in_US__c=date.today()+10;
            controller.save(); 
        
        Document doc= new Document();
            doc.Name='test';
            doc.Body=Blob.valueOf('test');
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        insert doc;
            
            controller.attachmentHelper.docIdList.add(doc.Id);
            controller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'20'));
       
        datetime assignstartdate2=date.Today().adddays(-2);
        controller.assignmentstartdate = assignstartdate2.format('MM/dd/yyyy');
            t.Ownerid = System.Label.GMI_User;
            t.status = 'completed';
        upsert t;
        controller.save(); 
        
            controller.pageError=true;
            controller.pageErrorMessage='error message';
            controller.supportAreaErrorMesssage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
    }
     public static testmethod void m5()
    {
       
        recordtype rt=[select id,Name from recordtype where DeveloperName = 'WCT_Employee'];
        recordtype mobRecType = [select id,name,DeveloperName  from recordtype where DeveloperName = 'Employment_Visa'];
         recordtype mobRecTypeB = [select id,name,DeveloperName from recordtype where DeveloperName = 'Business_Visa'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        String employeeType = 'Employee';
        
   
        
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        mob.WCT_India_Cellphone__c='12345678';
        mob.WCT_Accompanied_Status__c=true;
        mob.WCT_Number_Of_Children__c=1;
        mob.WCT_Accompanying_Dependents__c=2;
        mob.WCT_US_Project_Mngr__c='Testing@mobility.com';
        //mob.WCT_Regional_Dest__c= assign.WCT_Regional_Dest__c ;
        mob.WCT_USI_RCCode__c='Testing';
        mob.WCT_Project_Controller__c = 'test@mobility.com';
        mob.WCT_Mobility_Employee__c = con.id;
        mob.RecordTypeId = mobRecType.Id;
        
        insert mob;
        
        WCT_Assignment__c assign = new WCT_Assignment__c ();
        assign.WCT_Client_Office_Name__c='test';
        assign.WCT_Client_Office_City__c='test';
      //  assign.WCT_US_Project_Coordinator__c='test';
        assign.WCT_Client_Office_State__c='test';
        assign.WCT_Regional_Dest__c = 'sample';
        assign.WCT_Employee__c = con.id;
        assign.WCT_Mobility__c=mob.id;
        insert assign;
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Auto_Close__c=true;
        t.WCT_ToD_Task_Reference__c = 'sample';
        insert t;
        datetime startdate=date.Today().adddays(10);
        datetime assignstartdate=date.Today().adddays(2);
        datetime assignenddate=date.Today().adddays(5);
        
        Test.starttest();
       
        PageReference pageRef = Page.WCT_OnboardingInitiationForm;
        Test.setCurrentPage(pageRef); 
    
        WCT_OnboardingInitiationForm controller=new WCT_OnboardingInitiationForm();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
            controller=new WCT_OnboardingInitiationForm();
        List<Attachment> a= Test_Data_Utility.createAttachmentsForSpecificObject(t);
        insert a;
        
        Document dvar = WCT_UtilTestDataCreation.createDocument();
        List<String> lsvar = new List<String>();
        
        lsvar.add(dvar.id);
        
        GBL_Attachments attachmentHelper = new GBL_Attachments();      
            attachmentHelper.doc=WCT_UtilTestDataCreation.createDocument();
            //attachmentHelper.docIdList = lsvar;
            attachmentHelper.uploadDocument();
            attachmentHelper.uploadRelatedAttachment(mob.id);
            controller.getAttachmentInfo();           
            controller.save();
            controller.listAttachments=a;
            controller.employeeType='employeeType';
            controller.employeeType='Business';
            controller.getMobilityTypeDropDownValues();
            //controller.employeeType='Employee';
            // controller.getMobilityTypeDropDownValues();
            controller.save(); 
            controller.dateOfArrival=startdate.format('MM/dd/yyyy');
            controller.assignmentRec.WCT_Client_Office_Name__c='test';
            controller.assignmentRec.WCT_Client_Office_City__c='test';
            controller.assignmentRec.WCT_Client_Office_State__c='test';
            controller.assignmentstartdate = assignstartdate.format('MM/dd/yyyy');
            controller.assignmentenddate = assignenddate.format('MM/dd/yyyy');
            controller.assignmentRec.WCT_Regional_Dest__c='Testing';
            mob.WCT_Regional_Dest__c = controller.assignmentRec.WCT_Regional_Dest__c;
        
       
            controller.employeeType = 'Employee';
            controller.assignmentRec.WCT_Initiation_Date__c= Date.newInstance(2015,5,23);
            controller.assignmentRec.WCT_End_Date__c=Date.newInstance(2015,5,20);
            controller.assignmentRec.WCT_Initiation_Date__c= system.Today()-1;
            controller.assignmentRec.WCT_End_Date__c= date.Today();      
            controller.doc=WCT_UtilTestDataCreation.createDocument();
            controller.save(); 
            
            datetime assignstartdate1=date.Today().adddays(-2);
            datetime assignenddate1=date.Today().adddays(-5);
            controller.assignmentstartdate = assignstartdate1.format('MM/dd/yyyy');
            controller.assignmentenddate = assignenddate1.format('MM/dd/yyyy');
            controller.mobilityRecord.WCT_Date_of_Arrival_in_US__c=system.today()+10;
            controller.save(); 
        
            controller.mobilityRecord.recordTypeId=mobRecTypeB.Id;
      
        Document doc= new Document();
            doc.Name='test';
            doc.Body=Blob.valueOf('test');
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        insert doc;
            
            controller.attachmentHelper.docIdList.add(doc.Id);
            controller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'20'));
       
        datetime assignstartdate2=date.Today().adddays(-2);
        controller.assignmentstartdate = assignstartdate2.format('MM/dd/yyyy');
        controller.save(); 
       
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
    }
}