@isTest
private class WCT_CancelImmigrationRecordFlag_Test{
    static testMethod void testMyWebSvc(){ 
    
        WCT_Immigration__c rec = new WCT_Immigration__c();
        INSERT rec;
    
        Test.startTest();
        WCT_CancelImmigrationRecordFlag.updateFlags(rec.Id);
        Test.stopTest(); 
    } 
}