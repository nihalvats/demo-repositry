@istest
public class ProjectTrigger_handler_test 
{
   
    
    private static testmethod void insertSenario()
    {
               
        id devRecordTypeId = Schema.SObjectType.project__c.getRecordTypeInfosByName().get('Analytics Pipeline').getRecordTypeId();
        project__c p = new project__c();
        p.Talent_Channel__c ='test';
        p.name = 'test name';
        p.Status__c = 'Not started';
        p.RecordTypeId = devRecordTypeId;
        insert p;
        
      
       List<project__c> projects=[Select Id, Status__c from project__c ];
        
        for(project__c project :projects)
        {
            project.Status__c = 'Completed (Move to Refresher)';
            
        }
         ProjectTrigger_handler.Pipeline(projects);
        
       
        
        
    }
    
    
    
}