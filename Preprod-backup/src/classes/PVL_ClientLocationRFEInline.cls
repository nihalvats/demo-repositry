public class PVL_ClientLocationRFEInline 
{
	
    public List<Client_Location__c> clientLoc{get; set;}
    public WCT_Immigration__c immigration {get;set;}
    public PVL_ClientLocationRFEInline(ApexPages.StandardController stdController) 
    {
        this.immigration = (WCT_Immigration__c)stdController.getRecord();
        immigration= [Select Id, RFE_PVL_lk__r.Project__c, RFE_PVL_lk__r.Project__r.PVL_PL_Engagement_Partner__r.Name,RFE_PVL_lk__r.Project__r.PVL_PL_Engagement_Partner__r.Email from WCT_Immigration__c where id=:immigration.id ];
     	
        if(immigration.RFE_PVL_lk__r.Project__c!=null)
        {
            clientLoc= [Select Id, H1B_City__c, H1B_State__c, H1B_Street__c, Zip_Code__c From Client_Location__c Where Project_Name__c=:immigration.RFE_PVL_lk__r.Project__c ];
        }
        
    }
}