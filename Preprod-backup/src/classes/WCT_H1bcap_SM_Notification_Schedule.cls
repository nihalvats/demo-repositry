global class WCT_H1bcap_SM_Notification_Schedule implements Schedulable {

     global void execute(SchedulableContext sc) {

         

       //Define  batch size.       

       integer BATCH_SIZE = 1; 

      WCT_H1bcap_SM_Notification sndBatch = new WCT_H1bcap_SM_Notification ();
     system.debug('****WCT_H1bcap_SM_Notification : starting batch exection*****************');

     Id batchId = database.executeBatch(sndBatch , BATCH_SIZE);   

  system.debug('**** WCT_H1bcap_SM_Notification : Batch executed batchId: '+batchId);

   }
 }