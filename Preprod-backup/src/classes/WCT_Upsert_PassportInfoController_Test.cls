@isTest
private class WCT_Upsert_PassportInfoController_Test {

    static testMethod void myUnitTest1() {
        //recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        //Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
       // insert con;
        
       // String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');

        PageReference pageRef = Page.Wct_Upsert_Passport;
        Test.setCurrentPage(pageRef); 
        //ApexPages.CurrentPage().getParameters().put('em',encrypt);     
       Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCase','Employee','test@case.com');
       insert con;
       String profile = System.label.Label_for_Employee_Profile_Name;   
       User u = WCT_UtilTestDataCreation.createUser('test@case.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CaseLast','CaseFirst','casecase','case@deloitte.com');   
       insert u;
       System.runAs(u)
        {
        Test.starttest();

        WCT_Upsert_PassportInfoController controller=new WCT_Upsert_PassportInfoController();  
             
        controller.doc=WCT_UtilTestDataCreation.createDocument();
      //  controller.uploadAttachment();
        controller.employeeLastName='Raja';
        controller.employeeMiddleName='';
        //controller.passportNo='12345';
        controller.citizenship='Indian';
        controller.passportCity='Chennai';
        controller.passportCountry='India';
        controller.passportIssuanceDate = '12/01/2014';
        controller.passportExpirationDate = '12/01/2015';        
        controller.save();
        
        Test.stoptest();
    }}    

    
    static testMethod void myUnitTest3() {
        
       PageReference pageRef = Page.Wct_Upsert_Passport;
       Test.setCurrentPage(pageRef); 
        
       Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCase','Employee','test@case.com');
       insert con;
       String profile = System.label.Label_for_Employee_Profile_Name;   
       User u = WCT_UtilTestDataCreation.createUser('test@case.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CaseLast','CaseFirst','casecase','case@deloitte.com');   
       insert u;
       WCT_Passport__c createPass=new WCT_Passport__c(WCT_Employee__c=con.id,WCT_Passport__c='3453',WCT_Passport_First_Name__c='TestF',Passport_Last_Name__c='TestL',WCT_Country_of_Passport__c='INDIA',WCT_Passport_Issuing_City__c='CHENNAI',WCT_Passport_Issuance_Date__c=Date.newInstance(2014,10,20),WCT_Passport_Expiration_Date__c=Date.newInstance(2020,10,20));
       Insert createPass;
       Attachment a=WCT_UtilTestDataCreation.createAttachment(createPass.Id);
        
       System.runAs(u)
        {
        Test.starttest();

        WCT_Upsert_PassportInfoController controller=new WCT_Upsert_PassportInfoController();      
        controller.doc=WCT_UtilTestDataCreation.createDocument();
        
        controller.listAttachments =Test_Data_Utility.createAttachmentsForSpecificObject(createPass);
        
        //controller.employeeId=con.id;
        //controller.uploadAttachment();
        controller.employeeEncryptId='test';
        controller.employeeEmail='test@gmail.com';
        controller.supportAreaErrorMesssage='Testmsg';
        controller.validPage=true;
        controller.isError =true;
        controller.isErrorText = 'Test Message';
        controller.employeeLastName='Raja';
        controller.employeeMiddleName='';
        controller.passportNo='12345';
        controller.citizenship='Indian';
        controller.passportCity='Chennai';
        controller.passportCountry='India';
        controller.passportIssuanceDate = '12/01/2014';
        controller.passportExpirationDate = '12/01/2015';  
        GBL_Attachments attachmentHelper = new GBL_Attachments();      
        attachmentHelper.doc=WCT_UtilTestDataCreation.createDocument();
        attachmentHelper.uploadDocument();
        attachmentHelper.uploadRelatedAttachment(createPass.Id);
        controller.getAttachmentInfo();           
        controller.save();
        PageReference PageRefR = Page.WCT_Upsert_PassportThankYou;
        PageRefR.getParameters().put('ss', '1');
        Test.setCurrentPage(PageRefR);
        WCT_Upsert_PassportInfoController controller1=new WCT_Upsert_PassportInfoController();  
        controller1.passportNo='12434';
        controller1.save();
        controller1.getAttachmentInfo();
        Test.stoptest();
    }   } 
}