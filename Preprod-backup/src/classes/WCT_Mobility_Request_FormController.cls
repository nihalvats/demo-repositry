/********************************************************************************************************************************
Apex class         : <WCT_Mobility_Request_FormController>
Description        : <Controller which allows to Insert Mobility>
Type               :  Controller
Test Class         : <WCT_Mobility_Request_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 26/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/  

public with sharing class WCT_Mobility_Request_FormController extends SitesTodHeaderController{

    /* public variables */
    public WCT_Mobility__c MobilityRecord{get;set;} 

    ///ERROR RELATED VARIABLES 
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}   
    
    /* Upload Related Variables */
     public Document doc {get;set;}
     public List<String> docIdList = new List<string>();
    // public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
     public List<Attachment> listAttachments {get; set;}
     public Map<Id, String> mapAttachmentSize {get; set;} 
     public GBL_Attachments attachmentHelper{get; set;}
     
    //Controller Related Variables
    public String employeeType;
    public Contact Employee{get;set;}
    public Map<String,String> recordTypeMap {get;set;}
    public String selectedDropDownValue {get;set;}
    public String firstworkingday {get;set;}
    public String lastworkingday {get;set;}
    public String travelstartdate{get;set;}
    public String travelenddate{get;set;}
    public String VisaStart {get;set;}
    public String VisaExpiration {get;set;}
    public List<SelectOption> L2withEADOptions{set; get;}
    public String L2withEAD{set; get;}
    public String employeeEmail{set;get;}
    public boolean employeePresent{set;get;}
    public boolean employeePrevious{set;get;}

    public WCT_Mobility_Request_FormController(){
        init();
        attachmentHelper= new GBL_Attachments(); 
        getMobilityTypeDropDownValues();
        getAttachmentInfo();
        if(invalidEmployee)
        {
           return;
        }
    }

/********************************************************************************************
*Method Name         : <init()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Init() Used for loading Mobility, Attachments>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 
    public void init(){
        MobilityRecord= new WCT_Mobility__c();
        Employee=new Contact();
        employeeType = '';
        employeePresent=false;
        employeePrevious=false;
        recordTypeMap = new Map<String,String>();
     
        L2withEADOptions=new List<SelectOption>();
        L2withEADOptions.add(new SelectOption('None','None'));
        L2withEADOptions.add(new SelectOption('Yes','Yes'));
        L2withEADOptions.add(new SelectOption('No','No'));
        L2withEAD='None';
        //Document Related Init
        doc = new Document();
        //UploadedDocumentList = new List<AttachmentsWrapper>();
        listAttachments = new List<Attachment>();
        mapAttachmentSize = new Map<Id, String>();   
        
        }

/********************************************************************************************
*Method Name         : <getMobilityTypeDropDownValues()>
*Return Type         : <List>
*Param’s             : 
*Description         : <GetMobilityTypeDropDownValues() Used for Fetching Mobility Type>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 
    public list<SelectOption> getMobilityTypeDropDownValues(){

        List<SelectOption> options = new List<SelectOption>();

        for(RecordType rt: [SELECT ID, Name FROM RecordType WHERE sObjectType = 'WCT_Mobility__c'AND Name = 'Employment Visa']){

            //Create Record Map
            recordTypeMap.put(rt.ID, rt.Name);

           // if(String.isNotEmpty(employeeType)){
             // if(employeeType == 'Employee'){
                 //   if(rt.Name == 'Employment Visa'){                    
                        options.add(new SelectOption(rt.ID, rt.Name));
                        mobilityRecord.recordTypeId = rt.Id;
                  //  }
              //  }
          //  }
        }
        return options;
    }

/********************************************************************************************
*Method Name         : <getAttachmentInfo()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetAttachmentInfo() Used for Retrieving Attachment Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     
    
    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId , 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId = :MobilityRecord.id
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50 ];    
              
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }

/********************************************************************************************
*Method Name         : <getEmployeeDetails()>
*Return Type         : <Page Reference>
*Param’s             : 
*Description         : <GetEmployeeDetails() Used for Fetching Employee Details from Contact>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/
 
    public pageReference getEmployeeDetails()
    {
    
        try{
            recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
            employeeEmail=employeeEmail.trim();
            if((employeeEmail==null)||(employeeEmail==''))
            {
                pageErrorMessage = 'Please enter a email ID';
                pageError = true;
                employee=new contact();
                employeePresent=false;
                return null;
            }
            else
            {
                Employee=[select id,name from contact where recordtypeid=:rt.id and email =: employeeEmail limit 1];
                employeePresent=true;   
            }
        }
        catch(Exception e)
        {
            WCT_ExceptionUtility.logException('WCT_Mobility_Request_FormController','Get Employee Details',e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            pageErrorMessage = 'Email Id is not associated with any employee';
            pageError = true;
            employee=new contact();
            employeePresent=false;
            return null;
        }
        pageError = false;
        return null;
    }


/********************************************************************************************
*Method Name         : <saveMobilityRecord()>
*Return Type         : <Page Reference>
*Param’s             : 
*Description         : <SaveMobilityRecord() Used to Insert Mobility Information>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/    
  
    public pageReference saveMobilityRecord()
    { 
        try{
        //Check for Validation
        //||(null == VisaExpiration ) || ('' == VisaExpiration ) || (null == VisaStart) || ('' == VisaStart)
      
        if( (null == MobilityRecord.WCT_USI_Report_Mngr__c) || ('' == MobilityRecord.WCT_USI_Report_Mngr__c) ||(null == mobilityRecord.WCT_USI_RCCode__c) || 
            ('' == mobilityRecord.WCT_USI_RCCode__c) ||
            ('' == MobilityRecord.WCT_USI_Resource_Manager__c) ||
            (null == MobilityRecord.WCT_USI_Resource_Manager__c) ||
            ('' == MobilityRecord.WCT_Purpose_of_Travel__c) ||
            (null == MobilityRecord.WCT_Purpose_of_Travel__c)
            ){

                pageErrorMessage = 'Please fill in all the required fields on the form.';
                pageError = true;
                return null;
        }
             system.debug('visa start is'+VisaStart);
            if(String.isNotBlank(VisaStart)){
                MobilityRecord.WCT_Visa_start_date__c= Date.parse(VisaStart);
            }
            if(String.isNotBlank(VisaExpiration )){
                MobilityRecord.WCT_Visa_expiration_date__c= Date.parse(VisaExpiration );
            }
            
            MobilityRecord.WCT_Previous_Employer_Visa__c= employeePrevious;
            
            if(MobilityRecord.WCT_Visa_start_date__c > MobilityRecord.WCT_Visa_expiration_date__c) {
                pageErrorMessage = 'Visa End Date cannot be before the Visa Start Date. Please correct.';
                pageError = true;
                return null;
            }
        
        
            if(('None' != L2withEAD) ||
                (null != firstworkingday ) || ('' != firstworkingday ) ||
                (null != lastworkingday ) || ('' != lastworkingday )){       
                MobilityRecord.WCT_EmployeeTravel__c=L2withEAD;
                if(String.isNotBlank(firstworkingday )){
                    MobilityRecord.WCT_First_Working_Day_in_US__c = Date.parse(firstworkingday );
                }
                if(String.isNotBlank(lastworkingday )){
                    MobilityRecord.WCT_Last_Working_Day_in_US__c = Date.parse(lastworkingday );
                }
                
                if(MobilityRecord.WCT_First_Working_Day_in_US__c > MobilityRecord.WCT_Last_Working_Day_in_US__c) {
                        pageErrorMessage = 'The Last Working Day in the US cannot be before the First Working Day in the US. Please correct.';
                        pageError = true;
                        return null;
                }
                if(MobilityRecord.WCT_Last_Working_Day_in_US__c < system.today() || MobilityRecord.WCT_First_Working_Day_in_US__c < system.today()) {
                        pageErrorMessage = 'The First Working Day in US and Last Working Day in the US has to be a future date. Please correct.';
                        pageError = true;
                        return null;
                }
                
            }
            else
            {
                 pageErrorMessage = 'Please fill in all the required fields on the form.';
                 pageError = true;
                 return null;
            }
       
        //Update Record Type based on the type of Leaves Selected.
        if(selectedDropDownValue != ''){
            MobilityRecord.RecordTypeId = selectedDropDownValue;
        }
        
        //Set Employee Information
        MobilityRecord.WCT_Mobility_Employee__c = Employee.id;
        
        upsert MobilityRecord;
        
         if(attachmentHelper.docIdList.isEmpty()) {
           /* pageErrorMessage = 'Attachment is required to submit the form.';
            pageError = true;
            return null; */

        } 
        else {
              attachmentHelper.uploadRelatedAttachment(MobilityRecord.id);
        }

  
        }
        catch (Exception e) {
        Exception_Log__c errLog = WCT_ExceptionUtility.logException('WCT_Mobility_Request_FormController', 'Mobility Request Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
        
        Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_MRQT_EXP&expCode='+errLog.Name);
        pg.setRedirect(true);
        return pg;
        }
        Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_MQRT_MSG');
        pg.setRedirect(true);
        return pg;
          
    }
   
 
}