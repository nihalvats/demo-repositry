@isTest
public class PVL_Intake_Form_Test 
{
    static testmethod void testIntakePage()
    {
        Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCvm','Employee','test.pvl@deloitte.com');
       insert con;
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
       String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.pvl@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','cvm@deloitte.com');   
      insert u;
        
      System.runAs(u)
        {
             
             String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
             Test.setCurrentPageReference(new PageReference('PVL_Intake_Form')); 
             System.currentPageReference().getParameters().put('em', strEncryptEmail);
            
            PVL_Intake_Form form= new PVL_Intake_Form(null);
            
            form.ClientProject.Are_you_the_PLE_Partner__c ='No';
            form.hideSectionOnChange();
            form.ClientProject.Are_you_the_PLE_Partner__c ='Yes';
            form.hideSectionOnChange();
            form.updatePartnerDetails();
            form.file=Blob.valueOf('Text File');
            form.InputFileName='Test File Name';
            form.selectedClientId='New';
            form.newClientName='Test Company';
            form.SavePvl();
            form.newProject();
            
            form.PVL_Notes=form.PVL_Notes;
            form.selectedValues=form.selectedValues;
            form.searchemail=form.searchemail;
            form.RowIndex=form.RowIndex;
            form.recordType=form.recordType;
            form.requesttype=form.requesttype;
            form.reqtype=form.reqtype;
            form.doc=form.doc;
            form.pageError=form.pageError;
            form.pageErrorMessage=form.pageErrorMessage;
            
            PVL_Intake_Form.getExistingCompanies('Test');
            PVL_Intake_Form.getItemsList('Test');
            
        }
    }
}