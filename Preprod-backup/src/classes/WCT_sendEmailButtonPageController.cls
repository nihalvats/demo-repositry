global class WCT_sendEmailButtonPageController {

   

 
// *********************************************************************************************
// *************** variable declariation********************************************************
    public Document cDoc{get;set;}
    public String ToFieldEmail { get; set; }
    public list<string> ShowAttachmentlist{get;set;} 
    public String RelatedToFieldId { get; set; }
    public String RelatedToField { get; set; }
    public list < SelectOption > RelatedToSelectOptionValues { 
    get
    {
     list<SelectOption> tempOptions= new list<SelectOption>();
     tempOptions.add(new selectOption('case','Case'));
     return  tempOptions;       
    } 
    set; }

    public String SelectedRetaedTovalue { get; set; }
    public String Case_TR_EM_RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Label.Case_TR_EM_RecordTypeId).getRecordTypeId();
    public String Case_ELE_ES_RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Label.Case_ELE_ES_RecordTypeId).getRecordTypeId();
    public list <string> ccid   = new list<String>(); 
    public list<string> bccId = new list<String>();  
    public list<String>    toEmailAddress= new list<string>();     
    User usr = [Select Id, Name, Profile.Name from User where Id = :UserInfo.getUserId()];  
   global list<string> DocIds=new list<string>();
   public string templateId='';
   public String sEmailTemplateID {get;set;}
   public Boolean ShowselectTemplatepopup{get;set;}
   global Boolean ShowAttchPopUp{get;set;}
   global String fileName {get; set;}
   global Blob attachedFile {get; set;} 
   public List<EmailTemplate> Templatelist { get; set; }
   public list<selectOption> Folders { 
    get
    {
             list<selectOption> option= new list<selectoption>();
             option.add(new selectOption('','--Select--'));
             option.add(new SelectOption(UserInfo.getOrganizationId(), 'Unfiled Public Email Templates'));
                    for(folder f:[SELECT Id,Name,Type FROM Folder where Type='Email'])
                    {
                            if(f.name !='CIC Reports')
                            option.add(new selectOption(f.id,f.name));
                    }
             return option;
    }
     set; 
    }
    public String folderid { get; set; }
    public String BodyHiddenContentvalue { get; set; }
    public String BodyContent { get; set; }
    public String subjectLine { get; set; }
    public String BCcIdFieldValue { get; set; }
    public String BCcFieldValue { get; set; }
    public String CcIdFieldValue { get; set; }
    public String CcFieldValue { get; set; }
    public String AdditionalToFieldValue { get; set; }
    public String ShowAdditionalFieldValue { get; set; }
    public String ToFieldId { get; set; }
    public String ToField { get; set; }
    public string SelectedFromId{get;set;}
    public String caseid;

    //Added
    public list < SelectOption > fromAddressValues{ 
        get {
            list < SelectOption > tempOption = new list < SelectOption > ();
            //list <String> LstStatusValues = new list <String> ();
            //Map<String, Email_to_Case_Service__c> mapValues = Email_to_Case_Service__c.getAll();
            list<OrgWideEmailAddress> owea = [SELECT Address,DisplayName,Id FROM OrgWideEmailAddress];
            //LstStatusValues.addAll(mapValues.keySet()) ;
            
            if((usr.Profile.Name == Label.CIC_agent_Profile)||(usr.Profile.Name == Label.CIC_Manager_Profile))
            {
               tempOption.add(new selectOption(Label.TalentCICInbox,Label.TalentCICInbox));
            }
            else
            {
                tempOption.add(new selectOption(UserInfo.getUserEmail(),UserInfo.getUserEmail()));
            }
            for(OrgWideEmailAddress eslist :owea ){ //LstStatusValues) {
                //Email_to_Case_Service__c  es = mapValues.get(eslist);
                //tempOption.add(new selectOption(es.Email_Address__c,es.Routing_Name__c+'<'+es.Email_Address__c+'>'));
                tempOption.add(new selectOption(eslist.Address,eslist.DisplayName+'<'+eslist.Address+'>'));
               }

            return tempOption;
        }
        set;
    }
    
    
 //****************************************************************************************************************
 //********************** send email method to send email**********************************************************   
    public PageReference SendEmail() {
         
         Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
         list<String> AdditionalTo= new list<String>();
   
   
   //**********************  Adding custom error message if To field is blank. ************************************      
         if(ToField.length()==0)
         { 
          ToFieldId='';
         }
         if(ToFieldId.length()==0)
         {
          ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendEmail_To_fieldErrorMessage);
          ApexPages.addMessage(myMsg); 
          return null;
         }
    //**********************  Setting TO Address . ************************************
         set<String> TempToEmails= new  set<string>();
         if(ShowAdditionalFieldValue.length()>0)
         {
         AdditionalTo= ShowAdditionalFieldValue.split(',');
          if(AdditionalTo.size()>0){
              TempToEmails.addAll(AdditionalTo);
             }
             toEmailAddress.AddAll(TempToEmails);
             message.setToAddresses(toEmailAddress);
         }
         
        
    //**********************  Setting BCC Address . ************************************
         
         if(BCcFieldValue.length()>0)
             {
                list<String> BCCField=BCcFieldValue.split(',');
                if(BCCField.size()>0){  
                        message.setBccAddresses(BCCField);
                  }
              }
   //**********************  Setting CC Address . ************************************
               
         if(CcFieldValue.length()>0)
             {
               list<String> CCField=CcFieldValue.split(',');
               if(CCField.size()>0){
                       message.setCcAddresses(CCField);
                   }
               }
               
    //***************** Setting From Address ************************ //Added
   
   
   
        if(!fromAddressValues.isEmpty()){
           
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: SelectedFromId];
            
            if ( owea.size() > 0 ) {
                message.setOrgWideEmailAddressId(owea.get(0).Id);
                         
            }else
            {
             message.setSenderDisplayName(SelectedFromId);
            }
  
        }
        
     
   //**********************  Adding Custom message if there is no subject with the email. ******************
         
         if((templateId.length()==0)&&(subjectLine.length()==0))
         {
              ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendEmail_subject_fieldErrorMessage);
              ApexPages.addMessage(myMsg); 
              return null;
         }
         
   //**********************  Setting Template,body if user selects template. ************************************
        if(templateId.length()>0){
            
            message.setTemplateId(templateId);
            message.setWhatId(RelatedToFieldId);
            message.setTargetObjectId(ToFieldId); 
                
          }
          else
          {
  //**********************  Setting subject, body if user don't select a template. ************************************
           message.setTargetObjectId(ToFieldId);       
           message.setSubject(subjectLine); 
           message.setHtmlBody(BodyContent);  //Added 
           message.setWhatId(RelatedToFieldId);
              if(BodyContent.length()==0)
             {
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendEmail_body_fieldErrorMessage);
                  ApexPages.addMessage(myMsg); 
                  return null;
              }
           }
         
 //**********************  adding attachments to the email ************************************
      
        if(DocIds.size()>0)
        {
           message.setDocumentAttachments(DocIds);
        }
     try{
     
 //**********************  Sending email ************************************
        System.debug('*********TO ADDRESS************'+toEmailAddress);      //Added
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {message});  
      
        
         }catch(Exception e )
         {
          if(e.getMessage().contains('INVALID_EMAIL_ADDRESS'))
          {
                  ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.SendEmail_Invalid_Email_Id);
                  ApexPages.addMessage(myMsg); 
                  return null;
          }else{
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
           return null;
           }           
         }  
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Support Request submitted successfully.')); 
        list<Document> DocToDelete=[select id from Document where id In : DocIds];
        delete DocToDelete;  
        pagereference PageRef = new pagereference('/'+caseid);
        PageRef.setRedirect(true);
        return PageRef;
    
    }
//**********************  Cancel method  ************************************
  

    public PageReference cancel() {
        pagereference PageRef = new pagereference('/'+caseid);
        PageRef.setRedirect(true);
        return PageRef;
    }

//**********************  Method to close Attachment Popup  ************************************
 
    public PageReference CloseAttachmentPopup() {
        ShowAttchPopUp=false;
        return null;
    }
//**********************  Method to open Attachment Popup  ************************************


    public PageReference OpenAttachmentPopup() {
        ShowAttchPopUp=true;
        return null;
    }
//**********************  Method to attach a file/ createing document  ************************************

    public PageReference save() {
      
    if(cDoc.Name!=null){
        cDoc.FolderId=Label.Attachment_Zip_Document_Folder_Id;
        insert cDoc;
        ShowAttachmentlist.add(cDoc.Name);
        DocIds.add(cDoc.id);
        cDoc= new Document();
     }
    return null;
    }


//**********************  Method to attach a file/ createing document  ************************************
  public PageReference OpenSelectTemplatePopup() {
        ShowselectTemplatepopup=true;
        return null;
    }
 //**********************  Setting selected template on VF page  ************************************
  
   public PageReference SelectTemplate() {
         for(EmailTemplate temp : [SELECT Id,Name,Body,FolderId,HtmlValue,TemplateType FROM EmailTemplate where id=: sEmailTemplateID ]){
               if(temp.TemplateType=='HTML'){ BodyContent=temp.HtmlValue;
                BodyContent=BodyContent.replaceAll(']]>','');
                subjectLine=temp.Name;
                templateId=temp.Id;
                }else if(temp.TemplateType=='Text')
                {
                BodyContent=temp.Body;
                subjectLine=temp.Name;
                templateId=temp.Id;
                }
                           
                }
                ShowselectTemplatepopup=false;
                return null;
    }


//********************** Searching templates  ************************************
 

    public void Searchtemplfiles() {
        Templatelist = new list<EmailTemplate>();
                if(folderid!= null && folderid!='')
                    {
                       for(EmailTemplate et:[ SELECT Id,Name,Body,FolderId,Description,HtmlValue,TemplateType FROM EmailTemplate where FolderId=:folderid])
                       {
                         Templatelist.add(et); 
                        } 
                
                    }
          // return null;
        }


  

 //********************** Default constructor  ************************************
   
 public WCT_sendEmailButtonPageController()
 { 
    BCcFieldValue='';
    subjectLine='';
    CcFieldValue='';
    ShowAdditionalFieldValue='';
    ToFieldId='';
    ToField='';
    ToFieldEmail='';
    templateId='';
    cDoc= new Document();
    ShowselectTemplatepopup= false;
    ShowAttchPopUp=false;
    caseid=ApexPages.currentPage().getParameters().get('id') ; 
    list<Case> cCurrentCase=[select RecordTypeId, CaseNumber,ContactId, Id from case where id =: caseid ];
    
    
    String ISP =ApexPages.currentPage().getParameters().get('ISP');
        
        if(ISP=='Yes')
        {
            list<Interested_Party__c> lst_interdtedParty=[select id, WCT_Contact__r.Email from Interested_Party__c where WCT_Case__c =: caseid];
            //system.debug('>>>> Interested_Party__c  >>>>>>>>>>>>>>>>>>>>'+lst_interdtedParty[0].WCT_Contact__r.Email);
            for(Interested_Party__c loop_intPart : lst_interdtedParty)
            { 
                if(ShowAdditionalFieldValue=='')
                    {
                      ShowAdditionalFieldValue=loop_intPart .WCT_Contact__r.Email;
                    }else
                    {
                      ShowAdditionalFieldValue=ShowAdditionalFieldValue+','+loop_intPart .WCT_Contact__r.Email;
                    }
            
            }
             
        }
        
    
    if((cCurrentCase[0].RecordTypeId!=Case_TR_EM_RecordTypeId)&&(cCurrentCase[0].RecordTypeId!=Case_ELE_ES_RecordTypeId))
    {
      if(cCurrentCase[0].ContactId!=null){
      ToField= getContactName(cCurrentCase[0].ContactId, 'Name');
      ToFieldId=cCurrentCase[0].ContactId;
      ToFieldEmail= getContactName(cCurrentCase[0].ContactId, 'Email');
        }
    }else
    {
      ToField= '';
      ToFieldId='';
      ToFieldEmail='';
    }
    ShowAttachmentlist= new list<String>();
    SelectedRetaedTovalue='case';
    RelatedToField=cCurrentCase[0].CaseNumber;
    RelatedToFieldId=cCurrentCase[0].Id;
 }
 
 //********************** Method to get the contact name  ************************************
 
 public string getContactName(String contactId, String RetParameter)
     {
      list<Contact> contactTowork=[select name, id,Email from contact where id =:contactId];
      if(RetParameter=='Name'){
          return contactTowork[0].Name;
      }else
      {
       return contactTowork[0].Email;
      }
     }
}