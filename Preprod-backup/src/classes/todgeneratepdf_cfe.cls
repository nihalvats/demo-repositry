global class todgeneratepdf_cfe {

public Case_form_Extn__c ocaseformextn{get;set;} 
public Case_form_Extn__c caseformextnrec {get;set;} 
public String Sub_Category {get;set;}
public String id {get;set;}
   
    public todgeneratepdf_cfe(ApexPages.StandardController stdController)
    {

        this.ocaseformextn= (Case_form_Extn__c)stdController.getRecord();
        System.debug('#####'+ocaseformextn);
        caseformextnrec = [Select ID,Sub_Category_1__c,Sub_Category_2__c,Name_of_Entity__c,Location__c,Deloitte_Federal_Practice__c,Entity_Website_link__c,If_personal_work_defined_details_needed__c,Entity_description_and_type_of_services__c,Details_of_entity_owning_20_and_more__c,Activity_details_related_to_work__c,Mission_of_entity_if_NP_organization__c,Is_this_a_client__c,Is_this_an_SEC_registrant__c,Is_this_a_Restricted_Entity__c,Names_of_known_affiliates_of_the_entity__c,What_role_will_you_be_accepting__c,Is_there_a_Marketplace_Business_Relation__c,Is_this_an_elected_position__c,Is_this_a_paid_position__c,Will_you_have_any_authority__c,Transactions_with_restricted_entities__c,Roles_Responsibilities_you_approve__c,Do_you_own_intellectual_property_rights__c,What_is_the_time_commitment__c,Business_Approver_Name__c,Business_Approver_Title__c,OE_Candidate_s_name__c,OE_Candidate_s_personal_email__c,OE_Candidate_s_Level__c,OE_Candidate_s_Target_Start_Date__c,OE_Has_Business_Leader_approval__c,OE_Director_s_Only_Has_an_Independence__c,ELE_NP_GEN_Will_one_of_the_Rel_Per_influ__c,ELEC_NP_CLR_FSS__c,ELE_NP_GEN_Will_one_of_the_Related_Perso__c,ELE_NP_GEN_Have_you_obtained_Regional_Fu__c,ELE_NP_GEN_Do_the_individuals_in_questi__c,ELE_NP_GEN_How_did_the_new_hire_candidat__c,ELEC_NP_CLR_Industry_and_or_Market_Offer__c,ELEC_NP_CLR_Service_Line__c,ELEC_NP_CLR_Service_Area__c,ELEC_NP_Name_of_Close_Relative__c,ELEC_NP_CLR_Office__c ,ELEC_NP_CLR_Level__c,ELEC_NP_Relationship_to_Close_Relative__c,ELE_NP_Name_of_New_Hire__c,ELEC_NP_Level__c,ELEC_NP_Office__c,ELEC_NP_FSS__c,ELEC_NP_Service_Area__c,ELEC_NP_Service_Line__c,ELEC_NP_Industry_and_or_Market_Offering__c ,(SELECT ID,NAME,Sub_Category_1__c,Name_of_Entity__c,Location__c,Entity_Website_link__c,If_personal_work_defined_details_needed__c,Entity_description_and_type_of_services__c,Details_of_entity_owning_20_and_more__c,Activity_details_related_to_work__c,Names_of_known_affiliates_of_the_entity__c,Mission_of_entity_if_NP_organization__c
                     ,Is_there_a_Marketplace_Business_Relation__c,Is_this_a_client__c,Is_this_an_elected_position__c,Is_this_an_SEC_registrant__c,Is_this_a_paid_position__c,Is_this_a_Restricted_Entity__c,ELE_CLQR_Is_this_your_first_vehicle__c,What_role_will_you_be_accepting__c,Will_you_have_any_authority__c,Transactions_with_restricted_entities__c,Roles_Responsibilities_you_approve__c,Do_you_own_intellectual_property_rights__c,What_is_the_time_commitment__c,Business_Approver_Title__c,Date_Approved__c,Business_Approver_Name__c from Case_Form_Extension__r Limit 1)
                   FROM Case_form_Extn__c where Id=:this.ocaseformextn.id];
                   
        Sub_Category = caseformextnrec.Sub_Category_1__c;  
        id = caseformextnrec.ID;
                 
        
    }    
}