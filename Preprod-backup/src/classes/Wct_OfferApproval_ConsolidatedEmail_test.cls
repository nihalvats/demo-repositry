@isTest
public class Wct_OfferApproval_ConsolidatedEmail_test{
    static Id employeeRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
   
    public static testMethod void test() {
    
    Profile prof = [select id from profile where name='system Administrator'];
     User usr = new User(alias = 'usr', email='us.name@vmail.com',
                emailencodingkey='UTF-8', lastname='lstname',
                timezonesidkey='America/Los_Angeles',
                languagelocalekey='en_US',
                localesidkey='en_US', profileid = prof.Id,
                username='testuser128@testorg.com');
                insert usr;
    
       Account acc = new Account();
       acc.Name='Test';
       acc.phone='123456790';
       Insert acc;
       system.debug('acc values ----'+acc);
       
       Contact con = new Contact ();
       con.AccountId= acc.Id;
       con.LastName='tec';
       con.Email='user@gmail.com';
       con.WCT_User_Group__c = 'United States';
       Insert con;
      
       List<WCT_Offer__c> off=[select id from WCT_Offer__c];
       System.assert(off.size()==0);
    
        WCT_Offer__c  objCusWct= new WCT_Offer__c();
        objCusWct.WCT_status__c = 'Offer to Be Extended';
        objCusWct.WCT_Candidate__c = con.Id;
        objCusWct.WCT_Recruiter__c = con.id;
        objCusWct.OwnerId = usr.id;
        objCusWct.WCT_Team_Mailbox__c  = 'user@gmail.com';
        objCusWct.WCT_Recruiter_Email__c  = 'user12@gmail.com';
        objCusWct.WCT_Basic__c = '200';
        objCusWct.WCT_Annual_LTA__c='200';
        insert objCusWct;
        
        system.debug('objCusWct'+objCusWct);
        
        Attachment attach=WCT_UtilTestDataCreation.createAttachment(objCusWct.id);
        insert attach;
        
        objCusWct.WCT_status__c = 'Draft in Progress';
        update objCusWct;
        
        objCusWct.WCT_status__c = 'Ready for Review';
        objCusWct.WCT_Basic__c = '240';
        update objCusWct;
        
        
        datetime dt5 = system.now();
        objCusWct.WCT_Offer_RFR_Time__c = dt5.addDays(-2);
        update objCusWct;
       
        Test.StartTest();
        Wct_OfferApproval_ConsolidatedEmail objBatch = new Wct_OfferApproval_ConsolidatedEmail();
        ID batchprocessid = Database.executeBatch(objBatch);
        Test.StopTest();
        
    }
}