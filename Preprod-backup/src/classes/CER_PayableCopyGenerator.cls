global class CER_PayableCopyGenerator
{
     WebService static string generatePayables(String id)
        {
            
            PageReference pageRef= Page.CER_PayablesCopy;
            pageRef.getParameters().put('id',id);
            
            Attachment attachment= new Attachment();
            attachment.parentId=id;
            attachment.Body=pageRef.getContentAsPDF();
            attachment.Name='Payable Copy.pdf';
            attachment.ContentType='application/pdf';
            
            insert attachment;
            return null;
        }
}