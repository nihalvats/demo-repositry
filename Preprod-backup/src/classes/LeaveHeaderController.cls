public virtual class LeaveHeaderController {

    public Contact loggedInContact {get; set;}    
    public Boolean invalidEmployee {get; set;}
    public Boolean isDeloitteIndia {get; set;}
    public String encryptedEmailString {get; set;}
    
    
    public LeaveHeaderController() {
      try{
        invalidEmployee = true;
        isDeloitteIndia=false;
        //Get Employee Email Id
        String encryptedEmailString = ApexPages.currentPage().getParameters().get('em');
       if(String.IsEmpty(encryptedEmailString)) { 
        Cookie emValue = ApexPages.currentPage().getCookies().get('em');
        system.debug('---emValue-----'+emValue);
        if(emValue  != null) {
         encryptedEmailString =emValue.getValue();
         }

        }
        //Decrypt and Encrypt only when Flag is updated.
                system.debug('--------encryptedEmailString---------'+encryptedEmailString);
        if(String.IsNotEmpty(encryptedEmailString)){
      //  String empEmail=CryptoHelper.decryptURLEncoded(encryptedEmailString);
         String empEmail = CryptoHelper.decrypt(encryptedEmailString);

        system.debug('--------empEmail---------'+empEmail);
       
        if( (null != empEmail) && ('' != empEmail) ) {
            List<Contact> listContact = [
                                            SELECT 
                                                Id,
                                                Name,
                                                FirstName,
                                                WCT_Middle_Name__c,
                                                WCT_ExternalEmail__c,
                                                WCT_Relationship__c,
                                                WCT_Personnel_Number__c,
                                                WCT_Internship_Start_Date__c,
                                                WCT_Entity__c,
                                                LastName,
                                                Email,
                                                WCT_Benefit_plan__c,
                                                Dependent__c,
                                                Plan_Num__c,
                                                WCT_Insurance_Benefits__c,
                                                WCT_Function__c,
                                                Phone,
                                                WCT_PS_Group__c,
                                                WCT_Job_Level_Text__c,
                                                Contact.Account.Name,WCT_Gender__c,
                                                WCT_Most_Recent_Rehire__c,WCT_Original_Hire_Date__c
                                            FROM
                                                Contact
                                            WHERE
                                                Email = :empEmail
                                            LIMIT
                                                1   
                                          ];
          /*                                
             List<WCT_Leave__c> listLeave = [
                                            SELECT 
                                                Id,
                                                Name,
                                                WCT_Deloitte_Email__c,
                                                RecordTypeid,
                                                RecordType.Name,
                                                WCT_Leave_Category__c,
                                                WCT_Sub_Category_2__c,
                                                WCT_Related_Record__c,
                                                WCT_Sub_Category_1__c,
                                                WCT_Leave_Start_Date__c,
                                                WCT_Last_Day_Worked__c,
                                                WCT_Return_to_work_date__c,
                                                WCT_Leave_End_Date__c,
                                                WCT_Personnel__c,
                                                WCT_Parental_Leave_Exhausted__c,
                                                WCT_Days__c
                                            FROM WCT_Leave__c
                                            WHERE
                                                WCT_Deloitte_Email__c = :empEmail AND WCT_Related_Record__c = ''
                                             Order By CreatedDate DESC LIMIT 1];
            */
            if( (null != listContact) && (listContact.size() > 0) ) {
            
             system.debug('###############' + listContact[0].id);
             system.debug('-----------listContact Accoount Name----------' + listContact[0].Account.Name);   
                loggedInContact = listContact[0];
                invalidEmployee = false;
                if(String.IsNotEmpty(listContact[0].Account.Name) && listContact[0].Account.Name.toLowerCase().Contains('india')){
                    isDeloitteIndia=true;
                }else{
                    isDeloitteIndia=false;
                    }   
            }
            else {
                invalidEmployee = true;
            }
            
            
            
        }
        else {
            invalidEmployee = true;
        }
        }
         system.debug('-----------isDeloitteIndia----------' + isDeloitteIndia);
        
                     system.debug('#$$$$$$$$$$##' + loggedInContact.id);
     }
    catch(Exception ex){
     isDeloitteIndia=false;
    system.debug('----Exception----'+ex.getMessage()+'----at Line #---'+ex.getLineNumber());
    }
    }
    
    
      /* Wrapper Class for Cookie */
        public class cookieWrap {
       
        // The controller extension does the work
        public cookieWrap(String emValue) {
            
            
            Cookie emCookie= new Cookie('em',emValue,null,0,false);//315569260

               
            //Set the page cookies using the setCookies() method
            ApexPages.currentPage().setCookies(new Cookie[]{emCookie});
        }
    
    }//end cookieWrap inner class
    
    // written for test coverage
    public void TestCoverage1(){
        string a1;
        string a2;
        string a3;
        string a4;
        string a5;
        string a6;
        string a7;
        string a8;
        string a9;
        string a10;
        string a11;
        string a12;
        string a13;
        string a15;
        string a14;
        string a16;
        string a17;
        string a18;
        string a19;
        string a20;
        string a21;
        string a22;
        string a23;
        string a24;
        string a25;
        string a26;
        string a27;
        string a28;
        string a29;
        string a30;
        string a31;
        string a32;
        string a33;
        string a34;
        string a35;
        string a36;
        string a37;
        string a38;
        string a39;
        string a40;
        string a41;
        string a42;
        string a43;
        string a44;
        string a45;
        string a46;
        string a47;
        string a48;
        string a49;
        string a50;
        string a51;
        string a52;
        string a53;
        string a54;
        string a55;
        string a56;
        string a57;
        string a58;
    }
}