public with sharing class ProjectsByTimeLineController{
    /*
     Apex Class:  ProjectsByTimeLineController
     Purpose: This class is used to prepare view of Projects By Time line with Priority
     Created On:  9th July,2015.
     Created by:  Balu Devarapu.(Deloitte India)
    */
    List<string> lstMonths{get;set;}
    public List<ChartData> objChartData{get;set;}
    public List<SelectOption> PriorityOptions{get;set;}
    public string SelectedPriority{get;set;}
    List<ChartData> objChartDataClone = new List<ChartData>();
    Date MinDate;
    Date MaxDate;
    public string SearchLead='';
    public string Priority='';
    boolean IsFilterAction=false;
    public List<SelectOption> PCatOptions{get;set;}
    public string SelectedPCat{get;set;}
    public List<SelectOption> PSOptions{get;set;}
    public string SelectedPS{get;set;}
    public Date StartDate{get;set;}
    public Date EndDate{get;set;}
    public project__c prjdte{get;set;}
    Public string selectedLoe {get;set;}
    Public string sellevelofeffort {get;set;}
    
    
    
    /* Constructor */
    public ProjectsByTimeLineController(){
        
        prjdte=new project__c();
        SearchLead='';
        selectedLoe ='Level of effort – CPI';
        Priority='';
        IsFilterAction=false;
        StartDate=null;
        EndDate=null;
        sellevelofeffort = null;
        Period objPeriod=[SELECT StartDate,EndDate FROM Period WHERE Type = 'Year' AND StartDate <= TODAY AND EndDate >= TODAY];
        if(objPeriod!=null){
        StartDate=objPeriod.StartDate;
        EndDate=objPeriod.EndDate;
        }
    /* Priority Picklist */
    Schema.DescribeFieldResult objlstTC = Project__c.Priority__c.getDescribe();
    SelectedPriority='';
    List<Schema.PicklistEntry> lstPP = objlstTC.getPicklistValues();
    PriorityOptions= new List<SelectOption>();
    PriorityOptions.add(new SelectOption('-None-','-None-'));
    for(Schema.picklistEntry f:lstPP)   
    {   
        PriorityOptions.add(new SelectOption(f.getLabel(),f.getLabel()));    
    }
    
    /* Project Category Picklist */
    PCatOptions= new List<SelectOption>();
    PCatOptions.add(new SelectOption('-None-','-None-'));
    Schema.DescribeFieldResult objPCatOptions = Project__c.Project_Category__c.getDescribe();
    List<Schema.PicklistEntry> lstPCat = objPCatOptions.getPicklistValues();
        for(Schema.picklistEntry f:lstPCat)   
        {   
            PCatOptions.add(new SelectOption(f.getLabel(),f.getLabel()));    
        }
    SelectedPCat=null;
    
    /* Project Status Picklist */
     
    PSOptions = new List<SelectOption>();
    Schema.DescribeFieldResult objlstPS = Project__c.Status__c.getDescribe();
    List<Schema.PicklistEntry> lstPS = objlstPS.getPicklistValues();
        for(Schema.picklistEntry f:lstPS)   
        {   
            PSOptions.add(new SelectOption(f.getLabel(),f.getLabel()));    
        }
    SelectedPS='In Progress';
   
    
    ProcessData();
    }
 
    /*
        Method:ProcessData
        Description: Data view Preparation
    */
    public void ProcessData(){
    try{
    
    MinDate=null;
    MaxDate=null;
    lstMonths=new List<string>();
    objChartData = new List<ChartData>();
    List<Project__c> lstProj=new List<Project__c>();
    String SOQLUS='select Name,Project_Start_Date__c,Project_Planned_End_Date__c,Project_Lead_US__r.Name,Project_Lead_USI__r.Name,Priority__c,Level_of_effort_Technology__c,Level_of_effort_Operations__c,Level_of_effort_CPI__c,Level_of_effort_GMC__c,Level_of_effort_KM__c,GMC_Project_Lead__r.Name,KM_Project_Lead__r.Name,Ops_Project_Lead__r.Name,R_T_Project_Lead__r.Name from Project__c where Project_Start_Date__c!=null and Project_Planned_End_Date__c!=null ';
    SOQLUS+=' and Status__c= \''+SelectedPS+'\'';
    String SOQLUSFilter='';
   
    if(string.IsNotEmpty(SelectedPCat)){
    SOQLUSFilter+=' and Project_Category__c=\''+SelectedPCat+'\'';
    }
    if(string.IsNotEmpty(Priority)){
    SOQLUSFilter+=' and Priority__c=\''+Priority+'\'';
    }
   
    if(string.IsNotEmpty(SearchLead)){
    SOQLUSFilter+=' and (Project_Lead_US__r.Name like \'%'+SearchLead+'%\' or Project_Lead_USI__r.Name like \'%'+SearchLead+'%\') ';
    }
    
    system.debug('%%%%%%%%%%% ' + String.ValueOf(StartDate));
    // if(StartDate!=null && EndDate!=null){
    if(StartDate!=null){
    SOQLUSFilter+=' and Project_Start_Date__c>='+String.ValueOf(StartDate).replace(' 00:00:00','');
    }
 
    if(EndDate!=null){ //>:StartDate
    SOQLUSFilter+=' and Project_Planned_End_Date__c<='+String.ValueOf(EndDate).replace(' 00:00:00','');
    }
   
    SOQLUS+=SOQLUSFilter+' order by Name';
    system.debug('-----SOQLUS----'+SOQLUS);
    lstProj=database.Query(SOQLUS);
  
    
    system.debug('----lstProj-----'+lstProj);
    for(Project__c objProj: lstProj)
    {
        if(MinDate==null)
        MinDate=objProj.Project_Start_Date__c;
        if(MaxDate==null)
        MaxDate=objProj.Project_Planned_End_Date__c;
       
        if(objProj.Project_Start_Date__c<MinDate)
       MinDate=objProj.Project_Start_Date__c;
     
        if(objProj.Project_Planned_End_Date__c>MaxDate)
        MaxDate=objProj.Project_Planned_End_Date__c;
    }
 
    lstMonths.AddAll(setMonthSpan(MinDate,MaxDate));
    system.debug('----lstMonths 1-----'+lstMonths);
    set<string> lstProjDuration;
    List<string> lstProjTimeSpan;
    string USName='';
    string USIName='';
   
 
 
 
    set<string> setPP=new set<string>();
       
    for(Project__c objProj:lstProj){
    
        if(objProj.Priority__c=='Critical'){
        
        string levelofeffort = objProj.Level_of_effort_CPI__c;
        string leadname = '';
        string projectlead = '';
        if(sellevelofeffort == 'Level of effort – CPI'){levelofeffort = objProj.Level_of_effort_CPI__c;leadname='';projectlead =''; }
        else if(sellevelofeffort  == 'Level of effort – GMC'){levelofeffort = objProj.Level_of_effort_GMC__c;leadname = 'GMC Project Lead';projectlead = objProj.GMC_Project_Lead__r.Name;}
        else if(sellevelofeffort  =='Level of effort – KM'){levelofeffort = objProj.Level_of_effort_KM__c;leadname = 'KM Project Lead';projectlead = objProj.KM_Project_Lead__r.Name;}
        else if(sellevelofeffort =='Level of effort – Operations'){levelofeffort = objProj.Level_of_effort_Operations__c;leadname = 'Ops Project Lead';projectlead = objProj.Ops_Project_Lead__r.Name;}
        else if(sellevelofeffort =='Level of effort- Technology'){levelofeffort = objProj.Level_of_effort_Technology__c;leadname = 'R&T Project Lead';projectlead = objProj.R_T_Project_Lead__r.Name;}
        else{}

        if(!setPP.contains(objProj.Priority__c)){
        setPP.add(objProj.Priority__c);
        objChartData.add(new ChartData('Projects - Critical',true,'US Lead','USI Lead',leadname,'Critical', lstMonths,''));
        }
        lstProjDuration = new set<string>();   
        lstProjDuration=setMonthSpan(objProj.Project_Start_Date__c,objProj.Project_Planned_End_Date__c);
        lstProjTimeSpan=new List<string>();
        system.debug('---objProj.Name-----'+objProj.Name);
        system.debug('---lstMonths-----'+lstMonths);
        for(string strDur:lstMonths){
            if(lstProjDuration.Contains(strDur))
            lstProjTimeSpan.add('yes');
            else
            lstProjTimeSpan.add(' ');
        }
        system.debug('---lstProjTimeSpan-----'+lstProjTimeSpan);
        USName=objProj.Project_Lead_US__r.Name;
        if(USName!=null && USName.Contains('('))
        USName=USName.substring(0,objProj.Project_Lead_US__r.Name.indexOf('('));
   
        USIName=objProj.Project_Lead_USI__r.Name;
        if(USIName!=null && USIName.Contains('('))
        USIName=USIName.substring(0,objProj.Project_Lead_USI__r.Name.indexOf('('));
        
       if(projectlead!=null && projectlead.Contains('(') )
               projectlead=projectlead.substring(0,projectlead.indexOf('('));

        
        
        
        objChartData.add(new ChartData(objProj.Name,false,USName,USIName,projectLead,objProj.Priority__c, lstProjTimeSpan,levelofeffort ));   
        }
    }
   
    for(Project__c objProj:lstProj){
       
        if(objProj.Priority__c=='High'){
        
        string levelofeffort = objProj.Level_of_effort_CPI__c;
        string leadname = '';
        string projectlead = '';
        if(sellevelofeffort == 'Level of effort – CPI'){levelofeffort = objProj.Level_of_effort_CPI__c;leadname='';projectlead ='';  }
        else if(sellevelofeffort  == 'Level of effort – GMC'){levelofeffort = objProj.Level_of_effort_GMC__c;leadname = 'GMC Project Lead';projectlead = objProj.GMC_Project_Lead__r.Name;}
        else if(sellevelofeffort  =='Level of effort – KM'){levelofeffort = objProj.Level_of_effort_KM__c;leadname = 'KM Project Lead';projectlead = objProj.KM_Project_Lead__r.Name;}
        else if(sellevelofeffort =='Level of effort – Operations'){levelofeffort = objProj.Level_of_effort_Operations__c;leadname = 'Ops Project Lead';projectlead = objProj.Ops_Project_Lead__r.Name;}
        else if(sellevelofeffort =='Level of effort- Technology'){levelofeffort = objProj.Level_of_effort_Technology__c;leadname = 'R&T Project Lead';projectlead = objProj.R_T_Project_Lead__r.Name;}
        else{}
        
        if(!setPP.contains(objProj.Priority__c)){
        setPP.add(objProj.Priority__c);
        objChartData.add(new ChartData('Projects - High',true,'CPI Project Lead (US)','CPI Project Lead (USI)',leadname,'High', lstMonths,''));
        }  
        lstProjDuration = new set<string>();   
        lstProjDuration=setMonthSpan(objProj.Project_Start_Date__c,objProj.Project_Planned_End_Date__c);
        system.debug('---lstProjDuration-----'+lstProjDuration);
        lstProjTimeSpan=new List<string>();
        for(string strDur:lstMonths){
            if(lstProjDuration.Contains(strDur))
            lstProjTimeSpan.add('yes');
            else
            lstProjTimeSpan.add(' ');
        }
                system.debug('---lstProjTimeSpan-----'+lstProjTimeSpan);

        USName=objProj.Project_Lead_US__r.Name;
        if(USName!=null && USName.Contains('('))
        USName=USName.substring(0,objProj.Project_Lead_US__r.Name.indexOf('('));
   
        USIName=objProj.Project_Lead_USI__r.Name;
        if(USIName!=null && USIName.Contains('('))
        USIName=USIName.substring(0,objProj.Project_Lead_USI__r.Name.indexOf('('));
       
         if(projectlead!=null && projectlead.Contains('(') )
               projectlead=projectlead.substring(0,projectlead.indexOf('('));

        objChartData.add(new ChartData(objProj.Name,false,USName,USIName,projectLead,objProj.Priority__c, lstProjTimeSpan,levelofeffort));
        }
    }
    
     for(Project__c objProj:lstProj){
      
        if(objProj.Priority__c=='Medium'){
        
       string levelofeffort = objProj.Level_of_effort_CPI__c;
        string leadname = '';
        string projectlead = '';
        if(sellevelofeffort == 'Level of effort – CPI'){levelofeffort = objProj.Level_of_effort_CPI__c;leadname='';projectlead ='';  }
        else if(sellevelofeffort  == 'Level of effort – GMC'){levelofeffort = objProj.Level_of_effort_GMC__c;leadname = 'GMC Project Lead';projectlead = objProj.GMC_Project_Lead__r.Name;}
        else if(sellevelofeffort  =='Level of effort – KM'){levelofeffort = objProj.Level_of_effort_KM__c;leadname = 'KM Project Lead';projectlead = objProj.KM_Project_Lead__r.Name;}
        else if(sellevelofeffort =='Level of effort – Operations'){levelofeffort = objProj.Level_of_effort_Operations__c;leadname = 'Ops Project Lead';projectlead = objProj.Ops_Project_Lead__r.Name;}
        else if(sellevelofeffort =='Level of effort- Technology'){levelofeffort = objProj.Level_of_effort_Technology__c;leadname = 'R&T Project Lead';projectlead = objProj.R_T_Project_Lead__r.Name;}
        else{}
        
        if(!setPP.contains(objProj.Priority__c)){
        setPP.add(objProj.Priority__c);
        objChartData.add(new ChartData('Projects - Medium',true,'CPI Project Lead (US)','CPI Project Lead (USI)',leadname,'Medium', lstMonths,''));
        }      
        lstProjDuration = new set<string>();   
        lstProjDuration=setMonthSpan(objProj.Project_Start_Date__c,objProj.Project_Planned_End_Date__c);
        lstProjTimeSpan=new List<string>();
        for(string strDur:lstMonths){
            if(lstProjDuration.Contains(strDur))
            lstProjTimeSpan.add('yes');
            else
            lstProjTimeSpan.add(' ');
        }
        USName=objProj.Project_Lead_US__r.Name;
        if(USName!=null && USName.Contains('('))
        USName=USName.substring(0,objProj.Project_Lead_US__r.Name.indexOf('('));
   
        USIName=objProj.Project_Lead_USI__r.Name;
        if(USIName!=null && USIName.Contains('('))
        USIName=USIName.substring(0,objProj.Project_Lead_USI__r.Name.indexOf('('));
        
        if(projectlead!=null && projectlead.Contains('(') )
               projectlead=projectlead.substring(0,projectlead.indexOf('('));
        
        objChartData.add(new ChartData(objProj.Name,false,USName,USIName,projectLead,objProj.Priority__c, lstProjTimeSpan,levelofeffort ));}
    }
   
    for(Project__c objProj:lstProj){
       
        if(objProj.Priority__c=='Low'){
        
         string levelofeffort = objProj.Level_of_effort_CPI__c;
        string leadname = '';
        string projectlead = '';
        if(sellevelofeffort == 'Level of effort – CPI'){levelofeffort = objProj.Level_of_effort_CPI__c;leadname='';projectlead ='';  }
        else if(sellevelofeffort  == 'Level of effort – GMC'){levelofeffort = objProj.Level_of_effort_GMC__c;leadname = 'GMC Project Lead';projectlead = objProj.GMC_Project_Lead__r.Name;}
        else if(sellevelofeffort  =='Level of effort – KM'){levelofeffort = objProj.Level_of_effort_KM__c;leadname = 'KM Project Lead';projectlead = objProj.KM_Project_Lead__r.Name;}
        else if(sellevelofeffort =='Level of effort – Operations'){levelofeffort = objProj.Level_of_effort_Operations__c;leadname = 'Ops Project Lead';projectlead = objProj.Ops_Project_Lead__r.Name;}
        else if(sellevelofeffort =='Level of effort- Technology'){levelofeffort = objProj.Level_of_effort_Technology__c;leadname = 'R&T Project Lead';projectlead = objProj.R_T_Project_Lead__r.Name;}
        else{}
        
            if(!setPP.contains(objProj.Priority__c)){
            setPP.add(objProj.Priority__c);
            objChartData.add(new ChartData('Projects - Low',true,'CPI Project Lead (US)','CPI Project Lead (USI)',leadname,'Low', lstMonths,''));
            }      
        lstProjDuration = new set<string>();   
        lstProjDuration=setMonthSpan(objProj.Project_Start_Date__c,objProj.Project_Planned_End_Date__c);
        lstProjTimeSpan=new List<string>();
        for(string strDur:lstMonths){
            if(lstProjDuration.Contains(strDur))
            lstProjTimeSpan.add('yes');
            else
            lstProjTimeSpan.add(' ');
        }
        USName=objProj.Project_Lead_US__r.Name;
        if(USName!=null && USName.Contains('('))
        USName=USName.substring(0,objProj.Project_Lead_US__r.Name.indexOf('('));
   
        USIName=objProj.Project_Lead_USI__r.Name;
        if(USIName!=null && USIName.Contains('('))
        USIName=USIName.substring(0,objProj.Project_Lead_USI__r.Name.indexOf('('));
        if(projectlead!=null && projectlead.Contains('(') )
               projectlead=projectlead.substring(0,projectlead.indexOf('('));
               
        objChartData.add(new ChartData(objProj.Name,false,USName,USIName,projectlead,objProj.Priority__c, lstProjTimeSpan,levelofeffort));}
    }  
    if(!IsFilterAction){
    objChartDataClone=objChartData;
    }
    setPP.clear();
}catch(Exception e){
  system.debug('----------Exception-------------'+e.getMessage()+'-----at Line #----'+e.getLineNumber());
  }
 system.debug('-------objChartData----------'+objChartData);
}
 
    /*
        Method:setMonthSpan
        Description: Returns Set of Month,Year Combinations in between two dates provided to this Method.
    */
public set<string> setMonthSpan(Date MinDate,Date MaxDate){
    set<string> setMonthSpan=new set<string>();
    try{
    if(MinDate!=null && MaxDate!=null){
    Integer TotMonths=MinDate.monthsBetween(MaxDate)+1;
    system.debug('===TotMonths===' + TotMonths);
    if(TotMonths==0)
    TotMonths=1;
   
    Integer MinMonth=MinDate.Month();
   
    Date TempYear=MinDate;
    for(integer i=0;i<TotMonths;i++){
        setMonthSpan.add(ConvertToMonth(TempYear.Month())+','+String.ValueOf(TempYear.Year()).Substring(2,4));
        system.debug('&&&&&&&&&&&&& ---' + setMonthSpan);
        TempYear=TempYear.addMonths(1);
        }
    }
     }catch(Exception e){
  system.debug('----------Exception-------------'+e.getMessage()+'-----at Line #----'+e.getLineNumber());
  }
         system.debug('setMonthSpan &&&&&& ---' + setMonthSpan);

    return setMonthSpan;
}
 
 
 
 
    /*
    Method:FilterResult
    Description: Populates data based on Filter conditions provided by User.
    */
public void FilterResult(){
     StartDate=null;
     EndDate=null; 
     
      if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('levelofeffort'))){
        sellevelofeffort =apexpages.currentpage().getparameters().get('levelofeffort');   
      }else{
       sellevelofeffort = 'Level of effort- Technology';
      }
     
     system.debug('---------objChartDataClone size()--------'+objChartDataClone.size());
    // Search
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SearchLead'))){
    SearchLead=apexpages.currentpage().getparameters().get('SearchLead');
   SearchLead=SearchLead.ToLowerCase().trim();
   
    }
    // Priority
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('Priority'))){
    Priority=apexpages.currentpage().getparameters().get('Priority');
    Priority=Priority.ToLowerCase();
    if(Priority=='-None-'){
      Priority=''; 
    }
    }
    // Category
    SelectedPCat='';
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SelectedPCat'))){
    SelectedPCat=apexpages.currentpage().getparameters().get('SelectedPCat');
    if(SelectedPCat=='-None-')
    SelectedPCat=null;
    }
    // Status
    SelectedPS='';
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SelectedPS'))){
    SelectedPS=apexpages.currentpage().getparameters().get('SelectedPS');
    if(SelectedPS=='-None-')
    SelectedPS=null;
    }
    // Start Date
    String mm;String dd;String yy;
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('StartDate'))){
    string StrStartDate=apexpages.currentpage().getparameters().get('StartDate');
   system.debug('$$$$$$$$$$$ ' + StrStartDate);
   
   
    string[] strStart=StrStartDate.split('/');
    mm=strStart[0];
    dd='01';
    yy=strStart[2];
   
    StartDate=Date.newInstance(integer.valueOf(yy),integer.valueOf(mm),integer.valueOf(dd));
    system.debug('^^^^^^^666' + StartDate);
   // StartDate=StartDate.addDays(-1);
        system.debug('^^^^^^^777777' + StartDate);

   
    }
    // End Date
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('EndDate'))){
    string StrEndDate=apexpages.currentpage().getparameters().get('EndDate');
   
    string[] strEnd=StrEndDate.split('/');
    mm=strEnd[0];
    dd='27';//strEnd[2];
    yy=strEnd[2];
    EndDate=Date.newInstance(integer.valueOf(yy),integer.valueOf(mm),integer.valueOf(dd));
  //  EndDate=EndDate.addDays(1);
    }
    if(EndDate!=null && StartDate!=null){
    integer diffBw=StartDate.daysBetween(EndDate);
    if(diffBw<0)
    {
    apexpages.Message msg = new Apexpages.Message(ApexPages.Severity.Warning,'Start Date must be less than End date.');
    apexpages.addmessage(msg); 
    }
    }
       
    
    IsFilterAction=true;
    ProcessData();// Prepare Date by Querying on Database with provided filter options
    SearchLead='';
    Priority='';
}
     /*
     * Method Name: ConvertToFullMonth
     * Description: Number to Month Name Conversion
     *
     */
    public static string ConvertToMonth(integer Mon){
       
        
        if(Mon==1){
          return 'Jan'; 
        }
        else if(Mon==2){
          return 'Feb'; 
        }
        else if(Mon==3){
          return 'Mar';  
        }
        else if(Mon==4){
          return 'Apr'; 
        }
        else if(Mon==5){
          return 'May'; 
        }
        else if(Mon==6){
          return 'Jun'; 
        }
        else if(Mon==7){
         return 'Jul';  
        }
        else if(Mon==8){
         return 'Aug';  
        }
        else if(Mon==9){
         return 'Sep';  
        }
        else if(Mon==10){
        return 'Oct';   
        }
        else if(Mon==11){
        return 'Nov';   
        }
        else if(Mon==12){
          return 'Dec'; 
        }
        else
          return string.Valueof(Mon); 
    }
 
 
 
     /*
    Class:ChartData
    Description: Wrapper class used to prepare data
    */
    public class ChartData {
        public string ProjectName { get; set; }
        public boolean IsSubHeader{get;set;}
        public string USLead{get;set;}
        public string USILead{get;set;}
        Public string Leadname{get;set;}
        public string Priority{get;set;}
        public string lvlofEffrt{get;set;}
        public List<string> ItemsPresent{ get; set; }
       
        public ChartData(String ProjectName1,boolean IsSubHeader1,string USLead1,string USILead1,string leadname1,string Priority1,List<string> ItemsPresent1,string lvlofEffrt1) {
           
            this.ProjectName = ProjectName1;
            this.IsSubHeader= IsSubHeader1;
            this.USLead = USLead1;
            this.USILead= USILead1;
            this.Leadname=leadname1;
            this.Priority=Priority1;
            this.ItemsPresent= ItemsPresent1;
            this.lvlofEffrt=lvlofEffrt1;
        }
    }
 
 
}