@isTest
public class WCT_Visa_Frst_Advntge_Apliction_Cnt_Test
{
     public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        insert mob;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef;
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        Test.starttest();
       
        
        PageReference pageRef = Page.WCT_Visa_First_Advantage_App_Thankyou;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        WCT_Visa_First_Advantage_Application_Cnt controller=new WCT_Visa_First_Advantage_Application_Cnt();
        
        controller=new WCT_Visa_First_Advantage_Application_Cnt();
        controller.attachmentHelper = new GBL_Attachments();
       List<Folder> lstFolder = [Select Id From Folder Where Name = 'IEF Documents Zip' limit 1];
       Document d = new Document(FolderId = lstFolder.get(0).Id, Name='Test Name',Keywords = 'Test',Body = Blob.valueOf('Some Text'),ContentType = 'application/pdf');
      controller.attachmentHelper.doc = d;  
      controller.attachmentHelper.doc.body = Blob.valueOf('Selected Document');
      controller.attachmentHelper.doc.Name = 'My Document';
      controller.attachmentHelper.doc.FolderId = lstFolder.get(0).Id;
      controller.attachmentHelper.uploadDocument(); 
        controller.getAttachmentInfo();
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        controller.supportAreaErrorMesssage='error message';
        controller.taskid=t.id;
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
      
        
    }
    
    public static testmethod void method2()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        insert mob;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef;
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        Document document;
        document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'my_document';
        document.IsPublic = true;
        document.Name = 'My Document';
        document.FolderId = [select id from folder where name = 'BME Documentation'].id;
        insert document;

   //  ID tskid =  ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));    
        Attachment attachment = new Attachment();
        attachment.ParentId = t.ID;
        attachment.Name = 'Test Attachment for Parent';
        attachment.Body = document.Body ;
        insert attachment;
        
        Test.starttest();
       
        
        PageReference pageRef = Page.WCT_Visa_First_Advantage_App_Thankyou;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        WCT_Visa_First_Advantage_Application_Cnt controller=new WCT_Visa_First_Advantage_Application_Cnt();
        
        controller=new WCT_Visa_First_Advantage_Application_Cnt();
        controller.attachmentHelper = new GBL_Attachments();
        controller.getAttachmentInfo();
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        controller.supportAreaErrorMesssage='error message';
        controller.taskid=t.id;
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
      
        
    }
    
    
}