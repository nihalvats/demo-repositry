@isTest
private class WCT_CustomLookupSelectionController1_tes {

    static testMethod void myUnitTest() {
        
        list<Case> caseList = new list<Case>();
        Test_Data_Utility.createCase();
        caseList = [select Id from Case limit 1];
        test.setCurrentPage(page.WCT_ContactLookupSelectionPage1);
        ApexPages.currentPage().getParameters().put('ObjType', 'contact');
         ApexPages.currentPage().getParameters().put('lksrch', 'a');
         WCT_CustomLookupSelectionController1 controller1= new  WCT_CustomLookupSelectionController1();
        
        ApexPages.currentPage().getParameters().put('ObjType', 'case');
        ApexPages.currentPage().getParameters().put('lksrch', 'a');
        ApexPages.currentPage().getParameters().put('namefield', 'sa');
        ApexPages.currentPage().getParameters().put('frm','test');
        ApexPages.currentPage().getParameters().put('txt','test');
        
        Test_Data_Utility.createContact();
        list<Contact> con=[select id, name ,WCT_Type__c,WCT_Person_Id__c,Email,Phone,WCT_Job_Text__c,WCT_Service_Area__c from contact Limit 1 ];
        WCT_CustomLookupSelectionController1 controller= new  WCT_CustomLookupSelectionController1();
        system.assertEquals(controller.sObjectType,'case');
        controller.getFormTag();
        controller.search();
        Contact c= new Contact(LastName='test');
        controller.account= c;
        controller.saveAccount();
        controller.getTextBox();
        controller.strContactID=con[0].id;
        controller.refSelectContact();
         
     }
 }