global class WCT_FindsObjStagesCorrected {
  
   
    WebService static void processStagingRecords(List<Id> stagingIds,String stagingStatus)
    {
      set<id> tempset = new set<id>();
      tempset.addAll(stagingIds);
      new WCT_UpdateCandTracker().processStagingRecords(tempset,stagingStatus); 
    }
    
    WebService static void processIntvStagingRecords(List<Id> stagingIds,String stagingStatus)
    {
      set<id> tempset = new set<id>();
      tempset.addAll(stagingIds);
      new WCT_Interview_BulkUpload().processStagingRecords(tempset,stagingStatus); 
    }
    
    WebService static void processCandLoadStagingRecords(List<Id> stagingIds,String stagingStatus)
    {
      set<id> tempset = new set<id>();
      tempset.addAll(stagingIds);
      new WCT_CT_BulkUpload().processStagingRecords(tempset,stagingStatus); 
    }

}