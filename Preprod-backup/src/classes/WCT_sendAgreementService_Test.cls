@isTest(seealldata = true)
private class WCT_sendAgreementService_Test {

        
        static testMethod void UnitTest(){
        
        Contact con=WCT_UtilTestDataCreation.createContact();
        con.email='test@deloitte.com';
        con.WCT_Visa_Type__c='temp';
        con.MobilePhone='123';
        con.WCT_User_Group__c='United States';
        con.WCT_Taleo_Status__c='Offer Details Updated';           
        insert con;
      list<id> lscon = new list<id>();
       lscon.add(con.id);
       WCT_Offer__c  off = new WCT_Offer__c ();
       off.WCT_status__c= 'Offer Approved';
      off.WCT_Candidate__c = con.id;
      insert off;
      
      ID i = off.id;
      ID i1 = con.id;
     list<id> lsoff = new list<id>();
     lsoff.add(off.id);   
     Attachment Att= new Attachment();
        Blob b = Blob.valueOf('Test Data');  
        Att.Name='Test Attachment';  
        Att.Body=b;
        
        Att.parentId=off.id;
        insert Att;
        string message = 'hello'; 
        String olTitle = 'title';
        String t = 'test';
        
    
       // WCT_sendAgreementService wc = new WCT_sendAgreementService ();
        echosign_dev1__SIGN_Agreement__c agmt = new echosign_dev1__SIGN_Agreement__c();
        agmt.Name = 'testagreement';
        agmt.echosign_dev1__Recipient__c = con.id;
        agmt.WCT_Offer__c =  off.id;
        agmt.echosign_dev1__Status__c = 'Draft';
        agmt.echosign_dev1__Background_Action__c = 'Send';
        agmt.echosign_dev1__DaysUntilSigningDeadline__c = 120;
        //agmt.message = 'Please+sign+the+attached+document+from+Deloitte';
        agmt.echosign_dev1__Message__c = EncodingUtil.urlDecode('Please+sign+the+attached+document+from+Deloitte', 'UTF-8');
        agmt.echosign_dev1__Enable_Automatic_Reminders__c = true;
        insert agmt;
      Test.starttest();    
     //  Test.setMock(HttpCalloutMock.class, new WCT_MockHttpResponseGenerator_Test());  
     WCT_sendAgreementService wc = new WCT_sendAgreementService ();
     
     //  id TestOffer =off.id;
      // Test.setMock(WCT_sendAgreementService , new WCT_sendAgreementService());
        WCT_sendAgreementService.sendOffers(i, i1,'hey', 'title', ' hellomessage');
      Test.stoptest();
     
                            
}
}