public with sharing class ProjectsByInvolvedTeamsController{

/*
 Apex Class:  ProjectsByInvolvedTeamsController
 Purpose: This class is used to prepare view of Projects By talent Channerl
 Created On:  3rd July,2015.
 Created by:  Balu Devarapu.(Deloitte India)
*/


public set<string> lstInvTeam{get;set;}
public List<ChartData> objChartData{get;set;}
public List<SelectOption> ITOptions{get;set;}
public List<SelectOption> PSOptions{get;set;}
public List<SelectOption> PCatOptions{get;set;}
public string SelectedIT{get;set;}
public string SelectedPS{get;set;}
public string SelectedPCat{get;set;}
public Map<string,string> mapTC{get;set;}
public List<integer> lstTCOccurenceCount{get;set;}
set<string> lstInvTeamTemp=new set<string>();
Map<string,integer> mapTcOccournace{get;set;}
public boolean IsShowTConly=false;
/* Constructor */
public ProjectsByInvolvedTeamsController(){
    IsShowTConly=false;
    lstTCOccurenceCount=new List<integer>();
    mapTcOccournace=new Map<string,integer>();
    SelectedIT='-None-';
    lstInvTeam=new set<string>();
    
    Schema.DescribeFieldResult objlstTC = Project__c.Involved_Teams__c.getDescribe();
    List<Schema.PicklistEntry> lstTC = objlstTC.getPicklistValues();
    
    ITOptions = new List<SelectOption>();
    ITOptions.add(new SelectOption('-None-','-None-'));
    for(Schema.picklistEntry f:lstTC)    
    {    
        if(f.getLabel()!='------------------None------------------'){
          
        lstInvTeam.add(f.getLabel());  
        ITOptions.add(new SelectOption(f.getLabel(),f.getLabel()));     
        }
    }
    lstInvTeamTemp=lstInvTeam;
    PSOptions = new List<SelectOption>();
    Schema.DescribeFieldResult objlstPS = Project__c.Status__c.getDescribe();
    List<Schema.PicklistEntry> lstPS = objlstPS.getPicklistValues();
        for(Schema.picklistEntry f:lstPS)    
        {    
            PSOptions.add(new SelectOption(f.getLabel(),f.getLabel()));     
        }
    SelectedPS='In Progress';   
    
    PCatOptions= new List<SelectOption>();
    PCatOptions.add(new SelectOption('-None-','-None-'));
    Schema.DescribeFieldResult objPCatOptions = Project__c.Project_Category__c.getDescribe();
    List<Schema.PicklistEntry> lstPCat = objPCatOptions.getPicklistValues();
        for(Schema.picklistEntry f:lstPCat)    
        {    
            PCatOptions.add(new SelectOption(f.getLabel(),f.getLabel()));     
        }
    SelectedPCat='-None-';  
    ProcessData();
} 

    /* 
        Method:ProcessData
        Description: Data view Perparation 
    */
public void ProcessData(){
    try{
    lstTCOccurenceCount=new List<integer>();
    mapTcOccournace=new Map<string,integer>();
    objChartData = new List<ChartData>();

    mapTC=new Map<string,string>();

    If(string.IsEmpty(SelectedPS))
    SelectedPS='In Progress';
    If(string.IsEmpty(SelectedPCat) || SelectedPCat=='-None-')
    SelectedPCat=null;
   
    if(IsShowTConly){
        if(SelectedPCat!=null){
            for(Project__c objProj: [select Name,Involved_Teams__c from Project__c where Status__c=:SelectedPS and Involved_Teams__c!=null and Project_Category__c=:SelectedPCat order by Name]) 
            {
                mapTC.put(objProj.Name,objProj.Involved_Teams__c);
            }   
        }else{
            for(Project__c objProj: [select Name,Involved_Teams__c from Project__c where Status__c=:SelectedPS and Involved_Teams__c!=null order by Name]) 
            {
                mapTC.put(objProj.Name,objProj.Involved_Teams__c);
            }   
        }
          
    }
    else{
        if(SelectedPCat!=null){
            for(Project__c objProj: [select Name,Involved_Teams__c from Project__c where Status__c=:SelectedPS and Project_Category__c=:SelectedPCat order by Name]) 
            {
                mapTC.put(objProj.Name,objProj.Involved_Teams__c);
            }   
        }
        else{
            for(Project__c objProj: [select Name,Involved_Teams__c from Project__c where Status__c=:SelectedPS order by Name]) 
            {
                mapTC.put(objProj.Name,objProj.Involved_Teams__c);
            }   
        }

    }
    system.debug('----mapTC-Size--'+mapTC.Size());
    for(string objPName:mapTC.keyset())
    {
        string InvolvedTeams=mapTC.get(objPName);
        if(string.IsNotEmpty(InvolvedTeams)){
        List<string> lstInvolvedTeams=InvolvedTeams.split(';');
        
        Set<string> setInvolvedTeams=new Set<string>();
        
        for(string strTC:lstInvolvedTeams)
        {
          if(lstInvTeam.contains(strTC)){
           setInvolvedTeams.add(strTC);
           Integer iCount=1;
           if(mapTcOccournace.get(strTC)!=null){
            iCount= mapTcOccournace.get(strTC);
            iCount++;
           }
           mapTcOccournace.put(strTC,iCount);
           
          }
        }
        
        setInvolvedTeams.remove('------------------None------------------');
        if(IsShowTConly){
        if(setInvolvedTeams.Size()>0){
         objChartData.add(new ChartData(objPName, setInvolvedTeams)); 
        } 
        }else{
        objChartData.add(new ChartData(objPName, setInvolvedTeams));   
        }
    
     }
    }
     system.debug('----objChartData-Size--'+objChartData.Size());
     for(string strTalC:lstInvTeam){
     integer iCount=0;
     if(mapTcOccournace.get(strTalC)!=null){
        iCount= mapTcOccournace.get(strTalC);
       }
     lstTCOccurenceCount.add(iCount);
    }

 
 }catch(Exception e){
  system.debug('----------Exception-------------'+e.getMessage()+'-----at Line #----'+e.getLineNumber()); 
  }
 
 
 
} 

    /* 
    Method:FilterResult
    Description: Populates data based on Filter conditions provided by User. 
    */
public void FilterResult(){
    lstTCOccurenceCount=new List<integer>();
    mapTcOccournace=new Map<string,integer>();
    SelectedPS='';
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SelectedPS'))){
    SelectedPS=apexpages.currentpage().getparameters().get('SelectedPS');
    }
    
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('IsShowTConly'))){
    string strIsShowTConly=apexpages.currentpage().getparameters().get('IsShowTConly');
    system.debug('-------IsShowTCOnly----'+strIsShowTConly);
     if(strIsShowTConly=='true')
         IsShowTConly=true;
     else
         IsShowTConly=false;
    }
    
    SelectedPCat='';
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SelectedPCat'))){
    SelectedPCat=apexpages.currentpage().getparameters().get('SelectedPCat');
    }
    
    
    SelectedIT='-None-';
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SelectedIT'))){
    SelectedIT=apexpages.currentpage().getparameters().get('SelectedIT');
    if(SelectedIT=='-None-'){
    lstInvTeam=lstInvTeamTemp; // Show all Channels;
    }
    else{
    lstInvTeam=new set<string>();
    objChartData = new List<ChartData>();
    lstInvTeam.add(SelectedIT);
    }
    ProcessData();
    }
}


     /* 
    Class:ChartData 
    Description: Wrapper class used to prepare data
    */

    public class ChartData {
        public String ProjectName { get; set; }
        public set<string> ItemsPresent{ get; set; }

        public ChartData(String ProjectName1, set<string> setInvolvedTeams1) {
            
            this.ProjectName = ProjectName1;
            this.ItemsPresent= setInvolvedTeams1;

        }
    }


}