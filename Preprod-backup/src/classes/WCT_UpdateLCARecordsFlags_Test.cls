@isTest
private class WCT_UpdateLCARecordsFlags_Test{
    static testMethod void testMyWebSvc(){ 
    
        WCT_LCA__c rec = new WCT_LCA__c();
        INSERT rec;
    
        Test.startTest();
        WCT_UpdateLCARecordsFlags.updateFlags(rec.Id);
        Test.stopTest(); 
    } 
}