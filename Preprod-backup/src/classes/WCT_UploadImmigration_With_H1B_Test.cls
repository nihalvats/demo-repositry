@isTest
public class WCT_UploadImmigration_With_H1B_Test 
{
   testmethod static void test1()
    {
        Test.startTest();
        /*Test the Class*/
        List<WCT_List_Of_Names__c> configSettings= new List<WCT_List_Of_Names__c>();
        WCT_List_Of_Names__c setting1= new WCT_List_Of_Names__c();
        setting1.Name='Config1';
        setting1.WCT_Type__C='GGS';
        setting1.H1B_Column_Name__c='FN Email Address';
        setting1.H1B_Data_Type_of_the_Field__c='';
        setting1.H1B_Immigration_API_Name__c ='H1B_Email_Fragomen__c';
        setting1.H1B_Is_Required__c =True;
        setting1.H1B_Stagging_API_Name__c ='WCT_Employee_Email__c';
        setting1.recordTypeId=Schema.SObjectType.WCT_List_Of_Names__c.getRecordTypeInfosByName().get('H1B Immigration Upload Settings').getRecordTypeId();
        configSettings.add(setting1);
        
        WCT_List_Of_Names__c setting2= new WCT_List_Of_Names__c();
        setting2.Name='Config2';
        setting2.WCT_Type__C='GGS';
        setting2.H1B_Column_Name__c='Identifier';
        setting2.H1B_Data_Type_of_the_Field__c='';
        setting2.H1B_Immigration_API_Name__c ='WCT_Identifier__c';
        setting2.H1B_Is_Required__c =True;
        setting2.H1B_Stagging_API_Name__c ='WCT_Identifier__c';
        setting2.recordTypeId=Schema.SObjectType.WCT_List_Of_Names__c.getRecordTypeInfosByName().get('H1B Immigration Upload Settings').getRecordTypeId();
        configSettings.add(setting2);
        
        WCT_List_Of_Names__c setting3= new WCT_List_Of_Names__c();
        setting3.Name='Config3';
        setting3.WCT_Type__C='GGS';
        setting3.H1B_Column_Name__c='Date Field';
        setting3.H1B_Data_Type_of_the_Field__c='Date';
        setting3.H1B_Immigration_API_Name__c ='H1B_Date_of_Current_Process_Close__c';
        setting3.H1B_Is_Required__c =True;
        setting3.H1B_Stagging_API_Name__c ='WCT_Date_Initiation_Sent_to_Fragomen__c';
        setting3.recordTypeId=Schema.SObjectType.WCT_List_Of_Names__c.getRecordTypeInfosByName().get('H1B Immigration Upload Settings').getRecordTypeId();
        configSettings.add(setting3);
        
        insert configSettings;
        
        
        List<Contact> testContact= new List<Contact>();
        Contact contact1= new Contact();
        contact1.FirstName='test1';
        contact1.LastName='LN1';
        contact1.Email='test1@test.com';
        contact1.AR_Deloitte_Email__c='test1@test.com';
        contact1.AR_Personal_Email__c='test1@test.com';
        contact1.WCT_ExternalEmail__c='test1@test.com';
        
        
        contact1.RecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
        testContact.add(contact1);
        
        
        
      
        
        
        Contact contact2= new Contact();
        contact2.FirstName='test2';
        contact2.LastName='LN2';
        contact2.Email='test3@test.com';
        contact2.Additional_Email__c='test3@test.com';
        contact2.AR_Deloitte_Email__c='test3@test.com';
        contact2.AR_Personal_Email__c='test3@test.com';
        contact2.WCT_ExternalEmail__c='test3@test.com';
        contact2.RecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
        testContact.add(contact2);
        
          Contact contact3= new Contact();
        contact3.FirstName='test1';
        contact3.LastName='LN1';
        contact3.Email='test1@test.com';
        contact3.Additional_Email__c='test1@test.com';
        contact3.AR_Deloitte_Email__c='test1@test.com';
        contact3.AR_Personal_Email__c='test1@test.com';
        contact3.WCT_ExternalEmail__c='test1@test.com';
        contact3.RecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate').getRecordTypeId();
        testContact.add(contact3);
        
          Contact contact4= new Contact();
        contact4.FirstName='test1';
        contact4.LastName='LN1';
        contact4.Email='test1@test.com';
        contact4.Additional_Email__c='test1@test.com';
        contact4.AR_Deloitte_Email__c='test1@test.com';
        contact4.AR_Personal_Email__c='test1@test.com';
        contact4.WCT_ExternalEmail__c='test1@test.com';
        contact4.RecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate Ad Hoc').getRecordTypeId();
        testContact.add(contact4);
        
        
        insert testContact;
		
        
        
        WCT_UploadImmigration_With_H1B ctrl= new WCT_UploadImmigration_With_H1B();
        ctrl.law_Firm_Name='GGS';
        
        //String csvheader = 'Identifier,Company Name,Initiation Date,Fiscal year of current in-process initiation,Type of H1B Application Process,Milestone,Milestone Date,Hire Status,Current visa status,Date reached out to employee for documents,Date employee submitted documents,Reporting office location (full address),Client location (full address),Client documentation included with initial filing (if applicable),Receipt #,Receipt Date,RFE Date (if applicable),RFE Response Submission Date,RFE Due Date,RFE Type,Project Contact Name,Project Contact Email Address,RFE Client Letter #,Petition Start Date,Petition End Date,USCIS denial grounds,Law firm case contact,Resource Manager,Qualifies/Does not qualify for April 1st filing (H-1B cap),Current work authorization expiration date (H-1B cap),Work authorization through April 1st (H-1B cap),Work authorization through October 1st (H-1B cap),Cap rejection/denial contingency,Change of status effective date,Lottery results (H-1B cap),HR Contact Name,HR Contact Email,Database Title,Personnel Number,Last Name,First Name,FN Email Address,Client Name included in initial filing,Service Area,Service Line,Job Title,Region,RFE Status';
       // String dataRow = 'test2@test.com H1B,,,,,,,,,,,,,,,42289,,,,,,,,42289,42290,,,,,,,,,,,,,,40001,Tes ,LastName,test2@test.com,Test ,Test ,Test ,Test ,yesy,Test';
        String csvheader = 'Identifier,Date Field,FN Email Address';
        String dataRow = 'test3@test.comH1B, 10/12/2015,test3@test.com';
        String dataRow2 = 'test2@test.comH1B, 10/12/2015,test2@test.com';
        String dataRow3 = 'test5@test.comH1B, ,';
        String dataRow4 = 'test1@test.comH1B, ,test1@test.com';
        String blobCreator = csvheader + '\r\n' + dataRow+ '\r\n' + dataRow2+ '\r\n' + dataRow3+ '\r\n' + dataRow4;
        ctrl.contentFile=blob.valueof(blobCreator);
        ctrl.readFile();
        ctrl.getAllStagingRecords();
        
        Test.stopTest();
        
    }
}