global class WCT_CancelImmigrationRecordFlag{
    
    WebService static String updateFlags(String recordId)
    {
        
        WCT_Immigration__c rec;
        try{
            rec=new WCT_Immigration__c(id=recordId);
            rec.WCT_Immigration_Closed__c=true;
            rec.WCT_Record_Visible_to_ToD__c=false;
            update rec;
        
        }Catch(Exception e)
        {
            WCT_ExceptionUtility.logException('WCT_CancelImmigrationRecordFlag','Cancel Immigration Record Flag',e.getMessage());
        }
        
        return 'The Immigration Record has been canceled.';
        
    }

}