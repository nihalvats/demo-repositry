/************************************************************************************************
Apex class       : Emp_ResignationForm_onbehalfCon
Created Date     : 25 April 2016
Function         : Resignation form edited by the employee when someone submits on his behalf

* Developer                   Date                   Description				Test Class
* -------------------------------------------------------------------------------------------------                
* Deloitte                   25/04/2016            Original Version				Emp_ResignationForm_onbehalfCon_Test
***************************************************************************************************/
public class Emp_ResignationForm_onbehalfCon{
    
    public string pageerrormsg {get;set;}  
    public GBL_Attachments attachmentHelper{get; set;} 
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public boolean reset {get; set;}
    public case casesep{get;set;}
    public string caseId{get;set;}
    public boolean checkseattle{get;set;}
    public boolean checkstatus{get;set;}
    public boolean checkstatus1{get;set;}
    public Emp_ResignationForm_onbehalfCon(){
        casesep = new case();
        attachmentHelper = new GBL_Attachments();
        pageerrormsg ='false';
        reset = false;
        string olesepid = apexpages.currentpage().getparameters().get('rid');
        
        casesep= [Select id,contact.name,contact.WCT_Personnel_Number__c,contact.WCT_Hiring_location__c,ELE_Resignation_Date__c,
                  ELE_Are_you_an_ORCA_Card_holder__c,ELE_Office_Address__c,ELE_Other_Employer__c,ELE_Reason_for_Leaving__c,WCT_FutureEmployer__c,
                  WCT_Country_IEF__c,WCT_LastWorkDate__c,ELE_Name_of_Future_Employer__c,
                  WCT_Future_Employer_Job_Title__c,WCT_FutureEmployerStartDate__c,ELE_Are_you_Retiring__c,
                  WCT_PayrollTermDate__c,ELE_Comments__c,contact.WCT_Office_City_Personnel_Subarea__c,ELE_Count_of_Hours__c,ELE_Updated_by_Employee__c from case WHERE ID=:olesepid ];
        caseId=casesep.id;
        casesep.id=null;
        
        if(casesep.ELE_Count_of_Hours__c>2880 || casesep.ELE_Updated_by_Employee__c == true){
            checkstatus=true;
            checkstatus1=false;
        }
        else{
            checkstatus1=true;
            checkstatus=false;
        }
        if(casesep.contact.WCT_Office_City_Personnel_Subarea__c != null){
            if(casesep.contact.WCT_Office_City_Personnel_Subarea__c.containsIgnoreCase('Seattle')){
                checkseattle  = true;
            }
            else{
                checkseattle=false;
            }
        }
    }
    
 /**************************
Method Name  : saverecord
Return Type  : PageReference
Description  : Saves resignation details
*****************************/
    Public PageReference saverecord(){
        
        try{
        pageerrormsg = 'false';
        PageReference pageRef = Page.GBL_Page_Notification;
        
        
        list<MAP_ROL_SEPL__c> trtd = MAP_ROL_SEPL__c.getall().values();
        /*Submit Id */
        casesep.Id =caseId;
        
        for (MAP_ROL_SEPL__c maprol:trtd)
        {if(casesep.ELE_Reason_for_Leaving__c == maprol.Reason_of_Leaving__c){
            casesep.WCT_SeparationReason__c=maprol.Separation_Reason__c;
        }
         
        }
        
        casesep.ELE_Employee_Update_Date_Time__c=datetime.now();
        casesep.ELE_Updated_by_Employee__c=true;
        upsert casesep;
        //Global attachment method
        attachmentHelper.uploadRelatedAttachment(casesep.Id);
        
        List<case> Cseid=[Select id,CaseNumber from case where id=:casesep.id];  
            
        pageRef.getParameters().put('key','success');
        pageRef.getParameters().put('cid',Cseid[0].CaseNumber);
        PageRef.getParameters().put('res','No' );
              
        return pageRef;        
        }
        catch (Exception e) {
            
            Exception_Log__c log=WCT_ExceptionUtility.logException('Emp_ResignationForm_onbehalfCon', 'EMP_onBehalf_ResignationForm_New', e.getMessage());
            
            pagereference pg = new pagereference('/apex/GBL_Page_Notification?key=ErrorMsg&expCode='+log.Name);
            pg.setRedirect(true);
            return pg;
        }
        
        
    }
   
}