@isTest
private class WCT_UpdateRecordsFlags_Test{
    static testMethod void testMyWebSvc(){ 
    
        WCT_Immigration__c rec = new WCT_Immigration__c();
        INSERT rec;
    
        Test.startTest();
        WCT_UpdateRecordsFlags.updateFlags(rec.Id);
        Test.stopTest(); 
    } 
}