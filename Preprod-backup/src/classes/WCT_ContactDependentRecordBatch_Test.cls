@isTest
public class WCT_ContactDependentRecordBatch_Test
{
    static testMethod void m1(){
        recordtype rt=[select id from recordtype where Name = 'Dependant'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.WCT_Visa_End_Date__c = system.today().addDays(-2);
        con.WCT_GMI_Record__c = true;
        con.WCT_Visa_Start_Date__c = system.today().addDays(-5);
        insert con;
        
        Test.startTest();
        WCT_ContactDependentRecordBatch createCon = new WCT_ContactDependentRecordBatch();
        system.schedule('New','0 0 2 1 * ?',createCon); 
        Test.stopTest(); 
    } 
    static testMethod void m2(){
        recordtype rt=[select id from recordtype where Name = 'Dependant'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.WCT_Visa_End_Date__c = system.today().addDays(12);
        con.WCT_GMI_Record__c = true;
        con.WCT_Visa_Start_Date__c = system.today().addDays(-5);
        insert con;
        
        Test.startTest();
        WCT_ContactDependentRecordBatch createCon = new WCT_ContactDependentRecordBatch();
        system.schedule('New','0 0 2 1 * ?',createCon); 
        Test.stopTest(); 
    } 
}