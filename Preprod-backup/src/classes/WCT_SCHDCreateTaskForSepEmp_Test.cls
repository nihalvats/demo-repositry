@isTest
private class WCT_SCHDCreateTaskForSepEmp_Test {

    public static Contact Employee;
    public static List<WCT_Leave__c> leaveRecList;
    
     /** 
        Method Name  : createEmployee
        Return Type  : Contact
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createEmployee()
    {
        Employee=WCT_UtilTestDataCreation.createEmployee(WCT_Util.getRecordTypeIdByLabel('Contact','Employee'));
        Employee.WCT_Is_Separated__c = true;
        insert Employee;
        return  Employee;
    }  

    /** 
        Method Name  : createLeavesForEmp
        Return Type  : List<WCT_Leave__c>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static List<WCT_Leave__c> createLeavesForEmp(){
        leaveRecList = WCT_UtilTestDataCreation.createLeaves(WCT_Util.getRecordTypeIdByLabel('WCT_Leave__c','Personal'),Employee);
        insert leaveRecList;
        return leaveRecList;
    }
    
    static testMethod void schEmpTaskTest(){
        Employee = createEmployee();
        leaveRecList = createLeavesForEmp();
        test.startTest();
        system.schedule('Batch to create Tasks','0 0 2 1 * ?',new WCT_SCHDCreateTaskForSepEmp());
        test.stopTest();
    }

}