/**************************************************************************************
Class       :  ResignationFormController
Version     : 1.0 
Created Date: 25 April 2015
Function    : Controller for Resignation Form (USI) 
Test class  : ELE_Separation_Handler_Test
      

* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   04/25/2015            Original Version
* Abhisek                    05/18/2016            UI Change as part of compliance
*************************************************************************************/
public with sharing class ResignationFormController extends SitesTodHeaderController {
    
    
    public ELE_Separation__c eleList {get;set;}
    /* Error Message related variables */
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
   // public Contact loggedInContact {get; set;}    
    public Boolean invalidEmployee {get; set;}
    //public String encryptedEmailString {get; set;}
    public String contactName {get;set;}
    public String lwd {get;set;}
    public Date slwd {get;set;}
    //public String dateofResign{get;set;}
    public List<Contact> listContact{get;set;} 
    //public List<Contact> mailContact{get;set;} 
    public Contact employeeContact{get;set;}
    //Public Decimal star = '*';
    
    /*Attachment Related Variables*/   
    public GBL_Attachments attachmentHelper{get; set;}
    
    /*Common Fields*/
    //public String estimatedLastWorkDay {get;set;}
    //public String estimatedReturnWorkDay {get;set;}
    //public String comments {get;set;}
    //public String formType {get;set;}
    public String sContact {get;set;}
    public String empEmail{get;set;}
    
    /* Contact fields extract */
    public ResignationFormController()
    {
        pageError = false;
        employeeContact=new Contact();
        eleList = new ELE_Separation__c();
        attachmentHelper= new GBL_Attachments(); 
        //eleList = [Select e.US_Career_Model_Mapping_Separation__c, e.SystemModstamp, e.Service_Line_Separation__c, e.Service_Area_Separation__c, e.SAP_Separation_code_Separation__c, e.Region_Separation__c, e.RecordTypeId, e.RC_Code_Separation__c, e.Pers_No_Separation__c, e.Period_Of_Exit_Separation__c, e.OwnerId, e.Onsite_exit_Separation__c, e.Name_Separation__c, e.Name, e.Location_Separation__c, e.Last_Date_Separation__c, e.LastViewedDate, e.LastReferencedDate, e.LastModifiedDate, e.LastModifiedById, e.IsDeleted, e.Id, e.Global_Person_Number_Separation__c, e.Gender_Separation__c, e.Function_Separation__c, e.F_F_complete_Separation__c, e.FSS_Separation__c, e.Exit_Separation__c, e.Exit_Intw_Separation__c, e.Exit_Clearance_Complete_Separation__c, e.Entity_Separation__c, e.Email_id_Separation__c, e.Eligible_for_Rehire_Separation__c, e.Eligible_for_Alumni_Separation__c, e.Designation_Separation__c, e.Date_of_resignation_Separation__c, e.Date_of_action_move_Separation__c, e.DOJ_Separation__c, e.CreatedDate, e.CreatedById, e.Clearance_Records_Created__c From ELE_Separation__c e];
        
        invalidEmployee = false;
        
        /*       

mailContact = [
SELECT 

WCT_Function__c,
WCT_Service_Area__c,
WCT_Service_Line__c,
ELE_Contact__c

//       (select id,contact_name_email__c,WCT_LastWorkDate__c,date_of_resignation__c from cases where contact_name_email__c =:empEmail ) 
FROM
Contact
WHERE
Email = :empEmail and RecordType.Name = 'Employee'
LIMIT
1     
];   

*/     
     
        //encryptedEmailString = ApexPages.currentPage().getParameters().get('em');
        //empEmail = cryptoHelper.decrypt(encryptedEmailString);
        
        //system.debug('****Email'+loggedInContact.Email);
   
        empEmail = loggedInContact.email;  
        
        if( (null != empEmail) && ('' != empEmail) ) {
            listContact = [
                SELECT 
                Id,
                Name,
                FirstName,
                WCT_Middle_Name__c,
                WCT_ExternalEmail__c,
                WCT_Relationship__c,
                WCT_Personnel_Number__c,
                WCT_Internship_Start_Date__c,
                WCT_Entity__c,
                LastName,WCT_Last_Day_Worked__c,
                Email,ELE_Open_ELE_case__c ,
                ELE_Counselor_Email_ID__c,
                WCT_DPM_Counselor__c,   
                TRS_Resource_Manager__c,
                Resource_Managers_Email_ID__c,
                Phone
                //       (select id,contact_name_email__c,WCT_LastWorkDate__c,date_of_resignation__c from cases where contact_name_email__c =:empEmail ) 
                FROM
                Contact
                WHERE
                Email = :empEmail and RecordType.Name = 'Employee'
                LIMIT
                1   
            ];
            
            if(listContact.size()>0)
            {
                
                eleList.ELE_Counselor_Name__c = listContact[0].WCT_DPM_Counselor__c;
                eleList.ELE_Counselor_Email_ID__c = listContact[0].ELE_Counselor_Email_ID__c;
                eleList.ELE_Resource_Manger_Email_ID__c = listContact[0].Resource_Managers_Email_ID__c;
                eleList.ELE_Resource_Manger_Name__c = listContact[0].TRS_Resource_Manager__c;
                
                
            }
            
            
            if( (null != listContact) && (listContact.size() > 0) ) {
                sContact = listContact[0].id;
                loggedInContact = listContact[0];
                contactName = loggedInContact.Name;
                lwd = string.valueOf(loggedInContact.WCT_Last_Day_Worked__c);
                if(loggedInContact.WCT_Last_Day_Worked__c != null || lwd !='')
                eleList.ELE_Last_Date__c = loggedInContact.WCT_Last_Day_Worked__c; 
                eleList.ELE_Contact_Email__c= empEmail;
                eleList.ELE_case_Type__c = 'Voluntary';
                eleList.ELE_Date_of_resignation__c= system.today();
                eleList.ELE_Last_Date__c= eleList.ELE_Date_of_resignation__c.addDays(59);     
                // DateTime myDateTime = (DateTime) eleList.ELE_Last_Date__c;
                // String dayOfWeek = myDateTime.format('EEEE');  
                date diffdt=Date.newinstance(1985,6,24);
                
                List<String> listDay = new List<String>{'Monday' , 'Tuesday' , 'Wednesday' , 'Thursday' , 'Friday','Saturday' , 'Sunday'};  
                    date selectedDate = eleList.ELE_Last_Date__c;  
               // system.debug('>>selectedDate >>>>>>>>>>>>>>>>>>>>>>>>'+selectedDate );
                Integer remainder = Math.mod(diffdt.daysBetween(selectedDate) , 7);  
               // system.debug('>>remainder >>>>>>>>>>>>>>>>>>>>>>>>'+remainder );
                
                string dayOfWeek = listDay.get(remainder); 
                
             //   system.debug('>>dayOfWeek >>>>>>>>>>>>>>>>>>>>>>>>'+dayOfWeek);
                
                if(dayOfWeek == 'Saturday'){
                    slwd = eleList.ELE_Last_Date__c.addDays(2);
                }
                else if(dayOfWeek == 'Sunday'){
                    slwd = eleList.ELE_Last_Date__c.addDays(1);
                    invalidEmployee = false;
                }else
                {
                    slwd =eleList.ELE_Last_Date__c;
                }
                eleList.ELE_Last_Date__c = slwd;
            }
            else {
                invalidEmployee = true;
                
            }
            
        }
        else {
            invalidEmployee = true;
        }
    
      
    }
    public PageReference redirect(){ 
        
        if(listContact != null && listContact.size()>0 && listContact[0].ELE_Open_ELE_case__c > 0)
        { 
            
            PageReference pageErrRef = Page.ELE_ExitFormErrorMessagePage;            
            return pageErrRef;                  
            
        }
        else{
            return null;
        }
        
    }
    
 /**************************************************************************************
Method      : saveResignRecord
Type        : non void
Function    : Saves Resignation Form 

*************************************************************************************/
    
    public Pagereference saveResignRecord()
    {
        Boolean bSubmit =false,bSubmitFss = false;
        PageReference pageRef = Page.ELE_ThankyouPage; 
        // pageRef.getParameters().put('em',encryptedEmailString);
      
        if(eleList != null &&  eleList.ELE_Email_id__c != null && eleList.ELE_Address__c != null && eleList.ELE_Mobile_Number__c != null && eleList.ELE_Alternate_Contact_Number__c!=null && eleList.ELE_Date_of_resignation__c != null && eleList.ELE_Last_Date__c != null && eleList.ELE_Location__c != null && eleList.ELE_FSS__c != null && eleList.ELE_Consulted__c != null && eleList.ELE_Reason_for_Resignation__c != null && eleList.ELE_Work_Location__c != null )
        {
            eleList.ELE_Region__c = 'USI';  
            eleList.ELE_Contact__c = sContact;
            if(eleList.ELE_Work_Location__c == 'Onsite Assignment')
            {
                if(eleList.ELE_Onsite_Office_Location__c != null && eleList.ELE_Future_Employer_Name__c != null && eleList.ELE_Future_Company_Start_Date__c != null && eleList.ELE_Future_company_designation__c != null)
                    bSubmit = true;
            }
            else
                bSubmit = true;
            if(eleList.ELE_FSS__c == 'Consulting' || eleList.ELE_FSS__c == 'TAX' || eleList.ELE_FSS__c == 'FAS' ||eleList.ELE_FSS__c == 'AERS')
            {
                if(eleList.ELE_Counselor_Name__c != '' && eleList.ELE_Counselor_Email_ID__c != '' && eleList.ELE_Project_Manager_Name__c != '' && eleList.ELE_Project_Manager_Email_ID__c != '' && eleList.ELE_Resource_Manger_Name__c != '' && eleList.ELE_Resource_Manger_Email_ID__c != '')
                    bSubmitFss = true;
            }
            else if(eleList.ELE_FSS__c == 'Enabling Areas')
            {
                if(eleList.ELE_Counselor_Name__c != '' && eleList.ELE_Counselor_Email_ID__c != '')
                    bSubmitFss = true;
            }
            else if(eleList.ELE_FSS__c == 'Global')
            {
                if(eleList.ELE_Counselor_Name__c != '' && eleList.ELE_Counselor_Email_ID__c != '' && eleList.ELE_Project_Manager_Name__c != '' && eleList.ELE_Project_Manager_Email_ID__c != '' )
                    bSubmitFss = true;
            }
            if(bSubmit && bSubmitFss)
            {
                try
                {
                    if(eleList != null )
                    {
                        system.debug('eleList is ===== ? '+eleList);
                        
                        insert eleList;
                    }
                }
                catch(Exception e)
                { 
                    Exception_Log__c log = WCT_ExceptionUtility.logException('ResignationFormController', 'ResignationForm', e.getMessage());
                    
                    pagereference pg = new pagereference('/apex/GBL_Page_Notification?key=ErrorMsg&expCode='+log.Name);
                   pg.setRedirect(true);
                   return pg;  
            
                } 
                
            }
        }
        if(eleList.id!=null){
         
            attachmentHelper.uploadRelatedAttachment(eleList.Id);
        }
      
        
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
        List <Messaging.SingleEmailMessage> sendemail1 = new List <Messaging.SingleEmailMessage>();
        Contact Con = [select WCT_Function__c,WCT_Company_Code__c, Name,WCT_Person_Id__c , TRS_Resource_Manager__c ,WCT_Service_Area__c, WCT_Service_Line__c, WCT_Office_City_Personnel_Subarea__c,WCT_Job_Level_Text__c , WCT_RC_Code__c from Contact  WHERE Email = :empEmail and RecordType.Name = 'Employee' Limit 1];
        
        String confun = Con.WCT_Function__c;
        String consa = Con.WCT_Service_Area__c;
        String consl = Con.WCT_Service_Line__c;
        String conjl = Con.WCT_Job_Level_Text__c;
        Decimal conrc = Con.WCT_RC_Code__c;
        String starvar = '*';
        String conofc = Con.WCT_Office_City_Personnel_Subarea__c;
        String conname = Con.Name;
        Decimal zero = 0;
        List<ELE_Separation_Leads__c> ESL = new List<ELE_Separation_Leads__c>();
        ESL = [select Leads_email__c,Function__c , Office_City__c, Service_Line__c , Service_Area__c,RC_Code__c,Job_Level__c from ELE_Separation_Leads__c  where (Function__c =: confun and Service_Area__c =: consa and Service_Line__c =:consl and (Job_Level__c =:conjl or Job_Level__c =: starvar) and (RC_Code__c =: zero or RC_Code__c =: conrc) and (Office_City__c =: conofc or Office_City__c =: starvar))];
        System.debug('Test' +ESL);
        if(ESL.size()>0)
        {
            
            List <String> EAddress = new List<String>();
            String eaddressval;
            eaddressval = ESL[0].Leads_email__c;
            System.debug('@@@@@@@@@ :' +eaddressval);
            Eaddress = eaddressval.split(',');
            // System.debug('**********top Eaddress IS : '+Eaddress);
            // system.debug('CONTACT JOB LEVEL IS :'+Con.WCT_Job_Level_Text__c);
            // system.debug('CONTACT SA IS :'+Con.WCT_Service_Area__c);
            //  system.debug('CONTACT SL IS :'+Con.WCT_Service_Line__c);
            // system.debug('CONTACT FUNCTION IS :'+Con.WCT_Function__c);
            // system.debug('ELE LEADS JOB LEVEL IS :'+ESL[0].Job_Level__c);
            //  system.debug('ELE LEADS SA IS :'+ESL[0].Service_Area__c);
            // system.debug('ELE LEADS SL IS :'+ESL[0].Service_Line__c);
            //   system.debug('ELE LEADS FUNCTION IS :'+ESL[0].Function__c);
            // system.debug('JOB LEVEL IS :'+ESL[0].Job_Level__c);
            //  system.debug('Sample Test');
            //  System.debug('**********Eaddress IS : '+Eaddress);
            
            date dt = eleList.ELE_Last_Date__c;
            semail.setToAddresses(Eaddress);
            semail.setSubject(+ Con.WCT_Function__c +'- Employee Resignation Intimation - '+ Con.WCT_Person_Id__c + ' - ' + Con.Name);
            semail.setHtmlBody('<img src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000001U26Z&oid=00D40000000MxFD&lastMod=1406216483000"> </img> <br/><br/> Hi All, <br> <p>This is to inform you that <b>' + Con.Name +', Personnel Number: ' + Con.WCT_Person_Id__c+ '</b>  has expressed his/her intention to resign from the services of the company. <br/><br/> <b> Date of Resignation :</b>'+ eleList.ELE_Date_of_resignation__c + ' <br/><br/><b> Location : </b> ' + eleList.ELE_Location__c +'<br/><br/> <b> Entity Name : </b>' +Con.WCT_Company_Code__c+ '<br/><br/><b> Service Area : </b> '+ con.WCT_Service_Area__c + '<br/><br/><b> Service Line : </b>'+ Con.WCT_Service_Line__c+' <br/> <br/> Please note, this email is auto generated and for your information only.<br/><br/> <br/> Thanks and Regards, <br/>  <font face="Verdana" color="#1F497D"> USI Separations Team  <br/> <br/> </font> <a class="western" href="https://www.deloitte.com/us"> <font face="Arial,Sans-Serif" style="font-size: 8pt;"><b>Deloitte.com </b> </font></a>| <font face="Arial,Sans-Serif" style="font-size: 8pt;"><b> <a class="western" href="http://www.deloitte.com/us/security">Security </a> </b> </font>|<a class="western" href="http://www.deloitte.com/us/legal">  <font face="Arial,Sans-Serif" style="font-size: 8pt;"><b>Legal</b></font> </a>| <font face="Arial,Sans-Serif" style="font-size: 8pt;"><b> <a class="western" href="http://www.deloitte.com/us/privacy">Privacy </a> </b> </font> ');
            sendemail1.add(semail);
            Messaging.sendEmail(sendemail1);  
        }         
        return pageRef;  
        
    }
  
}