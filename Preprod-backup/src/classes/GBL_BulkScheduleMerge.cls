public class GBL_BulkScheduleMerge {
    
    
    public Blob contentFile{get;set;}
    public Integer noOfRows{get;set;}
    public Integer noOfRowsProcessed{get;set;}
    public String nameFile{get;set;}    
    
    public String MERGE_CONFIG_NAME_BULK='BulkRestricted';   
    public String MERGE_CONFIG_NAME_AC_ONLY='BulkACOnly';   
    public String MERGE_CONFIG_NAME_FY_ONLY='BulkFYOnly'; 
    public String MERGE_CONFIG_NAME_AC_FY='Bulk'; 
    
    public List<ErrorItem> errorRecords{get; set;}
    public List<ErrorItem> topErrors{get{
        List<ErrorItem> limitedErrors=new List<ErrorItem>();
        if(errorRecords!=null)
        {
            for(integer i=0;i<errorRecords.size() && i<MAX_RECORD_TO_VIEW; i++)
            {
                limitedErrors.add(errorRecords[i]);
            }
        }
        return    limitedErrors;     
    }}
    public boolean isFirstCall{get; set;} 
    public boolean isMoreRecord{get; set;}
    public boolean isError{get; set;}
    
    
    static integer MAX_RECORD_TO_PROCESS=10000;
    static integer MAX_RECORD_TO_VIEW=1000;
    
    WCT_parseCSV parseCSVInstance = new WCT_parseCSV();
    transient List<List<String>> filelines;
    List<WCT_sObject_Staging_Records__c> listToUpdate=new List<WCT_sObject_Staging_Records__c>();
    
    
    public GBL_BulkScheduleMerge()
    {
        resetValues();
        isFirstCall=true;
        
    }
    public void resetValues()
    {
        noOfRows=0;
        noOfRowsProcessed=0;
        filelines= new List<List<String>>();
        errorRecords= new List<ErrorItem>();
        listToUpdate= new List<WCT_sObject_Staging_Records__c>();
        isFirstCall=false;
        isMoreRecord=false;
        isError=false;
    }
    public Pagereference readFile()
    {
      resetValues();
      isMoreRecord=false;
      try
       {
        	String tempString=contentFile.toString();
            filelines = parseCSVInstance.parseCSV(tempString, true);
            system.debug('111111111'+filelines);
            noOfRows=fileLines.size();
            contentFile=Blob.valueOf('');
            tempString='';
            if(filelines.size()<MAX_RECORD_TO_PROCESS)
            {
                /*Parse through all the records from file, make identifier list and the Map of all uploaded records with key as identifier. */
              ProcessRequest(filelines);
            }
           else
           {
                isError=true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Cannot upload more than one '+MAX_RECORD_TO_PROCESS+' records.');
                ApexPages.addMessage(errormsg);    
           }
       }
       Catch(System.Exception stringException)
       {
           System.debug(''+stringException);
            isError=true;
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,String.valueof(stringException));
            ApexPages.addMessage(errormsg);    
       }
        
       	System.debug('About to inserted records '+listToUpdate.size());
        if(listToUpdate.size()>0 && !isError)
        {
			insert listToUpdate;
            System.debug('inserted records '+listToUpdate.size());
            noOfRowsProcessed=listToUpdate.size();
            /*Callimg Batch to handle this */
            GBL_BulkMergeBatch batch = new GBL_BulkMergeBatch(listToUpdate);
            Database.executeBatch(batch,5);  
        }

            
        system.debug('111111111 errors:'+errorRecords);
        
        return null;
    }
    
      
     public void ProcessRequest(List<List<String>> filelines)
     {
         
            for(integer index=0;index<filelines.size()  ; index++)
            {  
                string error='';
                List<String> fields =filelines[index];
                //system.debug('### fileds Size '+fields.size());
                
                WCT_sObject_Staging_Records__c tempSObject = new WCT_sObject_Staging_Records__c();
               
                	String temp=fields.size()>0?fields[0].trim():null;
                        if(temp!='' && temp!=null)
                        {
                            tempSObject.MR_Object_Type__c=temp;
                            
                        }
                        else
                        {
                            error='Object Type is required ;';
                        }
                                    
                    temp=fields.size()>1?fields[1].trim():null;
                    if(temp!='' && temp!=null)
                        {
                            tempSObject.MR_Source_Record_Name__c=temp;
                        }
                        else
                        {
                            error+='Source record name is required; ';
                        }
                
                	temp=fields.size()>2?fields[2].trim():null;
                    if(temp!='' && temp!=null)
                        {
                            tempSObject.MR_Source_Record_Id__c=temp;
                            
                        }
                        else
                        {
                            error+='Source Id is required; ';
                        }
                     temp=fields.size()>3?fields[3].trim():null;
                    if(temp!='' && temp!=null)
                        {
                            tempSObject.MR_Source_Load_Source__c =temp;
                        }
                        else
                        {
                            error+='Destination record name is required ;';
                        }
                
                   temp=fields.size()>4?fields[4].trim():null;
                    if(temp!='' && temp!=null)
                        {
                            tempSObject.MR_Destination_Record_Name__c =temp;
                        }
                        else
                        {
                            error+='Destination record name is required ;';
                        }
               
                
                    
                temp=fields.size()>5?fields[5].trim():null;
                    if(temp!='' && temp!=null)
                        {
                            tempSObject.MR_Destination_Record_Id__c=temp;
                            
                        }
                        else
                        {
                            error+='Destination Id is required; ';
                        }
                temp=fields.size()>6?fields[6].trim():null;
                    if(temp!='' && temp!=null)
                        {
                            tempSObject.MR_Destination_Load_Source__c=temp;
                            
                        }
                        else
                        {
                            error+='Destination Id is required; ';
                        }
                
                /*Adding the Move AC Fields*/
                boolean IsMoveACFields;
                boolean IsMoveFYFields;
                
                temp=fields.size()>7?fields[7].trim():null;
                    if(temp!=null && temp!='' )
                        {
                            IsMoveACFields=(temp=='Yes'?true:false);
                        }
              		else
                        {
                            error+='Move AC Fields invalid value; ';
                        }
                
                /*Adding the Move FY Fields*/
                temp=fields.size()>8?fields[8].trim():null;
                    if(temp!=null && temp!='' )
                        {
                            IsMoveFYFields=(temp=='Yes'?true:false);
                        }
              		else
                        {
                            error+='Move FY Fields invalid value; ';
                        }
                    
				if(error=='')    
                {
                   error=tempSObject.MR_Destination_Record_Id__c!=tempSObject.MR_Source_Record_Id__c?'':'Source and Destination Id cannot be same';
                }
                
                if(error=='')
                {
                    tempSObject.MR_Merge_Uploaded_File_Name__c=nameFile;
                    tempSObject.MR_Merged_Status__c='Not Started';
                    tempSObject.RecordTypeId =Label.Merge_Record_Stagging_Record_Type_ID;
                    
                    if(IsMoveACFields==true && IsMoveFYFields==true )
                    {
                                              
                        tempSObject.MR_Merge_Config_Name__c=MERGE_CONFIG_NAME_AC_FY;
                    }
                    else if(IsMoveACFields==true && IsMoveFYFields==false )
                    {
                        tempSObject.MR_Merge_Config_Name__c=MERGE_CONFIG_NAME_AC_ONLY;
                    }
                    else if(IsMoveACFields==false && IsMoveFYFields==true )
                    {
                        tempSObject.MR_Merge_Config_Name__c=MERGE_CONFIG_NAME_FY_ONLY;
                    }
                    else
                    {
                        tempSObject.MR_Merge_Config_Name__c=MERGE_CONFIG_NAME_BULK;
                    }
                    
                    
                    
                    
                    listToUpdate.add(tempSObject);
                   /* allUploadedRecords.put(fields[0], tempRequest);
                    identifiers.add(fields[0]);
					*/
                }
                else
                {
                    errorRecords.add(new ErrorItem(tempSObject, error));
                }
           }
     } 
     
      
      
      
    public class ErrorItem
    {
        public WCT_sObject_Staging_Records__c inValidRequest{get; set;}
        public String errorMessage{get; set;}
        public string identifier{get; set;}
        public  ErrorItem(WCT_sObject_Staging_Records__c errorRequest, String status)
        {
           inValidRequest=errorRequest;
           errorMessage=status;
           
        }
      
    }
    
  
}