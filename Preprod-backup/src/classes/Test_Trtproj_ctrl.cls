/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_Trtproj_ctrl {

    static testMethod void myUnitTest() {
        
        //inserting sample test user
         UserRole r=[select id from UserRole where DeveloperName='WCT_Admin'];
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            User u = new User(Alias = 'sample12', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,UserRoleId=r.id,IsActive=true,
            TimeZoneSidKey='America/Los_Angeles', UserName='sample12@testorg.com');
            insert u; 
        
       
        
         
        system.runas(u){  
             // TO DO: implement unit test
        Contact con=WCT_UtilTestDataCreation.createContact();
         con.email='standarduser@testorg.com';       
         insert con; 
       
       //  String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
        // System.currentPageReference().getParameters().put('em', strEncryptEmail);
         System.currentPageReference().getParameters().put('ProcessArea', 'Miscellaneous');
         trtproj_ctrl trtPage = new trtproj_ctrl();
         trtpage.useremail=con.email;
         trtPage.requesttype = 'req';
         trtPage.inc = 'inc';
         Blob b = Blob.valueOf('Test Data');  
         trtPage.attachment.Body = b;
         trtPage.attachment.Name = 'Test Attachment';
         trtPage.getProcessArea();
         trtPage.Save();
         trtPage.Clear();
         System.currentPageReference().getParameters().put('IsCreated', 'true');
         trtPage.Preinit();
         
         trtproj_ctrl trtPage2 = new trtproj_ctrl();
         trtPage2.attachment.Name = 'Test Attachment1';
         trtPage2.Save();
         
       //  String strEncryptEmail1 = EncodingUtil.urlDecode(CryptoHelper.encrypt('testdeloitte@deloitte.com'), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
       //  System.currentPageReference().getParameters().put('em', strEncryptEmail1);
         trtproj_ctrl trtPage1 = new trtproj_ctrl();
         trtPage1.attachment.Name = 'Test Attachment1';
         trtPage1.Save();
        }

    }
    
        static testMethod void myUnitTest1() {
            //inserting sample test user
         UserRole r=[select id from UserRole where DeveloperName='WCT_Admin'];
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            User u = new User(Alias = 'sample12', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,UserRoleId=r.id,IsActive=true,
            TimeZoneSidKey='America/Los_Angeles', UserName='sample12@testorg.com');
            insert u; 
        
            system.runas(u){ 
        // TO DO: implement unit test
         Contact con=WCT_UtilTestDataCreation.createContact();
         con.email='standarduser@testorg.com';       
         insert con;
         
         String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
         System.currentPageReference().getParameters().put('em', strEncryptEmail);
         //System.currentPageReference().getParameters().put('ProcessArea', 'Miscellaneous');
         System.currentPageReference().getParameters().put('Param1', 'Analytics');
         System.currentPageReference().getParameters().put('EfficiencyCategory', 'Contractor Fee');
         trtproj_ctrl trtPage = new trtproj_ctrl();
         trtPage.requesttype = 'req';
         trtPage.inc = 'inc';
         Blob b = Blob.valueOf('Test Data');  
         trtPage.attachment.Body = b;
         trtPage.attachment.Name = 'Test Attachment';
         trtPage.getProcessArea();
         trtPage.Save();
         trtPage.Clear();
         System.currentPageReference().getParameters().put('IsCreated', 'true');
         trtPage.Preinit();
         
         trtproj_ctrl trtPage2 = new trtproj_ctrl();
         trtPage2.attachment.Name = 'Test Attachment1';
         trtPage2.Save();
         
       //  String strEncryptEmail1 = EncodingUtil.urlDecode(CryptoHelper.encrypt('testdeloitte@deloitte.com'), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
       //  System.currentPageReference().getParameters().put('em', strEncryptEmail1);
         trtproj_ctrl trtPage1 = new trtproj_ctrl();
         trtPage1.attachment.Name = 'Test Attachment1';
         trtPage1.Save();
            }  
    }
    static testMethod void myUnitTest2() {
        //inserting sample test user
         UserRole r=[select id from UserRole where DeveloperName='WCT_Admin'];
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            User u = new User(Alias = 'sample12', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,UserRoleId=r.id,IsActive=true,
            TimeZoneSidKey='America/Los_Angeles', UserName='sample12@testorg.com');
            insert u; 
        system.runas(u){
        // TO DO: implement unit test
         Contact con=WCT_UtilTestDataCreation.createContact();
         con.email='standarduser@testorg.com';       
         insert con;
         
        // String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
       //  System.currentPageReference().getParameters().put('em', strEncryptEmail);
         //System.currentPageReference().getParameters().put('ProcessArea', 'Miscellaneous');
         System.currentPageReference().getParameters().put('Param1', 'Srvcarealed');
         System.currentPageReference().getParameters().put('EfficiencyCategory', 'Contractor Fee');
        
         trtproj_ctrl trtPage = new trtproj_ctrl();
         trtPage.requesttype = 'req';
         trtPage.inc = 'inc';
         Blob b = Blob.valueOf('Test Data');  
         trtPage.attachment.Body = b;
         trtPage.attachment.Name = 'Test Attachment';
         trtPage.getEfficiencyCategory();
         trtPage.Save();
         trtPage.Clear();
         System.currentPageReference().getParameters().put('IsCreated', 'true');
         trtPage.Preinit();
         
         trtproj_ctrl trtPage2 = new trtproj_ctrl();
         trtPage2.attachment.Name = 'Test Attachment1';
         trtPage2.Save();
         
        // String strEncryptEmail1 = EncodingUtil.urlDecode(CryptoHelper.encrypt('testdeloitte@deloitte.com'), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
        // System.currentPageReference().getParameters().put('em', strEncryptEmail1);
         trtproj_ctrl trtPage1 = new trtproj_ctrl();
         trtPage1.attachment.Name = 'Test Attachment1';
         trtPage1.Save();
        
         System.currentPageReference().getParameters().put('IsCreated', 'true');
        }  
         
    }
    static testMethod void myUnitTest3() {
        //inserting sample test user
         UserRole r=[select id from UserRole where DeveloperName='WCT_Admin'];
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            User u = new User(Alias = 'sample12', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,UserRoleId=r.id,IsActive=true,
            TimeZoneSidKey='America/Los_Angeles', UserName='sample12@testorg.com');
            insert u; 
        system.runas(u){
        // TO DO: implement unit test
         Contact con=WCT_UtilTestDataCreation.createContact();
         con.email='standarduser@testorg.com';       
         insert con;
         
        // String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
       //  System.currentPageReference().getParameters().put('em', strEncryptEmail);
         //System.currentPageReference().getParameters().put('ProcessArea', 'Miscellaneous');
         System.currentPageReference().getParameters().put('Param1', 'Srvcarealed');
         System.currentPageReference().getParameters().put('EfficiencyCategory', 'Overtime Reduction');
        
         trtproj_ctrl trtPage = new trtproj_ctrl();
         trtPage.requesttype = 'req';
         trtPage.inc = 'inc';
         Blob b = Blob.valueOf('Test Data');  
         trtPage.attachment.Body = b;
         trtPage.attachment.Name = 'Test Attachment';
         trtPage.getEfficiencyCategory();
         trtPage.Save();
         trtPage.Clear();
         System.currentPageReference().getParameters().put('IsCreated', 'true');
         trtPage.Preinit();
         
         trtproj_ctrl trtPage2 = new trtproj_ctrl();
         trtPage2.attachment.Name = 'Test Attachment1';
         trtPage2.Save();
         
        // String strEncryptEmail1 = EncodingUtil.urlDecode(CryptoHelper.encrypt('testdeloitte@deloitte.com'), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
        // System.currentPageReference().getParameters().put('em', strEncryptEmail1);
         trtproj_ctrl trtPage1 = new trtproj_ctrl();
         trtPage1.attachment.Name = 'Test Attachment1';
         trtPage1.Save();
        
         System.currentPageReference().getParameters().put('IsCreated', 'true');
        }   
         
    }
     static testMethod void myUnitTest4() {
         //inserting sample test user
         UserRole r=[select id from UserRole where DeveloperName='WCT_Admin'];
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            User u = new User(Alias = 'sample12', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,UserRoleId=r.id,IsActive=true,
            TimeZoneSidKey='America/Los_Angeles', UserName='sample12@testorg.com');
            insert u; 
         system.runas(u){
        // TO DO: implement unit test
         Contact con=WCT_UtilTestDataCreation.createContact();
         con.email='standarduser@testorg.com';       
         insert con;
         
        // String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
       //  System.currentPageReference().getParameters().put('em', strEncryptEmail);
         //System.currentPageReference().getParameters().put('ProcessArea', 'Miscellaneous');
         System.currentPageReference().getParameters().put('Param1', 'Srvcarealed');
         System.currentPageReference().getParameters().put('EfficiencyCategory', 'P&A');
        
         trtproj_ctrl trtPage = new trtproj_ctrl();
         trtPage.requesttype = 'req';
         trtPage.inc = 'inc';
         Blob b = Blob.valueOf('Test Data');  
         trtPage.attachment.Body = b;
         trtPage.attachment.Name = 'Test Attachment';
         trtPage.getEfficiencyCategory();
         trtPage.Save();
         trtPage.Clear();
         System.currentPageReference().getParameters().put('IsCreated', 'true');
         trtPage.Preinit();
         
         trtproj_ctrl trtPage2 = new trtproj_ctrl();
         trtPage2.attachment.Name = 'Test Attachment1';
         trtPage2.Save();
         
        // String strEncryptEmail1 = EncodingUtil.urlDecode(CryptoHelper.encrypt('testdeloitte@deloitte.com'), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
        // System.currentPageReference().getParameters().put('em', strEncryptEmail1);
         trtproj_ctrl trtPage1 = new trtproj_ctrl();
         trtPage1.attachment.Name = 'Test Attachment1';
         trtPage1.Save();
        
         System.currentPageReference().getParameters().put('IsCreated', 'true');
         }  
         
    }
    static testMethod void myUnitTest5() {
        //inserting sample test user
         UserRole r=[select id from UserRole where DeveloperName='WCT_Admin'];
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            User u = new User(Alias = 'sample12', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,UserRoleId=r.id,IsActive=true,
            TimeZoneSidKey='America/Los_Angeles', UserName='sample12@testorg.com');
            insert u; 
        system.runas(u){
        // TO DO: implement unit test
         Contact con=WCT_UtilTestDataCreation.createContact();
         con.email='standarduser@testorg.com';       
         insert con;
         
        // String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
       //  System.currentPageReference().getParameters().put('em', strEncryptEmail);
         //System.currentPageReference().getParameters().put('ProcessArea', 'Miscellaneous');
         System.currentPageReference().getParameters().put('Param1', 'Srvcarealed');
         System.currentPageReference().getParameters().put('EfficiencyCategory', 'Quality Improvement');
        
         trtproj_ctrl trtPage = new trtproj_ctrl();
         trtPage.requesttype = 'req';
         trtPage.inc = 'inc';
         Blob b = Blob.valueOf('Test Data');  
         trtPage.attachment.Body = b;
         trtPage.attachment.Name = 'Test Attachment';
         trtPage.getEfficiencyCategory();
         trtPage.Save();
         trtPage.Clear();
         System.currentPageReference().getParameters().put('IsCreated', 'true');
         trtPage.Preinit();
         
         trtproj_ctrl trtPage2 = new trtproj_ctrl();
         trtPage2.attachment.Name = 'Test Attachment1';
         trtPage2.Save();
         
        // String strEncryptEmail1 = EncodingUtil.urlDecode(CryptoHelper.encrypt('testdeloitte@deloitte.com'), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
        // System.currentPageReference().getParameters().put('em', strEncryptEmail1);
         trtproj_ctrl trtPage1 = new trtproj_ctrl();
         trtPage1.attachment.Name = 'Test Attachment1';
         trtPage1.Save();
        
         System.currentPageReference().getParameters().put('IsCreated', 'true');
         
        }         
    }
    static testMethod void myUnitTest6() {
        //inserting sample test user
         UserRole r=[select id from UserRole where DeveloperName='WCT_Admin'];
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            User u = new User(Alias = 'sample12', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,UserRoleId=r.id,IsActive=true,
            TimeZoneSidKey='America/Los_Angeles', UserName='sample12@testorg.com');
            insert u; 
        system.runas(u){
        // TO DO: implement unit test
         Contact con=WCT_UtilTestDataCreation.createContact();
         con.email='standarduser@testorg.com';       
         insert con;
         
        // String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
       //  System.currentPageReference().getParameters().put('em', strEncryptEmail);
         //System.currentPageReference().getParameters().put('ProcessArea', 'Miscellaneous');
         System.currentPageReference().getParameters().put('Param1', 'Srvcarealed');
         System.currentPageReference().getParameters().put('EfficiencyCategory', '');
        
         trtproj_ctrl trtPage = new trtproj_ctrl();
         trtPage.requesttype = 'req';
         trtPage.inc = 'inc';
         Blob b = Blob.valueOf('Test Data');  
         trtPage.attachment.Body = b;
         trtPage.attachment.Name = 'Test Attachment';
         trtPage.getEfficiencyCategory();
         trtPage.Save();
         trtPage.Clear();
         System.currentPageReference().getParameters().put('IsCreated', 'true');
         trtPage.Preinit();
         
         trtproj_ctrl trtPage2 = new trtproj_ctrl();
         trtPage2.attachment.Name = 'Test Attachment1';
         trtPage2.Save();
         
        // String strEncryptEmail1 = EncodingUtil.urlDecode(CryptoHelper.encrypt('testdeloitte@deloitte.com'), 'UTF-8');
         Test.setCurrentPageReference(new PageReference('trtproject')); 
        // System.currentPageReference().getParameters().put('em', strEncryptEmail1);
         trtproj_ctrl trtPage1 = new trtproj_ctrl();
          PageReference objPageRef = new PageReference('/apex/trtproject');
            test.setCurrentPage(objPageRef);
         System.currentPageReference().getParameters().put('IsCreated', 'true');
         
        }         
    }
}