/*****************************************************************************************
    Name    : DependentController 
    Desc    : This class is used to display emergency and dependent contact on the contact detail page
              by inline visualforce page
    Approach: 
                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Ashish Shishodiya               26 July, 2013         Created 
Sreenivasa Munnangi               4 Sept, 2013           Modified

******************************************************************************************/


public with sharing class DependentController {
 
    //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
    private Contact c;
    // the results from the search. do not init the results or a blank rows show up initially on page load
    public List<Contact> dependentContacts {get;set;}
    public List<Contact> emergencyContacts {get;set;}
    public List<Contact> listOfDepContacts{get;set;}
    public List<Contact> listOfEmContacts{get;set;}
    public boolean dependantFlag{get;set;}
    public boolean emergencyFlag{get;set;}
        
    private RecordType rDependant = [Select r.Name From RecordType r where name ='Dependant'];
    private RecordType rEmergency = [select name from recordtype where name = 'Emergency Contact'];
    public DependentController (ApexPages.StandardController controller) {
 
        //initialize the stanrdard controller
        this.controller = controller;
        this.c = (Contact)controller.getRecord();
        findDependentContacts();
        dependantFlag =(listOfDepContacts.size()!= 0)?true:false;
        findEmergencyContacts();
        emergencyFlag=(listOfEmContacts.size()!= 0)?true:false;
        system.debug('emergencyFlag = '+emergencyFlag+'dependantFlag'+dependantFlag);
    }
 
    public void findDependentContacts() {
       
       listOfDepContacts = [Select c.WCT_SSN__c, c.Phone,WCT_Dependant_Type__c, c.Name, birthdate, c.LastName, WCT_Type__c, WCT_Relationship__c,WCT_Related_Contact_Status__c From Contact c where  c.RecordTypeId =: rDependant.id and c.WCT_Primary_Contact__c =: c.id order by WCT_Related_Contact_Status__c];
     //  system.debug('???????????? rDependant.id '+rDependant.id );
      //  system.debug('??????Dependent Contact size'+listOfDepContacts.size());       
    
    }
    
    public void findEmergencyContacts() {
        listOfEmContacts = [Select c.Phone, c.Name, c.LastName,WCT_Relationship__c,WCT_Related_Contact_Status__c From Contact c where  c.RecordTypeId =: rEmergency.id and c.WCT_Primary_Contact__c =: c.id order by WCT_Related_Contact_Status__c];
    //    system.debug('????????????rEmergency.id '+rEmergency.id);
     //   system.debug('??????Emergency Contact size'+listOfEmContacts.size());
    }
 
}