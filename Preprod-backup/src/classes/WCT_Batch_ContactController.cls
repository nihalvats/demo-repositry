global class WCT_Batch_ContactController implements Database.Batchable<sObject>,Schedulable, Database.Stateful{

     global void execute(SchedulableContext SC) {
        WCT_Batch_ContactController batch = new WCT_Batch_ContactController();
            ID batchprocessid = Database.executeBatch(batch,100); 
    }

     public List<Contact> lstContacts = new List<Contact>();
     global Database.QueryLocator start(Database.BatchableContext BC) {
    /// SOQL Query
    /// SELECT id,WCT_Related_Contact_Status__c    FROM  Contact  Where  WCT_Primary_Contact__r.WCT_Contact_Type__c = 'Employee' and  WCT_Primary_Contact__r.WCT_Employee_Sub_Group__c='Regular Salaried' and  WCT_Related_Contact_Status__c != 'Inactive' and  Wct_relationship__c = 'Agency'
    ///      
       String query = 'SELECT ' +
                            'id,WCT_Related_Contact_Status__c ' +
                        'FROM ' +
                            ' Contact ' +
                            ' Where ' + 
                            ' WCT_Primary_Contact__r.WCT_Contact_Type__c = \'Employee\' and ' +
                           ' WCT_Primary_Contact__r.WCT_Employee_Sub_Group__c=\'Regular Salaried\' and ' +
                           ' WCT_Related_Contact_Status__c != \'Inactive\' and ' + 
                           ' RecordType.Name = \'Emergency Contact\' and ' + 
                           ' Wct_relationship__c = \'Agency\''; 
             system.debug('test query: '+query);               
      return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Contact> listContactRecords) { 
        for(Contact ContactRecord : listContactRecords) {
                System.debug(' Contact Record : ' + ContactRecord.Id + ' - ' + ContactRecord.Name);
                ContactRecord.WCT_Related_Contact_Status__c = 'Inactive';
                lstContacts.add(ContactRecord);
           }
            update lstContacts;
    } 
    global void finish(Database.BatchableContext BC){    
     //   system.debug('Contact Records Processed : '  + lstContacts.size()); 
     WCT_Util.sendMailToLoginUser('Emergency Contact Inactivate Job Ran Successfully', 'Emergency Contact Inactivate Job Ran Successfully On - '+Datetime.Now());
    }
    
      
}