/*
Class name : WCT_GroupInterviewClone_CTRL
Description : This class is controller for cloning an Interview.

     Date            Modified By                         Description
   ---------       ---------------------               ----------------------
               
                                                            
*/
public class WCT_GroupInterviewClone_CTRL{

    //Variable Declaration
    public WCT_Interview__c intvToBeCreated {get;set;}
    public WCT_Interview__c intvToClone {get;set;}
    public List<WCT_Interview_Junction__c> IntvTrackersToBeCreated {get;set;}
    public set<Id>  existingCadidates {get;set;}
    
    //Adding variables for including extra Candidates
    Public Static final String NEW_STATUS='New';
    Public Static final String OPEN_STATUS='Open';
    Public Static final String INPROGRESS_STATUS='In Progress';
    public String contactName {get;set;}
    public String contactPhone{get;set;}
    public String contactEmail{get;set;}
    public String userName {get;set;}
    public String Email {get;set;}
    public String reqNum{get;set;}
    public String rmsID{get;set;}
    public String soql {get;set;}
    public List<WCT_Candidate_Requisition__c> ctList {get;set;}
    public List<WCT_Candidate_Requisition__c> ctSelectedList {get;set;}
    public boolean isAddError {get;set;}
    
    //Variables for Event
    public Event eventToCreate {get;set;}
    public DateTime startDate,EndDate ;
    public List<EventRelation> eventRelationsToBeInserted {get;set;}
    public List<Id> inviteeIds{get;set;}
    
    public list<InviteeWrapper> availableInvitees {get;set;}
    public list<InviteeWrapper> selectedInvitees {get;set;}
    public boolean UserError {get;set;}
    
    //Variables for Interview Search
    public string intvNumber {get;set;}
    public string intvOwner {get;set;}
    public WCT_Interview__c tempIntvForSearch {get;set;}
    public List<addIntvWrapper> addIntvWrapperList {get;set;}
    public boolean pclone {get;set;}
    
    //Wrapper class
    public class InviteeWrapper{
        public boolean isSelected {get;set;}
        public string userId {get;set;}
        public User usr {get;set;}
        
        public InviteeWrapper(user us){
            usr = us;
            userId = us.id;
            isSelected = false;
        }   
    
    }
    
    public class addIntvWrapper{
        public boolean isSelected {get;set;}
        public WCT_Interview__c intv {get;set;}
        
        public addIntvWrapper(WCT_Interview__c iv){
            intv = iv;
            isSelected = false;
        }   
    }    
    
    //Constructor
    public WCT_GroupInterviewClone_CTRL(){
        addIntvWrapperList = new List<addIntvWrapper>();
        tempIntvForSearch  = new WCT_Interview__c();
        pclone = false;
        UserError = false;
        intvToClone = new WCT_Interview__c();
        intvToBeCreated = new WCT_Interview__c();
        existingCadidates = new set<id>();
        ctList =new List<WCT_Candidate_Requisition__c>();
        ctSelectedList =new List<WCT_Candidate_Requisition__c>();
        availableInvitees = new list<InviteeWrapper>();
        selectedInvitees = new list<InviteeWrapper>();
        String selects = getCreatableFieldsSOQL('WCT_Interview__c');
        Id intId = ApexPages.currentPage().getParameters().get('id');
        
        for(user us:[SELECT id,Username,name, Email from USER where isActive = true LIMIT 200]){
            availableInvitees.add(new InviteeWrapper(us));
        }
        //Event Related
        Event existingEvent = new Event();
        eventToCreate = new Event();
        inviteeIds = new List<Id>();
        try{
        existingEvent = [SELECT id,Description,EndDateTime,StartDateTime,Subject,Location FROM Event where whatId=:intId AND IsChild=false];
        }catch(Exception ex){}
        if(existingEvent != null){
        for(EventRelation ev:[SELECT id,RelationId,EventId from EventRelation where EventId=:existingEvent.Id AND isInvitee = true AND isParent = false]){
            inviteeIds.add(ev.RelationId);
        }
        if(!inviteeIds.isEmpty()){
            for(USER us:[SELECT id,Name,userName,Email FROM USER where id IN :inviteeIds AND isActive = true ]){
                selectedInvitees.add(new InviteeWrapper(us));
            }
        }
        
        eventToCreate = existingEvent.clone(false,true);
        eventToCreate.ownerId = userInfo.getUserId();
        }
        //End of event Related
        
        String query = 'SELECT id,Name,'+selects+', (SELECT id,WCT_Candidate_Tracker__c,WCT_Interviewer_Recommendation__c,WCT_Requisition_ID__c from Interview_Candidate_Tracker_Junction__r) FROM WCT_Interview__c where id = \''+intId + '\' LIMIT 1';
        //WCT_Interview__c intvToClone = [SELECT selects,(SELECT id,WCT_Candidate_Tracker__c,WCT_Requisition_ID__c from Interview_Candidate_Tracker_Junction__r)
        //                                 FROM WCT_Interview__c where id = :ApexPages.currentPage().getParameters().get('id')];
        intvToClone = database.Query(query);
        
        intvToBeCreated = intvToClone.clone(false,true);
        intvToBeCreated.WCT_Interview_Start_Time__c = null;
        intvToBeCreated.WCT_Interview_End_Time__c = null;
        intvToBeCreated.WCT_First_Event_Finalized_start_Time__c = null;
        intvToBeCreated.WCT_First_Event_Location__c = '';
        intvToBeCreated.WCT_Email_Body__c = '';
        intvToBeCreated.WCT_Interview_Status__c = 'Open';
        intvToBeCreated.ownerId = userInfo.getUserId();
        doSearch();
        for(WCT_Interview_Junction__c  ij : intvToClone.Interview_Candidate_Tracker_Junction__r){
            if(ij.WCT_Interviewer_Recommendation__c <> null && (ij.WCT_Interviewer_Recommendation__c == 'Select' || ij.WCT_Interviewer_Recommendation__c == 'Tentative')){
                existingCadidates.add(ij.WCT_Candidate_Tracker__c);
            }
            
        }
        if(!existingCadidates.isEmpty()){
            for(WCT_Candidate_Requisition__c ct : [select WCT_Contact__r.Name, WCT_Requisition__r.Name ,WCT_Select_Candidate_For_Interview__c 
                                                    from WCT_Candidate_Requisition__c where id in : existingCadidates]){
                ctSelectedList.add(ct);
            }
        }       
    }
    
    public static string getCreatableFieldsSOQL(String objectName){
         
        String selects = '';
         // Get a map of field name and field token
        Map<String, Schema.SObjectField> fMap = Schema.getGlobalDescribe().get(objectName.toLowerCase()).getDescribe().Fields.getMap();
        list<string> selectFields = new list<string>();
         
        if (fMap != null){
            for (Schema.SObjectField ft : fMap.values()){ // loop through all field tokens (ft)
                Schema.DescribeFieldResult fd = ft.getDescribe(); // describe each field (fd)
                if (fd.isCreateable()){ // field is creatable
                    selectFields.add(fd.getName());
                }
            }
        }
         
        if (!selectFields.isEmpty()){
            for (string s:selectFields){
                selects += s + ',';
            }
            if (selects.endsWith(',')){selects = selects.substring(0,selects.lastIndexOf(','));}
             
        }
         
        return  selects ;
    }
    
    //-----------------------------------
   //    Search Candidate Tracker
   //-----------------------------------
   public PageReference doSearch() {
   
      soql = 'SELECT id, Name, WCT_Contact__r.Name,WCT_Contact__r.FirstName,WCT_Contact__r.LastName,WCT_Requisition__c, WCT_Requisition__r.Name,WCT_Select_Candidate_For_Interview__c '+
                                'from WCT_Candidate_Requisition__c WHERE WCT_Candidate_Requisition_Status__c IN (\''+NEW_STATUS+'\',\''+INPROGRESS_STATUS+'\')';
                                 
      if (contactName!= null && !contactName.equals('')){
          soql += ' AND (WCT_Contact__r.FirstName LIKE \''+String.escapeSingleQuotes(contactName)+'%\' ';
          soql += ' OR WCT_Contact__r.Name LIKE \''+String.escapeSingleQuotes(contactName)+'%\' ';
          soql += ' OR WCT_Contact__r.LastName LIKE \''+String.escapeSingleQuotes(contactName)+'%\') ';
      }
      if(contactPhone!= null && contactPhone!=''){
          soql += ' and (WCT_Contact__r.HomePhone Like \''+String.escapeSingleQuotes(contactPhone)+'%\'' +
                  ' OR WCT_Contact__r.MobilePhone Like \''+String.escapeSingleQuotes(contactPhone)+'%\' )';
      }
      if(contactEmail!= null && contactEmail!=''){         
          soql += ' and WCT_Contact__r.Email Like \''+String.escapeSingleQuotes(contactEmail)+'%\'';
      }
      if(reqNum!=null && reqNum!=''){   
          soql += ' and WCT_Requisition__r.Name Like \''+String.escapeSingleQuotes(reqNum)+'%\'';       
      
      }
      if(rmsID!=null && rmsID!=''){   
          soql += ' and (WCT_Taleo_Id__c Like \''+String.escapeSingleQuotes(rmsID)+'%\' or WCT_Contact__r.WCT_Taleo_Id__c like\''+String.escapeSingleQuotes(rmsID)+'%\')';       
      
      }
      
      try{
          ctList = Database.query(soql + ' ORDER BY WCT_Contact__r.Name limit 200');
      }    
      catch(DmlException ex){
         for (Integer i = 0; i < ex.getNumDml(); i++) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getDmlMessage(i)));
         }
      }
      catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
      }    
      return null;
      
   }


  //---------------------------
    //    Search User List
    //---------------------------

public PageReference doSearchUser() {
   
   string   soqlUser = 'SELECT id, Name,Email,username from User WHERE isActive=True';
                                 
      if (userName!= null && !userName.equals('')){
          
          soqlUser += ' AND ( Name LIKE \'%'+String.escapeSingleQuotes(userName)+'%\') ';
      }
      
      if (Email!= null && !Email.equals('')){
          
          soqlUser += ' AND ( Email LIKE \'%'+String.escapeSingleQuotes(Email)+'%\') ';
      }
        
   
      system.debug('^^^'+soqlUser);
      try{
          List<user> tmpList = Database.query(soqlUser + ' ORDER BY User.Name limit 200');
          
            availableInvitees.clear();
            for(User u:tmpList){
                availableInvitees.add(new InviteeWrapper(u));
            }
          
      }    
      
      catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
      }    
      return null;
      
   }





   
   
   //---------------------------
    //    Add User to List
    //---------------------------
    public void addUser(){
        UserError = false;
        Map<id,InviteeWrapper> availableInviteesMap = new Map<id,InviteeWrapper>();
        List<Id> idsToRemove = new List<Id>();
        //availableInviteesBackUp.addAll(availableInvitees);
        
        integer count = 0;
        Boolean isAlreadyPresent;
        for(InviteeWrapper avUser : availableInvitees ){
            availableInviteesMap.put(avUser.userId,avUser);
            if(avUser.isSelected){
                isAlreadyPresent = false;
                for(InviteeWrapper seUser :selectedInvitees){
                    if(avUser.usr.id == seUser.usr.id){
                        isAlreadyPresent = true;
                        break;
                    }
                }
                if(!isAlreadyPresent){
                    avUser.isSelected = false;
                    selectedInvitees.add(avUser);
                    idsToRemove.add(avUser.userId);
                }else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'User already added !!'));
                    UserError = true;
                    isAddError = false;
                    return;
                }
            }
            count++;
        }
        if(!idsToRemove.isEmpty()){
            for(Id id:idsToRemove){
                availableInviteesMap.remove(id);
            }
        }
        if(availableInviteesMap.size()<>availableInvitees.size()){
            availableInvitees.clear();
            availableInvitees = availableInviteesMap.Values();
        }
        
        
        //availableInvitees.sort();
    }
   
   //---------------------------
    //    Remove User to List
    //---------------------------
    public void removeUser(){
        UserError = false;
        Map<Id,InviteeWrapper> selectedInviteesMap = new Map<Id,InviteeWrapper>();
        List<Id> idsToRemove = new List<Id>();
        
        integer count = 0;
        
        for(InviteeWrapper seUser : selectedInvitees ){
            selectedInviteesMap.put(seUser.userId,seUser);
            if(seUser.isSelected){
                seUser.isSelected = false;
                availableInvitees.add(seUser);
                idsToRemove.add(seUser.userId);
                
            }
            count++;
        }
        if(!idsToRemove.isEmpty()){
            for(Id id:idsToRemove){
                selectedInviteesMap.remove(id);
            }
        }
        if(selectedInviteesMap.size()<>selectedInvitees.size()){
            selectedInvitees.clear();
            selectedInvitees=selectedInviteesMap.values();
        }
        //availableInvitees.sort();
    }
   
   
   //---------------------------
    //    Add candidate to List
    //---------------------------
    public void addCandidate(){
        isAddError = false;
        
        for(WCT_Candidate_Requisition__c  tmpCandTrack : ctList){
            if(tmpCandTrack.WCT_Select_Candidate_For_Interview__c==true){
                tmpCandTrack.WCT_Select_Candidate_For_Interview__c=false;
                for(WCT_Candidate_Requisition__c  tmpCandTrackSelect :ctSelectedList){
                    if(tmpCandTrackSelect.id== tmpCandTrack.id){
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Candidate already added !!'));
                        isAddError = true;
                        UserError = false;
                        return;
                    }
                }
                ctSelectedList.add(tmpCandTrack);
            }
        }
        set<WCT_Candidate_Requisition__c> setCandTrack = new set<WCT_Candidate_Requisition__c>(ctList);
        for(WCT_Candidate_Requisition__c  tmpCandTrack : ctSelectedList){
            setCandTrack.remove(tmpCandTrack );
        }
        ctList.clear();
        for(WCT_Candidate_Requisition__c  tmpCandTrack : setCandTrack ){
            ctList.add(tmpCandTrack );
        }
        ctList.sort();
        ctSelectedList.sort();
    }
 
            
    //------------------------------
    //    Remove candidate from List
    //------------------------------
    public void removeCandidate(){
        for(WCT_Candidate_Requisition__c  tmpCandTrack : ctSelectedList){
            if(tmpCandTrack.WCT_Select_Candidate_For_Interview__c==true){
                tmpCandTrack.WCT_Select_Candidate_For_Interview__c=false;
                ctList.add(tmpCandTrack);
            }
        }
        set<WCT_Candidate_Requisition__c> setCandTrack = new set<WCT_Candidate_Requisition__c>(ctSelectedList);
        for(WCT_Candidate_Requisition__c  tmpCandTrack : ctList){
            setCandTrack.remove(tmpCandTrack );
        }
        ctSelectedList.clear();
        for(WCT_Candidate_Requisition__c  tmpCandTrack : setCandTrack ){
            ctSelectedList.add(tmpCandTrack );
        }
        ctList.sort();
        ctSelectedList.sort();
    }

    
    //Cloning Interview
    public pageReference cloneIntv(){
        if(ctSelectedList.size()==0){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select at least one candidate.'));
            isAddError = true;
            userError = false;
            return null;
        }
        if(eventToCreate.startDateTime == null){
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Event Start Date cannot be Blank!!'));
           isAddError = false;
           return null;
        
        }
        if(eventToCreate.enddateTime == null ){
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Event Event End Date cannot be Blank!!'));
           isAddError = false;
           return null;
           
        }
        if(eventToCreate.enddateTime < system.now()||eventToCreate.startDateTime < system.now()){
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Event Start Date and Event End Date cannot be less than Current Date Time!!'));
                        isAddError = false;
                        return null;
        
        }
        if(eventToCreate.enddateTime < eventToCreate.startDateTime){
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Event Start Date Cannot be less than Event End Date !!'));
                        isAddError = false;
                        return null;
        
        }
        
        IntvTrackersToBeCreated = new List<WCT_Interview_Junction__c>();
        try{
            insert intvToBeCreated;
            for(WCT_Candidate_Requisition__c candTrak : ctSelectedList){
                WCT_Interview_Junction__c tempIntvJun = new WCT_Interview_Junction__c();
                tempIntvJun.WCT_Candidate_Tracker__c = candTrak.id;
                tempIntvJun.WCT_Interview__c = intvToBeCreated.id;
                IntvTrackersToBeCreated.add(tempIntvJun);
            }
            if(!IntvTrackersToBeCreated.isEmpty()){
                insert IntvTrackersToBeCreated;
            }
                eventToCreate.whatId = intvToBeCreated.id;
                insert eventToCreate;
                eventRelationsToBeInserted = new List<EventRelation>();
                system.debug('^^^'+inviteeIds);
                if(!selectedInvitees.isEmpty()){
                    for(InviteeWrapper invitee : selectedInvitees){
                        EventRelation tempER = new EventRelation();
                        tempER.EventId = eventToCreate.Id;
                        tempER.RelationId = invitee.usr.id;
                        tempER.Status = 'New';
                        tempER.isInvitee = true;
                        if(invitee.usr.id <> userInfo.getUserId()){
                            eventRelationsToBeInserted.add(tempER);
                        }
                    }
                }
                if(!eventRelationsToBeInserted.isEmpty()){
                    Insert eventRelationsToBeInserted;
                }
                Set<Id> evId = new Set<Id>();
                evId.add(eventToCreate.Id);
                WCT_Interview_Scheduling_Utill.handelInvities(evId);
            return new PageReference('/'+intvToBeCreated.id);
        }catch(Exception ex){
            return null;
        }
    
    }
    
    public pageReference cancel(){
        return new PageReference('/'+intvToClone.id);
    }
    public static string DateTimetoXml(datetime dt) {
        return (dt==null) ? null : dt.formatGmt('yyyy-MM-dd')+'T'+ dt.formatGmt('HH:mm:ss')+'.'+dt.formatGMT('SSS')+'Z';  
    }
    
    public void searchInterview(){
        addIntvWrapperList = new List<addIntvWrapper>();
        List<WCT_Interview__c> tempList = new List<WCT_Interview__c>();
        String intvQuery = 'SELECT id,name,owner.name,createdDate,Group_Interview__c FROM WCT_Interview__c WHERE Group_Interview__c = true';
        if (intvNumber != null && !intvNumber.equals('')){
          intvQuery += ' AND ( Name LIKE \'%'+String.escapeSingleQuotes(intvNumber)+'%\') ';
        }
        if (intvOwner != null && !intvOwner.equals('')){
          intvQuery += ' AND ( owner.name LIKE \'%'+String.escapeSingleQuotes(intvOwner)+'%\') ';
        }
        if(tempIntvForSearch.WCT_Interview_Start_Time__c != null && tempIntvForSearch.WCT_Interview_End_Time__c != null && tempIntvForSearch.WCT_Interview_Start_Time__c > tempIntvForSearch.WCT_Interview_End_Time__c){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'From Date Cannot be less than To Date !!'));
            return ;
        }
        if (tempIntvForSearch.WCT_Interview_Start_Time__c != null){
          intvQuery += ' AND (createdDate > '+WCT_GroupInterviewClone_CTRL.DateTimetoXml(tempIntvForSearch.WCT_Interview_Start_Time__c)+')';
        }
        if (tempIntvForSearch.WCT_Interview_End_Time__c != null){
          intvQuery += ' AND (createdDate < '+WCT_GroupInterviewClone_CTRL.DateTimetoXml(tempIntvForSearch.WCT_Interview_End_Time__c)+')';
        }
        
        system.debug('$$'+intvQuery);
        tempList = DataBase.query(intvQuery + ' ORDER BY CreatedDate Desc limit 200');
        
        if(!tempList.isEmpty()){
            for(WCT_Interview__c tmp:tempList){
                addIntvWrapperList.add(new addIntvWrapper(tmp));
            }           
        }   
    
    } 
    //Method to add Candidates from additional Interviews.
    public void proClonePage(){
        Set<Id> AddIntwIdSet = new Set<Id>();
        set<Id> addCadidates = new Set<Id>();
        List<WCT_Candidate_Requisition__c> addCandTrackers = new List<WCT_Candidate_Requisition__c>();
        if(!addIntvWrapperList.isEmpty()){
            for(addIntvWrapper iw:addIntvWrapperList){
                if(iw.isSelected){
                    AddIntwIdSet.add(iw.intv.id);
                }
            }
        }
        if(!AddIntwIdSet.isEmpty()){
            for(WCT_Interview_Junction__c ij:[SELECT id,WCT_Candidate_Tracker__c,WCT_Requisition_ID__c,WCT_Interviewer_Recommendation__c from WCT_Interview_Junction__c 
                                                WHERE WCT_Interview__c IN :AddIntwIdSet]){
                                                
                if(ij.WCT_Interviewer_Recommendation__c <> null && (ij.WCT_Interviewer_Recommendation__c == 'Select' || ij.WCT_Interviewer_Recommendation__c == 'Tentative' )){
                    addCadidates.add(ij.WCT_Candidate_Tracker__c);
                }
                
            }           
        }
        if(!addCadidates.isEmpty()){
            if(!existingCadidates.isEmpty()){
                addCadidates.removeAll(existingCadidates);
            }
            if(!addCadidates.isEmpty()){
                for(WCT_Candidate_Requisition__c ct : [select WCT_Contact__r.Name, WCT_Requisition__r.Name ,WCT_Select_Candidate_For_Interview__c 
                                                    from WCT_Candidate_Requisition__c where id in : addCadidates]){
                    addCandTrackers.add(ct);
                }
            }
        }
        if(!addCandTrackers.isEmpty()){
            ctSelectedList.addAll(addCandTrackers);
        }       
        pclone = true;
    }   
}