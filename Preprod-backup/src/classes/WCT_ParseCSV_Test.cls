@isTest
public class WCT_ParseCSV_Test 
{
    
    public static testmethod void test1()
    {
        WCT_parseCSV csv= new WCT_parseCSV();
        Blob testBlob= Blob.valueOf('"Test Blob"');
        String blobString= WCT_parseCSV.blobToString(testBlob);
        csv.parseCSV(blobString, false);
        
        blobString='Test Class"';
        csv.parseCSV(blobString, true);
        
        blobString='"Test Class';
        csv.parseCSV(blobString, true);
        
        blobString='Test Class';
        csv.parseCSV(blobString, true);
    }

}