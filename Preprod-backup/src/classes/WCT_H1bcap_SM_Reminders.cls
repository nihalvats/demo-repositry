global class  WCT_H1bcap_SM_Reminders Implements Database.Batchable <sObject> {
    global Database.queryLocator start(Database.BatchableContext bc) {
         
        String query = 'SELECT Id,WCT_H1BCAP_RM_Name__c,WCT_H1BCAP_GDM_Email_id__c, WCT_H1BCAP_USI_SM_Name_GDM__c,WCT_H1BCAP_Resource_Manager_Email_ID__c,WCT_H1BCAP_Email_ID__c,WC_H1BCAP_USI_SLL_email_id__c,WCT_H1BCAP_Status__c, WCT_H1BCAP_Business_Days_SM__c FROM WCT_H1BCAP__c WHERE WCT_H1BCAP_Status__c =  \'Ready for USI SM / US PPD approval\' AND (WCT_H1BCAP_Business_Days_SM__c = 3 OR WCT_H1BCAP_Business_Days_SM__c = 4 OR WCT_H1BCAP_Business_Days_SM__c = 5)' ;
       
         
        return Database.getQueryLocator(query);
       
    }

    global void execute(Database.BatchableContext bc, List<WCT_H1BCAP__c> H1bcap) {
    
   
        string orgmail =  Label.WCT_H1BCAP_Mailbox;
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList1 = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList4 = new List<Messaging.SingleEmailMessage>();
        OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
        Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'Reminder_1_notification_USI_SM_US_PPD' AND IsActive = true];
        Emailtemplate et2 = [select id, developername , Body,IsActive from Emailtemplate where developername = 'WCT_Reminder_2_notification_USI_SM_PPD' AND IsActive = true];
        Emailtemplate et_RM = [select id, developername , Body,IsActive from Emailtemplate where developername = 'WCT_Escalation_to_RM_abt_USI_SM_PPD' AND IsActive = true];   
        
 
        for(WCT_H1BCAP__c w: H1bcap) {
          
          if(w.WCT_H1BCAP_Business_Days_SM__c == 3 && null <> w.WCT_H1BCAP_Resource_Manager_Email_ID__c && '' <> w.WCT_H1BCAP_Resource_Manager_Email_ID__c && Null <> w.WCT_H1BCAP_USI_SM_Name_GDM__c) 
     {
           list<string>  currentRMEmail = new list<string>();
           currentRMEmail.add( w.WCT_H1BCAP_Resource_Manager_Email_ID__c);
           system.debug('size of the list' +H1bcap.size());
           system.debug('ID**********' +w.id);
                           
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
            mail.SetTemplateid(et.id);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(w.WCT_H1BCAP_USI_SM_Name_GDM__c);
            mail.setCcAddresses(currentRMEmail);
            mail.setWhatid(w.id);
            mail.setOrgWideEmailAddressId(owe.id);
           
           system.debug('IDDDDDDDDDDD*******' +owe.id);
  
            system.debug('IDDDDDDDDDDD*******' +et.id);
    
   
            mailList.add(mail);   
           } 
              
        } 
        if(mailList.size() >0)
      {  
        try{
                Messaging.sendEmail(mailList);   
             }   
          catch(Exception npe) {
          System.debug('The following exception has occurred: ' + npe.getMessage());
             }       
      } 
        
               
        // Secong Mail Method 
     
        for(WCT_H1BCAP__c w: H1bcap) {
          
          if( w.WCT_H1BCAP_Business_Days_SM__c == 4 && null <> w.WCT_H1BCAP_Resource_Manager_Email_ID__c && '' <> w.WCT_H1BCAP_Resource_Manager_Email_ID__c && Null <>w.WCT_H1BCAP_USI_SM_Name_GDM__c) 
     {
           
             list<string>  currentUserEmail = new list<string>();
            currentUserEmail.add( w.WCT_H1BCAP_Resource_Manager_Email_ID__c);
            
       Messaging.SingleEmailMessage mail4 = new Messaging.SingleEmailMessage();
       
            mail4.SetTemplateid(et2.id);
            mail4.setSaveAsActivity(false);
            mail4.setTargetObjectId(w.WCT_H1BCAP_USI_SM_Name_GDM__c);
            mail4.setCcAddresses(currentUserEmail);
            mail4.setWhatid(w.id);
            mail4.setOrgWideEmailAddressId(owe.id);
            mailList4.add(mail4);   
           } 
              
        } 
        if(!mailList4.isempty())
      {  
          try{
                Messaging.sendEmail(mailList4);   
            
             }   
          catch(Exception npe) {
          System.debug('The following exception has occurred: ' + npe.getMessage());
             }          
      } 
        
     
     // Third Mail Method 
     
     map <set<id>,WCT_H1BCAP__c> Maprm = new map<set<id>,WCT_H1BCAP__c>();
       set<id> setrmids = new set<id>();
        
         for(WCT_H1BCAP__c w: H1bcap)
         {
           if(w.WCT_H1BCAP_Business_Days_SM__c == 5 && w.WCT_H1BCAP_RM_Name__c <> null )  
            {
             
             setrmids.add(w.WCT_H1BCAP_RM_Name__c);
             Maprm.put(setrmids,w);
             }
         }
     
     
     
     
        for(WCT_H1BCAP__c w: Maprm.values())
    {
                 
         Messaging.SingleEmailMessage mailtorms = new Messaging.SingleEmailMessage();
            mailtorms.SetTemplateid(et_RM.id);
            mailtorms.setSaveAsActivity(false);
            mailtorms.setTargetObjectId(w.WCT_H1BCAP_RM_Name__c);
            mailtorms.setWhatid(w.id);
            mailtorms.setOrgWideEmailAddressId(owe.id);  
            mailList1 .add(mailtorms);
        
    }    
    
    if(mailList1.size()>0)
    {
        try{
            Messaging.sendEmail(mailList1);  
           }   
          catch(Exception npe) {
          System.debug('The following exception has occurred: ' + npe.getMessage());
             }       
    }
}
    global void finish(Database.BatchableContext bc) {
    }
  }