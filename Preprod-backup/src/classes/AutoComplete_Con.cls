public class AutoComplete_Con {

    public String labelField{ get; set; }
    public String valueField{ get; set; }
    //public static String filterField{ get; set; }
    //public static String filtervalue{ get; set; }
    public String sObjVal{get;set;}
    public Integer randomJsIden{get;set;}
    public String cacheField{get;set;} 
   
    public AutoComplete_Con(){
        randomJsIden = getRandomNumber(1000000);
        sObjVal='Company__c';
        labelField='Name';
        valueField='Id';
        
    }

    
    /*
    *Random number generator to change the js function name if multiple components us
    ***/
    private Integer getRandomNumber(Integer size){
        Double d = Math.random() * size;
        return d.intValue();
    }
    
    /*
    *This method queries data according to the passed parameters
    ***/
    @RemoteAction
    public static List<SObject> getData(String sObjVal,String labelField,String valueField,String param){
        param = String.escapeSingleQuotes(param);
        string filterField='status__c';
        set<string> setFilterValue=new set<string>();
        setFilterValue.add('Approved');
        
        
        return Database.query('SELECT '+valueField+','+labelField+' FROM '+sObjVal+' WHERE '+filterField+' in: setFilterValue AND '+labelField+' LIKE \'%'+param+'%\'');
    }
    
    
}