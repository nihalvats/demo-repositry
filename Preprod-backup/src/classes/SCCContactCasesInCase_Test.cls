/*****************************************************************************************
    Name    : SCCContactCasesInCase_Test
    Desc    : Test class for SCCContactCasesInCase class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Rahul Agarwal                 09/11/2013         Created 
******************************************************************************************/
@isTest
/****************************************************************************************
    * @author      - Rahul Agarwal
    * @date        - 09/11/2013
    * @description - Test Method for SCCContactCasesInCase controller class
    *****************************************************************************************/
public class SCCContactCasesInCase_Test{
    public static testmethod void SCCContactCasesInCase(){
    	//Creating Case data
    	Test_Data_Utility.createCase();
    	Case c = [select Id, ContactId from Case limit 1];
    	// Creating Contact data
    	Test_Data_Utility.createContact();
    	Contact con = [select Id from Contact limit 1];
    	//aatching contact to case
    	c.ContactId = con.Id;
    	update c;
    	test.startTest();
    	ApexPages.StandardController controller = new ApexPages.StandardController(c);
        SCCContactCasesInCase cont = new SCCContactCasesInCase(controller);
        test.stopTest();
    }
}