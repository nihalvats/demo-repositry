/********************************************************************************************************************************
Apex class         : <Test_WCT_ExpUtility>
Description        : <Test class for Exception Utility class>
Type               :   Test Class

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 26/05/2016          100%                          --                            License Cleanup Project
************************************************************************************************************************************/   


@isTest (SeeAllData=false)

private class Test_WCT_ExpUtility {

    static testMethod void myTest() {
    String className = 'WCT_batchArticleExpNotify';
    String exceptionMessage = 'Picklist values';
    String pageName = 'test page';
    
    Test.StartTest();
    WCT_ExceptionUtility.logException(className,exceptionMessage,pageName);
    WCT_ExceptionUtility.returnExcLog(className,exceptionMessage,pageName);
    Test.StopTest();
    
  }
}