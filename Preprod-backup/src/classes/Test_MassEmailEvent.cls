@isTest(seeAlldata = true)
public class Test_MassEmailEvent 
{
 public static testMethod void MassEmailEvent()
 {
  ApexPages.StandardSetController ssc; 
  ApexPages.StandardSetController sscResults ;  
 list<id> contactids = new List<Id>();
 
Account acc=new Account(name='test');
insert acc;

Contact con=new Contact(AccountId=acc.Id,lastName='test2',TRM_LeadSourceDetail__c='Experienced Hire',Email = 'xyz@gmail.com');
//con.TRM_sourceDetailName__c='LinkedIn';
insert con;

List<Contact> selectedContacts = new List<Contact>();
selectedContacts.add(con);
contactids.add(con.id);

Folder f=new Folder(name='testfolder');
//f.create();
List<Folder> listfolder=[Select id from Folder where Name='PDAT'];
//String errormsg = ApexPages.getMessages();

Event__c event = new Event__c();
event.Name = 'National;';
event.start_date_time__c = system.now();
event.End_date_time__c = system.now();

//event.Contact__c = 'Ashley Tester';
 insert event;

Event_Registration_Attendance__c conts  = new Event_Registration_Attendance__c();
//conts=[Select id,Event__c,contact__c from Event_Registration_Attendance__c];
conts.Event__c = event.id;
conts.contact__c = con.id;

insert conts;
 
List<EmailTemplate> emialtemp=[Select id from Emailtemplate LIMIT 1];

List<ApexPages.Message> msgList = ApexPages.getMessages();
// or loop over the messages
for(ApexPages.Message msg :  ApexPages.getMessages()) 
{
System.assertEquals('Have a nice day', msg.getSummary());
System.assertEquals(ApexPages.Severity.ERROR, msg.getSeverity());
}
MassEmailEvent testemailcon=new MassEmailEvent(new ApexPages.StandardController(con));

Pagereference p=Page.MassEmailEvent;

testemailcon.selectedEmailTemplateFolder='';
testemailcon.selectedEmailTemplate='';
testemailcon.selectedEmailTemplate =null;
testemailcon.selectedEmailTemplateFolder=null;
testemailcon.selectedEmailTemplateFolder='00l500000019luV';
testemailcon.selectedEmailTemplate ='00X50000001SVAH';
testemailcon.getEmailTemplateFolderOpts();
testemailcon.getEmailTemplateOpts();
testemailcon.refreshEmailTemplateSectionid();
testemailcon.columnNames();
testemailcon.fieldresults.put('String','1');
testemailcon.convertData('String','1');
testemailcon.getOperatorOptions();
testemailcon.getObj();
String obj='test';
testemailcon.setObj(obj);
testemailcon.go();
testemailcon.getObjNames();
testemailcon.getContactList();
MassEmailEvent.cContact cCon=new MassEmailEvent.cContact(con);
testemailcon.getContactList();
testemailcon.getSelected();
testemailcon.contactids.add(con.id);
testemailcon.LoadContacts();
testemailcon.errorString();
testemailcon.condString();

testemailcon.getSelected();
testemailcon.refreshEmailTemplateSection();
//testemailcon.nextResults();
//testemailcon.previousResults();
 }
}