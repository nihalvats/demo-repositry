global class WCT_updateFlagsOnLeaves_Batch implements Database.Batchable<sObject>{

Date DateInbetween_14 = date.Today().AddDays(14);
Date  DateAfter11Months=date.Today().addMonths(11);

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([select id, name, WCT_Confirm_Return_to_Work__c, WCT_Leave_Type__c, WCT_Return_to_work_date__c, WCT_Leave_Start_Date__c, WCT_Leave_End_Date__c, WCT_Leave_End_Date_Status__c 
        from WCT_Leave__c  where WCT_Leave_End_Date__c < :DateInbetween_14 OR WCT_Leave_Start_Date__c < :DateAfter11Months]);
   
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<WCT_Leave__c> leaveUpdateList = new List<WCT_Leave__c>();
         system.debug('%%%%%%%%%' + scope);
        for(sObject l: scope){
        
            WCT_Leave__c leave = (WCT_Leave__c)l;
            system.debug('Leave recrod'+leave.Id);
            if(leave.WCT_Leave_Start_Date__c != null){
                system.debug('11 months difference---'+leave.WCT_Leave_Start_Date__c.monthsBetween(date.today()));
                if(leave.WCT_Leave_Start_Date__c.monthsBetween(date.today()) > 11 ){                    
                    leave.WCT_Flag_Initiate_review_process__c = true;
                    system.debug('11 months difference flag'+leave.WCT_Flag_Initiate_review_process__c);
                }
            
                system.debug('17 months difference---'+leave.WCT_Leave_Start_Date__c.monthsBetween(date.today()));
                if(leave.WCT_Leave_Start_Date__c.monthsBetween(date.today()) > 17 ){
                    leave.WCT_Flag_18_months_military_notify__c = true;
                    system.debug('17 months difference flag'+leave.WCT_Flag_18_months_military_notify__c);
                }
            }
            
            if(leave.WCT_Leave_End_Date__c != null){
                system.debug('2 weeks difference---'+leave.WCT_Leave_End_Date__c.daysBetween(date.today()));
                 if(date.today().daysBetween(leave.WCT_Leave_End_Date__c) <= 14 ){
                                  
                    leave.WCT_Flag_2_weeks_prior_to_End_Date__c = true;
                    leave.WCT_Leave_End_Date_Status__c = 'Confirmed';
                    system.debug('2 weeks difference flag'+leave.Name+date.today().daysBetween(leave.WCT_Leave_End_Date__c));
                }
            }
            leaveUpdateList.add(leave);
        }
        
        if(!leaveUpdateList.isEmpty()){
             Database.Update(leaveUpdateList,false);
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
}