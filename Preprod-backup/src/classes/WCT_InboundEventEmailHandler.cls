/*
    * Class Name  : WCT_InboundEventEmailHandler   
    * Description : This is the email service class to handle responses for Meeting 
    *               Invite sent to Interviewers 

*/
global class WCT_InboundEventEmailHandler implements Messaging.InboundEmailHandler{
    //Method to handle incoming emails
    
     global static  Boolean isAccepted = false;
     global static  Boolean isDeclined = false;
     global static  Boolean isTentative = false;
     global static  Boolean isreply = false;
     global  Boolean fromInterviewer=false;
     global  Boolean fromRecruiter=false;
     global  Boolean fromCoordinator=false;
     global static String eveId {get;set;}
     global static string getemailbody{get;set;}
     //Handle out of office mails
     global static  Boolean isAutoReply = false;   
     global string getAutorplybdy;
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.Inboundenvelope envelope) {
        
        //Variable Declaration
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        String ACCEPTED_STATUS = 'Accepted';
        String DECLINED_STATUS = 'Declined';
        String TENTATIVE_STATUS = 'Tentative';
        String PROPOSETIME_STATUS = 'New Time Proposed';
        //Handle out of office mails
        String AUTOREPLY_STATUS = 'Automatic reply:';
        //variable for adhoc change in pannel
        string REPLY_STATUS='RE';
        system.debug('####'+REPLY_STATUS);
        //This convention is used when sending invite
        String  INTERVIEWNUMBER = 'Id:';
        String EventId ;
        String emailSubject = email.subject.trim();
        system.debug('$$$$'+emailSubject);
        String InterviewName ;
        String fromAddress = email.fromAddress; // can be recuirter, coordinator or interviewr
        string NewInterviewrAddress='';
        string currentInterviewer='';
        //variable for adhoc change in pannel
        list<string> ccAddress= email.ccAddresses;
        //ccAddress.add(email.ccAddresses);// new Interviewr , recruiter, Coordinator
        List<EventRelation> liEventRelations= new List<EventRelation>();
        List<EventRelation> liEventRelationsToUpdate= new List<EventRelation>();
        List<EventRelation> liEventRelationsTocreate= new List<EventRelation>();
        List<WCT_Interview_Junction__c> liInterviewJunction = new List<WCT_Interview_Junction__c>();
        List<User> userList = new List<User>();
        //handle adhoc changes of pannel
        List<User> curuserList = new List<User>();
        Set<Id> userIdSet = new Set<Id>();
        Set<Id> curuserIdSet = new Set<Id>();
        id intjuncid;
        list<id> lintid=new list<id>();
        String emailBody;
        if(email.plainTextBody != null && email.plainTextBody !=''){
            emailBody = email.plainTextBody;
        }else if(email.htmlBody !=null){
                string html=email.htmlBody;
                string resultStr = html.replaceAll('<br/>', '\n');
                resultStr = resultStr.replaceAll('<br />', '\n');
                resultStr = resultStr.replaceAll('&nbsp;', '');
                resultStr = resultStr.replaceAll('&gt;', '');
                resultStr = resultStr.replaceAll('&lt;', '');
                //string HTML_TAG_PATTERN = '<.*?>';
                string HTML_TAG_PATTERN = '<[^>]+>|&nbsp;';
                pattern myPattern = pattern.compile(HTML_TAG_PATTERN);
                matcher myMatcher = myPattern.matcher(resultStr);
                resultStr = myMatcher.replaceAll('');
                emailBody = resultStr ;
        }
        if(emailSubject.startsWith(AUTOREPLY_STATUS)){
        getemailbody=emailBody;
        getAutorplybdy=getemailbody.substringBefore('-----Original Appointment-----');
        }
        //Get All users based on from Address of email , since 
        try{
            userList = [SELECT id,email from User where email=:fromAddress];
            //handle adhoc change in pannel
            
        }Catch(QueryException queryException){ 
            WCT_ExceptionUtility.logException('Email Service Handler','InboundEventEmailHandler',queryException.getMessage());
            result.success = false;
            result.message = queryException.getMessage(); 
        }
        if(!userList.IsEmpty()){
            for(User u:userList){
                userIdSet.add(u.id);
            }
        }

        //Processing email subject appropriately as required
        List<String> stringSubjectList=new List<String>();
        if(emailSubject != null){
        system.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>emailSubject >>>>>>>>>>>>'+emailSubject);
            if(emailSubject.startsWith(ACCEPTED_STATUS)){
                isAccepted = true;
            }else if(emailSubject.startsWith(DECLINED_STATUS)){
                isDeclined = true;
            }else if(emailSubject.startsWith(TENTATIVE_STATUS) || emailSubject.startsWith(PROPOSETIME_STATUS)){
                isTentative = true;         
            }
            else if(emailSubject.startsWith(REPLY_STATUS)){
                isreply = true;
            }
            
              else if(emailSubject.startsWith(AUTOREPLY_STATUS)){
                  
                isAutoReply = true;
                  system.debug('$$$$'+isAutoReply);
            }
            system.debug('%%%%'+emailSubject.startsWith(AUTOREPLY_STATUS));
            if(emailSubject.Contains(INTERVIEWNUMBER)){
                emailSubject = emailSubject.replace(']','');
                emailSubject = emailSubject.replace('[','');
                stringSubjectList = emailSubject.split(':');            
            }
            for(String s : stringSubjectList){
                s=s.trim();
                if(s.startsWith('I-')){
                    InterviewName = s;
                    //break;
                }
            }
        }
        //handle adhoc change in interview . so get the interview user group
        WCT_Interview__c intv=[SELECT id,WCT_User_group__c,Owner.email from WCT_Interview__c where Name=:InterviewName limit 1];
     
        try{
            
            
            if(!isreply)
            {
            liInterviewJunction = [SELECT Id,WCT_Interview__r.Name,WCT_Interview_Junction__c.CreatedById,WCT_Interviewer_Email__c,WCT_Interview__c,WCT_Requisition_Recruiter_Email__c,WCT_Requisition_RC_Email__c,
                                   WCT_Interview__r.Owner.Name,WCT_Interviewer__r.name,WCT_Interview__r.Owner.Email,WCT_Interview__r.WCT_User_group__c,WCT_Interview__r.WCT_Interview_Start_Time__c,
                                   WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email,WCT_Interview__r.WCT_Collaborator__r.email,
                                   WCT_Interview__r.WCT_Collaborator_2__r.email,WCT_Interview__r.WCT_Collaborator_3__r.email from WCT_Interview_Junction__c  
                                   WHERE WCT_Interview__r.Name =:InterviewName AND WCT_Interviewer_Email__c =:fromAddress];
            }
            else if(intv.WCT_User_group__c!='US' && isreply==true)
            {
              
                 liInterviewJunction = [SELECT Id,WCT_Interview__r.Name,WCT_Interview_Junction__c.CreatedById,WCT_Interviewer_Email__c,WCT_Interview__c,WCT_Requisition_Recruiter_Email__c,WCT_Requisition_RC_Email__c,
                                   WCT_Interview__r.Owner.Name,WCT_Interviewer__r.name,WCT_Interview__r.Owner.Email,WCT_Interview__r.WCT_User_group__c,WCT_Interview__r.WCT_Interview_Start_Time__c,
                                   WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email,WCT_Interview__r.WCT_Collaborator__r.email,
                                   WCT_Interview__r.WCT_Collaborator_2__r.email,WCT_Interview__r.WCT_Collaborator_3__r.email from WCT_Interview_Junction__c  
                                   WHERE WCT_Interview__r.Name =:InterviewName AND (WCT_Interviewer_Email__c =:fromAddress or WCT_Requisition_Recruiter_Email__c=:fromAddress or WCT_Requisition_RC_Email__c=:fromAddress)];
                
                /*for(WCT_Interview_Junction__c lintjunc :liInterviewJunction)
                {
                    for(string c:ccAddress) 
                    {
                        if(lintjunc.WCT_Interviewer_Email__c==c)
                        {
                            lintid.add(lintjunc.id);
                        }
                    }
                }*/
                for(WCT_Interview_Junction__c lintjunc :liInterviewJunction)
                {
                    if(lintjunc.WCT_Interviewer_Email__c==fromAddress)
                    {
                       fromInterviewer=true;
                       currentInterviewer=fromAddress;
                       for(string c:ccAddress) 
                       {
                           if(lintjunc.WCT_Requisition_Recruiter_Email__c!=c && lintjunc.WCT_Requisition_RC_Email__c!=c && intv.Owner.email!=c)
                           {
                               NewInterviewrAddress=c;
                               
                           }
                       }
                        break;
                    }
                    else if(lintjunc.WCT_Requisition_Recruiter_Email__c==fromAddress)
                    {
                       fromRecruiter=true;
                       for(string c:ccAddress) 
                       {
                           if(lintjunc.WCT_Interviewer_Email__c!=c && lintjunc.WCT_Requisition_RC_Email__c!=c && intv.Owner.email!=c)
                           {                              
                               NewInterviewrAddress=c;
                           }
                           if(lintjunc.WCT_Interviewer_Email__c==c)
                           {
                               currentInterviewer=c;
                           }
                       }
                        if(currentInterviewer=='')
                        {
                            continue;
                        }
                        else
                        {
                        break;
                        }
                    }
                    else if(lintjunc.WCT_Requisition_RC_Email__c==fromAddress)
                    {
                       fromCoordinator=true; 
                       for(string c:ccAddress) 
                       {
                           if(lintjunc.WCT_Interviewer_Email__c!=c && lintjunc.WCT_Requisition_Recruiter_Email__c!=c && intv.Owner.email!=c)
                           {                              
                               NewInterviewrAddress=c;
                           }
                           if(lintjunc.WCT_Interviewer_Email__c==c)
                           {
                               currentInterviewer=c;
                           }
                       }
                        if(currentInterviewer=='')
                        {
                            continue;
                        }
                        else
                        {
                        break;
                        }
                    }
                    
                }
               for(WCT_Interview_Junction__c lintjunc :liInterviewJunction)
                {
               
                        if(lintjunc.WCT_Interviewer_Email__c==currentInterviewer)
                        {
                            lintid.add(lintjunc.id);
                        }
                }
                curuserList = [SELECT id,email from User where email=:NewInterviewrAddress limit 1];
                        //handle adhoc change in pannel
       			if(!curuserList.IsEmpty()){
                    for(User u:curuserList){
                        curuserIdSet.add(u.id);
                    }
                }
            }
        }
        Catch(QueryException queryException){  
            WCT_ExceptionUtility.logException('Email Service Handler','InboundEventEmailHandler',queryException.getMessage());
            result.success = false;
            result.message = queryException.getMessage();
        }

        try{
            liEventRelations = [SELECT EventId, RelationId, Event.WhatId,Event.What.Name,Status FROM EventRelation where IsInvitee = true AND Event.What.Name = :InterviewName];
        }Catch(QueryException queryException){  
            WCT_ExceptionUtility.logException('Email Service Handler','InboundEventEmailHandler',queryException.getMessage());
            result.success = false;
            result.message = queryException.getMessage();
        }  

        for(EventRelation er:liEventRelations ){
            if(eveId == '' || eveId == null){
                eveId = er.EventId;
            }
            if(userIdSet.Contains(er.RelationId)){
                if(isAccepted){
                    er.status = ACCEPTED_STATUS;
                    liEventRelationsToUpdate.add(er); 
                }else if(isDeclined){
                    er.status = DECLINED_STATUS;
                    liEventRelationsToUpdate.add(er); 
                }else if(isTentative){
                    if(emailBody!=null && emailBody!=''){
                        er.Response = emailBody;
                        liEventRelationsToUpdate.add(er);
                    }
                }
            }
        }
        try{
            update liEventRelationsToUpdate;
            if((isDeclined)||(isAccepted)){
                InitiateRejectedEmail(liInterviewJunction);
             }
            if(isTentative){
               // InitiateAutoEmailToIntvr(liInterviewJunction);
               InitiateTentitiveEmail(liInterviewJunction);
            }
            if(isReply==true && intv.WCT_User_group__c!='US'){
               // handle adhoc change in pannel
               UpdateEventSendInvite(lintid,liEventRelations,curuserList[0],intv.id);
            }
            if(isAutoReply){
               // InitiateAutoEmailToIntvr(liInterviewJunction);
               InitiateTentitiveEmail(liInterviewJunction);
            }
            result.success = true;
            /*result.message = 'updated Event Relations'+'\n';
        for(EventRelation e:liEventRelationsToUpdate){
            result.message += e.Id + 'with'+e.status +'\n';

        }*/

        }Catch(DmlException dmlException){  
            WCT_ExceptionUtility.logException('Email Service Handler','InboundEventEmailHandler',dmlException.getMessage());
            result.success = false;
            result.message = dmlException.getMessage();
        } 
        return result;


    }
    //Start Method-InitiateAutoEmailToIntvr
   /* global void InitiateAutoEmailToIntvr(List<WCT_Interview_Junction__c> liIntJunction){
        String recEmail,recCoorEmail,OwnerEmail,emailBodyMessage;
        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        for(WCT_Interview_Junction__c ij:liIntJunction){
            if(ij.WCT_Interviewer__c <> null){
                if(ij.WCT_Requisition_Recruiter_Email__c <> null && ij.WCT_Requisition_Recruiter_Email__c <> ''){
                    recEmail = ij.WCT_Requisition_Recruiter_Email__c;
                }
                if(ij.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email!=null){
                    recCoorEmail = ij.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email;
                }
                OwnerEmail = ij.WCT_Interview__r.owner.email;
                emailBodyMessage = 'Dear '+ ij.WCT_Interviewer__r.name +',<Br>';
                emailBodyMessage += 'You responded with either “Tentative” or “Propose New Time” to the interview invite you received. '+
                                    'The Salesforce system cannot track these two response types.  '+
                                    'Therefore, please contact your Recruiting team to reschedule the interview.<Br>';
                if(recEmail <> null){
                    emailBodyMessage += 'Recruiter Email: '+recEmail+'<Br>';
                }
                if(recCoorEmail <> null){
                    emailBodyMessage += 'Recruiting Coordinator Email: '+recCoorEmail+'<Br>';
                }
                if(ij.WCT_Interview__r.WCT_Collaborator__r.email <> null){
                    emailBodyMessage += 'Collaborator 1 Email: '+ij.WCT_Interview__r.WCT_Collaborator__r.email+'<Br>';
                }
                if(ij.WCT_Interview__r.WCT_Collaborator_2__r.email <> null){
                    emailBodyMessage += 'Collaborator 2 Email: '+ij.WCT_Interview__r.WCT_Collaborator_2__r.email+'<Br>';
                }
                if(ij.WCT_Interview__r.WCT_Collaborator_3__r.email <> null){
                    emailBodyMessage += 'Collaborator 3 Email: '+ij.WCT_Interview__r.WCT_Collaborator_3__r.email+'<Br>';
                }
                emailBodyMessage += 'Interview Owner Email: '+ OwnerEmail+'<Br>';
                emailBodyMessage += '<Br>Thank you,<Br>Deloitte Recruiting';
                
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.setSubject('Tentative & Propose New Time are not tracked through Salesforce');
                message.settargetObjectId(ij.WCT_Interviewer__c);
                message.setHtmlBody(emailBodyMessage);
                message.setsaveAsActivity(false); 
                messages.add(message);
                
            }
            if(!messages.isEmpty()){
                Messaging.sendEmail(messages);
            }
            break;
        }
    
    }*/
    //handle autoreply for out of office

    //End Method-InitiateAutoEmailToIntvr
    //handle adhoc changes to pannel
    //method to update event with new interviewr ad send mail
    global void UpdateEventSendInvite(list<id> intjuncid,List<EventRelation> liEventRelationsup,user userIdSetup,id i){
        list<WCT_Interview_Junction__c> liIntJunctionup=new list<WCT_Interview_Junction__c>();
        //event relation creation
        List<EventRelation> liEventRelationsTocreate= new List<EventRelation>();
        EventRelation ercreate=new EventRelation();
        ercreate.RelationId= userIdSetup.id;
        ercreate.EventId=liEventRelationsup[0].EventId;
        insert ercreate;
        event e=new event();
        e.id=eveId;
        update e;
        for(id idjuc:intjuncid)
        {
        WCT_Interview_Junction__c IntJunctionup=new WCT_Interview_Junction__c();
        IntJunctionup.id=idjuc;
        IntJunctionup.WCT_IEF_Submission_Status__c='Canceled';
        IntJunctionup.WCT_Interview_Junction_Status__c='Canceled';
        liIntJunctionup.add(IntJunctionup);
        }
        
        update liIntJunctionup;
        PageReference pageRef;
        WCT_Finalize_Interview_Controller fintcnt=new WCT_Finalize_Interview_Controller(eveId,string.valueOf(i),'');
        fintcnt.EmailBodyDecoded=fintcnt.EmailBody;
        fintcnt.InvitIds=new set<id>{userIdSetup.id};
        pageRef=fintcnt.processSelectedAtts();
        
    }
    
    //Method to send email if event has been declined
    global void InitiateRejectedEmail(List<WCT_Interview_Junction__c> liIntJunction){
    
        string userTimeZone,usertimeZoneVal;
        Map<string,string>timezonMap = new Map<string,string>
        {
        'America/Puerto_Rico' => 'AST',
        'America/Denver' => 'MST',
        'America/Phoenix' => 'MST',
        'America/New_York' => 'EST',
        'America/Panama' => 'EST',
        'America/Chicago' => 'CST',
        'America/Indiana/Indianapolis' => 'EST',
        'America/Mexico_City' => 'CST',
        'America/Los_Angeles' => 'PST',
        'Asia/Kolkata' => 'IST',
        'America/Adak' => 'HAST',
        'America/Anchorage' => 'AKST',
        'America/Argentina/Buenos_Aires' => 'ART',
        'America/Bogota' => 'COT',
        'America/Caracas' => 'VET',
        'America/El_Salvador' => 'CST',
        'America/Halifax' => 'AST',
        'America/Lima' => 'PET',
        'America/Santiago' => 'CLT',
        'America/Sao_Paulo' => 'BRT',
        'America/Scoresbysund' => 'EGT',
        'America/St_Johns' => 'NST',
        'America/Tijuana' => 'PST'
        };  
        
         User u = [select timezonesidkey from user where id=:UserInfo.getUserId()];
         if(u.TimeZoneSidKey!=null)userTimeZone = u.TimeZoneSidKey;
        
        if(timezonMap.get(userTimeZone)!=null){
            usertimeZoneVal=timezonMap.get(userTimeZone);
        }else{usertimeZoneVal = '';}  

        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        for(WCT_Interview_Junction__c intJun : liIntJunction){
            list<string> toAddress =new list<string>();
            list<string> ccAddress =new list<string>();
            if(intJun.WCT_Requisition_Recruiter_Email__c != null && intJun.WCT_Requisition_Recruiter_Email__c != ''){
                ccAddress.add(intJun.WCT_Requisition_Recruiter_Email__c);
            }
            if(intJun.WCT_Interview__r.Owner.Email <> intJun.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email){
                if(intJun.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email!=null){
                    ccAddress.add(intJun.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email);
                }
            }
            //if(intJun.WCT_Requisition_RC_Email__c != null && intJun.WCT_Requisition_RC_Email__c != ''){
                toAddress.add(intJun.WCT_Interview__r.Owner.Email);//(intJun.WCT_Requisition_RC_Email__c);
            //}
            String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/'+intJun.WCT_Interview__c ;
            String eventURL = URL.getSalesforceBaseUrl().toExternalForm() + '/'+eveId ;
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            if(toAddress!=null)message.setToAddresses(toAddress);
            if(ccAddress!=null)message.setCCAddresses(ccAddress);
            if(isDeclined){
             message.setSubject('Interviewer has Declined Interview Event');
             message.setHtmlBody('Dear '+intJun.WCT_Interview__r.Owner.Name+',<br><b>'+
                    intJun.WCT_Interviewer__r.name +'</b> has declined the Interview Event Scheduled on <b>' +intJun.WCT_Interview__r.WCT_Interview_Start_Time__c.format('MM/dd/yyyy h:mm a')+' '+usertimeZoneVal+' </b>.'+
                    candTable(intJun.WCT_Interview__c)+'<br>'+
                    '<br>As a result, please either select a new interviewer for the event or reschedule the Interview with the Candidate.<p>'+
                    'Link of Interview '+fullRecordURL+'<a href=fullRecordURL></a><br>'+
                    'Link of Event '+eventURL+'<a href=eventURL></a><br>'+
            '<br><br>Thanks,<br>Deloitte Recruiting');
            }else
            {
            message.setSubject('Interviewer has accepted Interview Event');
            message.setHtmlBody('Dear '+intJun.WCT_Interview__r.Owner.Name+',<br><b>'+
                    intJun.WCT_Interviewer__r.name +'</b> has accepted the Interview Event Scheduled on <b>' +intJun.WCT_Interview__r.WCT_Interview_Start_Time__c.format('MM/dd/yyyy h:mm a')+' '+usertimeZoneVal+' </b>.'+
                    candTable(intJun.WCT_Interview__c)+'<br>'+
                    'Link of Interview '+fullRecordURL+'<a href=fullRecordURL></a><br>'+
                    'Link of Event '+eventURL+'<a href=eventURL></a><br>'+
                    '<br><br>Thanks,<br>Deloitte Recruiting');
            }
            if(!toAddress.IsEmpty()){
                messages.add(message);
            }      
         break;
        }
        if(!messages.IsEmpty()){
            Messaging.sendEmail(messages);
        }

    }
    /*Email to Owner when the interviewer Sends a tentitive response */
    global void InitiateTentitiveEmail(List<WCT_Interview_Junction__c> liIntJunction){
    
        string userTimeZone,usertimeZoneVal;
        Map<string,string>timezonMap = new Map<string,string>
        {
        'America/Puerto_Rico' => 'AST',
        'America/Denver' => 'MST',
        'America/Phoenix' => 'MST',
        'America/New_York' => 'EST',
        'America/Panama' => 'EST',
        'America/Chicago' => 'CST',
        'America/Indiana/Indianapolis' => 'EST',
        'America/Mexico_City' => 'CST',
        'America/Los_Angeles' => 'PST',
        'Asia/Kolkata' => 'IST',
        'America/Adak' => 'HAST',
        'America/Anchorage' => 'AKST',
        'America/Argentina/Buenos_Aires' => 'ART',
        'America/Bogota' => 'COT',
        'America/Caracas' => 'VET',
        'America/El_Salvador' => 'CST',
        'America/Halifax' => 'AST',
        'America/Lima' => 'PET',
        'America/Santiago' => 'CLT',
        'America/Sao_Paulo' => 'BRT',
        'America/Scoresbysund' => 'EGT',
        'America/St_Johns' => 'NST',
        'America/Tijuana' => 'PST'
        };  
        
         User u = [select timezonesidkey from user where id=:UserInfo.getUserId()];
         if(u.TimeZoneSidKey!=null)userTimeZone = u.TimeZoneSidKey;
        
        if(timezonMap.get(userTimeZone)!=null){
            usertimeZoneVal=timezonMap.get(userTimeZone);
        }else{usertimeZoneVal = '';}  

        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        for(WCT_Interview_Junction__c intJun : liIntJunction){
            list<string> toAddress =new list<string>();
            list<string> ccAddress =new list<string>();
            if(intJun.WCT_Requisition_Recruiter_Email__c != null && intJun.WCT_Requisition_Recruiter_Email__c != ''){
                toAddress.add(intJun.WCT_Requisition_Recruiter_Email__c);
            }
            
            if(intJun.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email!=null)
             {
                    toAddress.add(intJun.WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email);
             }
            
            /*Owner is also sent email for US firm.*/
            if(intJun.WCT_Interview__r.WCT_User_group__c=='US'){
                if(intJun.WCT_Interview__r.Owner.Email != null && intJun.WCT_Interview__r.Owner.Email != ''){
                   toAddress.add(intJun.WCT_Interview__r.Owner.Email);
                }
           }
            String fullRecordURL = URL.getSalesforceBaseUrl().toExternalForm() + '/'+intJun.WCT_Interview__c ;
            String eventURL = URL.getSalesforceBaseUrl().toExternalForm() + '/'+eveId ;
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            if(toAddress!=null)message.setToAddresses(toAddress);
            if(ccAddress!=null)message.setCCAddresses(ccAddress);
            if(isTentative){
             message.setSubject('Interviewer is tentative for Interview Event');
             message.setHtmlBody('Hi Everyone, <br><br>'+
                    '<b>'+intJun.WCT_Interviewer__r.name +'</b> is tentative for the Interview Event Scheduled on <b>' +intJun.WCT_Interview__r.WCT_Interview_Start_Time__c.format('MM/dd/yyyy h:mm a')+' '+usertimeZoneVal+' </b>.'+
                    candTable(intJun.WCT_Interview__c)+'<br>'+
                    '<br>As a result, please either select a new interviewer for the event or reschedule the Interview with the Candidate.<p>'+
                    'Link of Interview '+fullRecordURL+'<a href=fullRecordURL></a><br>'+
                    'Link of Event '+eventURL+'<a href=eventURL></a><br>'+
            '<br><br>Thanks,<br>Deloitte Recruiting');
            }
            else if(isAutoReply)
            {
             System.debug('$$$$ Entered Auto reply');
             message.setSubject('Interviewer is Out of Office');
             message.setPlainTextBody(getAutorplybdy);
            }
            if(!toAddress.IsEmpty()){
                messages.add(message);
            }      
         break;
        }
        if(!messages.IsEmpty()){
            Messaging.sendEmail(messages);
        }

    }
    
    //handle autoreply for out of office
    
     public string candTable(Id intvId){
        string htmlTable = '';
        Set<Id> candTrakIdSet = new Set<Id>();
        for(WCT_Interview_Junction__c ij:[SELECT id,WCT_Candidate_Tracker__r.WCT_Contact__r.name,WCT_Candidate_Tracker__r.WCT_Requisition__r.name 
                                            FROM WCT_Interview_Junction__c WHERE WCT_Interview__c =:intvId]){
            candTrakIdSet.add(ij.WCT_Candidate_Tracker__c);
        }
        if(!candTrakIdSet.isEmpty()){
            integer count = 0;
            for(WCT_Candidate_Requisition__c ct:[SELECT id,WCT_Contact__r.name,WCT_Requisition__r.name,WCT_Email__c,WCT_RMS_Taleo_ID__c FROM WCT_Candidate_Requisition__c 
                                                    WHERE Id IN :candTrakIdSet]){
                count++;                                    
                if(htmlTable == ''){
                    htmlTable= 'Interview is scheduled with following candidates.<Br/><Br/>'+
                     '<table id="tableRecords" border="1">'+
                        '<thead>'+
                            '<tr>'+
                                '<th >S.No</th>'+
                                '<th >Candidate Name</th>'+
                                '<th >Email</th>'+
                                '<th >RMS ID</th>'+
                                '</th>'+
                            '</tr>'+
                        '</thead>'+
                        '<tbody>';
                    
                }//else{
                    htmlTable += '<tr><td>'+count+'</td>'+
                                  '<td>'+ct.WCT_Contact__r.name+'</td>'+
                                  '<td>'+ct.WCT_Email__c+'</td>'+
                                  '<td>'+ct.WCT_RMS_Taleo_ID__c+'</td></tr>';
                //}
            }
        }
        if(htmlTable <> ''){
            htmlTable += '</tbody></table><br>';
        }   
        
        return htmlTable;
    }
}