public class Wct_Workers_Compensation_form {
public boolean mainpage{get;set;}
public boolean tqpage{get;set;}
public boolean empview{get;set;}
public Workers_Compensation__c WC {get;set;}
public string wid = ApexPages.currentPage().getParameters().get('wid');
public string empviewval= ApexPages.currentPage().getParameters().get('empvw');   
public wct_Workers_compensation_form()
{
mainpage = true;
tqpage=false;
WC = [select id,Employer_s_Insured_s_Name_Use_Applica__c, name,Street_Address_Assigned_Office_for_Empl__c,Suite_Floor_Room_No__c, City_State_Zip_Code__c,Location_Code__c,US_workers_compensation_coordinator_Nam__c,US_workers_compensation_coordinator_Pho__c,US_workers_compensation_coordinator_Fax__c,Location_of_Human_Resource_Contact_If_D__c,Street_Address__c,HR_Suite_Floor_Room_No__c,HR_City_State_Zip_Code__c,US_workers_compensation_coordinator_Ema__c,
Status__c,Employee_Name__c,SAP_ID__c,Function__c,Location__c,Date_Notification_Received__c,Home_Address__c,Home_City_State_Zip_Code__c,Home_Phone_Including_Area_Code__c,Gender__c,Marital_Status__c,Number_of_Dependents__c,Did_the_employee_have_prior_accidents_i__c,Are_there_any_known_medical_conditions_o__c,Pre_existing_Disabilities__c,If_yes_Explain__c,Employee_Hire_Date__c,Employee_Job_Title__c,Work_Phone__c,Number_of_Hours_Per_Day__c,Name_Days_of_Week_Usually_Worked__c,Employment_Status__c,
Employee_Wage_Rate_per_year__c,Amount__c,Per_Year__c,Did_the_employee_receive_full_pay_for_th__c,Did_or_will_the_employee_s_salary_contin__c,Date_of_Injury_Illness__c,Date_Employer_Notified__c,Date_Disability_Began__c,Did_injury_illness_occur_on_employer_s_p__c,Loss_Location_Name_Address_If_Different__c,Nature_of_Injury_Brief_Description__c,Part_of_Body_Affected__c,Source_Cause_of_Injury__c,Specific_Activity_the_Employee_Was_Engag__c,How_did_this_accident_injury_occur__c,Additional_Comments__c,Initial_Treatment_Describe__c,
Was_Surgery_Required__c,Has_the_claimant_been_directed_to_a_medi__c,Physician_s_Name__c,Address__c,Treatment_City_State_Zip_Code__c,Physician_Phone__c,Hospital_Name__c,Hospital_Phone__c,Type_of_Facility__c,Date_Report_Prepared__c,Name_of_Person_Preparing_Report__c,Preparer_s_Job_Title__c,Report_Address__c,Report_City_State_Zip_Code__c,Report_Preparer_s_Phone__c,Preparer_s_Email_Address__c,Were_there_witnesses__c,Witness_1_Name__c,Witness_1_Address__c,Witness_1_Phone__c,Witness_2_Name__c,Witness_2_Address__c,Witness_2_Phone__c,
Has_the_employee_lost_time_from_work_due__c,Did_the_injury_occur_in_the_employee_s_d__c ,Is_the_employee_receiving_any_pay_from_e__c,Date_Last_Worked__c,Has_the_employee_returned_to_work__c,If_returned_to_work_are_there_any_restr__c,Date_Returned_to_Work__c,FRF_send_to_Chartis_AIG__c,Reference_Number__c,Claim_Number__c,Adjuster_Name__c,Adjuster_Contact_details__c,Employee_Email__c from Workers_Compensation__c where id=:wid];//new Workers_Compensation__c();
if(empviewval !=null)
{
    empview=true;
}
    else
    {
       empview=false; 
    }
}

public pagereference pg()
{

Update WC;
mainpage=false;
tqpage=true;
return null;
}
}