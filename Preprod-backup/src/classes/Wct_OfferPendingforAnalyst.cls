global class Wct_OfferPendingforAnalyst implements Database.Batchable<sObject>,Schedulable{

    global void execute(SchedulableContext SC) {
        Wct_OfferPendingforAnalyst batch = new Wct_OfferPendingforAnalyst();
        Integer batchSize = 1;
        ID batchprocessid = Database.executeBatch(batch, batchSize); 
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String strSql = 'SELECT Id,CreatedDate,Name,RecordTypeID,Owner.Email,WCT_Created_Date_Time__c,WCT_Time_sent__c,WCT_Iteration_count__c,Offer_Sent_Track__c,WCT_Candidate_Email__c,WCT_Team_Mailbox__c,WCT_RMS_ID__c,WCT_Full_Name__c,WCT_status__c,WCT_AgentState_Time__c, WCT_User_Group__c' +
                     ' FROM WCT_Offer__c'+
                     ' where WCT_status__c IN (\'Offer to Be Extended\', \'Draft in Progress\', \'Returned for Revisions\', \'Offer Approved\', \'Manual (Outside of SFDC)\')'+
                     ' AND owner.email != null'+
                     ' AND Offer_Sent_Track__c = False'+
                     ' AND CreatedDate = LAST_N_DAYS:3'+
                     ' AND RecordType.Name IN (\'US Experienced Hire Offer\', \'US Campus Hire Offer\')'; 
        system.debug('query123******'+strSql);
        return database.getQuerylocator(strSql);
        
    }

    global void execute(Database.BatchableContext BC, List<WCT_Offer__c> scope)
    {
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList2 = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList3 = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList4 = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList5 = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList6 = new List<Messaging.SingleEmailMessage>();
        set<string> setRecEmails = new set<string>();
        set<string> setteamEmails = new set<string>();
        map<string,string> RecTeamEmails = new map<string,string>();
        map<string,string> RecTeamEmails2 = new map<string,string>();
        map<string,string> RecTeamEmails3 = new map<string,string>();
        map<string,string> RecTeamEmails4 = new map<string,string>();
        map<string,string> RecTeamEmails5 = new map<string,string>();
        map<string,string> RecTeamEmails6 = new map<string,string>();
        Map<String, List<WCT_Offer__c>> recOffers = new Map<String, List<WCT_Offer__c>>(); 
        Map<String, List<WCT_Offer__c>> recOffers2 = new Map<String, List<WCT_Offer__c>>();
        Map<String, List<WCT_Offer__c>> recOffers3 = new Map<String, List<WCT_Offer__c>>(); 
        Map<String, List<WCT_Offer__c>> recOffers4 = new Map<String, List<WCT_Offer__c>>();
        Map<String, List<WCT_Offer__c>> recOffers5 = new Map<String, List<WCT_Offer__c>>();
        Map<String, List<WCT_Offer__c>> recOffers6 = new Map<String, List<WCT_Offer__c>>();
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosById();
        Decimal seconds;
        Decimal hrs;
        Decimal min;
        
        
        for(WCT_Offer__c tmp : scope) {
             WCT_Offer__c offerRecord = (WCT_Offer__c) tmp;
             WCT_Offer__c offerRecord2 = (WCT_Offer__c) tmp;
             WCT_Offer__c offerRecord3 = (WCT_Offer__c) tmp;
             WCT_Offer__c offerRecord4 = (WCT_Offer__c) tmp;
             WCT_Offer__c offerRecord5 = (WCT_Offer__c) tmp;
             WCT_Offer__c offerRecord6 = (WCT_Offer__c) tmp;
             system.debug('offerRecord**********'+offerRecord);
             system.debug('offerRecord**********'+offerRecord2);
             system.debug('offerRecord**********'+offerRecord3);
             system.debug('offerRecord**********'+offerRecord4);
             system.debug('offerRecord**********'+offerRecord5);
             system.debug('offerRecord**********'+offerRecord6);
             
             datetime recorddatetime = date.valueof(offerRecord.CreatedDate);
             system.debug('dt**********'+recorddatetime);
             
             seconds = BusinessHours.diff(Label.WCT_Default_Business_Hours_ID, offerRecord.WCT_Created_Date_Time__c, System.now())/ 1000;
             system.debug('offerRecord.WCT_Created_Date_Time__c**********'+offerRecord.WCT_Created_Date_Time__c);
             system.debug('Label.WCT_Default_Business_Hours_ID**********'+Label.WCT_Default_Business_Hours_ID);
             
             min = Integer.valueOf(seconds / 60);
             hrs = Integer.valueOf(seconds / 3600);
             system.debug('seconds**********'+seconds);
             system.debug('hrs11**********'+hrs);
             system.debug('min**********'+min);
             
             system.debug('recordtype**********'+rtMap.get(offerRecord.RecordTypeId).getName());
             //-------------------1st---------------------------
             if(rtMap.get(offerRecord.RecordTypeId).getName() == 'US Experienced Hire Offer' &&  min >=240 && min < 255 && (offerRecord.WCT_Iteration_count__c == 0 || offerRecord.WCT_Iteration_count__c == null)) {    //hrs >= 4 && hrs < 5  ||  min >=238 && min < 257
                 system.debug('recordtype1**********'+rtMap.get(offerRecord.RecordTypeId).getName());
                setRecEmails.add(offerRecord.Owner.Email);
                RecTeamEmails.put(offerRecord.Owner.Email,offerRecord.WCT_Team_Mailbox__c);
                system.debug('RecTeamEmails:'+RecTeamEmails);
                List<WCT_Offer__c> offerList = recOffers.get(offerRecord.Owner.Email);
                if(offerList == null) {
                    offerList = new List<WCT_Offer__c>();
                } 
                offerList.add(offerRecord);
                recOffers.put(offerRecord.Owner.Email,offerList);
                
            }
            //-------------------2nd---------------------------
           if(rtMap.get(offerRecord2.RecordTypeId).getName() == 'US Experienced Hire Offer' && min >=360 && min < 375 && (offerRecord2.WCT_Iteration_count__c == 1 || offerRecord2.WCT_Iteration_count__c == null)) {   //hrs >= 6 && hrs < 7
                system.debug('alert2**********');
                setRecEmails.add(offerRecord2.Owner.Email);
                RecTeamEmails2.put(offerRecord2.Owner.Email,offerRecord2.WCT_Team_Mailbox__c);
                system.debug('RecTeamEmails2:'+RecTeamEmails2);
                List<WCT_Offer__c> offerList2 = recOffers2.get(offerRecord2.Owner.Email);
                if(offerList2 == null) {
                    offerList2 = new List<WCT_Offer__c>();
                } 
                offerList2.add(offerRecord2);
                recOffers2.put(offerRecord2.Owner.Email,offerList2);
                
            }
            //-------------------3rd---------------------------
            
            if(rtMap.get(offerRecord3.RecordTypeId).getName() == 'US Experienced Hire Offer' && min >= 480 && min < 495 && (offerRecord3.WCT_Iteration_count__c == 2 || offerRecord3.WCT_Iteration_count__c == null)) {    //hrs >= 8 && hrs < 9
                system.debug('alert3**********');
                setRecEmails.add(offerRecord3.Owner.Email);
                RecTeamEmails3.put(offerRecord3.Owner.Email,offerRecord3.WCT_Team_Mailbox__c);
                system.debug('RecTeamEmails3:'+RecTeamEmails3);
                List<WCT_Offer__c> offerList3 = recOffers3.get(offerRecord3.Owner.Email);
                if(offerList3 == null) {
                    offerList3 = new List<WCT_Offer__c>();
                } 
                offerList3.add(offerRecord3);
                recOffers3.put(offerRecord3.Owner.Email,offerList3);
                
            }
            //-------------------4th---------------------------
            if(rtMap.get(offerRecord4.RecordTypeId).getName() == 'US Campus Hire Offer' && min >= 480 && min < 495 && (offerRecord4.WCT_Iteration_count__c == 0 || offerRecord4.WCT_Iteration_count__c == null)) {  //hrs >= 8 && hrs < 9
                setRecEmails.add(offerRecord4.Owner.Email);
                RecTeamEmails4.put(offerRecord4.Owner.Email,offerRecord4.WCT_Team_Mailbox__c);
                system.debug('RecTeamEmails4:'+RecTeamEmails4);
                List<WCT_Offer__c> offerList4 = recOffers4.get(offerRecord4.Owner.Email);
                if(offerList4 == null) {
                    offerList4 = new List<WCT_Offer__c>();
                } 
                offerList4.add(offerRecord4);
                recOffers4.put(offerRecord4.Owner.Email,offerList4);
                
            }
            //-------------------5th---------------------------
            if(rtMap.get(offerRecord5.RecordTypeId).getName() == 'US Campus Hire Offer' && min >= 840 && min < 855 && (offerRecord5.WCT_Iteration_count__c == 1 || offerRecord5.WCT_Iteration_count__c == null)) { //hrs >= 14 && hrs < 15
                setRecEmails.add(offerRecord5.Owner.Email);
                RecTeamEmails5.put(offerRecord5.Owner.Email,offerRecord5.WCT_Team_Mailbox__c);
                system.debug('RecTeamEmails5:'+RecTeamEmails5);
                List<WCT_Offer__c> offerList5 = recOffers5.get(offerRecord5.Owner.Email);
                if(offerList5 == null) {
                    offerList5 = new List<WCT_Offer__c>();
                } 
                offerList5.add(offerRecord5);
                recOffers5.put(offerRecord5.Owner.Email,offerList5);
                
            }
            //-------------------6th---------------------------
            
            if(rtMap.get(offerRecord6.RecordTypeId).getName() == 'US Campus Hire Offer' && min >= 1080 && min < 1095 && (offerRecord6.WCT_Iteration_count__c == 2 || offerRecord6.WCT_Iteration_count__c == null)) {  //hrs >= 18 && hrs < 19
                setRecEmails.add(offerRecord6.Owner.Email);
                RecTeamEmails6.put(offerRecord6.Owner.Email,offerRecord6.WCT_Team_Mailbox__c);
                system.debug('RecTeamEmails6:'+RecTeamEmails6);
                List<WCT_Offer__c> offerList6 = recOffers6.get(offerRecord6.Owner.Email);
                if(offerList6 == null) {
                    offerList6 = new List<WCT_Offer__c>();
                } 
                offerList6.add(offerRecord6);
                recOffers6.put(offerRecord6.Owner.Email,offerList6);
                
            }
            //-------------------------------------------------
        }
        
        List<WCT_Offer__c> offertrack = new List<WCT_Offer__c>();
        
        String strEmailTop ='';
        String strEmailBody ='';
        String strEmailBottom ='';
        
        for(string strRecEmail: recOffers.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord : recOffers.get(strRecEmail)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 4 Hours.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord.Name+'</td> <td>'+offerRecord.WCT_full_Name__c+'</td><td>'+offerRecord.WCT_RMS_ID__c+'</td><td>'+offerRecord.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
                //if(offerRecord.WCT_Iteration_count__c == null && offerRecord.WCT_Time_sent__c == null){
                    offertrack.add(new WCT_Offer__c(Id = offerRecord.id, WCT_Iteration_count__c = 1, WCT_Time_sent__c = string.valueof(system.now())));
                    //offertrack.add(new WCT_Offer__c( WCT_Time_sent__c = string.valueof(system.now())));
                //}
                
                system.debug('offertrack**********'+offertrack);
                update offertrack;
            }
            list<string> ToEmailAddress = new list<string>();
            List<string> ToCcAddress = new List<string>();
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            ToEmailAddress.add(strRecEmail);
            system.debug('strRecEmail1:'+strRecEmail);
            if(RecTeamEmails != null && RecTeamEmails.get(strRecEmail) != null){
                 ToCcAddress.add(RecTeamEmails.get(strRecEmail));
            }
            system.debug('strRecEmail:'+strRecEmail);
            system.debug('ToCcAddress:'+ToCcAddress);
            
            mail.settoAddresses(ToEmailAddress);
            system.debug('ToEmailAddress:'+ToEmailAddress);
            mail.setccAddresses(ToCcAddress);
            system.debug('ToCcAddress1:'+ToCcAddress);
            mail.setSubject('1st Alert Immediate Action: Offer Letter(s) are pending');
            
            mail.setHTMLBody('<font color="green">'+strEmailTop+'</font>'+'<font color="green">'+strEmailBody+'</font>'+'<font color="green">'+strEmailBottom+'</font>');
            
            mail.setSaveAsActivity(false);
            mailList.add(mail);  
        
        }
        //------------------2nd------------------------------------------------------------------------------------------------
        List<WCT_Offer__c> offertrack2 = new List<WCT_Offer__c>();
        for(string strRecEmail2 : recOffers2.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord2 : recOffers2.get(strRecEmail2)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 6 Hours.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord2.Name+'</td> <td>'+offerRecord2.WCT_full_Name__c+'</td><td>'+offerRecord2.WCT_RMS_ID__c+'</td><td>'+offerRecord2.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord2.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord2.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
                //List<WCT_Offer__c> offertrack2 = new List<WCT_Offer__c>();
                
                offertrack2.add(new WCT_Offer__c(Id = offerRecord2.id, WCT_Iteration_count__c = 2, WCT_Time_sent__c = offerRecord2.WCT_Time_sent__c +' ; '+string.valueof(system.now())));
                update offertrack2;
            }
            list<string> ToEmailAddress2 = new list<string>();
            List<string> ToCcAddress2 = new List<string>();
            
            Messaging.SingleEmailMessage mail2 = new Messaging.SingleEmailMessage();
            ToEmailAddress2.add(strRecEmail2);
            system.debug('strRecEmail2:'+strRecEmail2);
            if(RecTeamEmails2 != null && RecTeamEmails2.get(strRecEmail2) != null){
                 ToCcAddress2.add(RecTeamEmails2.get(strRecEmail2));
            }
            system.debug('strRecEmail2:'+strRecEmail2);
            system.debug('ToCcAddress:'+ToCcAddress2);
            
            mail2.settoAddresses(ToEmailAddress2);
            system.debug('ToEmailAddress:'+ToEmailAddress2);
            
                ToCcAddress2.add(system.label.OfferAnalystNotification1);
                ToCcAddress2.add(system.label.OfferAnalystNotification2);
                
            mail2.setccAddresses(ToCcAddress2);
            system.debug('ToCcAddress1:'+ToCcAddress2);
            mail2.setSubject('2nd Alert Immediate Action: Offer Letter(s) are pending');
            
            mail2.setHTMLBody('<font color="orange">'+strEmailTop+'</font>'+'<font color="orange">'+strEmailBody+'</font>'+'<font color="orange">'+strEmailBottom+'</font>');
            
            mail2.setSaveAsActivity(false);
            mailList2.add(mail2);  
        
        }
        //------------------3rd------------------------------------------------------------------------------------------------
        List<WCT_Offer__c> offertrack3 = new List<WCT_Offer__c>();
        for(string strRecEmail3 : recOffers3.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord3 : recOffers3.get(strRecEmail3)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 8 Hours.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord3.Name+'</td> <td>'+offerRecord3.WCT_full_Name__c+'</td><td>'+offerRecord3.WCT_RMS_ID__c+'</td><td>'+offerRecord3.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord3.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord3.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
                offertrack3.add(new WCT_Offer__c(Id = offerRecord3.id, WCT_Iteration_count__c = 3, WCT_Time_sent__c = offerRecord3.WCT_Time_sent__c +' ; '+string.valueof(system.now())));
                update offertrack3;
            }
            list<string> ToEmailAddress3 = new list<string>();
            List<string> ToCcAddress3 = new List<string>();
            
            Messaging.SingleEmailMessage mail3 = new Messaging.SingleEmailMessage();
            ToEmailAddress3.add(strRecEmail3);
            system.debug('strRecEmail3:'+strRecEmail3);
            if(RecTeamEmails3 != null && RecTeamEmails3.get(strRecEmail3) != null){
                 ToCcAddress3.add(RecTeamEmails3.get(strRecEmail3));
            }
            system.debug('strRecEmail3:'+strRecEmail3);
            system.debug('ToCcAddress:'+ToCcAddress3);
            
                ToCcAddress3.add(system.label.OfferAnalystNotification1);
                ToCcAddress3.add(system.label.OfferAnalystNotification2);
                ToCcAddress3.add(system.label.OfferAnalystNotification3);
                //ToCcAddress3.add('pjamalapuram@deloitte.com');
                //ToCcAddress3.add('rkomma@deloitte.com');
                //ToCcAddress3.add('eupadhyay@deloitte.com');
                
            mail3.settoAddresses(ToEmailAddress3);
            system.debug('ToEmailAddress:'+ToEmailAddress3);
            mail3.setccAddresses(ToCcAddress3);
            system.debug('ToCcAddress1:'+ToCcAddress3);
            mail3.setSubject('3rd Alert Immediate Action: Offer Letter(s) are pending');
            
            mail3.setHTMLBody('<font color="Red">'+strEmailTop+'</font>'+'<font color="Red">'+strEmailBody+'</font>'+'<font color="Red">'+strEmailBottom+'</font>');
            
            mail3.setSaveAsActivity(false);
            mailList3.add(mail3);  
        
        }
        //------------------4th------------------------------------------------------------------------------------------------
        List<WCT_Offer__c> offertrack4 = new List<WCT_Offer__c>();
        for(string strRecEmail4 : recOffers4.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord4 : recOffers4.get(strRecEmail4)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 8 Hours.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord4.Name+'</td> <td>'+offerRecord4.WCT_full_Name__c+'</td><td>'+offerRecord4.WCT_RMS_ID__c+'</td><td>'+offerRecord4.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord4.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord4.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
                offertrack4.add(new WCT_Offer__c(Id = offerRecord4.id, WCT_Iteration_count__c = 1, WCT_Time_sent__c = string.valueof(system.now())));
                update offertrack4;
            }
            list<string> ToEmailAddress4 = new list<string>();
            List<string> ToCcAddress4 = new List<string>();
            
            Messaging.SingleEmailMessage mail4 = new Messaging.SingleEmailMessage();
            ToEmailAddress4.add(strRecEmail4);
            system.debug('strRecEmail4:'+strRecEmail4);
            if(RecTeamEmails4 != null && RecTeamEmails4.get(strRecEmail4) != null){
                 ToCcAddress4.add(RecTeamEmails4.get(strRecEmail4));
            }
            system.debug('strRecEmail4:'+strRecEmail4);
            system.debug('ToCcAddress:'+ToCcAddress4);
            
            mail4.settoAddresses(ToEmailAddress4);
            system.debug('ToEmailAddress:'+ToEmailAddress4);
            mail4.setccAddresses(ToCcAddress4);
            system.debug('ToCcAddress1:'+ToCcAddress4);
            mail4.setSubject('1st Alert Immediate Action: Offer Letter(s) are pending');
            
            mail4.setHTMLBody('<font color="Green">'+strEmailTop+'</font>'+'<font color="Green">'+strEmailBody+'</font>'+'<font color="Green">'+strEmailBottom+'</font>');
            
            mail4.setSaveAsActivity(false);
            mailList4.add(mail4);  
        
        }
        
        //------------------5th------------------------------------------------------------------------------------------------
        List<WCT_Offer__c> offertrack5 = new List<WCT_Offer__c>();
        for(string strRecEmail5 : recOffers5.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord5 : recOffers5.get(strRecEmail5)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 14 Hours.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord5.Name+'</td> <td>'+offerRecord5.WCT_full_Name__c+'</td><td>'+offerRecord5.WCT_RMS_ID__c+'</td><td>'+offerRecord5.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord5.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord5.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
                offertrack5.add(new WCT_Offer__c(Id = offerRecord5.id, WCT_Iteration_count__c = 2, WCT_Time_sent__c = offerRecord5.WCT_Time_sent__c +' ; '+string.valueof(system.now())));
                update offertrack5;
            }
            list<string> ToEmailAddress5 = new list<string>();
            List<string> ToCcAddress5 = new List<string>();
            
            Messaging.SingleEmailMessage mail5 = new Messaging.SingleEmailMessage();
            ToEmailAddress5.add(strRecEmail5);
            system.debug('strRecEmail5:'+strRecEmail5);
            if(RecTeamEmails5 != null && RecTeamEmails5.get(strRecEmail5) != null){
                 ToCcAddress5.add(RecTeamEmails5.get(strRecEmail5));
            }
            system.debug('strRecEmail5:'+strRecEmail5);
            system.debug('ToCcAddress:'+ToCcAddress5);
            
            mail5.settoAddresses(ToEmailAddress5);
            system.debug('ToEmailAddress:'+ToEmailAddress5);
            
            ToCcAddress5.add(system.label.OfferAnalystNotification1);
            ToCcAddress5.add(system.label.OfferAnalystNotification2);
            
            mail5.setccAddresses(ToCcAddress5);
            system.debug('ToCcAddress1:'+ToCcAddress5);
            mail5.setSubject('2nd Alert Immediate Action: Offer Letter(s) are pending');
            
            mail5.setHTMLBody('<font color="orange">'+strEmailTop+'</font>'+'<font color="orange">'+strEmailBody+'</font>'+'<font color="orange">'+strEmailBottom+'</font>');
            
            mail5.setSaveAsActivity(false);
            mailList5.add(mail5);  
        
        }
        //------------------6th------------------------------------------------------------------------------------------------
        List<WCT_Offer__c> offertrack6 = new List<WCT_Offer__c>();
        for(string strRecEmail6 : recOffers6.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord6 : recOffers6.get(strRecEmail6)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 18 Hours.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord6.Name+'</td> <td>'+offerRecord6.WCT_full_Name__c+'</td><td>'+offerRecord6.WCT_RMS_ID__c+'</td><td>'+offerRecord6.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord6.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord6.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
                offertrack6.add(new WCT_Offer__c(Id = offerRecord6.id, WCT_Iteration_count__c = 3, WCT_Time_sent__c = offerRecord6.WCT_Time_sent__c +' ; '+string.valueof(system.now())));
                update offertrack6;
            }
            list<string> ToEmailAddress6 = new list<string>();
            List<string> ToCcAddress6 = new List<string>();
            
            Messaging.SingleEmailMessage mail6 = new Messaging.SingleEmailMessage();
            ToEmailAddress6.add(strRecEmail6);
            system.debug('strRecEmail6:'+strRecEmail6);
            if(RecTeamEmails6 != null && RecTeamEmails6.get(strRecEmail6) != null){
                 ToCcAddress6.add(RecTeamEmails6.get(strRecEmail6));
            }
            system.debug('strRecEmail6:'+strRecEmail6);
            system.debug('ToCcAddress:'+ToCcAddress6);
            
            mail6.settoAddresses(ToEmailAddress6);
            system.debug('ToEmailAddress:'+ToEmailAddress6);
            
                ToCcAddress6.add(system.label.OfferAnalystNotification1);
                ToCcAddress6.add(system.label.OfferAnalystNotification2);
                ToCcAddress6.add(system.label.OfferAnalystNotification3);
                
            mail6.setccAddresses(ToCcAddress6);
            system.debug('ToCcAddress1:'+ToCcAddress6);
            mail6.setSubject('3rd Alert Immediate Action: Offer Letter(s) are pending');
            
            mail6.setHTMLBody('<font color="Red">'+strEmailTop+'</font>'+'<font color="Red">'+strEmailBody+'</font>'+'<font color="Red">'+strEmailBottom+'</font>');
            
            mail6.setSaveAsActivity(false);
            mailList6.add(mail6);  
        
        }
        //------------------------------------------------------------------------------------------------------------------
        
            Messaging.sendEmail(mailList); 
            Messaging.sendEmail(mailList2);
            Messaging.sendEmail(mailList3);
            Messaging.sendEmail(mailList4); 
            Messaging.sendEmail(mailList5);
            Messaging.sendEmail(mailList6);
    }

    global void finish(Database.BatchableContext BC){
      
    }
}