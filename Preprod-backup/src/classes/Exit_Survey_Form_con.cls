/********************************************
Apex class       : Exit_Survey_Form_con
Version          : 1.0 
Created Date     : 27 June 2015
Function         : Controller for Exit survey sent to user via email
Test Class       : ExitSurveyForm_Test

* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   27/06/2015            Original Version
*************************************************************************************/

public with sharing class Exit_Survey_Form_con {
    
    public Exit_Survey__c oexitsurvey {get;set;}
    public List<SelectOption> options {get;set;} 
    public List<SelectOption> worklifebaloptions{get;set;}
    public List<SelectOption> feedbackoptions{get;set;}
    public List<SelectOption> alumnioptions{get;set;}
    public List<SelectOption> discussionwthoptions{get;set;}
    public List<SelectOption> Fssoptions{get;set;}
    public string selectedopt {get;set;}
    public Decimal Personnelno {get;set;}
    
    //constructor retrieving all the fields
    public Exit_Survey_Form_con (){ 
      
        String surveyID = ApexPages.currentPage().getParameters().get('sid');  //sid is exitsurvey id here
        oexitsurvey = new Exit_Survey__c ();
        
        oexitsurvey = [SELECT Id,Name,ELE_Separation_Case__r.ELE_Name__c,ELE_Separation_Case__r.ELE_Last_Date__c,ELE_Separation_Case__r.ELE_FSS__c,ELE_USI_Location__c,ELE_Separation_Case__r.ELE_Service_Line__c,ELE_Designation__c,ELE_Immediate_Reporting_Manager__c,ELE_Shift_Timings__c,ELE_long_leave_Deloitte_before_resigned__c,ELE_Any_other_reason_your_Exit__c  
                       ,ELE_Secondary_reason_leaving_Deloitte__c,ELE_Separation_Case__r.ELE_Service_Area__c,ELE_Flexibility_benefits_did_use__c,ELE_most_during_your_tenure_Deloitte__c,ELE_least_during_your_tenure_Deloitte__c,ELE_what_you_expected_you_first_joined__c,
                       ELE_Feedback_from_your_Manager_Deloitte__c,ELE_Which_Flexibility_benefits_you_use__c,ELE_share_feedback_inputs_you_may_have__c,ELE_Which_benefit_help_worklife_balance__c,ELE_primary_reason_leaving_Deloitte__c ,ELE_Area_focus_retain_its_top_talent__c,ELE_Separation_Case__r.ELE_Contact_Email__c,
                       ELE_Share_name_of_your_new_employer__c,ELE_What_attracted_your_new_employer__c,ELE_Percent_hike_from_next_employer__c, ELE_Like_Deloitte_Adopt_Alumni_program__c,ELE_Separation_Case__r.ELE_Gender__c,ELE_Separation_Case__r.Designation_As_Per_SAP__c,
                       ELE_current_Deloitte_professional_touch__c,ELE_What_New_benefits_your_next_Employer__c,ELE_share_your_personal_email_ID__c,ELE_share_phone_number__c ,ELE_Career_Level__c,ELE_Entity_Name__c,ELE_Separation_Case__r.ELE_DOJ__c,ELE_Separation_Case__r.ELE_RC_Code__c,ELE_Separation_Case__r.ELE_DOJ_1__c       
                       ,ELE_If_others_who_had_a_discussion_reg_R__c,ELE_Exit_Interview_Conducted_Employee_ID__c,ELE_Exit_Interview_Conducted_by__c,ELE_About_your_exit_Who_discussion_with__c,ELE_Capability_If_it_Consulting__c,ELE_Separation_Case__r.ELE_Personnel_Number__c,ELE_Separation_Case__r.ELE_EntityName__c,ELE_Separation_Case__r.ELE_Contact__r.Name,ELE_Separation_Case__r.ELE_Contact__c
                       ,Other_than_on_your_Deloitte_issued_la__c,Personal_Did_a_PPD_approve_the_usage__c,Have_you_sent_any_Deloitte_or_client__c,sent_any_Deloitte_or_client_Confidential__c
                       ,provide_details_so_that_we_can_take_step__c,Anyone_Did_a_PPD_approve_the_usage__c,Have_you_backed_up_your_Deloitte_issued__c,What_data_did_you_back_up_or_store__c 
                       ,Other_than_backing_up_your_Deloitte_issu__c,Have_you_otherwise_disclosed_any_Deloitt__c,CCI_provide_details_so_that_we_can_take__c,Do_you_understand_your_continuing_obliga__c,CommentsPR__c,CommentSR__c,CommentMD__c,CommentLD__c,CommentBH__c FROM Exit_Survey__c WHERE ID=:surveyID];
        
        //Assigning recordtype 'USI' to Exit survey
        
        Id recdtype = Schema.SObjectType.Exit_Survey__c.getRecordTypeInfosByName().get('USI').getRecordTypeId();
        
        oexitsurvey.recordtypeId=recdtype;
        
        
        getoptions();
        getworklifebaloptions();
        getfeedbackoptions();
        getalumnioptions();
        getdicussionoptions();
        getFssoptions();
        selectedopt = '';
        
    }
    
    // This is the method to retrieve picklist values for 'Which of the Deloitte flexibility benefits did you use?:'. 
    
    Public List<SelectOption> getoptions(){
        options = new List<SelectOption>();
        for(Schema.PicklistEntry pic: Exit_Survey__c.ELE_Which_Flexibility_benefits_you_use__c.getDescribe().getPicklistValues()){
            
            if(pic.isDefaultValue()){
                oexitsurvey.ELE_Which_Flexibility_benefits_you_use__c = pic.getValue();
            }
            options.add(new Selectoption(pic.getValue(), pic.getLabel()));       
        }
        return options;
    }
    
    // This is the method to retrieve picklist values for 'Did you find these benefits helpful towards your work-life balance?:'. 
    
    Public List<SelectOption> getworklifebaloptions(){
        worklifebaloptions = new List<SelectOption>();
        for(Schema.PicklistEntry pic: Exit_Survey__c.ELE_Which_benefit_help_worklife_balance__c.getDescribe().getPicklistValues()){
            
            if(pic.isDefaultValue()){
                oexitsurvey.ELE_Which_benefit_help_worklife_balance__c = pic.getValue();
            }
            worklifebaloptions.add(new Selectoption(pic.getValue(), pic.getLabel()));       
        }
        return worklifebaloptions;
    }
    
    // Method to retrieve picklist values for 'Did you get sufficient and appropriate feedback/guidance from your immediate Manager at Deloitte?:'. 
    Public List<SelectOption> getfeedbackoptions(){
        feedbackoptions = new List<SelectOption>();
        for(Schema.PicklistEntry pic: Exit_Survey__c.ELE_Feedback_from_your_Manager_Deloitte__c.getDescribe().getPicklistValues()){
            
            if(pic.isDefaultValue()){
                oexitsurvey.ELE_Feedback_from_your_Manager_Deloitte__c = pic.getValue();
            }
            feedbackoptions.add(new Selectoption(pic.getValue(), pic.getLabel()));       
        }
        return feedbackoptions;
    }
    
    // Method to retrieve picklist values for 'Would you like to participate in Deloitte's 'Adopt Alumni' program?'.   
    Public List<SelectOption> getalumnioptions(){
        alumnioptions = new List<SelectOption>();
        for(Schema.PicklistEntry pic: Exit_Survey__c.ELE_Like_Deloitte_Adopt_Alumni_program__c.getDescribe().getPicklistValues()){
            
            if(pic.isDefaultValue()){
                oexitsurvey.ELE_Like_Deloitte_Adopt_Alumni_program__c = pic.getValue();
            }
            alumnioptions.add(new Selectoption(pic.getValue(), pic.getLabel()));       
        }
        return alumnioptions;
    }
    // Method to retrieve picklist values for 'After you resigned, who had a discussion with you regarding your exit?:'.   
    
    Public List<SelectOption> getdicussionoptions(){
        discussionwthoptions = new List<SelectOption>();
        for(Schema.PicklistEntry pic: Exit_Survey__c.ELE_About_your_exit_Who_discussion_with__c.getDescribe().getPicklistValues()){
            
            if(pic.isDefaultValue()){
                oexitsurvey.ELE_About_your_exit_Who_discussion_with__c = pic.getValue();
            }
            discussionwthoptions.add(new Selectoption(pic.getValue(), pic.getLabel()));       
        }
        return discussionwthoptions;
    }
    
    // Method  to retrieve picklist values for 'Fss Options'.   
    Public List<SelectOption> getFssoptions(){
        Fssoptions = new List<SelectOption>();
        Fssoptions.add(new Selectoption('--None--','--None--'));       
        
        for(Schema.PicklistEntry pic: ELE_Separation__c.ELE_FSS__c.getDescribe().getPicklistValues()){
            
            if(pic.isDefaultValue()){
                oexitsurvey.ELE_Separation_Case__r.ELE_FSS__c = pic.getValue();
            }
            Fssoptions.add(new Selectoption(pic.getValue(), pic.getLabel()));       
        }
        return Fssoptions;
    }
    
    public String[] discussedopt{ 
        get {
            String[] selectedval = new List<String>();
            List<SelectOption> soss = this.discussionwthoptions;
            for(SelectOption s : soss) {
                if (this.oexitsurvey.ELE_About_your_exit_Who_discussion_with__c!=null && this.oexitsurvey.ELE_About_your_exit_Who_discussion_with__c.contains(s.getValue()))
                    selectedval.add(s.getValue());
            }
            return selectedval;
        }public set {
            String selectedConcatval = '';
            for(String s : value) {
                if (selectedConcatval == '') 
                    selectedConcatval += s;
                else selectedConcatval += ';' + s;
            }
            oexitsurvey.ELE_About_your_exit_Who_discussion_with__c= selectedConcatval;
        }
    } 
    
    public String[] selectedItems { 
        get {
            String[] selected = new List<String>();
            List<SelectOption> sos = this.options;
            for(SelectOption s : sos) {
                if (this.oexitsurvey.ELE_Which_Flexibility_benefits_you_use__c!=null && this.oexitsurvey.ELE_Which_Flexibility_benefits_you_use__c.contains(s.getValue()))
                    selected.add(s.getValue());
            }
            return selected;
        }public set {
            String selectedConcat = '';
            for(String s : value) {
                if (selectedConcat == '') 
                    selectedConcat += s;
                else selectedConcat += ';' + s;
            }
            oexitsurvey.ELE_Which_Flexibility_benefits_you_use__c= selectedConcat;
        }
    } 
    
    // Save Method 
    Public Pagereference submitsurvey(){
        try{    
            oexitsurvey.Employee__c= oexitsurvey.ELE_Separation_Case__r.ELE_Contact__c;
            Database.Update(oexitsurvey,false);
        }
        catch(Exception e)
        {
            Exception_Log__c log = WCT_ExceptionUtility.logException('Exit_Survey_Form_con', 'Exit_Survey_Form', e.getMessage());
            
            pagereference pg = new pagereference('/apex/GBL_Page_Notification?key=ErrorMsg&expCode='+log.Name);
            pg.setRedirect(true);
            return pg; 
        } 
        PageReference pageRef = Page.GBL_Page_Notification; 
        pageRef.getParameters().put('key','Survey_success');
        return pageRef  ;
        
    }
    
    
    
}