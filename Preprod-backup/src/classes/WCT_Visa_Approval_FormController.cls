/********************************************************************************************************************************
Apex class         : <WCT_Visa_Approval_FormController>
Description        : <Controller which allows to Update Task and Immigration>
Type               :  Controller
Test Class         : <WCT_Visa_Approval_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 24/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/ 
public class WCT_Visa_Approval_FormController extends SitesTodHeaderController{

     //PUBLIC VARIABLES
     public WCT_Immigration__c ImmigrationRecord{get;set;}
     
     //UPLOAD RELATED VARIABLES
     public Document doc {get;set;}
     public List<String> docIdList = new List<string>();
     public Task taskObj{get;set;}
     public String taskID{get;set;}
     public List<Attachment> listAttachments {get; set;}
     public Map<Id, String> mapAttachmentSize {get; set;} 
     public GBL_Attachments attachmentHelper{get; set;}
     
    //ERROR MESSAGE RELATED VARIABLES
     public boolean pageError {get; set;}
     public String pageErrorMessage {get; set;}
     public String supportAreaErrorMesssage {get; set;}   
    
    //VISA RELATED VARIABLES
     
     public String visaStartDate {get;set;}
     public String visaEndDate {get;set;}
     
     /* Invoking a Constructor with Task and Attachments  */
     public WCT_Visa_Approval_FormController()
     {  
          attachmentHelper= new GBL_Attachments();   
            init();
            getParameterInfo();  
            if(taskID =='' || taskID ==null)
            {
               invalidEmployee=true;
               return;
            }
            getTaskInstance();
            getAttachmentInfo(); 
            getImmigrationDetails();
     }

/********************************************************************************************
*Method Name         : <init()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Init() Used for loading Immigration,Attachments>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/      
     
     private void init(){
           ImmigrationRecord= new WCT_Immigration__c (); 
           doc = new Document();
           listAttachments = new List<Attachment>();
           mapAttachmentSize = new Map<Id, String>();
     }   

/********************************************************************************************
*Method Name         : <getParameterInfo()>
*Return Type         : <Null>
*Param’s             : URL
*Description         : <GetParameterInfo() Used to get URL Params for TaskID>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/        
     
     private void getParameterInfo(){
           taskID = ApexPages.currentPage().getParameters().get('taskid');
     }

/********************************************************************************************
*Method Name         : <getTaskInstance()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetTaskInstance() Used for Querying Task Instance>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/    
     
     private void getTaskInstance(){
          taskObj =[SELECT Status, OwnerId, WhatId, WCT_Auto_Close__c, WCT_Is_Visible_in_TOD__c  FROM Task WHERE Id =: taskid];
     }
    
/********************************************************************************************
*Method Name         : <getAttachmentInfo()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetAttachmentInfo() Used for Retrieving Attachment Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/
     
     @TestVisible
     private void getAttachmentInfo(){
     listAttachments = [SELECT ParentId , 
                                        Name, 
                                        BodyLength, 
                                        Id,
                                        CreatedDate
                                        FROM  Attachment 
                                        WHERE ParentId = :taskID
                                        ORDER BY CreatedDate DESC
                                        LIMIT 50 ];    
                  
            for(Attachment a : listAttachments) {
                String size = null;
    
                if(1048576 < a.BodyLength){
                    //SIZE GREATAR THAN 1MB
                    size = '' + (a.BodyLength / 1048576) + ' MB';
                }
                else if(1024 < a.BodyLength){
                    //SIZE GREATER THAN 1KB
                    size = '' + (a.BodyLength / 1024) + ' KB';            
                }
                else{
                    size = '' + a.BodyLength + ' bytes';
                }
                mapAttachmentSize.put(a.id, size);
            }
      }
      
/********************************************************************************************
*Method Name         : <getImmigrationDetails()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetImmigrationDetails() Used for Fetching Immigration Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/    
    
    public void getImmigrationDetails()
    {
        ImmigrationRecord = [SELECT   Id,
                                         Name,
                                         WCT_Visa_Expiration_Date__c,
                                         WCT_Visa_Interview_Q_A__c,
                                         WCT_Visa_Start_Date__c,
                                         WCT_Visa_Status__c,
                                         WCT_Immigration_Status__c ,
                                         WCT_Visa_Type__c ,
                                         WCT_Reason_for_Denial__c,
                                         WCT_Visa_Approval_Status__c,
                                         Ownerid
                                         FROM WCT_Immigration__c 
                                         where id=:taskObj.WhatId ];
      
    }
    
/********************************************************************************************
*Method Name         : <save()>
*Return Type         : <PageReference>
*Param’s             : 
*Description         : <Save() Used for for Updating Task and Mobility>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/    
   
    public pageReference save()
    { 
        /*Custom Validation Rules are handled here*/
        
            if( (null == visaStartDate) ||('' == visaStartDate) || (null == visaEndDate) ||('' == visaEndDate) || (attachmentHelper.docIdList.isEmpty()))
                {
                pageErrorMessage = 'Please fill in all the required fields on the form.';
                pageError = true;
                return null;
            }
            if(String.isNotBlank(visaStartDate)){
                ImmigrationRecord.WCT_Visa_Start_Date__c = Date.parse(visaStartDate);
            }
            if(String.isNotBlank(visaEndDate)){
                ImmigrationRecord.WCT_Visa_Expiration_Date__c = Date.parse(visaEndDate);
            }
            if(ImmigrationRecord.WCT_Visa_Expiration_Date__c<date.today())
            {
                pageErrorMessage = 'Visa Expiration Date cannot be in the past.';
                pageError = true;
                return null;
            }
            
            if(ImmigrationRecord.WCT_Visa_Start_Date__c>ImmigrationRecord.WCT_Visa_Expiration_Date__c)
            {
                pageErrorMessage = 'Visa Expiration Date cannot be less then Visa Start Date.';
                pageError = true;
                return null;
            }
            
            /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
            try {
            taskObj.OwnerId=UserInfo.getUserId();
            upsert taskObj;
    
            //Update Dates
            if(String.isNotBlank(visaStartDate)){
                ImmigrationRecord.WCT_Visa_Start_Date__c = Date.parse(visaStartDate);
            }
            if(String.isNotBlank(visaEndDate)){
                ImmigrationRecord.WCT_Visa_Expiration_Date__c = Date.parse(visaEndDate);
            }
            
            //Checked if Visa is approved for L1,H1,B1 Visas
            
            if(ImmigrationRecord.WCT_Visa_Approval_Status__c == false) {
                 
                  ImmigrationRecord.WCT_Visa_Approval_Status__c = true;
            }
            
            /*update ImmigrationRecord Record*/
            upsert ImmigrationRecord ;
              attachmentHelper.uploadRelatedAttachment(taskID);   
            //updating task record
            If (taskObj.WCT_Auto_Close__c == true){   
               taskObj.status = 'completed';
            }else{
                taskObj.status = 'Employee Replied';  
            }
    
            if(string.valueOf(ImmigrationRecord.Ownerid).startsWith('00G')){
                taskObj.Ownerid = System.Label.GMI_User; 
            }else{
                 taskObj.OwnerId = ImmigrationRecord.Ownerid;
            }
            taskObj.WCT_Is_Visible_in_TOD__c=false; 
            upsert taskObj;
           
            getAttachmentInfo();
           
            }
            catch (Exception e) {
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Visa_Approval_FormController', 'Visa Approval Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_VAPP_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
            }
            PageReference page=new PageReference('/apex/WCT_Visa_Approval_FormThankYou?taskid='+taskID) ;  
            page.setredirect(true);
            return page;
        
    }
}