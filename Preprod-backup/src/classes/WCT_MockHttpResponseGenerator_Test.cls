@isTest
global class WCT_MockHttpResponseGenerator_Test implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Create a JSON parser
        JSONParser parser = JSON.createParser(req.getBody());
        String taskId = null;
        while (parser.nextToken() != null) {
            taskId = null;            
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'Extra')) {               
                // Parse the taskId
                parser.nextToken();
                taskId = parser.getText(); 
                break;
            }                    
        }
    
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        String responseBody = '[{' +
        '    "Id": 3357,' +
        '    "AudienceFilter": {' +
        '        "AudienceFilter": [{' +
        '            "EntityType": 1' +
        '        }],' +
        '        "ShowAll": false' +
        '    },' +
        '    "Extra":"' +  taskId + '"' +
        '}]';
        
        res.setBody(responseBody);
        res.setStatusCode(200);
        return res;
    }
    
    global HTTPResponse respond1(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');        
        res.setBody('error:error');
        res.setStatusCode(400);
        return res;
    }    
}