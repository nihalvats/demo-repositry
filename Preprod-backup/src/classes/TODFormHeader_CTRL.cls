public class TODFormHeader_CTRL
{

public boolean isGuest{get;set;}
    
public string redirectURL{get;set;}

    public TODFormHeader_CTRL()
    {
    
       if(UserInfo.getUserType()=='Guest')
      {
          isGuest=True;
          redirectURL= Label.BaseURL+ApexPages.currentPage().getUrl();
      }
      else
      {
          isGuest=False;
          redirectURL='';
         
      }
   
    
    }


}