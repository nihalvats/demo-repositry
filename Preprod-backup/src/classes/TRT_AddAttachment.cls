public class TRT_AddAttachment {
    
    public string cfsid;
    Public Attachment myfile;
    public TRT_AddAttachment(ApexPages.StandardController controller){
        cfsid = System.currentPagereference().getParameters().get('id');
    }
   
    Public Attachment getmyfile()
    {
        myfile = new Attachment();
        return myfile;
    }
   
    Public Pagereference Savedoc()
    {
       
        Attachment a = new Attachment();
        list<Case_form_Extn__c> c = new list<Case_form_Extn__c>();
        system.debug('cfsid'+cfsid); 
        if(cfsid != '')
        {
             c=[select GEN_Case__c,TRT_Request_Status__c,TRT_Status_Checkbox__c from Case_form_Extn__c where id=:cfsid limit 1];
            system.debug(c);
        }
        if(!c.isEmpty() && c != null)
        {
            if(myfile != null && myfile.body != null )
            {
                a.parentId = c[0].GEN_Case__c; 
                a.name=myfile.name; 
                a.body = myfile.body;
            }
        }
        /* insert the attachment */
        try{
            insert a;
            c[0].TRT_Status_Checkbox__c=true;
            update c[0];
            PageReference pr = new PageReference('/'+cfsid); 
            pr.setRedirect(true);  
            return pr;
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attachment is not added to case'+ e.getMessage()));
            system.debug('-------exception---------'+e.getMessage()+'----at---Line #----'+e.getLineNumber());
        }
        
      return null;
    } 
    
    Public Pagereference Cancel()
    {
        String accid = System.currentPagereference().getParameters().get('id');
        PageReference pr = new PageReference('/'+accid); 
        pr.setRedirect(true);  
        return pr;
    }

}