global class WCT_scheduledArticleExpiredNotify implements Schedulable{
   global void execute(SchedulableContext SC) {
      WCT_batchArticleExpiredNotify artExp = new WCT_batchArticleExpiredNotify(); 
      Database.executeBatch(artExp,200);
   }
   
}