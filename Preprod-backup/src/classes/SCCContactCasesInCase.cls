/*****************************************************************************************
    Name    : SCCContactCasesInCase
    Desc    : Controller Class to Display All the Contact Cases in Service Cloud Console
    		  'SCCContactCasesInCases' Page
    Approach: 
                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
 Sreenivasa Munnangi            5 Aug, 2013         Created 
******************************************************************************************/

public class SCCContactCasesInCase {
	public Case caseRec;    
	public list<Case> Contact_Cases{get; set;}    
	public SCCContactCasesInCase (ApexPages.StandardController controller)
	{    
		this.caseRec = (Case)controller.getRecord();        
       	Case rec = new Case();        
        if(caseRec.Id <> null)          
       		rec = [Select Id, ContactId from Case where Id =: caseRec.Id];        
       Contact_Cases = new list<Case>();       
       if(rec <> null && rec.ContactId <> null)         
       		Contact_Cases = [Select Id, CaseNumber, Contact.Name,Subject, Priority, CreatedDate,WCT_Category__c,Owner.Name,ClosedDate, Status from Case where ContactId =: rec.ContactId];
	}
    
}