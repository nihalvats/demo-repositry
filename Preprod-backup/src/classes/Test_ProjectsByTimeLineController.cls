/*=========================================Test Class=================================================

***************************************************************************************************************************************** 
 * Class Name   : Test_ProjectsByTimeLineController
 * Description  : test Class for ProjectsByTimeLineController
 * Created By   : Deloitte India.
 *
 *****************************************************************************************************************************************/
 
 @
istest(SeeAllData = false)
public class Test_ProjectsByTimeLineController {
    
   public static Project__c createprj(string priority)
    {
       Project__c prj = new Project__c();
       prj.Status__c = 'In Progress';
       prj.Talent_Channel__c = 'test';
       prj.Project_Category__c = 'test';
       prj.Project_Start_Date__c=system.today();
       prj.Project_Planned_End_Date__c=system.today()+1;
       prj.Priority__c= priority;
       return prj;
    }
    
     static testmethod void ProjectsByTimeLineControllernew() {
         
         
         Test.startTest();
      
       Project__c prj = createprj('Critical');    
       insert prj;
      
       
        ProjectsByTimeLineController testcontroller = new ProjectsByTimeLineController();
        
        testcontroller.SelectedPS ='';
        testcontroller.SelectedPCat=null;
        //testcontroller.IsShowTConly = false;
        testcontroller.ProcessData();
        
        testcontroller.SelectedPS ='In Progress';
        testcontroller.SelectedPCat='test';
       // testcontroller.IsShowTConly = false;
        testcontroller.ProcessData();
        
        
        
        testcontroller.SelectedPS ='';
        testcontroller.SelectedPCat=null;
        //testcontroller.IsShowTConly = true;
        testcontroller.ProcessData();
        
        testcontroller.SelectedPS ='In Progress';
        testcontroller.SelectedPCat='test';
        //testcontroller.IsShowTConly = true;
        testcontroller.ProcessData();
        apexpages.currentpage().getparameters().put('SelectedPS','In Progress');
        apexpages.currentpage().getparameters().put('SelectedPCat','test');
        apexpages.currentpage().getparameters().put('IsShowTConly','false');
        apexpages.currentpage().getparameters().put('SelectedTC','');
         apexpages.currentpage().getparameters().put('Priority','Critical');
         
        testcontroller.FilterResult();
        apexpages.currentpage().getparameters().put('SelectedPS','In Progress');
        apexpages.currentpage().getparameters().put('SelectedPCat','test');
        apexpages.currentpage().getparameters().put('IsShowTConly','true');
       apexpages.currentpage().getparameters().put('SelectedTC','test');
apexpages.currentpage().getparameters().put('Priority','Critical');
        testcontroller.FilterResult();

        
        
        
         Test.stopTest();
         }
    static testmethod void ProjectsByTimeLineControllernew1() {
         
         
         Test.startTest();
      
       Project__c prj = createprj('High');    
       insert prj;
      
       
        ProjectsByTimeLineController testcontroller = new ProjectsByTimeLineController();
        
        testcontroller.SelectedPS ='';
        testcontroller.SelectedPCat=null;
        //testcontroller.IsShowTConly = false;
        testcontroller.ProcessData();
        
        testcontroller.SelectedPS ='In Progress';
        testcontroller.SelectedPCat='test';
       // testcontroller.IsShowTConly = false;
        testcontroller.ProcessData();
        
        
        
        testcontroller.SelectedPS ='';
        testcontroller.SelectedPCat=null;
        //testcontroller.IsShowTConly = true;
        testcontroller.ProcessData();
        
        testcontroller.SelectedPS ='In Progress';
        testcontroller.SelectedPCat='test';
        //testcontroller.IsShowTConly = true;
        testcontroller.ProcessData();
        apexpages.currentpage().getparameters().put('SelectedPS','In Progress');
        apexpages.currentpage().getparameters().put('SelectedPCat','test');
        apexpages.currentpage().getparameters().put('IsShowTConly','false');
        apexpages.currentpage().getparameters().put('SelectedTC','');
        apexpages.currentpage().getparameters().put('Priority','High');
        testcontroller.FilterResult();
        apexpages.currentpage().getparameters().put('SelectedPS','In Progress');
        apexpages.currentpage().getparameters().put('SelectedPCat','test');
        apexpages.currentpage().getparameters().put('IsShowTConly','true');
       apexpages.currentpage().getparameters().put('SelectedTC','test');
apexpages.currentpage().getparameters().put('Priority','High');
        testcontroller.FilterResult();

        
        
        
         Test.stopTest();
         }
    static testmethod void ProjectsByTimeLineControllernew2() {
         
         
         Test.startTest();
      
       Project__c prj = createprj('Medium');    
       insert prj;
      
       
        ProjectsByTimeLineController testcontroller = new ProjectsByTimeLineController();
        
        testcontroller.SelectedPS ='';
        testcontroller.SelectedPCat=null;
        //testcontroller.IsShowTConly = false;
        testcontroller.ProcessData();
        
        testcontroller.SelectedPS ='In Progress';
        testcontroller.SelectedPCat='test';
       // testcontroller.IsShowTConly = false;
        testcontroller.ProcessData();
        
        
        
        testcontroller.SelectedPS ='';
        testcontroller.SelectedPCat=null;
        //testcontroller.IsShowTConly = true;
        testcontroller.ProcessData();
        
        testcontroller.SelectedPS ='In Progress';
        testcontroller.SelectedPCat='test';
        //testcontroller.IsShowTConly = true;
        testcontroller.ProcessData();
        apexpages.currentpage().getparameters().put('SelectedPS','In Progress');
        apexpages.currentpage().getparameters().put('SelectedPCat','test');
        apexpages.currentpage().getparameters().put('IsShowTConly','false');
        apexpages.currentpage().getparameters().put('SelectedTC','');
    apexpages.currentpage().getparameters().put('Priority','Low');
        testcontroller.FilterResult();
        apexpages.currentpage().getparameters().put('SelectedPS','In Progress');
        apexpages.currentpage().getparameters().put('SelectedPCat','test');
        apexpages.currentpage().getparameters().put('IsShowTConly','true');
       apexpages.currentpage().getparameters().put('SelectedTC','test');
apexpages.currentpage().getparameters().put('Priority','Low');
    apexpages.currentpage().getparameters().put('StartDate','05/01/2014');
    apexpages.currentpage().getparameters().put('EndDate','05/01/2015');
        testcontroller.FilterResult();

        
        
        
         Test.stopTest();
         }
static testmethod void ProjectsByTimeLineControllernew3() {
         
         
         Test.startTest();
      
       Project__c prj = createprj('Low');    
       insert prj;
      
       
        ProjectsByTimeLineController testcontroller = new ProjectsByTimeLineController();
        
        testcontroller.SelectedPS ='';
        testcontroller.SelectedPCat=null;
        //testcontroller.IsShowTConly = false;
        testcontroller.ProcessData();
        
        testcontroller.SelectedPS ='In Progress';
        testcontroller.SelectedPCat='test';
       // testcontroller.IsShowTConly = false;
        testcontroller.ProcessData();
        
        
        
        testcontroller.SelectedPS ='';
        testcontroller.SelectedPCat=null;
        //testcontroller.IsShowTConly = true;
        testcontroller.ProcessData();
        
        testcontroller.SelectedPS ='In Progress';
        testcontroller.SelectedPCat='test';
        //testcontroller.IsShowTConly = true;
        testcontroller.ProcessData();
        apexpages.currentpage().getparameters().put('SelectedPS','In Progress');
        apexpages.currentpage().getparameters().put('SelectedPCat','test');
        apexpages.currentpage().getparameters().put('IsShowTConly','false');
        apexpages.currentpage().getparameters().put('SelectedTC','');
    apexpages.currentpage().getparameters().put('Priority','Low');
        testcontroller.FilterResult();
        apexpages.currentpage().getparameters().put('SelectedPS','In Progress');
        apexpages.currentpage().getparameters().put('SelectedPCat','test');
        apexpages.currentpage().getparameters().put('IsShowTConly','true');
       apexpages.currentpage().getparameters().put('SelectedTC','test');
apexpages.currentpage().getparameters().put('Priority','Low');
    apexpages.currentpage().getparameters().put('StartDate','05/01/2014');
    apexpages.currentpage().getparameters().put('EndDate','05/01/2015');
        testcontroller.FilterResult();

        
        
        
         Test.stopTest();
         }

}