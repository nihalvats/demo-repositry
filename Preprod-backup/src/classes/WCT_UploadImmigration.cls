/**  
    * Class Name  : WCT_UploadImmigration 
    * Description : This apex class will use to upload CSV into staging table 
*/

public class WCT_UploadImmigration{


    public List<WCT_Immigration_Stagging_Table__c> stagingRecordsList;
    public Blob contentFile{get;set;}
    public Integer noOfRows{get;set;}
    public Integer noOfRowsProcessed{get;set;}
    public Set<Id> stagingIds=new Set<Id>();     // Store all StagingIds after reading from file.
    public String nameFile{get;set;}
    public List<List<String>> fileLines = new List<List<String>>();
    
    Map<String,Integer> questionColOrderMap=new Map<String,Integer>();
    WCT_parseCSV parseCSVInstance = new WCT_parseCSV();
    
    /*Variable to hold the current load type ie.., GGS or Fragomen. Which will be passed as page param.*/
    public String lawFrimName{get; set;}
    public boolean isError{get;set;}
  
    
     /*Get the id of the record type of Stagging named as "USI Immigration Record"*/
    public String USI_Upload_Staging_RecordTypeId;
  
    
     public WCT_UploadImmigration()
   {
        USI_Upload_Staging_RecordTypeId= Schema.SObjectType.WCT_Immigration_Stagging_Table__c.getRecordTypeInfosByName().get('USI Immigration Record').getRecordTypeId();
       
       lawFrimName= ApexPages.currentPage().getParameters().get('law_firm');  
       
       if(lawFrimName==null|| lawFrimName=='')
       {
           isError=true;
       }
       else if(lawFrimName!='Fragomen'&& lawFrimName!='GGS')
       {
          isError=true;
       }
       else
       {
           isError=false;
       }
       
   }
        
     /** 
        Method Name  : readFile
        Return Type  : PageReference
        Description  : Read CSV and push records into staging table        
    */
    
    public Pagereference readFile()
    {
        stagingRecordsList = new List<WCT_Immigration_Stagging_Table__c>(); 
        noOfRowsProcessed=0;
        
        try{
            stagingRecordsList= mapStagingRecords();
            
            
        }
        Catch(System.StringException stringException)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_String_Exception_Error + stringException);
            ApexPages.addMessage(errormsg);    
        }
        Catch(System.ListException listException)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Columns_count_not_proper +listException);
            ApexPages.addMessage(errormsg); 
        
        }
        
        Catch(Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_Exception +e );
            ApexPages.addMessage(errormsg);    
        
        }
        if(stagingRecordsList.size()>Limits.getLimitQueryRows())
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Error_Data_File_too_large + Limits.getLimitQueryRows());
            ApexPages.addMessage(errormsg);   
        }
        Database.SaveResult[] srList = Database.insert(stagingRecordsList, false);
        for(Database.SaveResult sr: srList)
        {
            if(sr.IsSuccess())
            {   
                stagingIds.add(sr.getId());
                noOfRowsProcessed=stagingIds.size();
            }
            else if(!sr.IsSuccess())
            {
                for(Database.Error err : sr.getErrors())
                {
                     ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.WCT_Data_Error+err.getMessage());
                     ApexPages.addMessage(errormsg);       
                }
            }
        }   
        if(!stagingIds.IsEmpty())
        {            
            try{                
                WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);               
            }Catch(Exception e)
            {
                throw e;
            }
            
        }
          
        return null;
    }
    
    /** 
        Method Name  : mapStagingRecords
        Return Type  : List<WCT_Immigration_Stagging_Table__c>
        Description  : Map fields names with CSV col names         
    */
    private List<WCT_Immigration_Stagging_Table__c> mapStagingRecords()
    {
        //nameFile=EncodingUtil.base64Encode(contentFile);
        nameFile=WCT_parseCSV.blobToString(contentFile);

        filelines = parseCSVInstance.parseCSV(nameFile, true);
        system.debug('NoOfLines'+filelines);
        List<WCT_Immigration_Stagging_Table__c> stagingRecordsList=new List<WCT_Immigration_Stagging_Table__c>();
        noOfRows=fileLines.size();
        Map<string,id> emailToIdMap = new Map<string,id>();
        set<string> empEmailsSet = new set<string>();
        
        for(List<String> inputValues:filelines) 
        { 
            if(inputValues[3] <> null)
            {empEmailsSet.add(inputValues[3].toLowerCase());}
        }
        if(!empEmailsSet.isEmpty()){
            for(contact con : [SELECT id,Fragomen_Email__c FROM contact WHERE Fragomen_Email__c=:empEmailsSet]){
                emailToIdMap.put(con.Fragomen_Email__c.toLowerCase(),con.id);
            }
        }
        
        
        for(List<String> inputValues:filelines) 
        {   
            WCT_Immigration_Stagging_Table__c stagingRecord = new WCT_Immigration_Stagging_Table__c();
            
            stagingRecord.WCT_Last_Name__c = inputValues[1];
            stagingRecord.WCT_First_Name__c = inputValues[2];
            stagingRecord.WCT_Employee_Email__c = inputValues[3].toLowerCase();
            
            //Create Identifier as Emp Email+Case Type(record type)+Initiation Date
            if(inputValues[4] != null && inputValues[5] != null && inputValues[9] != null){
                stagingRecord.WCT_Case_type__c = inputValues[4];
                if(stagingRecord.WCT_Case_type__c=='B-1')
                {
                    stagingRecord.WCT_Category__c = 'B1/B2'; 
                }
                else
                    stagingRecord.WCT_Category__c = inputValues[5]; 
                stagingRecord.WCT_L_1_Blanket_or_Individual__c = inputValues[6];
                stagingRecord.WCT_H_1B_Cap__c = inputValues[7];
                stagingRecord.WCT_Action_EOS_COS_COE_Amendment__c = inputValues[8];
             
               
                stagingRecord.WCT_Identifier__c = inputValues[0];
                
                if(inputValues[9]!='')
                    stagingRecord.WCT_Date_Initiation_Sent_to_Fragomen__c = date.parse(inputValues[9]);
            }
                 
            stagingRecord.H1B_Is_USI_Upload__c=true;
            
            
            stagingRecord.WCT_Initiatior__c = inputValues[10];
            stagingRecord.WCT_Initiator_Email_Address__c = inputValues[11];
            stagingRecord.WCT_Milestone__c = inputValues[12];
            if(inputValues.size()>13 && inputValues[13]!='')
                stagingRecord.WCT_Milestone_Date__c = date.parse(inputValues[13]);
            if(inputValues.size()>14 && inputValues[14]!='')
                stagingRecord.WCT_RFE_Due_Date__c=date.parse(inputValues[14]);
            if(inputValues.size()>15 && inputValues[15]!='')
                stagingRecord.WCT_Petition_Start_Date__c=date.parse(inputValues[15]);
            if(inputValues.size()>16 && inputValues[16]!='')
                stagingRecord.WCT_Petition_End_Date__c=date.parse(inputValues[16]);
            if(inputValues.size()>17 && inputValues[17]!='')
                stagingRecord.WCT_Personnel_Number__c=inputValues[17];
                
                
             /*  CASE -ENHANCEMENT #02101985 */   
             
             if(inputValues.size()>18 && inputValues[18]!='')
                stagingRecord.WCT_Petition_Salary_Fragomen__c=inputvalues[18].replace('"','');  
            
             if(inputValues.size()>19 && inputValues[19]!='')
            stagingRecord.USI_Client_Name1__c=inputValues[19];
            
              if(inputValues.size()>20 && inputValues[20]!='')
            stagingRecord.USI_Client_Location1__c =inputValues[20];
            
                  
            if(stagingRecord.WCT_Employee_Email__c <> null)
            {
                if(emailToIdMap.containsKey(stagingRecord.WCT_Employee_Email__c.toLowerCase()))
                {
                    stagingRecord.Contact__c =emailToIdMap.get(stagingRecord.WCT_Employee_Email__c.toLowerCase());
                }
            }
            
              
            
            /*
             * ADDED BY DEEPU : 
             *   Adding after the VTS System. 
             * Stagging uploaded by the USI will be saved as "USI Immigration Record" record Type 
      */
            stagingRecord.recordTypeId=USI_Upload_Staging_RecordTypeId;
            stagingRecord.H1B_Law_Firm_Name__c=lawFrimName;
            stagingRecordsList.add(stagingRecord);
        }
        return stagingRecordsList;
    } 
    
     /** 
        Method Name  : getAllStagingRecords
        Return Type  : List<WCT_Immigration_Stagging_Table__c>
        Description  : Show all records into VF        
    */
    public List<WCT_Immigration_Stagging_Table__c> getAllStagingRecords()
    {
        if (stagingIds!= NULL)
            if (stagingIds.size() > 0)
            {
                return [Select id,Name, WCT_Initiatior__c,H1B_Client_Name__c,H1B_Client_location_full_address__c,WCT_Emp_Number__c, WCT_Last_Name__c, WCT_First_Name__c, WCT_Employee_Email__c,WCT_Case_type__c, WCT_Entity__c, 
                    WCT_Service_Area__c,WCT_Petition_Salary_Fragomen__c, WCT_Service_Line__c, WCT_Deloitte_India_Office__c, WCT_Received_Request_for_Evidence__c, 
                    WCT_Request_for_Evidence_Due_Date__c, WCT_Response_to_Requst_for_Evidnce_Filed__c, WCT_Petition_Approved__c, 
                    WCT_Last_Action__c,WCT_Receivd_H_1B_LCA_Amendment_initiatin__c, WCT_Received_Confirmation_of_LCA_Posting__c, 
                    WCT_H_1B_LCA_filed_with_DOL__c, WCT_Copy_of_certified_H_1B_LCA_sent_date__c, Status__c, WCT_Date_Initiation_Received__c, 
                    USI_Client_Name1__c,USI_Client_Location1__c,WCT_Date_B_Package_Sent__c,WCT_CSV_Type__c,WCT_Error_Message__C from WCT_Immigration_Stagging_Table__c where Id In:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_ERROR];
                
            }
            else
                return null;                   
        else
            return null;
    }   
}