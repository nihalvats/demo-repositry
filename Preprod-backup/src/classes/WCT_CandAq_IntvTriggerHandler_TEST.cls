/*****************************************************************************************
    Name    : WCT_CandAq_IntvTriggerHandler_TEST
    Desc    : Test Method for WCT_CandidateAquisitionTriggerHandler
              Test method for WCT_InterviewTriggerHandler                 
                                                
    Modification Log : 
    ---------------------------------------------------------------------------
     Developer                      Date            Description
    ---------------------------------------------------------------------------
    Mrudula konnoju                12/02/2013         Created 

******************************************************************************************/
@isTest
Private class WCT_CandAq_IntvTriggerHandler_TEST{
        
        public static testmethod void UpdateOwner(){    
        Test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name=:WCT_UtilConstants.RECRUITER_COMPANY]; 
        UserRole r1 = [SELECT Id FROM UserRole Where Name='CTS Recruiter'];
        User recutr1 = new User(Alias = 'rUser1', Email='recuiteruser1@wct.com', LastName='Testing1', 
                                EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US', LocaleSidKey='en_US', 
                                ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles',
                                UserName='recuiteruser1@wct.com', UserRoleId=r1.Id);
               
        system.runAs(recutr1){
            Contact newContact=new Contact(LastName='Test');
            insert newContact;
            WCT_Requisition__c req = new WCT_Requisition__c();    
            req.WCT_Recruiter__c = recutr1.Id;
            insert req;
            WCT_Candidate_Requisition__c newCandidateAquisition=new  WCT_Candidate_Requisition__c(WCT_Contact__c=newContact.id,
            WCT_Requisition__c=req.id,WCT_Candidate_Hiring_Level__c='Summer Conference - Freshman Programs');
            insert  newCandidateAquisition;
          /* Candidate Requisition is not associated with Interview
          
            WCT_Interview__c newInterviewRec=new WCT_Interview__c(WCT_Candidate_Requistion__c=newCandidateAquisition.id);
            insert newInterviewRec;
            newCandidateAquisition.WCT_Candidate_Requisition_Status__c='Canceled';
            update newCandidateAquisition;   */    
        }
        Test.stopTest();
        }   
    
}