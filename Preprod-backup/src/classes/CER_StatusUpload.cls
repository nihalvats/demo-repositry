public class CER_StatusUpload {
    
    
    public Blob contentFile{get;set;}
    public Integer noOfRows{get;set;}
    public Integer noOfRowsProcessed{get;set;}
    public String nameFile{get;set;}    
    public List<ErrorItem> errorRecords{get; set;}
    public List<ErrorItem> topErrors{get{
        List<ErrorItem> limitedErrors=new List<ErrorItem>();
        if(errorRecords!=null)
        {
            for(integer i=0;i<errorRecords.size() && i<MAX_RECORD_TO_VIEW; i++)
            {
                limitedErrors.add(errorRecords[i]);
            }
        }
        return    limitedErrors;     
    }}
    public boolean isFirstCall{get; set;} 
    public boolean isMoreRecord{get; set;}
    public boolean isError{get; set;}
    
    
    static integer MAX_RECORD_TO_PROCESS=10000;
    static integer MAX_RECORD_TO_VIEW=1000;
    
    WCT_parseCSV parseCSVInstance = new WCT_parseCSV();
    transient List<List<String>> filelines;
    transient List<CER_Expense_Reimbursement__c> validRecords;
    Map<String, CER_Expense_Reimbursement__c> allUploadedRecords=new Map<String, CER_Expense_Reimbursement__c>();
    List<String> identifiers=new List<String>();
    
    
    public CER_StatusUpload()
    {
        resetValues();
        isFirstCall=true;
        
    }
    public void resetValues()
    {
        noOfRows=0;
        noOfRowsProcessed=0;
        validRecords= new List<CER_Expense_Reimbursement__c>();
        errorRecords= new List<ErrorItem>();
        filelines= new List<List<String>>();
        isFirstCall=false;
        isMoreRecord=false;
        isError=false;
    }
     public Pagereference readFile()
    {
      resetValues();
        isMoreRecord=false;
      
     

       try
       {
        
        nameFile=contentFile.toString();
        filelines = parseCSVInstance.parseCSV(nameFile, true);
        system.debug('111111111'+filelines);
        noOfRows=fileLines.size();
        contentFile=Blob.valueOf('');
        nameFile='';
        if(filelines.size()<MAX_RECORD_TO_PROCESS)
        {
            /*Parse through all the records from file, make identifier list and the Map of all uploaded records with key as identifier. */
          ProcessRequest(filelines);
        }
       else
       {
            isError=true;
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Cannot upload more than one '+MAX_RECORD_TO_PROCESS+' records.');
            ApexPages.addMessage(errormsg);    
       }
        
       }
        Catch(System.Exception stringException)
        {
            isError=true;
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_String_Exception_Error);
            ApexPages.addMessage(errormsg);    
        }
        system.debug('111111111 identifiers:'+identifiers);
        if(identifiers.size()>0 && !isError)
        {
			IdentifierRequest(identifiers);
        }
            
        system.debug('111111111 errors:'+errorRecords);
        
        return null;
    }
    
      
     public void ProcessRequest(List<List<String>> filelines){
         
            for(integer index=0;index<filelines.size()  ; index++)
            {  
                string error='';
                List<String> fields =filelines[index];
                CER_Expense_Reimbursement__c tempRequest= new CER_Expense_Reimbursement__c();
                
                string status=fields.size()>1?fields[1]:'';
                if(status.toUpperCase() =='PAID')
                {
                    tempRequest.CER_Request_Status__c=fields.size()>1?fields[1]:'';
                    try
                    {
                      Date tempDate= Date.parse(fields[3]);
                      tempRequest.CER_Paid_Date__c=fields.size()>3?Date.parse(fields[3]):null;
                    }
                    catch(Exception e)
                    {
                        error='Invalid Date';
                    }
                }
                else if( status.toUpperCase()=='PENDING CORR.')
                {
                    tempRequest.CER_Request_Status__c=fields.size()>1?fields[1]:'';
                    tempRequest.CER_Rejected_by_AP_Reason__c=fields.size()>2?fields[2]:'';
                }
                else
                {
                    
                    tempRequest.CER_Request_Status__c=fields.size()>1?fields[1]:'';
                    error='Invalid status.';
                }
                if(error=='')
                {
                    allUploadedRecords.put(fields[0], tempRequest);
                    identifiers.add(fields[0]);
                }
                else
                {
                     errorRecords.add(new ErrorItem(tempRequest, error, fields[0]));
                }
               
         
         
     } 
     }
      
     public void IdentifierRequest(List<String> identifiers){
         
             List<CER_Expense_Reimbursement__c> requests= [Select Id, Name, CER_Request_Status__c,CER_Paid_Date__c, CER_Rejected_by_AP_Reason__c From CER_Expense_Reimbursement__c where Name in :identifiers];
             for(CER_Expense_Reimbursement__c request :requests)
             {
                 
                 /*Move the record from all records to ValidRecords*/
                 CER_Expense_Reimbursement__c temp=allUploadedRecords.get(request.Name);
                 if(temp!=null)
                 {
                     request.CER_Request_Status__c= temp.CER_Request_Status__c;
                     request.CER_Paid_Date__c= temp.CER_Paid_Date__c;
                     request.CER_Rejected_by_AP_Reason__c= temp.CER_Rejected_by_AP_Reason__c;
                     validRecords.add(request);
                     
                     
                     /*Remove from all records*/
                     allUploadedRecords.remove(request.Name);
                 }
                 
                 
                 
             }
            system.debug('111111111 requests:'+requests);
            system.debug('111111111 remaining rec:'+allUploadedRecords);
            /*The rest of the all Uploaded Records are inavlid records without any trace in Salesforce. */
            for(String key : allUploadedRecords.keySet())
            {
                if(allUploadedRecords.get(key) !=null)
                {
                     errorRecords.add(new ErrorItem(allUploadedRecords.get(key), 'No Record found in Salesforce', key));
                }
            }
            system.debug('111111111 Not in SFDC :'+errorRecords);
            system.debug('111111111'+' SIZE '+validRecords.size()+' LIMIT :: '+Limits.getLimitDMLRows());
            if(validRecords.size()>0 && validRecords.size()<Limits.getLimitDMLRows())
            {
                system.debug('111111111 VALID :'+validRecords.size());
               
                    Database.SaveResult[] srList = Database.update(validRecords, false);
                    system.debug('111111111 VALID :'+srList);
                    for(integer i=0; i<srList.size();i++)
                    {
                        if(srList[i].IsSuccess())
                        {   
                            
                            noOfRowsProcessed=noOfRowsProcessed+1;
                        }
                        else
                        {
                           
                           errorRecords.add(new ErrorItem(validRecords.get(i), srList[i].getErrors()[0].getMessage(),''));
                        }
                    } 
            }
            else
            {
                if(validRecords.size()>0)
                {
                 ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,'Too many records to handle');
                 ApexPages.addMessage(errormsg);  
                }
                
            } 
         
     } 
      
      
    public class ErrorItem
    {
        public CER_Expense_Reimbursement__c inValidRequest{get; set;}
        public String errorMessage{get; set;}
        public string identifier{get; set;}
        public  ErrorItem(CER_Expense_Reimbursement__c errorRequest, String status, string tempIden)
        {
           inValidRequest=errorRequest;
           errorMessage=status;
           identifier=tempIden;
        }
      
    }
    
  
}