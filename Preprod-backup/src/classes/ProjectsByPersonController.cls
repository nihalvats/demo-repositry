public with sharing class ProjectsByPersonController
{

/*
 Apex Class:  ProjectsByPersonController
 Purpose: This class is used to prepare view of Projects By Project Lead.
 Created On:  6th July,2015.
 Created by:  Balu Devarapu.(Deloitte India)
*/


public set<string> lstPriority{get;set;}
public Set<string> setLeadName{get;set;}
public List<string> lstFinalPriority{get;set;}
public Map<string,Map<string,Integer>> mapAllLeadData{get;set;}
public List<ChartData> objChartData{get;set;}
List<ChartData> objChartDataClone = new List<ChartData>();
public List<SelectOption> PCatOptions{get;set;}
public string SelectedPCat{get;set;}
public List<SelectOption> PSOptions{get;set;}
public string SelectedPS{get;set;}
Map<string,integer> MapLeadProjCount=new Map<string,integer>();
public string SearchLead;
public List<string> lstTotals{get;set;}

/* Constructor */
public ProjectsByPersonController(){
    SearchLead=null;
    lstPriority=new set<string>();
    objChartData = new List<ChartData>();
    lstFinalPriority=new List<string>();
    lstTotals=new List<string>();
    PCatOptions= new List<SelectOption>();
    PCatOptions.add(new SelectOption('-None-','-None-'));
    Schema.DescribeFieldResult objPCatOptions = Project__c.Project_Category__c.getDescribe();
    List<Schema.PicklistEntry> lstPCat = objPCatOptions.getPicklistValues();
        for(Schema.picklistEntry f:lstPCat)    
        {    
            PCatOptions.add(new SelectOption(f.getLabel(),f.getLabel()));     
        }
    SelectedPCat=null; 

    PSOptions = new List<SelectOption>();
    Schema.DescribeFieldResult objlstPS = Project__c.Status__c.getDescribe();
    List<Schema.PicklistEntry> lstPS = objlstPS.getPicklistValues();
        for(Schema.picklistEntry f:lstPS)    
        {    
            PSOptions.add(new SelectOption(f.getLabel(),f.getLabel()));     
        }
    SelectedPS='In Progress';  

    Schema.DescribeFieldResult objlstTC = Project__c.Priority__c.getDescribe();

    List<Schema.PicklistEntry> lstTC = objlstTC.getPicklistValues();
    for(Schema.picklistEntry f:lstTC)    
    {    
        lstPriority.add(f.getLabel());    
    }
    
    ProcessData();
    
}


    /* 
        Method:ProcessData
        Description: Data view Perparation 
    */
    public void ProcessData(){
    try{
    objChartData = new List<ChartData>();
    System.debug('-----SelectedPCat----'+SelectedPCat);
    System.debug('-----SelectedPS----'+SelectedPS);
    System.debug('-----SearchLead----'+SearchLead);
    setLeadName=new Set<string>();
    mapAllLeadData=new Map<string,Map<string,Integer>>();
    MapLeadProjCount=new Map<string,integer>();
    String SOQLUS='select Priority__c Priority,Project_Lead_US__r.Name LeadName,Count(Id) prjCnt from Project__c where Project_Lead_US__c!=null and Priority__c!=null';
    SOQLUS+=' and Status__c= \''+SelectedPS+'\'';
    String SOQLUSFilter='';
    AggregateResult[] PriorityUSResults;
    if(string.IsNotEmpty(SelectedPCat)){ //and Project_Lead_US__r.Name Like:SearchLead Project_Lead_US__c!=null and Priority__c!=null
     SOQLUSFilter=' and Project_Category__c=\''+SelectedPCat+'\'';
    }
    
    if(string.IsNotEmpty(SearchLead)){
     SOQLUSFilter+=' and Project_Lead_US__r.Name Like \''+SearchLead+'\'';
    }
    
   
    SOQLUS+=SOQLUSFilter;
    SOQLUS+=' group by Priority__c,Project_Lead_US__r.Name order by Project_Lead_US__r.Name';
    System.debug('----SOQLUS-----'+SOQLUS);
    for (AggregateResult ar : Database.query(SOQLUS)){
        system.debug('----ar---'+ar);
         string LdName='';
         LdName=String.ValueOf(ar.get('LeadName'));
         system.debug('----LdName---'+LdName);
         if(LdName.contains('(')){
         LdName=LdName.substring(0,LdName.indexOf('('));
         }
        LdName+=' (US)';
        setLeadName.add(LdName);
        Map<string,Integer> mapLeadProj=new Map<string,Integer>();
        if(mapAllLeadData.get(LdName)!=null){
        mapLeadProj=mapAllLeadData.get(LdName);
        mapLeadProj.put(String.ValueOf(ar.get('Priority')),integer.ValueOf(ar.get('prjCnt')));
        }else{
        mapLeadProj.put(String.ValueOf(ar.get('Priority')),integer.ValueOf(ar.get('prjCnt')));
        }
        
        integer Projcnt=0;
        if(MapLeadProjCount.get(LdName)!=null){
        Projcnt=MapLeadProjCount.get(LdName);
        Projcnt+=integer.ValueOf(ar.get('prjCnt'));
         MapLeadProjCount.put(LdName,Projcnt);
        }else{
         Projcnt=integer.ValueOf(ar.get('prjCnt'));
         MapLeadProjCount.put(LdName,Projcnt);
        }
        
        mapAllLeadData.put(LdName,mapLeadProj);
    }
    
    
    
    String SOQLUSI='select Priority__c Priority,Project_Lead_USI__r.Name LeadName,Count(Id) prjCnt from Project__c where Project_Lead_USI__c!=null and Priority__c!=null';
    SOQLUSI+=' and Status__c= \''+SelectedPS+'\'';
    String SOQLUSIFilter='';
    AggregateResult[] PriorityUSIResults;
    if(string.IsNotEmpty(SelectedPCat)){ 
     SOQLUSIFilter=' and Project_Category__c=\''+SelectedPCat+'\'';
    }
    
    if(string.IsNotEmpty(SearchLead)){
      SOQLUSIFilter+='and Project_Lead_USI__r.Name Like \''+SearchLead+'\'';
    }
    
    SOQLUSI+=SOQLUSIFilter;
    SOQLUSI+=' group by Priority__c,Project_Lead_USI__r.Name order by Project_Lead_USI__r.Name';
    System.debug('----SOQLUSI-----'+SOQLUSI);
    for (AggregateResult ar : Database.query(SOQLUSI))  {
         string LdName='';
         LdName=String.ValueOf(ar.get('LeadName'));
         if(LdName.contains('(')){
         LdName=LdName.substring(0,LdName.indexOf('('));
         }
        LdName+=' (USI)';
        setLeadName.add(LdName);
        Map<string,Integer> mapLeadProj=new Map<string,Integer>();
        if(mapAllLeadData.get(LdName)!=null){
        mapLeadProj=mapAllLeadData.get(LdName);
        mapLeadProj.put(String.ValueOf(ar.get('Priority')),integer.ValueOf(ar.get('prjCnt')));
        }else{
        mapLeadProj.put(String.ValueOf(ar.get('Priority')),integer.ValueOf(ar.get('prjCnt')));
        }
        integer Projcnt=0;
        if(MapLeadProjCount.get(LdName)!=null){
        Projcnt=MapLeadProjCount.get(LdName);
        Projcnt+=integer.ValueOf(ar.get('prjCnt'));
         MapLeadProjCount.put(LdName,Projcnt);
        }else{
         Projcnt=integer.ValueOf(ar.get('prjCnt'));
         MapLeadProjCount.put(LdName,Projcnt);
        }
        
        mapAllLeadData.put(LdName,mapLeadProj);
    }

    
    List<string> lstLeadName=new List<string>();
    lstLeadName.addAll(setLeadName);
    lstLeadName.sort();
    for(string objLead:lstLeadName){
     Map<string,Integer> objmapLeadProj=new Map<string,Integer>();
     List<Integer> lstLeadPriority=new List<integer>();
     if(mapAllLeadData.get(objLead)!=null){
     lstLeadPriority=new List<Integer>();
     objmapLeadProj=mapAllLeadData.get(objLead);
     for(string objPrioty:lstPriority){
     integer PriorityLead=0;
     if(objmapLeadProj.get(objPrioty)!=null){
     PriorityLead=objmapLeadProj.get(objPrioty);
     }
     lstLeadPriority.add(PriorityLead);
     }
     }
     objChartData.add(new ChartData(objLead,MapLeadProjCount.get(objLead), lstLeadPriority)); 
     }
    system.debug('----------objChartData-------------'+objChartData);
    //objChartDataClone = objChartData;
    }catch(Exception e){
    system.debug('----------Exception-------------'+e.getMessage()+'-----at Line #----'+e.getLineNumber()); 
    }

 } 

 
    /* 
    Method:FilterResult
    Description: Populates data based on Filter conditions provided by User. 
    */
public void FilterResult(){
   
    SearchLead=null;
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SearchLead'))){
    SearchLead=apexpages.currentpage().getparameters().get('SearchLead');
    SearchLead='%'+SearchLead.ToLowerCase()+'%';
    }
    
    SelectedPCat='';
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SelectedPCat'))){
    SelectedPCat=apexpages.currentpage().getparameters().get('SelectedPCat');
    if(SelectedPCat=='-None-')
    SelectedPCat=null;
    }
    
    SelectedPS='';
    if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('SelectedPS'))){
    SelectedPS=apexpages.currentpage().getparameters().get('SelectedPS');
    if(SelectedPS=='-None-')
    SelectedPS=null;
    }
    /*
    if(SearchLead==''){
    objChartData = new List<ChartData>();
    objChartData=objChartDataClone;
    }else{
    objChartData = new List<ChartData>();
    for(ChartData objCD:objChartDataClone){
     if(objCD.LeadName.ToLowerCase().contains(SearchLead)){
      objChartData.add(objCD); 
     }
    }
    }
    */
    ProcessData();
}

 
 
     /* 
    Class:ChartData 
    Description: Wrapper class used to prepare data
    */
 
    public class ChartData {
        public String LeadName { get; set; }
        public integer LeadProjCount {get;set;}
        public List<integer> PriorityItems{ get; set; }

        public ChartData(String LeadName1,integer LeadProjCount1, List<integer> PriorityItems1) {
            
            this.LeadName = LeadName1;
            this.LeadProjCount=LeadProjCount1;
            this.PriorityItems= PriorityItems1;

        }
    }  



}