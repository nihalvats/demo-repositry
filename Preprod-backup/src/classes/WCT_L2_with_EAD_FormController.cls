/********************************************************************************************************************************
Apex class         : <WCT_L2_with_EAD_FormController>
Description        : <Controller which allows to Update Mobility, Immigration and Task>
Type               :  Controller
Test Class         : <WCT_L2_with_EAD_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 25/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/ 
public class WCT_L2_with_EAD_FormController  extends SitesTodHeaderController{

/* public variables */
public WCT_Immigration__c ImmigrationRecord{get;set;}
public WCT_Mobility__c MobilityRecord{get;set;}
public List<RecordType> recordlist{get;set;}

 /* Upload Related Variables */
public Document doc {get;set;}
public List<String> docIdList = new List<string>();

public task t{get;set;}
public String taskid{get;set;}
public List<Attachment> listAttachments {get; set;}
public Map<Id, String> mapAttachmentSize {get; set;}  
 public GBL_Attachments attachmentHelper{get; set;}
 
Public Map<id,string> maprecord{get;set;}

// Error Message related variables
public boolean pageError {get; set;}
public String pageErrorMessage {get; set;}
public String supportAreaErrorMesssage {get; set;}   

public String l1visaStartDate {get;set;}
public String l1visaEndDate {get;set;}
public String l2visaStartDate {get;set;}
public String l2visaEndDate {get;set;}
public String eadstart {get;set;}
public String eadexpire {get;set;}
public String applicationdate{get;set;} 
public String OwnerIdStr {get;set;} 
public string immigrationRecordTypeId {get;set;} 
 public WCT_L2_with_EAD_FormController ()
{  
    
    
        /*Initialize all Variables*/
        init();

        /*Get Task ID from Parameter*/
        getParameterInfo();  
        
        /*Task ID Null Check*/
        if(taskid=='' || taskid==null)
        {
           invalidEmployee=true;
           return;
        }

        /*Get Task Instance*/
        getTaskInstance();
        MobilityRecord = [SELECT Id,Immigration__c,OwnerId,name, WCT_Mobility_Status__c FROM WCT_Mobility__c where id=:t.WhatId ];
        OwnerIdStr = MobilityRecord.OwnerId;
        /*Get Attachment List and Size*/
        getAttachmentInfo();
        
        getrecordtype();
        
        pageError=false;
}

/********************************************************************************************
*Method Name         : <init()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Init() Used for loading Mobility, Assignment and Attachments>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 
 private void init(){

        //Custom Object Instances
        ImmigrationRecord= new WCT_Immigration__c (); 
        maprecord=new Map<id,string>();
    
      
        //Document Related Init
        doc = new Document();
       
        listAttachments = new List<Attachment>();
        mapAttachmentSize = new Map<Id, String>();
         attachmentHelper= new GBL_Attachments();
        //recordtype RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'WCT_Immigration__c' AND DeveloperName = 'L2_with_EAD' Limit 1];
        //Immigrationrecord.recordtypeid =RecordTypeId.id ;
      
        MobilityRecord=new WCT_Mobility__c ();
        ImmigrationRecord.WCT_Immigration_Status__c='L2 with EAD';
       
       
    }   

/********************************************************************************************
*Method Name         : <getParameterInfo()>
*Return Type         : <Null>
*Param’s             : URL
*Description         : <GetParameterInfo() Used to get URL Params for TaskID>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     
    
    private void getParameterInfo(){
        taskid = ApexPages.currentPage().getParameters().get('taskid');
    }

/********************************************************************************************
*Method Name         : <getTaskInstance()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetTaskInstance() Used for Querying Task Instance>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/   
   
    private void getTaskInstance(){
        t=[SELECT Status,OwnerId,WhatId  FROM Task WHERE Id =: taskid];
    }

/********************************************************************************************
*Method Name         : <getrecordtype()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Getrecordtype() Used for Querying Recordtype>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/   
    @TestVisible
    private void getrecordtype()
    {
        recordlist=[SELECT id,DeveloperName,Name,SobjectType FROM RecordType WHERE SobjectType = 'WCT_Immigration__c'];
    }

/********************************************************************************************
*Method Name         : <getAttachmentInfo()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetAttachmentInfo() Used for Retrieving Attachment Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/
    @Testvisible
    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId , 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId = :taskid
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50 ];    
              
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }
   

/********************************************************************************************
*Method Name         : <save()>
*Return Type         : <PageReference>
*Param’s             : 
*Description         : <Save() Used for for Updating Task and Immigration>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 

public pageReference save()
{   
     
       /*Custom Validation Rules are handled here*/  
      if( (null == l1visaStartDate) ||('' == l1visaStartDate) || (null == l1visaEndDate) ||('' == l1visaEndDate))
        {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
        system.debug('@case2');
        if( (null == l2visaStartDate) ||('' == l2visaStartDate) || (null == l2visaEndDate) ||('' == l2visaEndDate))
        {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
        if( (null == eadstart) ||('' == eadstart) || (null == eadexpire) ||('' == eadexpire))
        {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
        if( (null == applicationdate) ||('' == applicationdate) )
        {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
        if(null == Immigrationrecord.WCT_EAD_Status__c)
        {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
        
        /*ImmigrationRecord.WCT_Immigration_Status__c = L2dropDownValue;
        ImmigrationRecord.L1_Visa_Status__c = L1dropDownValue;
        Immigrationrecord.L1_Visa_Type__c = 'L1';*/
    
        //Set Record Type
        try {
        immigrationRecordTypeId = Schema.SObjectType.WCT_Immigration__c.RecordTypeInfosByName.get('L2 with EAD').RecordTypeId;
        Immigrationrecord.RecordTypeid = immigrationRecordTypeId;
        //Update Dates
   
        if(String.isNotBlank(l2visaStartDate)){
            ImmigrationRecord.WCT_Visa_Start_Date__c = Date.parse(l2visaStartDate);
        }
        if(String.isNotBlank(l2visaEndDate)){
            ImmigrationRecord.WCT_Visa_Expiration_Date__c = Date.parse(l2visaEndDate);
        }
        if(String.isNotBlank(l1visaStartDate)){
            ImmigrationRecord.L1_Visa_Start_Date__c= Date.parse(l1visaStartDate);
        }
        if(String.isNotBlank(l1visaEndDate)){
            ImmigrationRecord.L1_Visa_Expiration_Date__c = Date.parse(l1visaEndDate);
        }
        if(String.isNotBlank(eadstart)){
            ImmigrationRecord.WCT_EAD_Valid_On__c= Date.parse(eadstart);
        }
        if(String.isNotBlank(eadexpire)){
            ImmigrationRecord.WCT_EAD_Card_Expires_On__c = Date.parse(eadexpire);
        }
        if(String.isNotBlank(applicationdate)){
            ImmigrationRecord.WCT_Request_Initiation_Date__c= Date.parse(applicationdate);
        }
        
  

         if(ImmigrationRecord.WCT_Visa_Expiration_Date__c < ImmigrationRecord.WCT_Visa_Start_Date__c)
        {
        system.debug('@case52'+ImmigrationRecord.WCT_Visa_Expiration_Date__c );
            pageErrorMessage = 'Visa Expiration Date cannot be less than Visa Start Date.';
            pageError = true;
            return null;
        }
        system.debug('@case53'+ImmigrationRecord.L1_Visa_Expiration_Date__c );
           if(ImmigrationRecord.L1_Visa_Expiration_Date__c <  ImmigrationRecord.L1_Visa_Start_Date__c)
        {        system.debug('@case53'+ImmigrationRecord.L1_Visa_Expiration_Date__c );
            pageErrorMessage = 'Visa Expiration Date cannot be less than Visa Start Date.';
            pageError = true;
            return null;
        }
        
           if(ImmigrationRecord.WCT_EAD_Card_Expires_On__c < ImmigrationRecord.WCT_EAD_Valid_On__c)
        {
            pageErrorMessage = 'EAD Card Expiration Date cannot be less than Valid Date.';
            pageError = true;
            return null;
        }
        //ImmigrationRecord.WCT_Assignment_Owner__c=LoggedInContact.id;
        
          
        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        t.OwnerId=UserInfo.getUserId();
        upsert t;

        

    
        //insert immigration record.
        Map<string,id> mapUserId = new Map<string,id>(); 
        List<User> lstAssignedUser = [select id,name from user where name in('GM & I','Fragomen')];
        for(User tmpUser : lstAssignedUser ){
            mapUserId.put(tmpUser.name,tmpUser.id);
        } 

        ImmigrationRecord.ownerId=mapUserId.get('GM & I'); 
        ImmigrationRecord.WCT_Immigration_Status__c='Visa Approved';
        ImmigrationRecord.WCT_Visa_Status__c='Visa Approved';
        insert ImmigrationRecord ;
        
        //updating mobility record.
        MobilityRecord.Immigration__c=ImmigrationRecord.id;
        update MobilityRecord;
        if(attachmentHelper.docIdList.isEmpty()) {
            pageErrorMessage = 'Attachment is required to submit the form.';
            pageError = true;
            return null;

        }
         attachmentHelper.uploadRelatedAttachment(taskid);
        

        //updating task record
        If (t.WCT_Auto_Close__c == true){   
            t.status = 'Completed';
        }else{
            t.status = 'Employee Replied';  
        }
        
        if(ownerIdStr.startsWith('00G'))
            t.OwnerId = mapUserId.get('GM & I');
        else
            t.OwnerId=ownerIdStr;

        t.WCT_Is_Visible_in_TOD__c=false; 
        upsert t;
       
          getAttachmentInfo();
          }
          
          catch (Exception e) {
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_L2_with_EAD_FormController', 'L2 with EAD Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_L2EAD_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
            
          }
          
         PageReference pageRef = new PageReference('/apex/WCT_L2_with_EAD_FormThankYou?taskid=' + t.id);
          pageRef.setRedirect(true);
          return pageRef;
  
}

 }