/********************************************************************************************************************************
Apex class         : <WCT_OnboardingInitiationForm>
Description        : <Controller which allows to Update Mobility, Assignment and Task>
Type               :  Controller
Test Class         : <WCT_Mobility_Onboard_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 25/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/ 
public class WCT_OnboardingInitiationForm  extends SitesTodHeaderController{

    /* public variables */
    public WCT_Assignment__c assignmentRec {get;set;}
    public WCT_Mobility__c mobilityRecord{get;set;}
    

    /* Upload Related Variables */
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public task t{get;set;}
    public String taskid{get;set;}
    public List<Attachment> listAttachments {get; set;}
    public Map<Id, String> mapAttachmentSize {get; set;}  
     public GBL_Attachments attachmentHelper{get; set;}
    //Controller Related Variables
    public String employeeType;
    public Contact Employee{get;set;}
    public Map<String,String> recordTypeMap {get;set;}
    public String assignmentstartdate {get;set;}
    public String assignmentenddate {get;set;}
    public String dateOfArrival {get;set;}
    public String lastWorkingDayUS {get;set;}

    // Error Message related variables
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    private boolean isAllFieldsValid;
 
 
    public WCT_OnboardingInitiationForm()
    {
        /*Initialize all Variables*/
        init();
        
        /*Get Task ID from Parameter*/
        getParameterInfo();  

        getMobilityTypeDropDownValues();
        
        /*Task ID Null Check*/
        if(taskid=='' || taskid==null)
        {
           invalidEmployee=true;
           return;
        }

        /*Get Task Instance*/
        getTaskInstance();
        
        /*Get Attachment List and Size*/
        getAttachmentInfo();
        
        /*Query to get Immigration Record*/  
        getMobilityDetails();
    }

/********************************************************************************************
*Method Name         : <init()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Init() Used for loading Mobility, Assignment and Attachments>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 
    @TestVisible private void init(){

        //Custom Object Instances
        mobilityRecord = new  WCT_Mobility__c();
        assignmentRec = new  WCT_Assignment__c();
        attachmentHelper= new GBL_Attachments();
        
        Employee=new Contact();
        employeeType = '';
        recordTypeMap = new Map<String,String>();

        //Document Related Init
        doc = new Document();
        listAttachments = new List<Attachment>();
        mapAttachmentSize = new Map<Id, String>();

        //Set Default Validation Flag to Pass All Validation
        isAllFieldsValid = true;
    }   

/********************************************************************************************
*Method Name         : <getParameterInfo()>
*Return Type         : <Null>
*Param’s             : URL
*Description         : <GetParameterInfo() Used to get URL Params for TaskID>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 
    
    @TestVisible private void getParameterInfo(){
        taskid = ApexPages.currentPage().getParameters().get('taskid');
       }
    
/********************************************************************************************
*Method Name         : <getTaskInstance()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetTaskInstance() Used for Querying Task Instance>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/      
   
    @TestVisible
    private void getTaskInstance(){
        t=[SELECT Status,OwnerId,WhatId,WCT_Is_Visible_in_TOD__c,WCT_ToD_Task_Reference__c, WCT_Auto_Close__c  FROM Task WHERE Id =: taskid];
    }

/********************************************************************************************
*Method Name         : <getMobilityTypeDropDownValues()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <getMobilityTypeDropDownValues() Used for Retrieving Visa Type>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/  
    
    public list<SelectOption> getMobilityTypeDropDownValues(){

        List<SelectOption> options = new List<SelectOption>();

        for(RecordType rt: [SELECT ID, Name FROM RecordType WHERE sObjectType = 'WCT_Mobility__c']){

            //Create Record Map
            recordTypeMap.put(rt.ID, rt.Name);

            if(String.isNotEmpty(employeeType)){
             if(employeeType == 'Employee'){
                    if(rt.Name == 'Employment Visa'){                    
                        options.add(new SelectOption(rt.ID, rt.Name));
                        mobilityRecord.recordTypeId = rt.Id;
                    }
                }
            }
        }
        return options;
    }

/********************************************************************************************
*Method Name         : <getAttachmentInfo()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetAttachmentInfo() Used for Retrieving Attachment Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/  
     @TestVisible
    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId , 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId = :taskid
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50 ];    
              
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }

/********************************************************************************************
*Method Name         : <getMobilityDetails()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetMobilityDetails() Used for Fetching Mobility Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     
   
    public void getMobilityDetails()
    {
        mobilityRecord = [SELECT WCT_Number_Of_Children__c, recordTypeId, WCT_Accompanying_Dependents__c,WCT_Date_of_Arrival_in_US__c, WCT_India_Cellphone__c,WCT_Accompanied_Status__c,WCT_Deloitte_US_Office_City__c,WCT_Deloitte_US_Office_State__c,WCT_Email_Received__c,
                            WCT_First_Working_Day_in_US__c,WCT_Last_Working_Day_in_India__c,WCT_Last_Working_Day_in_US__c,WCT_Mobility_Employee__c,WCT_Mobility_Status__c,WCT_Mobility_Type__c, 
                            WCT_Project_Controller__c,WCT_USI_Resource_Manager__c,WCT_US_Project_Mngr__c,WCT_To_Country__c,WCT_Travel_Duration__c,WCT_Travel_End_Date__c,WCT_Travel_Insurance_End_Date__c,WCT_Travel_Start_Date__c,
                            WCT_Purpose_of_Travel__c,WCT_US_Benefits_Effective_Date__c,WCT_US_Contact_Number__c,ownerid,WCT_Visa_Type__c,WCT_Regional_Dest__c,WCT_US_RCCode__c,WCT_USI_RCCode__c
                            FROM WCT_Mobility__c
                            where id=:t.WhatId];   
                           
    }

/********************************************************************************************
*Method Name         : <save()>
*Return Type         : <PageReference>
*Param’s             : 
*Description         : <Save() Used for for Updating Task and Immigration>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 
   
    public pageReference save()
    {

        /*Custom Validation Rules are handled here*/

        if( (null == mobilityRecord.WCT_India_Cellphone__c) ||
            ('' == mobilityRecord.WCT_India_Cellphone__c) ||
            (null == assignmentRec.WCT_Client_Office_Name__c) ||
            ('' == assignmentRec.WCT_Client_Office_Name__c) ||
            (null == assignmentRec.WCT_Client_Office_City__c) ||
            ('' == assignmentRec.WCT_Client_Office_City__c) ||
            (null == mobilityRecord.WCT_Project_Controller__c) ||
            ('' == mobilityRecord.WCT_Project_Controller__c) ||
            (null == assignmentRec.WCT_Client_Office_State__c) ||
            ('' == assignmentRec.WCT_Client_Office_State__c) ||
            (null == mobilityRecord.WCT_US_Project_Mngr__c) ||
            ('' == mobilityRecord.WCT_US_Project_Mngr__c)||
            (null == assignmentRec.WCT_Regional_Dest__c) ||
            ('' == assignmentRec.WCT_Regional_Dest__c)||
            //(null == mobilityRecord.WCT_USI_RCCode__c) ||
            //('' == mobilityRecord.WCT_USI_RCCode__c)||
            (null == dateOfArrival) ||('' == dateOfArrival)) 
            {

            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
           }
        
        lastWorkingDayUS = String.valueOf(mobilityRecord.WCT_Last_Working_Day_in_US__c);
        
        RecordType rt1 = [SELECT ID, Name FROM RecordType WHERE sObjectType = 'WCT_Mobility__c'and id=:mobilityRecord.recordTypeId];
        if(rt1.Name == 'Employment Visa'){
            if((null != assignmentstartdate ) || ('' != assignmentstartdate ) ||
                (null != assignmentenddate ) || ('' != assignmentenddate )){       
                if(String.isNotBlank(assignmentstartdate )){
                    assignmentRec.WCT_Initiation_Date__c = Date.parse(assignmentstartdate );
                }
                if(String.isNotBlank(assignmentenddate )){
                    assignmentRec.WCT_End_Date__c = Date.parse(assignmentenddate );
                }
                
                if(assignmentRec.WCT_Initiation_Date__c > assignmentRec.WCT_End_Date__c) {
                        pageErrorMessage = 'The Assignment End Date cannot be before the Assignment End Date. Please correct.';
                        pageError = true;
                        return null;
                }
                if(assignmentRec.WCT_Initiation_Date__c < system.today() || assignmentRec.WCT_End_Date__c < system.today()){
                        pageErrorMessage = 'The Assignment Start Date and End Date cannot be a past date. Please correct.';
                        pageError = true;
                        return null;   
                }
                 
                if(String.valueOf(assignmentRec.WCT_End_Date__c) != lastWorkingDayUS){
                        pageErrorMessage = 'The Assignment End Date must be same as Last Working Day in US which is: '+lastWorkingDayUS+'. Please correct.';
                        pageError = true;
                        return null;   
                }
            }
            else
            {
                 pageErrorMessage = 'Please fill in all the required fields on the form.';
                 pageError = true;
                 return null;
            }
       }
        try{
        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        t.OwnerId=UserInfo.getUserId();
        upsert t;
        
        pageError = false;
           
        /*update mobility Record*/
         if(String.isNotBlank(dateOfArrival)){
            mobilityRecord.WCT_Date_of_Arrival_in_US__c = Date.parse(dateOfArrival);
        }
        if(mobilityRecord.WCT_Date_of_Arrival_in_US__c < system.today()){
                        pageErrorMessage = 'The Date of Arrival in US cannot be a past date. Please correct.';
                        pageError = true;
                        return null;   
                }
        
       
        
        
        if(t.WCT_ToD_Task_Reference__c!='0013')
          mobilityRecord.WCT_Mobility_Status__c='Onboarding Completed';
        update mobilityRecord;
            
        //creating assignment record and association it with mobility record updated above.
        if(mobilityRecord.WCT_Mobility_Type__c=='Employment Visa')
        {
          assignmentRec.WCT_Mobility__c=mobilityRecord.id;
          assignmentRec.WCT_Employee__c = mobilityRecord.WCT_Mobility_Employee__c;
          assignmentRec.OwnerId = mobilityRecord.OwnerId;
          insert assignmentRec;

        }
        /* Upload Documents as Related Attachments to Case After the Case Record is created */
        /*
       if(attachmentHelper.docIdList.isEmpty()) {
            pageErrorMessage = 'Attachment is required to submit the form.';
            pageError = true;
            return null;

        }
        */
       if(!attachmentHelper.docIdList.isEmpty()) {
               attachmentHelper.uploadRelatedAttachment(taskid);

        } 
         
        if(string.valueOf(mobilityRecord.Ownerid).startsWith('00G')){
            //owner is Queue. So set it to GM&I User
           // t.Ownerid = '005f0000001AWth';
            t.Ownerid = System.Label.GMI_User;
        }else{
            t.OwnerId = mobilityRecord.Ownerid;
        }
        

        //updating task record
        if(t.WCT_Auto_Close__c == true){   
            t.status = 'completed';
        }else{
            t.status = 'Employee Replied';  
        }

        t.WCT_Is_Visible_in_TOD__c = false; 
        upsert t;
        
        
         mobilityRecord.WCT_Regional_Dest__c = assignmentRec.WCT_Regional_Dest__c;
        //Reset Page Error Message
        pageError = false;
        
        getAttachmentInfo();
         /*Set Page Reference After Save*/
       
          
        }
        catch (Exception e) {
         Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_OnboardingInitiationForm', 'Onboarding Initiation', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_OBIN_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;       
        }
         Pagereference pg = new Pagereference('/apex/WCT_OnboardingInitiationForm_ThankYou?taskid='+taskid);
            pg.setRedirect(true);
            return pg; 
        
    }

  
}