/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage. 
 */
@isTest
global class WebServiceMockImpl implements HttpCalloutMock {
//global class MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals(Label.DescribecallEndPoint, req.getEndpoint());
        System.assertEquals('POST', req.getMethod());
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        String s=' <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:partner.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> <soapenv:Header> <LimitInfoHeader> <limitInfo> <current>9511</current> <limit>5000000</limit> <type>API REQUESTS</type> </limitInfo> </LimitInfoHeader> </soapenv:Header> <soapenv:Body> <describeLayoutResponse> <result> <recordTypeMappings> <available>true</available> <defaultRecordTypeMapping>true</defaultRecordTypeMapping> <layoutId>00h40000001H4lHAAS</layoutId> <name>Case</name> <picklistsForRecordType> <picklistName>Type</picklistName> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Complaint</label> <value>Complaint</value> </picklistValues> </picklistsForRecordType> <picklistsForRecordType> <picklistName>Status</picklistName> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Assigned to Tier 2</label> <value>Assigned to Tier 2</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Canceled</label> <value>Canceled</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Closed</label> <value>Closed</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Closed – Duplicate</label> <value>Closed – Duplicate</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>In Progress</label> <value>In Progress</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>true</defaultValue> <label>New</label> <value>New</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Pending - Customer Action</label> <value>Pending - Customer Action</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Pending - Non SFDC</label> <value>Pending - Non SFDC</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Pending – Tier 2 Action</label> <value>Pending – Tier 2 Action</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Pending – Tier 3 Action</label> <value>Pending – Tier 3 Action</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Pending - Vendor Action</label> <value>Pending - Vendor Action</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Reopened</label> <value>Reopened</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Resolved</label> <value>Resolved</value> </picklistValues> </picklistsForRecordType> <picklistsForRecordType> <picklistName>Reason</picklistName> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>User did attend training</label> <value>User did attend training</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Complex functionality</label> <value>Complex functionality</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Existing problem</label> <value>Existing problem</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Instructions not clear</label> <value>Instructions not clear</value> </picklistValues> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>New problem</label> <value>New problem</value> </picklistValues> </picklistsForRecordType> <picklistsForRecordType> <picklistName>Origin</picklistName> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Chat</label> <value>Chat</value> </picklistValues> </picklistsForRecordType> <picklistsForRecordType> <picklistName>Priority</picklistName> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>1 - Critical</label> <value>1 - Critical</value> </picklistValues> </picklistsForRecordType> <picklistsForRecordType> <picklistName>How_would_you_like_for_us_to_contact_you__c</picklistName> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Phone</label> <value>Phone</value> </picklistValues> </picklistsForRecordType> <picklistsForRecordType> <picklistName>WCT_Category__c</picklistName> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Acquisition</label> <value>Acquisition</value> </picklistValues> </picklistsForRecordType> <picklistsForRecordType> <picklistName>WCT_ContactChannel__c</picklistName> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Phone</label> <value>Phone</value> </picklistValues> </picklistsForRecordType> <picklistsForRecordType> <picklistName>WCT_Requisition_FSS__c</picklistName> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Audit</label> <value>Audit</value> </picklistValues> </picklistsForRecordType> <picklistsForRecordType> <picklistName>WCT_Requisition_Service_Area__c</picklistName> <picklistValues> <active>true</active> <defaultValue>false</defaultValue> <label>Audit</label> <value>Audit</value> </picklistValues> </picklistsForRecordType> <recordTypeId>01240000000DuEPAA0</recordTypeId> </recordTypeMappings> <recordTypeSelectorRequired>true</recordTypeSelectorRequired> </result> </describeLayoutResponse> </soapenv:Body> </soapenv:Envelope>';
        res.setBody(s);
        res.setStatus('OK');
        res.setStatusCode(200);
        return res;
    }
}