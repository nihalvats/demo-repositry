/*******************************************************************
 APEX CLASS :WCT_batchArticleExpNotify_2days
 DESCRIPTION: This batch class is written to update the article to Draft if the article gets expired
 CREATED BY: Chandrasekhar Pinarouthu
 CREATED DATE:8/15/2013
********************************************************************/
global class WCT_batchArticleExpNotify_2days implements Database.batchable< KnowledgeArticleVersion >{

    /******************************************
    METHOD NAME: Start
    DESCRIPTION: This method will fetch all the article from KnowledgeArticleVersion. 
                 The output of this method is a query passed as an input to Execute method
    ******************************************/
global Iterable< KnowledgeArticleVersion > start(Database.batchableContext info){

       return new WCT_KAVExp();
    }

    global void execute(Database.BatchableContext BC, List < sObject > articles) {
    
    WCT_ArticleExp_EmailNotify_2days.kavNames();

    }
    global void finish(Database.BatchableContext BC ) {
    }
}