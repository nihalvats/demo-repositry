@isTest
private class WCT_Accept_OR_Decline_Event_Test
{
    public static testmethod void m1()
    {
         Event eventRec=new Event(StartDateTime=system.now(),EndDateTime=system.now()+1);
         insert eventRec;
         
         Contact contactRec = WCT_UtilTestDataCreation.createContact();
         contactRec.Email='candidate@deloitte.com';
         insert contactRec;
         
         EventRelation InterviewEventRelation=new EventRelation(EventId=eventRec.ID, IsInvitee=true,Status='New',RelationId = contactRec.id);
         insert InterviewEventRelation;
         Test.startTest();
         WCT_Accept_OR_Decline_Event.sendInvite(eventRec.id);
         Test.stopTest();
         
         
         
    }
}