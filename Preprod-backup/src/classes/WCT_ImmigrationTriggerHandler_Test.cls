@isTest
private class WCT_ImmigrationTriggerHandler_Test
{
    public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        Test.startTest(); 
        
        WCT_Immigration__c imRec = new WCT_Immigration__c();
        imRec.WCT_Immigration_Status__c = 'New';
        imRec.WCT_Assignment_Owner__c=con.id;
      imRec.H1B_Is_USI_Upload__c=True;
       imRec.H1B_Is_US_Upload__c=True;
        INSERT imRec; 
        
  
        
        Task tempTask = new Task();
        tempTask.WhatId = imRec.Id;
        tempTask.Status = 'Not Started';
        INSERT tempTask;
        
        imRec.WCT_Immigration_Status__c = 'Initiate Onboarding';
        UPDATE imRec;
     
        
        /*Test code to cover the markEmployeeHasH1B */
         imRec.H1B_Process_Status__c='Petition Approved';
         UPDATE imRec;
        
         Test.stopTest();

        
    }
    
      static testmethod void contemailupdate_Test()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email = 'test@gmail.com';
        insert con;
        
        Contact PMcon=WCT_UtilTestDataCreation.createEmployee(rt.id);
        PMcon.Email = 'test@gmail.com';
        insert PMcon;
        
        Contact PCcon=WCT_UtilTestDataCreation.createEmployee(rt.id);
        PCcon.Email = 'test@Yahoo.com';
        insert PCcon;
        
        Contact BSpocon=WCT_UtilTestDataCreation.createEmployee(rt.id);
        BSpocon.Email = 'test@Yahoo.com';
        insert BSpocon;
        
        Contact FSSpoc=WCT_UtilTestDataCreation.createEmployee(rt.id);
        FSSpoc.Email = 'test@deloitte.com';
        insert FSSpoc;
        
        Contact RMcon=WCT_UtilTestDataCreation.createEmployee(rt.id);
        RMcon.Email = 'test@deloitte.com';
        insert RMcon;
        
        WCT_Immigration__c imRec = new WCT_Immigration__c();
        imRec.WCT_Immigration_Status__c = 'New';
        imRec.H1B_Is_USI_Upload__c=True;
        imRec.WCT_Assignment_Owner__c = con.id;
        imRec.WCT_Project_Manager__c = PMcon.id;
        imRec.WCT_Project_Controller__c = PCcon.id;
        imRec.WCT_Business_SPOC__c = BSpocon.id;
        imRec.WCT_FSS_SPOC__c = FSSpoc.id;
        imRec.WCT_Resource_Manager__c = RMcon.id;
        INSERT imRec; 
        
       Test.startTest(); 
        
          /*Test code to cover the contemailupdate */       
        imRec.WCT_Project_Manager_Email_Id__c = label.WCT_Talent_SFDC_Admin;
        imRec.WCT_Project_Controller_Email_ID__c = label.WCT_Talent_SFDC_Admin;
        imRec.WCT_Business_Spoc_Emailid__c = label.WCT_Talent_SFDC_Admin;
        imRec.WCT_FSS_SPOC_Email_Id__c = 'test@deloitte.com';
        imRec.WCT_Resource_Manager_Email_ID__c = 'test@deloitte.com';
        UPDATE imRec;
     
                    
        
         Test.stopTest();

        
    }
    
}