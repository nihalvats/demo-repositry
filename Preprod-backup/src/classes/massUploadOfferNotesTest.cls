@isTest
private class massUploadOfferNotesTest {

    @isTest
    Static void m1(){
    
        Blob contentFile;
        string strvar ='sample';
        contentFile = Blob.valueof(strvar);
        
       WCT_Offer_Data_Fields_Parameters__c fp = new WCT_Offer_Data_Fields_Parameters__c();
        fp.Name='TestSetting';
        fp.WCT_Sub_Area_Code__c='FAS';
        fp.WCT_Look_Up_Code__c='I442';
        fp.WCT_Designation_Code__c='AA';
        fp.WCT_Car_Lease_Eligibility__c=10;
        fp.WCT_Communication_Allowance__c=10;
        fp.WCT_Driver_Code__c='test';
        fp.WCT_Final_Title_to_be_used__c='test';
        fp.WCT_Fuel_Designation__c='test';
        fp.WCT_Medical_Insurance_Premium__c=5;
        fp.WCT_US_Level__c='test';
        fp.WCT_US_Level_Designation__c='test';
        fp.WCT_Variable_Percentage__c='0-10';
        fp.WCT_Vehicle_Running_Expenses__c=12;
        insert fp; 
    
        WCT_Offer_Note__c OfferNote = new WCT_Offer_Note__c();
        OfferNote.WCT_Offer__c = fp.Id;
        OfferNote.WCT_Description__c='Test';
        OfferNote.WCT_Offer_Reason__c='Accepted counter-offer from current employer';
        OfferNote.WCT_Offer_Status__c='Offer Declined';
        OfferNote.WCT_Subject__c='Test';
        insert OfferNote;  
        
       Test.StartTest() ;
       PageReference pageRef = Page.Offer_Note_Mass_Update;
       Test.setCurrentPage(pageRef);
       
       massUploadOfferNotes offnotevar = new massUploadOfferNotes();
       
       offnotevar.save();
       
       Test.StopTest() ;
       
    
    }






}