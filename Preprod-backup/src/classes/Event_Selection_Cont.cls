public class Event_Selection_Cont {
public string selectCategoryValue{get;set;}
public string selecteventvalue{get;set;}
public string thyperlink{get;set;}
public string ohyperlink{get;set;}
public Contact con{get;set;}
public list<Event__c> recevnt = new list<Event__c> ();
public Event__c ec{get;set;}
public Event_Selection_Cont(){

recevnt=[select name,id,Event_Category__c from event__c];
ec = new Event__c();
thyperlink='mailto:?subject=';
ohyperlink='http://talent.force.com/event/checkin?eid=';
}

   public list<selectoption> getcategorylist(){
        List<SelectOption> Options = new List<SelectOption>();
        Schema.DescribeFieldResult fieldResult =
        Event__c.Event_Category__c.getDescribe();
   List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
   options.add(new SelectOption('---Select Category---','---Select Category---'));
   for( Schema.PicklistEntry f : ple)
   {
      options.add(new SelectOption(f.getLabel(), f.getValue()));
   }
        return Options;
   }
   
   public list<selectoption> geteventlist(){
   list<SelectOption> Options = new list<SelectOption>();
          date Next = system.today()+2;
          date cnow = system.today();
          date Last = system.today()-2;
   options.clear();
    options.add(new SelectOption('---Select Event---','---Select Event---'));
   for(Event__c e:[select name,id from Event__c where Event_Category__c=:selectCategoryValue and Start_Date__c >=:Last and Start_Date__c <=:Next and Status__c <>'Completed']){
   options.add(new SelectOption(e.name,e.name));
   }
   return options;
    }
   public PageReference createlink(){
   try{
   thyperlink='mailto:?subject=';
   ohyperlink='http://talent.force.com/event/checkin?eid=';
   ec=[select id,name from event__c where name=:selecteventvalue limit 1];
   system.debug('*ind*'+ec.name);
   thyperlink+=ec.name;
   thyperlink+='&body=http://talent.force.com/event/checkin?eid=';
   thyperlink+=ec.id;
   ohyperlink+=ec.id;
   }
   catch(System.QueryException ex)
   {
   apexpages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please select event from dropdown.'));
   }
   return null;
   }
   public pagereference gotocheckin()
   {
   if(selectCategoryValue=='---Select Category---')
   {
    apexpages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Provide Event Category'));
   return null;
   }
    if(selectEventValue=='---Select Event---')
   {
    apexpages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Provide Event Name'));
   return null;
   }
   
   
   else{
    pagereference pg = new pagereference (ohyperlink);
   system.debug('*aus*'+ohyperlink);
   return pg;
   }
   } 
}