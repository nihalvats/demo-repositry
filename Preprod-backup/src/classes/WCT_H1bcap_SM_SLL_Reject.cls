global class  WCT_H1bcap_SM_SLL_Reject Implements Database.Batchable <sObject> {
    global Database.queryLocator start(Database.BatchableContext bc) {
         
        Date d= system.today();
        String query = 'SELECT Id,WCT_H1BCAP_RM_Name__c, WCT_H1BCAP_Status__c,WCT_H1BCAP_USI_SM_Name_GDM__c,WCT_H1BCAP_USI_SLL_Name__c FROM WCT_H1BCAP__c WHERE WCT_H1BCAP_Status__c <> Null AND ( Wct_H1bCap_SM_RejectedDate__c = :d OR Wct_H1bcap_SM_Realigned_Date__c = :d OR Wct_H1bcap_SLL_RejectDate__c =:d OR Wct_H1bcap_SLL_RealignedDate__c =:d ) ' ;
       
         
        return Database.getQueryLocator(query);
       
    }

    global void execute(Database.BatchableContext bc, List<WCT_H1BCAP__c> H1bcap) {
    
        system.debug('size of the list' +H1bcap.size());
        string orgmail =  Label.WCT_H1BCAP_Mailbox;
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> SM_REALIGLST = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> SLL_REJ = new List<Messaging.SingleEmailMessage>();
         List<Messaging.SingleEmailMessage> SLL_REALIG = new List<Messaging.SingleEmailMessage>();
        OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
        Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'wct_H1bcap_Notification_to_SM_Reject' AND IsActive = true];
        Emailtemplate ET_SMREALIG = [select id, developername , IsActive from Emailtemplate where developername = 'wct_H1bcap_Notification_to_SM_Realigned' AND IsActive = true];
        Emailtemplate ET_SLLREJ = [select id, developername , IsActive from Emailtemplate where developername = 'wct_H1bcap_Notification_to_SLL_Reject' AND IsActive = true];
         Emailtemplate ET_SLLREALIG = [select id, developername , IsActive from Emailtemplate where developername = 'wct_H1bcap_Notification_to_SLL_Realigned' AND IsActive = true];
 
        for(WCT_H1BCAP__c w: H1bcap) {
          
          if(w.WCT_H1BCAP_Status__c == 'USI SM Rejected' && null != w.WCT_H1BCAP_RM_Name__c) 
     {
           //list<string>  currentRMEmail = new list<string>();
          // currentRMEmail.add( w.WCT_H1BCAP_Resource_Manager_Email_ID__c);
           system.debug('size of the list' +H1bcap.size());
           system.debug('ID**********' +w.id);
           system.debug('SLL Name1**********' +w.WCT_H1BCAP_USI_SLL_Name__c );                
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
            mail.SetTemplateid(et.id);
            mail.setSaveAsActivity(true);
            mail.setTargetObjectId(w.WCT_H1BCAP_RM_Name__c);
          //  mail.setCcAddresses(currentRMEmail);
            mail.setWhatid(w.id);
            mail.setOrgWideEmailAddressId(owe.id);
           
            mailList.add(mail);   
           } 
              
        } 
        if(mailList.size() >0)
      {  
        try{
                Messaging.sendEmail(mailList);   
             }   
          catch(Exception npe) {
          System.debug('The following exception has occurred: ' + npe.getMessage());
             }       
      } 
        
       
        
        // Second Mail Method
        
        for(WCT_H1BCAP__c w: H1bcap) {
          
          if(w.WCT_H1BCAP_Status__c == 'USI SM Realigned' && null != w.WCT_H1BCAP_RM_Name__c) 
     {
           //list<string>  currentRMEmail = new list<string>();
          // currentRMEmail.add( w.WCT_H1BCAP_Resource_Manager_Email_ID__c);
           system.debug('size of the list' +H1bcap.size());
           system.debug('ID**********' +w.id);
                           
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
            mail.SetTemplateid(ET_SMREALIG.id);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(w.WCT_H1BCAP_RM_Name__c);
          //  mail.setCcAddresses(currentRMEmail);
            mail.setWhatid(w.id);
            mail.setOrgWideEmailAddressId(owe.id);
           
            SM_REALIGLST.add(mail);   
           } 
              
        } 
        if(SM_REALIGLST.size() >0)
      {  
        try{
                Messaging.sendEmail(SM_REALIGLST);   
             }   
          catch(Exception npe) {
          System.debug('The following exception has occurred: ' + npe.getMessage());
             }       
      } 
             
      
         
       // Third Mail Method
        
        for(WCT_H1BCAP__c w: H1bcap) {
          
          if(w.WCT_H1BCAP_Status__c == 'USI SLL Rejected' && null != w.WCT_H1BCAP_RM_Name__c) 
     {
           //list<string>  currentRMEmail = new list<string>();
          // currentRMEmail.add( w.WCT_H1BCAP_Resource_Manager_Email_ID__c);
           system.debug('size of the list' +H1bcap.size());
           system.debug('SLL Name**********' +w.WCT_H1BCAP_USI_SLL_Name__c );
                           
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
            mail.SetTemplateid(ET_SLLREJ.id);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(w.WCT_H1BCAP_RM_Name__c);
          //  mail.setCcAddresses(currentRMEmail);
            mail.setWhatid(w.id);
            mail.setOrgWideEmailAddressId(owe.id);
           
            SLL_REJ.add(mail);   
           } 
              
        } 
        if(SLL_REJ.size() >0)
      {  
        try{
                Messaging.sendEmail(SLL_REJ);   
             }   
          catch(Exception npe) {
          System.debug('The following exception has occurred: ' + npe.getMessage());
             }       
      }    
      
       // Fourth Mail Method
        
        for(WCT_H1BCAP__c w: H1bcap) {
          
          if(w.WCT_H1BCAP_Status__c == 'USI SLL Realigned' && null != w.WCT_H1BCAP_RM_Name__c) 
     {
           //list<string>  currentRMEmail = new list<string>();
          // currentRMEmail.add( w.WCT_H1BCAP_Resource_Manager_Email_ID__c);
           system.debug('size of the list' +H1bcap.size());
           system.debug('SLL Name**********' +w.WCT_H1BCAP_USI_SLL_Name__c );
                           
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
            mail.SetTemplateid(ET_SLLREALIG.id);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(w.WCT_H1BCAP_RM_Name__c);
          //  mail.setCcAddresses(currentRMEmail);
            mail.setWhatid(w.id);
            mail.setOrgWideEmailAddressId(owe.id);
           
            SLL_REALIG.add(mail);   
           } 
              
        } 
        if(SLL_REALIG.size() >0)
      {  
        try{
                Messaging.sendEmail(SLL_REALIG);   
             }   
          catch(Exception npe) {
          System.debug('The following exception has occurred: ' + npe.getMessage());
             }       
      }          
      
            
      
}
    global void finish(Database.BatchableContext bc) {
    }
  }