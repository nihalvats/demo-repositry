public class Updated_GMI
{
    public integer totalrecords { get; set; }
    public integer totalsuccessrec { get; set; }
    public integer totalunsuccessrec { get; set; }
    public String nameFile { get; set; }
    public String fileName { get; set; }
    public integer size{get; set;}
   
    
    transient public Blob contentFile { get; set; }
    transient public List<List<String>> fileLines = new List<List<String>>();
    
    transient List<WCT_H1BCAP__c> wageList;
    List<String> rows;
    List<String> failedLines;
    WCT_parseCSV parseCSVInstance = new WCT_parseCSV();  
    
    public boolean b {get{
       if(rows != null && rows.size() > 1)
       {
           system.debug('TRUE*********');
          return true;
       }
       else 
       {
           system.debug('FALSE*********');
          return false;
       }
     }
    }  
/*   
  public Updated_GMI (WCT_RMPortalControllerDesign controller) {

    }  
  */ 
    
    public Pagereference ReadFile()
    {   
        wageList = new List<WCT_H1BCAP__c>();
        rows = new List<String>();
        fileLines = new List<List<String>>();
                
        totalrecords = 0;
        totalsuccessrec = 0;
        totalunsuccessrec = 0;
        
        try {
            wageList = uploadProcess();   
            system.debug('Entered Uploadprocess **********');
        } 
  /*
      Catch(System.StringException stringException)
        {    
            
            if(wageList.size()<0)
           { 
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_String_Exception_Error);
            ApexPages.addMessage(errormsg);  
           }   
        }
        
        
        Catch(System.ListException listException)
        {    
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Columns_count_not_proper);
            ApexPages.addMessage(errormsg); 
        }    
   */  
       
        Catch(Exception e)
        {     
            System.debug('e.getCause'+e.getCause());
            System.debug('e.getLinenumber'+e.getLinenumber());
            System.debug('e.getMessage'+e.getMessage());
            
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_Exception);
            ApexPages.addMessage(errormsg);
          
              
        }                     
        
        totalrecords = wageList.size();
      system.debug('Error Displays **********' +totalrecords);  
     // update wageList;
      system.debug(' Displays **********' +wageList);  
      
       
       Database.SaveResult[] srList = Database.update(wageList,false);
        System.debug('srList'+srList);
        
        for(Database.SaveResult sr: srList)
        {
            if(!sr.IsSuccess())
            {    
                for(Database.Error err : sr.getErrors())
                {
                     ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.WCT_Data_Error+err.getMessage());
                     ApexPages.addMessage(errormsg);       
                }
            }
        }           
        
        return null; 
    }     
    
    public List<WCT_H1BCAP__c> uploadProcess() {
        
        /*Validtion if no file selected */
        if(contentFile!=null)
        {
            system.debug('selected something**********');
            fileName = nameFile;
            nameFile = contentFile.toString();
            System.debug('nameFile'+nameFile);
            filelines = parseCSVInstance.parseCSV(nameFile, true);
            System.debug('nameFile1'+filelines);
            failedLines = nameFile.split('\r\n');
            System.debug('failedLines'+failedLines);
        }
        else
        {    
            system.debug('Not selected **********');
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please choose a file to upload !'));
            
            //return null;
        }
        //Map for User
        
        Map<String, Id> sllMap = new Map<String, Id>();
        Map<String, Id> smMap = new Map<String, Id>();
        Map<String, Id> rmMap = new Map<String, Id>();
        Map<String, Id> ppdMap = new Map<String, Id>();
        Map<String, Id> delMap = new Map<String, Id>();
        
        //Map for Contacts
        Map<String, Id> fssMap = new Map<String, Id>();
        Map<String, Id> serviceMap = new Map<String, Id>();
        Map<Id,String> employeeStatusMap = new Map<Id,String>();
        Map<String, Id> deploymentMap = new Map<String, Id>();
        Map<String, Id> projectMap = new Map<String, Id>();
        Map<String, Id> expatMap = new Map<String, Id>();
        Map<String, Id> businessMap = new Map<String, Id>();
         

        
        
        
        //SET for User
          set<String> sllUserList = new set<String>(); 
          set<String> smUserList = new set<String>();
          set<String> rmUserList = new set<String>();
          set<String> ppdUserList = new set<String>();   
          set<String> delUserList = new set<String>();
        
        //SET for Contact
         set<String> fssEmailList = new set<String>();
         set<String> serviceEmailList = new set<String>();
         set<String> deploymentEmailList = new set<String>();
         set<String> projectEmailList = new set<String>();   
         set<String> expatEmailList = new set<String>();
         set<String> businessEmailList = new set<String>();
        
        
        set<string> personnelidList = new set<string>();
        set<id> h1bIdPersonnelid = new set<id>();
        
        for(List<String> inputValues:filelines) 
        {
            personnelidList.add(string.valueOf(inputvalues[1]));
            system.debug('personnelidList****'+personnelidList);
            system.debug('Size of the personnelidList****'+personnelidList.size());
            rmUserList.add(String.valueOf(inputvalues[18])); 
            smUserList.add(String.valueOf(inputvalues[21])); 
            ppdUserList.add(String.valueOf(inputvalues[32]));
            sllUserList.add(String.valueOf(inputvalues[35]));
            serviceEmailList.add(String.valueOf(inputvalues[40]));
            fssEmailList.add(String.valueOf(inputvalues[43]));          
            delUserList.add(String.valueOf(inputvalues[46]));
            projectEmailList.add(String.valueOf(inputvalues[49]));  
            expatEmailList.add(String.valueOf(inputvalues[52]));            
            deploymentEmailList.add(String.valueOf(inputvalues[55])); 
            businessEmailList.add(String.valueOf(inputvalues[58]));             
        }
    
    
     //Quering the list of RM
     List<User> rmList = [SELECT Id,Email FROM User WHERE Email IN :rmUserList AND IsActive = true ];
        for(User u :rmList) {
           rmMap.put(String.valueOf(u.Email), u.Id);
        }
    system.debug('SLLs list *********'+sllUserList);
    
    //Quering the list of SLL
      List<User> sllList = [SELECT Id,Email FROM User WHERE Email IN :sllUserList AND IsActive = true ];
        for(User u :sllList) {
           sllMap.put(String.valueOf(u.Email), u.Id);
       }
       system.debug('SLLs *********'+sllMap);
    //Quering the list of SM
    List<User> smList = [SELECT Id,Email FROM User WHERE Email IN :smUserList AND IsActive = true ];
        for(User u :smList) {
           smMap.put(String.valueOf(u.Email), u.Id);
       
        }
    
     List<User> ppdList = [SELECT Id,Email FROM User WHERE Email IN :ppdUserList AND IsActive = true LIMIT 1];
        for(User u :ppdList) {
           ppdMap.put(String.valueOf(u.Email), u.Id);
       
        }
    
        List<User> delList = [SELECT Id,Email FROM User WHERE Email IN :delUserList AND IsActive = true LIMIT 1];
        for(User u :delList) {
           delMap.put(String.valueOf(u.Email), u.Id);
       
        }
    
       List<Contact> fssconList = [SELECT Id, WCT_Personnel_Number__c,Email FROM Contact WHERE Email IN :fssEmailList AND WCT_Contact_Type__c='Employee' AND WCT_Employee_Status__c='Active' LIMIT 1];
        for(Contact c :fssconList) {
           fssMap.put(String.valueOf(c.Email), c.Id);
       
        }

       
       List<Contact> serviceconList = [SELECT Id, WCT_Personnel_Number__c,Email FROM Contact WHERE Email IN :serviceEmailList AND WCT_Contact_Type__c='Employee' AND WCT_Employee_Status__c='Active' LIMIT 1];
        for(Contact c :serviceconList) {
           serviceMap.put(String.valueOf(c.Email), c.Id);
       
        }
        

        List<Contact> deployList = [SELECT Id, WCT_Personnel_Number__c,Email FROM Contact WHERE Email IN :deploymentEmailList AND WCT_Contact_Type__c='Employee' AND WCT_Employee_Status__c='Active' LIMIT 1];
        for(Contact c:deployList) {
            deploymentMap.put(String.valueOf(c.Email), c.Id);
       
        }
        
       List<Contact> projList = [SELECT Id, WCT_Personnel_Number__c,Email FROM Contact WHERE Email IN :projectEmailList AND WCT_Contact_Type__c='Employee' AND WCT_Employee_Status__c='Active' LIMIT 1];
        for(Contact c :projList) {
           projectMap.put(String.valueOf(c.Email), c.Id);
       
        }
                
         List<Contact> expatList = [SELECT Id, WCT_Personnel_Number__c,Email FROM Contact WHERE Email IN :expatEmailList AND WCT_Contact_Type__c='Employee' AND WCT_Employee_Status__c='Active' LIMIT 1];
        for(Contact c:expatList) {
            expatMap.put(String.valueOf(c.Email), c.Id);
       
        }
        
       List<Contact> businessList = [SELECT Id, WCT_Personnel_Number__c,Email FROM Contact WHERE Email IN :businessEmailList AND WCT_Contact_Type__c='Employee' AND WCT_Employee_Status__c='Active' LIMIT 1];
        for(Contact c :businessList) {
           businessMap.put(String.valueOf(c.Email), c.Id);
       
        }
    
    
    
    
    
        List<WCT_H1BCAP__c> h1bList = [SELECT Id, WCT_H1BCAP_Practitioner_Personal_Number__c FROM WCT_H1BCAP__c WHERE Id IN :personnelidList];
       system.debug('size of the list*****'+h1bList.size());
        for(WCT_H1BCAP__c c:h1bList) {
           h1bIdPersonnelid.add(c.Id);
           System.debug('h1bIdPersonnelid'+h1bIdPersonnelid);
           
        }
        
        rows.add(failedLines[0]);
        Integer counter = 1;        
        
        for(List<String> inputvalues:filelines) 
        {       
            totalrecords++;
            if(!h1bIdPersonnelid.contains(inputvalues[1]) ) {
             
             system.debug('Fire*******************' +inputvalues[1]);  
                totalunsuccessrec++;
                
                rows.add(failedLines[counter]);
                
            } else {
                totalsuccessrec++;
                WCT_H1BCAP__c h1b = new WCT_H1BCAP__c();
              system.debug('Entering the looop1*******'); 
                if((inputvalues.size()>0 && inputvalues[0]!=null)?inputvalues[0].trim()!='':false){
                   h1b.WCT_Sl_No__c = integer.valueOf(inputvalues[0]!=null?inputvalues[0].trim():inputvalues[0]);
                   system.debug('SNO*******************' +h1b.WCT_Sl_No__c);
                }
                
                String idString=inputvalues[1];
                System.debug('### idString'+idString+'Sample');
                Id idtemp=idString;
                System.debug('### idtemp'+idtemp+'Sample');
             
                h1b.id=idtemp;
               
                h1b.WCT_H1BCAP_Noti_sent_to_RM_Misseddetails__c = true; //This field to send mails to RM's for controlling in future
               
                h1b.WCT_H1BCAP_Practitioner_Personal_Number__c = inputvalues[2];
                h1b.WCT_H1BCAP_Practitioner_Name__c = (inputvalues.size()>3 && inputvalues[3]!='')?inputvalues[3]:Null ;
               // h1b.WCT_H1BCAP_Practitioner_Name__r.Name = inputvalues[4];
                h1b.WCT_H1BCAP_Email_ID__c = (inputvalues.size()>5 && inputvalues[5]!='')?inputvalues[5]:'' ;
              
              
                if(inputvalues[6]!=null?inputvalues[6].trim()!='':false)
             {   
                h1b.WCT_H1BCAP_Practitioner_Level__c = inputvalues[6]!=null?inputvalues[6].trim():inputvalues[6]; 
                system.debug('level*******'+h1b.WCT_H1BCAP_Practitioner_Level__c); 
              }  
                /*  if(inputvalues.size()>7 && inputvalues[7]!='')
                 {
                     h1b.WCT_H1BCAP_Practitioner_Date_of_Joining__c =date.valueof(inputvalues[7]);
                 }
                */ 
              if(inputvalues.size()>8 && inputvalues[8]!='')
                  {
                       h1b.WCT_H1BCAP_Deloitte_Tenure_Years__c = decimal.valueof(inputvalues[8]);
                       system.debug('Years*******'+h1b.WCT_H1BCAP_Deloitte_Tenure_Years__c); 
                  }
                     
                h1b.WCT_H1BCAP_Deloitte_Entity__c = (inputvalues.size()>9 && inputvalues[9]!='')?inputvalues[9]:'';
                h1b.WCT_H1BCAP_Deloitte_Reporting_Office_Loc__c = (inputvalues.size()>10 && inputvalues[10]!='')?inputvalues[10]:'';
                h1b.WCT_H1BCAP_Service_Area__c = (inputvalues.size()>11 && inputvalues[11]!='')?inputvalues[11]:'';
                h1b.WCT_H1BCAP_Service_Line__c = (inputvalues.size()>12 && inputvalues[12]!='')?inputvalues[12]:'';
                h1b.WCT_H1BCAP_Capability__c = (inputvalues.size()>13 && inputvalues[13]!='')?inputvalues[13]:'';
                h1b.WCT_H1BCAP_Skill_set__c = (inputvalues.size()>14 && inputvalues[14]!='')?inputvalues[14]:'';
                h1b.WCT_H1BCAP_Educational_Background__c = (inputvalues.size()>15 && inputvalues[15]!='')?inputvalues[15]:'';
                 
             // h1b.WCT_H1BCAP_RM_Name__r.Name = inputvalues[17];
                h1b.WCT_H1BCAP_Resource_Manager_Email_ID__c = (inputvalues.size()>18 && inputvalues[18]!='')?inputvalues[18]:'';
                h1b.WCT_H1BCAP_RM_Name__c = rmMap.get(h1b.WCT_H1BCAP_Resource_Manager_Email_ID__c);  
                
             // h1b.WCT_H1BCAP_USI_SM_Name_GDM__r.Name = inputvalues[20]; 
                h1b.WCT_H1BCAP_GDM_Email_id__c =  (inputvalues.size()>21 && inputvalues[21]!='')?inputvalues[21]:'';
                h1b.WCT_H1BCAP_USI_SM_Name_GDM__c = smMap.get(h1b.WCT_H1BCAP_GDM_Email_id__c);
               system.debug('SM Name***********'+h1b.WCT_H1BCAP_USI_SM_Name_GDM__c);
                h1b.WCT_H1BCAP_Processing_Type__c = (inputvalues.size()>22 && inputvalues[22]!='')?inputvalues[22]:'';
                h1b.WCT_H1BCAP_Project_Name__c = (inputvalues.size()>23 && inputvalues[23]!='')?inputvalues[23]:'';
                system.debug('Project*******'+h1b.WCT_H1BCAP_Project_Name__c);
                h1b.WCT_H1BCAP_Rationale_project_alignment__c = (inputvalues.size()>24 && inputvalues[24]!='')?inputvalues[24]:'';
                h1b.WCT_H1BCAP_Client_Name__c = (inputvalues.size()>25 && inputvalues[25]!='')?inputvalues[25]:'';
           if((inputvalues.size()>26 && inputvalues[26]!=null)?inputvalues[26].trim()!='':false)
           {     
                h1b.WCT_H1BCAP_Client_Location_Flat_Number__c = decimal.valueof(inputvalues[26]!=null?inputvalues[26].trim():inputvalues[26]);
            } 
                h1b.WCT_H1BCAP_Client_location_Building__c = (inputvalues.size()>27 && inputvalues[27]!='')?inputvalues[27]:'';
                h1b.WCT_H1BCAP_Client_Location_City_State__c = (inputvalues.size()>28 && inputvalues[28]!='')?inputvalues[28]:'';
            if((inputvalues.size()>29 && inputvalues[29]!=null)?inputvalues[29].trim()!='':false)
            {
                
                h1b.WCT_H1BCAP_Client_Location_Zip_code__c = decimal.valueof(inputvalues[29]!=null?inputvalues[29].trim():inputvalues[29]);
                system.debug('Zip code**********'+h1b.WCT_H1BCAP_Client_Location_Zip_code__c);
             }
               
           // h1b.WCT_H1BCAP_US_PPD_Name__r.Name = inputvalues[31]; 
              h1b.WCT_H1BCAP_US_PPD_mail_Id__c= (inputvalues.size()>32 && inputvalues[32]!='')?inputvalues[32]:'';
              h1b.WCT_H1BCAP_US_PPD_Name__c = ppdMap.get(h1b.WCT_H1BCAP_US_PPD_mail_Id__c);
              h1b.WC_H1BCAP_USI_SLL_email_id__c = (inputvalues.size()>35 && inputvalues[35]!='')?inputvalues[35]:'';
              system.debug('SLL eamil******************'+h1b.WC_H1BCAP_USI_SLL_email_id__c );
              h1b.WCT_H1BCAP_USI_SLL_Name__c =  sllMap.get(h1b.WC_H1BCAP_USI_SLL_email_id__c);
              system.debug('SLL Name******************'+h1b.WCT_H1BCAP_USI_SLL_Name__c );
         //   h1b.WCT_H1BCAP_USI_SLL_Name__r.Name =  inputvalues[34];
              
              h1b.WCT_H1BCAP_Employment_Status__c= (inputvalues.size()>36 && inputvalues[36]!='')?inputvalues[36]:'';
              h1b.WCT_H1BCAP_RM_comments__c= (inputvalues.size()>37 && inputvalues[37]!='')?inputvalues[37]:'';
              system.debug('RM Comments******************'+h1b.WCT_H1BCAP_RM_comments__c);
              
         //   h1b.WCT_H1BCAP_Service_Leader_Name__r.Name = inputvalues[39];
              h1b.WCT_H1BCAP_Service_Area_Leader_email_Id__c=  (inputvalues.size()>40 && inputvalues[40]!='')?inputvalues[40]:'';
              system.debug('Service eamil******************'+h1b.WCT_H1BCAP_Service_Area_Leader_email_Id__c );
              h1b.WCT_H1BCAP_Service_Leader_Name__c =serviceMap.get(h1b.WCT_H1BCAP_Service_Area_Leader_email_Id__c); 

        //    h1b.WCT_H1BCAP_FSS_Leader_Name__r.Name = inputvalues[42];
              h1b.WCT_H1BCAP_FSS_Leader_Email_Id__c= (inputvalues.size()>43 && inputvalues[43]!='')?inputvalues[43]:'';
              h1b.WCT_H1BCAP_FSS_Leader_Name__c = fssMap.get(h1b.WCT_H1BCAP_FSS_Leader_Email_Id__c);
              
      //      h1b.WCT_H1BCAP_Delegate_for_USI_SM_GDM__r.Name = inputvalues[45];
              h1b.WCT_H1BCAP_Delegate_USI_SM_GDM_Email__c= (inputvalues.size()>46 && inputvalues[46]!='')?inputvalues[46]:'';
              h1b.WCT_H1BCAP_Delegate_for_USI_SM_GDM__c = delMap.get(h1b.WCT_H1BCAP_Delegate_USI_SM_GDM_Email__c); 
              
              
        //    h1b.WCT_H1BCAP_Project_Manager_Name__r.Name = inputvalues[48];
              h1b.WCT_H1BCAP_Project_Manager_Email_Id__c= (inputvalues.size()>49 && inputvalues[49]!='')?inputvalues[49]:'';
              h1b.WCT_H1BCAP_Project_Manager_Name__c = projectMap.get(h1b.WCT_H1BCAP_Project_Manager_Email_Id__c);  
              
              h1b.WCT_H1BCAP_USI_Expat__c = expatMap.get(h1b.WCT_H1BCAP_USI_Expat_email_id__c);
  //          h1b.WCT_H1BCAP_USI_Expat__r.Name = inputvalues[51];
              h1b.WCT_H1BCAP_USI_Expat_email_id__c= (inputvalues.size()>52 && inputvalues[52]!='')?inputvalues[52]:'';
              
    //        h1b.WCT_H1BCAP_Deployment_Advisor_Name__r.Name = inputvalues[54];
              h1b.WCT_H1BCAP_Deployment_Advisor_Email_id__c= (inputvalues.size()>55 && inputvalues[55]!='')?inputvalues[55]:'';
              h1b.WCT_H1BCAP_Deployment_Advisor_Name__c = deploymentMap.get(h1b.WCT_H1BCAP_Deployment_Advisor_Email_id__c);  
              
              
              
    //        h1b.WCT_H1BCAP_Business_immigration_champ__r.Name = inputvalues[57];
              h1b.WCT_H1BCAP_Business_email__c= (inputvalues.size()>58 && inputvalues[58]!='')?inputvalues[58]:'';
              h1b.WCT_H1BCAP_Business_immigration_champ__c = businessMap.get(h1b.WCT_H1BCAP_Business_email__c);
              system.debug('Business*********'+h1b.WCT_H1BCAP_Business_immigration_champ__c);
              h1b.WCT_H1BCAP_CE_Level__c= (inputvalues.size()>59 && inputvalues[59]!='')?inputvalues[59]:'';
              system.debug('CE Level*********'+h1b.WCT_H1BCAP_CE_Level__c);
              if((inputvalues.size()>60 && inputvalues[60]!=''))
            {  
              h1b.WCT_H1BCAP_YE_Rating__c=decimal.valueOf(inputvalues[60]);
            }
          /*  else
            {
                h1b.WCT_H1BCAP_YE_Rating__c = '';
            }   
          */     
       
          system.debug('Industry**********' +h1b.WCT_H1BCAP_Industry__c);
    h1b.WCT_H1BCAP_Industry__c =   (inputvalues.size()>61 && inputvalues[61]!='')?inputvalues[61]:'' ;
        system.debug('Industry**********' +h1b.WCT_H1BCAP_Industry__c);
         
           h1b.WCT_H1BCAP_Year_At_Level__c = (inputvalues.size()>62 && inputvalues[62]!='')?inputvalues[62]:'' ;
          system.debug('Year*******' +h1b.WCT_H1BCAP_Year_At_Level__c);  
           h1b.WCT_H1BCAP_Certification__c= (inputvalues.size()>63 && inputvalues[63]!='')?inputvalues[63]:'' ;
           system.debug('Certification*******' +h1b.WCT_H1BCAP_Certification__c);   
           h1b.WCT_H1BCAP_CPA_Certification__c= (inputvalues.size()>64 && inputvalues[64]!='')?inputvalues[64]:'' ;
          system.debug('Certification1*******' +h1b.WCT_H1BCAP_CPA_Certification__c);    
           h1b.WCT_H1BCAP_Total_experience__c= (inputvalues.size()>65 && inputvalues[65]!='')?inputvalues[65]:'' ;
          system.debug('Total Exp*******' +h1b.WCT_H1BCAP_Total_experience__c); 
          
         
          
           h1b.WCT_H1BCAP_Status__c  = 'Review Practitioner Nomination';
           system.debug('Status1***********' +h1b.WCT_H1BCAP_Status__c);
        
        
            if(inputvalues.size()>67 && inputValues[67]!='')
            {
                
             system.debug('Flagoff***********' +h1b.WCT_H1BCAP_Flagged_Off_by_GMI__c );
                h1b.WCT_H1BCAP_Flagged_Off_by_GMI__c = Boolean.valueof(inputvalues[67]);
              system.debug('Flagoff1***********' +h1b.WCT_H1BCAP_Flagged_Off_by_GMI__c );  
            }   
            system.debug('Notes***********' +h1b.WCT_H1BCAP_Notes_Given_GM_I__c);
             
             
             h1b.WCT_H1BCAP_Notes_Given_GM_I__c  =  (inputvalues.size()>68 && inputvalues[68]!='')?inputvalues[68]:'';
             
            
             system.debug('Notes1***********' +h1b.WCT_H1BCAP_Notes_Given_GM_I__c); 
                wageList.add(h1b);  
                system.debug('size of the practitionerlist***********' +wageList.size());
            }
            counter++;  
        }
        System.debug('rows'+rows);

        return wageList;
      
    }
  /*      
    public pagereference pg()
    {
        return page.ViewFailedflaggedbygmiInUI;
    }
   */ 
    public String getFailedRows() {
        String result='';
        result = rows.remove(0);
        while(!rows.isEmpty()) {
            result += '\r\n' + rows[0];
            rows.remove(0);
        }
        
        return result;
    }
  
}