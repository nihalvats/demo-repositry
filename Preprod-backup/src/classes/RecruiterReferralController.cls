/************************************************************************************************************** 
    * Class Name   : RecruiterReferralController
    * Description  : This Class is PPD  
       
****************************************************************************************************************/
Public class RecruiterReferralController extends SitesTodHeaderController{

    /*Public Variables*/
    
    Public Case Ocase{get;set;}
    public List<String> docIdList = new List<string>();
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public Document doc {get;set;}
    Public Contact PPDcontact{get;set;}
    Public List<RecordType> OcaseRecordType;
    public String optionValue { get; set; }
    public Boolean Resume { get; set; }
    public Boolean Emailcorrespondence { get; set; }
    public Boolean chkBx { get; set; }
    public String input { get; set; }
    public Boolean display { get; set; }
    public Boolean chkdisable {get;set;}
    public Boolean chkdisable1 {get;set;}
    
    // Attachment Related Variables
    public GBL_Attachments attachmentHelper{get; set;}
   
    // page controller
    public RecruiterReferralController(){
        Ocase = new case();
        doc = new Document();
        OcaseRecordType = new List<RecordType>();
        OcaseRecordType = [SELECT ID, Name FROM RecordType WHERE sObjectType = 'Case' And Name =:'PPD-Referral' Limit 1];
        if(OcaseRecordType[0] != null){
        Ocase.recordtypeid = OcaseRecordType[0].id;
        }
        attachmentHelper= new GBL_Attachments();
     }
     
     // Enable/Disable check boxes
     public PageReference displaytext() {
        if(chkBx){
        display = true;
        chkdisable1 = True;
        }
        else{
        display = false;
        chkdisable1 = false;
        }
        return null;
    }
    public void disa(){
        if(Emailcorrespondence == true || Resume == true){
            chkdisable = True;
        }
        else{
            chkdisable = False;
        }
        
    }
   
    // details from contact
    Public void employeeDetail(){

        PPDcontact = new contact();
        if(Ocase.FCPA_PPD_Email_Address__c != null ||Ocase.FCPA_PPD_Email_Address__c !=''){
        PPDcontact =[SELECT Id, Name,Email,WCT_Function__c,WCT_Home_Country__c,WCT_Region__c,WCT_Hiring_Location__c from Contact where Email=:Ocase.FCPA_PPD_Email_Address__c LIMIT 1];
        system.debug('PPDcontact :::::::::'+PPDcontact);
        }
        Ocase.FCPA_PPD_FSS__c =PPDcontact.WCT_Function__c;
        Ocase.FCPA_PPD_Name__c=PPDcontact.Name;
        //Ocase.FCPA_PPD_Country__c=PPDcontact.WCT_Home_Country__c;
        //Ocase.FCPA_PPD_Region__c=PPDcontact.WCT_Region__c;
        //Ocase.FCPA_PPD_Location__c=PPDcontact.WCT_Hiring_Location__c;    
        
        
      }
      
      // save button Method and conditions in form
      public PageReference saveRequest()
      {
       
       pageError =false;
         if(Ocase.FCPA_PPD_First_Name__c=='' || Ocase.FCPA_PPD_First_Name__c== null)
          {
          pageError =true;
          pageErrorMessage ='PPD first name is mandatory';  
          return null;  
          }
          if(Ocase.FCPA_PPD_Last_Name__c=='' || Ocase.FCPA_PPD_Last_Name__c== null)
          {
          pageError =true;
          pageErrorMessage ='PPD last name is mandatory';  
          return null;  
          }
          if(Ocase.FCPA_PPD_Email_Address__c=='' || Ocase.FCPA_PPD_Email_Address__c== null)
          {
          pageError =true;
          pageErrorMessage ='PPD Email Address is mandatory';  
          return null;  
          }
         if(Ocase.FCPA_Candidate_First_Name__c=='' || Ocase.FCPA_Candidate_First_Name__c== null)
          {
          pageError =true;
          pageErrorMessage ='Candidate first name is mandatory';  
          return null;  
          }
          if(Ocase.FCPA_Candidate_Last_Name__c=='' || Ocase.FCPA_Candidate_Last_Name__c== null)
          {
          pageError =true;
          pageErrorMessage ='Candidate last name is mandatory';  
          return null;  
          }
          if(Ocase.FCPA_Candidate_Email_Address__c=='' || Ocase.FCPA_Candidate_Email_Address__c== null)
          {
          pageError =true;
          pageErrorMessage ='Candidate Email is mandatory';  
          return null;  
          }
          
          if(Ocase.FCPA_Type_of_Hire__c=='' || Ocase.FCPA_Type_of_Hire__c== null)
          {
          pageError =true;
          pageErrorMessage ='Type of hire is mandatory';  
          return null;  
          }
          if(Ocase.FCPA_Which_country_are_you_referring_to__c=='' || Ocase.FCPA_Which_country_are_you_referring_to__c== null)
          {
          pageError =true;
          pageErrorMessage ='Which country are you referring to? is mandatory';  
          return null;  
          } 
          if(Ocase.FCPA_Which_FSS_are_you_recommending__c=='' || Ocase.FCPA_Which_FSS_are_you_recommending__c== null)
          {
          pageError =true;
          pageErrorMessage ='Which FSS do you recommend? is mandatory';  
          return null;  
          } 
          if(Ocase.FCPA_Which_FSS_are_you_recommending__c=='Other'){
              if(Ocase.FCPA_Other__c=='' || Ocase.FCPA_Other__c== null)
              {
              pageError =true;
              pageErrorMessage ='Other is mandatory';  
              return null;  
              }
          } 
          if(Ocase.FCPA_Which_FSS_are_you_recommending__c=='Unknown'){
              if(Ocase.FCPA_Unknown__c=='' || Ocase.FCPA_Unknown__c== null)
              {
              pageError =true;
              pageErrorMessage ='unknown is mandatory';  
              return null;  
              }
          } 
          
          if(Ocase.FCPA_Referring_PPD_function__c=='' || Ocase.FCPA_Referring_PPD_function__c== null)
          {
          pageError =true;
          pageErrorMessage ='Referring PPD function is mandatory';  
          return null;  
          }
          
          if(Ocase.FCPA_Referring_PPD_function__c=='Other'){
              if(Ocase.FCPA_Other_PPD_function__c=='' || Ocase.FCPA_Other_PPD_function__c== null)
              {
              pageError =true;
              pageErrorMessage ='Other PPD function is mandatory';  
              return null;  
              }
          } 
          
          if(chkBx == True && input == ''){
              pageError =true;
              pageErrorMessage ='Please Provide Description';  
              return null;
          }
          if(resume == False && EmailCorrespondence == False && chkBx == False){
              pageError =true;
              pageErrorMessage ='Please Provide Resume/EmailCorrespondence/Other'; 
              return null;
          }
          if((resume == True || EmailCorrespondence == True) && (attachmentHelper.UploadedDocumentList.size() == 0)){
              pageError =true;
              pageErrorMessage ='Attachment is required to submit the form.';  
              return null;
          }     
           
             
          try{ 
            
            Ocase.Status    = 'New';
            Ocase.Subject = 'PPD Case';
            Ocase.WCT_Category__c=  'PPD-Referral';  
            Ocase.Origin = 'Web';
            Ocase.Description = 'PPD Case';  
            
            Ocase.Contactid   = loggedInContact.id;           
            Ocase.RecordTypeid   = OcaseRecordType[0].Id;
            Ocase.FCPA_Recruiter_Name__c=loggedInContact.Name;
            Ocase.FCPA_Recruiter_First_Name__c=loggedInContact.FirstName;
            Ocase.FCPA_Recruiter_Last_Name__c=loggedInContact.LastName;
            Ocase.FCPA_Recruiter_Email_Address__c=loggedInContact.Email;
        
            Ocase.FCPA_Form_Submitted_By__c='Recruiter';
            //ocase.FCPA_Document_Type__c=optionValue;
            if(Resume == True){
            ocase.FCPA_Document_Type__c = 'Resume';
            }
            if(Emailcorrespondence == True){
            ocase.FCPA_Document_Type__c = 'Email correspondence';
            }
            if(Resume == True && Emailcorrespondence == True){
            ocase.FCPA_Document_Type__c = 'Resume'+', '+'Email correspondence';
            }
            if(chkBx == True){
            ocase.FCPA_Document_Type__c = input;
            }
            // getting information from list of names            
            List<WCT_List_Of_Names__c> listofnames = new List<WCT_List_Of_Names__c>();
            listofnames  = [SELECT ID,Name,WCT_Type__c,FCPA_FSS__c,FCPA_Type_of_Hire__c,FCPA_Referral_SPOC_Email__c,FCPA_Referral_SPOC_Name__c FROM WCT_List_Of_Names__c WHERE FCPA_Type_of_Hire__c=:ocase.FCPA_Type_of_Hire__c AND FCPA_FSS__c=:ocase.FCPA_Which_FSS_are_you_recommending__c];
            if(!listofnames.isEmpty()){
            Ocase.FCPA_Referral_SPOC_Email__c= listofnames[0].FCPA_Referral_SPOC_Email__c ;
            Ocase.FCPA_Referral_SPOC_Name__c= listofnames[0].FCPA_Referral_SPOC_Name__c;
            }
           
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
             
            dmlOpts.EmailHeader.TriggerUserEmail = false;
            //dmlOpts.assignmentRuleHeader.useDefaultRule = False;
            Ocase.setOptions(dmlOpts); 
           Database.SaveResult savlist= Database.insert(Ocase, dmlOpts);  
           
            system.debug('Ocase*****************'+savlist);
            // Iterate through each returned result
                    if (savlist.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        System.debug('Successfully inserted case. case ID: ' + savlist.getId());
                    }
                    else {
                        // Operation failed, so get all errors                
                        for(Database.Error err : savlist.getErrors()) {
                            System.debug('The following error has occurred.');                    
                            System.debug(err.getStatusCode() + ': ' + err.getMessage());
                            System.debug('Account fields that affected this error: ' + err.getFields());
                        }
                    }
                    
            // Database.insert(Ocase);
            
            // Attachment related
             attachmentHelper.uploadRelatedAttachment(Ocase.Id);
            
             // Thankyou page
             //PageReference reference=new PageReference('/apex/Recruiter_Thankyou_Page?id=' + ocase.id+'&em='+CryptoHelper.encrypt(LoggedInContact.Email));
             PageReference reference=new PageReference('/apex/GBL_Page_Notification?id='+ ocase.id+'&key=RecruiterThankyou');
             reference.setRedirect(false);
             return reference;
             
          }catch(System.DmlException e){
            System.debug('error: ' + e.getMessage());
           }

             return null;  
      } 
        
}