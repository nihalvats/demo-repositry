/************************************************************
Class : WCT_EmployeeAttendanceTest
Description : Test Class for WCT_EmployeeAttendance
*************************************************************/

@isTest

private class WCT_EmployeeAttendanceTest
{
    static testMethod void testCase1() 
    {    
        // Contact Record creation
        Contact contactRec = new Contact();
        contactRec.LastName = 'Test';
        insert contactRec;
        
        // Event Record creation
        Event eventRec = new Event();
        eventRec.Subject = 'Call';
        eventRec.EndDateTime = System.now()+1;
        eventRec.StartDateTime = System.now();
        insert eventRec;
        
        //EventRelation Record creation
        EventRelation eveRel = new EventRelation();
        eveRel.Status = 'New';
        eveRel.Response = 'Accepted';
        eveRel.EventId = eventRec.id;
        eveRel.RelationId = contactRec.id;
        eveRel.IsInvitee = true;
        insert everel;
        
        Test.StartTest();
        ApexPages.StandardController stdEve = new ApexPages.StandardController(eventRec);
        WCT_EmployeeAttendance empAttendance = new  WCT_EmployeeAttendance(stdEve);
        empAttendance.markAttendance();
        Test.StopTest();
    }
}