/**************************************************************************************
Apex class       : ELE_StakeholderDashboard_crt 
Version          : 1.0 
Created Date     : 27 June 2015
Function         : Controller to display data in Stakeholder Dashboard
Test Class       : ELE_StakeholderDashboard_Test

 
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                  6/27/2015            Original Version
*************************************************************************************/
public with sharing class ELE_StakeholderDashboard_crt  extends SitesTodHeaderController {



public list<Clearance_separation__c> List_ClearanceToUpdate { get; set; }

public String sUserEmail{get;set;}
public String sStakeholderType{get;set;}
public list<contact> cConUser{get;set;}
public list<string> listAccessLevel{get;set;}
public String sPageURL{get;set;}
public String sPageURL2{get;set;}
public string sStakeholderDesignation {get;set;}
//public String sEncryptedEmail {get;set;}
//public String sEnEmail {get;set;}
public boolean bIsvalidUser{get;set;}
public String strJobName{get;set;}
//Search closed ELE cases    
public String CloseSrh{get;set;} 
public string closeView{get;set;} 
public list<Clearance_separation__c> tmpList_ClearanceToUpdate { get; set; }


/** 
Method Name  : ELE_StakeholderDashboard_crt
Return Type  : list<cAsset>
Description  : returns the list of assets to display on the page
*/
public ELE_StakeholderDashboard_crt()
{
closeView='';
//sEncryptedEmail = ApexPages.currentPage().getParameters().get('em');
//sUserEmail= cryptoHelper.decrypt(sEncryptedEmail);
sUserEmail=loggedInContact.Email;
//sEnEmail = cryptoHelper.encrypt(sUserEmail);
sStakeholderType= ApexPages.currentPage().getParameters().get('Param1');      
List_ClearanceToUpdate= new list<Clearance_separation__c>();
cConUser= new list<contact>();
if(sStakeholderType=='p')
{
if(sUserEmail.length()!=0){

String filter= '%'+sUserEmail+'%';
List_ClearanceToUpdate=[select id,ELE_Status__c,ELE_Employee_name__c, ELE_EMP_Personal_no__c,ELE_Employee_Comments__c, ELE_USI_Team_Comments__c, ELE_Contact_Email__c,ELE_Counselor_Name__c ,ELE_Counselor_Email_ID__c,createdDate ,
ELE_Stakeholder_Designation__c,ELE_Last_Working_Day__c, ELE_Location_Separation__c,ELE_Location__c,ELE_Service_Line__c,ELE_Service_Area__c  from Clearance_separation__c where ELE_Clearance_Authority_Type__c ='Primary Stakeholder' and (ELE_Status__c  <> 'Closed' and ELE_Status__c <> 'Revoke Resignation Approved')  And (ELE_Primary_Stakeholder_Email_id__c=:sUserEmail OR ELE_Additional_Fields__c LIKE :filter) and ELE_Separation__r.ELE_Contact__r.Email <> :sUserEmail ];

bIsvalidUser=true;

}else
{
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You are not a authorized user. Please contact your administrator for access issue.'));
      bIsvalidUser=false;
       
}
}else if(sStakeholderType=='s')
{ 
cConUser=[select id, ELE_Access_Level__c from contact where Email=:sUserEmail and ELE_Contact__c=:true and RecordType.Name = 'Employee'];

//system.debug('***size'+cConUser.size());
    
 if((cConUser.size()>0 )&& (cConUser[0].ELE_Access_Level__c!=null))
 {
  
   listAccessLevel = cConUser[0].ELE_Access_Level__c.split(';'); 
   List_ClearanceToUpdate=[select id,ELE_Status__c,ELE_Employee_name__c,ELE_EMP_Personal_no__c,ELE_Employee_Comments__c, ELE_USI_Team_Comments__c,ELE_Contact_Email__c,ELE_Counselor_Name__c, ELE_Counselor_Email_ID__c,createdDate 
   ,ELE_Stakeholder_Designation__c, ELE_Location_Separation__c,ELE_Last_Working_Day__c,ELE_Location__c,ELE_Service_Line__c,ELE_Service_Area__c
   ,ELE_Clearance_Authority_Type__c from Clearance_separation__c where  ELE_Stakeholder_Designation__c IN: listAccessLevel AND ELE_Clearance_Authority_Type__c ='Secondary Stakeholders' and (ELE_Status__c  <> 'Closed' and ELE_Status__c <> 'Revoke Resignation Approved') and ELE_Separation__r.ELE_Contact__r.Email <> :sUserEmail order by ELE_Last_Working_Day__c asc];
bIsvalidUser= true;
     
     system.debug('****'+List_ClearanceToUpdate);
     
      }
 else{
   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You are not a authorized user. Please contact your administrator for access issue.'));
   bIsvalidUser=false;
 }
 
}
tmpList_ClearanceToUpdate=List_ClearanceToUpdate;
//system.debug('TestC:'+tmpList_ClearanceToUpdate);
sPageURL='/ELE_employeeDataUpdatePage?tm=';
sPageURL2='&type='+sStakeholderType+'&id=';
}


public pagereference searchcls()
{

string tmp=CloseSrh+1;

if(tmp !='1')
{

closeView='Close';   
List_ClearanceToUpdate.clear();
if(sStakeholderType=='p')
{

if(sUserEmail.length()!=0){

String filter= '%'+sUserEmail+'%';
List_ClearanceToUpdate=[select id,ELE_Status__c,ELE_Employee_name__c, ELE_EMP_Personal_no__c,ELE_Employee_Comments__c, ELE_USI_Team_Comments__c,ELE_Contact_Email__c,ELE_Counselor_Name__c  ,ELE_Counselor_Email_ID__c,createdDate ,
ELE_Stakeholder_Designation__c,ELE_Last_Working_Day__c, ELE_Location_Separation__c,ELE_Location__c,ELE_Service_Line__c,ELE_Service_Area__c from Clearance_separation__c where ELE_Clearance_Authority_Type__c ='Primary Stakeholder' and (ELE_Status__c  = 'Closed' or ELE_Status__c = 'Revoke Resignation Approved')  And (ELE_Primary_Stakeholder_Email_id__c=:sUserEmail OR ELE_Additional_Fields__c LIKE :filter) and ELE_Separation__r.ELE_Contact__r.Email <> :sUserEmail and ELE_EMP_Personal_no__c=:CloseSrh];

bIsvalidUser=true;

}else
{
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You are not a authorized user. Please contact your administrator for access issue.'));
      bIsvalidUser=false;
       
}
}else if(sStakeholderType=='s')
{ 
 cConUser=[select id, ELE_Access_Level__c from contact where Email=:sUserEmail and ELE_Contact__c=:true and RecordType.Name = 'Employee'];
 if((cConUser.size()>0 )&& (cConUser[0].ELE_Access_Level__c!=null))
 {
  
   listAccessLevel = cConUser[0].ELE_Access_Level__c.split(';'); 
   List_ClearanceToUpdate=[select id,ELE_Status__c,ELE_Employee_name__c,ELE_EMP_Personal_no__c,ELE_Employee_Comments__c, ELE_USI_Team_Comments__c,ELE_Contact_Email__c,ELE_Counselor_Name__c ,ELE_Counselor_Email_ID__c ,createdDate   
   ,ELE_Stakeholder_Designation__c, ELE_Location_Separation__c,ELE_Last_Working_Day__c,ELE_Location__c
   ,ELE_Clearance_Authority_Type__c,ELE_Service_Line__c,ELE_Service_Area__c from Clearance_separation__c where  ELE_Stakeholder_Designation__c IN: listAccessLevel AND ELE_Clearance_Authority_Type__c ='Secondary Stakeholders' and (ELE_Status__c  = 'Closed' or ELE_Status__c = 'Revoke Resignation Approved') and ELE_Separation__r.ELE_Contact__r.Email <> :sUserEmail and ELE_EMP_Personal_no__c=:CloseSrh  ];
bIsvalidUser= true;
      }
 else{
   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You are not a authorized user. Please contact your administrator for access issue.'));
   bIsvalidUser=false;
 }

}
  return null;
}
  else
  {
  List_ClearanceToUpdate=tmpList_ClearanceToUpdate;
  PageReference pg=new PageReference('/ELE/ELE_stakeholderdashboard?Param1='+sStakeholderType); 
  pg.setRedirect(false);    
  return pg;    
  }
}
public pagereference backbtn()
{
  List_ClearanceToUpdate=tmpList_ClearanceToUpdate;
   
  PageReference pg=new PageReference('/apex/ELE_stakeholderdashboard?Param1='+sStakeholderType); 
  pg.setRedirect(true);      
  return pg; 
}
}