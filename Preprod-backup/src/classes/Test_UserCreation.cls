@isTest
public class Test_UserCreation {
//test method for user creation class
static testMethod void myTest(){ 
//creating test users
List<User> usr=new list<user>();
 Profile p = [SELECT Id FROM Profile WHERE Name='Standard Platform User']; 
        User ur1 = new User(Alias = 'sample1', Email='testct1@deloitte.com', 
            EmailEncodingKey='UTF-8', LastName='testct1', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,CommunityNickname='testct1',IsActive=false,
            FederationIdentifier='testct1', 
            TimeZoneSidKey='America/Los_Angeles', UserName='testct1@deloitte.com.wct.prd');
        usr.add(ur1);
        User ur2 = new User(Alias = 'sample2', Email='testct2@deloitte.com', 
            EmailEncodingKey='UTF-8', LastName='testct2', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,CommunityNickname='testct2',IsActive=false,
            FederationIdentifier='testct2', 
            TimeZoneSidKey='America/Los_Angeles', UserName='testct2@deloitte.com.wct.prd');
        usr.add(ur2);
        User ur3 = new User(Alias = 'sample3', Email='testct3@deloitte.com', 
            EmailEncodingKey='UTF-8', LastName='testct3', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,CommunityNickname='testct3',IsActive=true,
            FederationIdentifier='testct3', 
            TimeZoneSidKey='America/Los_Angeles', UserName='testct3@deloitte.com.wct.prd');
          usr.add(ur3);  
     User ur4 = new User(Alias = 'sample5', Email='testct5@deloitte.com', 
            EmailEncodingKey='UTF-8', LastName='testct5', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,CommunityNickname='testct5.ex',IsActive=false,
            FederationIdentifier='testct5.ex', 
            TimeZoneSidKey='America/Los_Angeles', UserName='testct5@deloitte.com.wct.prd.ex');
           usr.add(ur4); 
     Profile pd = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    User ur6 = new User(Alias = 'sample6', Email='testct6@deloitte.com', 
            EmailEncodingKey='UTF-8', LastName='testct6', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = pd.Id,CommunityNickname='testct6',IsActive=true,
            FederationIdentifier='testct6', 
            TimeZoneSidKey='America/Los_Angeles', UserName='testct6@deloitte.com.wct.prd');
          usr.add(ur6); 
          insert usr;
    //inserting sample test user
         UserRole r=[select id from UserRole where DeveloperName='WCT_Admin'];
        
            User us = new User(Alias = 'sample12', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = pd.Id,UserRoleId=r.id,IsActive=true,
            TimeZoneSidKey='America/Los_Angeles', UserName='sample12@testorg.com');
            insert us; 
    
    
    system.runas(us){
      PermissionSet pr = new PermissionSet(Label='Case Permisson Set',Name='Case_Permisson_Set');
      insert pr;
    
 string contid=Schema.Sobjecttype.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
  string query='SELECT Id,Email,Name,Firstname,lastname,App_Assignment__c,WCT_Region__c,WCT_Person_Id__c,WCT_Personnel_Number__c,WCT_Personnel_Number1__c,User_Management_category__c,WCT_Employee_Status__c  from Contact where WCT_Contact_Type__c=\'Employee\' and RecordTypeId= :contid and WCT_Type__c=\'Employee\' and WCT_Employee_Status__c=\'Active\' and User_Management_category__c=true ';
      system.debug('hello'+Database.Query(query));   
List<Contact>cont=new list<Contact>();
 Contact c=new Contact();
 c.firstname='test';
 c.lastname='testct1';
 c.email='testct1@deloitte.com';
 c.RecordTypeId=contid;
 c.WCT_Employee_Status__c='Active';
 c.WCT_Employee_Group__c='Active';
 c.WCT_Person_Id__c='9999999';
 c.WCT_Personnel_Number__c=9999999;
 c.WCT_Region__c='US-USI';
 cont.add(c);
 Contact ct=new Contact();
 ct.firstname='test2';
 ct.lastname='testct2';
 ct.email='testct2@deloitte.com';
 ct.RecordTypeId=contid;
 ct.WCT_Employee_Status__c='Active';
 ct.WCT_Employee_Group__c='Active';
 ct.WCT_Person_Id__c='9999998';
 ct.WCT_Personnel_Number__c=1199998;
 ct.WCT_Region__c='WEST';
 cont.add(ct);
 Contact ct1=new Contact();
 ct1.firstname='test3';
 ct1.lastname='testct3';
 ct1.email='testct3@deloitte.com';
 ct1.RecordTypeId=contid;
 ct1.WCT_Employee_Status__c='Active';
 ct1.WCT_Employee_Group__c='Active';
 ct1.WCT_Person_Id__c='9999997';
 ct1.WCT_Personnel_Number__c=9999997;
 
 cont.add(ct1);
  Contact ct2=new Contact();
 ct2.firstname='test123';
 ct2.lastname='testct4';
 ct2.email='testct4@deloitte.com';
 ct2.RecordTypeId=contid;
 ct2.WCT_Employee_Status__c='Active';
 ct2.WCT_Employee_Group__c='Active';
 ct2.WCT_Person_Id__c='9999996';
 ct2.WCT_Personnel_Number__c=9999996;
 
 cont.add(ct2);
 
  Contact ct3=new Contact();
 ct3.firstname='test5';
 ct3.lastname='testct5';
 ct3.email='testct5@deloitte.com';
 ct3.RecordTypeId=contid;
 ct3.WCT_Employee_Status__c='Active';
 ct3.WCT_Employee_Group__c='Active';
 ct3.WCT_Person_Id__c='9999918';
 ct3.WCT_Personnel_Number__c=9999918;
 ct3.App_Assignment__c='TRT';
 cont.add(ct3);
        
   Contact ct6=new Contact();
 ct6.firstname='test6';
 ct6.lastname='testct6';
 ct6.email='testct6@deloitte.com';
 ct6.RecordTypeId=contid;
 ct6.WCT_Employee_Status__c='Active';
 ct6.WCT_Employee_Group__c='Active';
 ct6.App_Assignment__c='TRT';
 cont.add(ct6);
  
  

 User_Permission_sets__c ps=new User_Permission_sets__c();
 ps.name='TRT';
 ps.PS_For_Salesforce_User__c=pr.id;
 ps.PS_For_SalesforcePlatform_User__c=pr.id;
  insert ps;  
 insert cont;
 string rec=cont[0].id;
 if(rec !=null){
  Test.startTest();
        User_Creation u = new User_Creation(query);
        Database.executeBatch(u);
        Database.executeBatch(new Contact_Update_Batch(query));
      Test.stopTest();
      
 }
    }
}
     static testMethod void myTest1(){ 
      Test.StartTest();
        
     User_Creation_Scheduler sc1 = new User_Creation_Scheduler();
     String sch = '0 15 15 * * ?';
    string jobID= system.schedule('Schedule Report class',sch,sc1);
     CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime
         FROM CronTrigger WHERE id = :jobId];
          System.assertEquals(sch,ct.CronExpression);
          System.assertEquals(0, ct.TimesTriggered);
        
          

     Test.stopTest();   
     }
    static testMethod void myTest2(){ 
        List<Exception_Log__c> userlog=new List<Exception_Log__c>();
       
        string contid = Schema.SObjectType.Exception_Log__c.getRecordTypeInfosByName().get('User Management').getRecordTypeId();
         Exception_Log__c e=new Exception_Log__c();
        e.Exception_Date_and_Time__c=system.now();
        e.Action__c='NewUser Creation';
        e.User_Description__c='Creating a new user';
        e.Running_User__c = UserInfo.getUserId();
        e.RecordTypeId=contid;
        userlog.add(e);
         Exception_Log__c e1=new Exception_Log__c();
        e1.Exception_Date_and_Time__c=system.now();
        e1.Action__c='NewUser Creation';
        e1.User_Description__c='Creating a new user';
        e1.Running_User__c = UserInfo.getUserId();
        e1.RecordTypeId=contid;
        userlog.add(e1);
        Exception_Log__c e2=new Exception_Log__c();
        e2.pageName__c='112xshugxqgqshbqshcgiisgcuwgeigdsiuqgacddddddgggguigiguuggkjhhuyut7wrstffdygdqsygdqgdqygyuqgdyu';
         userlog.add(e2);
      Test.StartTest();
        
     User_Login_Batch sc1 = new User_Login_Batch(userlog);
      Database.executeBatch(sc1);
          

     Test.stopTest();   
     }
}