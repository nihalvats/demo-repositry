public class MIS_Add_SOP {
    
    public string cfsid;
    Public Attachment myfile;
    public MIS_Add_SOP(ApexPages.StandardController controller){
        cfsid = System.currentPagereference().getParameters().get('id');
    }
    
    Public Attachment getmyfile()
    {
        myfile = new Attachment();
        return myfile;
    }
    
    Public Pagereference Savedoc()
    {
        
        Attachment a = new Attachment();
        list<Case_form_Extn__c> c = new list<Case_form_Extn__c>();
        system.debug('cfsid'+cfsid); 
        if(cfsid != '')
        {
            c=[select id,GEN_Case__c,MIS_SOP_Uploaded__c from Case_form_Extn__c where id=:cfsid limit 1];
            system.debug(c);
        }
        if(!c.isEmpty() && c != null)
        {
            if(myfile != null && myfile.body != null )
            {
                a.parentId = c[0].id; 
                a.name=myfile.name; 
                a.body = myfile.body;
                a.Description='SOP';
            }
        }
        /* insert the attachment */
        try{
            insert a;
            c[0].MIS_SOP_Uploaded__c=true;
            c[0].MIS_SOP_Document__c= 'https://talent--cvm--c.cs25.content.force.com/servlet/servlet.FileDownload?file='+a.id;
            update c[0];
            PageReference pr = new PageReference('/'+cfsid); 
            pr.setRedirect(true);  
            return pr;
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attachment is not added to case'+ e.getMessage()));
            system.debug('-------exception---------'+e.getMessage()+'----at---Line #----'+e.getLineNumber());
        }
        
        return null;
    } 
    
    Public Pagereference Cancel()
    {
        String accid = System.currentPagereference().getParameters().get('id');
        PageReference pr = new PageReference('/'+accid); 
        pr.setRedirect(true);  
        return pr;
    }
    
}