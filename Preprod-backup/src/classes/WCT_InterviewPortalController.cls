public with sharing class WCT_InterviewPortalController {
    public list<WCT_Interview_Junction__c> todayLst {get;set;}
    public list<WCT_Interview_Junction__c> upComingLst {get;set;}
    public list<WCT_Interview_Junction__c> pastLst {get;set;}
    public list<WCT_Interview_Junction__c> allLst {get;set;}
    private list<WCT_Interview_Junction__c> allDataLst {get;set;}
    //start New Variables:R2.5
    public list<WCT_Interview_Junction__c> actionItemLst {get;set;}
    public list<WCT_Interview_Junction__c> upComLst {get;set;}
    public Integer acitemSize {get;set;}
    public Integer upcomSize {get;set;}
    public static final String INTERVIWER_Portal_ActionItems = 'ActionItems';
    public static final String INTERVIWER_Portal_UpComItems = 'UpComItems';
    //End New Variables:R2.5
    
    public Integer tdSize {get;set;}
    public Integer upSize {get;set;}
    public Integer ptSize {get;set;}
    public Integer allSize {get;set;}
    public boolean addSection {get; set;}

    public string profileName {get;set;}
    /*
    @Constructor
    */
    public WCT_InterviewPortalController() {
    todayLst    = new list<WCT_Interview_Junction__c>();
    upComingLst = new list<WCT_Interview_Junction__c>();
    pastLst     = new list<WCT_Interview_Junction__c>();
    allLst      = new list<WCT_Interview_Junction__c>();
    //start R2.5
    actionItemLst = new list<WCT_Interview_Junction__c>();
    upComLst =  new list<WCT_Interview_Junction__c>();
    //End R2.5
    addSection = false;
    
    /* Querying Profile Name For HomePage Component to redirect to VF - Communities Licenses have no access to Query Profile thought JS. :( )*/
    User UserInf =[Select profile.Name from User where id=:UserInfo.getUserId()];
    profileName = UserInf.profile.Name;
    
        //Hiring level,WCT_IEF_Submission_Status__c,WCT_Contact__r.Name, WCT_Contact__r.WCT_Taleo_Id__c,  is removed from Inteview. So removing it from Query
        allDataLst = [SELECT Id, Name, WCT_Candidate_Name__c, WCT_Hiring_Level__c, WCT_Start_Date_Time__c, WCT_End_Date_Time__c, WCT_Interview_Type__c, WCT_Interview_Medium__c, 
                        WCT_Requisition_RC_Email__c, WCT_IEF_Submission_Status__c, WCT_Candidate_Tracker__r.WCT_Contact__r.Id, WCT_Interview__c, WCT_Interview__r.Name,
                        WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Name, WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiting_Coordinator__r.Email,
                        WCT_Interview__r.WCT_Interview_Location_Details__c, WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Name, 
                        WCT_Candidate_Tracker__r.WCT_Requisition__r.WCT_Recruiter__r.Email ,WCT_IEF_URL_Base__c  
                        FROM WCT_Interview_Junction__c 
                    where WCT_Start_Date_Time__c !=null 
                    and WCT_Interviewer__c = :UserInfo.getUserId()
                    and WCT_Interview_Status__c IN ('Complete','Invite Sent')
                    order by WCT_Start_Date_Time__c desc
                    limit:Limits.getLimitQueryRows()];
        tdSize = rtnsize(allDataLst, WCT_UtilConstants.INTERVIWER_Portal_Today);
        upSize = rtnsize(allDataLst, WCT_UtilConstants.INTERVIWER_Portal_UpComing);
        ptSize = rtnsize(allDataLst, WCT_UtilConstants.INTERVIWER_Portal_Past);
        allSize = rtnsize(allDataLst, WCT_UtilConstants.INTERVIWER_Portal_All);
        //start R2.5
        acitemSize = rtnsize(allDataLst, INTERVIWER_Portal_ActionItems);
        upcomSize = rtnsize(allDataLst, INTERVIWER_Portal_UpComItems);
        actionItemLst();
        //End R2.5
       // todayLst();//Default landing list...

    }
    public void setAddSection(){
        if(addSection){
            addSection = false;
        }else{
            addSection= true;}
    }
    public void cancelAddSection(){
        //attachment = new attachment();
        setAddSection();

    }       
    private void clearListData(){
        todayLst.clear();
        upComingLst.clear();
        pastLst.clear();
        allLst.clear();
        //start R2.5
        upComLst.clear();
        actionItemLst.clear();
        //End R2.5
    }
    public void todayLst(){
        clearListData();
        todayLst = rtnLst(allDataLst,WCT_UtilConstants.INTERVIWER_Portal_Today);
    }
    public void upComingLst(){
        clearListData();
        upComingLst = rtnLst(allDataLst,WCT_UtilConstants.INTERVIWER_Portal_UpComing);
    }
    public void pastLst(){
        clearListData();
        pastLst = rtnLst(allDataLst,WCT_UtilConstants.INTERVIWER_Portal_Past);
    }
    public void allLst(){
        clearListData();
        allLst = rtnLst(allDataLst,WCT_UtilConstants.INTERVIWER_Portal_All);
    } 

    //start R2.5
    public void actionItemLst(){
        clearListData();
        actionItemLst = rtnLst(allDataLst,INTERVIWER_Portal_ActionItems);
    } 
    public void upComLst(){
        clearListData();
        upComLst = rtnLst(allDataLst,INTERVIWER_Portal_UpComItems);
    } 
    
    //End R2.5
    public list<WCT_Interview_Junction__c> rtnLst(list<WCT_Interview_Junction__c> AllDt, String crt){
        list<WCT_Interview_Junction__c> lstDt = new list<WCT_Interview_Junction__c>();
        for(WCT_Interview_Junction__c ads : AllDt){

                Integer stDate,stMonth,stYear,sysStDate,sysStMonth,sysStYear;
                stDate = Integer.valueOf(ads.WCT_Start_Date_Time__c.format('DD'));
                stMonth = Integer.valueOf(ads.WCT_Start_Date_Time__c.format('MM'));
                stYear = Integer.valueOf(ads.WCT_Start_Date_Time__c.format('YYYY'));
                sysStDate = Integer.valueOf(system.now().format('DD'));
                sysStMonth = Integer.valueOf(system.now().format('MM'));
                sysStYear = Integer.valueOf(system.now().format('YYYY'));
                //start R2.5
                string status = ads.WCT_IEF_Submission_Status__c;
            

            
            if(crt==INTERVIWER_Portal_ActionItems){
                if( status!='Submitted' && status!='Canceled' && system.now()>ads.WCT_Start_Date_Time__c){
                    lstDt.add(ads);
                }
            }
            
            if(crt==INTERVIWER_Portal_UpComItems){
                if( system.now()<ads.WCT_Start_Date_Time__c){
                    lstDt.add(ads);
                }
            }
            //End R2.5
            
            if(crt==WCT_UtilConstants.INTERVIWER_Portal_Today){
                if(stDate==sysStDate && stMonth==sysStMonth && stYear==sysStYear){
                    lstDt.add(ads);
                }
            }
            if(crt==WCT_UtilConstants.INTERVIWER_Portal_UpComing){
                if(stDate > sysStDate && stMonth >= sysStMonth && stYear >= sysStYear){
                    lstDt.add(ads);
                }
            }
            if(crt==WCT_UtilConstants.INTERVIWER_Portal_Past){
                if(stDate < sysStDate && stMonth <= sysStMonth && stYear <= sysStYear){
                    lstDt.add(ads);
                }
            }
        }
        if(crt==WCT_UtilConstants.INTERVIWER_Portal_All){
            lstDt.addAll(AllDt);
        }
        
        return lstDt;
    }
    public Integer rtnsize(list<WCT_Interview_Junction__c> AllDt, String crt){
        Integer lstSize = 0;
        for(WCT_Interview_Junction__c ads : AllDt){
                Integer stDate,stMonth,stYear,sysStDate,sysStMonth,sysStYear;
                stDate = Integer.valueOf(ads.WCT_Start_Date_Time__c.format('DD'));
                stMonth = Integer.valueOf(ads.WCT_Start_Date_Time__c.format('MM'));
                stYear = Integer.valueOf(ads.WCT_Start_Date_Time__c.format('YYYY'));
                sysStDate = Integer.valueOf(system.now().format('DD'));
                sysStMonth = Integer.valueOf(system.now().format('MM'));
                sysStYear = Integer.valueOf(system.now().format('YYYY'));
               //start R2.5
               string status = ads.WCT_IEF_Submission_Status__c;
            
           
            if(crt==INTERVIWER_Portal_ActionItems){
                if( status!='Submitted' && status!='Canceled' && system.now()>ads.WCT_Start_Date_Time__c){
                    lstSize+=1;
                }
            }
            
            if(crt==INTERVIWER_Portal_UpComItems){
                if( system.now()<ads.WCT_Start_Date_Time__c){
                    lstSize+=1;
                }
            }
            //End R2.5
            if(crt==WCT_UtilConstants.INTERVIWER_Portal_Today){
                if(stDate==sysStDate && stMonth==sysStMonth && stYear==sysStYear){
                    lstSize+=1;
                }
            }
            if(crt==WCT_UtilConstants.INTERVIWER_Portal_UpComing){
                if(stDate > sysStDate && stMonth >= sysStMonth && stYear >= sysStYear){
                    lstSize+=1;
                }
            }
            if(crt==WCT_UtilConstants.INTERVIWER_Portal_Past){
                if(stDate < sysStDate && stMonth <= sysStMonth && stYear <= sysStYear){
                    lstSize+=1;
                }
            }
            if(crt==WCT_UtilConstants.INTERVIWER_Portal_All){
                lstSize+=1;
            }
        
        }
        return lstSize;
    }
}