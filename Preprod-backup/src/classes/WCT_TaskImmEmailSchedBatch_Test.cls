@isTest
public class WCT_TaskImmEmailSchedBatch_Test 
{
    static testMethod void m1(){
        
        String recordTypeId= Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
        Contact con=WCT_UtilTestDataCreation.createEmployee(recordTypeId);
        insert con;
        WCT_Immigration__c imm = WCT_UtilTestDataCreation.createImmigration(con.id);
        imm.H1B_Is_USI_Upload__c=True;
        insert imm;
        Task t = WCT_UtilTestDataCreation.createTask(imm.id);
        t.Subject = 'Submit Visa logistics information';
        t.ActivityDate = system.today().addDays(-3);
        insert t;
        Test.startTest();
        WCT_TaskImmigrationEmailSchedulableBatch controller = new WCT_TaskImmigrationEmailSchedulableBatch();
        system.schedule('New','0 0 2 1 * ?',controller); 
        Test.stopTest(); 
     }       
}