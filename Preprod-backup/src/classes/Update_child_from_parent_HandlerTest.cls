@isTest

/********************************************************************************************************************************
Apex class         : Update_child_from_parent_HandlerTest
Description        : This is about ISBEFORE functionality Test class for UpdateChildStatus_ParentStatus_Handler.
Type                 : Test Class for Handler
Test Class         : Update_child_from_parent_HandlerTest

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Anji Reddy                 31/05/2016          84%                  <Req / Case #>                  Original Version
************************************************************************************************************************************/



public class Update_child_from_parent_HandlerTest {
    static testmethod void updatemethod()
    {
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User ur1 = new User(Alias = 'sample1', Email='testct1@deloitte.com', 
                            EmailEncodingKey='UTF-8', LastName='testct1', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = p.Id,CommunityNickname='testct1',IsActive=true,
                            FederationIdentifier='testct1', 
                            TimeZoneSidKey='America/Los_Angeles', UserName='testct1@deloitte.com.wct.prd');  
        insert ur1;
        // func();
        system.runas(ur1){
            Test.StartTest();
            Talent_Delivery_Contact_Database__c TDC  = new Talent_Delivery_Contact_Database__c();
            TDC.name = '009877';
            TDC.HR_Function__c   = 'Sample Test Function';
            TDC.HR_Service_Area__c = 'Sample HR Service Area';
            TDC.HR_Service_Line__c = 'Test Service Line ';
            TDC.India_Talent_Contact_1__c = 'India@deloitte.com';
            TDC.India_Talent_Contact_2__c = 'India2@deloitte.com';
            TDC.US_Talent_Contact_1__c = 'US1@deloitte.com';
            TDC.US_Talent_Contact_2__c  = 'US2@deloitte.com';
            TDC.US_Talent_Contact_3__c =  'US3@deloitte.com';
            TDC.US_Talent_Contact_4__c = 'US4@deloitte.com';
            insert TDC;
            
            Talent_Delivery_Contact_Database__c TDC1  = new Talent_Delivery_Contact_Database__c();
            TDC1.name = '009878';
            TDC1.HR_Function__c   = 'Sample Test Functions';
            TDC1.HR_Service_Area__c = 'Sample HR Service Areas';
            TDC1.HR_Service_Line__c = 'Test Service Lines ';
            TDC1.India_Talent_Contact_1__c = 'India1@deloitte.com';
            TDC1.India_Talent_Contact_2__c = 'India3@deloitte.com';
            TDC1.US_Talent_Contact_1__c = 'US2@deloitte.com';
            TDC1.US_Talent_Contact_2__c  = 'US3@deloitte.com';
            TDC1.US_Talent_Contact_3__c =  'US4deloitte.com';
            TDC1.US_Talent_Contact_4__c = 'US5deloitte.com';
            insert TDC1;
            
            UpdateChildStatus_ParentStatus_Handler handlr  = new UpdateChildStatus_ParentStatus_Handler();
            
            recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
            contact con = WCT_UtilTestDataCreation.createEmployee(rt.id);
            con.lastname ='Test Record';
            con.WCT_Cost_Center__c  = '009877';
            con.WCT_Contact_Type__c = 'Employee__c';
            insert con;
            List<wct_leave__c> leaveslist=new List<wct_leave__c>();
            wct_leave__c leave = new wct_leave__c();
            leave.WCT_Employee__c = con.id;
            leave.WCT_Alternate_Personal_Email_Id__c = 'Test_alternate@deloitte.com';
            leaveslist.add(leave);
            
            
            wct_leave__c leave1 = new wct_leave__c();
            leave.WCT_Alternate_Personal_Email_Id__c = 'Test_alternate1@deloitte.com';
            leaveslist.add(leave1);
            insert leaveslist;
            
            
            
            UpdateChildStatus_ParentStatus_Handler handlr1  = new UpdateChildStatus_ParentStatus_Handler();
            handlr1.updatemethod(leaveslist);
            
            Test.stopTest();
        }
    }
    
    
    
    
}