global class User_Login_Batch implements Database.Batchable<sObject>,Database.Stateful{
List<Exception_Log__c> excp=new List<Exception_Log__c>();//holds the exception log records
    global final List<Exception_Log__c> Query;
    //passing the query of the batch from scheduler in constructerr
    global User_Login_Batch(List<Exception_Log__c> q){
        Query=q;
    }
    global List<Exception_Log__c> start(Database.BatchableContext BC) {
        return  Query;
    }
    global void execute(Database.BatchableContext BC, List<Exception_Log__c> scope){
        
         //holds the record type id of exception object to log the failures.
        Schema.DescribeSObjectResult Cas1 = Exception_Log__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames1 = Cas1.getRecordTypeInfosByName();// getting the record Type Info
        Id exceptionid =rtMapByNames1.get('Exception').getRecordTypeId();//particular RecordId by  Name
        if(scope.size()>0){
         Database.SaveResult[] newuserList = Database.insert(scope, false);
            for (Integer i=0;i<newuserList.size();i++ ) {
                if(newuserList.get(i).isSuccess()){
                   
                }
                else {
                    for (Database.Error err : newuserList.get(i).getErrors()){
                         string exmsg=err.getMessage();
                        string msg='Error while inserting the log : '+ scope.get(i)+':'+ err.getStatusCode() + ' :' + err.getMessage();
                       excp.add(New Exception_Log__c(className__c='User Log',pageName__c='User Log',Exception_Date_and_Time__c=system.now(),Detailed_Exception__c=msg,RecordTypeId= exceptionid,Running_User__c = UserInfo.getUserId()) );    
                }   
                }
            }
        }
    }
    global void finish(Database.BatchableContext BC){
       insert excp;
    }
}