/*****************************************************************************************
    Name    : WCT_RequisitionTriggerHandler
    Desc    : This class is used by the trigger to update Owner on Requisition on change of Recuiter 
      
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
 Deloitte               20 Nov, 2013         Created 

******************************************************************************************/

public with sharing class WCT_RequisitionTriggerHandler{
    
     /*
     * Method name  : UpdateOwnerOnInsert
     * Description  : Populate Owner with Requisition Recruiter on insert
     * Return Type  : Nil
     * Parameter    : New Requisitions list
     */    
    public void UpdateOwnerOnInsert(List<WCT_Requisition__c> listNewRequisitions){
        for(WCT_Requisition__c req:listNewRequisitions ){
            //Compare Owner with Recuiter
            if(req.WCT_Recruiter__c != null && req.WCT_Recruiter__c != req.OwnerId){
                req.Ownerid = req.WCT_Recruiter__c; 
            }
        }
    }
    
     /*
     * Method name  : PopulateRecruitingCoordinatorOnInsert
     * Description  : Populate Recruiting Co-ordinator with Recruiter on insert
     * Return Type  : Nil
     * Parameter    : New Requisitions list
     */    
    public void PopulateRecruitingCoordinatorOnInsert(List<WCT_Requisition__c> listNewRequisitions){
        for(WCT_Requisition__c req:listNewRequisitions ){
            //Compare Recuiter
            if(req.WCT_Recruiter__c != null && req.WCT_Recruiting_Coordinator__c == null){
                req.WCT_Recruiting_Coordinator__c = req.WCT_Recruiter__c; 
            }
        }
    }

     /*
     * Method name  : PopulateRecruitingCoordinatorRecruiterFromEmailOnInsert
     * Description  : Populate Recruiting Co-ordinator and Recruiter from User on insert
     * Return Type  : Nil
     * Parameter    : New Requisitions list
     */    
    public void PopulateRecruitingCoordinatorRecruiterFromEmailOnInsert(List<WCT_Requisition__c> listNewRequisitions){
        set<string> setEmail = new set<string> ();

        for(WCT_Requisition__c req:listNewRequisitions ){
            if(req.WCT_Recruiter_Email_ID__c != null){
                setEmail.add(req.WCT_Recruiter_Email_ID__c);

                }
            if(req.WCT_Recruiting_Coordinator_Email_ID__c != null){
                setEmail.add(req.WCT_Recruiting_Coordinator_Email_ID__c);

                }  
                system.debug('######' + setEmail);                     
        }
       if(!listNewRequisitions.isempty()){
       updateRecuiterandRCbyEmails(setEmail,listNewRequisitions);
       }
            
    }
    public void PopulateRecruitingCoordinatorRecruiterFromEmailOnUpdate(Map<Id,WCT_Requisition__c> mapOldRequisitions, List<WCT_Requisition__c> listNewRequisitions){
    set<string> setEmail = new set<string> ();

    for(WCT_Requisition__c reqNew:listNewRequisitions ){
            //Compare Recuiter and RC email
            if(reqNew.WCT_Recruiting_Coordinator_Email_ID__c !=mapOldRequisitions.get(reqNew.id).WCT_Recruiting_Coordinator_Email_ID__c){           
               setEmail.add(reqNew.WCT_Recruiting_Coordinator_Email_ID__c); 
               
                }
            if(reqNew.WCT_Recruiter_Email_ID__c !=mapOldRequisitions.get(reqNew.id).WCT_Recruiter_Email_ID__c){           
               setEmail.add(reqNew.WCT_Recruiter_Email_ID__c); 

                }
        }
       if(!listNewRequisitions.isempty()){
       updateRecuiterandRCbyEmails(setEmail,listNewRequisitions);
       }
    }
    public void updateRecuiterandRCbyEmails(set<string> setEmail,List<WCT_Requisition__c> listNewRequisitions){
    
    List<User> lstUser = [select id , email from User where email in :setEmail];
    Map<String,id> UserMap=new Map<String,id>();
        
    for(User tmpUser:lstUser){
            UserMap.put(tmpUser.Email, tmpUser.id);
        }
        
    for(WCT_Requisition__c  tmpReq : listNewRequisitions){
                if(UserMap.containsKey(tmpReq.WCT_Recruiting_Coordinator_Email_ID__c)){
                        tmpReq.WCT_Recruiting_Coordinator__c  = UserMap.get(tmpReq.WCT_Recruiting_Coordinator_Email_ID__c);     
                        
                    }
                if(UserMap.containsKey(tmpReq.WCT_Recruiter_Email_ID__c)){
                        tmpReq.WCT_Recruiter__c = UserMap.get(tmpReq.WCT_Recruiter_Email_ID__c);        
                        
                    }
                              
        }
    }

    
     /*
     * Method name  : UpdateOwnerOnUpdate
     * Description  : Populate Owner with Requisition Recruiter on update
     * Return Type  : Nil
     * Parameter    : Old Requisitons Map, New Requisitions list
     */ 
    public void UpdateOwnerOnUpdate(Map<Id,WCT_Requisition__c> mapOldRequisitions, List<WCT_Requisition__c> listNewRequisitions){
        for(WCT_Requisition__c reqNew:listNewRequisitions ){
            //Compare new Owner with new Recuiter && Change of Recuiter
            if(reqNew.WCT_Recruiter__c != null && reqNew.Ownerid != reqNew.WCT_Recruiter__c && reqNew.WCT_Recruiter__c != mapOldRequisitions.get(reqNew.id).WCT_Recruiter__c ){           
               reqNew.Ownerid = reqNew.WCT_Recruiter__c;               
            }    
        }
    }
    
    
    public void UpdateRCEmailOnJunction(Map<Id,WCT_Requisition__c> mapOldRequisitions, List<WCT_Requisition__c> listNewRequisitions){
        map<String,String> MapRequisitionWithRCEmail=new map<String,String>();
        map<String,String> MapRequisitionWithREmail=new map<String,String>();
        for(WCT_Requisition__c reqNew:[select id,Name,WCT_Recruiting_Coordinator__c,WCT_Recruiter__c,WCT_Recruiter__r.Email,WCT_Recruiting_Coordinator__r.Email from WCT_Requisition__c where id in:listNewRequisitions] ){
            //Compare new Owner with new Recuiter && Change of Recuiter
            if(reqNew.WCT_Recruiting_Coordinator__c !=mapOldRequisitions.get(reqNew.id).WCT_Recruiting_Coordinator__c){           
               MapRequisitionWithRCEmail.put(reqNew.Name,reqNew.WCT_Recruiting_Coordinator__r.Email); 
                }
            if(reqNew.WCT_Recruiter__c !=mapOldRequisitions.get(reqNew.id).WCT_Recruiter__c){           
               MapRequisitionWithREmail.put(reqNew.Name,reqNew.WCT_Recruiter__r.Email); 
                }
            }
        System.debug('Map'+MapRequisitionWithRCEmail);
        List <WCT_Interview_Junction__c>  lstIJTobeUpdated= new List<WCT_Interview_Junction__c>();
        for(WCT_Interview_Junction__c tempJunction:[select id,WCT_Requisition_ID__c,WCT_Requisition_RC_Email__c,WCT_Requisition_RecruiterEmail__c from WCT_Interview_Junction__c Where WCT_Requisition_ID__c in:MapRequisitionWithRCEmail.keyset() OR WCT_Requisition_ID__c in:MapRequisitionWithREmail.keyset()]){
            if(MapRequisitionWithRCEmail.get(tempJunction.WCT_Requisition_ID__c)!= null)
                tempJunction.WCT_Requisition_RC_Email__c=MapRequisitionWithRCEmail.get(tempJunction.WCT_Requisition_ID__c);
            if(MapRequisitionWithREmail.get(tempJunction.WCT_Requisition_ID__c)!= null)
                tempJunction.WCT_Requisition_RecruiterEmail__c=MapRequisitionWithREmail.get(tempJunction.WCT_Requisition_ID__c);
            lstIJTobeUpdated.add(tempJunction);
        }
        System.debug('list'+lstIJTobeUpdated);
         if(!lstIJTobeUpdated.isEmpty()){
            
              Database.SaveResult[] srList=Database.update(lstIJTobeUpdated);
                // Iterate through each returned result
                for (Database.SaveResult sr : srList) {
                    if (!sr.isSuccess()) {
                        String ErrorMessage='';
                        // Operation failed, so get all errors                
                        for(Database.Error err : sr.getErrors()) {
                            ErrorMessage=ErrorMessage+err.getStatusCode() + ': ' + err.getMessage()+'';                                                
                        }
                        WCT_ExceptionUtility.logException('WCT_RequisitionTriggerHandler','UpdateRCEmailOnJunction', ErrorMessage);

                    }
               }
        }
          
        }
        
    

}