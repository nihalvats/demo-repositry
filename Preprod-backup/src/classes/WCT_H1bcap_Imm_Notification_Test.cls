@isTest
 private class WCT_H1bcap_Imm_Notification_Test{
 Static testmethod void WCT_H1bcap_Imm_Notification_TestMethod(){
 
   Date td = system.Today().addDays(-11);
  
    recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
    Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
    con.WCT_Employee_Group__c = 'Active';
    insert con;
  
    list<WCT_H1BCAP__c> lst = new list<WCT_H1BCAP__c>();
    WCT_H1BCAP__c h1 = new WCT_H1BCAP__c();
    h1.WCT_H1BCAP_Practitioner_Name__c = con.id;
    h1.WCT_H1BCAP_Email_ID__c = 'svalluru@gmail.com';
    h1.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
    h1.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'abc@yahoo.com';
    h1.WCT_H1BCAP_Status__c  = 'USI SLL Approved';
    h1.WCT_H1BCAP_Capture_BeginingDate_SLL__c = td;
    h1.recalculateFormulas(); 
    lst.add(h1);
    insert lst;      
   
  Contact con1=WCT_UtilTestDataCreation.createEmployee(rt.id);
  con1.WCT_Employee_Group__c = 'Active';
  con1.Email = 'spuligilla@deloitte.com';
  insert con1;
         
  string orgmail = Label.WCT_H1BCAP_Mailbox;
  OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
  Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'Notification_to_GM_I_Team' AND IsActive = true];
         
    Test.StartTest();
    WCT_H1bcap_Imm_Notification h1b1 = new WCT_H1bcap_Imm_Notification();
    WCT_H1bcap_Imm_Notification_Schedule createCon = new WCT_H1bcap_Imm_Notification_Schedule();
    system.schedule('New','0 0 2 1 * ?',createCon); 
    database.Executebatch(h1b1);
    Test.StopTest();   
 
     }
 }