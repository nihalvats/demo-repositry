@isTest
public class VTS_ProcessStaggingImigrationRe_Test {

    testMethod Static void test1()
    {
        
        /*Test Data */
        String immigrationStgH1RT=Schema.SObjectType.WCT_Immigration_Stagging_Table__c.getRecordTypeInfosByName().get('VTS Upload Record').getRecordTypeId();
        WCT_Immigration_Stagging_Table__c staggingRec= new WCT_Immigration_Stagging_Table__c();
        staggingRec.WCT_Identifier__c='dginde@deloitte.com H1B';
        staggingRec.H1B_Identifier_new__c='dginde@deloitte.com H1B';
        staggingRec.Status__c='Corrected';
        staggingRec.RecordTypeId=immigrationStgH1RT;
		insert staggingRec;
        
        Immigration_Upload_Settings__c setting= new Immigration_Upload_Settings__c();
        setting.Data_Type_of_Field__c='';
        setting.Immigration_API_Name__c='WCT_Identifier__c';
        setting.Column_Name__c='Identifier';
        setting.Name='test';
        setting.Staging_API_Name__c='WCT_Identifier__c';
        
        Immigration_Upload_Settings__c setting2= new Immigration_Upload_Settings__c();
        setting2.Data_Type_of_Field__c='';
        setting2.Immigration_API_Name__c='H1B_Identifier_new__c';
        setting2.Column_Name__c='Identifier';
        setting2.Name='test2';
        setting2.Staging_API_Name__c='H1B_Identifier_new__c';
        
        List<Immigration_Upload_Settings__c> allSettings = new List<Immigration_Upload_Settings__c>();
        allSettings.add(setting2);
        allSettings.add(setting);
        
        insert allSettings;
        
        VTS_ProcessStaggingImigrationRecords.processImmigrationRecords('Corrected');
    }
    
}