global class EncryptEmail {
    
    WebService static String encryptmail(string email)
    {
        return CryptoHelper.encrypt(email);
    }

}