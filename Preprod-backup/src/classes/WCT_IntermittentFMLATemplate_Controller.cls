global Class WCT_IntermittentFMLATemplate_Controller
{

public String idSet{get;set;}
public list<WCT_FMLA_Hours__c> fmlahours {get;set;}
public List<WCT_FMLA_Hours__c> getleavelist(){
 fmlahours= [ select id, WCT_FMLA_Hours_Used__c, WCT_FMLA_Week_Ending__c
        from WCT_FMLA_Hours__c where WCT_Associated_Leave__c = :idSet  ];
        return fmlahours;
}

public WCT_IntermittentFMLATemplate_Controller(){}

}