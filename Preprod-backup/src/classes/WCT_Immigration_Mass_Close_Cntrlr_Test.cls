@isTest

Private class WCT_Immigration_Mass_Close_Cntrlr_Test
{

 
    public static testmethod void test1()
    {
         
       Recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con11=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con11.WCT_Employee_Group__c = 'Active';
        insert con11;  
           
        WCT_Immigration__c immigRec1  = new WCT_Immigration__c();
        immigRec1.WCT_Immigration_Status__c = 'New';
        immigRec1.WCT_Milestone__c = 'TestMilestone';
        immigRec1.WCT_Visa_Type__c = 'H1BCAP';
        immigRec1.WCT_Assignment_Owner__c = con11.id;
        immigRec1.H1B_Is_USI_Upload__c=true;
        insert immigRec1;
        
        WCT_Task_Reference_Table__c refRec = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
        refRec.WCT_Assigned_to__c = 'Fragomen';
        insert refRec;
        
        Task t1= new Task();
        t1.Subject='Send Visa questionnaire to employee';
        t1.Status='Not Started';
        t1.OwnerId = Label.Fragomen_UserID;
        t1.WCT_Task_Reference_Table_ID__c= refRec.id;
        t1.WhatId = immigRec1.Id;
        t1.Task_Type__c = 'New';
        insert t1;
        
        Test.startTest();
        List<Task> tasklist1 = new List<Task>();
        tasklist1.add(t1);
        
        WCT_Immigration_Mass_Close_Controller.taskListWrapper tasklistWrap1 = new WCT_Immigration_Mass_Close_Controller.taskListWrapper(tasklist1);
        
        
        List<WCT_Immigration_Mass_Close_Controller.taskListWrapper> wraplist1 = new List<WCT_Immigration_Mass_Close_Controller.taskListWrapper>();
        wraplist1.add(tasklistWrap1);
        
        WCT_Immigration_Mass_Close_Controller.taskWrapper taskWrap1 = new WCT_Immigration_Mass_Close_Controller.taskWrapper(true,'test',wraplist1);       
        
        List<WCT_Immigration_Mass_Close_Controller.taskWrapper> taskwraplist1 = new List<WCT_Immigration_Mass_Close_Controller.taskWrapper>();
   
        taskwraplist1.add(taskWrap1);
            
        WCT_Immigration_Mass_Close_Controller.immigrationWrapper immigWrap1 = new WCT_Immigration_Mass_Close_Controller.immigrationWrapper(immigRec1,taskwraplist1);
        
        WCT_Immigration_Mass_Close_Controller controller1 = new WCT_Immigration_Mass_Close_Controller();
        controller1.selectedStatus = 'New';
        controller1.selectedViewBy = 'Fragomen';
        controller1.selectedViewBy = 'Fragomen';
        controller1.RecordTypeValue = 'ALL';
        controller1.fromRID = system.today()+50;
        controller1.toRID = system.today()+150;
        controller1.VisaTypeValue = 'B1/B2';
        

        controller1.getViewBy();
        controller1.getStatus();
        controller1.setRecordType();
        controller1.setVisaType();
        controller1.lawFirmValues();
        controller1.getListTasks();
        controller1.checkAll();
        controller1.massClose();
    
    
    
    
    
      WCT_Immigration__c immigRec2  = new WCT_Immigration__c();
        immigRec2.WCT_Immigration_Status__c = 'New';
        immigRec2.H1B_Is_USI_Upload__c=true;
        insert immigRec2;
        
        Contact con = new Contact();
        con.LastName = 'test';
        INSERT con;
        
        WCT_Task_Reference_Table__c refRec2 = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
        refRec2.WCT_Assigned_to__c = 'Employee';
        insert refRec2;

        Task t2= new Task();
        t2.Subject='Send Visa questionnaire to employee';
        t2.Status='Not Started';
        t2.WCT_Task_Reference_Table_ID__c= refRec2.id;
        t2.WhatId = immigRec2.Id;
        t2.Task_Type__c = 'New';
        t2.whoId = con.Id;
        insert t2;
        
        
        List<Task> tasklist2 = new List<Task>();
        tasklist2.add(t2);
        
        WCT_Immigration_Mass_Close_Controller.taskListWrapper tasklistWrap2 = new WCT_Immigration_Mass_Close_Controller.taskListWrapper(tasklist2);
        
        
        List<WCT_Immigration_Mass_Close_Controller.taskListWrapper> wraplist2 = new List<WCT_Immigration_Mass_Close_Controller.taskListWrapper>();
        wraplist2.add(tasklistWrap2);
        
        WCT_Immigration_Mass_Close_Controller.taskWrapper taskWrap2 = new WCT_Immigration_Mass_Close_Controller.taskWrapper(true,'test',wraplist2);
        
        List<WCT_Immigration_Mass_Close_Controller.taskWrapper> taskwraplist2 = new List<WCT_Immigration_Mass_Close_Controller.taskWrapper>();
        taskwraplist2.add(taskWrap2);
       
        WCT_Immigration_Mass_Close_Controller.immigrationWrapper immigWrap2 = new WCT_Immigration_Mass_Close_Controller.immigrationWrapper(immigRec2,taskwraplist2);
        
        WCT_Immigration_Mass_Close_Controller controller2 = new WCT_Immigration_Mass_Close_Controller();
        controller2.selectedStatus = 'New';
        controller2.selectedViewBy = 'Employee';
        controller2.RecordTypeValue = 'H1 Visa';
        controller2.VisaTypeValue = 'H1B Cap';
        controller2.getViewBy();
        controller2.getStatus();
        controller2.setRecordType();
        controller2.setVisaType();
        controller2.getListTasks();
        controller2.checkAll();
        controller2.massClose();
        controller2.lawFirmValues();
        
   WCT_Immigration__c immigRec3  = new WCT_Immigration__c();
        immigRec3.WCT_Immigration_Status__c = 'New';
        insert immigRec3;
        
        WCT_Task_Reference_Table__c refRec3 = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
        refRec3.WCT_Assigned_to__c = 'GM&I Team';
        insert refRec3;

        Task t3= new Task();
        t3.Subject='Send Visa questionnaire to employee';
        t3.Status='Not Started';
        t3.WCT_Task_Reference_Table_ID__c= refRec3.id;
        t3.WhatId = immigRec3.Id;
        t3.Task_Type__c = 'New';
        insert t3;
        
        
        List<Task> tasklist3 = new List<Task>();
        tasklist3.add(t3);
        
        WCT_Immigration_Mass_Close_Controller.taskListWrapper tasklistWrap3 = new WCT_Immigration_Mass_Close_Controller.taskListWrapper(tasklist3);
        
        
        List<WCT_Immigration_Mass_Close_Controller.taskListWrapper> wraplist3 = new List<WCT_Immigration_Mass_Close_Controller.taskListWrapper>();
        wraplist3.add(tasklistWrap3);
        
        WCT_Immigration_Mass_Close_Controller.taskWrapper taskWrap3 = new WCT_Immigration_Mass_Close_Controller.taskWrapper(true,'test',wraplist3);
        
        List<WCT_Immigration_Mass_Close_Controller.taskWrapper> taskwraplist3 = new List<WCT_Immigration_Mass_Close_Controller.taskWrapper>();
        taskwraplist3.add(taskWrap3);
       
        WCT_Immigration_Mass_Close_Controller.immigrationWrapper immigWrap3 = new WCT_Immigration_Mass_Close_Controller.immigrationWrapper(immigRec3,taskwraplist3);
        
        WCT_Immigration_Mass_Close_Controller controller3 = new WCT_Immigration_Mass_Close_Controller();      
        
     controller3.selectedStatus = '';
        controller3.selectedViewBy = '';
        controller3.RecordTypeValue = '';
        controller3.VisaTypeValue = '';
        controller3.lawFirmValues();
        controller3.getViewBy();
        controller3.getStatus();
        controller3.setRecordType();
        controller3.setVisaType();
        controller3.getListTasks();
        controller3.checkAll();
        controller3.massClose();
        
        Test.stopTest();
    
    }   
    public static testmethod void test2()
    {         Recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
    Contact con11=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con11.WCT_Employee_Group__c = 'Active';
        insert con11;  
    
       //   contact con = new contact();
       //   con.FirstName='Test';
        //  con.LastName='contact';
         // Insert con;
          contact con1=[SELECT Id FROM contact where Id =:con11.id];
          
    Recordtype rt1=[select id from recordtype where DeveloperName = 'L1_Visa'];
          WCT_Immigration__c immigRec1  = new WCT_Immigration__c();
         immigRec1.WCT_Immigration_Status__c = 'New';
         immigRec1.WCT_Milestone__c = 'TestMilestone';
         immigRec1.WCT_Visa_Type__c = 'L1A Extension';
         //immigRec1.Name='I-117087';
         immigRec1.WCT_Assignment_Owner__c = con1.id;
         immigRec1.H1B_Is_USI_Upload__c=true;
         immigRec1.H1B_Law_Firm_Name__c='Fragomen';
         //immigRec1.OwnerId='00G40000001WmgOEAS';
         immigRec1.WCT_Request_Initiation_Date__c=date.parse('10/10/2018');
         immigRec1.RecordTypeId='01240000000QLeu';
         insert immigRec1;
         System.debug('@tasklist1234'+ immigRec1);
         //System.debug('@tasklist12345'+ WCT_Immigration_Status__c + '---' + WCT_Assignment_Owner__c +'---'+H1B_Law_Firm_Name__c +'---'+H1B_Is_USI_Upload__c );
         WCT_Task_Reference_Table__c refRec = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
         refRec.WCT_Task_for_Object__c = 'WCT_Immigration__c';
         refRec.WCT_Assigned_to__c = 'Fragomen';
         insert refRec;
          
          Task t1 = new Task();
        t1.Subject='Send Visa questionnaire to employee';
        t1.Status='Not Started';
        t1.OwnerId = Label.Fragomen_UserID;
        t1.WCT_Task_Reference_Table_ID__c= refRec.id;
        t1.WhatId = immigRec1.Id;
        t1.Task_Type__c = 'New';
        insert t1;
        System.debug('@tasklist1'+t1 );
         Test.startTest();
        List<Task> tasklist1 = new List<Task>();
        tasklist1.add(t1);
        System.debug('@tasklist12'+tasklist1 );
        WCT_Immigration_Mass_Close_Controller.taskListWrapper tasklistWrap1 = new WCT_Immigration_Mass_Close_Controller.taskListWrapper(tasklist1);
        
        
        List<WCT_Immigration_Mass_Close_Controller.taskListWrapper> wraplist1 = new List<WCT_Immigration_Mass_Close_Controller.taskListWrapper>();
        wraplist1.add(tasklistWrap1);
        
        WCT_Immigration_Mass_Close_Controller.taskWrapper taskWrap1 = new WCT_Immigration_Mass_Close_Controller.taskWrapper(true,'test',wraplist1);       
        
        List<WCT_Immigration_Mass_Close_Controller.taskWrapper> taskwraplist1 = new List<WCT_Immigration_Mass_Close_Controller.taskWrapper>();
   
        taskwraplist1.add(taskWrap1);
                
        WCT_Immigration_Mass_Close_Controller.immigrationWrapper immigWrap1 = new WCT_Immigration_Mass_Close_Controller.immigrationWrapper(immigRec1,taskwraplist1);
        
        WCT_Immigration_Mass_Close_Controller controller1 = new WCT_Immigration_Mass_Close_Controller();
        controller1.selectedStatus = 'New';
        controller1.selectedViewBy = 'Fragomen';
        controller1.selectedViewBy = 'Fragomen';
        controller1.RecordTypeValue = 'L1 Visa';
        controller1.fromRID = system.today()-200;
        controller1.toRID = system.today()+200;

     

        controller1.getViewBy();
        controller1.getStatus();
     
        
        controller1.RecordTypeValue = 'L1 Visa';
  controller1.setVisaType();
        controller1.lawFirmValues();
        controller1.getListTasks();
        controller1.checkAll();
        controller1.massClose();
        Test.stopTest();
    }
   
   public static testmethod void test3()
    {         Recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
    Contact con11=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con11.WCT_Employee_Group__c = 'Active';
        insert con11;  
    
       //   contact con = new contact();
       //   con.FirstName='Test';
        //  con.LastName='contact';
         // Insert con;
          contact con1=[SELECT Id FROM contact where Id =:con11.id];
          
    Recordtype rt1=[select id from recordtype where DeveloperName = 'H1_Visa'];
          WCT_Immigration__c immigRec1  = new WCT_Immigration__c();
         immigRec1.WCT_Immigration_Status__c = 'New';
         immigRec1.WCT_Milestone__c = 'TestMilestone';
         immigRec1.WCT_Visa_Type__c = 'H1B Extension';
         //immigRec1.Name='I-117087';
         immigRec1.WCT_Assignment_Owner__c = con1.id;
         immigRec1.H1B_Is_USI_Upload__c=true;
         immigRec1.H1B_Law_Firm_Name__c='Fragomen';
         //immigRec1.OwnerId='00G40000001WmgOEAS';
         immigRec1.WCT_Request_Initiation_Date__c=date.parse('10/10/2018');
         immigRec1.RecordTypeId='01240000000QLeu';
         insert immigRec1;
         System.debug('@tasklist1234'+ immigRec1);
         //System.debug('@tasklist12345'+ WCT_Immigration_Status__c + '---' + WCT_Assignment_Owner__c +'---'+H1B_Law_Firm_Name__c +'---'+H1B_Is_USI_Upload__c );
         WCT_Task_Reference_Table__c refRec = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
         refRec.WCT_Task_for_Object__c = 'WCT_Immigration__c';
         refRec.WCT_Assigned_to__c = 'Fragomen';
         insert refRec;
          
          Task t1 = new Task();
        t1.Subject='Send Visa questionnaire to employee';
        t1.Status='Not Started';
        t1.OwnerId = Label.Fragomen_UserID;
        t1.WCT_Task_Reference_Table_ID__c= refRec.id;
        t1.WhatId = immigRec1.Id;
        t1.Task_Type__c = 'New';
        insert t1;
        System.debug('@tasklist1'+t1 );
         Test.startTest();
        List<Task> tasklist1 = new List<Task>();
        tasklist1.add(t1);
        System.debug('@tasklist12'+tasklist1 );
        WCT_Immigration_Mass_Close_Controller.taskListWrapper tasklistWrap1 = new WCT_Immigration_Mass_Close_Controller.taskListWrapper(tasklist1);
        
        
        List<WCT_Immigration_Mass_Close_Controller.taskListWrapper> wraplist1 = new List<WCT_Immigration_Mass_Close_Controller.taskListWrapper>();
        wraplist1.add(tasklistWrap1);
        
        WCT_Immigration_Mass_Close_Controller.taskWrapper taskWrap1 = new WCT_Immigration_Mass_Close_Controller.taskWrapper(true,'test',wraplist1);       
        
        List<WCT_Immigration_Mass_Close_Controller.taskWrapper> taskwraplist1 = new List<WCT_Immigration_Mass_Close_Controller.taskWrapper>();
   
        taskwraplist1.add(taskWrap1);
                
        WCT_Immigration_Mass_Close_Controller.immigrationWrapper immigWrap1 = new WCT_Immigration_Mass_Close_Controller.immigrationWrapper(immigRec1,taskwraplist1);
        
        WCT_Immigration_Mass_Close_Controller controller1 = new WCT_Immigration_Mass_Close_Controller();
        controller1.selectedStatus = 'New';
        controller1.selectedViewBy = 'Fragomen';
        controller1.selectedViewBy = 'Fragomen';
        controller1.RecordTypeValue = 'H1 Visa';
        controller1.fromRID = system.today()-200;
        controller1.toRID = system.today()+200;

     

        controller1.getViewBy();
        controller1.getStatus();
     
        
        controller1.RecordTypeValue = 'H1 Visa';
  controller1.setVisaType();
        controller1.lawFirmValues();
        controller1.getListTasks();
        controller1.checkAll();
        controller1.massClose();
        Test.stopTest();
    }
    
     public static testmethod void test4()
    {         Recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
    Contact con11=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con11.WCT_Employee_Group__c = 'Active';
        insert con11;  
    
       //   contact con = new contact();
       //   con.FirstName='Test';
        //  con.LastName='contact';
         // Insert con;
          contact con1=[SELECT Id FROM contact where Id =:con11.id];
          
    Recordtype rt1=[select id from recordtype where DeveloperName = 'B1_Visa'];
          WCT_Immigration__c immigRec1  = new WCT_Immigration__c();
         immigRec1.WCT_Immigration_Status__c = 'New';
         immigRec1.WCT_Milestone__c = 'TestMilestone';
         immigRec1.WCT_Visa_Type__c = 'B1/B2';
         //immigRec1.Name='I-117087';
         immigRec1.WCT_Assignment_Owner__c = con1.id;
         immigRec1.H1B_Is_USI_Upload__c=true;
         immigRec1.H1B_Law_Firm_Name__c='Fragomen';
         //immigRec1.OwnerId='00G40000001WmgOEAS';
         immigRec1.WCT_Request_Initiation_Date__c=date.parse('10/10/2018');
         immigRec1.RecordTypeId='01240000000QLeu';
         insert immigRec1;
         System.debug('@tasklist1234'+ immigRec1);
         //System.debug('@tasklist12345'+ WCT_Immigration_Status__c + '---' + WCT_Assignment_Owner__c +'---'+H1B_Law_Firm_Name__c +'---'+H1B_Is_USI_Upload__c );
         WCT_Task_Reference_Table__c refRec = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
         refRec.WCT_Task_for_Object__c = 'WCT_Immigration__c';
         refRec.WCT_Assigned_to__c = 'Fragomen';
         insert refRec;
          
          Task t1 = new Task();
        t1.Subject='Send Visa questionnaire to employee';
        t1.Status='Not Started';
        t1.OwnerId = Label.Fragomen_UserID;
        t1.WCT_Task_Reference_Table_ID__c= refRec.id;
        t1.WhatId = immigRec1.Id;
        t1.Task_Type__c = 'New';
        insert t1;
        System.debug('@tasklist1'+t1 );
         Test.startTest();
        List<Task> tasklist1 = new List<Task>();
        tasklist1.add(t1);
        System.debug('@tasklist12'+tasklist1 );
        WCT_Immigration_Mass_Close_Controller.taskListWrapper tasklistWrap1 = new WCT_Immigration_Mass_Close_Controller.taskListWrapper(tasklist1);
        
        
        List<WCT_Immigration_Mass_Close_Controller.taskListWrapper> wraplist1 = new List<WCT_Immigration_Mass_Close_Controller.taskListWrapper>();
        wraplist1.add(tasklistWrap1);
        
        WCT_Immigration_Mass_Close_Controller.taskWrapper taskWrap1 = new WCT_Immigration_Mass_Close_Controller.taskWrapper(true,'test',wraplist1);       
        
        List<WCT_Immigration_Mass_Close_Controller.taskWrapper> taskwraplist1 = new List<WCT_Immigration_Mass_Close_Controller.taskWrapper>();
   
        taskwraplist1.add(taskWrap1);
                
        WCT_Immigration_Mass_Close_Controller.immigrationWrapper immigWrap1 = new WCT_Immigration_Mass_Close_Controller.immigrationWrapper(immigRec1,taskwraplist1);
        
        WCT_Immigration_Mass_Close_Controller controller1 = new WCT_Immigration_Mass_Close_Controller();
        controller1.selectedStatus = 'New';
        controller1.selectedViewBy = 'Fragomen';
        controller1.selectedViewBy = 'Fragomen';
        controller1.RecordTypeValue = 'B1 Visa';
        controller1.fromRID = system.today()-200;
        controller1.toRID = system.today()+200;

     

        controller1.getViewBy();
        controller1.getStatus();
     
        
        controller1.RecordTypeValue = 'B1 Visa';
  controller1.setVisaType();
        controller1.lawFirmValues();
        controller1.getListTasks();
        controller1.checkAll();
        controller1.massClose();
        Test.stopTest();
    }
}