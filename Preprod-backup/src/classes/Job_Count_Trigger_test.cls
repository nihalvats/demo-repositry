@isTest
public class Job_Count_Trigger_test {
    static testmethod void method1(){
        //set<Id> compIds= new Set<Id>();
      //  List<company__c> compRollup = new List<company__c>(); 
        Company__c  com = new Company__c ();
        com.DUNS_Number__c='123456789';
        com.Name = 'Test1';
        insert com;
        
        job__c j = new job__c();
        j.name = 'Test';
        j.Company__c = com.Id;
        j.AR_Company_Name__c = 'Test company';
        j.Job_Category__c = 'HR';        
        j.AR_Primary_Location_City__c ='Test city';
        j.CCL_Contact_for_company_if_applicable__c  = 'Test Comapany';
        j.Email__c = 'TEst@deloitte.com';
        j.Description__c = 'Sample Description';
        j.AR_Requirements__c ='Test Requirement';
        j.Source__c = 'External';
        insert j;
            
}
}