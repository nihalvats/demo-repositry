/*

Project : VRM Contractor Management
Associated VF Page : CVM Thank You Page
Test Class : CVM_Class_Test
Author : Karthik Raju Gollapalli

*/


public with sharing class CVM_Thank_You_Class extends SitesTodHeaderController
{
    public string emValue {get;set;}
    
    public CVM_Thank_You_Class()
    {
        //emValue = ApexPages.currentPage().getParameters().get('em');
        //string userEmail = cryptoHelper.decrypt(emValue);
        string userEmail = loggedInContact.email;
    }
    
    public Pagereference gotohomepage()
    {    
        system.debug('## gotorequestpage');
        PageReference MyRequestsPageRef = Page.CVM_My_Request;
        MyRequestsPageRef.getParameters().put('em',ApexPages.currentPage().getParameters().get('em'));
        MyRequestsPageRef.setRedirect(true);
        return MyRequestsPageRef;
    }
}