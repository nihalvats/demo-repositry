/**
Created By: Balu
Started: 22/06/2015
Class Name: BatchDataImport  
Description: This class is used to handle batch request for large data download request.              
**/


global class BatchDataImport  implements Database.Batchable<sObject>, Database.Stateful{

    /* Variable Declaration */
    
    public String QueryText;
    public string Conditions;
    public string selectedObject;
    public boolean IsContainParentRecords;
    global Set<string> lstQueryFields;
    global string csvFileString='';
    public string QueryLimit='limit 100000';
    public string ParentRefAPIName;
    public string ParentQuery;
    public set<Id> setAllIds=new set<Id>();

    /*
    * METHOD: - Start Method of Batch Apex used to prepare query initiate process.
    * INPUT: - batchableContext bc,List<sObject> 
    */
    global Database.querylocator start(Database.BatchableContext BC){
          

          
          List<SObject> instance = new List<SObject>();
          if(QueryText.ToLowerCase().contains('where'))
          {
          QueryText=QueryText.substring(0,QueryText.ToLowerCase().indexOf('where'));
          
          }
          /* Remove Limit in Query */
          if(QueryText.ToLowerCase().contains('limit'))
          {
          QueryText=QueryText.substring(0,QueryText.ToLowerCase().indexOf('limit'));
          }
          //system.debug('------1-QueryText--Qry-------------'+QueryText);
          ParentQuery= QueryText;
          if(string.IsNotEmpty(Conditions)){
          QueryText+=' where '+Conditions;
          }
          //system.debug('-------Condition-------------'+Conditions);
          //system.debug('------2-QueryText--Condition-------------'+QueryText);
          //QueryText+=QueryLimit;
         
            
            for(String fieldName : lstQueryFields)
            {
               if(fieldName.ToLowerCase()!='id'){
              //csvFileString = csvFileString + ',' + fieldName.replace(',','');
              csvFileString = csvFileString + '\t' + fieldName.replace(',','\t');
              }
            } 
           // csvFileString = csvFileString.replaceFirst(',','') + '\n';
            csvFileString = csvFileString.replaceFirst('\t','') + '\n';
            if(Test.IsRunningTest()){
            QueryText='select Id,Name,Type from Account';
            }
    
        return Database.getQueryLocator(QueryText);        
    }//end of start method
    
    /*
    * METHOD: - Execute Method of Batch Apex
    * INPUT: - batchableContext bc,List<sObject> 
    */
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
    if (Limits.getHeapSize() < 11000000) {    
     Export(scope);  
     }
     else{
         
        ProcessAndNotification();
        System.abortJob(BC.getJobId());     
     }
    }
    
    /* 
    Method: Export 
    Description: Method to Process records batch wise to binaries.
    */ 
    
    public void Export(List<sObject> instance){
       
        
        List<SObject> lstParentRecs = new List<SObject>();
        try{  

          
          /* Start Parent Records Functionality  */
          if(IsContainParentRecords){
          Set<Id> RecIds =new Set<Id>();
          for(SObject objId:instance){
          RecIds.add(objId.Id);
          }
          //string ParentLimit=' limit '+QueryLimit;
          string ParentQueryString='select '+ParentRefAPIName+' from '+selectedObject+' where Id in :RecIds ';//+ParentLimit;
          Map<Id,sObject> mapParent = new Map<Id,sObject>(Database.query(ParentQueryString));
          Set<Id> ParentIds =new Set<Id>();
          for(Id objParentId:mapParent.keySet()){
           sObject sObjParent = mapParent.get(objParentId);
           if(!RecIds.contains((Id)sObjParent.get(ParentRefAPIName))){
           if((Id)sObjParent.get(ParentRefAPIName)!=null)
           ParentIds.add((Id)sObjParent.get(ParentRefAPIName));
            }
          }
          
          Boolean WhereContains = false;
            if(ParentQuery.toLowerCase().indexOf('where')>-1) {
                    WhereContains = true;
                }
             if(WhereContains){
               ParentQuery+=' and '+ParentRefAPIName+' in :ParentIds and Id not in :RecIds'; //  
             }else{
               ParentQuery+=' where '+ParentRefAPIName+' in :ParentIds and Id not in :RecIds';//  
             }
          //system.debug('----------ParentQuery-------------'+ParentQuery);
          //system.debug('----------ParentRefAPIName-------------'+ParentRefAPIName);
           //ParentQuery+=' limit '+QueryLimit;
           lstParentRecs = Database.query(ParentQuery);
           instance.addAll(lstParentRecs);  
          }     
          /* End Parent Records Functionality  */
            ProcessRecordsToFile(instance);
            instance.clear(); // Clear for heap size improvement
           
          }catch(Exception e){
          system.debug('----------Exception-------------'+e.getMessage()+'-----at Line #----'+e.getLineNumber()); 
          }
       }
   
    /* 
    Method: ProcessRecordsToFile 
    Description: Method to Process Sobject List into Binaries.
    */ 
    
    global void ProcessRecordsToFile(List<sObject> lstObj)
    {
            String fileRow = '';
            String fieldValue = '';
        try{
           for(SObject obj : lstObj) 
            {
              If(!setAllIds.Contains(obj.Id)){
                setAllIds.add(obj.Id);
                fileRow = '';
                for(String fieldName : lstQueryFields)
                {
                  if(fieldName.ToLowerCase()!='id'){    
                   String DynamicRefField = '';
                    if(fieldName.contains('.')){
                    try{
                    string[] strAryDymic=fieldName.split('\\.');
                    DynamicRefField=(string)obj.getSobject(strAryDymic[0]).get(strAryDymic[1]);
                    fieldValue = '' + DynamicRefField;
                    }
                    catch(exception ex){
                    fieldValue = '' + DynamicRefField;
                    }
                    }
                    else{
                       fieldValue = '' + obj.get(fieldName);
                        }
                    //fieldValue = fieldValue.replace(',','');
                    //fileRow = fileRow + ',' + fieldValue;
                    fieldValue = fieldValue.replace('\t','');
                    fileRow = fileRow + '\t' + fieldValue;
                  }
                }
                //fileRow = fileRow.replaceFirst(',','');
                fileRow = fileRow.replaceFirst('\t','');
                csvFileString = csvFileString + fileRow + '\n';
                //SObject a = c.getSObject('Account');// for parent objects fields like Account.Name
               
            }
            }
         }catch(Exception e){
          system.debug('----------Exception-------------'+e.getMessage()+'-----at Line #----'+e.getLineNumber()); 
         }
        
        
        
    }
    /*
    * METHOD: - ProcessAndNotification
    * Description: - Process string to Blob and Mail Notification with Attachment
    */ 
    global void ProcessAndNotification(){
        try{
        
        Blob csvBlob = Blob.valueOf(csvFileString);
        String base64Value=EncodingUtil.base64Encode(csvBlob);
     
        if(string.IsNotEmpty(base64Value)){
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        string csvname= selectedObject+'_'+Date.today().Year()+'-'+Date.today().Month()+'-'+Date.today().Day()+'.xls';
        csvAttc.setFileName(csvname);
        csvAttc.setContentType('application/vnd.ms-excel');
        csvAttc.setBody(csvBlob);   
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        string MailId = UserInfo.getUserEmail();
        string sBody = 'Hello '+UserInfo.getName()+',<br/><br/> Requested Data Report is prepared. Please find attachment with this mail. <br/><br/>';
        String[] toAddresses = new String[] {MailId};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Requested '+selectedObject+' Data Report is Prepared at: '+Datetime.now());
        mail.setHtmlBody(sBody);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        csvFileString=null;
        csvBlob=null;
        
        }   
        }catch(Exception e){
          system.debug('----------Exception-------------'+e.getMessage()+'-----at Line #----'+e.getLineNumber()); 
         }
        
    }
    
    /*
    * METHOD DEFINITION: - Finish Method of Batch Apex
    * INPUT: - batchableContext bc
    */
        
    global void finish(Database.BatchableContext BC){
      
      ProcessAndNotification(); 

    }//end of finish
    
}