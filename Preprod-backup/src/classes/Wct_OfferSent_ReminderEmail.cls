global class Wct_OfferSent_ReminderEmail implements Database.Batchable<sObject>,Schedulable
{
    global void execute(SchedulableContext SC) {
        //Wct_OfferSent_ReminderEmail batch = new Wct_OfferSent_ReminderEmail();
        Wct_OfferSent_ReminderEmail batch = new Wct_OfferSent_ReminderEmail();
        ID batchprocessid = Database.executeBatch(batch,100); 
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        Date offerCreatedDate = Date.valueOf(System.Label.WCT_Offer_CreatedDate2);
        String usergroup = Label.usergroup;
        String query = 'SELECT Id,Name,WCT_Candidate__c,WCT_Full_Name__c,WCT_status__c,WCT_Candidate_Email__c,WCT_Offer_Sent_Date__c,WCT_User_Group__c FROM WCT_Offer__c where WCT_status__c = \'Offer Sent\' and WCT_Candidate_Email__c != null and WCT_Offer_Sent_Date__c != null and  createddate >=:offerCreatedDate and WCT_User_Group__c =:usergroup';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<WCT_Offer__c> listofferRecords)
    {
        String strEmailTop  ='';
        String strEmailBottom ='';
        Decimal seconds;
        Decimal hrs;
        DateTime dT,dT2;
        Date myDate,myDate2;
        string offerSentDate,nextDate;
        List<WCT_Offer__c> offerList = new List<WCT_Offer__c>();
        List<Messaging.SingleEmailMessage> email = new List<Messaging.SingleEmailMessage>();
        
        for(WCT_Offer__c offerRecord : listofferRecords)
        {
            //seconds = BusinessHours.diff(Label.WCT_Default_Business_Hours_ID, offerRecord.WCT_Offer_Sent_Date__c, System.now())/ 1000;
             if (offerRecord.WCT_User_Group__c =='US GLS India')
             {
                 //seconds = BusinessHours.diff(Label.WCT_USI_Offer_Business_Hours_ID, offerRecord.CreatedDate, System.now())/ 1000;
                 seconds = BusinessHours.diff(Label.WCT_USI_Offer_Business_Hours_ID, offerRecord.WCT_Offer_Sent_Date__c, System.now())/ 1000;
             } else if (offerRecord.WCT_User_Group__c =='United States')
             {
                 //seconds = BusinessHours.diff(Label.WCT_US_Offer_Business_Hours_ID, offerRecord.CreatedDate, System.now())/ 1000;
                 seconds = BusinessHours.diff(Label.WCT_US_Offer_Business_Hours_ID, offerRecord.WCT_Offer_Sent_Date__c, System.now())/ 1000;
             }
            hrs = seconds / 3600;
            system.debug('hrs:value'+hrs);
            system.debug('seconds :value'+seconds );
            if(hrs > 72)
            {
            offerList.add(offerRecord);
            }
        }
            strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; }, </style> </head> <body> <br> Dear Candidate,<br><br>';
            strEmailBottom += '<br>Regards,<br><br>Deloitte India Offers (US).<br> </body> </html>';
            
         for(WCT_Offer__c offerRecord : offerList)
         {
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
             List<string> ToEmailAddress = new List<string>();
             String strEmailBody ='';
             
             dT = offerRecord.WCT_Offer_Sent_Date__c; 
             myDate = date.newinstance(dT.year(), dT.month(), dT.day());  
             offerSentDate = myDate.format();
             
             dT2 = System.now().addDays(1); 
             myDate2 = date.newinstance(dT2.year(), dT2.month(), dT2.day());  
             nextDate = myDate2.format();
             
             strEmailBody += '<tr><td>  This is a reminder to confirm your decision on the offer which was extended to you on '+ offerSentDate +'.</td></tr><br>';
             strEmailBody += '<tr><td>  We request you to send in your response by signing the offer letter digitally, no later than '+ nextDate +'.</td></tr><br>';
             strEmailBody += '<tr><td>  Should you have any concerns, kindly reach out to your respective recruiter.</td></tr><br>';
             
             ToEmailAddress.add(offerRecord.WCT_Candidate_Email__c);
             mail.settoAddresses(ToEmailAddress);
             mail.setSubject('Reminder:Please sign Deloitte Offer');
             mail.setHTMLBody(strEmailTop+strEmailBody+strEmailBottom);
             mail.setSaveAsActivity(false);
             ToEmailAddress.clear();
             email.add(mail);
         }
         if(email!= null)
         {
                Messaging.sendEmail(email);
         }
   }

   global void finish(Database.BatchableContext BC){
      
   }
}