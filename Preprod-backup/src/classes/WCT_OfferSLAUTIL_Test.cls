/*****************************************************************************************
    Name    : WCT_OfferSLAUTIL_Test
    Desc    : Test class for WCT_OfferSLAUTIL class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Lakshmi Madhuri               08/05/2014         Created 

******************************************************************************************/
@isTest

public class WCT_OfferSLAUTIL_Test {
  /****************************************************************************************
    * @author      - Lakshmi Madhuri
    * @date        - 08/05/2014
    * @description - Test Method for all methods of WCT_OfferSLAUTIL Class 
    *****************************************************************************************/
    public static Contact con;
   // public static WCT_Offer__c offer;
    public static WCT_Offer_Status__c tempOffSt;
        
    public static void createData(){
       String caseRecordTypeId=Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Campus Hire Offer').getRecordTypeId();

        con=WCT_UtilTestDataCreation.createContact();
        con.email='test@deloitte.com';
        con.WCT_Visa_Type__c='temp';
        con.MobilePhone='123';
        con.WCT_User_Group__c = 'United States';
        insert con;
        
               
        
        WCT_Offer__c offer = new WCT_Offer__c();
        
        offer.RecordTypeId= caseRecordTypeId;
        offer.WCT_status__c = 'Offer to Be Extended'; 
        offer.WCT_Candidate__c=con.id;
        offer.WCT_Recruiter_Email__c='test@test.com';
        offer.WCT_Recruiting_Coordinator_Email__c='testco@test.com';
        offer.Team_Name__c ='West';
        
        insert offer;
        
        tempOffSt = new WCT_Offer_Status__c(WCT_Related_Offer__c=offer.id);
        tempOffSt.WCT_Enter_State_Date_Time__c=system.now();
        insert tempOffSt;
    
    }
    
    public static testmethod void test1()
    {
        Test.starttest();
        WCT_OfferSLAUTIL_Test.createData();
        WCT_Offer__c off = [SELECT id,WCT_status__c FROM WCT_Offer__c where id=:tempOffSt.WCT_Related_Offer__c LIMIT 1];
        off.WCT_status__c = 'Draft in Progress';      
        update off; 
        off.WCT_status__c = 'Offer Accepted'; 
        update off; 
             
        Test.stoptest();
    }
      
    public static testmethod void test3()
    {
        Test.starttest();
        WCT_OfferSLAUTIL_Test.m1();
        WCT_Offer__c off = [SELECT id,WCT_status__c,Time_capture_berfore_pause__c FROM WCT_Offer__c where id=:tempOffSt.WCT_Related_Offer__c LIMIT 1];
        off.WCT_status__c = 'Draft In Progress';      
        update off; 
        off.WCT_status__c = 'Offer Accepted'; 
        update off;
        off.WCT_status__c= 'On Hold per COE';
        off.WCT_Waiting_for_inputs_from__c='Address fields missing.';
        update off; 
                    
        Test.stoptest();
    }
    
  public static testmethod void test4()
    {
        Test.starttest();
        WCT_OfferSLAUTIL_Test.m1();
        WCT_Offer__c off = [SELECT id,WCT_status__c,Time_capture_berfore_pause__c FROM WCT_Offer__c where id=:tempOffSt.WCT_Related_Offer__c LIMIT 1];
        off.WCT_status__c = 'Draft In Progress';      
        update off; 
        off.WCT_status__c = 'Offer Accepted'; 
        update off;
        off.WCT_status__c= 'On Hold per COE';
        off.WCT_Waiting_for_inputs_from__c='Address fields missing.';
        update off; 
                    
        Test.stoptest();
    }  
    
   public static testmethod void test5()
    {
        Test.starttest();
        WCT_OfferSLAUTIL_Test.m2();
        WCT_Offer__c off = [SELECT id,WCT_status__c,Time_capture_berfore_pause__c FROM WCT_Offer__c where id=:tempOffSt.WCT_Related_Offer__c LIMIT 1];
        off.WCT_status__c = 'Draft In Progress';      
        update off; 
        off.WCT_status__c = 'Offer Accepted'; 
        update off;
        off.WCT_status__c= 'On Hold per COE';
        off.WCT_Waiting_for_inputs_from__c='Address fields missing.';
        update off; 
                    
        Test.stoptest();
    } 
    
    
     public static testmethod void test6()
    {
        Test.starttest();
        WCT_OfferSLAUTIL_Test.m3();
        WCT_Offer__c off = [SELECT id,WCT_status__c,Time_capture_berfore_pause__c FROM WCT_Offer__c where id=:tempOffSt.WCT_Related_Offer__c LIMIT 1];
        off.WCT_status__c = 'Draft In Progress';      
        update off; 
        off.WCT_status__c = 'Offer Accepted'; 
        update off;
        off.WCT_status__c= 'On Hold per COE';
        off.WCT_Waiting_for_inputs_from__c='Address fields missing.';
        update off; 
                    
        Test.stoptest();
    }    
 
 public static testmethod void test7()
    {
        Test.starttest();
        WCT_OfferSLAUTIL_Test.m4();
        WCT_Offer__c off = [SELECT id,WCT_status__c,Time_capture_berfore_pause__c FROM WCT_Offer__c where id=:tempOffSt.WCT_Related_Offer__c LIMIT 1];
        off.WCT_status__c = 'Draft In Progress';      
        update off; 
        off.WCT_status__c = 'Offer Accepted'; 
        update off;
        off.WCT_status__c= 'On Hold per COE';
        off.WCT_Waiting_for_inputs_from__c='Address fields missing.';
        update off; 
                    
        Test.stoptest();
    }    
    
 public static testmethod void test9()
    {
        Test.starttest();
        WCT_OfferSLAUTIL_Test.m5();
        WCT_Offer__c off = [SELECT id,WCT_status__c,Time_capture_berfore_pause__c FROM WCT_Offer__c where id=:tempOffSt.WCT_Related_Offer__c LIMIT 1];
        off.WCT_status__c = 'Draft In Progress';      
        update off; 
        off.WCT_status__c = 'Offer Accepted'; 
        update off;
        off.WCT_status__c= 'On Hold per COE';
        off.WCT_Waiting_for_inputs_from__c='Address fields missing.';
        update off; 
                    
        Test.stoptest();
    }   
    
 /* public static testmethod void test11()
    {
        Test.starttest();
        WCT_OfferSLAUTIL_Test.m5();
        WCT_Offer__c off = [SELECT id,WCT_status__c,Time_capture_berfore_pause__c FROM WCT_Offer__c where id=:tempOffSt.WCT_Related_Offer__c LIMIT 1];
        
        off.WCT_status__c = 'Draft In Progress';      
        update off; 
        off.WCT_status__c = 'Offer Accepted'; 
        update off;
        off.WCT_status__c= 'Ready for Review';
        off.WCT_Waiting_for_inputs_from__c='Address fields missing.';
        off.Count_Pause_time__c =3;
       
        update off; 
                    
        Test.stoptest();
    }    */    
 
 public static testmethod void test10()
    {
        Test.starttest();
        WCT_OfferSLAUTIL_Test.m5();
        WCT_Offer__c off = [SELECT id,WCT_status__c,Time_capture_berfore_pause__c FROM WCT_Offer__c where id=:tempOffSt.WCT_Related_Offer__c LIMIT 1];
        
        off.WCT_status__c= 'On Hold per COE';
        off.WCT_Waiting_for_inputs_from__c='Address fields missing.';
        update off; 
                    
        Test.stoptest();
    }       
 
 
  public static void m1(){
  String caseRecordTypeId=Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Campus Hire Offer').getRecordTypeId();
        con=WCT_UtilTestDataCreation.createContact();
        con.email='test@deloitte.com';
        con.WCT_Visa_Type__c='temp';
        con.MobilePhone='123';
        con.WCT_User_Group__c = 'United States';
        insert con;
        
               
       
        
        WCT_Offer__c offer = new WCT_Offer__c();
        offer.RecordTypeId= caseRecordTypeId;
        offer.WCT_status__c = 'Offer to Be Extended'; 
        offer.WCT_Candidate__c=con.id;
        offer.WCT_Recruiter_Email__c='test@test.com';
        offer.WCT_Recruiting_Coordinator_Email__c='testco@test.com';
        offer.Team_Name__c ='West';
        insert offer;
        
        tempOffSt = new WCT_Offer_Status__c(WCT_Related_Offer__c=offer.id);
        tempOffSt.WCT_Enter_State_Date_Time__c=system.now();
        insert tempOffSt;
    
    }   
   
  public static void m2(){
  String caseRecordTypeId=Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Campus Hire Offer').getRecordTypeId();
        con=WCT_UtilTestDataCreation.createContact();
        con.email='test@deloitte.com';
        con.WCT_Visa_Type__c='temp';
        con.MobilePhone='123';
        con.WCT_User_Group__c = 'United States';
        insert con;
        
               
      WCT_Offer__c offer = new WCT_Offer__c();
        offer.RecordTypeId= caseRecordTypeId;
        offer.WCT_status__c = 'Offer to Be Extended'; 
        offer.WCT_Candidate__c=con.id;
        offer.WCT_Recruiter_Email__c='test@test.com';
        offer.WCT_Recruiting_Coordinator_Email__c='testco@test.com';
        offer.Team_Name__c ='West';
        insert offer;
        
        tempOffSt = new WCT_Offer_Status__c(WCT_Related_Offer__c=offer.id);
        tempOffSt.WCT_Enter_State_Date_Time__c=system.now();
        insert tempOffSt;
    
    }    

   public static void m3(){
   String caseRecordTypeId=Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Campus Hire Offer').getRecordTypeId();
        con=WCT_UtilTestDataCreation.createContact();
        con.email='test@deloitte.com';
        con.WCT_Visa_Type__c='temp';
        con.MobilePhone='123';
        con.WCT_User_Group__c = 'United States';
        insert con;
        
              WCT_Offer__c offer = new WCT_Offer__c(); 
       
        offer.RecordTypeId= caseRecordTypeId;
        offer.WCT_status__c = 'Offer to Be Extended'; 
        offer.WCT_Candidate__c=con.id;
        offer.WCT_Recruiter_Email__c='test@test.com';
        offer.WCT_Recruiting_Coordinator_Email__c='testco@test.com';
        offer.Team_Name__c ='West';
        insert offer;
        
        tempOffSt = new WCT_Offer_Status__c(WCT_Related_Offer__c=offer.id);
        tempOffSt.WCT_Enter_State_Date_Time__c=system.now();
        insert tempOffSt;
    
    }  
   
   public static void m4(){
   String caseRecordTypeId=Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Campus Hire Offer').getRecordTypeId();
        con=WCT_UtilTestDataCreation.createContact();
        con.email='test@deloitte.com';
        con.WCT_Visa_Type__c='temp';
        con.MobilePhone='123';
        con.WCT_User_Group__c = 'United States';
        insert con;
        
          WCT_Offer__c offer = new WCT_Offer__c();     
       
        offer.RecordTypeId= caseRecordTypeId;
        offer.WCT_status__c = 'Offer to Be Extended'; 
        offer.WCT_Candidate__c=con.id;
        offer.WCT_Recruiter_Email__c='test@test.com';
        offer.WCT_Recruiting_Coordinator_Email__c='testco@test.com';
        offer.Team_Name__c ='West';
        insert offer;
        
        tempOffSt = new WCT_Offer_Status__c(WCT_Related_Offer__c=offer.id);
        tempOffSt.WCT_Enter_State_Date_Time__c=system.now();
        insert tempOffSt;
    
    } 
    
  public static void m5(){
  String caseRecordTypeId=Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosByName().get('US Campus Hire Offer').getRecordTypeId();
        con=WCT_UtilTestDataCreation.createContact();
        con.email='test@deloitte.com';
        con.WCT_Visa_Type__c='temp';
        con.MobilePhone='123';
        con.WCT_User_Group__c = 'United States';
        insert con;
        WCT_Offer__c offer = new WCT_Offer__c();
        offer.RecordTypeId= caseRecordTypeId;
        offer.WCT_status__c = 'Offer to Be Extended'; 
        offer.WCT_Candidate__c=con.id;
        offer.WCT_Recruiter_Email__c='test@test.com';
        offer.WCT_Recruiting_Coordinator_Email__c='testco@test.com';
        offer.Team_Name__c ='West';
        offer.WCT_1ST_INSTALL_PAYMENT_DATE__c = date.parse('03/05/2015');
        
        insert offer;
        
        Attachment attvar = WCT_UtilTestDataCreation.createAttachment(offer.id);
        
        insert attvar;
        
        tempOffSt = new WCT_Offer_Status__c(WCT_Related_Offer__c=offer.id);
        tempOffSt.WCT_Enter_State_Date_Time__c=system.now();
        insert tempOffSt;
    
    }          
    
    
}