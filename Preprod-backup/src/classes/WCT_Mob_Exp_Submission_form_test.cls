@isTest
public class WCT_Mob_Exp_Submission_form_test
{
    public static testmethod void m1()
    {    
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Immigration__c immi=WCT_UtilTestDataCreation.createImmigration(con.id);
        insert immi;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        insert taskRef;
        task t=WCT_UtilTestDataCreation.createTask(immi.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        Test.starttest();
        PageReference pageRef = Page.WCT_Task_Completion_form;
        Test.setCurrentPage(pageRef); 
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        
        WCT_Mob_Exp_Submission_formController controller=new WCT_Mob_Exp_Submission_formController();
        
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller.updateTaskFlags();
        controller=new WCT_Mob_Exp_Submission_formController();
        controller.updateTaskFlags();
        Test.stoptest();
    }
    
 }