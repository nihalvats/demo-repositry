public class WCT_ArticleExp_EmailNotify_30days {

        public static final list <string> DEFAULT_EMAIL_LIST = new list <string> {
                                    'kpopli@deloitte.com', 'lvinay_qa@deloitte.com'
                                    };
                                                                
        public static Map<String,String> assetOwnersMap = new map<string, string>();
        
        public static void kavNames(){ 

            string query = 'SELECT ArticleNumber,ArticleType,CreatedBy.email,KnowledgeArticleId,Title,UrlName FROM KnowledgeArticleVersion where PublishStatus = \'Online\' and Language = \'en_US\' ';
            system.debug('Results of query'+query);
            
            map < String, KnowledgeArticleVersion > knowledgearticleversiondata = new map < String, KnowledgeArticleVersion > (); // To Store all the article data from KAV
            map < String, List < Sobject >> knowledgearticlemap                 = new map < String, List < Sobject >> (); //To Store the Article information from each Article type
            Set < String > articletype                                          = new Set < String > (); // Collects all the Article Types from query results
            map < string, Set < String >> articletypetoarticles                 = new map < string, Set < String >> (); // Collects the articles of different article type
            list < Messaging.SingleEmailMessage > emailmessages                 = new list < Messaging.SingleEmailMessage > (); // Initializing the emailmessage
            String mailbody;
            
            for (Sobject obj: database.query(query)) { // this for loop will iterate over the query and all the distinct ArticleTypes and Articlenumbers are captured

                KnowledgeArticleVersion kav = (KnowledgeArticleVersion) obj;
                String s = kav.ArticleType;
                String s1 = '__kav';
                if (s.contains(s1)) { 
                    articletype.add(s); 
                }
                
                String artnumber = kav.Articlenumber;
                knowledgearticleversiondata.put(artnumber, kav);

                if (articletypetoarticles.containsKey(s)) {
                    Set < String > existingids = articletypetoarticles.get(s);
                    existingids.add(artnumber);
                    articletypetoarticles.put(s, existingids);
                } 
                else {
                    Set < String > articleIds = new Set < String > ();
                    articleIds.add((String) kav.get('ArticleNumber'));
                    articletypetoarticles.put(s, articleIds);
                }
            }

            list < String > temparticletypes = new list < String > ();
            map < String, Knowledgearticleversion > validrecordsForUpdate   = new map < String, Knowledgearticleversion > (); // This map will capture the valid records which are expired
            temparticletypes.addAll(articletype);
            
            for (String s: temparticletypes) { // this block will execute only number of articletypes times
                List<Date> dateValue= new List<Date>();
                Date dt = Date.today();
                Date datevalue1 = dt+30;
    
                Set < String > ids = articletypetoarticles.get(s);
                System.debug('the value of ids' + ids);
                string idstring = '(';
                for (String idv: ids) {
                    idstring = idstring + ',' + '\'' + idv + '\'';
                }
                idstring = idstring.replace('(,', '(');
                idstring = idstring + ')';
                // the below quuery will get the ArticleNumbers and ArticleTypes dynamicaly and check the expiration date
                String query1 = 'Select Id,ArticleNumber,Asset_owner__c,Asset_Owner_lkp__c,Expiration_Date__c, Data_Source_Link__c from ' + s + ' where ArticleNumber IN ' + idstring + ' and PublishStatus = \'Online\' and Language = \'en_US\' and Expiration_Date__c = ' + datevalue1.year() + '-' + ((datevalue1.month() < 10) ? '0' + datevalue1.month() : datevalue1.month() + '') + '-' + ((datevalue1.day() < 10) ? '0' + datevalue1.day() : datevalue1.day() + '');
                system.debug('Results of query1'+query1);
                knowledgearticlemap.put(s, (List < Sobject > ) database.query(query1));
            }
                
            
            Messaging.SingleEmailMessage mail;
            List<String> emailList = new List<String>();
            try { // To catch any exceptions, try block is created
                for (String s: knowledgearticlemap.keySet()) {
                    for (Sobject sob: (List < Sobject > ) knowledgearticlemap.get(s)) {
                        
                        String Asset_Owner  = (String)sob.get('Asset_owner__c');
                        String title1       = knowledgearticleversiondata.get((string) sob.get('ArticleNumber')).Title;                    
                        
                        System.debug('Article title'+title1);
                        System.debug('Asset Owner name'+Asset_Owner);                    
                        emailList = new List<String>();
                      //  emailList.addall(getEmailAddressOfAssetOwnersForAnArticle((String)sob.get('Asset_owner__c')));
                        
                        System.debug('email list to send'+ emailList);
                        System.debug('[kavNames]  emailList'+emailList);
                        
                        mailbody = '<html><body>Dear Employee,<br><br>Article:{title} is expiring in 30 days, on {expiration}.<br><br>Please view the article linked below and confirm if you want to extend the expiration date or modify the existing article.<br><br> You can access the Article directly in Salesforce.com from the URL below:<br>{Link}<br><br> Please send your updates to the <a target="_blank" href="mailto:ctsknowledgemgmt@deloitte.com"> CTS Knowledge Management Team </a>. If you cannot access the URL above please contact the <a target="_blank" href="mailto:ctsknowledgemgmt@deloitte.com"> CTS Knowledge Management Team </a>. <br><br>Thanks for your support!'; 
                        KnowledgeArticleVersion kavrec  = knowledgearticleversiondata.get((string) sob.get('ArticleNumber'));
                        String artid                    = kavrec.KnowledgeArticleId;
                     //   KbManagement.PublishingService.editOnlineArticle(artid, false); // All the articles that satisfy the results of the query1,are updated to Draft using this method
                        validrecordsForUpdate.put((string) sob.get('ArticleNumber'), kavrec);
                        mail = new Messaging.SingleEmailMessage();
                        mail.setUseSignature(false);
                        mail.setToAddresses(emailList);
                        
                        mail.setTargetObjectId((id)sob.get('Asset_Owner_lkp__c'));
                        mail.setSaveAsActivity(false);
                        
                        //mail.setCcAddresses(new String[] {'kpopli@deloitte.com','cpinarouthu@deloitte.com','lvinay_qa@deloitte.com'});
                        mail.setSubject('Article: ' + knowledgearticleversiondata.get((string) sob.get('ArticleNumber')).Title + ' will expire in 30 days');
                        
                        String title = knowledgearticleversiondata.get((string) sob.get('ArticleNumber')).Title;
                        String datasourceurl = URL.getSalesforceBaseUrl().toExternalForm() + label.ArticleOnlineURL+artid;
                        String datasourceur2;
                        
                        if(sob.get('Data_Source_Link__c') != null ){
                            datasourceur2 = String.valueOf(sob.get('Data_Source_Link__c')) ;
                        } else {
                            datasourceur2 = 'N/A(Not Applicable)';
                        }
                            
                        String expirationdate = String.valueOf(sob.get('Expiration_Date__c'));
                        mailbody = mailbody.replace('{title}', title);
                        mailbody = mailbody.replace('{Link}', datasourceurl);
                        mailbody = mailbody.replace('{DataSourceLink}', datasourceur2);                    
                        mailbody = mailbody.replace('{expiration}', expirationdate);
                        mail.setHtmlBody(mailbody);
                        emailmessages.add(mail);
                    }
                }
                list < Messaging.SendEmailResult > results = Messaging.sendEmail(emailmessages);
            }
            catch (Exception ex) {
                // Whenever the batca class fails, the exceptions are captured in an object and from there email will be sent to the login user with the exception reason
                WCT_ExceptionUtility.logException('batchArticleExpNotify', null, ex.getMessage() + ' ::: ' + ex.getStackTraceString());
            }
    }

    /*
        Retrieve AssetOwnerMap<Owner, EmailId> from Asset_Owners_List__c Custom Setting
    */
     private static void getAssetOwnerEmailMap(){
            for(Asset_Owners_List__c awl : Asset_Owners_List__c.getAll().values()){
                assetOwnersMap.put(awl.Asset_Owner__c.toLowerCase(),awl.Email_Address__c);
            }
     }     
     
     /*
        Logic to get
            assetOwnerNames EmailIdSet from Asset_Owners_List__c customSetting
            else set to Default Emails
            
        
     */
     private static set<string> getEmailAddressOfAssetOwnersForAnArticle(String assetOwnerNames){
        set<String> assetEmailsSet = new set<String>();
                    
        if(assetOwnerNames != null){
            //Retrieve assetOwnersMap from AssetOwners Custom Setting if assetOwnersMap is blank
            if(assetOwnersMap.size() == 0)
                getAssetOwnerEmailMap();
                
            for(String owner:assetOwnerNames.toLowerCase().split(';')){
                if(assetOwnersMap.containskey(owner))
                    assetEmailsSet.add(assetOwnersMap.get(owner));
            }
        }
        
        if(assetEmailsSet.size() == 0){
            assetEmailsSet.addall(DEFAULT_EMAIL_LIST);
        }
        system.debug(' assetEmailsSet :'+assetEmailsSet);
        return assetEmailsSet;
    }
}