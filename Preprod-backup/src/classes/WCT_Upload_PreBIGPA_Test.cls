/**
    * Class Name  : WCT_UploadCases_Test
    * Description : This apex test class will use to test the WCT_Upload_PreBIGPA
*/

@isTest
private class WCT_Upload_PreBIGPA_Test {

    public static String taskRecordTypeID= Schema.SObjectType.Task.getRecordTypeInfosByName().get(WCT_UtilConstants.TASK_RECORDTYPE_PRE_BI_GPA).getRecordTypeId();
    public static String caseRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get(WCT_UtilConstants.CASE_RECORDTYPE_PRE_BI_GPA).getRecordTypeId();
    public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
    public static List<Contact> contactList=new List<Contact>();
    public static List<WCT_PreBI_GPA_Stage_Table__c> refList=new List<WCT_PreBI_GPA_Stage_Table__c>();
    public static List<Task> taskList=new List<Task>();
    public static List<Case> CaseList=new List<Case>();
    public static User userRec;
    public static Id profileId=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    public static WCT_Requisition__c req;
    public static Id permissionSetID=[Select id from PermissionSet where name=:WCT_UtilConstants.PRE_BI_GPA_PERMISSION_SET_NAME].Id;
    public static List<Case> CAsList=new List<Case>();
    public static List<Task> TaskList1=new List<Task>();    
    /** 
        Method Name  : createContacts
        Return Type  : List<Contact>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static List<Contact> createContacts()
    {
        contactList=WCT_UtilTestDataCreation.createContactWithCandidate(candidateRecordTypeId);
        insert contactList;
        return contactList;
    }
    /** 
        Method Name  : createReq
        Return Type  : List<Contact>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static WCT_Requisition__c createRequistion()
    {
        req=WCT_UtilTestDataCreation.createRequistion();
        insert req;
        return req;
    }
    /** 
        Method Name  : createuser
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUser()
    {
        userRec=WCT_UtilTestDataCreation.createUser('PBICSV', profileId, 'svelusamy@deloitte.com', 'svelusamy@deloitte.com');
        system.debug('user::'+userRec);
         
        insert userRec;
     
        return  userRec;
    }
    /** 
        Method Name  : permissionSetAssignment
        Return Type  : Void
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static void permissionSetAssignment(Id userId)
    {
        PermissionSetAssignment assignmentUser=new PermissionSetAssignment();
        assignmentUser.AssigneeId=userId;
        assignmentUser.PermissionSetId=permissionSetID;
        insert assignmentUser;
        
    }
    /** 
        Method Name  : createPreBiReferenceRecords
        Return Type  : List<WCT_PreBIQAReferenceTable__c>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static List<WCT_PreBI_GPA_Stage_Table__c> createPreBiGPARecords()
    {
        refList=WCT_UtilTestDataCreation.createPreBIGPAStageTable();
        insert refList;
        
        return refList;
    }
    private static List<Case> createcase(String caseId)
    { 
        //CAsList=WCT_UtilTestDataCreation.createcaseRecord();
         caseList=new List<Case>();
   
    
        Case CaseRec = new Case();
       // CaseRec.Id = caseRecordTypeId;
        //CaseRec.Contact = 'suresh';
         CaseRec.OwnerId = caseId;
        //CaseRec.OwnerId = UserInfo.getUserId();
        CaseRec.OBS__c = 'suresh';
        CaseRec.School_Name__c = 'suresh';
        CaseRec.Out_Of_GPA__c = 5;
        CaseRec.OverAll_GPA__c = 5;
        CaseRec.No_GPA_Comment__c = 'test';
        CaseRec.Status=WCT_UtilConstants.CASE_STATUS_NEW;
        CaseRec.Priority=WCT_UtilConstants.CASE_PRIORITY_MEDIUM;
        CaseRec.Subject=WCT_UtilConstants.CASE_SUBJECT_PREBIGPA;
        CaseRec.WCT_Category__c=WCT_UtilConstants.CASE_CATEFORY;
        CaseRec.WCT_SubCategory1__c=WCT_UtilConstants.CASE_SUBCATEGORY1_CAMPUS;
        CaseRec.WCT_SubCategory2__c=WCT_UtilConstants.CASE_SUBCATEGORY2;
        CaseRec.Origin=WCT_UtilConstants.CASE_ORIGIN;
       
        caseList.add(CaseRec);
        insert caseList;
        return caseList;
       
         
        
    }
    private static List<Task> createTasks()
    {
        TaskList1=WCT_UtilTestDataCreation.createTaskRecord();
        insert TaskList1;
        return TaskList1;
    }
     /** 
        Method Name  : noOfColsException
        Return Type  : void
        Type         : Test Method
        Description  : Test will do on number of column in CSV and send error message on page.         
    */  
    static testMethod void noOfColsException() {
        refList=createPreBiGPARecords();
        contactList=createContacts();
        req=createRequistion();
        String blobCreator = 'RMSId' + '\r\n' + 'ApplicationId' + '\r\n' + 'RecuiterName' + '\r\n' ; 
        PageReference pageRef = Page.WCT_PreBIGPA;
        Test.setCurrentPageReference(pageRef);
        WCT_Upload_PreBIGPA uploadCasesCls=new WCT_Upload_PreBIGPA();
        uploadCasesCls.contentFile=blob.valueof(blobCreator);
        uploadCasesCls.readFile();
        List<Apexpages.Message> msgs = ApexPages.getMessages(); 
        boolean b = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains(Label.Columns_count_not_proper)) b = true;
        }
        system.assert(b);
    }
    
    /** 
        Method Name  : fileNotValid
        Return Type  : void
        Type         : Test Method
        Description  : Test will do when no file will be selected or file is not valid CSV.         
    */  
    static testMethod void fileNotValid() {
        refList=createPreBiGPARecords();
        contactList=createContacts();
        req=createRequistion();
        PageReference pageRef = Page.WCT_PreBIGPA;
        Test.setCurrentPageReference(pageRef);
        WCT_Upload_PreBIGPA uploadCasesCls=new WCT_Upload_PreBIGPA();
        uploadCasesCls.readFile();
        List<Apexpages.Message> msgs = ApexPages.getMessages(); 
        boolean b = false;
        for(Apexpages.Message msg:msgs){
            if (msg.getDetail().contains(Label.Upload_Case_Exception)) b = true;
        }
        system.assert(b);
    }
    
   
    /** 
        Method Name  : readFileMethod
        Return Type  : void
        Type         : Test Method
        Description  : Test will do read file in all cases.         
    */  
   static testMethod void readFileMethod() {
       Test.startTest();
        refList=createPreBiGPARecords();
        system.debug(refList);
        contactList=createContacts();
        req=createRequistion();
        userRec=createUser();
        String csvheader='RMS ID,Last Name,First Name,Middle Name,Email Address,EA Trigger Date,BI Trigger Date,Start Date,Requisition #,Recruiter Name,Recruiter Email Address,Candidate Type,OverallGPA,Out_of__c,No GPA Comment,School Name,OBS';
        String dataRow='1234551,Smith,Joe,Test,Email1@deloitte.com,3/2/2016,3/2/2016,3/2/2016,123456,Test user,svelusamy@deloitte.com,Static,3,4,test,test,test';
        String blobCreator = csvheader + '\r\n' + dataRow ; 
        PageReference pageRef = Page.WCT_PreBIGPA;
        Test.setCurrentPageReference(pageRef);
        WCT_Upload_PreBIGPA uploadCasesCls=new WCT_Upload_PreBIGPA();
        uploadCasesCls.contentFile=blob.valueof(blobCreator);
        uploadCasesCls.readFile();
        CAsList=createcase(userRec.id);
        List<WCT_PreBI_GPA_Stage_Table__c> preBIStageRecordList=uploadCasesCls.getAllStagingRecords();
        //System.assertEquals(0, preBIStageRecordList.Size());
            Test.stopTest();
    }
    
   
  /**
        Method Name  : readDataException
        Return Type  : void
        Type         : Test Method
        Description  : Test will do read file in all cases with Data exception.         
    */  
    static testMethod void readDataException() {
        Test.startTest();
        userRec=createUser();
        refList=createPreBiGPARecords();
        contactList=createContacts();
        req=createRequistion();
        
        String csvheader='RMS ID,Last Name,First Name,Middle Name,Email Address,EA Trigger Date,BI Trigger Date,Start Date,Requisition #,Recruiter Name,Recruiter Email Address,Candidate Type,OverallGPA,Out_of__c,No GPA Comment,School Name,OBS';
        String dataRow='1234551,Smith,Joe,Test,Email1@deloitte.com,3/2/2016,3/2/2016,3/2/2016,123456,Test user,svelusamy@deloitte.com,Static,3,4,test,test,test';
        String blobCreator = csvheader + '\r\n' + dataRow ; 
        System.runAs(userRec)
        {
            permissionSetAssignment(userRec.id);
            PageReference pageRef = Page.WCT_PreBIGPA;
            Test.setCurrentPageReference(pageRef);
            WCT_Upload_PreBIGPA uploadCasesCls=new WCT_Upload_PreBIGPA();
            uploadCasesCls.contentFile=blob.valueof(blobCreator);
            uploadCasesCls.readFile();
            
            List<WCT_PreBI_GPA_Stage_Table__c> preBIStageRecordList=uploadCasesCls.getAllStagingRecords();
            
            
            //System.assertEquals(0, preBIStageRecordList.Size());
        } 
        
         
        Test.stopTest();
    }
}