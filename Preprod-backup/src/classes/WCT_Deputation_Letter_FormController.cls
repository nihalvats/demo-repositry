/********************************************************************************************************************************
Apex class         : <WCT_Deputation_Letter_FormController>
Description        : <Controller which allows to Update Mobility and Assignment>
Type               :  Controller
Test Class         : <WCT_Deputation_Letter_FormCntrler_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 25/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/ 
public with sharing class WCT_Deputation_Letter_FormController extends SitesTodHeaderController{

 //PUBLIC VARIABLES
 public WCT_Mobility__c MobilityRecord{get;set;}
 
 //UPLOAD RELATED VARIABLES
  
 public Document doc {get;set;}
 public List<String> docIdList = new List<string>();
 public task t{get;set;}
 public String taskid{get;set;}
 public List<Attachment> listAttachments {get; set;}
 public Map<Id, String> mapAttachmentSize {get; set;} 
 public Integer countattach{get;set;}

 // ERROR MESSAGE RELATED VARIABLES
    
 public boolean pageError {get; set;}
 public String pageErrorMessage {get; set;}
 public String supportAreaErrorMesssage {get; set;}   
 public GBL_Attachments attachmentHelper{get; set;}
 public WCT_Task_ManageHandler taskInstance {get;set;}
 public boolean validPage {get;set;}
   
 
 // DEFINING A CONSTRUCTOR    
 public WCT_Deputation_Letter_FormController() {  
    
     taskInstance = new WCT_Task_ManageHandler();
     attachmentHelper= new GBL_Attachments();   
     init();
     if(taskInstance.isError){
        validPage = false;
          }
          else {
           validPage = true;
        }

       getAttachmentInfo();
}

/********************************************************************************************
*Method Name         : <init()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Init() Used for loading Mobility and Attachments>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 
  
  private void init(){

       MobilityRecord= new WCT_Mobility__c (); 
       countattach = 0;
       doc = new Document();
       listAttachments = new List<Attachment>();
        mapAttachmentSize = new Map<Id, String>();
  }   

/********************************************************************************************
*Method Name         : <getAttachmentInfo()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetAttachmentInfo() Used for Retrieving Attachment Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     

    @TestVisible
    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId , 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId = :taskInstance.taskID 
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50 ];    
              
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                //SIZE GREATAR THAN 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                //SIZE GREATER THAN 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }


/********************************************************************************************
*Method Name         : <save()>
*Return Type         : <PageReference>
*Param’s             : 
*Description         : <Save() Used for for Updating Task Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 

   public pageReference save()
    { 
           try{
           taskInstance.saveTask();
           taskInstance.taskObj.OwnerId=UserInfo.getUserId();
           upsert taskInstance.taskObj;
           
           if(attachmentHelper.docIdList.isEmpty()) {
                pageErrorMessage = 'Attachment is required to submit the form.';
                pageError = true;
                return null;
    
            }
            
           // getAttachmentInfo();
            attachmentHelper.uploadRelatedAttachment(taskInstance.taskID);
            }
             catch (Exception e) {
                 Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Deputation_Letter_FormController', 'US Deputation Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
                
                Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_DEP_EXP&expCode='+errLog.Name);
                pg.setRedirect(true);
                return pg;
            
            }
            PageReference pageRef = new PageReference('/apex/WCT_Deputation_Letter_FormThankYou?taskid='+taskInstance.taskID);
            pageRef.setRedirect(true);
            return pageRef;  
    
        }
}