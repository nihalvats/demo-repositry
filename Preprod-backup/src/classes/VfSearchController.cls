public class VfSearchController{
Public WCT_Article_Types__c wctarticles; 
    //Page Size
    private Static Final Integer PAGE_NUMBER = 10;
    //Search String used in ArticleList tag
    public String searchstring { get; set; }
    //Is new List reqd
    private boolean isRefRequired = true;
    //Exclude filter criteria for UI only
    private static final String EXCLUDE_CRITERIA_FILTER = 'All';
    //Keeps track of current page & max size of article list
    Integer currentPage = 1;
    Integer maxSize = 1;
    public string categoryFilter = '';
    ///
    public string selectedArtTypes {get;set;}
    public list<string> articleTypeList = new list<string>();
    public list<string> articletypes = new list<string>();
    public list<selectOption> myOptions {get;set;}

   // public list<string> checkboxSelections {get;set;} 
    public List<KnowledgeArticleVersion> articleList {get;set;}
    public List<ArticleTable> lstArticleTable {get;set;}
    public List<selectOption> lstEditOptions  {get;set;}
    public boolean isEditError {get;set;}
    public string categoryType;   
    String ArtType = null;
    public String getArtType() {
        return ArtType;
    }

public void setArtType(String ArtType) { this.ArtType = ArtType; } 

    public List<SelectOption> getItems() {
List<SelectOption> options = new List<SelectOption>(); 

selectedArtTypes = '';
      //  WCT_Knowledge_Tree__c kt = WCT_Knowledge_Tree__c.getValues('AvailableArticleTypes');
       WCT_Article_Types__c kt = WCT_Article_Types__c.getValues(UserInfo.getProfileId());
        string availArticleType = kt.ArticleTypes__c;
        if(availArticleType != null && availArticleType.length()>0){
        
            articletypes = availArticleType.split(',');
            articletypes.sort();
        }
        if(!articletypes.isEmpty()){
            articleTypeList.addAll(articletypes);
        }
        myOptions = new list<selectOption>();
        for(string s: articleTypeList ){
            s = s.removeEnd('__kav');
            s= s.replace('_',' ');
            options.add(new selectOption(s,s)); 
            
            system.debug('arttyp'+s);
        }

return options; 
}


//------------------------------------------------------      
//    Wrapper class for ArticleTable   
//------------------------------------------------------
    public class ArticleTable{
        public string ArticleId {get;set;}
        public string ArticleNumber {get;set;}
        public string Title {get;set;}
        public boolean US {get;set;}
        public boolean USI {get;set;}
        public boolean USPSN {get;set;}    
        public string PubStatus {get;set;}   
        public string EditArchive {get;set;}
        public string ArticleType {get;set;}
        public datetime LastPublishedDate {get;set;}
    }
    
//--------------------------
//    Constructor
//--------------------------    
    public VfSearchController()
    {
        isEditError = false;
        lstEditOptions = new List<selectOption>();
        lstEditOptions.add(new SelectOption('', '--None--'));
        lstEditOptions.add(new SelectOption('Archive', 'Archive'));
        lstEditOptions.add(new SelectOption('Edit', 'Edit'));
        wctarticles = WCT_Article_Types__c.getInstance(UserInfo.getProfileId());
       /* checkboxSelections = new list<string>();
        selectedArtTypes = '';
        WCT_Knowledge_Tree__c kt = WCT_Knowledge_Tree__c.getValues('AvailableArticleTypes');
        string availArticleType = kt.ArticleTypes__c;
        if(availArticleType != null && availArticleType.length()>0){
        
            articletypes = availArticleType.split(',');
            articletypes.sort();
        }
        if(!articletypes.isEmpty()){
            articleTypeList.addAll(articletypes);
        }
        myOptions = new list<selectOption>();
        for(string s: articleTypeList ){
            myOptions.add(new selectOption(s,s)); 
        }
        */
        
        
    }
//----------------------------------------    
//    Returns array of Category Groups
//----------------------------------------
    public DataCategoryGroupInfo[] getDataCategoryGroupInfo() {
        //return DataCategoryUtil.getInstance().getAllCategoryGroups();
        list<DataCategoryGroupInfo> dcg = DataCategoryUtil.getInstance().getAllCategoryGroups();
        list<string> groupNamesList = new List<string>();
        list<DataCategoryGroupInfo> dcg1 = new list<DataCategoryGroupInfo>();
        for(DataCategoryGroupInfo d : dcg){
            groupNamesList.add(d.Name);
        }
        groupNamesList.sort();
        for(string s:groupNamesList){
            for(DataCategoryGroupInfo d1 : dcg){
                if(s == d1.name){
                    dcg1.add(d1);
                }
            }
        }
        return dcg1;
    }
//---------------------------------------------------------------    
//    Returns category keyword required to filter articleList.
//---------------------------------------------------------------
    public String getCategoryKeyword() {
    categoryFilter ='';
        DataCategoryGroupInfo[] categoryGroups =
        DataCategoryUtil.getInstance().getAllCategoryGroups();
        String categoryCondition = '';
        for (DataCategoryGroupInfo categoryGroup : categoryGroups) {
            String selectedCategoryName =
            System.currentPageReference().getParameters().Get('categoryType_'+categoryGroup.getName());
            if(selectedCategoryName != null && !selectedCategoryName.equals('NoFilter')) {
                if(categoryCondition=='' && selectedCategoryName != null){
                    categoryCondition=categoryCondition+categoryGroup.getName() + ':' +
                    System.currentPageReference().getParameters().Get('categoryType_'+categoryGroup.getName());
                }else {
                    categoryCondition=categoryCondition + ',' +categoryGroup.getName() + ':' +
                    System.currentPageReference().getParameters().Get('categoryType_'+categoryGroup.getName());
                }
            }
        }
        
        for (DataCategoryGroupInfo categoryGroup : categoryGroups) {
            categoryType =
            System.currentPageReference().getParameters().Get('categoryType_'+categoryGroup.getName());
            if(categoryType != null && !categoryType.equals('NoFilter')) {
                if(categoryFilter == ''){
                    categoryFilter = categoryGroup.getName() + '__c ABOVE_OR_BELOW ' + categoryType +'__c';
                } else {
                    categoryFilter = categoryFilter + ' AND ' + categoryGroup.getName() +'__c ABOVE_OR_BELOW ' + categoryType +'__c';
               system.debug('***filter'+categoryFilter);

                }
                 system.debug('***group'+categoryGroup.getName() );
                 system.debug('***filter'+categoryType);

            }
        }
        system.debug('***jsn'+categoryCondition);
        system.debug('####'+categoryFilter);
        try {
            if(categoryFilter.length()>0) {
                if(searchString != null && searchString.length() >0 ) {
                String searchquery = 'FIND \'' + searchString + '*\'IN ALL FIELDS RETURNING KnowledgeArticleVersion(Id, title, UrlName, LastPublishedDate,LastModifiedById where PublishStatus =\'online\'  AND IsLatestVersion=true and Language = \'en_US\') WITH DATA CATEGORY '+categoryFilter ;
                List<List<SObject>>searchList = search.query(searchquery);
                List<KnowledgeArticleVersion> articleList = (List<KnowledgeArticleVersion>)searchList[0];
                maxSize = articleList.size() ;
                // maxSize = maxSize.divide(PAGE_NUMBER,2,System.RoundingMode.UP);
                } else {
                String qryString = 'SELECT Id, title, UrlName, LastPublishedDate,LastModifiedById FROM KnowledgeArticleVersion WHERE (PublishStatus = \'online\'  AND IsLatestVersion=true and Language = \'en_US\') WITH DATA CATEGORY '+categoryFilter;
                articleList= Database.query(qryString);
                maxSize = articleList.size() ;
                // maxSize = maxSize.divide(PAGE_NUMBER,2,System.RoundingMode.UP);
                }
            } else {
                system.debug('entered####');
                String qryString = 'SELECT Id, title, UrlName, LastPublishedDate,LastModifiedById FROM KnowledgeArticleVersion WHERE (PublishStatus = \'online\'  AND IsLatestVersion=true and Language = \'en_US\')';
                articleList= Database.query(qryString);
                maxSize = articleList.size() ;
                // maxSize = maxSize.divide(PAGE_NUMBER,2,System.RoundingMode.UP);
                system.debug('####'+articleList);
                
            }
            selectedArtTypes = '';
           // system.debug('@@@@1'+checkboxSelections);
          /*  
            if(!checkboxSelections.isEmpty()){
             system.debug('##ksn##'+checkboxSelections);

                //selectedArtTypes = '';
                set<string> tempstring = new set<string>();
                tempstring.addAll(checkboxSelections);
                for(string s : tempstring){
                selectedArtTypes += s +',';
                }
            }*/
        }
        catch(Exception e) {
            Apexpages.addmessages( e );
        }
        if(categoryFilter =='') {
            // maxSize = 0;
            categoryCondition = '' ;
        }
        return categoryCondition;
    }
    
    // Action call when the new list needs to be fetched
    public PageReference refreshSearchResult() {
        selectedArtTypes = '';
        //system.debug('@@@@'+checkboxSelections);
       /* if(!checkboxSelections.isEmpty()){
            //selectedArtTypes = '';
            set<string> tempstring = new set<string>();
            tempstring.addAll(checkboxSelections);
            for(string s : tempstring){
                selectedArtTypes += s +',';
            }
        }*/
        maxSize = currentPage = 1;
        return null;
    }
    
//-----------------------------------------------------
//    Search Articles
//-----------------------------------------------------
   Boolean blnUSFlag = false ;
   Boolean blnUSIFlag = false;
   Boolean blnPSNFlag = false;
   string strParentID='';
   List<KnowledgeArticleVersion> lstArticle ;
   List<KnowledgeArticleVersion> lstArticleDraft = new List<KnowledgeArticleVersion>();
   Map<String,KnowledgeArticleVersion> kavDraftMap = new Map<String,KnowledgeArticleVersion>();
   string searchDraftQuery ;
    public void search(){
    
   
    // string strAT = ArtType.removeEnd('__kav');  
    system.debug('*test*'+ArtType);
    if(ArtType=='' || ArtType==null)
    {
                 ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Select At least one article type to search.'));
                  isEditError = true;
                 return ;
    }
    ArtType = ArtType.replace(' ','_');
   String str = 'Select c.id,c.ParentId, c.DataCategoryName From '+ArtType+'__DataCategorySelection c';
    //Map<String, String> mpDC = new Map<String, String>();
    Map<String, List<String>> mpDC = new Map<String, List<String>>();
    system.debug('^^^'+str);
     List<sObject> lst = DataBase.query(str) ;
     if(!lst.IsEmpty()){ 
        for(sObject cd : lst)
       { 
            if(mpDC.containsKey(string.valueOf(cd.get('ParentID')))){
                list<string> tempStringList = new list<string>();
                tempStringList.addAll(mpDC.get(string.valueOf(cd.get('ParentID'))));
                tempStringList.add(string.valueOf(cd.get('DataCategoryName')));
                mpDC.put(string.valueOf(cd.get('ParentID')),tempStringList);
            }else{
                list<string> tempStringListEl = new list<string>();
                tempStringListEl.add(string.valueOf(cd.get('DataCategoryName')));
                mpDC.put(string.valueOf(cd.get('ParentID')),tempStringListEl);
            }
        } 
    }

  /*  List<List<sObject>> myQuery = search.query(str) ;   
    Map<String, String> mpDC = new Map<String, String>();
    if(!lst.IsEmpty()){    
        for(sObject cd : lst)
        {        
            mpDC.put(cd.ParentID,cd.DataCategoryName);
        }   
     }
     */
    system.debug('*test*');
    getCategoryKeyword();
        system.debug('*test123*');
    //system.debug('**'+ checkboxSelections);
    lstArticleTable = new List<ArticleTable>();
    ArticleTable article;
///----------------Search box    
    //searchstring = '%' + searchstring + '%';
    // and title like :searchstring --- add this in the searchquery
///----------------Search box    
    //system.debug('*tsn*'+checkboxSelections+'**');
    string strCat = ' WITH DATA CATEGORY '+categoryFilter;
    String searchquery = '';
    searchDraftQuery = '';
     ArtType = ArtType+'__kav';
    ArtType = ArtType.replace(' ','_');
    
   
    if(categoryFilter!='')
    {    searchquery ='SELECT PublishStatus,Id, KnowledgeArticleId, ArticleNumber,ArticleType, title, LastPublishedDate FROM KnowledgeArticleVersion WHERE PublishStatus = \'online\' AND IsLatestVersion=true and Language = \'en_US\' and ArticleType =:ArtType'+strCat;
         searchDraftQuery = 'SELECT PublishStatus,Id, KnowledgeArticleId, ArticleNumber,ArticleType, title, LastPublishedDate FROM KnowledgeArticleVersion WHERE PublishStatus = \'Draft\' AND IsLatestVersion=true and Language = \'en_US\' and ArticleType =:ArtType'+strCat;
         }
    else
    {  searchquery ='SELECT PublishStatus,Id, KnowledgeArticleId, ArticleNumber,ArticleType, title, LastPublishedDate FROM KnowledgeArticleVersion WHERE PublishStatus = \'online\' AND IsLatestVersion=true and Language = \'en_US\' and ArticleType =:ArtType';
       searchDraftQuery = 'SELECT PublishStatus,Id, KnowledgeArticleId, ArticleNumber,ArticleType, title, LastPublishedDate FROM KnowledgeArticleVersion WHERE PublishStatus = \'Draft\' AND IsLatestVersion=true and Language = \'en_US\' and ArticleType =:ArtType';}
      //else
      //{ searchquery = 'SELECT Title FROM KnowledgeArticleVersion WHERE PublishStatus=\'Draft\' AND IsLatestVersion=true and language =\'en_US\' and ArticleType =:ArtType'; 
      //}
system.debug('*qqqq*'+searchquery+'**');

//    for(string tmpArticleType : checkboxSelections){
      // if(checkboxSelections.size()>0){
       lstArticle =DataBase.query(searchquery) ; 
       lstArticleDraft = DataBase.query(searchDraftQuery) ;
       if(!lstArticleDraft.isEmpty()){
        for(KnowledgeArticleVersion kav : lstArticleDraft){
            kavDraftMap.put(kav.ArticleNumber,kav);
        }      
       }
            system.debug('**'+ lstArticle );            
            for(KnowledgeArticleVersion tmpKA : lstArticle ){
                article = new ArticleTable();
                article.ArticleId = tmpKA.KnowledgeArticleId;
                article.ArticleNumber = tmpKA.ArticleNumber;
                article.Title = tmpKA.Title;
                if(kavDraftMap.containsKey(tmpKA.ArticleNumber)){
                    article.PubStatus = 'Draft';
                }else{
                //article.PubStatus = tmpKA.PublishStatus;
                
                    article.PubStatus = 'Published';
                }               
                system.debug('*qqqq*'+'PubStatus');
                string strMP='';
                if(mpDC.ContainsKey(tmpKA.Id))
                {
                //strMP = mpDC.get(tmpKA.Id);
                //system.debug('--mp--'+strMP);
                    for(string s: mpDC.get(tmpKA.Id)){
                        if(s == 'US')
                         {
                             blnUSFlag=true;
                           article.us = true;
                         } 
                         else if(s == 'India')
                         {
                             blnUSIFlag=true;
                           article.USI = true;
                         }
                         else if(s  == 'US_PSN')
                        {
                             blnPSNFlag=true;
                             article.USPSN = true;
                        }  
                       /*else 
                       {
                       article.us = False;
                       article.USI = false;
                       article.USPSN = false;
                        }*/                      
                    }                  
                }       
                 if(article.us==null){
                    article.us = false;
                }if(article.USI==null){
                    article.USI= false;
                }if(article.USPSN==null){
                    article.USPSN = false;
                }
                
               /* if(article.PubStatus==draft)
               {
               
               }*/
                article.ArticleType = tmpKA.ArticleType;
                
                article.LastPublishedDate = tmpKA.LastPublishedDate;
                
                if(article.PubStatus =='Published')
                {                
                    lstArticleTable.add(article);
                }                    
            }
               
                       
        //}
//    }
    system.debug('**'+ lstArticleTable);
  }
  
//-----------------------------------------------------
//    Save Article Status
//-----------------------------------------------------  

    public static sObject createObject(String typeName) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        if (targetType == null) {
            // throw an exception
        }
        
        // Instantiate an sObject with the type passed in as an argument
        //  at run time.
        return targetType.newSObject(); 
    }
  public PageReference saveArticleStatus(){
  system.debug('--USIPSN--'+blnUSFlag+'-'+blnUSIFlag+'-'+blnPSNFlag);
      integer editCount = 0;
      integer archiveCount = 0;
      string articleToEditId;
      set<String> setArticleIds = new set<String>();
      list<ArticleTable> attliclesToArchive = new list<ArticleTable>();
      list<ArticleTable> artliclesToEdit = new list<ArticleTable>();
      Boolean usFlag = false;
      Boolean IndFlag = false;
      Boolean PSNFlag = false;
      
      for(ArticleTable article : lstArticleTable){
          if(article.EditArchive=='Edit' ){
              editCount++;
              //articleToEditId = article.ArticleId;
              artliclesToEdit.add(article);
              if(editCount == 2){
                  ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Only one article should be selected for Editing'));
                  isEditError = true;
                  return null;
              }
             //articleToEditId =  
               //KbManagement.PublishingService.editOnlineArticle(articleToEditId, true); 
                   
            }
          if(article.EditArchive=='Archive'){
              archiveCount++;
              if(article.US!=null && article.US){
                usFlag = true;
                 ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'At least one article should be selected for Editing'));
                 //return null;
              }
              if(article.USI!=null &&  article.USI ){
                IndFlag = true;
                 ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'At least one article should be selected for Editing'));
                 //return null;
              }
              if(article.USPSN!=null && article.USPSN ){
                PSNFlag = true;
                 ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'At least one article should be selected for Editing'));
              //return null;
              }
              system.debug('**'+ article.ArticleId);
              attliclesToArchive.add(article);
             //KbManagement.PublishingService.archiveOnlineArticle(article.ArticleId , null);
              //search();
          }
          system.debug('**'+ editCount +'**'+ archiveCount);
      }
      
      if(editCount==0)
      {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'At least one article should be selected for Editing'));
          isEditError = true;
          return null;
      }
       if(archiveCount==0)
      {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'At least one article should be selected for Archiving'));
          isEditError = true;
          return null;
      }
      if(editCount==1){
        if(!attliclesToArchive.isEmpty()){
            for(ArticleTable atw : attliclesToArchive){
                KbManagement.PublishingService.archiveOnlineArticle(atw.ArticleId , null);      
            }
        }
        //Inserting dataCategorySelection
        String id = KbManagement.PublishingService.editOnlineArticle(artliclesToEdit[0].ArticleId, false);
        string strAT = artliclesToEdit[0].ArticleType.removeEnd('__kav');
        strAT += '__DataCategorySelection';
        system.debug(strAT);
        if(usFlag){
            if(!artliclesToEdit[0].US){
                                
                sobject artDS = createObject(strAT);
                //FAQ__DataCategorySelection fdc = new FAQ__DataCategorySelection();
                artDS.put('DataCategoryGroupName','KMCallCenter');
                artDS.put('DataCategoryName','US');
                artDS.put('parentId',id);
               
                insert artDS;
            }
        }
        if(IndFlag){
           if(!artliclesToEdit[0].USI){
                sobject artDSInd = createObject(strAT);
                artDSInd.put('DataCategoryGroupName','KMCallCenter');
                artDSInd.put('DataCategoryName','India');
                artDSInd.put('parentId',id);
                insert artDSInd;
           }
        }
        if(PSNFlag){
          if(!artliclesToEdit[0].USPSN){
                sobject artDSPSN = createObject(strAT);
                artDSPSN.put('DataCategoryGroupName','KMCallCenter');
                artDSPSN.put('DataCategoryName','US_PSN');
                artDSPSN.put('parentId',id);
               
                insert artDSPSN;
            }
        }
        
        //articleToEditId = KbManagement.PublishingService.editOnlineArticle(artliclesToEdit[0].ArticleId, false);
        system.debug('testing ids'+id+'**'+artliclesToEdit[0].ArticleId.substring(0,15));
        //KbManagement.PublishingService.publishArticle(id, true);
        //system.debug('testing ids'+id+'**'+artliclesToEdit[0].ArticleId.substring(0,15));
        //PageReference pageRef = new PageReference('/knowledge/publishing/articleEdit.apexp?id='+id);
        
         PageReference pageRef = new PageReference('/knowledge/publishing/articleEdit.apexp?id='+artliclesToEdit[0].ArticleId.substring(0,15) );
          pageRef.setRedirect(true);
          return pageRef;
      }
      else if(archiveCount > 0){
          if(!attliclesToArchive.isEmpty()){
            for(ArticleTable atw : attliclesToArchive){
                KbManagement.PublishingService.archiveOnlineArticle(atw.ArticleId , null);      
            }
        }
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Selected articles archived.'));          
          return null;
      }
      isEditError = false;
      return null;
   }


}