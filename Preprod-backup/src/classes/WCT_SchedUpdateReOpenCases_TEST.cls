/*****************************************************************************************
    Name    : WCT_SchedUpdateReOpenCases_TEST
    Desc    : Test class for WCT_schedUpdateReOpenCases class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                  Date            Description
---------------------------------------------------------------------------
  Deloitte                 12/11/2013         Created 
******************************************************************************************/
@isTest 
private class WCT_SchedUpdateReOpenCases_TEST {
    /****************************************************************************************
    * @author      - Deloitte
    * @date        - 12/11/2013
    * @description - Test Method for  WCT_schedUpdateReOpenCases schedule class
    *****************************************************************************************/
    static testMethod void testSched_batchUpdateReOpenCases(){
        //Starts Testing
        List<Case> casToUpdate = new List<case>();
        Test_Data_Utility.createCase();
        for(Case ca : [Select id,status,Unread_Email_on_Closed_Case__c from case]){
            ca.status = 'In Progress';
            ca.Unread_Email_on_Closed_Case__c = true;
            casToUpdate.add(ca);
        }
        if(!casToUpdate.isEmpty()){
        update casToUpdate;
        EmailMessage em = new EmailMessage(FromAddress = userInfo.getUserEmail(), Incoming = True, ToAddress= 'rala@2st7vyr4mxpe8rugztg4f6jve5dhm1je1m6wa6brrmcn758nrb.e-5ntbeea0.cs15.case.sandbox.salesforce.com',
                status='0',Subject = 'Testing trigger', TextBody = '23456 ',parentId = casToUpdate[0].id);
        insert em;
        EmailMessage emTemp = new EmailMessage(FromAddress = userInfo.getUserEmail(), Incoming = True, ToAddress= 'rala@2st7vyr4mxpe8rugztg4f6jve5dhm1je1m6wa6brrmcn758nrb.e-5ntbeea0.cs15.case.sandbox.salesforce.com',
                status='1',Subject = 'Testing trigger', TextBody = '23456 ',parentId = casToUpdate[1].id);
        insert emTemp ;
        }
        
        test.startTest();
        WCT_schedUpdateReOpenCases s = new WCT_schedUpdateReOpenCases();
        system.schedule('Last Batch Sent','0 0 2 1 * ?',s);
        test.stopTest();
    }     
}