/********************************************************************************************************************************
Apex class         : Contact_Update_Batch
Description        : Updation of user managemnet filed by this batch
Type               :  Batch 
Test Class         :  Test_UserCreation

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01                 Deloitte                   27/06/2015          80%                         <Req / Case #>                  Original Version
************************************************************************************************************************************/

global class Contact_Update_Batch implements Database.Batchable<sObject>{
   List<Exception_Log__c> excp=new List<Exception_Log__c>();//holds the exception log records
    //querying the record type 'employee' of contact
    
    global Id contid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
    global final String Query;
    global Contact_Update_Batch(string q){
        Query=q;
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
         
        
        return Database.getQueryLocator(query);    
     }
     global void execute(Database.BatchableContext BC,List<contact> names) {
          /*Logic to update the feild 'User_Management_category__c' of type checkbox in contact record  
          This logic will update the feild to false
           This is used to limit the number of records returned by the query*/
          For(Contact em:names){
              em.User_Management_category__c=false;
          }
        
          //holds the record type id of exception object to log the failures.
        Schema.DescribeSObjectResult Cas1 = Exception_Log__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames1 = Cas1.getRecordTypeInfosByName();// getting the record Type Info
        Id exceptionid =rtMapByNames1.get('Exception').getRecordTypeId();//particular RecordId by  Name
         if (names.size()>0){
            System.debug('Updating the contact');
            Database.SaveResult[] updcnList = Database.update(names, false);
            for (Integer i=0;i<updcnList.size();i++ ) {
                if(updcnList.get(i).isSuccess()){
                    //  system.debug('contact id is is'+result.getId());
                }
                else {
                    
                    for (Database.Error err : updcnList.get(i).getErrors()){
                        string msg='Error while updating field in the contact at: '+ names.get(i)+':'+ err.getStatusCode() + ' :' + err.getMessage();
                        // system.debug('error is'+msg);
                        excp.add(new Exception_Log__c(className__c='User Creation',pageName__c='Batch Class',Detailed_Exception__c=msg,Exception_Date_and_Time__c=system.now(),RecordTypeId=exceptionid,Running_User__c = UserInfo.getUserId()));
                        
                    }
                }
            }
        }   
     }
     global void finish(Database.BatchableContext BC) {
         if(excp.size()>0){
          insert excp;
         }
     }
}