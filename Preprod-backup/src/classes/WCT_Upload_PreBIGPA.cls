public class WCT_Upload_PreBIGPA {
        
        public String nameFile{get;set;}
        public Blob contentFile{get;set;}
        public Integer noOfRows{get;set;}
        public List<string> recruiterMailid = new List<String>(); 
        public Integer noOfRowsProcessed{get;set;}
        public Integer noOfError{get;set;}
        public List<WCT_PreBI_GPA_Stage_Table__c> GPAcaseList;
        public List<List<String>> fileLines = new List<List<String>>();
        List<WCT_PreBI_GPA_Stage_Table__c> stagingRecordsList;   
        String caseRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get(WCT_UtilConstants.CASE_RECORDTYPE_PRE_BI_GPA).getRecordTypeId();
        public Set<Id> stagingIds=new Set<Id>(); // Store all StagingIds after reading from file.
        public Set<Id> CaseIds=new Set<Id>(); //Store all CaseIds after creating case.    
        //public String versionNo = 'GPA';
        String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
        String EmpRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
        String candidateAdhocRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Candidate Ad Hoc').getRecordTypeId();
        Map<String,String> oldCaseTeleoMapping=new Map<String,String>();
        Set<String> rmsIds=new Set<String>();
        Set<String> RequistionID=new Set<String>();
        Set<String> candEmailIds=new Set<String>();
        Map<String,Id> recruiterEmailMapping=new Map<String,Id>(); 
        Set<String> recruiterEmailIds=new Set<String>();
        Map<String,Id> candidateTeleoMapping=new Map<String,Id>();
        public String taskRecordTypeID;
        Map<String,String> oldCaseMapping=new Map<String,String>();
        Map<String,String> oldCaseIdMapping=new Map<String,String>(); 
        Map<String,Id> candidateEmailMapping=new Map<String,Id>();
        Set<Id> reqIds=new Set<Id>();
        List<String> conNames;
        Set<Id> contactIds=new Set<Id>();
        Map<String,WCT_Requisition__c> requisitionMapping=new Map<String,WCT_Requisition__c>();
        Map<Id,List<WCT_Candidate_Requisition__c>> candidateTrackerMapping=new Map<Id,List<WCT_Candidate_Requisition__c>>(); 
        WCT_parseCSV parseCSVInstance = new WCT_parseCSV();
        /** 
        Method Name  : WCT_UploadPreBIGPACases
        Return Type  : Void
        Description  : Constructor        
        */
        public WCT_Upload_PreBIGPA()
        {
          
        }
        
        
        /** 
        Method Name  : readFile
        Return Type  : PageReference
        Description  : Read CSV and push records into staging table        
        */
        public Pagereference readFile()
        {    
        stagingRecordsList = new List<WCT_PreBI_GPA_Stage_Table__c>(); 
        noOfRowsProcessed=0;
        try{
        stagingRecordsList= mapStagingRecords();
        }
        Catch(System.StringException stringException)
        {
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_String_Exception_Error);
        ApexPages.addMessage(errormsg);    
        }
        Catch(System.ListException listException)
        {   
        system.debug('@#@#@'+listException); 
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Columns_count_not_proper);
        ApexPages.addMessage(errormsg); 
        }
        Catch(Exception e)
        {
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_Exception);
        ApexPages.addMessage(errormsg);    
        }
        
        if(stagingRecordsList.size()>Limits.getLimitQueryRows())
        {
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Error_Data_File_too_large + Limits.getLimitQueryRows());
        ApexPages.addMessage(errormsg);   
        }
        Database.SaveResult[] srList = Database.insert(stagingRecordsList, false);
        
        for(Database.SaveResult sr: srList)
        {
            if(sr.IsSuccess())
            {   
            stagingIds.add(sr.getId());
            system.debug('srlist::'+sr.getId());
            noOfRowsProcessed=stagingIds.size();
            
            }
            else if(!sr.IsSuccess())
            {
            for(Database.Error err : sr.getErrors())
            {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.WCT_Data_Error+err.getMessage());
            ApexPages.addMessage(errormsg);       
            }
            }
        }   
     //   createCase(stagingIds);
 
        List<Case> casesToCreate = new List<Case>();
        system.debug('ids::'+stagingIds); 
        
        List<WCT_PreBI_GPA_Stage_Table__c> GPAcaseList = [SELECT Id,Name,OwnerId,Candidate_E_mail__c,Candidate_First_Name__c,
                                                   Candidate_Last_Name__c,Case_Creation__c,Recruiter_E_mail__c,
                                                   OverallGPA__c,Recruiter_Name__c,Requisition__c,Taleo_RMS_ID__c,EA_Trigger_Date__c,BI_Trigger_Date__c,OBS__c,Out_of__c,School_Name__c
                                                   FROM WCT_PreBI_GPA_Stage_Table__c WHERE ID IN: stagingIds AND Case_Creation__c = true ];
        
        
   
         // List<WCT_PreBI_GPA_Stage_Table__c> GPAcaseList = [SELECT Id,Name,Recruiter_E_mail__c,OverallGPA__c FROM WCT_PreBI_GPA_Stage_Table__c WHERE ID IN: stagingIds AND Case_Creation__c = true];
        
        system.debug('listgpa::'+GPAcaseList.size());   
   
        if(GPAcaseList.size() > 0)
        {
        for( WCT_PreBI_GPA_Stage_Table__c  staginglist9 :GPAcaseList)
        {
        system.debug('stagelist::'+staginglist9); 
        if(staginglist9.Recruiter_E_mail__c != null)
        {
         recruiterMailid.add(staginglist9.Recruiter_E_mail__c);
        system.debug('recemail_lp::'+recruiterMailid); 
        system.debug('recemail_list::'+staginglist9.Recruiter_E_mail__c); 
        }
        if(staginglist9.Taleo_RMS_ID__c!=null) 
        {
                rmsIds.add(staginglist9.Taleo_RMS_ID__c);
                system.debug('taleoID_lp::'+rmsIds);
        }
        if(staginglist9.Candidate_E_mail__c!=null) 
        {
                candEmailIds.add(staginglist9.Taleo_RMS_ID__c);
                system.debug('taleoID_lp::'+candEmailIds);
        }
        if(staginglist9.Requisition__c!=null) 
        {
                RequistionID.add(staginglist9.Requisition__c);
                system.debug('taleoID_lp::'+RequistionID);
        }
                     
        }
        
        }
        List<User> userRecord= [Select Id,Email from User where Email IN :recruiterMailid and IsActive=true limit:Limits.getLimitQueryRows()];
        
        For(User cc :userRecord){
        
        recruiterEmailMapping.put((cc.Email).toLowerCase(),cc.Id);
        
        }  
        system.debug('rmsIds111::'+rmsIds);      
        List<Contact> contactRecord= [Select Id,Email,WCT_Taleo_Id__c from Contact where ( WCT_Taleo_Id__c IN:rmsIds )
        and (RecordTypeID=:candidateRecordTypeId or RecordTypeId=:EmpRecordTypeId) limit:Limits.getLimitQueryRows()];
        system.debug('contactRecord::'+contactRecord);
        For(Contact cct :contactRecord){
        system.debug('cct ::'+cct );    
        candidateTeleoMapping.put((cct.WCT_Taleo_Id__c).toLowerCase(),cct.Id);
        contactIds.add(cct.Id);
        system.debug('TeleoMapping_lp11::'+candidateTeleoMapping);
        }
         if(!candEmailIds.IsEmpty())
        {
            // add logic for Contact
            for(Contact candidateRecord:[Select Id,Email from Contact where (Email IN:candEmailIds   )
                                        and (RecordTypeID=:candidateRecordTypeId or RecordTypeId=:candidateAdhocRecordTypeId or RecordTypeId=:EmpRecordTypeId) limit:Limits.getLimitQueryRows()] )
            {
                if(!candidateEmailMapping.containsKey(candidateRecord.Email)){
                    candidateEmailMapping.put((candidateRecord.Email).toLowerCase(),candidateRecord.Id);
                }
                  System.debug('candidateEmailMapping'+candidateEmailMapping);
            }
            
        
         for(Case openCase:[Select id,ContactId,Contact.Email,CaseNumber from Case where (Contact.Email IN:candEmailIds)
                                        and RecordTypeID=:caseRecordTypeId and IsClosed =false limit:Limits.getLimitQueryRows()])
            {
                oldCaseMapping.put((openCase.Contact.Email).toLowerCase(),openCase.CaseNumber);
            }
            System.debug('oldCaseMapping'+oldCaseMapping);
          for(Case openCase:[Select id,ContactId,Contact.Email,CaseNumber,Contact.WCT_Taleo_Id__c from Case where (Contact.WCT_Taleo_Id__c IN:rmsIds)
                                        and RecordTypeID=:caseRecordTypeId limit:Limits.getLimitQueryRows()])
            {
                oldCaseTeleoMapping.put((openCase.Contact.WCT_Taleo_Id__c).toLowerCase(),openCase.CaseNumber);
                oldCaseIdMapping.put((openCase.Contact.WCT_Taleo_Id__c).toLowerCase(),openCase.Id);
                System.debug('oldCaseTeleoMapping'+oldCaseTeleoMapping);
                
               // rmsIds.remove(openCase.Contact.WCT_Taleo_Id__c);
                
            } 
            System.debug('oldCaseIdMapping'+oldCaseIdMapping);
            System.debug('oldCaseTeleoMapping'+oldCaseTeleoMapping);
          }
            if(!RequistionID.IsEmpty())
        {
            for(WCT_Requisition__c requisitionRecord:[Select WCT_Requisition_Number__c,WCT_Job_Type__c,WCT_FSS__c,WCT_Service_Area__c from WCT_Requisition__c 
                                                where WCT_Requisition_Number__c IN:RequistionID limit:Limits.getLimitQueryRows()] )
            {
                requisitionMapping.put((requisitionRecord.WCT_Requisition_Number__c).toLowerCase(),requisitionRecord);
                reqIds.add(requisitionRecord.Id);        
            }
            System.debug('requisitionMapping'+requisitionMapping);
            System.debug('reqIds'+reqIds);
        }

        if(!contactIds.isEmpty() && !reqIds.isEmpty())
        {
            for(WCT_Candidate_Requisition__c candidateTracker:[Select id, WCT_Contact__c,WCT_Requisition__c from WCT_Candidate_Requisition__c 
                                                where WCT_Contact__c IN:contactIds AND WCT_Requisition__c IN:reqIds limit:Limits.getLimitQueryRows()] )
            {
                List<WCT_Candidate_Requisition__c> ctList = candidateTrackerMapping.get(candidateTracker.WCT_Contact__c);
                if(ctList != null) {
                    ctList.add(candidateTracker);
                    System.debug('ctList'+ctList); 
                } else {
                    candidateTrackerMapping.put(candidateTracker.WCT_Contact__c, new List<WCT_Candidate_Requisition__c>{candidateTracker});
                }
                
            }
           System.debug('candidateTrackerMapping'+candidateTrackerMapping); 
           
        }
        List<WCT_PreBI_GPA_Stage_Table__c> GPAcaseList11 = [SELECT Id,Name,OwnerId,Candidate_E_mail__c,Candidate_First_Name__c,
                                                   Candidate_Last_Name__c,Case_Creation__c,Recruiter_E_mail__c,No_GPA_Comment__c,
                                                   OverallGPA__c,Recruiter_Name__c,Requisition__c,Taleo_RMS_ID__c,Version_No__c,EA_Trigger_Date__c,BI_Trigger_Date__c,OBS__c,Out_of__c,School_Name__c
                                                   FROM WCT_PreBI_GPA_Stage_Table__c WHERE ID IN: stagingIds AND Case_Creation__c = true and Taleo_RMS_ID__c IN:rmsIds ];
        
            
        for(WCT_PreBI_GPA_Stage_Table__c pbgp : GPAcaseList11)
        {
        System.debug('candidateTeleoMapping11'+candidateTeleoMapping); 
        System.debug('candidateEmailMapping11'+candidateEmailMapping); 
        //if(pbgp.OverallGPA__c >= 3)
       if(pbgp.OverallGPA__c <= 3 && (pbgp.OBS__c == '' || pbgp.OBS__c != ''  ) && pbgp.Taleo_RMS_ID__c!=null && candidateTeleoMapping.containsKey((pbgp.Taleo_RMS_ID__c).toLowerCase()))
        {
        
        Case caseToAdd = new Case();
        
        caseToAdd.RecordTypeId=caseRecordTypeId;
        caseToAdd.ContactId=candidateTeleoMapping.get(pbgp.Taleo_RMS_ID__c);
        system.debug('TeleoMapping_lp::'+caseToAdd.ContactId);
        caseToAdd.OwnerId=recruiterEmailMapping.get(pbgp.Recruiter_E_mail__c);
        caseToAdd.Status=WCT_UtilConstants.CASE_STATUS_NEW;
        caseToAdd.Priority=WCT_UtilConstants.CASE_PRIORITY_MEDIUM;
        caseToAdd.Subject=WCT_UtilConstants.CASE_SUBJECT_PREBIGPA;
        caseToAdd.WCT_Category__c=WCT_UtilConstants.CASE_CATEFORY;
        caseToAdd.WCT_SubCategory1__c=WCT_UtilConstants.CASE_SUBCATEGORY1_CAMPUS;
        caseToAdd.WCT_SubCategory2__c=WCT_UtilConstants.CASE_SUBCATEGORY2;
        caseToAdd.Origin=WCT_UtilConstants.CASE_ORIGIN;
        caseToAdd.EA_Start_Date__c = pbgp.EA_Trigger_Date__c;
        caseToAdd.BI_Start_Date__c = pbgp.BI_Trigger_Date__c;
        caseToAdd.Out_Of_GPA__c=pbgp.Out_of__c;
        caseToAdd.OverAll_GPA__c= pbgp.OverallGPA__c;
        caseToAdd.School_Name__c= pbgp.School_Name__c;
        caseToAdd.OBS__c= pbgp.OBS__c;
        caseToAdd.No_GPA_Comment__c= pbgp.No_GPA_Comment__c;
        casesToCreate.add(caseToAdd);
        }
        else if(pbgp.OverallGPA__c == null && (pbgp.OBS__c == '' || pbgp.OBS__c != ''  ) && pbgp.Taleo_RMS_ID__c!=null && candidateTeleoMapping.containsKey((pbgp.Taleo_RMS_ID__c).toLowerCase()))
        {
        
        Case caseToAdd = new Case();
        
        caseToAdd.RecordTypeId=caseRecordTypeId;
        caseToAdd.ContactId=candidateTeleoMapping.get(pbgp.Taleo_RMS_ID__c);
        system.debug('TeleoMapping_lp::'+caseToAdd.ContactId);
        caseToAdd.OwnerId=recruiterEmailMapping.get(pbgp.Recruiter_E_mail__c);
        caseToAdd.Status=WCT_UtilConstants.CASE_STATUS_NEW;
        caseToAdd.Priority=WCT_UtilConstants.CASE_PRIORITY_MEDIUM;
        caseToAdd.Subject=WCT_UtilConstants.CASE_SUBJECT_PREBIGPA;
        caseToAdd.WCT_Category__c=WCT_UtilConstants.CASE_CATEFORY;
        caseToAdd.WCT_SubCategory1__c=WCT_UtilConstants.CASE_SUBCATEGORY1_CAMPUS;
        caseToAdd.WCT_SubCategory2__c=WCT_UtilConstants.CASE_SUBCATEGORY2;
        caseToAdd.Origin=WCT_UtilConstants.CASE_ORIGIN;
        caseToAdd.EA_Start_Date__c = pbgp.EA_Trigger_Date__c;
        caseToAdd.BI_Start_Date__c = pbgp.BI_Trigger_Date__c;
        caseToAdd.Out_Of_GPA__c=pbgp.Out_of__c;
        caseToAdd.OverAll_GPA__c= pbgp.OverallGPA__c;
        caseToAdd.School_Name__c= pbgp.School_Name__c;
        caseToAdd.OBS__c= pbgp.OBS__c;
        caseToAdd.No_GPA_Comment__c= pbgp.No_GPA_Comment__c;
        casesToCreate.add(caseToAdd);
        }
        }
        if(!casesToCreate.IsEmpty())
        {
        Database.SaveResult[] srList1 = Database.insert(casesToCreate, false);
        for(Database.SaveResult sr1: srList1)
        {
            if(sr1.IsSuccess())
            {   
            CaseIds.add(sr1.getId());
            system.debug('srlist::'+sr1.getId());
            
            
            }
            else if(!sr1.IsSuccess())
            {
            for(Database.Error err : sr1.getErrors())
            {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.WCT_Data_Error+err.getMessage());
            ApexPages.addMessage(errormsg);       
            }
            }
        }
        }
       List<Case> GPAcaseList1 = [SELECT Id FROM Case WHERE ID IN: CaseIds ];
  
       List<Task> TaskList1 = new List<Task>();
       taskRecordTypeID= Schema.SObjectType.Task.getRecordTypeInfosByName().get(WCT_UtilConstants.TASK_RECORDTYPE_PRE_BI_GPA).getRecordTypeId();
       
        For(Case Cas: GPAcaseList1){
        Task task=new Task();
        task.RecordTypeId=taskRecordTypeID;
        task.ActivityDate=System.today()+7;
        task.IsReminderSet = true;
        task.ReminderDateTime=System.Now()+6;
        task.Status=WCT_UtilConstants.TASK_NOT_STARTED;
        task.Priority=WCT_UtilConstants.TASK_PRIORITY_NORMAl;
        task.Subject='PreBI GPA Issue';
        task.WhatId=Cas.Id;
        task.Description='Do the Required process to complete PreBI GPA Issue for this candidate';
        TaskList1.add(task);                                     
                                       
        }
        if(!TaskList1.IsEmpty())
        {
        Database.SaveResult[] srList2 = Database.insert(TaskList1, false);
        for(Database.SaveResult sr1: srList2)
        {
            if(sr1.IsSuccess())
            {   
            CaseIds.add(sr1.getId());
            system.debug('srlist::'+sr1.getId());
            
            
            }
            else if(!sr1.IsSuccess())
            {
            for(Database.Error err : sr1.getErrors())
            {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.WCT_Data_Error+err.getMessage());
            ApexPages.addMessage(errormsg);       
            }
            }
        }
        
        }
       
        return null;
        }
        public void createCase(Set<Id> stagingIds){
        }
        
        
        /** 
        Method Name  : mapStagingRecords
        Return Type  : List<WCT_PreBI_GPA_Stage_Table__c>
        Description  : Map fields names with CSV col names         
        */
        private List<WCT_PreBI_GPA_Stage_Table__c> mapStagingRecords()
        {    
        nameFile=contentFile.toString();
        //filelines = parseCSVInstance.parseCSV(nameFile, true);
        filelines=parseCSV(nameFile, true);
        List<WCT_PreBI_GPA_Stage_Table__c> stagingRecordsList=new List<WCT_PreBI_GPA_Stage_Table__c>();
        noOfRows=fileLines.size();
        
        system.debug('filenines---'+filelines);
        system.debug('noOfRows---'+noOfRows);
        
        Integer counter=0;
        /*
        for(List<String> inputValues:filelines) 
        {    
        if(counter>0) 
        break;
        //versionNo = inputValues[0];
        counter++;
        }   
        */
             
        for(List<String> inputValues:filelines) 
        {  
        
        
        system.debug('@#@#'+inputValues[15]);
       // system.debug('@#@#'+inputValues[16]); 
        system.debug('@#@#'+inputValues.size()); 
         system.debug('@#@#'+filelines.size()); 
        
        
        Boolean caseFlag=false;
        Map<String,Boolean> flagCompare=new Map<String,Boolean>();
        system.debug('stringSize---' + inputValues.size());
        Boolean tempFlag=false;
        WCT_PreBI_GPA_Stage_Table__c stagingRecord = new WCT_PreBI_GPA_Stage_Table__c();
        //stagingRecord.Version_No__c =versionNo;
        stagingRecord.Taleo_RMS_ID__c =inputValues[0];
        
        stagingRecord.Candidate_Last_Name__c =inputValues[1];
        stagingRecord.Candidate_First_Name__c =inputValues[2];
        stagingRecord.Candidate_Middle_Name__c =inputValues[3];
        stagingRecord.Candidate_E_mail__c =inputValues[4];
        stagingRecord.EA_Trigger_Date__c =inputValues[5];
        stagingRecord.BI_Trigger_Date__c =inputValues[6];
        stagingRecord.Start_Date__c =inputValues[7];
        stagingRecord.Requisition__c =inputValues[8];
        stagingRecord.Recruiter_Name__c =inputValues[9];
        stagingRecord.Recruiter_E_mail__c =inputValues[10];
        stagingRecord.Candidate_Type__c =inputValues[11];
        if(inputValues[12]!='')
        {
        stagingRecord.OverallGPA__c =decimal.valueOf(inputValues[12]);
        }
        else
        {stagingRecord.OverallGPA__c = null;}
        if(inputValues[13]!='')
        {
        stagingRecord.Out_of__c = decimal.valueOf(inputValues[13]);
        }
        else
        {stagingRecord.Out_of__c = null;}
        /*if(inputValues[13]!='')
        stagingRecord.Out_of__c =decimal.valueOf(inputValues[13]);*/
        stagingRecord.No_GPA_Comment__c = inputValues[14];   
        stagingRecord.School_Name__c =inputValues[15];
        if(inputValues.size() == 17)
        stagingRecord.OBS__c =  (inputvalues.size()>16 && inputvalues[16]!='')?inputvalues[16]:''; 
      /*  if(inputValues[16]!= '' )
        {
        stagingRecord.OBS__c =inputValues[16];
        }
        else
        {
        stagingRecord.OBS__c = 'null';
        } */
        system.debug('size'+inputValues.size());
        //stagingRecord.Case_Creation__c = true;
        stagingRecordsList.add(stagingRecord);
        }
        system.debug(stagingRecordsList+'stagingRecordsList');
        return stagingRecordsList;
        }
        /** 
        Method Name  : parseCSV
        Return Type  : String contents,Boolean skipHeaders
        Description  : Business logic for parsing the string         
        */
        private List<List<String>> parseCSV(String contents,Boolean skipHeaders) {
        List<List<String>> allFields = new List<List<String>>();
        
        // replace instances where a double quote begins a field containing a comma
        // in this case you get a double quote followed by a doubled double quote
        // do this for beginning and end of a field
        contents = contents.replaceAll(',"""',',"DBLQT').replaceall('""",','DBLQT",');
        // now replace all remaining double quotes - we do this so that we can reconstruct
        // fields with commas inside assuming they begin and end with a double quote
        contents = contents.replaceAll('""','DBLQT');
        // we are not attempting to handle fields with a newline inside of them
        // so, split on newline to get the spreadsheet rows
        
        System.debug('contents'+contents);
        
        List<String> lines = new List<String>();
        try {
        lines = contents.split('\r\n');
        } catch (System.ListException e) {
        System.debug('Limits exceeded?' + e.getMessage());
        }
        system.debug(lines+'lines');
        Integer num = 0;
        for(String line : lines)
        {
        system.debug('line'+line);
        // check for blank CSV lines (only commas)
        if (line.replaceAll(',','').trim().length() == 0) continue;
        
        List<String> fields = line.split(',');  
        system.debug(fields);
        List<String> cleanFields = new List<String>();
        String compositeField;
        Boolean makeCompositeField = false;
        for(String field : fields)
        {
        if (field.startsWith('"') && field.endsWith('"')) {
        cleanFields.add(field.replaceAll('DBLQT','"'));
        } else if (field.startsWith('"')) {
        makeCompositeField = true;
        compositeField = field;
        } else if (field.endsWith('"')) {
        compositeField += ',' + field;
        cleanFields.add(compositeField.replaceAll('DBLQT','"'));
        makeCompositeField = false;
        } else if (makeCompositeField) {
       // compositeField +=  ',' + field;
        } else {
        cleanFields.add(field.replaceAll('DBLQT','"'));
        }
        }
        
        allFields.add(cleanFields);
        }
        if (skipHeaders) allFields.remove(0);
        return allFields;       
        }
        
        /**
        Method Name  : getAllStagingRecords
        Return Type  : List<WCT_PreBI_GPA_Stage_Table__c>
        Description  : Show all records into VF        
        */
        public List<WCT_PreBI_GPA_Stage_Table__c> getAllStagingRecords()
        {
        if (stagingIds!= NULL)
        if (stagingIds.size() > 0)
        {
        return [Select id,Name, Taleo_RMS_ID__c,Candidate_Last_Name__c,Candidate_E_mail__c,Recruiter_E_mail__c from WCT_PreBI_GPA_Stage_Table__c where Id In:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_ERROR];
        
        }
        else
        return null;                   
        else
        return null;
        } 
        }