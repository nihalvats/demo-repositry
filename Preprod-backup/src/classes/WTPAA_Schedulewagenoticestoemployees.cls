global class WTPAA_Schedulewagenoticestoemployees implements Schedulable {

     global void execute(SchedulableContext sc) {

         

       //Define  batch size.       

       integer BATCH_SIZE = 200; 

      WTPAA_sendReminderwagenoticestoemployees sndBatch = new WTPAA_sendReminderwagenoticestoemployees ();
     system.debug('****WTPAA_Schedulewagenoticestoemployees : starting batch exection*****************');

     Id batchId = database.executeBatch(sndBatch , BATCH_SIZE);   

  system.debug('**** WTPAA_Schedulewagenoticestoemployees : Batch executed batchId: '+batchId);

   }
 }