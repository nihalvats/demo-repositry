public class Skills_Extractcon extends SitesTodHeaderController{
    
    Public string skillsextract {get;set;}
    Public List<string> liskillsextract {get;set;}
    Public list<innercl> skillslist{get;set;}
    


    Public Skills_Extractcon(){
        skillslist= new list<innercl>();
        liskillsextract= new list<string>();
       init(); 
       
    }
    
    Public void init(){
        
        
       if(loggedInContact!=null)
        {
        List<Contact> loggedinContactWithExtraDetails= [Select Skills_Extract__c , Id from Contact Where Id = :loggedInContact.Id];
        
        
            skillsextract = loggedinContactWithExtraDetails[0]!=null?loggedinContactWithExtraDetails[0].Skills_Extract__c:null;
            skillslist = new list<innercl> ();
            if(skillsextract!=null && skillsextract!='')
            {
            liskillsextract = skillsextract.split('&&&&&');
    
    
            for(string li : liskillsextract){
                List<string> skilli = li.split('-----');
               System.debug('2131241234234' +skilli  );
                if(skilli.size()==2){
                  skillslist.add(new innercl(skilli[0].trim(),skilli[1].trim()));
                }
                if(skilli.size()==1){
                  skillslist.add(new innercl(skilli[0],''));
                }
            }
            } 
        }
        
   }
    
    Public class innercl{
         Public string skills{get; set;}
         Public string expertise{get; set;}
        Public innercl(string sk,string expert){
            skills = sk;
            expertise =expert;
        }

    }
    
    Public Pagereference exportskills(){
     system.debug('1111111111' +skillslist);
     if(skillslist.size()>0){
        Pagereference pageRef = Page.Skills_Export; 
        return pageRef;
      }
      else{
            return null;
        }
        
        
        
    }
    
   

}