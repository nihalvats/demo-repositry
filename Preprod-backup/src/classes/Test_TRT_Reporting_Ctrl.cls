@isTest
public class Test_TRT_Reporting_Ctrl 
{
	
    public static Contact con;
    public static Contact c;
    public static Case objCase;
    public static Id rtId; 
    public static Id rtIdGMI;
    public static Id rtIdRM;
    public static Id rtIdPM; 
    public static Id rtIdTA;
    public static Id rtIdSur;
    public static Id rtIdOth; 
    public static Id rtIdLea;
    public static Id rtIdLeaEva;
   	public static Account a;
    public static Case_form_Extn__c trtReports; 
        
	
	static testmethod void myTestData()
	{
        //inserting sample test user
         UserRole r=[select id from UserRole where DeveloperName='WCT_Admin'];
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
            User u = new User(Alias = 'sample12', Email='test@deloitte.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,UserRoleId=r.id,IsActive=true,
            TimeZoneSidKey='America/Los_Angeles', UserName='sample12@testorg.com');
            insert u; 
        system.runas(u){
        // Retrieving the record type of Case_form_Extn__c
        Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        rtId =rtMapByName.get('TRT SAP Reporting').getRecordTypeId();//particular RecordId by  Name
        rtIdGMI =rtMapByName.get('TRT GMNI').getRecordTypeId();//particular RecordId by  Name
        rtIdRM =rtMapByName.get('TRT RM Reporting').getRecordTypeId();//particular RecordId by  Name
        rtIdPM =rtMapByName.get('TRT PM Reporting').getRecordTypeId();//particular RecordId by  Name
        rtIdTA =rtMapByName.get('TRT TA Reporting').getRecordTypeId();//particular RecordId by  Name
        rtIdSur =rtMapByName.get('TRT Survey').getRecordTypeId();//particular RecordId by  Name
        rtIdOth =rtMapByName.get('TRT Other Reporting').getRecordTypeId();//particular RecordId by  Name
        rtIdLea =rtMapByName.get('TRT Learning Reporting').getRecordTypeId();//particular RecordId by  Name
        rtIdLeaEva =rtMapByName.get('TRT Learning Evaluation').getRecordTypeId();//particular RecordId by  Name
        
        Schema.DescribeSObjectResult Cas = Case.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames = Cas.getRecordTypeInfosByName();// getting the record Type Info
        Id caseRtId =rtMapByNames.get('Case Mail Consolidation').getRecordTypeId();//particular RecordId by  Name
        
        TRT_Record_Type_Details__c test=new TRT_Record_Type_Details__c(name='SAP test',TRT_Parameter_Value__c='SAP',TRT_Main_Value__c='SAP',TRT_SubCategory__c='',TRT_Page_Name__c='thanku',Record_Type__c='TRT SAP Reporting');
        insert test;
        a = new Account(Name='Deloitte US-India Offices');
        insert a ;
        
        con = new Contact();
        con.Email = 'test@deloitte.com';
      	con.LastName = 'TestName';
      	con.accountId = a.id;
      	con.WCT_Function__c='TALENT';
      	con.WCT_Job_Level_Text__c='test level'; 
      	insert con;
        
        objCase = new case();
        objCase.Status='New';
        objCase.RecordTypeId=caseRtId;
        objCase.WCT_Category__c='TRT Reporting';  
        objCase.Origin = 'Web';
        objCase.Gen_Request_Type__c ='';
        objCase.Priority='3 - Medium';
        objCase.Description ='TRT test';  
        objCase.Gen_RecordType__c ='TRT SAP Reporting';
        objCase.Subject='A reporting request';
        objCase.contactId=con.id;
        insert objCase;
        
        trtReports = new Case_form_Extn__c();
        //trtReports.TRT_Actual_Requester_email__c=c.email;
        trtReports.TRT_Requestor_Name__c = con.id;
        trtReports.TRT_Requestor_Function__c=con.WCT_Function__c;
        trtReports.TRT_Requestor_Level__c=con.WCT_Job_Level_Text__c;
        trtReports.TRT_Requester_Group__c=con.Account.Name;
        trtReports.Case_Form_App__c ='TRT Reporting';
        trtReports.TRT_Request_Status__c='New';
        trtReports.TRT_Include_PII__c=true;
        trtReportS.GEN_Case__c =objCase.id;
        insert trtReports;
        
        System.currentPageReference().getParameters().put('cse',trtReports.Id);
        System.currentPageReference().getParameters().put('reqn','SAP');
        TRT_Reporting_ctrl trtReportings = new TRT_Reporting_ctrl();
        trtReportings.loggedInContact=con;
         trtReportings.trtReport.TRT_Group_sugg__c='SAP Report';
        c = [SELECT Name,email,WCT_Function__c,WCT_Job_Level_Text__c,id,
                       Account.Name FROM contact WHERE id=:con.id limit 1];
       trtReportings.mainvaluereporting= 'SAP';
		trtReportings.subvaluereporting='';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
		trtReportings.efileName='test';
		trtReportings.efileBody=bodyBlob;
		trtReportings.fileName='test';
		trtReportings.fileBody=bodyBlob;
		list<string> checkList = new list<string>{'a','b'};
		trtReportings.checkboxSelections=checkList;
		trtReportings.leaCheckboxSelections=checkList;
		trtReportings.leacompCheckboxSelections=checkList;
		trtReportings.evaCheckboxSelections=checkList;
		trtReportings.surveyListCheckBox=checkList;
        trtReportings.submit();
        }      
	}
    
   
   
	static testmethod void trtReportingRequest1()
	{
		myTestData();
	  	TRT_Reporting_ctrl trtReporting = new TRT_Reporting_ctrl();
        trtReporting.mainvaluereporting='Sap Reporting';
        
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
		trtReporting.efileName='test';
		trtReporting.efileBody=bodyBlob;
		trtReporting.fileName='test';
		trtReporting.fileBody=bodyBlob;
		list<string> checkList = new list<string>{'a','b'};
		trtReporting.checkboxSelections=checkList;
		trtReporting.leaCheckboxSelections=checkList;
		trtReporting.leacompCheckboxSelections=checkList;
		trtReporting.evaCheckboxSelections=checkList;
		trtReporting.surveyListCheckBox=checkList;
         trtReporting.trtReport = new Case_form_Extn__c();
	  	 Test.startTest();
	    
		
		trtReporting.sapReport();
		trtReporting.trtReport.RecordTypeId=rtIdLeaEva;
		
		trtReporting.submit();
	    
	    trtReporting.getLearning();
	    trtReporting.getRManagement();
	    trtReporting.getPManagement();
        trtReporting.getMyCheckboxes();
	    trtReporting.getTrtReport();
        trtReporting.questions();
       
        trtReporting.compPannel();
        trtReporting.Enddatecheck();
        
        Test.stopTest();  
	} 
	
	
}