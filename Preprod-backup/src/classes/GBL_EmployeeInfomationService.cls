@RestResource(urlMapping='/getEmployeeInfo/*')
global class GBL_EmployeeInfomationService 
{
   
  @HttpGet
    global static Contact doGet() 
    {
        system.debug('###doGet called');
        Contact result=null;
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String contactEmail = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        if(contactEmail!=null)
        {
       		List<Contact> resultList = [SELECT LastName, FirstName, Name,Email,WCT_Function__c,WCT_Primary_Industry__c,WCT_Service_Line__c,WCT_Service_Area__c,WCT_Home_Office__c,WCT_Region__c,WCT_Job_Level_Text__c,WCT_Tenure_at_Level__c FROM Contact WHERE Email = :contactEmail and recordType.Name ='Employee'];
        	result=resultList.size()>0?resultList[0]:null;
        }
        return result;
    }	
  
}