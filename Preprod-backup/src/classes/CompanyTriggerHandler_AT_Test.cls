@isTest
public class CompanyTriggerHandler_AT_Test
{
public static testmethod void  CompanyTriggerHandler_AT ()
{
    Contact c = new Contact();
    c.firstname='jkxcver434';
    c.lastname='kljklsdrererrer';
    c.Email='abc@gmnailxdferer.com';
    insert c;
    
    Company__c comp = new Company__c();
    comp.Name='abc';
    comp.DUNS_Number__c='120459709';
    comp.MDR_Parent_Company__c='xyz';
    comp.Industry__c = 'OTH';
    insert comp;
    
    job__c jb = new job__c ();
    jb.name= 'Test';
    jb.company__c = comp.id;
    jb.AR_Company_Name__c = 'Test';
    jb.AR_Primary_Location_City__c = 'Test location';
    jb.CCL_Contact_for_company_if_applicable__c = 'Test Company contact';
    jb.Email__c = 'test@deloitte.com';
    jb.Description__c ='test';
    jb.AR_Requirements__c = 'TEst Requirements';
    jb.Source__c = 'External';
    jb.industry__c = 'Others';
    insert jb;
    
    
     set<id> comids  = new set<id>();
   	 comids.add(comp.Id);
	 List<Company__c> company = new List<Company__c>();
	 company.add(comp);
     list<job__c> lst = [select id, company__r.name, industry__c from job__c where company__r.id in: comids];
 
 CompanyTriggerHandler_AT compTrigger = new CompanyTriggerHandler_AT();
 // CompanyTriggerHandler_AT.CompanyNameList();
 compTrigger.assignCompanyName(company);

	}
    
    public static testmethod void  Method1()
	{
		
		Company__c comp = new Company__c();
			comp.Name='abc';
			comp.DUNS_Number__c='120459709';
			comp.MDR_Parent_Company__c='xyz';
			comp.Industry__c = 'OTH';
		insert comp;
		
		job__c jb = new job__c ();
			jb.name= 'Test';
			jb.company__c = comp.id;
			jb.AR_Company_Name__c = 'Test';
			jb.AR_Primary_Location_City__c = 'Test location';
			jb.CCL_Contact_for_company_if_applicable__c = 'Test Company contact';
			jb.Email__c = 'test@deloitte.com';
			jb.Description__c ='test';
			jb.AR_Requirements__c = 'TEst Requirements';
			jb.Source__c = 'External';
			jb.industry__c = 'Others';
		insert jb;
		
		comp.Industry__c = 'CIP';
		update comp;
		
		comp.Industry__c = 'E & R';
		update comp;

		comp.Industry__c = 'FED';
		update comp;
		
		comp.Industry__c = 'FS';
		update comp;

		comp.Industry__c = 'CIP';
		update comp;

		comp.Industry__c = 'LSHC';
		update comp;

		comp.Industry__c = 'PS';
		update comp;
		
		comp.Industry__c = 'TMT';
		update comp;

		comp.Industry__c = 'NONE';
		update comp;

	}
   
   
}