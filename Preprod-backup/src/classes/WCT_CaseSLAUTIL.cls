public class WCT_CaseSLAUTIL {
    
    //public static final String CONTACTINDIA_REGION = 'INDIA/REGION 10';
    public static final Integer BUSINESSHOURS_INHOURS = 28800000;
    public static final Set<String> PENDINGCUSTOMER_STATUS = new Set<String>{'Pending - Vendor Action','Pending - Customer Action'};
        public static Boolean isBeforeUpdateTrigger = false;
    public static Boolean isAfterUpdateTrigger = false;
    public Static boolean isSLACreated=false;
    
    
    //Insertion of Case State Records on insertion of Case
    public static void createCaseStateRecords(List<Case> caseList)
    {
        try{
        List<WCT_Case_State__c> caseStateList = new List<WCT_Case_State__c>();
        
        for(Case ca:caseList)
        {
            // if(ca.WCT_IsSLAInitiated__c){
            WCT_Case_State__c tempCastate = new WCT_Case_State__c();
            tempCastate.WCT_Category__c = ca.WCT_Category__c;
            tempCastate.WCT_Enter_State__c = system.now();
            tempCastate.WCT_Related_Case__c = ca.id;
            tempCastate.WCT_Status__c = ca.Status;
            tempCastate.WCT_Priority__c = ca.Priority;
            tempCastate.WCT_Record_Status__c = 'Open';
            tempCastate.WCT_User_Tier__c=ca.WCT_User_Tier__c;
            if(string.valueOf(ca.ownerId).startswith('005')){
                tempCastate.WCT_Owner__c = ca.ownerId;
            }
            
            tempCastate.WCT_Event__c ='Record Creation';
            tempCastate.WCT_Related_SLA__c = ca.WCT_Related_SLA__c;
            tempCastate.WCT_SLA_Breach_Date__c = ca.WCT_SLA_Breach_Date__c;
            if(ca.WCT_SLA_Breach_Date__c!=null)
                tempCastate.WCT_Expected_SLA_Hours__c = BusinessHours.diff(ca.WCT_Related_Business_Hours_Id__c,system.now(), ca.WCT_SLA_Breach_Date__c)/3600000.0; 
            
            caseStateList.add(tempCastate);
            //   }
        }
        system.debug('#### new Case State List '+caseStateList);
        if(!caseStateList.isEmpty()){
            insert caseStateList;
        }
        }
        catch(Exception e )
        {
            
            WCT_ExceptionUtility.logException('EmailMessage Trigger','Attching email to existing Case','Line # '+e.getLineNumber()+'Message'+e.getMessage()+e.getStackTraceString());
        }   
    }
    
    //Update and Creation of Case State Records on Update of Case
    
    public static void createAndUpdateCaseStateRecords(Map<Id,Case> OldCaseMap,Map<Id,Case> UpdatedCaseMap,List<Case> updatedCaseList){
        try
        {
            Map<Id,List<WCT_Case_State__c>> caseWithCaseStatesMap = new Map<Id,List<WCT_Case_State__c>>();
            List<WCT_Case_State__c> prevCaseStatesToUpdate = new List<WCT_Case_State__c>();
            List<WCT_Case_State__c> newCaseStatesToInsert = new List<WCT_Case_State__c>();
            List<Case> caseListToCreateCaseState = new List<Case>();
            Map<Id,WCT_SLA_Values__c> slaValuesMap = new Map<Id,WCT_SLA_Values__c>();
            for(WCT_SLA_Values__c sla:[SELECT id,WCT_Business_Hours_Days__c,WCT_Category__c,WCT_Priority__c,WCT_Record_Type__c,Sub_Category_1__c,
                                       WCT_Type__c FROM WCT_SLA_Values__c]){
                                           slaValuesMap.put(sla.id,sla);
                                       }
            
            
            //Create Map of Case and Related Case state records
            for(WCT_Case_State__c cs:[Select id,WCT_Related_Case__c,WCT_Category__c,WCT_Enter_State__c,WCT_Status__c,WCT_Priority__c,WCT_Related_Business_Hours_Id__c,
                                      WCT_Previous_Status__c,WCT_Leave_State__c,WCT_Case_Sub_Category_1__c from WCT_Case_State__c where WCT_Related_Case__c IN :OldCaseMap.keySet() ORDER BY Name Desc]){
                                          
                                          if(!caseWithCaseStatesMap.containsKey(cs.WCT_Related_Case__c)){
                                              List<WCT_Case_State__c> tempCaseStateList = new List<WCT_Case_State__c>();
                                              tempCaseStateList.add(cs);
                                              caseWithCaseStatesMap.put(cs.WCT_Related_Case__c,tempCaseStateList);
                                          }else{
                                              List<WCT_Case_State__c> tempCaseStateList = new List<WCT_Case_State__c>();
                                              tempCaseStateList = caseWithCaseStatesMap.get(cs.WCT_Related_Case__c);
                                              tempCaseStateList.add(cs);
                                              caseWithCaseStatesMap.put(cs.WCT_Related_Case__c,tempCaseStateList);
                                          }  
                                          system.debug('sizeofMap'+caseWithCaseStatesMap.get(cs.WCT_Related_Case__c).size());
                                      }
            
            
            for(case ca:updatedCaseList){
                
                
                if(caseWithCaseStatesMap.containsKey(ca.id) && !isAfterUpdateTrigger){
                    if((OldCaseMap.get(ca.id).status != ca.status) || (OldCaseMap.get(ca.id).OwnerId != ca.OwnerId)){
                        system.debug('entered 1'+OldCaseMap.get(ca.id).status+'^^'+ca.status);
                        //if(caseWithCaseStatesMap.containsKey(ca.id)){
                        WCT_Case_State__c tempPrevCaseSate = new WCT_Case_State__c();
                        tempPrevCaseSate = caseWithCaseStatesMap.get(ca.id)[0];
                        tempPrevCaseSate.WCT_Leave_State__c = system.now();
                        tempPrevCaseSate.WCT_Record_Status__c = 'Closed';
                        if(tempPrevCaseSate.WCT_Related_Business_Hours_Id__c != null){
                            tempPrevCaseSate.WCT_Actual_SLA_Hours__c =  BusinessHours.diff(tempPrevCaseSate.WCT_Related_Business_Hours_Id__c,tempPrevCaseSate.WCT_Enter_State__c,tempPrevCaseSate.WCT_Leave_State__c )/3600000.0; 
                            
                        }
                        tempPrevCaseSate.Time_in_Status_Hours_CD__c= (tempPrevCaseSate.WCT_Leave_State__c.getTime()-tempPrevCaseSate.WCT_Enter_State__c.getTime())/3600000.0;
                        prevCaseStatesToUpdate.add(tempPrevCaseSate);
                        if(!(ca.IsClosed)){
                            WCT_Case_State__c newCaseSate = new WCT_Case_State__c();
                            newCaseSate.WCT_Category__c = ca.WCT_Category__c;
                            newCaseSate.WCT_Enter_State__c = system.now();
                            newCaseSate.WCT_Related_Case__c = ca.id;
                            newCaseSate.WCT_Status__c = ca.Status;
                            newCaseSate.WCT_Priority__c = ca.Priority;
                            newCaseSate.WCT_User_Tier__c = ca.WCT_User_Tier__c;
                            newCaseSate.WCT_Record_Status__c = 'Open';
                            newCaseSate.WCT_Previous_Status__c = OldCaseMap.get(ca.id).status;
                            if(string.valueOf(ca.ownerId).startswith('005')){
                                newCaseSate.WCT_Owner__c = ca.ownerId;
                            }
                            if(OldCaseMap.get(ca.id).status != ca.status){
                                newCaseSate.WCT_Event__c = 'Status Change';
                            }else{
                                newCaseSate.WCT_Event__c = 'Owner Change';
                            }
                            
                            newCaseSate.WCT_Related_SLA__c = ca.WCT_Related_SLA__c;
                            newCaseSate.WCT_SLA_Breach_Date__c = ca.WCT_SLA_Breach_Date__c;
                            if(ca.WCT_SLA_Breach_Date__c!=null)
                                newCaseSate.WCT_Expected_SLA_Hours__c = BusinessHours.diff(ca.WCT_Related_Business_Hours_Id__c,system.now(), ca.WCT_SLA_Breach_Date__c)/3600000.0; 
                            newCaseStatesToInsert.add(newCaseSate); 
                        }                       
                        //}
                    }
                    else if(OldCaseMap.get(ca.id).WCT_Category__c != ca.WCT_Category__c || OldCaseMap.get(ca.id).Priority != ca.Priority){
                        //if(caseWithCaseStatesMap.containsKey(ca.id)){
                        WCT_Case_State__c tempCaseSate = new WCT_Case_State__c();
                        //List<WCT_SLA_Values__c> relSLARecord = getRelatedSLA(ca.WCT_Category__c,ca.Priority,string.valueof(ca.recordTypeId));
                        tempCaseSate = caseWithCaseStatesMap.get(ca.id)[0];
                        if(OldCaseMap.get(ca.id).WCT_Category__c != ca.WCT_Category__c ){
                            tempCaseSate.WCT_Category__c = ca.WCT_Category__c;
                        }
                        if(OldCaseMap.get(ca.id).Priority != ca.Priority){
                            tempCaseSate.WCT_Priority__c = ca.Priority;
                        }
                        //if(!relSLARecord.isEmpty()){
                        if(tempCaseSate.WCT_Related_Business_Hours_Id__c != null){
                            
                            if(slaValuesMap.get(ca.WCT_Related_SLA__c).WCT_Type__c == 'Hours'){
                                tempCaseSate.WCT_SLA_Breach_Date__c = BusinessHours.add(tempCaseSate.WCT_Related_Business_Hours_Id__c, ca.WCT_SLA_Initiated_Date__c, (((slaValuesMap.get(ca.WCT_Related_SLA__c).WCT_Business_Hours_Days__c)*3600000)+ca.WCT_Customer_SLA_Hours__c*3600000).longValue());
                            }else{
                                tempCaseSate.WCT_SLA_Breach_Date__c = BusinessHours.add(tempCaseSate.WCT_Related_Business_Hours_Id__c, ca.WCT_SLA_Initiated_Date__c, (((slaValuesMap.get(ca.WCT_Related_SLA__c).WCT_Business_Hours_Days__c)*BUSINESSHOURS_INHOURS)+ca.WCT_Customer_SLA_Hours__c*3600000).longValue());
                            }
                        }
                        //}
                        prevCaseStatesToUpdate.add(tempCaseSate);
                        //}
                    }
                }else if(ca.WCT_IsSLAInitiated__c && !isAfterUpdateTrigger){
                    caseListToCreateCaseState.add(ca);
                }
            }
            if(!prevCaseStatesToUpdate.isEmpty()){
                update prevCaseStatesToUpdate;
                //isAfterUpdateTrigger = true;
            }
            if(!newCaseStatesToInsert.isEmpty()){
                insert newCaseStatesToInsert;
                //isAfterUpdateTrigger = true;
            }
            if(!caseListToCreateCaseState.isEmpty()){
                WCT_CaseSLAUTIL.createCaseStateRecords(caseListToCreateCaseState);
                //isAfterUpdateTrigger = true;
            }
        }
        catch(Exception e )
        {
            
            WCT_ExceptionUtility.logException('EmailMessage Trigger','Attching email to existing Case','Line # '+e.getLineNumber()+'Message'+e.getMessage()+e.getStackTraceString());
        }
    }
    
    //Assign SLA for Case before Update
    public static void assignSLAoNCase(List<Case> caseList,Map<Id,Case> caseOldMap){
        
        /*
* Identify for which cases needs the SLA Values to be initiated Or re calculated.
* If below logic is met
* If ( !isSLAInitiated || Catgeory Chnaged || priotiy Chnaged || Record Type Chnaged ||Sub Category Changed )
*/
        try
        {
            List<Case> cases_new_list = new List<Case>();
            
            for( Case temp: caseList)
            {
                if(caseOldMap.get(temp.id)!=null)
                {
                    if(temp.WCT_IsSLAInitiated__c==False || (temp.WCT_Category__c != caseOldMap.get(temp.id).WCT_Category__c) || (temp.Priority !=caseOldMap.get(temp.id).Priority) || (temp.RecordTypeId!=caseOldMap.get(temp.id).RecordTypeId) || (temp.WCT_SubCategory1__c!=caseOldMap.get(temp.id).WCT_SubCategory1__c)   )
                    {
                        cases_new_list.add(temp);
                    }
                }
            }
            
            
            if(cases_new_list.size()>0)
            {
                assignSLAAndCalculateBeachTime(cases_new_list, false);
            }
            
            
            
            
            /*

list<WCT_SLA_Values__c> SLAValueList = new list<WCT_SLA_Values__c>();


for(WCT_SLA_Values__c sl : [SELECT id,WCT_Business_Hours_Days__c,WCT_Category__c,WCT_Priority__c,
WCT_Record_Type__c,WCT_Type__c FROM WCT_SLA_Values__c])
{
SLAValueList.add(sl);
}


for(Case tmpCase:caseList){

if((tmpCase.WCT_First_Response_time__c!=null)&&(tmpCase.WCT_Time_For_First_Response__c == null))
{
tmpCase.WCT_Time_For_First_Response__c = BusinessHours.diff(tmpCase.WCT_Related_Business_Hours_Id__c ,tmpCase.CreatedDate, tmpCase.WCT_First_Response_time__c)/3600000.0; 
}

//Check if formula field is populating *** To DO ****
WCT_SLA_Values__c sla = new WCT_SLA_Values__c();
boolean slaValue = false;
for(WCT_SLA_Values__c tempSla:SLAValueList)
{
if(tempSla.WCT_Category__c == tmpCase.WCT_Category__c && tempSla.WCT_Priority__c == tmpCase.Priority &&  tempSla.WCT_Record_Type__c == tmpCase.recordTypeId ){
sla = tempSla;
slaValue = true;
break;
}
}

Case ca = tmpCase;
if(tmpCase.WCT_IsSLAInitiated__c){
ca.WCT_Related_SLA__c = sla.id;
if(ca.WCT_Related_Business_Hours_Id__c!=null && (tmpCase.status != caseOldMap.get(tmpCase.id).status)){
if(tmpCase.WCT_Customer_SLA_Hours__c != null){
if(sla.WCT_Type__c == 'Hours'){
ca.WCT_SLA_Breach_Date__c = BusinessHours.add(ca.WCT_Related_Business_Hours_Id__c,tmpCase.WCT_SLA_Initiated_Date__c , (((sla.WCT_Business_Hours_Days__c * 3600000)+tmpCase.WCT_Customer_SLA_Hours__c*3600000).longValue()));
}else{
ca.WCT_SLA_Breach_Date__c = BusinessHours.add(ca.WCT_Related_Business_Hours_Id__c, tmpCase.WCT_SLA_Initiated_Date__c, (((sla.WCT_Business_Hours_Days__c)*BUSINESSHOURS_INHOURS)+tmpCase.WCT_Customer_SLA_Hours__c*3600000).longValue());
}
}else{
if(sla.WCT_Type__c == 'Hours'){
ca.WCT_SLA_Breach_Date__c = BusinessHours.add(ca.WCT_Related_Business_Hours_Id__c,tmpCase.WCT_SLA_Initiated_Date__c , ((sla.WCT_Business_Hours_Days__c)*3600000).longValue());
}else{
ca.WCT_SLA_Breach_Date__c = BusinessHours.add(ca.WCT_Related_Business_Hours_Id__c, tmpCase.WCT_SLA_Initiated_Date__c, ((sla.WCT_Business_Hours_Days__c)*BUSINESSHOURS_INHOURS).longValue());
}
}
system.debug('**Breach 12*'+ca.WCT_SLA_Breach_Date__c);
if(tmpCase.status != caseOldMap.get(tmpCase.id).status && PENDINGCUSTOMER_STATUS.contains(caseOldMap.get(tmpCase.id).status)){
Long customerTime;
customerTime = BusinessHours.diff(ca.WCT_Related_Business_Hours_Id__c,ca.WCT_Time_when_status_change__c,system.now());
if(tmpCase.WCT_Customer_SLA_Hours__c != null){
customerTime += (tmpCase.WCT_Customer_SLA_Hours__c*3600000).longValue();
}
system.debug('** Cust*'+customerTime);
ca.WCT_SLA_Breach_Date__c = BusinessHours.add(ca.WCT_Related_Business_Hours_Id__c, ca.WCT_SLA_Breach_Date__c,customerTime);
system.debug('**Breach*'+ca.WCT_SLA_Breach_Date__c);
}
system.debug('**Breach 12 closed*'+ca.WCT_SLA_Breach_Date__c);

if(tmpCase.status != caseOldMap.get(tmpCase.id).status){
ca.WCT_Time_when_status_change__c = system.now();
}

//isBeforeUpdateTrigger = true;
}
}
else if(!tmpCase.WCT_IsSLAInitiated__c && !isBeforeUpdateTrigger){
ca.WCT_Related_SLA__c = sla.id;
system.debug('^^^'+ca.WCT_Related_Business_Hours_Id__c);
if(ca.WCT_Related_Business_Hours_Id__c!=null){
if(sla.WCT_Type__c == 'Hours'){
ca.WCT_SLA_Breach_Date__c = BusinessHours.add(ca.WCT_Related_Business_Hours_Id__c,system.now() , ((sla.WCT_Business_Hours_Days__c)*3600000).longValue());
ca.WCT_SLA_Initiated_Date__c = system.now();
ca.WCT_IsSLAInitiated__c = true;
}else{
ca.WCT_SLA_Breach_Date__c = BusinessHours.add(ca.WCT_Related_Business_Hours_Id__c, system.now(), ((sla.WCT_Business_Hours_Days__c)*BUSINESSHOURS_INHOURS).longValue());
ca.WCT_SLA_Initiated_Date__c = system.now();
ca.WCT_IsSLAInitiated__c = true;
}
}
//isBeforeUpdateTrigger = true;

}
//break;
//}
//}

}

*/
        }
        catch(Exception e )
        {
            
            WCT_ExceptionUtility.logException('EmailMessage Trigger','Attching email to existing Case','Line # '+e.getLineNumber()+'Message'+e.getMessage()+e.getStackTraceString());
        }
    }
    
    
    /*
* On before & Insert
* Look out for SLA values from the system, based on the category, priority & Sub Category.
* 
*/
    
    public static void assignSLAOnCaseBeforeInsert(List<Case> caseList)
    {
        try
        {
            assignSLAAndCalculateBeachTime(caseList, true);
        }
        catch(Exception e )
        {
            
            WCT_ExceptionUtility.logException('EmailMessage Trigger','Attching email to existing Case','Line # '+e.getLineNumber()+'Message'+e.getMessage()+e.getStackTraceString());
        }
    }
    
    
    /*
* Logic goes here to find the SLA Values based on the case Details 
* And Calculate the Breach time based on the identified SLA Values & considering below logic for starttime
* If(isInsert == true)
*   starttime= System.now
* else (isInsert == false)
*   starttime = Case.CreatedDate   
*
*/
    
    public static void assignSLAAndCalculateBeachTime(List<Case> caseList, boolean isInsert)
    {
        try
        {
            System.debug('assignSLAOnCaseBeforeInsert called  ');
            SET<String> INDIA_REGIONS = new SET<String>();
            INDIA_REGIONS.addAll(Label.WCT_India_Related_Regions_in_Contact.split(','));
            Map<Id,String> ConIdRegMap = new Map<Id,string>();
            list<WCT_SLA_Values__c> SLAValueList = new list<WCT_SLA_Values__c>();
            List<Id> contactIdList = new List<Id>();
            
            /*Loop through the Cases and find the list of  Category, Priority, RecordTypeName, SubCategory  */
            
            List<String> caseCategories= new List<String>();
            List<String> casePriorities= new List<String>();
            List<String> caseRecordTypeNames= new List<String>();
            List<String> caseSubCategories= new List<String>();
            
            map<Id, Schema.RecordTypeInfo> caseRecTypeById = Schema.SObjectType.case.getRecordTypeInfosById();
            
            for(Case temp:caseList)
            {
                caseCategories.add(temp.WCT_Category__c);
                casePriorities.add(temp.Priority);
                if(caseRecTypeById.get(temp.RecordTypeId)!=null)
                    caseRecordTypeNames.add(caseRecTypeById.get(temp.RecordTypeId).getName());
                caseSubCategories.add(temp.WCT_SubCategory1__c);
                contactIdList.add(temp.contactid);      
            }
            
            /*
* The "*" is wild card specifying any value. filter and select the SLA. 
*/
            caseCategories.add('*');
            casePriorities.add('*');
            caseRecordTypeNames.add('*');
            caseSubCategories.add('*');
            
            try
            {
                SLAValueList = [SELECT id,WCT_Business_Hours_Days__c,WCT_Category__c,WCT_Priority__c,WCT_Record_Type__c,WCT_Type__c,Sub_Category_1__c FROM WCT_SLA_Values__c where WCT_Category__c in :caseCategories or WCT_Priority__c in :casePriorities or WCT_Record_Type__c in :caseRecordTypeNames or Sub_Category_1__c in :caseSubCategories];
            }
            catch(Exception e )
            {
                
                WCT_ExceptionUtility.logException('EmailMessage Trigger','Attching email to existing Case','Line # '+e.getLineNumber()+'Message'+e.getMessage()+e.getStackTraceString());
            }
            
            
            /*Identify the Region of Contact who is tagged to Case */
            
            for(Contact  tmpCon :[select id,WCT_Region__c from contact where id IN :contactIdList]){
                if(!ConIdRegMap.containsKey(tmpCon.id)){
                    if(tmpCon.WCT_Region__c == null || tmpCon.WCT_Region__c == '')
                    {
                        ConIdRegMap.put(tmpCon.id,'US');
                    }else{
                        ConIdRegMap.put(tmpCon.id,tmpCon.WCT_Region__c);
                    }
                }
            }
            
            for(Case tmpCase : caseList)
            {
                
                /*
* Building the record type name from id as recorType.Name will not work for insert call
*/
                String RecordTypeName=caseRecTypeById.get(tmpCase.RecordTypeId).getName();
                
                if(!SLAValueList.isEmpty())
                {
                    WCT_SLA_Values__c matchedSLA= null;
                    
                    /*Checking for Exact Match with SLA values configured.*/
                    
                    for(WCT_SLA_Values__c sla:SLAValueList)
                    {
                        if(sla.WCT_Category__c == tmpCase.WCT_Category__c && sla.WCT_Priority__c == tmpCase.Priority &&  sla.WCT_Record_Type__c== RecordTypeName && sla.Sub_Category_1__c == tmpCase.WCT_SubCategory1__c)
                        {
                            matchedSLA=sla;
                            break;
                        }
                    }
                    /*If Exact match is not found then, Check for SLA values non exact & with wild card in them */
                    if(matchedSLA==null)
                    {
                        for(WCT_SLA_Values__c sla:SLAValueList)
                        {
                            if((sla.WCT_Category__c == tmpCase.WCT_Category__c || sla.WCT_Category__c=='*' ) && ( sla.WCT_Priority__c == tmpCase.Priority || sla.WCT_Priority__c=='*' ) &&  ( sla.WCT_Record_Type__c== RecordTypeName || sla.WCT_Record_Type__c=='*' ) &&  ( sla.Sub_Category_1__c == tmpCase.WCT_SubCategory1__c  || sla.Sub_Category_1__c=='*' ) )
                            {
                                matchedSLA=sla;   
                                break;
                            }                               
                        }
                    }
                    
                    System.Debug(''+matchedSLA);
                    if(matchedSLA!=null)
                    {
                        System.debug('SLA Found '+matchedSLA);
                        // Case ca = tmpCase;
                        string bhoursId;
                        /*Assign SLA Value record to case. */
                        tmpCase.WCT_Related_SLA__c = matchedSLA.id;
                        
                        if(matchedSLA.WCT_Type__c =='hours' )
                        {
                            tmpCase.WCT_Expected_SLA_Hours__c=string.valueOf(matchedSLA.WCT_Business_Hours_Days__c) ;
                        }
                        else
                        {
                            tmpCase.WCT_Expected_SLA_Hours__c=string.valueOf(matchedSLA.WCT_Business_Hours_Days__c*8);  
                        }
                        /*
* Finding the Region of the Owner. based on same, find the Business Hour id for India,US  
*/
                        if(ConIdRegMap.containsKey(tmpCase.contactid))
                        {
                            if(INDIA_REGIONS.Contains(ConIdRegMap.get(tmpCase.contactid).toUpperCase()))
                            {
                                bhoursId = WCT_BusinessHours__c.getInstance().India_Business_Hours_Id__c;
                            }
                            else
                            {
                                bhoursId = WCT_BusinessHours__c.getInstance().US_Business_Hours_Id__c;
                            }
                        }
                        DateTime systemDateTime=null;
                        if(isInsert==true)
                        {
                            systemDateTime=system.now();
                        }
                        else
                        {
                            systemDateTime=tmpCase.createdDate;
                            
                        }
                        
                        /*
* Calulating the SLA Initiated Date based on the SLA  configured in matched record.
*/
                        if(matchedSLA.WCT_Type__c == 'Hours')
                        {
                            tmpCase.WCT_SLA_Initiated_Date__c = BusinessHours.nextStartDate(bhoursId, systemDateTime);
                            tmpCase.WCT_SLA_Breach_Date__c = BusinessHours.add(bhoursId, BusinessHours.nextStartDate(bhoursId, systemDateTime), ((matchedSLA.WCT_Business_Hours_Days__c)*3600000).longValue());
                            tmpCase.WCT_IsSLAInitiated__c = true;
                        }
                        else
                        {
                            tmpCase.WCT_SLA_Initiated_Date__c = BusinessHours.nextStartDate(bhoursId, system.now());
                            tmpCase.WCT_SLA_Breach_Date__c = BusinessHours.add(bhoursId, BusinessHours.nextStartDate(bhoursId, systemDateTime), ((matchedSLA.WCT_Business_Hours_Days__c)*BUSINESSHOURS_INHOURS).longValue());
                            tmpCase.WCT_IsSLAInitiated__c = true;
                        }
                        
                    }
                    else
                    {
                        tmpCase.WCT_IsSLAInitiated__c = false;
                        tmpCase.WCT_SLA_Breach_Date__c =null;
                        tmpCase.WCT_Related_SLA__c =null;
                        
                    }
                    
                    
                }
                
                
            }
            
            
        }
        catch(Exception e )
        {
            
            WCT_ExceptionUtility.logException('EmailMessage Trigger','Attching email to existing Case','Line # '+e.getLineNumber()+'Message'+e.getMessage()+e.getStackTraceString());
        }     
        
        
    }
}