public class MIS_AddAttachmentclass {
    
    public string cfsid;
    Public Attachment myfile;
    public MIS_AddAttachmentclass(ApexPages.StandardController controller){
        // getting the id from parameter
        cfsid = System.currentPagereference().getParameters().get('id');
    }
   
    Public Attachment getmyfile()
    {
        //attaching the file
        myfile = new Attachment();
        return myfile;
    }
   
    Public Pagereference Savedoc()
    {
       //attaching the file to parent object
        Attachment a = new Attachment();
        list<Case_form_Extn__c> c = new list<Case_form_Extn__c>();
        system.debug('cfsid'+cfsid); 
        if(cfsid != '')
        {
             c=[select id from Case_form_Extn__c where id=:cfsid limit 1];
            system.debug(c);
        }
        if(!c.isEmpty() && c != null)
        {
            if(myfile != null && myfile.body != null )
            {
                a.parentId = c[0].id; 
                a.name=myfile.name; 
                a.body = myfile.body;
            }
        }
        /* insert the attachment */
        try{
            insert a;
            
            PageReference pr = new PageReference('/'+cfsid); 
            pr.setRedirect(true);  
            return pr;
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Attachment is not added to case form extension'+ e.getMessage()));
            system.debug('-------exception---------'+e.getMessage()+'----at---Line #----'+e.getLineNumber());
        }
        
      return null;
    } 
    
    Public Pagereference Cancel()
    {
        //page redirection 
        String accid = System.currentPagereference().getParameters().get('id');
        PageReference pr = new PageReference('/'+accid); 
        pr.setRedirect(true);  
        return pr;
    }

}