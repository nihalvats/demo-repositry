/*********************************************************************************
Class Name      : WCT_Batch_Offer_RecordType_Queue_Update 
Description     : This class will be Update the record types and queues fro offers.
Created By      : Dhruv Bhalodi
Created Date    : 05-Aug-14
Modification Log:
---------------------------------------------------------------------------------- 
Developer                   Date                   Description
----------------------------------------------------------------------------------            
Dhruv Bhalodi            05-Aug-14             Initial Version
*********************************************************************************/

global class WCT_Batch_Offer_RecordType_Queue_Update implements Database.Batchable<sObject> {  

    List<Wct_Offer__c> lstBatchURL = new List<Wct_Offer__c>();
    
    /*********************************************************************************
    Method Name    : start
    Description    : Start method to fetch the Offer List
    Return Type    : Iterable<WCT_Offer__c>
    Parameter      : 1. BC : Context for the batch                
    *********************************************************************************/
   
    global Iterable<sObject> start(Database.BatchableContext BC){
    date Yesterday  =(date) date.today()-1;
    return Database.getQueryLocator('SELECT Id, WCT_User_Group__c, WCT_Candidate__c, WCT_Job_Type__c, WCT_Candidate__r.WCT_Requisition_Number__c, RecordTypeID, WCT_Candidate_Tracker__c FROM Wct_Offer__c WHERE RecordType.Name = \'Master\' and CreatedDate >  : Yesterday');
    }
    /*********************************************************************************
    Method Name    : execute
    Description    : Execute method to process the list of Offer in Batch
    Return Type    : void
    Parameter      : 1. BC : Context for the batch 
    *********************************************************************************/
    global void execute(Database.BatchableContext BC,  List<sObject> lstOffSobject)
    {   
        List<Wct_Offer__c> lstOff = (List<Wct_Offer__c>)lstOffSobject;
        
        map<string,id> rtIdMap = new map<string,id> ();
        for(RecordType rtypes : [Select Name, Id From RecordType where sObjectType='Wct_Offer__c' and isActive=true and (not(name like '%Manual')) and (not(name like '%Locked'))])
        {
        rtIdMap.put(rtypes.Name,rtypes.Id);
        }
        set<String> queueName= new set<string> {'Deloitte India Campus Offers','Deloitte India Experienced Offers','Deloitte US Campus Offers','Deloitte US Experienced Offers'}; 
        String queuesObjectType='Offer'; 
        map<string,id> quIdMap = new map<string,Id>();
        for(QueueSobject queueID : [Select QueueId , Queue.Name from QueueSobject where Queue.Name =:queueName])
        {
        quIdMap.put(queueID.Queue.Name,queueID.QueueId);
        }  
    
       
        List<Wct_Offer__c> OfferList = New List<Wct_Offer__c>();         
        For(Wct_Offer__c Off : lstOff) {
            
            
            
            If(Off.WCT_Job_Type__c == 'Experienced') {
                if(Off.WCT_User_Group__c == 'United States'){
                     Off.RecordTypeID = rtIdMap.get('US Experienced Hire Offer');
                     Off.OwnerID = quIdMap.get('Deloitte US Experienced Offers');
                     OfferList.add(Off);
                }else if(Off.WCT_User_Group__c == 'US GLS India'){
                    Off.RecordTypeID = rtIdMap.get('India Experienced Hire Offer');
                    Off.OwnerID = quIdMap.get('Deloitte India Experienced Offers');    
                    OfferList.add(Off);
                }
            }else if(Off.WCT_Job_Type__c != 'Experienced'){
                if(Off.WCT_User_Group__c == 'United States'){
                    Off.RecordTypeID = rtIdMap.get('US Campus Hire Offer');
                    Off.OwnerID = quIdMap.get('Deloitte US Campus Offers');
                    OfferList.add(Off);
                }else if(Off.WCT_User_Group__c == 'US GLS India'){
                    Off.RecordTypeID = rtIdMap.get('India Campus Hire Offer');
                    Off.OwnerID = quIdMap.get('Deloitte India Campus Offers');
                    OfferList.add(Off);
                }
            
            }
        } 
        If(OfferList.size()>0){
            Update OfferList;
        } 
    }
    /*********************************************************************************
    Method Name    : finish
    Description    : finish method 
    Return Type    : void
    Parameter      : 1. BC : Context for the batch                         
    *********************************************************************************/
    global void finish(Database.BatchableContext BC)
    {
    }

}