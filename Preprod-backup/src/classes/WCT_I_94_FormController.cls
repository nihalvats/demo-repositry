/********************************************************************************************************************************
Apex class         : <WCT_I_94_FormController>
Description        : <Controller which allows to Update Task, I94 & Mobility>
Type               :  Controller
Test Class         :  <WCT_I_94_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 24/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/ 
 public class WCT_I_94_FormController extends SitesTodHeaderController{
    
    /* Creating a Mobility Instance */
    public WCT_Mobility__c MobilityRecord{get;set;}
    
    /* Task Related Variables */
    public Task taskObj{get;set;}
    public String taskID{get;set;}
    
    /* I-94 Expiration Details */
    public String I94Expiration{get;set;}
    public I_94__c I94Record{get;set;}
    
    /* Error Related Variables */
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}  
    
    /* Invoking a Constructor with Task,Mobility and Attachments */
        
    public WCT_I_94_FormController () {
        
        init();
        getParameterInfo();  

        if(taskID =='' || taskID == null)
        {
           invalidEmployee=true;
           return;
        }

        getTaskInstance();
     
        getMobilityDetails();
        
    }

/********************************************************************************************
*Method Name         : <init()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Init() Used for Initializing Mobility and I-94 Record>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     
 
    private void init(){

        pageError = false;
        pageErrorMessage = '';
        MobilityRecord= new WCT_Mobility__c (); 
        I94Record = new I_94__c();
        
    }   

/********************************************************************************************
*Method Name         : <getParameterInfo()>
*Return Type         : <Null>
*Param’s             : URL
*Description         : <GetParameterInfo() Used for Retrieving URL Params for Task>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/      

    private void getParameterInfo() {
        taskID = ApexPages.currentPage().getParameters().get('taskid');
    }

/********************************************************************************************
*Method Name         : <getTaskInstance()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetTaskInstance() Used for Querying Task Instance>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/

    private void getTaskInstance(){
        taskObj=[SELECT Status, OwnerId, WhatId, WCT_Auto_Close__c, WCT_Is_Visible_in_TOD__c  FROM Task WHERE Id =: taskID];
    }
  
    /********************************************************************************************
    *Method Name         : <getMobilityDetails()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <GetMobilityDetails() Used for Fetching Mobility Details>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/         

      public void getMobilityDetails(){
             MobilityRecord= [SELECT   Id,
                                     Name,
                                     WCT_First_Working_Day_in_US__c,
                                     WCT_Last_Working_Day_in_US__c,
                                     WCT_USI_Resource_Manager__c,
                                     WCT_I_94_Expiration__c,
                                     WCT_US_Project_Mngr__c,
                                     WCT_Mobility_Employee__c,
                                     WCT_Purpose_of_Travel__c
                                     FROM WCT_Mobility__c
                                     where id=:taskObj.WhatId ];  
  
    }

/********************************************************************************************
*Method Name         : <save()>
*Return Type         : <Page Reference>
*Param’s             : 
*Description         : <Save() Used to Upsert Method for Task and I-94 Record>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 

   public pagereference insertorupdate()
   {
       if( (null == I94Expiration) ||('' == I94Expiration))
            {
            pageErrorMessage = 'Please fill in all the required fields on the form.';
            pageError = true;
            return null;
        }
        
        if(String.isNotBlank(I94Expiration)){
           MobilityRecord.WCT_I_94_Expiration__c = Date.parse(I94Expiration);
        }
        
        try {
         update MobilityRecord;
      
        I94Record.WCT_Mobility__c = MobilityRecord.id;
        I94Record.WCT_Employee__c = MobilityRecord.WCT_Mobility_Employee__c;
        I94Record.WCT_I94_Expiration__c = MobilityRecord.WCT_I_94_Expiration__c;
        insert I94Record;
        
      if(taskObj.WCT_Auto_Close__c == true){   
           taskObj.status = 'completed';
       }else{
           taskObj.status = 'Employee Replied';  
       }
       
       taskObj.WCT_Is_Visible_in_TOD__c = false; 
       upsert taskObj;
       }
        catch (Exception e) {
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_I_94_FormController', 'I-94 Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_I94_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
        }
        
        PageReference pageRef = new PageReference('/apex/WCT_I_94_FormThankYou?taskid=' + taskID);
        pageRef.setRedirect(true);
        return pageRef;
     
   
   }
}