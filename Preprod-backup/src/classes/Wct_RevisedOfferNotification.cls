global class Wct_RevisedOfferNotification implements Database.Batchable<sObject>,Schedulable{

    global void execute(SchedulableContext SC) {
        Wct_RevisedOfferNotification batch = new Wct_RevisedOfferNotification();
        Integer batchSize = 1;
        ID batchprocessid = Database.executeBatch(batch, batchSize); 
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String strSql = 'SELECT Id,CreatedDate,Name,RecordTypeID,Owner.Email,WCT_Revised_Offer_CreatedDate__c,WCT_Offer_Approved_Date__c,WCT_Time_sent__c,WCT_Iteration_count__c,Offer_Sent_Track__c,WCT_Candidate_Email__c,WCT_Team_Mailbox__c,WCT_RMS_ID__c,WCT_Full_Name__c,WCT_status__c,WCT_AgentState_Time__c, WCT_User_Group__c' +
                     ' FROM WCT_Offer__c'+
                     ' where WCT_status__c IN (\'Offer to Be Extended\', \'Draft in Progress\', \'Returned for Revisions\', \'Offer Approved\', \'Manual (Outside of SFDC)\')'+
                     ' AND owner.email != null'+
                     ' AND Offer_Sent_Track__c = True'+
                     ' AND (CreatedDate = LAST_N_DAYS:4 OR WCT_Offer_Approved_Date__c = LAST_N_DAYS:4 )'+
                     ' AND RecordType.Name IN (\'US Experienced Hire Offer\', \'US Campus Hire Offer\')'; 
        system.debug('query123******'+strSql);
        return database.getQuerylocator(strSql);
        
    }

    global void execute(Database.BatchableContext BC, List<WCT_Offer__c> scope)
    {
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList2 = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList3 = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList4 = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList5 = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList6 = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList7 = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList8 = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList9 = new List<Messaging.SingleEmailMessage>();
        
        set<string> setRecEmails = new set<string>();
        set<string> setteamEmails = new set<string>();
        map<string,string> RecTeamEmails = new map<string,string>();
        map<string,string> RecTeamEmails2 = new map<string,string>();
        map<string,string> RecTeamEmails3 = new map<string,string>();
        map<string,string> RecTeamEmails4 = new map<string,string>();
        map<string,string> RecTeamEmails5 = new map<string,string>();
        map<string,string> RecTeamEmails6 = new map<string,string>();
        Map<String, List<WCT_Offer__c>> recOffers = new Map<String, List<WCT_Offer__c>>(); 
        Map<String, List<WCT_Offer__c>> recOffers2 = new Map<String, List<WCT_Offer__c>>();
        Map<String, List<WCT_Offer__c>> recOffers3 = new Map<String, List<WCT_Offer__c>>(); 
        Map<String, List<WCT_Offer__c>> recOffers4 = new Map<String, List<WCT_Offer__c>>();
        Map<String, List<WCT_Offer__c>> recOffers5 = new Map<String, List<WCT_Offer__c>>();
        Map<String, List<WCT_Offer__c>> recOffers6 = new Map<String, List<WCT_Offer__c>>();
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.WCT_Offer__c.getRecordTypeInfosById();
        Decimal seconds;
        Decimal hrs;
        Decimal min;
        
        map<string,string> RecTeamEmails7 = new map<string,string>();
        map<string,string> RecTeamEmails8 = new map<string,string>();
        map<string,string> RecTeamEmails9 = new map<string,string>();
        Map<String, List<WCT_Offer__c>> recOffers7 = new Map<String, List<WCT_Offer__c>>();
        Map<String, List<WCT_Offer__c>> recOffers8 = new Map<String, List<WCT_Offer__c>>();
        Map<String, List<WCT_Offer__c>> recOffers9 = new Map<String, List<WCT_Offer__c>>();
        Decimal sec;
        Decimal mins;
        
        
        for(WCT_Offer__c tmp : scope) {
             WCT_Offer__c offerRecord = (WCT_Offer__c) tmp;
             WCT_Offer__c offerRecord2 = (WCT_Offer__c) tmp;
             WCT_Offer__c offerRecord3 = (WCT_Offer__c) tmp;
             WCT_Offer__c offerRecord4 = (WCT_Offer__c) tmp;
             WCT_Offer__c offerRecord5 = (WCT_Offer__c) tmp;
             WCT_Offer__c offerRecord6 = (WCT_Offer__c) tmp;
             WCT_Offer__c offerRecord7 = (WCT_Offer__c) tmp;
             WCT_Offer__c offerRecord8 = (WCT_Offer__c) tmp;
             WCT_Offer__c offerRecord9 = (WCT_Offer__c) tmp;
             system.debug('offerRecord**********'+offerRecord);
             
             
             datetime recorddatetime = date.valueof(offerRecord.CreatedDate);
             system.debug('dt**********'+recorddatetime);
             
             seconds = BusinessHours.diff(Label.WCT_Default_Business_Hours_ID, offerRecord.WCT_Revised_Offer_CreatedDate__c, System.now())/ 1000;
             min = Integer.valueOf(seconds / 60);
             hrs = Integer.valueOf(seconds / 3600);
             
             system.debug('seconds**********'+seconds);
             system.debug('min**********'+min);
             system.debug('revised offer Date**********'+offerRecord.WCT_Revised_Offer_CreatedDate__c);
             
             sec = BusinessHours.diff(Label.WCT_Default_Business_Hours_ID, offerRecord.WCT_Offer_Approved_Date__c, System.now())/ 1000;
             mins = Integer.valueOf(sec / 60);
             
             system.debug('sec**********'+sec);
             system.debug('mins**********'+mins);
             system.debug('created date**********'+offerRecord.WCT_Offer_Approved_Date__c);
             
             system.debug('recordtype**********'+rtMap.get(offerRecord.RecordTypeId).getName());
             
             //-------------------1st---------------------------
             if(rtMap.get(offerRecord.RecordTypeId).getName() == 'US Experienced Hire Offer' &&  min >=120 && min < 145 && offerRecord7.WCT_status__c != 'Offer Approved') {  
                 system.debug('recordtype1**********'+rtMap.get(offerRecord.RecordTypeId).getName());
                setRecEmails.add(offerRecord.Owner.Email);
                RecTeamEmails.put(offerRecord.Owner.Email,offerRecord.WCT_Team_Mailbox__c);
                system.debug('RecTeamEmails:'+RecTeamEmails);
                List<WCT_Offer__c> offerList = recOffers.get(offerRecord.Owner.Email);
                if(offerList == null) {
                    offerList = new List<WCT_Offer__c>();
                } 
                offerList.add(offerRecord);
                recOffers.put(offerRecord.Owner.Email,offerList);
                
            }
            //-------------------2nd---------------------------
           if(rtMap.get(offerRecord2.RecordTypeId).getName() == 'US Experienced Hire Offer' && min >=180 && min < 195 && offerRecord7.WCT_status__c != 'Offer Approved' ) {   
                system.debug('alert2**********');
                setRecEmails.add(offerRecord2.Owner.Email);
                RecTeamEmails2.put(offerRecord2.Owner.Email,offerRecord2.WCT_Team_Mailbox__c);
                system.debug('RecTeamEmails2:'+RecTeamEmails2);
                List<WCT_Offer__c> offerList2 = recOffers2.get(offerRecord2.Owner.Email);
                if(offerList2 == null) {
                    offerList2 = new List<WCT_Offer__c>();
                } 
                offerList2.add(offerRecord2);
                recOffers2.put(offerRecord2.Owner.Email,offerList2);
                
            }
            //-------------------3rd---------------------------
            
            if(rtMap.get(offerRecord3.RecordTypeId).getName() == 'US Experienced Hire Offer' && min >= 240 && min < 255 && offerRecord7.WCT_status__c != 'Offer Approved' ) {    
                system.debug('alert3**********');
                setRecEmails.add(offerRecord3.Owner.Email);
                RecTeamEmails3.put(offerRecord3.Owner.Email,offerRecord3.WCT_Team_Mailbox__c);
                system.debug('RecTeamEmails3:'+RecTeamEmails3);
                List<WCT_Offer__c> offerList3 = recOffers3.get(offerRecord3.Owner.Email);
                if(offerList3 == null) {
                    offerList3 = new List<WCT_Offer__c>();
                } 
                offerList3.add(offerRecord3);
                recOffers3.put(offerRecord3.Owner.Email,offerList3);
                
            }
            //-------------------4th---------------------------
            if(rtMap.get(offerRecord4.RecordTypeId).getName() == 'US Campus Hire Offer' && min >= 120 && min < 135 && offerRecord7.WCT_status__c != 'Offer Approved' ) {  
                setRecEmails.add(offerRecord4.Owner.Email);
                RecTeamEmails4.put(offerRecord4.Owner.Email,offerRecord4.WCT_Team_Mailbox__c);
                system.debug('RecTeamEmails4:'+RecTeamEmails4);
                List<WCT_Offer__c> offerList4 = recOffers4.get(offerRecord4.Owner.Email);
                if(offerList4 == null) {
                    offerList4 = new List<WCT_Offer__c>();
                } 
                offerList4.add(offerRecord4);
                recOffers4.put(offerRecord4.Owner.Email,offerList4);
                
            }
            //-------------------5th---------------------------
            if(rtMap.get(offerRecord5.RecordTypeId).getName() == 'US Campus Hire Offer' && min >= 180 && min < 195 && offerRecord7.WCT_status__c != 'Offer Approved' ) { 
                setRecEmails.add(offerRecord5.Owner.Email);
                RecTeamEmails5.put(offerRecord5.Owner.Email,offerRecord5.WCT_Team_Mailbox__c);
                system.debug('RecTeamEmails5:'+RecTeamEmails5);
                List<WCT_Offer__c> offerList5 = recOffers5.get(offerRecord5.Owner.Email);
                if(offerList5 == null) {
                    offerList5 = new List<WCT_Offer__c>();
                } 
                offerList5.add(offerRecord5);
                recOffers5.put(offerRecord5.Owner.Email,offerList5);
                
            }
            //-------------------6th---------------------------
            
            if(rtMap.get(offerRecord6.RecordTypeId).getName() == 'US Campus Hire Offer' && min >= 240 && min < 255 && offerRecord7.WCT_status__c != 'Offer Approved' ) {  
                setRecEmails.add(offerRecord6.Owner.Email);
                RecTeamEmails6.put(offerRecord6.Owner.Email,offerRecord6.WCT_Team_Mailbox__c);
                system.debug('RecTeamEmails6:'+RecTeamEmails6);
                List<WCT_Offer__c> offerList6 = recOffers6.get(offerRecord6.Owner.Email);
                if(offerList6 == null) {
                    offerList6 = new List<WCT_Offer__c>();
                } 
                offerList6.add(offerRecord6);
                recOffers6.put(offerRecord6.Owner.Email,offerList6);
                
            }
            //--------------------7th-----------------------------
            
            if( (rtMap.get(offerRecord7.RecordTypeId).getName() == 'US Campus Hire Offer' || rtMap.get(offerRecord7.RecordTypeId).getName() == 'US Experienced Hire Offer') 
                && mins >= 120 && mins < 145 && offerRecord7.WCT_status__c == 'Offer Approved') { 
                setRecEmails.add(offerRecord7.Owner.Email);
                RecTeamEmails7.put(offerRecord7.Owner.Email,offerRecord7.WCT_Team_Mailbox__c);
                system.debug('sucesses-------');
                List<WCT_Offer__c> offerList7 = recOffers7.get(offerRecord7.Owner.Email);
                if(offerList7 == null) {
                    offerList7 = new List<WCT_Offer__c>();
                } 
                offerList7.add(offerRecord7);
                recOffers7.put(offerRecord7.Owner.Email,offerList7);
            }
          //--------------------8th-----------------------------
            if( (rtMap.get(offerRecord8.RecordTypeId).getName() == 'US Campus Hire Offer' || rtMap.get(offerRecord8.RecordTypeId).getName() == 'US Experienced Hire Offer') 
                && mins >= 240 && mins < 255 && offerRecord8.WCT_status__c == 'Offer Approved') { 
                setRecEmails.add(offerRecord8.Owner.Email);
                RecTeamEmails8.put(offerRecord8.Owner.Email,offerRecord8.WCT_Team_Mailbox__c);
                system.debug('sucesses8-------');
                List<WCT_Offer__c> offerList8 = recOffers8.get(offerRecord8.Owner.Email);
                if(offerList8 == null) {
                    offerList8 = new List<WCT_Offer__c>();
                } 
                offerList8.add(offerRecord8);
                recOffers8.put(offerRecord8.Owner.Email,offerList8);
            }
          //--------------------9th-----------------------------
            if( (rtMap.get(offerRecord9.RecordTypeId).getName() == 'US Campus Hire Offer' || rtMap.get(offerRecord9.RecordTypeId).getName() == 'US Experienced Hire Offer') 
                && mins >= 360 && mins < 375 && offerRecord9.WCT_status__c == 'Offer Approved') { 
                setRecEmails.add(offerRecord9.Owner.Email);
                RecTeamEmails9.put(offerRecord9.Owner.Email,offerRecord9.WCT_Team_Mailbox__c);
                system.debug('sucesses9-------');
                List<WCT_Offer__c> offerList9 = recOffers9.get(offerRecord9.Owner.Email);
                if(offerList9 == null) {
                    offerList9 = new List<WCT_Offer__c>();
                } 
                offerList9.add(offerRecord9);
                recOffers9.put(offerRecord9.Owner.Email,offerList9);
            }
            
            //-------------------------------------------------
        }
        
        String strEmailTop ='';
        String strEmailBody ='';
        String strEmailBottom ='';
        
        for(string strRecEmail: recOffers.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord : recOffers.get(strRecEmail)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 2 Hours to Revise.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord.Name+'</td> <td>'+offerRecord.WCT_full_Name__c+'</td><td>'+offerRecord.WCT_RMS_ID__c+'</td><td>'+offerRecord.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
            }
            list<string> ToEmailAddress = new list<string>();
            List<string> ToCcAddress = new List<string>();
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            ToEmailAddress.add(strRecEmail);
            system.debug('strRecEmail1:'+strRecEmail);
            if(RecTeamEmails != null && RecTeamEmails.get(strRecEmail) != null){
                 ToCcAddress.add(RecTeamEmails.get(strRecEmail));
            }
            system.debug('strRecEmail:'+strRecEmail);
            system.debug('ToCcAddress:'+ToCcAddress);
            
            mail.settoAddresses(ToEmailAddress);
            system.debug('ToEmailAddress:'+ToEmailAddress);
            mail.setccAddresses(ToCcAddress);
            system.debug('ToCcAddress1:'+ToCcAddress);
            mail.setSubject('1st Alert Immediate Action: Offer Letter(s) are pending');
            mail.setHTMLBody('<font color="green">'+strEmailTop+'</font>'+'<font color="green">'+strEmailBody+'</font>'+'<font color="green">'+strEmailBottom+'</font>');
            mail.setSaveAsActivity(false);
            mailList.add(mail);  
        
        }
        //------------------2nd------------------------------------------------------------------------------------------------
        
        for(string strRecEmail2 : recOffers2.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord2 : recOffers2.get(strRecEmail2)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 3 Hours to Revise.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord2.Name+'</td> <td>'+offerRecord2.WCT_full_Name__c+'</td><td>'+offerRecord2.WCT_RMS_ID__c+'</td><td>'+offerRecord2.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord2.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord2.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
            }
            list<string> ToEmailAddress2 = new list<string>();
            List<string> ToCcAddress2 = new List<string>();
            
            Messaging.SingleEmailMessage mail2 = new Messaging.SingleEmailMessage();
            ToEmailAddress2.add(strRecEmail2);
            system.debug('strRecEmail2:'+strRecEmail2);
            if(RecTeamEmails2 != null && RecTeamEmails2.get(strRecEmail2) != null){
                 ToCcAddress2.add(RecTeamEmails2.get(strRecEmail2));
            }
            system.debug('strRecEmail2:'+strRecEmail2);
            system.debug('ToCcAddress:'+ToCcAddress2);
            
            mail2.settoAddresses(ToEmailAddress2);
            system.debug('ToEmailAddress:'+ToEmailAddress2);
            
                ToCcAddress2.add(system.label.OfferAnalystNotification1);
                ToCcAddress2.add(system.label.OfferAnalystNotification2);
                
            mail2.setccAddresses(ToCcAddress2);
            system.debug('ToCcAddress1:'+ToCcAddress2);
            mail2.setSubject('2nd Alert Immediate Action: Offer Letter(s) are pending');
            
            mail2.setHTMLBody('<font color="orange">'+strEmailTop+'</font>'+'<font color="orange">'+strEmailBody+'</font>'+'<font color="orange">'+strEmailBottom+'</font>');
            
            mail2.setSaveAsActivity(false);
            mailList2.add(mail2);  
        
        }
        //------------------3rd------------------------------------------------------------------------------------------------
        
        for(string strRecEmail3 : recOffers3.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord3 : recOffers3.get(strRecEmail3)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 4 Hours to Revise.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord3.Name+'</td> <td>'+offerRecord3.WCT_full_Name__c+'</td><td>'+offerRecord3.WCT_RMS_ID__c+'</td><td>'+offerRecord3.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord3.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord3.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
            }
            list<string> ToEmailAddress3 = new list<string>();
            List<string> ToCcAddress3 = new List<string>();
            
            Messaging.SingleEmailMessage mail3 = new Messaging.SingleEmailMessage();
            ToEmailAddress3.add(strRecEmail3);
            system.debug('strRecEmail3:'+strRecEmail3);
            if(RecTeamEmails3 != null && RecTeamEmails3.get(strRecEmail3) != null){
                 ToCcAddress3.add(RecTeamEmails3.get(strRecEmail3));
            }
            system.debug('strRecEmail3:'+strRecEmail3);
            system.debug('ToCcAddress:'+ToCcAddress3);
            
                ToCcAddress3.add(system.label.OfferAnalystNotification1);
                ToCcAddress3.add(system.label.OfferAnalystNotification2);
                ToCcAddress3.add(system.label.OfferAnalystNotification3);
                
            mail3.settoAddresses(ToEmailAddress3);
            system.debug('ToEmailAddress:'+ToEmailAddress3);
            mail3.setccAddresses(ToCcAddress3);
            system.debug('ToCcAddress1:'+ToCcAddress3);
            mail3.setSubject('3rd Alert Immediate Action: Offer Letter(s) are pending');
            
            mail3.setHTMLBody('<font color="Red">'+strEmailTop+'</font>'+'<font color="Red">'+strEmailBody+'</font>'+'<font color="Red">'+strEmailBottom+'</font>');
            
            mail3.setSaveAsActivity(false);
            mailList3.add(mail3);  
        
        }
        //------------------4th------------------------------------------------------------------------------------------------
       
        for(string strRecEmail4 : recOffers4.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord4 : recOffers4.get(strRecEmail4)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 2 Hours to Revise.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord4.Name+'</td> <td>'+offerRecord4.WCT_full_Name__c+'</td><td>'+offerRecord4.WCT_RMS_ID__c+'</td><td>'+offerRecord4.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord4.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord4.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
            }
            list<string> ToEmailAddress4 = new list<string>();
            List<string> ToCcAddress4 = new List<string>();
            
            Messaging.SingleEmailMessage mail4 = new Messaging.SingleEmailMessage();
            ToEmailAddress4.add(strRecEmail4);
            system.debug('strRecEmail4:'+strRecEmail4);
            if(RecTeamEmails4 != null && RecTeamEmails4.get(strRecEmail4) != null){
                 ToCcAddress4.add(RecTeamEmails4.get(strRecEmail4));
            }
            system.debug('strRecEmail4:'+strRecEmail4);
            system.debug('ToCcAddress:'+ToCcAddress4);
            
            mail4.settoAddresses(ToEmailAddress4);
            system.debug('ToEmailAddress:'+ToEmailAddress4);
            mail4.setccAddresses(ToCcAddress4);
            system.debug('ToCcAddress1:'+ToCcAddress4);
            mail4.setSubject('1st Alert Immediate Action: Offer Letter(s) are pending');
            
            mail4.setHTMLBody('<font color="Green">'+strEmailTop+'</font>'+'<font color="Green">'+strEmailBody+'</font>'+'<font color="Green">'+strEmailBottom+'</font>');
            
            mail4.setSaveAsActivity(false);
            mailList4.add(mail4);  
        
        }
        
        //------------------5th------------------------------------------------------------------------------------------------
        
        for(string strRecEmail5 : recOffers5.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord5 : recOffers5.get(strRecEmail5)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 3 Hours to Revise.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord5.Name+'</td> <td>'+offerRecord5.WCT_full_Name__c+'</td><td>'+offerRecord5.WCT_RMS_ID__c+'</td><td>'+offerRecord5.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord5.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord5.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
            }
            list<string> ToEmailAddress5 = new list<string>();
            List<string> ToCcAddress5 = new List<string>();
            
            Messaging.SingleEmailMessage mail5 = new Messaging.SingleEmailMessage();
            ToEmailAddress5.add(strRecEmail5);
            system.debug('strRecEmail5:'+strRecEmail5);
            if(RecTeamEmails5 != null && RecTeamEmails5.get(strRecEmail5) != null){
                 ToCcAddress5.add(RecTeamEmails5.get(strRecEmail5));
            }
            system.debug('strRecEmail5:'+strRecEmail5);
            system.debug('ToCcAddress:'+ToCcAddress5);
            
            mail5.settoAddresses(ToEmailAddress5);
            system.debug('ToEmailAddress:'+ToEmailAddress5);
            
            ToCcAddress5.add(system.label.OfferAnalystNotification1);
            ToCcAddress5.add(system.label.OfferAnalystNotification2);
            
            mail5.setccAddresses(ToCcAddress5);
            system.debug('ToCcAddress1:'+ToCcAddress5);
            mail5.setSubject('2nd Alert Immediate Action: Offer Letter(s) are pending');
            
            mail5.setHTMLBody('<font color="orange">'+strEmailTop+'</font>'+'<font color="orange">'+strEmailBody+'</font>'+'<font color="orange">'+strEmailBottom+'</font>');
            
            mail5.setSaveAsActivity(false);
            mailList5.add(mail5);  
        
        }
        //------------------6th------------------------------------------------------------------------------------------------
        
        for(string strRecEmail6 : recOffers6.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord6 : recOffers6.get(strRecEmail6)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 4 Hours to Revise.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord6.Name+'</td> <td>'+offerRecord6.WCT_full_Name__c+'</td><td>'+offerRecord6.WCT_RMS_ID__c+'</td><td>'+offerRecord6.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord6.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord6.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
            }
            list<string> ToEmailAddress6 = new list<string>();
            List<string> ToCcAddress6 = new List<string>();
            
            Messaging.SingleEmailMessage mail6 = new Messaging.SingleEmailMessage();
            ToEmailAddress6.add(strRecEmail6);
            system.debug('strRecEmail6:'+strRecEmail6);
            if(RecTeamEmails6 != null && RecTeamEmails6.get(strRecEmail6) != null){
                 ToCcAddress6.add(RecTeamEmails6.get(strRecEmail6));
            }
            system.debug('strRecEmail6:'+strRecEmail6);
            system.debug('ToCcAddress:'+ToCcAddress6);
            
            mail6.settoAddresses(ToEmailAddress6);
            system.debug('ToEmailAddress:'+ToEmailAddress6);
            
                ToCcAddress6.add(system.label.OfferAnalystNotification1);
                ToCcAddress6.add(system.label.OfferAnalystNotification2);
                ToCcAddress6.add(system.label.OfferAnalystNotification3);
                
            mail6.setccAddresses(ToCcAddress6);
            system.debug('ToCcAddress1:'+ToCcAddress6);
            mail6.setSubject('3rd Alert Immediate Action: Offer Letter(s) are pending');
            
            mail6.setHTMLBody('<font color="Red">'+strEmailTop+'</font>'+'<font color="Red">'+strEmailBody+'</font>'+'<font color="Red">'+strEmailBottom+'</font>');
            
            mail6.setSaveAsActivity(false);
            mailList6.add(mail6);  
        
        }
        
        //--------------offer approved------1st----------------------------------------------------------------------------------------------
            
            
        List<WCT_Offer__c> offertrack7 = new List<WCT_Offer__c>();
        for(string strRecEmail7 : recOffers7.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord7 : recOffers7.get(strRecEmail7)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 2 Hours to Revise.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord7.Name+'</td> <td>'+offerRecord7.WCT_full_Name__c+'</td><td>'+offerRecord7.WCT_RMS_ID__c+'</td><td>'+offerRecord7.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord7.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord7.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
            }
            list<string> ToEmailAddress7 = new list<string>();
            List<string> ToCcAddress7 = new List<string>();
            
            Messaging.SingleEmailMessage mail7 = new Messaging.SingleEmailMessage();
            ToEmailAddress7.add(strRecEmail7);
            if(RecTeamEmails7 != null && RecTeamEmails7.get(strRecEmail7) != null){
                 ToCcAddress7.add(RecTeamEmails7.get(strRecEmail7));
            }
            
            mail7.settoAddresses(ToEmailAddress7);
                system.debug('ToEmailAddress7**********'+ToEmailAddress7);
            mail7.setccAddresses(ToCcAddress7);
            mail7.setSubject('1st Alert Immediate Action: Offer Letter(s) are pending');
            mail7.setHTMLBody('<font color="Green">'+strEmailTop+'</font>'+'<font color="Green">'+strEmailBody+'</font>'+'<font color="Green">'+strEmailBottom+'</font>');
            
            mail7.setSaveAsActivity(false);
            mailList7.add(mail7);  
        
        }
        
        //--------------offer approved------3rd-----------------------------------------------------------------------------
          List<WCT_Offer__c> offertrack8 = new List<WCT_Offer__c>();
         for(string strRecEmail8 : recOffers8.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord8 : recOffers8.get(strRecEmail8)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 4 Hours to Revise.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord8.Name+'</td> <td>'+offerRecord8.WCT_full_Name__c+'</td><td>'+offerRecord8.WCT_RMS_ID__c+'</td><td>'+offerRecord8.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord8.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord8.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
            }
            list<string> ToEmailAddress8 = new list<string>();
            List<string> ToCcAddress8 = new List<string>();
            system.debug('strRecEmail8**********'+strRecEmail8);
            Messaging.SingleEmailMessage mail8 = new Messaging.SingleEmailMessage();
            ToEmailAddress8.add(strRecEmail8);
            
            if(RecTeamEmails8 != null && RecTeamEmails8.get(strRecEmail8) != null){
                 ToCcAddress8.add(RecTeamEmails8.get(strRecEmail8));
            }
                        
            mail8.settoAddresses(ToEmailAddress8);
                system.debug('ToEmailAddress8**********'+ToEmailAddress8);
            ToCcAddress8.add(system.label.OfferAnalystNotification1);
            ToCcAddress8.add(system.label.OfferAnalystNotification2);
            
            mail8.setccAddresses(ToCcAddress8);
            
            mail8.setSubject('2nd Alert Immediate Action: Offer Letter(s) are pending');
            
            mail8.setHTMLBody('<font color="orange">'+strEmailTop+'</font>'+'<font color="orange">'+strEmailBody+'</font>'+'<font color="orange">'+strEmailBottom+'</font>');
            
            mail8.setSaveAsActivity(false);
            mailList8.add(mail8);  
        
         }
        //--------------offer approved------2nd-----------------------------------------------------------------------------
          List<WCT_Offer__c> offertrack9 = new List<WCT_Offer__c>();
        for(string strRecEmail9 : recOffers9.keyset()) {
            strEmailBody ='';
            for(WCT_Offer__c offerRecord9 : recOffers9.get(strRecEmail9)) {
                
                strEmailTop  += '<!DOCTYPE html> <html> <head> <style> table,th,td { border: 1px solid black; border-collapse:collapse; }, th,td { padding:5px; } </style> </head> <body> <br> Dear Analyst,<br> <br>';
                strEmailTop  += 'Offer Letter(s) are pending for more than 6 Hours to Revise.';
                strEmailTop  += ' <br> <br> <br> <table> <thead> <tr> <th>Offer Name</th> <th>Candidate Name</th> <th>RMS ID</th> <th>Candidate Email</th> <th>Status</th> <th>Offer letter link</th> </tr> </thead> <tbody>';
                
                strEmailBody += '<tr> <td>'+offerRecord9.Name+'</td> <td>'+offerRecord9.WCT_full_Name__c+'</td><td>'+offerRecord9.WCT_RMS_ID__c+'</td><td>'+offerRecord9.WCT_Candidate_Email__c+'</td>';
                strEmailBody += '<td>'+offerRecord9.WCT_status__c+'</td><td><a href='+Label.BaseURL+'/'+offerRecord9.id+'>URL</a></td></tr>';
                
                strEmailBottom += '</tbody> </table> <br> Thank you,<br> Deloitte Recruting.<br> </body> </html>';
                
            }
            list<string> ToEmailAddress9 = new list<string>();
            List<string> ToCcAddress9 = new List<string>();
            
            Messaging.SingleEmailMessage mail9 = new Messaging.SingleEmailMessage();
            ToEmailAddress9.add(strRecEmail9);
            if(RecTeamEmails9 != null && RecTeamEmails9.get(strRecEmail9) != null){
                 ToCcAddress9.add(RecTeamEmails9.get(strRecEmail9));
            }
            
            mail9.settoAddresses(ToEmailAddress9);
              system.debug('ToEmailAddress9**********'+ToEmailAddress9);
                ToCcAddress9.add(system.label.OfferAnalystNotification1);
                ToCcAddress9.add(system.label.OfferAnalystNotification2);
                ToCcAddress9.add(system.label.OfferAnalystNotification3);
                
            mail9.setccAddresses(ToCcAddress9);
            mail9.setSubject('3rd Alert Immediate Action: Offer Letter(s) are pending');
            mail9.setHTMLBody('<font color="Red">'+strEmailTop+'</font>'+'<font color="Red">'+strEmailBody+'</font>'+'<font color="Red">'+strEmailBottom+'</font>');
            
            mail9.setSaveAsActivity(false);
            mailList9.add(mail9);  
        
        }
        //------------------------------------------------------------------------------------------------------------------
        
            Messaging.sendEmail(mailList); 
            Messaging.sendEmail(mailList2);
            Messaging.sendEmail(mailList3);
            Messaging.sendEmail(mailList4); 
            Messaging.sendEmail(mailList5);
            Messaging.sendEmail(mailList6);
            Messaging.sendEmail(mailList7);
            Messaging.sendEmail(mailList8);
            Messaging.sendEmail(mailList9);
    }

    global void finish(Database.BatchableContext BC){
      
    }
}