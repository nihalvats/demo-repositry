/********************************************************************************************************************************
Apex class         : <WCT_Enroll_Benefits_FormController>
Description        : <Controller which allows to Update Task and Upload Attachments>
Type               :  Controller
Test Class         : <WCT_Enroll_Benefits_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 23/05/2016          85%                          --                            License Cleanup Project
************************************************************************************************************************************/   

public class WCT_Enroll_Benefits_FormController extends SitesTodHeaderController{
    public task taskRecord{get;set;}
    public WCT_Task_Reference_Table__c taskRefRecord{get;set;}
    public String taskSubject {get;set;}
    public String taskid{get;set;}
    public boolean display{get;set;}
    public boolean checked{get;set;}
    public String taskVerbiage{get;set;}
    
     /* public variables */
    public WCT_Mobility__c MobilityRec {get;set;}
    
    // Error Message related variables
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    
    public WCT_Enroll_Benefits_FormController()
    {
         /*Initialize all Variables*/
        init();
        
        taskid = ApexPages.currentPage().getParameters().get('taskid');
        display=true;
        checked=false;
        taskSubject = '';
        taskVerbiage = '';
    }
/********************************************************************************************
*Method Name         : <init()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Init() Used for loading Mobility>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/ 
    private void init(){

        //Custom Object Instances
        MobilityRec = new WCT_Mobility__c();

    } 
/********************************************************************************************
*Method Name         : <UpdateTaskFlags()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <UpdateTaskFlags() Used for fetching Task & Mobility record to update taskflags>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/   
    public void updateTaskFlags()
    {
        if(taskid==''|| taskid==null){
            display=false;
            return;
        }

        taskRecord=[SELECT id, Subject, WhatId, Ownerid, Status, WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c, WCT_Task_Reference_Table_ID__c FROM Task WHERE Id =: taskid];
        taskRefRecord = [SELECT Id, Name, WCT_Task_for_Object__c, WCT_Task_Subject__c, Form_Verbiage__c, ownerId FROM WCT_Task_Reference_Table__c Where id=:taskRecord.WCT_Task_Reference_Table_ID__c];
        MobilityRec = [SELECT Id, Name, OwnerId FROM WCT_Mobility__c WHERE Id=:taskRecord.WhatId];

        //Get Task Subject
        taskSubject = taskRecord.Subject;
        taskVerbiage = taskRefRecord.Form_Verbiage__c;

    }
/********************************************************************************************
*Method Name         : <save()>
*Return Type         : <PageReference>
*Param’s             : 
*Description         : <Save() Used to Insert & Update Task Records>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/    
    public pageReference save()
    {

        /*Custom Validation Rules are handled here*/

        if(checked != true)
        {
            pageErrorMessage = 'Please check the checkbox in order to submit the form.';
            pageError = true;
            return null;
        }

       // update MobilityRec;
         try{   
        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        taskRecord.OwnerId=UserInfo.getUserId();
        upsert taskRecord;

        if(string.valueOf(MobilityRec.OwnerId).startsWith('00G')){
            taskRecord.OwnerId = System.Label.GMI_User;
        }else{
            taskRecord.OwnerId = MobilityRec.Ownerid;
        }
        

        //updating task record
        if(taskRecord.WCT_Auto_Close__c == true){   
            taskRecord.status = 'Completed';
        }else{
            taskRecord.status = 'Employee Replied';  
        }

        taskRecord.WCT_Is_Visible_in_TOD__c = false; 
        upsert taskRecord;
        
        //Reset Page Error Message
        pageError = false;
        }
        catch (Exception e) {
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Enroll_Benefits_FormController', 'Enroll Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
                
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_EBF_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
        }
        Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_EBF_MSG');
        pg.setRedirect(true);
        return pg;
    }
}