/*****************************************************************************************
    Name    : DependentController_Test
    Desc    : Test class for DependentController class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Ashish Shishodiya                 09/11/2013         Created 

******************************************************************************************/
@isTest
public class DependentController_Test {
  /****************************************************************************************
    * @author      - Ashish Shishodiya
    * @date        - 09/11/2013
    * @description - Test Method for all methods of OtherContactCtrl Class 
    *****************************************************************************************/

    public static testmethod void DependentTestController(){
      // creating contacts
      Test_Data_Utility.createContact();
      //querying one contact
      Contact c = [Select Id from Contact limit 1];
    //private RecordType rDependant = [Select r.Name From RecordType r where name ='Dependant'];
    //private RecordType rEmergency = [select name from recordtype where name = 'Emergency Contact'];
    
    List<Contact> lstContact = new List<Contact>();
    c.LastName = 'Test Employee';
    c.WCT_Contact_Type__c = 'Employee'; 
    lstContact.add(c);
    
    Contact cDep = new Contact();
    cDep.lastName = 'Test Dependent';
    cDep.WCT_Primary_Contact__c = c.id;
    cDep.WCT_Contact_Type__c = 'Dependent';
    lstContact.add(cDep);
    
    Contact cEm = new Contact();
    cEm.lastName = 'Test Emergency';
    cEm.WCT_Primary_Contact__c = c.id;
    cEm.WCT_Contact_Type__c = 'Emergency';
    lstContact.add(cEm);
    
    upsert lstContact;
    
      //setting id and phone parameters
     // System.currentPageReference().getParameters().put('id',c.Id);
      test.startTest();
      // instantiating the controller class
      
      ApexPages.StandardController controller = new ApexPages.StandardController(c);            
      DependentController con = new DependentController(controller);
      
    }
}