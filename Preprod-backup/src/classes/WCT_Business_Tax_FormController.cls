/********************************************************************************************************************************
Apex class         : <WCT_Business_Tax_FormController>
Description        : <Controller which allows to Update Mobility and Task>
Type               :  Controller
Test Class         : <WCT_Business_Tax_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 25/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/ 
public class WCT_Business_Tax_FormController extends SitesTodHeaderController{

    // PUBLIC VARIABLES
    public WCT_Mobility__c mobilityRecord{get;set;}
  
    // UPLOAD RELATED VARIABLES
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public GBL_Attachments attachmentHelper{get; set;}
    public List<Attachment> listAttachments {get; set;}
    public Map<Id, String> mapAttachmentSize {get; set;}  
    public Integer countattach{get;set;}
    
    
   // TASK RELATED VARIABLES
    public task t{get;set;}
    public String taskid{get;set;}
    public String taskVerbiage{get;set;}
    public WCT_Task_Reference_Table__c taskRefRecord{get;set;}
   

   // ERROR MESSAGE RELATED VARIABLES
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
 
      // DEFINING A CONSTRUCTOR 
    public WCT_Business_Tax_FormController()
    {
        init();
        attachmentHelper= new GBL_Attachments();   
        getParameterInfo();  

        if(taskid=='' || taskid==null)   
        {
           invalidEmployee=true;
           return;
        }

        getTaskInstance();
        getMobilityDetails();
        taskVerbiage = '';
    }

    /********************************************************************************************
    *Method Name         : <init()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <Init() Used for loading Mobility and Attachments>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/ 
  @Testvisible
   private void init(){
        mobilityRecord = new  WCT_Mobility__c();
        countattach = 0;
        doc = new Document();
        listAttachments = new List<Attachment>();
        mapAttachmentSize = new Map<Id, String>();

      
    }   
    
    /********************************************************************************************
    *Method Name         : <getParameterInfo()>
    *Return Type         : <Null>
    *Param’s             : URL
    *Description         : <GetParameterInfo() Used to get URL Params for TaskID>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/     
  @Testvisible
   private void getParameterInfo(){
        taskid = ApexPages.currentPage().getParameters().get('taskid');
        if(invalidEmployee||taskid==''||taskid==null)
            {
              invalidEmployee=true;
               return;
            }
    }
 
    /********************************************************************************************
    *Method Name         : <getTaskInstance()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <GetTaskInstance() Used for Querying Task Instance>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/ 
@Testvisible
    private void getTaskInstance(){
        t=[SELECT Status,OwnerId,WhatId,WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c,WCT_Task_Reference_Table_ID__c   FROM Task WHERE Id =: taskid];
        taskRefRecord = [SELECT Id, Name, WCT_Task_for_Object__c, WCT_Task_Subject__c, Form_Verbiage__c, ownerId FROM WCT_Task_Reference_Table__c Where id=:t.WCT_Task_Reference_Table_ID__c];
        taskVerbiage = taskRefRecord.Form_Verbiage__c;
    }
    
    /********************************************************************************************
    *Method Name         : <getMobilityDetails()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <GetMobilityDetails() Used for Fetching Mobility Details>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/       
 
    public void getMobilityDetails()
    {
        mobilityRecord = [SELECT id,OwnerId FROM WCT_Mobility__c
                            where id=:t.WhatId];
                           
    }
    
    /********************************************************************************************
    *Method Name         : <save()>
    *Return Type         : <PageReference>
    *Param’s             : 
    *Description         : <Save() Used for for Updating Task and Immigration>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/
 
    public pageReference save()
    {
        try{    
        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        t.OwnerId=UserInfo.getUserId();
        upsert t;
        
 system.debug('Doclist******'+attachmentHelper.docIdList.size());
 system.debug('Docbody******'+attachmentHelper.doc.body);
 system.debug('Doclist Isempty******'+attachmentHelper.docIdList.isEmpty());
         if(attachmentHelper.docIdList.isEmpty()) {
            pageErrorMessage = 'Attachment is required to submit the form.';
            pageError = true;
            return null;

        }
        attachmentHelper.uploadRelatedAttachment(t.id);
     

        if(string.valueOf(mobilityRecord.Ownerid).startsWith('00G')){
            t.Ownerid = System.Label.GMI_User;
        }else{
            t.OwnerId = mobilityRecord.Ownerid;
        }

        //updating task record
        if(t.WCT_Auto_Close__c == true){   
            t.status = 'completed';
        }else{
            t.status = 'Employee Replied';  
        }

        t.WCT_Is_Visible_in_TOD__c = false; 
        upsert t;
       

        pageError = false;
        }
        catch (Exception e) {
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Business_Tax_FormController', 'Business Tax Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_BFORM_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
        }
        PageReference pageRef = new PageReference('/apex/WCT_Business_Tax_FormThankyou?taskid='+ t.id);
        pageRef.setRedirect(true);
        return pageRef;
      
    }

       
    
}