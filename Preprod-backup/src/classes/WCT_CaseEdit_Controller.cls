public with sharing class WCT_CaseEdit_Controller {
	public Case caseEdit {get;set;}

    public WCT_CaseEdit_Controller() {
        caseEdit = [SELECT contactId,Id, WCT_ResolutionNotes__c FROM Case 
                   WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    }

    public Case getCase() {
    	//System.debug('***SFDC: Old Value: ' + caseEdit);
        return caseEdit;
    }

    public PageReference save() {
    	//System.debug('***SFDC: New Value: ' + caseEdit);
        update caseEdit;
        return null;
    }
}