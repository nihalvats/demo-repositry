global class WCT_ImmigrationUpdateSchedulableBatch implements Database.Batchable<sObject>,Schedulable, Database.Stateful{

    public string strSql{get;set;}
//-------------------------------------------------------------------------------------------------------------------------------------
//    Constructor
//-------------------------------------------------------------------------------------------------------------------------------------
    
    public WCT_ImmigrationUpdateSchedulableBatch(){

    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Scheduler Execute 
//-------------------------------------------------------------------------------------------------------------------------------------
    global void execute(SchedulableContext SC) {
        WCT_ImmigrationUpdateSchedulableBatch batch = new  WCT_ImmigrationUpdateSchedulableBatch();
        ID batchprocessid = Database.executeBatch(batch,200);
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Set the query
//-------------------------------------------------------------------------------------------------------------------------------------
    global Database.QueryLocator start(Database.BatchableContext bc){
    
        string strSql = 'select id, WCT_Fragomen_Milestone_Change__c, WCT_Petition_End_Date__c, WCT_Visa_Expiration_Date__c, WCT_Request_for_Evidence_Due_Date__c , WCT_Request_Initiation_Date__c,wct_immigration_status__c,WCT_RFE_Date__c,WCT_Petition_Start_Date__c from WCT_Immigration__c where ' +
                ' H1B_Is_USI_Upload__c=true and ( (WCT_Request_Initiation_Date__c <= ' + string.valueof(system.today().addDays(-90)) + ' and wct_immigration_status__c = \'Petition In Progress\')'+
                '  or (WCT_Request_Initiation_Date__c <= ' + string.valueof(system.today().addDays(-150)) + ' and wct_immigration_status__c = \'Petition On Hold\')'+
                '  or (WCT_Immigration_Status__c = \'Request For Evidence\' and WCT_RFE_Date__c <= ' + string.valueof(system.today().addDays(-42)) + ')' +
                '  or (WCT_Petition_End_Date__c <=' + string.valueof(system.today()) + ')'+
                '  or (WCT_Visa_Expiration_Date__c != null and WCT_Visa_Expiration_Date__c <' + string.valueof(system.today()) + ')'+
                '  or (WCT_Request_for_Evidence_Due_Date__c <='+ string.valueof(system.today())+') )';  
        System.debug('## Query '+strSql);
       return database.getQuerylocator(strSql);   
       
    }
    
//-------------------------------------------------------------------------------------------------------------------------------------
//    Database execute method to Process Query results for email notification of tasks
//-------------------------------------------------------------------------------------------------------------------------------------
    global void execute(Database.batchableContext bc, List<sObject> scope){
    // Update to petition on hold
        List<WCT_Immigration__c > lstImmigration = new List<WCT_Immigration__c >();
        for(sObject tmp : scope){
            WCT_Immigration__c tmpImm = new WCT_Immigration__c();
            tmpImm = (WCT_Immigration__c )tmp;
            lstImmigration.add(tmpImm);
            if(tmpImm.WCT_Request_Initiation_Date__c <=system.today().addDays(-90) && tmpImm.wct_immigration_status__c == 'New')
                tmpImm.wct_immigration_status__c = 'Petition On Hold';
            else if(tmpImm.WCT_Request_Initiation_Date__c <= system.today().addDays(-150) && tmpImm.wct_immigration_status__c == 'Petition On Hold')    
                tmpImm.wct_immigration_status__c = 'Petition Canceled';
            else if(tmpImm.WCT_Petition_End_Date__c  <=system.today())
                tmpImm.wct_immigration_status__c = 'Petition Expired';
            else if(tmpImm.WCT_Fragomen_Milestone_Change__c<>true && tmpImm.WCT_Request_for_Evidence_Due_Date__c <=system.today() && tmpImm.wct_immigration_status__c == 'Request For Evidence') 
                tmpImm.wct_immigration_status__c = 'Request For Evidence Expired';
            else if(tmpImm.WCT_RFE_Date__c <= system.today().addDays(-42) && tmpImm.WCT_Immigration_Status__c == 'Request For Evidence')
                tmpImm.WCT_Check_for_Email__c = true; 
            else if(tmpImm.WCT_Visa_Expiration_Date__c < system.today() && tmpImm.WCT_Visa_Expiration_Date__c != null)
                tmpImm.WCT_Immigration_Status__c = 'Visa Expired';  
                 
        }
        Database.SaveResult[] sr = Database.update(lstImmigration, false);
        
       /* List<Task> lstTask= [select id,whatId,createdDate,subject from task where whoId!=null and ((createdDate <= :system.today().addDays(-10) and subject ='Submit Visa questionnaire on Fragomen Portal') )and status <> 'Completed' and RecordType.name='Immigration'];
        Map<Id,Task> mapIdTask = new Map<Id,Task>(lstTask);         
        set<Id> setImmIds = new set<Id>();
        for(Task tmpTask : lstTask)
            setImmIds.add(tmpTask.whatId); 
        lstImmigration = [select id , wct_immigration_status__c  from WCT_Immigration__c where id in : setImmIds];
        for(WCT_Immigration__c tmpImm : lstImmigration ){
                tmpImm.wct_immigration_status__c ='Visa Application Canceled';
           
        }    
        if(!lstImmigration.isEmpty())
            update lstImmigration ;   */    
    }
    
    global void finish(Database.batchableContext info){     
   
    }     
}