/**************************************************************************************
Apex Class Name: WCT_batchUpdateReOpenCases
Version          : 1.0 
Created Date     : 12 Dec 2013
Function         : This class is a batch class to update all cases to un check Unread_Email_on_Closed_Case__c which 
                    are in either In-progress   or ReOpened status and don't have any un read email
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                    12/11/2013              Original Version
*************************************************************************************/
global class WCT_BatchUpdateReOpenCases implements Database.Batchable<sObject>{
    //Variable Declaration
    
    list<case> liCasesToUpdateBatch;
    public String query;
     
    //Constructor to prepare Query to fetch Case Records
    global WCT_BatchUpdateReOpenCases(string Querystr){
        query = Querystr;
    }

    //Start Method ,it returns a list of cases which needs to be updated to Execute method
    global Database.QueryLocator start(Database.BatchableContext BC){
            return Database.getQueryLocator(query);
    }

    //Execute method to update case's
    global void execute(Database.BatchableContext BC,List<Case> caseList){
        liCasesToUpdateBatch = new list<case>();
        for(Case caseTemp:caseList){
            if(!caseTemp.EmailMessages.isEmpty()){
                Boolean flag = true;
                for(EmailMessage em:caseTemp.EmailMessages){
                    if(em.Status == '0'){
                        flag=false;
                        break;
                    }
                    
                }
                if(flag){
                        Case ca = new Case();
                        ca = caseTemp;
                        ca.Unread_Email_on_Closed_Case__c = false;
                        liCasesToUpdateBatch.add(ca);
                }
            }
        
        }
    
        
        if(!liCasesToUpdateBatch.isEmpty()){
            DataBase.SaveResult[] srList = DataBase.Update(liCasesToUpdateBatch,false);
            for(Database.SaveResult sr: srList){
                    if(!sr.IsSuccess()){
                        for(Database.Error err : sr.getErrors()){
                        WCT_ExceptionUtility.logException('WCT_batchUpdateReOpenCases','Batch class',err.getMessage());
                        }
                    }
                
                }
        }
    
    }
    //Finish method to send email and do post execution task's on completion of batch
    global void finish(Database.BatchableContext BC ) {
        
    }

}