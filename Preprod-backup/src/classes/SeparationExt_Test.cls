@isTest
public class SeparationExt_Test {
    public static ELE_Separation__c elesepobj;
    public static void createeleseprec()
    {
        contact cnt=WCT_UtilTestDataCreation.createContact();
        insert cnt;
        elesepobj=new ELE_Separation__c();
        elesepobj.ELE_Contact__c=cnt.id;
        elesepobj.ELE_FSS__c='Global';
        elesepobj.ELE_Region__c='USI';
        elesepobj.ELE_Location__c='Gurgaon';
        elesepobj.Employee_Comment_Unread__c=true;
        insert elesepobj;
    }
    public static testmethod void elesep()
    {
        pagereference pg=page.ele_comment_read;
        test.setCurrentPage(pg);
        createeleseprec();
        ApexPages.Standardcontroller sp = new ApexPages.Standardcontroller(elesepobj);
        ApexPages.currentPage().getparameters().put('id',elesepobj.id);
        SeparationExt Spext=new SeparationExt(sp);
        Spext.updateEmpcmmt();
    }
    
        public static testmethod void clrchk()
    {
        pagereference pg=page.ele_clearance_comment;
        test.setCurrentPage(pg);
        createeleseprec();
        Clearance_separation__c clrobj=new Clearance_separation__c();
        clrobj.ELE_Separation__c=elesepobj.id;
        clrobj.Stakeholder_Comment_Unread__c=true;
        insert clrobj;
        ApexPages.Standardcontroller sp = new ApexPages.Standardcontroller(clrobj);
        ApexPages.currentPage().getparameters().put('id',clrobj.id);
        SeparationExt Spext=new SeparationExt(sp);
        Spext.updateEmpcmmt();
    }
}