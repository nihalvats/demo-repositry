@isTest
public class Skills_Extractcon_Test 
{

    public static testmethod void test1()
    {
            /*Test Data*/
         /**/
          Contact contactTemp = WCT_UtilTestDataCreation.createAdhocContactsbyEmail('testUser@deloitte.com.test');
            contactTemp.LastName ='TestUser';
            contactTemp.Skills_Extract__c='test-----3&&&&&test2-----3';
            insert contactTemp;
            System.debug('### contactTemp'+contactTemp);
            
            PageReference pRef = Page.Skills_Extract;
        	Test.setCurrentPage(pRef);
       		String em_encoded=EncodingUtil.urlDecode(CryptoHelper.encrypt(contactTemp.email), 'UTF-8');
            ApexPages.currentPage().getParameters().put('em',em_encoded);
			Skills_Extractcon extrac= new Skills_Extractcon();
        	extrac.exportskills();
    }
}