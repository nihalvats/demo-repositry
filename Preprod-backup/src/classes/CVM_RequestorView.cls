/*

Project : VRM Contractor Management
Associated VF Page : CVM_My_Request
Description:Controller which handles the view for the requestor for its Request
Test Class : CVM_Class_Test
Author : Karthik Raju Gollapalli

*/


global with sharing class CVM_RequestorView extends SitesTodHeaderController
{
    public String setBtnValue{set;get;}//holds the record id
    public contact conObj{get;set;}
    public string emValue {get;set;}
    public string reqid {get;set;}
    public string requestNumber {get;set;}
    public CVM_Contractor_Request__c treq{get;set;} // holds the record value of CVM_Contractor_Request__c
    public list<CVM_Contractor_Request__c> newValue{get;set;}//holds the list of records of CVM_Contractor_Request__c
    public list<CVM_Contractor_Request__c> reportingList{get;set;}// holds the list of records of CVM_Contractor_Request__c
    public string comment{get;set;}// holds the reopen comment value
    ApexPages.StandardController controller;// standard controller
    Public decimal promptness{get;set;}
    Public decimal accuracy{get;set;}
    Public decimal helpfullness{get;set;}
    Public string commentbox{get;set;} 
    
    Public String siteurl{get;set;}
    Public String frontdoor{get;set;}
    
    
    // constructor
    public  CVM_RequestorView()
    {
        init();
        getReportingInfo();
        
    }
    
    public Pagereference gotohomepage()
    {    
        system.debug('## gotohomepage');
        PageReference MyRequestsPageRef = Page.todNewContractor;
        MyRequestsPageRef.getParameters().put('em',ApexPages.currentPage().getParameters().get('em'));
        MyRequestsPageRef.setRedirect(true);
        return MyRequestsPageRef;
    }
    
    public Pagereference displayrequest()
    {    
        system.debug('## gotohomepage');
        /*PageReference MyRequestsPageRef = Page.CVM_Request_View_Page;        
        MyRequestsPageRef.getParameters().put('id',ApexPages.currentPage().getParameters().get('reqid'));
        MyRequestsPageRef.getParameters().put('rec',ApexPages.currentPage().getParameters().get('rectype'));
        MyRequestsPageRef.getParameters().put('rnum',ApexPages.currentPage().getParameters().get('requestNumber'));
        MyRequestsPageRef.setRedirect(true);*/
        //PageReference MyRequestsPageRef = System.Label.Site_Base_URL+System.Label.Internal_FrontDoor+"?key=MyRequest";
        //ApexPages.currentPage().getParameters().put(System.Label.Site_Base_URL);
        siteurl = System.Label.Site_Base_URL;
        frontdoor = System.Label.Internal_FrontDoor;
        system.debug('## site url is'+siteurl);
        system.debug('## frontdoor is'+frontdoor);
        Pagereference ref = new Pagereference(siteurl+frontdoor+'?key=DisplayRequest');
        system.debug('## ref is'+ref);
        ref.getParameters().put('id',ApexPages.currentPage().getParameters().get('reqid'));
        ref.getParameters().put('rec',ApexPages.currentPage().getParameters().get('rectype'));
        ref.getParameters().put('rnum',ApexPages.currentPage().getParameters().get('requestNumber'));
        return ref;
        //return MyRequestsPageRef;
    }
    
    
    // constructor code method
    public Void Init()
    {
        //Initializing the Reporting object
        Treq = new CVM_Contractor_Request__c();
        //emValue = ApexPages.currentPage().getParameters().get('em');
        reqid = ApexPages.currentPage().getParameters().get('id');
        //string userEmail = cryptoHelper.decrypt(emValue);
        string userEmail = loggedInContact.email;
        conObj = [select id,name, email from contact where email = : userEmail limit 1];
        // Initilizing the list for holding reporting values
        newValue = new list<CVM_Contractor_Request__c> ();
        setBtnValue = null;
    }
    
    // Method which returns the requested requests by the requestor
    public list<CVM_Contractor_Request__c> getReportingInfo()
    {
        // List the pass the values in Apex Page
        system.debug('loggedInContact111'  +loggedInContact);
        if(loggedInContact != null)
        {
            reportingList=[Select id,Name,CVM_Request_Type1__c,CVM_Additional_Comments_Requests_Notes__c,CVM_Request_Status__c,CVM_On_Behalf_Of__r.Name,CVM_Request_ID__c,CVM_Request_Date__c,CVM_Tentative_End_Date__c,CVM_Tentative_Start_Date__c,CVM_Request_Type__c,CVM_Requestor_Name__c from CVM_Contractor_Request__c where CVM_Requested_By__c=:loggedInContact.Id or CVM_On_Behalf_Of__c =:loggedInContact.Id ];
        }
        return reportingList;
        
    }    
}