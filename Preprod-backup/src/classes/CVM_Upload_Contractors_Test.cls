@isTest
public class CVM_Upload_Contractors_Test{
       
    
   static testMethod void CVM_Upload_Contractors_TestMethod(){  
       integer size = 0;  
       
       String csvcontent = 'Last Name,First Name,Project Name,Project Start Date,Project End Date,Project Location,'+
           'Project: New or Current and adding,Project Language,Contractor:  New or Returning,'+
           'Supplier Name,Supplier Pay Rate,Supplier Bill Rate to Deloitte,Personnel Number (SAP Account #),Expiration Date (Of SAP Account )';
       string   data = 'test1,Test1,Project One,10/12/2015,12/15/2015,Hyderabad,'+
           'New,English,New,TestName,$10.00,$12.50,428497,12/12/2016';
       String blobCreator = csvcontent + '\r\n' + data ;
       
       WCT_parseCSV parseCSVInstance = new WCT_parseCSV();  
       string namefile = 'test.csv';
       List<List<String>> fileLines = new List<List<String>>(); 
       filelines = parseCSVInstance.parseCSV(nameFile, true);
       string extension = namefile.substring(namefile.lastindexof('.')+1);       
       test.startTest();
       pagereference p = page.CVM_Upload_Contractors;
       Test.SetCurrentPage(p); 
       list<Contact> wlistg = new list<Contact>();
       
       CVM_Upload_Contractors cont= new CVM_Upload_Contractors();
       cont.contentFile = Blob.valueof(blobCreator);
       cont.ReadFile();
       cont.uploadProcess();
       cont.pg();
       cont.getFailedRows();
       ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error');
       ApexPages.addMessage(msg);

       list<contact> lst= new list<contact>();
       Database.SaveResult[] srList = Database.insert(lst, false);
       contact ct = new contact();
       ct.LastName = 'test1';
       ct.FirstName = 'Test1';
       ct.WCT_Project_Name__c = 'Project One' ;
       ct.CVM_Project_Start_Date__c = date.parse('10/12/2015');
       ct.CVM_Project_End_Date__c = date.parse('12/15/2015');
       ct.CVM_Project_Location__c = 'Hyderabad';
       ct.CVM_Project_New_Current__c = 'New';
       ct.CVM_Project_Language__c = 'English';
       ct.CVM_Contractor_New_Returning__c = 'New'; 
   
       ct.CVM_Supplier_Name__c='TestName';
       ct.CVM_Supplier_Pay_Rate__c=decimal.valueOf('10.00');
       ct.CVM_Supplier_Bill_Rate_to_Deloitte__c=decimal.valueOf('12.50');
       ct.WCT_Personnel_Number__c=decimal.valueOf('428497');
       ct.CVM_Expiration_Date_Of_SAP_Account__c=date.parse('12/12/2016');
       
       lst.add(ct);
       insert lst;
      
       test.stoptest();
       
   }
  }