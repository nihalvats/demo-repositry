@isTest
public class WCT_ImmigrationUpdateSchedBatch_Test 
{


 static testMethod void Test_WCT_ImmigrationUpdateSchedulableBatch()
 
  {
 // Test.startTest();
     string  strSql;
        WCT_Mobility__c mobRec = new WCT_Mobility__c();
        mobRec.WCT_Total_Family_Size_at_Home__c = 2;
        mobRec.WCT_Mobility_Status__c = 'New';
        insert mobRec ;
        
        Contact ct = WCT_UtilTestDataCreation.createEmployee(WCT_Util.getRecordTypeIdByLabel('Contact','Employee'));
         insert ct;
    Set<Id> stagingIds = new Set<id>();
   
  
   
   
        WCT_Immigration__c immi = new WCT_Immigration__c();
       // immi.WCT_Assignment_Owner__c=im;
       // immi.ownerId=UserInfo.getUserId();
        immi.WCT_Visa_Type__c = 'L1A Individual';
        
        immi.WCT_Assignment_Owner__c = ct.id;
        immi.WCT_Request_Initiation_Date__c = system.today().addDays(-59);
         immi.WCT_Request_for_Evidence_Due_Date__c = system.today();
      //  immi.WCT_Immigration_Status__c = 'Petition In Progress';
        immi.WCT_Petition_Start_Date__c = Date.today();
        immi.WCT_Petition_End_Date__c   = system.today().addDays(-5);
       // immi.WCT_Fragomen_Milestone_Change__c=true;
        immi.WCT_Request_for_Evidence_Due_Date__c =   system.today().addDays(-10); 
        immi.WCT_Visa_Expiration_Date__c =  system.today().addDays(-15);
        immi.wct_immigration_status__c = 'Request For Evidence';
        insert  immi;
        
   WCT_Immigration__c immi1 = new WCT_Immigration__c(); 
    immi1.wct_immigration_status__c = 'Request For Evidence';
    immi1.WCT_Fragomen_Milestone_Change__c=true;
    immi1.WCT_Request_for_Evidence_Due_Date__c = system.today();
        insert  immi1;  
      
        
     Test.startTest();      
      immi.wct_immigration_status__c = 'Petition Expired'; 
        update immi;
       
      immi.wct_Immigration_Status__c = 'Visa Expired';
      update immi; 
      
      immi.wct_immigration_status__c = 'Request For Evidence Expired';
      update immi;
      
      immi1.wct_immigration_status__c = 'Request For Evidence Expired';
      update immi1;
      WCT_ImmigrationUpdateSchedulableBatch createCon = new WCT_ImmigrationUpdateSchedulableBatch();
     // Task task = new Task();
      system.schedule('New','0 0 2 1 * ?',createCon); 
    
          Test.stopTest();   
        

     }
     
      
  }