@isTest
public class WCT_BusinessRecordEmail_Test
{
    static testMethod void m1(){
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        
        mob.WCT_Travel_Start_Date__c=system.today().addDays(2);
        insert mob;
        Test.startTest();
        WCT_BusinessRecordEmailSchedulableBatch createCon = new WCT_BusinessRecordEmailSchedulableBatch();
        system.schedule('New','0 0 2 1 * ?',createCon); 
        Test.stopTest(); 
     } 
     static testMethod void m2(){
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        
        mob.WCT_Travel_Start_Date__c=system.today();
        insert mob;
        Test.startTest();
        WCT_BusinessRecordEmailSchedulableBatch createCon = new WCT_BusinessRecordEmailSchedulableBatch();
        system.schedule('New1','0 0 2 1 * ?',createCon); 
        Test.stopTest(); 
     }
     static testMethod void m3(){
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        
        mob.WCT_Travel_Start_Date__c=system.today().addDays(2);
        insert mob;
        Test.startTest();
        WCT_BusinessRecordEmailSchedulableBatch createCon = new WCT_BusinessRecordEmailSchedulableBatch();
        system.schedule('New3','0 0 2 1 * ?',createCon); 
        Test.stopTest(); 
     } 
      
}