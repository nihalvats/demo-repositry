public  class ToDAttachmentsDisplayController extends SitesTodHeaderController {
    public List<Attachment> listAttachments {get; set;}
    public Case createdCase {get; set;}
    public Map<Id, String> mapAttachmentSize {get; set;}  
   
    public ToDAttachmentsDisplayController() {
        if(invalidEmployee) {
            return;
        }
        String parentId = ApexPages.currentPage().getParameters().get('id'); 
       createdCase = new Case();
       createdCase =[SELECT Id,ToD_Case_Category__c,caseNumber,WCT_ContactChannel__c,WCT_AlternatePhone__c,WCT_AlternateEmail__c,Subject,Description from Case where id=:parentId  LIMIT 1];
        listAttachments = new List<Attachment>();
        listAttachments = [
                           SELECT
                               ParentId , 
                               Name, 
                               BodyLength, 
                               Id,
                               CreatedDate
                           FROM 
                               Attachment 
                           WHERE
                               ParentId = :parentId
                           ORDER BY
                               CreatedDate DESC
                           LIMIT
                               50
                   ];    
        mapAttachmentSize = new Map<Id, String>();
        for(Attachment a : listAttachments) {
            String size = null;
            if(1048576 < a.BodyLength) {
                // Size greater than 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength) {
                // Size greater than 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + a.BodyLength + ' bytes';
            }
            
            mapAttachmentSize.put(a.id, size);
        }                         
    }    
}