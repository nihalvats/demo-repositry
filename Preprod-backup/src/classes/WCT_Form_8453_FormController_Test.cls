@isTest
public class WCT_Form_8453_FormController_Test
{
     public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        insert mob;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef;
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        Test.starttest();
        //String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_Form_8453_Form;
        Test.setCurrentPage(pageRef); 
        GBL_Attachments attachmentHelper= new GBL_Attachments();
        attachmentHelper.doc=WCT_UtilTestDataCreation.createDocument();
        attachmentHelper.uploadDocument();
        attachmentHelper.uploadRelatedAttachment(t.id);
        //ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_Form_8453_FormController controller=new WCT_Form_8453_FormController();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_Form_8453_FormController();
        controller.save();
        //controller.doc=WCT_UtilTestDataCreation.createDocument();
        controller.uploadAttachment();
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        controller.supportAreaErrorMesssage = 'Error Meassage';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        controller.uploadAttachment();
    }
}