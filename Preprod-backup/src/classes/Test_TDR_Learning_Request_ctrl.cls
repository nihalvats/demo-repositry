/*=========================================Test Class=================================================

***************************************************************************************************************************************** 
 * Class Name   : Test_TDR_Learning_Request_ctrl
 * Description  : test Class for TDR_Learning_Request_ctrl
 * Created By   : Deloitte India.
 *
 *****************************************************************************************************************************************/
 
 @
istest(SeeAllData = false)
public class Test_TDR_Learning_Request_ctrl{

    static testmethod void Tdrlearninequestnew() {
         
        Id consrty;
        Id lsnrt;
        
       // Recordtype lsnrt = [select id from RecordType Where  SobjectType = 'WCT_List_Of_Names__c' and DeveloperName = 'TDR_Category_Instruction'];
       //Recordtype consrty = [select id from RecordType Where  SobjectType = 'Case_form_Extn__c' and DeveloperName = 'TDR_Consulting'];
        
        Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        consrty =rtMapByName.get('Consulting').getRecordTypeId();//particular RecordId by  Name
        
        Schema.DescribeSObjectResult Rep1 = WCT_List_Of_Names__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName1 = Rep1.getRecordTypeInfosByName();// getting the record Type Info
        lsnrt =rtMapByName1.get('TDR Category Instruction').getRecordTypeId();//particular RecordId by  Name
        
                
       WCT_List_Of_Names__c olistofnames = new WCT_List_Of_Names__c();
       olistofnames.TDR_Attachment_Label__c = 'Screenshot';
       olistofnames.WCT_Type__c = 'TAP POR (TAP into Power of Relationships) session registration';
       olistofnames.ToD_Case_Category_Instructions__c = 'Queries related to cancellation of registration';
       olistofnames.Name = 'LD';
       olistofnames.RecordTypeId = lsnrt;
       insert olistofnames;
        
       WCT_List_Of_Names__c olistofnames1 = new WCT_List_Of_Names__c();
       olistofnames1.TDR_Attachment_Label__c = 'Screenshot';
       olistofnames1.WCT_Type__c = 'Java is not available on my computer';
       olistofnames1.ToD_Case_Category_Instructions__c = 'Queries related to cancellation of registration';
       olistofnames1.Name = 'DLC Learning Queries';
       olistofnames1.RecordTypeId = lsnrt;
       insert olistofnames1; 
     
        
       Test.startTest();
            TDR_Learning_Request_ctrl testcontroller = new TDR_Learning_Request_ctrl();
            recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
            Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
            insert con;
            
            String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
            ApexPages.CurrentPage().getParameters().put('em',encrypt);
            apexpages.currentpage().getparameters().put('Param1','TAP POR (TAP into Power of Relationships) session registration');
            testcontroller.displayPopup = false;
            testcontroller.init();
            testcontroller.getfields();
           
          
            testcontroller.ocaseformextn.TDR_Query_on__c = 'Registration';
            testcontroller.ocaseformextn.TDR_TAP_POR_session_start_date__c = system.today();
            apexpages.currentpage().getparameters().put('description','test test');
            apexpages.currentpage().getparameters().put('recordtype',consrty);
          
            apexpages.currentpage().getparameters().put('dlcAPI','');
            testcontroller.saveRequest();
            
            apexpages.currentpage().getparameters().put('dlcAPI','DLC_Java_is_not_available_on_your_comput');
            apexpages.currentpage().getparameters().put('iscentraloginissue','false');
            apexpages.currentpage().getparameters().put('setDLCcategory','Java is not available on my computer');
            apexpages.currentpage().getparameters().put('reqtype','');
            testcontroller.requesttypedef();
            apexpages.currentpage().getparameters().put('reqtype','TAP POR (TAP into Power of Relationships) session registration');
            testcontroller.requesttypedef();
            testcontroller.setDLCcategory();
             try{testcontroller.loggedInContact = con;}
            catch(exception e){}
            testcontroller.ocaseformextn.TDR_Request_Type__c = 'TAP POR (TAP into Power of Relationships) session registration';
            
            apexpages.currentpage().getparameters().put('setpopdisplay','');
            testcontroller.shwattachmentpopup();
            apexpages.currentpage().getparameters().put('setpopdisplay','showpanel');
            testcontroller.shwattachmentpopup();
            testcontroller.doc=WCT_UtilTestDataCreation.createDocument();
            //testcontroller.uploadAttachment();        
             
            testcontroller.saveRequest();
            
            testcontroller.pageError=true;
            testcontroller.pageErrorMessage='error message';
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
            ApexPages.addMessage(msg);
            //testcontroller.uploadAttachment();
            
            testcontroller.myrequestpageref();
            testcontroller.myRequests();
            
            apexpages.currentpage().getparameters().put('Recid',testcontroller.ocaseformextn.id);
            apexpages.currentpage().getparameters().put('sitename','tod');
            try{
            testcontroller.dummyinn[0].comments = '';
            testcontroller.Updaterecord();
            testcontroller.dummyinn[0].comments = 'test comments';
            testcontroller.Updaterecord();
             }catch(exception e){}
            
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.ocaseformextn.TDR_Delivery_Type__c = 'Live Instructor-Led (Classroom)';
            testcontroller.DlccategorySelection  = 'Incorrect CPE Credits / Completion Date';
            testcontroller.ocaseformextn.TDR_Delivery_Type_1__c = 'Elearning' ;
            testcontroller.ocaseformextn.Completion_date_only_for_e_learning__c = null;
            testcontroller.ocaseformextn.TDR_Delivery_Type2__c = 'Live Instructor-Led (Classroom)';
            testcontroller.ocaseformextn.TDR_Offering_Start_Date__c = Null;
            testcontroller.ocaseformextn.TDR_Offering_Number__c = '123';
            testcontroller.ocaseformextn.TDR_Office_Phone_No__c = '2124';        
            testcontroller.saveRequest();
            
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.ocaseformextn.TDR_Request_Type__c = 'Attestations';
            testcontroller.ocaseformextn.TDR_Delivery_Offering_Number__c  = null;
            testcontroller.saveRequest();
            
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'Package_Request';
            //testcontroller.UploadedDocumentList = new List<TDR_Learning_Request_ctrl.AttachmentsWrapper>();
            testcontroller.saveRequest();
            
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'Attestations';
            testcontroller.saveRequest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'Bulk_course_Offering_Modification';
            testcontroller.saveRequest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'F_Class_Offering_Modification';
            testcontroller.saveRequest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'Package_Request';
            testcontroller.saveRequest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'F_Course_Modification_Request';
            testcontroller.saveRequest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'Other';
            testcontroller.saveRequest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'Package_Request';
            testcontroller.saveRequest();
            String dcID;
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'F_Learning_Evaluation_Modification';
            testcontroller.ocaseformextn.Tev_Request_Type_Sub_Category__c = 'W2D Modification';
            dcID = testcontroller.getDownloadDocumentID();
            testcontroller.ocaseformextn.Tev_Request_Type_Sub_Category__c = 'Conference Modification';
            dcID = testcontroller.getDownloadDocumentID();
            testcontroller.ocaseformextn.Tev_Request_Type_Sub_Category__c = 'Learner Follow-up Modification';
            dcID = testcontroller.getDownloadDocumentID();
            testcontroller.ocaseformextn.Tev_Request_Type_Sub_Category__c = 'Custom Questions Modification';
            dcID = testcontroller.getDownloadDocumentID();
            testcontroller.ocaseformextn.Tev_Request_Type_Sub_Category__c = 'Instructor Modification';
            dcID = testcontroller.getDownloadDocumentID();
            testcontroller.ocaseformextn.Tev_Request_Type_Sub_Category__c = 'Offering Date Modification';
            dcID = testcontroller.getDownloadDocumentID();          
            testcontroller.RequestValue = 'F_Learning_Evaluation_Setup';
            testcontroller.ocaseformextn.Request_Type1_Sub_Category__c = 'Conference evaluation setup';
            dcID = testcontroller.getDownloadDocumentID();
            testcontroller.ocaseformextn.Request_Type1_Sub_Category__c = 'Custom questions';
            dcID = testcontroller.getDownloadDocumentID();
            testcontroller.ocaseformextn.Request_Type1_Sub_Category__c = 'Learner followup';
            dcID = testcontroller.getDownloadDocumentID();          
            testcontroller.ocaseformextn.Request_Type1_Sub_Category__c = 'W2D evaluation setup';
            dcID = testcontroller.getDownloadDocumentID();
            testcontroller.saveRequest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'F_Learning_Evaluation_Setup';
            testcontroller.saveRequest();
            
            
            
        Test.StopTest();
         
    
    
    }
    Static testmethod void Tdrlearninequestnew3()   {
        Id consrty;
        Id lsnrt;
        
       // Recordtype lsnrt = [select id from RecordType Where  SobjectType = 'WCT_List_Of_Names__c' and DeveloperName = 'TDR_Category_Instruction'];
       //Recordtype consrty = [select id from RecordType Where  SobjectType = 'Case_form_Extn__c' and DeveloperName = 'TDR_Consulting'];
        
        Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        consrty =rtMapByName.get('Consulting').getRecordTypeId();//particular RecordId by  Name
        
        Schema.DescribeSObjectResult Rep1 = WCT_List_Of_Names__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName1 = Rep1.getRecordTypeInfosByName();// getting the record Type Info
        lsnrt =rtMapByName1.get('TDR Category Instruction').getRecordTypeId();//particular RecordId by  Name
        
                
       WCT_List_Of_Names__c olistofnames = new WCT_List_Of_Names__c();
       olistofnames.TDR_Attachment_Label__c = 'Screenshot';
       olistofnames.WCT_Type__c = 'TAP POR (TAP into Power of Relationships) session registration';
       olistofnames.ToD_Case_Category_Instructions__c = 'Queries related to cancellation of registration';
       olistofnames.Name = 'LD';
       olistofnames.RecordTypeId = lsnrt;
       insert olistofnames;
        
       WCT_List_Of_Names__c olistofnames1 = new WCT_List_Of_Names__c();
       olistofnames1.TDR_Attachment_Label__c = 'Screenshot';
       olistofnames1.WCT_Type__c = 'Java is not available on my computer';
       olistofnames1.ToD_Case_Category_Instructions__c = 'Queries related to cancellation of registration';
       olistofnames1.Name = 'DLC Learning Queries';
       olistofnames1.RecordTypeId = lsnrt;
       insert olistofnames1;
        Test.StartTest();
            TDR_Learning_Request_ctrl testcontroller = new TDR_Learning_Request_ctrl();
            recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
            Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
            insert con;
            
            String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
            ApexPages.CurrentPage().getParameters().put('em',encrypt);
            apexpages.currentpage().getparameters().put('Param1','TAP POR (TAP into Power of Relationships) session registration');
            testcontroller.displayPopup = false;
            testcontroller.init();
            testcontroller.getfields();
           
          
            testcontroller.ocaseformextn.TDR_Query_on__c = 'Registration';
            testcontroller.ocaseformextn.TDR_TAP_POR_session_start_date__c = system.today();
            apexpages.currentpage().getparameters().put('description','test test');
            apexpages.currentpage().getparameters().put('recordtype',consrty);
          
            apexpages.currentpage().getparameters().put('dlcAPI','');
            testcontroller.saveRequest();
            Case_form_Extn__c extn = new Case_form_Extn__c();
            ID cid;
            String comments = 'test' ;
            TDR_Learning_Request_ctrl.innercl incl = new TDR_Learning_Request_ctrl.innercl(extn,cid,comments);
            Boolean sel = true;
            String cid1 = '';
            String docn = '';
            String siz = '' ;
         //   TDR_Learning_Request_ctrl.AttachmentsWrapper attwrpr = new TDR_Learning_Request_ctrl.AttachmentsWrapper(sel,docn,cid1,siz);
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.ocaseformextn.TDR_Delivery_Type__c = 'Live Instructor-Led (Classroom)';
            testcontroller.DlccategorySelection  = 'Incorrect CPE Credits / Completion Date';
            testcontroller.ocaseformextn.TDR_Delivery_Type_1__c = 'Elearning' ;
            testcontroller.ocaseformextn.Completion_date_only_for_e_learning__c = null;
            testcontroller.ocaseformextn.TDR_Delivery_Type2__c = 'Live Instructor-Led (Classroom)';
            testcontroller.ocaseformextn.TDR_Offering_Start_Date__c = Null;
            testcontroller.ocaseformextn.TDR_Offering_Number__c = '123';
            testcontroller.ocaseformextn.TDR_Office_Phone_No__c = '2124';        
            testcontroller.saveRequest();
            testcontroller.requestTypeOnChange();
            testcontroller.RequestValue = 'F_Learning_Evaluation_Modification';
            testcontroller.requestTypeOnChange();
            testcontroller.RequestValue = 'F_Class_Offering_Modification';
            testcontroller.requestTypeOnChange();
            testcontroller.displayModificationPanel();
            testcontroller.RequestValue = 'F_Course_Modification_Request';
            testcontroller.displayModificationPanel();
           // testcontroller.uploadRelatedAttachment();
            //testcontroller.DeleteAttachment();
            testcontroller.cancel();
            PageReference pageRef = Page.TDR_Mylearningrequests; // Add your VF page Name here
            pageRef.getParameters().put('em', CryptoHelper.encrypt(UserInfo.getUserEmail()));
            Test.setCurrentPage(pageRef);
            testcontroller.ocaseformextn.TDR_Request_Type__c = 'DLC Learning Queries';
            testcontroller.saveRequest();
            testcontroller.RequestValue = 'Attestations';           
            testcontroller.saveRequest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'Bulk_course_Offering_Modification';
            testcontroller.saveRequest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'F_Class_Offering_Modification';
            testcontroller.saveRequest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'Package_Request';
            testcontroller.saveRequest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'F_Course_Modification_Request';
            testcontroller.saveRequest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'Package_Request';
            testcontroller.saveRequest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'Package_Request';
            testcontroller.saveRequest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.RequestValue = 'Other';
            testcontroller.saveRequest();
            testcontroller.Updaterecord();
        Test.StopTest();
    }
    static testmethod void Tdrlearninequestnew1() {
         
        Id consrty;
        Id lsnrt;
        
       // Recordtype lsnrt = [select id from RecordType Where  SobjectType = 'WCT_List_Of_Names__c' and DeveloperName = 'TDR_Category_Instruction'];
       //Recordtype consrty = [select id from RecordType Where  SobjectType = 'Case_form_Extn__c' and DeveloperName = 'TDR_Consulting'];
        
        Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        consrty =rtMapByName.get('Consulting').getRecordTypeId();//particular RecordId by  Name
        
        Schema.DescribeSObjectResult Rep1 = WCT_List_Of_Names__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName1 = Rep1.getRecordTypeInfosByName();// getting the record Type Info
        lsnrt =rtMapByName1.get('TDR Category Instruction').getRecordTypeId();//particular RecordId by  Name
        
                
       WCT_List_Of_Names__c olistofnames = new WCT_List_Of_Names__c();
       olistofnames.TDR_Attachment_Label__c = 'Screenshot';
       olistofnames.WCT_Type__c = 'TAP POR (TAP into Power of Relationships) session registration';
       olistofnames.ToD_Case_Category_Instructions__c = 'Queries related to cancellation of registration';
       olistofnames.Name = 'LD';
       olistofnames.RecordTypeId = lsnrt;
       insert olistofnames;
        
       WCT_List_Of_Names__c olistofnames1 = new WCT_List_Of_Names__c();
       olistofnames1.TDR_Attachment_Label__c = 'Screenshot';
       olistofnames1.WCT_Type__c = 'Java is not available on my computer';
       olistofnames1.ToD_Case_Category_Instructions__c = 'Queries related to cancellation of registration';
       olistofnames1.Name = 'DLC Learning Queries';
       olistofnames1.RecordTypeId = lsnrt;
       insert olistofnames1; 
     
        
       Test.startTest();
            TDR_Learning_Request_ctrl testcontroller = new TDR_Learning_Request_ctrl();
            recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
            Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
            insert con;
            
            String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
            ApexPages.CurrentPage().getParameters().put('em',encrypt);
            apexpages.currentpage().getparameters().put('Param1','TAP POR (TAP into Power of Relationships) session registration');
            testcontroller.displayPopup = false;
            testcontroller.init();
            testcontroller.getfields();
           
          
            testcontroller.ocaseformextn.TDR_Query_on__c = 'Registration';
            testcontroller.ocaseformextn.TDR_TAP_POR_session_start_date__c = system.today();
            apexpages.currentpage().getparameters().put('description','test test');
            apexpages.currentpage().getparameters().put('recordtype',consrty);
          
            apexpages.currentpage().getparameters().put('dlcAPI','');
            PageReference pageRef1 = new PageReference('/apex/TDR_Delivery_Request/'); // Add your VF page Name here
            pageRef1.getParameters().put('em', CryptoHelper.encrypt(UserInfo.getUserEmail())); 
            Test.setCurrentPage(pageRef1); 
            testcontroller.RequestValue = 'F_Course_Modification_Request';            
            testcontroller.saveRequest();
            //PageReference pageRef2 = Page.TDR_Learning_Request; // Add your VF page Name here
            PageReference pageRef2 =new pagereference('/apex/TDR_Learning_Request/');
            pageRef2.getParameters().put('em', CryptoHelper.encrypt(UserInfo.getUserEmail()));
            pageRef2.getParameters().put('description', 'Test');
            Test.setCurrentPage(pageRef2);
            testcontroller.saveRequest();
            Document document1;

            document1 = new Document();
            document1.Body = Blob.valueOf('Some Text');
            document1.ContentType = 'application/pdf';
            document1.DeveloperName = 'my_document';
            document1.IsPublic = true;
            document1.Name = 'My Test Document';
            document1.FolderId = 
            document1.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert document1;
            testcontroller.uploaddoc = document1;
            testcontroller.docIdList.add(document1.ID);
            testcontroller.RequestValue = 'F_Class_Offering_Modification';
            testcontroller.saveRequest();
         //   testcontroller.uploadRelatedAttachment();
            Test.StopTest();
         
    
    
    }
    static testmethod void Tdrlearninequestnew2() {
         
        Id consrty;
        Id lsnrt;
        
       // Recordtype lsnrt = [select id from RecordType Where  SobjectType = 'WCT_List_Of_Names__c' and DeveloperName = 'TDR_Category_Instruction'];
       //Recordtype consrty = [select id from RecordType Where  SobjectType = 'Case_form_Extn__c' and DeveloperName = 'TDR_Consulting'];
        
        Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
        consrty =rtMapByName.get('Consulting').getRecordTypeId();//particular RecordId by  Name
        
        Schema.DescribeSObjectResult Rep1 = WCT_List_Of_Names__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByName1 = Rep1.getRecordTypeInfosByName();// getting the record Type Info
        lsnrt =rtMapByName1.get('TDR Category Instruction').getRecordTypeId();//particular RecordId by  Name
        
                
       WCT_List_Of_Names__c olistofnames = new WCT_List_Of_Names__c();
       olistofnames.TDR_Attachment_Label__c = 'Screenshot';
       olistofnames.WCT_Type__c = 'TAP POR (TAP into Power of Relationships) session registration';
       olistofnames.ToD_Case_Category_Instructions__c = 'Queries related to cancellation of registration';
       olistofnames.Name = 'LD';
       olistofnames.RecordTypeId = lsnrt;
       insert olistofnames;
        
       WCT_List_Of_Names__c olistofnames1 = new WCT_List_Of_Names__c();
       olistofnames1.TDR_Attachment_Label__c = 'Screenshot';
       olistofnames1.WCT_Type__c = 'Java is not available on my computer';
       olistofnames1.ToD_Case_Category_Instructions__c = 'Queries related to cancellation of registration';
       olistofnames1.Name = 'DLC Learning Queries';
       olistofnames1.RecordTypeId = lsnrt;
       insert olistofnames1; 
     
        
       Test.startTest();
            TDR_Learning_Request_ctrl testcontroller = new TDR_Learning_Request_ctrl();
            recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
            Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
            insert con;
            
            String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
            ApexPages.CurrentPage().getParameters().put('em',encrypt);
            apexpages.currentpage().getparameters().put('Param1','TAP POR (TAP into Power of Relationships) session registration');
            testcontroller.displayPopup = false;
            testcontroller.init();
            testcontroller.getfields();
           
          
            testcontroller.ocaseformextn.TDR_Query_on__c = 'Registration';
            testcontroller.ocaseformextn.TDR_TAP_POR_session_start_date__c = system.today();
            apexpages.currentpage().getparameters().put('description','test test');
            apexpages.currentpage().getparameters().put('recordtype',consrty);
          
            apexpages.currentpage().getparameters().put('dlcAPI','');
            PageReference pageRef1 = Page.TDR_Delivery_Request; // Add your VF page Name here
            pageRef1.getParameters().put('em', CryptoHelper.encrypt(UserInfo.getUserEmail()));  
            Test.setCurrentPage(pageRef1);
            testcontroller.RequestValue = 'F_Course_Modification_Request';
            testcontroller.saveRequest();
            testcontroller.RequestValue = 'F_Class_Offering_Modification';
            testcontroller.saveRequest();
            Document document1;
            document1 = new Document();
            document1.Body = Blob.valueOf('Some Text');
            document1.ContentType = 'application/pdf';
            document1.DeveloperName = 'my_document';
            document1.IsPublic = true;
            document1.Name = 'My Test Document';
            document1.FolderId = 
            document1.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert document1;
            testcontroller.uploaddoc = document1;
            testcontroller.docIdList.add(document1.ID);
            testcontroller.RequestValue = 'F_Class_Offering_Modification';
            testcontroller.saveRequest();
           // testcontroller.uploadAttachment();
            Test.StopTest();
    
    }
    /*static testmethod void Tdrlearninequestnew4() {
        TDR_Learning_Request_ctrl testcontroller = new TDR_Learning_Request_ctrl();
        PageReference pageRef1 = Page.TDR_Delivery_Request; // Add your VF page Name here
        pageRef1.getParameters().put('em', CryptoHelper.encrypt(UserInfo.getUserEmail()));  
        Test.setCurrentPage(pageRef1);
        Test.StartTest();
            testcontroller.ocaseformextn = new Case_form_Extn__c();
            testcontroller.ocaseformextn.TDR_Delivery_Type__c = 'Live Instructor-Led (Classroom)';
            testcontroller.DlccategorySelection  = 'Incorrect CPE Credits / Completion Date';
            testcontroller.ocaseformextn.TDR_Delivery_Type_1__c = 'Elearning' ;
            testcontroller.ocaseformextn.Completion_date_only_for_e_learning__c = null;
            testcontroller.ocaseformextn.TDR_Delivery_Type2__c = 'Live Instructor-Led (Classroom)';
            testcontroller.ocaseformextn.TDR_Offering_Start_Date__c = Null;
            testcontroller.ocaseformextn.TDR_Offering_Number__c = '123';
            testcontroller.ocaseformextn.TDR_Office_Phone_No__c = '2124';  
            testcontroller.RequestValue = 'F_Course_Modification_Request';
            testcontroller.saveRequest();
            Case_form_Extn__c extn = new Case_form_Extn__c();
            insert extn;
            extn =[Select ID , Name from Case_form_Extn__c Limit 1];
            TDR_Learning_Request_ctrl.Submit(extn.ID,0,0,0,0,'');
        Test.StopTest();
    }*/
    
}