@isTest
public class WCT_groupInterviewpgcntllr_test {
 public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
    public static Id profileIdIntv=[Select id from Profile where Name=:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
    public static Id profileIdRecr=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
    public static Event InterviewEvent;
    public static EventRelation InterviewEventRelation;
    public static User userRecRecCoo, userRecRecIntv, userRecRecr;
    public static WCT_Interview__c Interview;
    public static Document document;
    public static WCT_List_Of_Names__c ClickToolForm;
    public static WCT_Interview_Junction__c InterviewTracker;
    public static Contact Candidate;
    public static WCT_Requisition__c Requisition;
    public static WCT_Candidate_Requisition__c CandidateRequisition;
    
    
    
    /** 
Method Name  : createUserRecCoo
Return Type  : User
Type         : private
Description  : Create temp records for data mapping         
*/
    private Static User createUserRecCoo()
    {
        userRecRecr=WCT_UtilTestDataCreation.createUser('RecrFinl', profileIdRecr, 'assl@deloitte.com', 'abc@deloitte.com');
        insert userRecRecr;
        return  userRecRecr;
    } 
    /** 
Method Name  : createUserRecr
Return Type  : User
Type         : private
Description  : Create temp records for data mapping         
*/
    private Static User createUserRecr()
    {
        userRecRecr=WCT_UtilTestDataCreation.createUser('RecrFinl', profileIdRecr, 'skati3654764767@deloitte.com', 'test6868i68@deloitte.com');
        insert userRecRecr;
        return  userRecRecr;
    }
    /** 
Method Name  : createUserIntv
Return Type  : User
Type         : private
Description  : Create temp records for data mapping         
*/
    private Static User createUserIntv()
    {
        userRecRecIntv=WCT_UtilTestDataCreation.createUser('IntvFinl', profileIdIntv, 'arunsharmaIntvFinl@deloitte.com', 'asds@deloitte.com');
        insert userRecRecIntv;
        return  userRecRecIntv;
    }  
    /** 
Method Name  : createDocument
Return Type  : Document
Type         : private
Description  : Create temp records for data mapping         
*/    
    private Static Document createDocument()
    {
        document=WCT_UtilTestDataCreation.createDocument();
        insert document;
        return  document;
    }     
    /** 
Method Name  : createClickToolForm
Return Type  : WCT_List_Of_Names__c
Type         : private
Description  : Create temp records for data mapping         
*/
    private Static WCT_List_Of_Names__c createClickToolForm()
    {
        ClickToolForm=WCT_UtilTestDataCreation.createClickToolForm(document.id);
        insert ClickToolForm; 
        return  ClickToolForm;
    }      
    /** 
Method Name  : createCandidate
Return Type  : Contact
Type         : private
Description  : Create temp records for data mapping         
*/
    private Static Contact createCandidate()
    {
        Candidate=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
        insert Candidate;
        return  Candidate;
    }    
    /** 
Method Name  : createRequisition
Return Type  : WCT_Requisition__c
Type         : private
Description  : Create temp records for data mapping         
*/
    private Static WCT_Requisition__c createRequisition()
    {
        Requisition=WCT_UtilTestDataCreation.createRequisition(userRecRecr.Id);
        insert Requisition;
        return  Requisition;
    }
    /** 
Method Name  : createCandidateRequisition
Return Type  : WCT_Candidate_Requisition__c
Type         : private
Description  : Create temp records for data mapping         
*/
    private Static WCT_Candidate_Requisition__c createCandidateRequisition()
    {
        CandidateRequisition=WCT_UtilTestDataCreation.createCandidateRequisition(Candidate.ID,Requisition.Id);
        insert CandidateRequisition;
        return  CandidateRequisition;
    } 
    
    /** 
Method Name  : createInterview
Return Type  : WCT_Interview__c
Type         : private
Description  : Create temp records for data mapping         
*/
    private Static WCT_Interview__c createInterview()
    {
        Interview=WCT_UtilTestDataCreation.createInterview(ClickToolForm.Id);
        system.runAs(userRecRecCoo)
        {
            insert Interview;
        }
        return  Interview;
    }   
    
    /** 
Method Name  : createInterviewTracker
Return Type  : WCT_Interview_Junction__c
Type         : private
Description  : Create temp records for data mapping         
*/
    private Static WCT_Interview_Junction__c createInterviewTracker()
    {
        InterviewTracker=WCT_UtilTestDataCreation.createInterviewTracker(Interview.Id,CandidateRequisition.Id,userRecRecIntv.Id);
        insert InterviewTracker;
        return  InterviewTracker;
    }  
    
    /** 
Method Name  : createEvent
Return Type  : Event
Type         : private
Description  : Create temp records for data mapping         
*/    
    private Static Event createEvent()
    {
        InterviewEvent=WCT_UtilTestDataCreation.createEvent(Interview.Id);
        system.runAs(userRecRecCoo)
        {
            insert InterviewEvent;
        }
        return  InterviewEvent;
    }     
    /** 
Method Name  : createEventRelation
Return Type  : EventRelation
Type         : private
Description  : Create temp records for data mapping         
*/    
    private Static EventRelation createEventRelation()
    {
        InterviewEventRelation=WCT_UtilTestDataCreation.createEventRelation(InterviewEvent.Id,userRecRecIntv.Id);
        insert InterviewEventRelation;
        return  InterviewEventRelation;
    }      
    static testMethod void withInvitees() {
        test.startTest();
        
        userRecRecCoo=createUserRecCoo();
        userRecRecIntv=createUserIntv();
        userRecRecr=createUserRecr();
        document=createDocument();
        ClickToolForm=createClickToolForm();
        Candidate=createCandidate();
        Requisition=createRequisition();
        CandidateRequisition=createCandidateRequisition();
        Interview=createInterview();
        InterviewEvent=createEvent();
        InterviewEventRelation=createEventRelation();
        InterviewTracker=createInterviewTracker();
        list<string> intJunLst = new list<string>();
        intJunLst.add(InterviewTracker.Id);
        pagereference pg=page.wct_groupInterviewPageRedesign;
        test.setCurrentPage(pg);
        ApexPages.currentPage().getParameters().put('interviewId',Interview.Id);
        ApexPages.currentPage().getParameters().put('status','Submitted');
        ApexPages.currentPage().getParameters().put('submType','Single');
        WCT_groupInterviewPageRdsnController wctgpc= new WCT_groupInterviewPageRdsnController();
        //GD_candidateWraper gdc = new GD_candidateWraper();
        ApexPages.currentPage().getParameters().put('interviewId',Interview.Id);
        ApexPages.currentPage().getParameters().put('status','Submitted');
        ApexPages.currentPage().getParameters().put('submType','Group');
        wctgpc= new WCT_groupInterviewPageRdsnController();
        WCT_Interview_Forms__c I_Form= new WCT_Interview_Forms__c();

        //wctgpc.list_iform=new list<WCT_Interview_Forms__c>{I_Form};
        system.debug('##$$'+wctgpc.list_iform);
        Interview=createInterview();
        wctgpc.SaveIEF();
              
        I_Form.WCT_Interview_Junction__c=InterviewTracker.id;
        I_Form.WCT_Interviewer_s_Final_Recommendation__c='Please enter final recomendation.';
        insert I_Form;
        wctgpc.list_iform=[select id from WCT_Interview_Forms__c where id=:I_Form.id];
        wctgpc.createPdfs();
        wctgpc.redirectSubIEF();
        
        WCT_groupInterviewPageRdsnController.GD_candidateWraper wctwrap = new WCT_groupInterviewPageRdsnController.GD_candidateWraper();
        WCT_Interview_Junction__c cc = new WCT_Interview_Junction__c();
        wctwrap.getResultTypes();
    }
}