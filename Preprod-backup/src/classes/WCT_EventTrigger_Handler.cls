/*
*    Class name : WCT_EventTrigger_Handler
*    Description : This class is called from Event Trigger
*    
*/
public class WCT_EventTrigger_Handler{

//---------------------------------------------------------------------------------
//    Decsription : This method checks whether Event date is in past 
//---------------------------------------------------------------------------------
    public static void checkEventDate(List<Event> lstNewEvent){
        for(Event tmpEvent : lstNewEvent){
            if(tmpEvent.StartDateTime.addMinutes(5) < System.Now())
                tmpEvent.StartDateTime.addError(Label.WCT_EventStartDateTime);
            if(tmpEvent.EndDateTime.addMinutes(5) < System.Now())
                tmpEvent.EndDateTime.addError(Label.WCT_EventEndDateTime);
            if(tmpEvent.StartDateTime > tmpEvent.EndDateTime)
                tmpEvent.StartDateTime.addError(Label.WCT_EventStartEndDateTime);
        }
    }
//---------------------------------------------------------------------------------
//    Decsription : This method allows to create only one event per interview
//---------------------------------------------------------------------------------
    public static void checkSingleEventPerInterview(List<Event> lstEvent){
        set<Id> setInterviewId = new set<Id>();
        for(Event tmpEvent : lstEvent){
            setInterviewId.add(tmpEvent.whatId);
        }
        List<Event> lstExistingEvent = [Select Id,whatId From event Where whatId in :setInterviewId];
        for(Event tmpOldEvent : lstExistingEvent ){
            for(Event tmpNewEvent : lstEvent){
                if(tmpNewEvent.id ==null && tmpNewEvent.whatId == tmpOldEvent.whatId){
                    tmpNewEvent.whatId.addError('Only one event can be scheduled per Interview.');
                }
            }
        }
    } 

//-------------------------------------------------------------------------------------------
//    Decsription : This method populates Start date and End date from event to Interview
//-------------------------------------------------------------------------------------------
    public static void populateStartEndDatesOnInterview(List<Event> lstEvent){
        set<Id> setInterviewId = new set<Id>();
        for(Event tmpEvent : lstEvent){
            setInterviewId.add(tmpEvent.whatId);
        }
        List<Wct_Interview__c> lstInterview = [select WCT_Interview_Start_Time__c,WCT_Interview_End_Time__c
                                               from Wct_Interview__c where id in :setInterviewId];
        for(Wct_Interview__c tmpIntv : lstInterview ){
            for(Event tmpEvent : lstEvent){
                if(tmpEvent.whatId==tmpIntv.id){
                    /*Updated as part of the Case - IEF notifcatio issue when event date chnaged. This is sending a flag to interview stating the change in the event date. And later the interview will update the interview tracker with the same to remove the xisting notifcation from the time based workflow queues. 
						*/
                    if(tmpIntv.WCT_Interview_Start_Time__c !=tmpEvent.StartDateTime)
                     {
                         tmpIntv.IsEvent_Updated__c=true;
                     }
                    tmpIntv.WCT_Interview_Start_Time__c = tmpEvent.StartDateTime;
                    tmpIntv.WCT_Interview_End_Time__c = tmpEvent.EndDateTime;
                    //for bulk upload invite
                    tmpIntv.Event_ID__c=tmpEvent.Id;
                    tmpIntv.Event_Name__c=tmpEvent.Subject;
                }
            }
        }
        Database.saveResult[] result = Database.Update(lstInterview );                                               
    }
    

    /****************************************************************************
     * Description : This method should be called before update. 
     * Logic will check for changes in certain fields. If changed, then set the field Send_Updated_Invite_auto__c. 
	 ****************************************************************************/
     public static void setSendUpdateInvite(List<Event> oldEvents, List<Event> newEvents)
     {
		for(integer i=0;i<oldEvents.size();i++)  
        {
           /*
			  Checking the Send update Invite to true , whenever the following fields are changed.
				1. Subject
				2. Location
				3. Type
                4. ShowAs
                5. StartDateTime
                6. EndDateTime
                7. Owner
                8. IsAllDayEvent
                9. IsPrivate
                10.Description
			*/
            if(oldEvents[i].Subject!=newEvents[i].Subject || oldEvents[i].Location!=newEvents[i].Location || oldEvents[i].Type!=newEvents[i].Type || oldEvents[i].ShowAs!=newEvents[i].ShowAs || oldEvents[i].StartDateTime!=newEvents[i].StartDateTime || oldEvents[i].EndDateTime != newEvents[i].EndDateTime)
            {
                newEvents[i].Send_Updated_Invite_auto__c=true;
            }
            
            if(oldEvents[i].OwnerId!=newEvents[i].OwnerId || oldEvents[i].DurationInMinutes!=newEvents[i].DurationInMinutes  || oldEvents[i].Description!=newEvents[i].Description || oldEvents[i].IsAllDayEvent!=newEvents[i].IsAllDayEvent || oldEvents[i].IsPrivate!=newEvents[i].IsPrivate)
            {
                newEvents[i].Send_Updated_Invite_auto__c=true;
            }
            
            
        }
         
     }
}