/********************************************************************************************************************************
Apex class         : TCDInsertRecordsFromCsv_controller
Description        : This is  for inserting records from CSV file to the TCD Custom Object..
Type               : Controller for TCDInsertRecordFromCSV
Test Class         : TCDInsertRecordsFromCsv_controller_Test

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Anji Reddy                 15/04/2016          90%                  <Req / Case #>                  Original Version
************************************************************************************************************************************/

public with sharing class TCDInsertRecordsFromCsv_controller {
    // Declaring all the variables ....
    
    public integer totalrecords { get; set; }
    public integer totalsuccessrec { get; set; }
    public integer totalunsuccessrec { get; set; }
    public integer totalsuccessreccount { get; set; }
    public String nameFile { get; set; }
    public String fileName { get; set; }
    public integer size{get; set;}
    
    transient public  blob ContentFile {get; set;}
    
    public list<list<string>> AllRows = new list<list<string>> ();
    public list<Talent_Delivery_Contact_Database__c> TCDlist {get; set;}
    list<string> AllColumns; 
    list<string> failedlines;
    list<string> rows;
    public list<string> CCnumberlist {get; set;}
    
    // here i am creating the  object for the WCT_parserCSV class to parse data from the  Selected CSV file..
    
    WCT_parseCSV parseCSVInstance = new WCT_parseCSV();
    
    // This pagereference method is going to call when we click on the Export command button in the vf page.
    
    public Pagereference ReadFile()
    {
        TCDlist = new list<Talent_Delivery_Contact_Database__c>();
        AllRows  =   new list<list<string>>();
        AllColumns = new list<string>();
        failedlines= new list<string>();
        CCnumberlist = new list<string>();
        rows = new list<string>();  
        
        totalrecords =0;
        totalsuccessrec = 0;
        totalunsuccessrec = 0;
        totalsuccessreccount = 0;
        
        
        try
        {
            TCDlist = uploadprocess();
            
            if(TCDlist.size()>0)
            {
                
                upsert TCDlist;
                
               //  totalsuccessrec++;
            }
            
        }
        
        catch(Exception e)
        {
            System.debug('e.getCause'+e.getCause());
            System.debug('e.getLinenumber'+e.getLinenumber());
            System.debug('e.getMessage'+e.getMessage());
            
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_Exception);
            ApexPages.addMessage(errormsg);    
        }
        /* if (TCDlist == null)  
{
return null;    
} */
        
        totalrecords = TCDList.size(); 
        system.debug('The CSV file is having with number of records ---->'+totalrecords);
        return null;
    }
    
    
    // This method is Called from the Readfile method..
    
    public list<Talent_Delivery_Contact_Database__c> uploadprocess()
    {
        if(contentfile != null)
        {
            filename = namefile;
            namefile = WCT_parseCSV.blobToString(contentfile);
            system.debug('The name of the file is -------->'+filename);
            AllRows = parseCSVinstance.parseCSV(namefile, true);
            system.debug('The number of file lines contain is ---------->'+AllRows);
            failedLines = nameFile.split('\r\n');
            System.debug('failedLines**********'+failedLines);
        }
        else
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please select a file before clicking on "Submit file."'));
            return null;
        } 
        //   system.debug('The Failed lines and rows added -------------->'+rows);
        //   system.debug('The falied new lines are ----------------->'+rows.add(failedLines[0]));
        
        rows.add(failedLines[0]);
        Integer counter = 1;   
        
        for(list<string> inputvalues : AllRows)   
        {
            totalrecords++;
            if(inputvalues[0]=='' || inputvalues[1]=='' || inputvalues[2]=='') 
            {
                totalunsuccessrec++;
                system.debug('The total unsuccess records count are ------>  '+totalunsuccessrec);
                rows.add(failedLines[counter]);
            } 
            
            if(inputvalues[0] != null)
            {
                string modifiedCCnumber = inputvalues[0].replaceFirst('^0+(?!$)',''); // This method is removing the Leading Zeros from the Cost Center Number.. 
                CCnumberlist.add(modifiedCCnumber);
                // system.debug('The list is having the value of ----->'+CCnumberlist);
            }
        }
        
        list<Talent_Delivery_Contact_Database__c>  ExistingRecords = [select id, name from Talent_Delivery_Contact_Database__c where name like :CCnumberlist];
        //  system.debug('The Existing Records size from the data base are ---->'+ExistingRecords.size());
        for(list<string> inputvalues : AllRows)   
        {
            //  system.debug('The input values is having the ------->'+inputvalues);
            //New TCD Record is creating from the csv file..
            
            map<id, Talent_Delivery_Contact_Database__c> maptcd = new map<id,Talent_Delivery_Contact_Database__c>();
            
            Talent_Delivery_Contact_Database__c tempTCD = new Talent_Delivery_Contact_Database__c();
            string   tcdname = inputvalues[0].replaceFirst('^0+(?!$)','');
            system.debug('The modified TCD name is ----->    '+tcdname);
            tempTCD.name   =  (inputvalues.size()>0 && inputvalues[0]!='')?tcdname:'';
            tempTCD.HR_Function__c   =   (inputvalues.size()>1 && inputvalues[1]!='')? inputvalues[1]:'';
            tempTCD.HR_Service_Area__c  = (inputvalues.size()>2 && inputvalues[2]!='')?inputvalues[2]:'';
            tempTCD.HR_Service_Line__c =  (inputvalues.size()>3 && inputvalues[3]!='')?inputvalues[3]:'';
            tempTCD.India_Talent_Contact_1__c = (inputvalues.size()>4 && inputvalues[4]!='')?inputvalues[4]:'';
            tempTCD.India_Talent_Contact_2__c = (inputvalues.size()>5 && inputvalues[5]!='')? inputvalues[5]:'';
            tempTCD.US_Talent_Contact_1__c =   (inputvalues.size()>6 && inputvalues[6]!='')? inputvalues[6]:'';
            tempTCD.US_Talent_Contact_2__c =   (inputvalues.size()>7 && inputvalues[7]!='')?inputvalues[7]:'';
            tempTCD.US_Talent_Contact_3__c =   (inputvalues.size()>8 && inputvalues[8]!='')?inputvalues[8]:'';
            tempTCD.US_Talent_Contact_4__c =   (inputvalues.size()>9 && inputvalues[9]!='')?inputvalues[9]:'';
            tempTCD.Payroll_Contact_Email__c = (inputvalues.size()>10 && inputvalues[10]!='')?inputvalues[10]:'';
            tempTCD.Workers_Comp_Contact_Email__c = (inputvalues.size()>11 && inputvalues[11]!='')?inputvalues[11]:'';
            tempTCD.Cost_Center_Description__c = (inputvalues.size()>12 && inputvalues[12]!='')?inputvalues[12]:'';
           
            boolean isUpdate=false;
            for(Talent_Delivery_Contact_Database__c currenttexTCD : ExistingRecords)  
            {
                string tempName1 = currenttexTCD.name.replaceFirst('^0+(?!$)','');
                string tempname2 = inputvalues[0].replaceFirst('^0+(?!$)','');
                
                if(tempName1==tempName2)
                {
                    isUpdate=true;
                    if(maptcd.get(currenttexTCD.id)==null)
                    {
                        tempTCD.id = currenttexTCD.id; 
                        maptcd.put(currenttexTCD.id, currenttexTCD);
                        
                    }
                    else
                    { 
                        //Duplicate Names
                        list<Talent_Delivery_Contact_Database__c> DuplicateList = new list<Talent_Delivery_Contact_Database__c>();
                        DuplicateList.add(currenttexTCD);
                    }
                }
                
            }
            
            
            TCDlist.add(tempTCD);
           
            
           
            system.debug('The Total Success Records are --------->'+totalsuccessrec);
        } 
        
        // upsert TCDlist;  
            
       
        counter++;
        system.debug('The Total counter are --------->'+counter);
        
        return TCDlist;
    }
    
    //Method to catch records failed to insert
    public String getFailedRows() {
        String result='';
        result = rows.remove(0);
        while(!rows.isEmpty()) {
            result += '\r\n' + rows[0];
            rows.remove(0);
        }
        return result;
    }
}