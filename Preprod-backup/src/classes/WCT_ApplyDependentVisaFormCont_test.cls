@isTest
public class WCT_ApplyDependentVisaFormCont_test
{
    public static testmethod void m1()
    {    
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Immigration__c immi=WCT_UtilTestDataCreation.createImmigration(con.id);
        insert immi;
        WCT_Task_Reference_Table__c taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.Form_Verbiage__c = 'Hi. This is a test class.';
        insert taskRef;
        task t=WCT_UtilTestDataCreation.createTask(immi.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        Test.starttest();
        PageReference pageRef = Page.WCT_Apply_Dependent_Visa_Form;
        Test.setCurrentPage(pageRef); 
        contact tesCon = [select id,email from contact where id = :con.id limit 1];
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.Email), 'UTF-8');
        system.debug('enc'+encrypt);
        system.debug('enc1'+CryptoHelper.encrypt(tesCon.Email));
        ApexPages.currentPage().getParameters().put('em',encrypt);
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        WCT_Apply_Dependent_Visa_FormController controller=new WCT_Apply_Dependent_Visa_FormController();
        
        //ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller.updateTaskFlags();
        controller=new WCT_Apply_Dependent_Visa_FormController();
        controller.updateTaskFlags();
        Test.stoptest();
    }
    public static testmethod void m2()
    {
     WCT_Apply_Dependent_Visa_FormController controller=new WCT_Apply_Dependent_Visa_FormController();
         //controller.validPage=true;
       
        controller.updateTaskFlags(); 
    }
}