public class WCT_SMList_View extends SitesTodHeaderController {


    /*Public Variables*/


    Public WCT_H1BCAP__c WCTH1BCAP {
        get;
        set;
    }

    public list < WCT_H1BCAP__c > h1bcapSMList {
        get;
        set;
    }
    public String SMComments {
        get;
        set;
    }
    public String SLLComments {
        get;
        set;
    }
    public String SMRealignComments {
        get;
        set;
    }
    public Integer SMFlag {
        get;
        set;
    }
    public Id getRecordId;

    public string LoggedInUserEmail {
        get;
        set;
    }
    public boolean pageError {
        get;
        set;
    }
    public boolean rejectblock {
        get;
        set;
    }
    public boolean realignblock {
        get;
        set;
    }
    public boolean rejectflag {
        get;
        set;
    }
    public boolean realignflag {
        get;
        set;
    }


    public boolean refreshBlock {
        get;
        set;
    }
    public String pageErrorMessage {
        get;
        set;
    }
    /* Error Message related variables */


    public List < wrapAccount > wrapAccountList {
        get;
        set;
    }
    public String filterSMId {
        get;
        set;
    }
     public String filterPPDId{
        get;
        set;
    }
   public boolean Refreshtotalpage{
         get;
         set;
     }     
    
    
    public String loggedName {
        get;
        set;
    }
    public String StatusUpdate {
        get;
        set;
    }
    
    public boolean pageError_SM {get;set;}
    public string pageErrorMessage_SM {get;set;}
    public boolean pageError_SM_Real {get;set;}
    public string pageErrorMessage_SM_Real{get;set;}
    
    public List < Profile > loggedProfile = new List < Profile > ();
    public contact Employee = new contact();

    public WCT_SMList_View() {
    
    Refreshtotalpage = false;
        LoggedInUserEmail = UserInfo.getUserEmail();

      //  getRecordId = ApexPages.currentPage().getParameters().get('id'); 
        loggedProfile = [SELECT Id, Name FROM Profile WHERE Id = : userinfo.getProfileId() LIMIT 1];
        loggedName = loggedProfile[0].Name;

        //MultirowsController();
        if (wrapAccountList == null) {
            wrapAccountList = new List < wrapAccount > ();

            if (loggedName == '20_H1BCAP_SM') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c, WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c, WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != ''
                    AND WCT_H1BCAP_Status__c IN('Ready for USI SM / US PPD approval', 'USI SM Approved', 'USI SM Rejected', 'USI SM Realigned') AND WCT_H1BCAP_GDM_Email_id__c = : LoggedInUserEmail order by CreatedDate asc limit 1000
                ];
            }

            if (loggedName == '20_H1BCAP_SLL') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c, WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c, WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != ''
                    AND WCT_H1BCAP_Status__c IN('Ready for SLL approval', 'USI SLL Rejected', 'USI SLL Approved', 'USI SLL Realigned') AND WC_H1BCAP_USI_SLL_email_id__c = : LoggedInUserEmail order by CreatedDate asc limit 1000
                ];

            }

            if (loggedName == '20_H1BCAP_PPD') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c, WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c, WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != ''
                    AND WCT_H1BCAP_Status__c IN('Ready for SLL approval', 'USI SLL Rejected', 'USI SLL Approved', 'USI SLL Realigned') AND WCT_H1BCAP_US_PPD_mail_Id__c= : LoggedInUserEmail order by CreatedDate asc limit 1000
                ];

            }

        }

        refreshBlock = true;
        rejectblock = false;
        rejectflag = true;
        realignflag = true;
        for (WCT_H1BCAP__c a: h1bcapSMList) {
            if (a.WCT_H1BCAP_Status__c <> null) {
                // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                wrapAccountList.add(new wrapAccount(false, a));
            }
        }
    }

    public List < SelectOption > getSMItems() {
        List < SelectOption > options = new List < SelectOption > ();

        if (loggedName == '20_H1BCAP_SM') {
            options.add(new SelectOption('All-SM', 'All'));
            options.add(new SelectOption('SMApproved', 'SM Approved'));
            options.add(new SelectOption('SMRejected', 'SM Rejected'));
            options.add(new SelectOption('SMRealigned', 'SM Realigned'));
            // options.add(new SelectOption('NoResponse', 'No Response'));
            return options;
        } else if (loggedName == '20_H1BCAP_SLL') {
            options.add(new SelectOption('All-SLL', 'All'));
            options.add(new SelectOption('SLLApproved', 'SLL Approved'));
            options.add(new SelectOption('SLLRejected', 'SLL Rejected'));
            options.add(new SelectOption('SLLRealigned', 'SLL Realigned'));
            // options.add(new SelectOption('NoResponse', 'No Response'));
            return options;
        } else {
            options.add(new SelectOption('All-SLL', 'All'));
            options.add(new SelectOption('SLLApproved', 'SLL Approved'));
            options.add(new SelectOption('SLLRejected', 'SLL Rejected'));
            options.add(new SelectOption('SLLRealigned', 'SLL Realigned'));
            return options;
        }

    }
    
    

    public PageReference processSMRequests() {
            String smemailAddress = UserInfo.getUserEmail();

            //write the query for getting the data based on filterId
            if (filterSMId == 'All-SM') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c, WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c, WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != null AND
                    WCT_H1BCAP_Status__c IN('Ready for USI SM / US PPD approval', 'USI SM Approved', 'USI SM Rejected', 'USI SM Realigned') AND WCT_H1BCAP_GDM_Email_id__c = : smemailAddress order by CreatedDate asc limit 1000
                ];

                wrapAccountList.clear();
                for (WCT_H1BCAP__c a: h1bcapSMList) {
                    if (a.WCT_H1BCAP_Status__c <> null) {
                        // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                        wrapAccountList.add(new wrapAccount(false, a));
                    }
                }
            } else if (filterSMId == 'SMApproved') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c, WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c,WCT_H1BCAP_Project_Name__c, WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != null AND
                    WCT_H1BCAP_Status__c IN('USI SM Approved') AND WCT_H1BCAP_GDM_Email_id__c = : smemailAddress order by CreatedDate asc limit 1000
                ];

                wrapAccountList.clear();
                for (WCT_H1BCAP__c a: h1bcapSMList) {
                    if (a.WCT_H1BCAP_Status__c <> null) {
                        // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                        wrapAccountList.add(new wrapAccount(false, a));
                    }
                }
            } else if (filterSMId == 'SMRejected') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c, WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c, WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != null AND
                    WCT_H1BCAP_Status__c IN('USI SM Rejected') AND WCT_H1BCAP_GDM_Email_id__c = : smemailAddress order by CreatedDate asc limit 1000
                ];


                wrapAccountList.clear();
                for (WCT_H1BCAP__c a: h1bcapSMList) {
                    if (a.WCT_H1BCAP_Status__c <> null) {
                        // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                        wrapAccountList.add(new wrapAccount(false, a));
                    }
                }
            } else if (filterSMId == 'SMRealigned') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c, WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c, WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != null AND
                    WCT_H1BCAP_Status__c IN('USI SM Realigned') AND WCT_H1BCAP_GDM_Email_id__c = : smemailAddress order by CreatedDate asc limit 1000
                ];

                wrapAccountList.clear();
                for (WCT_H1BCAP__c a: h1bcapSMList) {
                    if (a.WCT_H1BCAP_Status__c <> null) {
                        // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                        wrapAccountList.add(new wrapAccount(false, a));
                    }
                }
            }
            if (filterSMId == 'All-SLL') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c, WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c,WCT_H1BCAP_Project_Name__c, WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c, WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != null AND
                    WCT_H1BCAP_Status__c IN('Ready for SLL approval', 'USI SLL Approved', 'USI SLL Rejected', 'USI SLL Realigned') AND WC_H1BCAP_USI_SLL_email_id__c = : smemailAddress order by CreatedDate asc limit 1000
                ];

                wrapAccountList.clear();
                for (WCT_H1BCAP__c a: h1bcapSMList) {
                    if (a.WCT_H1BCAP_Status__c <> null) {
                        // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                        wrapAccountList.add(new wrapAccount(false, a));
                    }
                }
            } else if (filterSMId == 'SLLApproved') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c, WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c, WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != null AND
                    WCT_H1BCAP_Status__c IN('USI SLL Approved') AND WC_H1BCAP_USI_SLL_email_id__c = : smemailAddress order by CreatedDate asc limit 1000
                ];

                wrapAccountList.clear();
                for (WCT_H1BCAP__c a: h1bcapSMList) {
                    if (a.WCT_H1BCAP_Status__c <> null) {
                        // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                        wrapAccountList.add(new wrapAccount(false, a));
                    }
                }
            } else if (filterSMId == 'SLLRejected') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c, WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c, WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != null AND
                    WCT_H1BCAP_Status__c IN('USI SLL Rejected') AND WC_H1BCAP_USI_SLL_email_id__c = : smemailAddress order by CreatedDate asc limit 1000
                ];


                wrapAccountList.clear();
                for (WCT_H1BCAP__c a: h1bcapSMList) {
                    if (a.WCT_H1BCAP_Status__c <> null) {
                        // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                        wrapAccountList.add(new wrapAccount(false, a));
                    }
                }
            } else if (filterSMId == 'SLLRealigned') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c, WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c,WCT_H1BCAP_Project_Name__c, WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c, WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != null AND
                    WCT_H1BCAP_Status__c IN('USI SLL Realigned') AND WC_H1BCAP_USI_SLL_email_id__c = : smemailAddress order by CreatedDate asc limit 1000
                ];

                wrapAccountList.clear();
                for (WCT_H1BCAP__c a: h1bcapSMList) {
                    if (a.WCT_H1BCAP_Status__c <> null) {
                        // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                        wrapAccountList.add(new wrapAccount(false, a));
                    }
                }
            }
            return null;
        }
        
          public PageReference processPPDRequests() {
            String smemailAddress = UserInfo.getUserEmail();

            //write the query for getting the data based on filterId
           
            if (filterPPDId == 'All-SLL') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c, WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c, WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != null AND
                    WCT_H1BCAP_Status__c IN('Ready for SLL approval', 'USI SLL Approved', 'USI SLL Rejected', 'USI SLL Realigned') AND WCT_H1BCAP_US_PPD_mail_Id__c = : smemailAddress order by CreatedDate asc limit 1000
                ];

                wrapAccountList.clear();
               
                for (WCT_H1BCAP__c a: h1bcapSMList) {
                    if (a.WCT_H1BCAP_Status__c <> null) {
                        // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                        wrapAccountList.add(new wrapAccount(false, a));
                    }
                }
              
            } else if (filterPPDId == 'SLLApproved') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c,WCT_H1BCAP_Project_Name__c, WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c, WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != null AND
                    WCT_H1BCAP_Status__c IN('USI SLL Approved') AND WCT_H1BCAP_US_PPD_mail_Id__c = : smemailAddress order by CreatedDate asc limit 1000
                ];

                wrapAccountList.clear();
               
                for (WCT_H1BCAP__c a: h1bcapSMList) {
                    if (a.WCT_H1BCAP_Status__c <> null) {
                        // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                        wrapAccountList.add(new wrapAccount(false, a));
                    }
                }
                
            } else if (filterPPDId == 'SLLRejected') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c, WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c, WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != null AND
                    WCT_H1BCAP_Status__c IN('USI SLL Rejected') AND WCT_H1BCAP_US_PPD_mail_Id__c= : smemailAddress order by CreatedDate asc limit 1000
                ];


                wrapAccountList.clear();
               
                for (WCT_H1BCAP__c a: h1bcapSMList) {
                    if (a.WCT_H1BCAP_Status__c <> null) {
                        // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                        wrapAccountList.add(new wrapAccount(false, a));
                    }
                }
               
            } else if (filterPPDId == 'SLLRealigned') {
                h1bcapSMList = [SELECT CreatedDate, WCT_H1BCAP_Status__c, WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_prac_name__c, WCT_H1BCAP_Practitioner_Service_Line__c, WCT_H1BCAP_Practitioner_Service_Area__c,
                    WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Employment_Status__c, Name, Id,
                    Multi_checkbox__c, WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c FROM WCT_H1BCAP__c where WCT_H1BCAP_Email_ID__c != null AND
                    WCT_H1BCAP_Status__c IN('USI SLL Realigned') AND WCT_H1BCAP_US_PPD_mail_Id__c= : smemailAddress order by CreatedDate asc limit 1000
                ];

                wrapAccountList.clear();
              
                for (WCT_H1BCAP__c a: h1bcapSMList) {
                    if (a.WCT_H1BCAP_Status__c <> null) {
                        // As each Account is processed we create a new wrapAccount object and add it to the wrapAccountList
                        wrapAccountList.add(new wrapAccount(false, a));
                    }
                }
               
            }
            return null;
        }
        
    public PageReference rejectAction() {
        
        system.debug('wrappersize********'+wrapAccountList.size());
        list<Boolean> lstbool = new list<Boolean>();
        for (wrapAccount w: wrapAccountList) {
           

                if (w.selected == true) {
                lstbool.add(w.selected );
                }
                
             }
        system.debug('wrappersize lstbool********'+lstbool.size()); 
        if(lstbool.isempty()){
        //rejectblock = false;
        pageErrorMessage = 'Select Atleast One Record';
        pageError= true;
        system.debug('Error message********'+pageError); 
        }else{
        system.debug('Enter wrong loop********');
        rejectblock = true;
        rejectflag = false;
       } 
        return null;
    }
    public PageReference cancelAction() {
        rejectblock = false;
        //rejectflag = true;
        pageReference  page  = new pageReference('/apex/WCT_ApprovalList_View?id='+getRecordId );
         page.setRedirect(true);
        return page;
        
    }
    public PageReference realignAction() {
     
    list<Boolean> lstbool = new list<Boolean>();
        for (wrapAccount w: wrapAccountList) {
           

                if (w.selected == true) {
                lstbool.add(w.selected );
                }
                
             }
        system.debug('wrappersize lstbool********'+lstbool.size()); 
        if(lstbool.isempty()){
        //rejectblock = false;
        pageErrorMessage = 'Select Atleast One Record';
        pageError= true;
        
       }
       else
       { 
        realignblock = true;
        rejectflag = false;
       } 
        return null;
    }
    public PageReference cancelRealignAction() {
       // realignblock = false;
       // rejectflag = true;
      pageReference  page = new pageReference('/apex/WCT_ApprovalList_View?id='+getRecordId );
               page.setRedirect(true);  
               return page;
    }

    public PageReference callCheck() {
        realignblock = false;
        rejectflag = true;
        return null;
    }

    public PageReference DoRefresh() {
        Refreshtotalpage = true;
        system.debug('******'+Refreshtotalpage);
       
        return null;
    }
    public pageReference rejectRecord() {
        pageReference page;
        try {
            list < WCT_H1BCAP__c > rejlst = new list < WCT_H1BCAP__c > ();
            if(string.isblank(SMComments)){
            
            pageError_SM = true;
            pageErrorMessage_SM = 'Please fill the Reject Reason';
            
            
            }
            else{
            if (loggedName == '20_H1BCAP_SLL') {
                StatusUpdate = 'USI SLL Rejected';
                SMFlag = 1;
            } else if (loggedName == '20_H1BCAP_SM') {
                StatusUpdate = 'USI SM Rejected';
                SMFlag = 0;
            }
            for (wrapAccount w: wrapAccountList) {
           

                if (w.selected == true) {
             
                    w.acc.WCT_H1BCAP_Status__c = StatusUpdate;
                    if (SMFlag == 0) {
                        w.acc.WCT_H1BCAP_Rejected_Reason_from_SM__c = SMComments;
                        w.acc.Wct_H1bCap_SM_RejectedDate__c = system.today();
                        
                    }
                    if (SMFlag == 1) {
                        w.acc.Wct_H1bcap_SLL_RejectDate__c = system.today();    
                        w.acc.WCT_Rejected_Reason_from_SLL__c = SMComments;
                    }
                    rejlst.add(w.acc);
                }
            }
          }  
            if (!rejlst.isEmpty()) {
                update rejlst;
                rejlst.clear();
                refreshBlock = true;
                cancelAction();
               //rejectblock=false;
                page = new pageReference('/apex/WCT_ApprovalList_View?id='+getRecordId );
               page.setRedirect(true);  
            }

        } catch (Exception e) {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
        }

        return page;

    }
    public pageReference approveRecord() {
        
        pageReference page;
        list<Boolean> lstbool = new list<Boolean>();
        for (wrapAccount w: wrapAccountList) {
           

                if (w.selected == true) {
                lstbool.add(w.selected );
                }
                
             }
        system.debug('wrappersize lstbool********'+lstbool.size()); 
        if(lstbool.isempty()){
        //rejectblock = false;
        pageErrorMessage = 'Select Atleast One Record';
        pageError= true;
        
       }

        if (loggedName == '20_H1BCAP_SLL') {
            StatusUpdate = 'USI SLL Approved';
        } else if (loggedName == '20_H1BCAP_SM') {
            StatusUpdate = 'USI SM Approved';
        }
        try {
            list < WCT_H1BCAP__c > applst = new list < WCT_H1BCAP__c > ();
    

            for (wrapAccount w: wrapAccountList) {
                system.debug('recID::' + w.acc.Id);
                system.debug('selec11::' + w.selected);

                if (w.selected == true) {

                    w.acc.WCT_H1BCAP_Status__c = StatusUpdate;
                    //  w.acc.WCT_H1BCAP_Rejected_Reason_from_SM__c= Comment;
                    applst.add(w.acc);
                }
            }
            if (!applst.isEmpty()) {
                refreshBlock = true;
                update applst;
                  applst.clear();
             page = new pageReference('/apex/WCT_ApprovalList_View?id='+getRecordId );
           page.setRedirect(true);     

            }

        } catch (Exception e) {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
        }

        return page;

    }
    public pageReference realignRecord() {

        pageReference page;
        
      
        try {
            list < WCT_H1BCAP__c > relignlist = new list < WCT_H1BCAP__c > ();
            if(string.isblank(SMRealignComments)){
            
            pageError_SM_Real = true;
            pageErrorMessage_SM_Real= 'Please fill the Re-align Reason';
            
            
            }
            else{
            
             if (loggedName == '20_H1BCAP_SLL') {
            StatusUpdate = 'USI SLL Realigned';
            SMFlag = 1;

        } else if (loggedName == '20_H1BCAP_SM') {
            StatusUpdate = 'USI SM Realigned';
            SMFlag = 0;

        }

            for (wrapAccount w: wrapAccountList) {
                system.debug('recID::' + w.acc.Id);
                system.debug('selec11::' + w.selected);

                if (w.selected == true) {

                    w.acc.WCT_H1BCAP_Status__c = StatusUpdate;
                    if (SMFlag == 0) {
                        w.acc.Wct_H1bcap_SM_Realigned_Date__c = system.today();
                        w.acc.WCT_Realigned_Reason_from_SM__c = SMRealignComments;
                    }
                    if (SMFlag == 1) {
                        w.acc.Wct_H1bcap_SLL_RealignedDate__c = system.today();
                        w.acc.WCT_Realigned_Reason_from_SLL__c = SMRealignComments;
                    }

                    relignlist.add(w.acc);
                }
            }
        }    
            if (!relignlist.isEmpty()) {
                refreshBlock = true;
                //  realignblock=false;
                update relignlist;
                relignlist.clear();
              //  cancelRealignAction();
                 page = new pageReference('/apex/WCT_ApprovalList_View?id='+getRecordId );
                  page.setRedirect(true);  
            }

        } catch (Exception e) {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
        }

        return page;

    }
    public class wrapAccount {
        public WCT_H1BCAP__c acc {
            get;
            set;
        }
        public Boolean selected {
            get;
            set;
        }

        //This is the contructor method. When we create a new wrapAccount object we pass a Account that is set to the acc property. We also set the selected value to false
        public wrapAccount(boolean b, WCT_H1BCAP__c a) {
            acc = a;
            selected = b;
        }
    }
}