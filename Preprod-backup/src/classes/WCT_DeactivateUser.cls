/**
    * Class Name  : WCT_DeactivateUser 
    * Description : Class will use to Deactivate USers where Employee has withdrawan Deloitte.  
*/

global class WCT_DeactivateUser implements Database.Batchable<sObject> {
   public String query;

   global WCT_DeactivateUser (String queryStr)
   {
       query=queryStr;
   }
   
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<Contact> contactList){
      Set<String> emailIds=new Set<String>();
      List<User> userList=new List<User>();
      for(Contact obj:contactList)
      {
      	if(obj.Email!=null)
      		emailIds.add(obj.Email);
      }
      
      try
      {
      	userList=[Select id,IsActive from User where Email IN:emailIds and IsActive=true];
      	
      	if(!userList.IsEmpty())
      	{
      		for(User u:userList)
      		{
      			u.IsActive=false;
      		}
      		update userList;
      	}
      	
      }Catch(Exception e)
      {
      	throw e;
      }
      
   }

   global void finish(Database.BatchableContext BC){
   }
}