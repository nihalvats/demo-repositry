/********************************************************************************************************************************
Apex class         : <WCT_Upsert_PassportInfoController>
Description        : <Controller which allows to Insert & Update Passport Info and Upload Attachments>
Type               :  Controller
Test Class         :  <WCT_Upsert_PassportInfoController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 20/05/2016          80%                          --                            License Cleanup Project
************************************************************************************************************************************/   

public with sharing class WCT_Upsert_PassportInfoController extends SitesTodHeaderController{

    /* Public Variables */
    public WCT_Passport__c passportRecord{get;set;}
    public String employeeEncryptId{get;set;}
    public String employeeEmail{get;set;}
    public String employeeLastName{get;set;}
    public String employeeMiddleName{get;set;}
    public String employeeFirstName{get;set;}
    public String passportNo{get;set;}
    public String citizenship{get;set;}
    public String passportCity{get;set;}
    public String passportCountry{get;set;}
    public String passportIssuanceDate{get;set;}
    public String passportExpirationDate{get;set;}
    
    /* Upload Related Variables */
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public List<Attachment> listAttachments {get; set;}
    public Map<Id, String> mapAttachmentSize {get; set;}  
    public String ssMsg {get; set;} 
    public String ssMsgStyle {get; set;} 
    
    /* Error Message Related Variables */
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}  
    public boolean validPage {get;set;}
    public Boolean isError {get;set;}
    public String isErrorText {get;set;}
    public GBL_Attachments attachmentHelper{get; set;}
    
   /* Invoking a Constructor with Passport Info and Attachments  */

     public WCT_Upsert_PassportInfoController() {
          try{
          init();
          getParameterInfo();
          getExistingPassportInfo();
          getAttachmentInfo();
          }
          catch(Exception e) {isError=true; isErrorText=e.getMessage();}  
         }
        public pagereference setStatus() {
        if(isError == true) {
         Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Upsert_PassportInfoController', 'Upsert Passport',isErrorText);
            
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_UPPT_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
        }
        return null;
        }
  

/********************************************************************************************
*Method Name         : <init()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Init() Used for Initializing Attachments and Passport>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     

     private void init() {
          passportRecord = new WCT_Passport__c();
          doc = new Document();
          attachmentHelper = new GBL_Attachments();
          listAttachments = new List < Attachment > ();
          mapAttachmentSize = new Map < Id, String > ();
     }

/********************************************************************************************
*Method Name         : <getParameterInfo()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetParameterInfo() Used to get Parameter Information>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/      

     private void getParameterInfo() {
          ssMsg = ApexPages.currentPage().getParameters().get('ss');
          if (ssMsg != null && ssMsg.equals('1')) {
           ssMsgStyle = 'padding-left:12px;';
          } else {
           ssMsgStyle = 'padding-left:12px;display:none;';
          }
          //    employeeId = [SELECT id, Name, RecordTypeId, Email FROM Contact WHERE RecordType.Name = 'Employee' AND Email =:loggedInContact.Email].Id;
     }

/********************************************************************************************
*Method Name         : <getExistingPassportInfo()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetExistingPassportInfo() Used to get Existing Passport Information>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/      

     private void getExistingPassportInfo() {
         List < WCT_Passport__c > passportList = [SELECT Id, WCT_Employee__c, WCT_Passport__c, WCT_Passport_First_Name__c,
         Passport_Last_Name__c, WCT_Passport_Middle_Name__c,
         WCT_Citizenship__c, WCT_Country_of_Passport__c,
         WCT_Passport_Issuing_City__c, WCT_Passport_Issuance_Date__c, WCT_Passport_Expiration_Date__c,
         WCT_Record_Visible_to_ToD__c
         FROM WCT_Passport__c WHERE WCT_Employee__c = : loggedInContact.id LIMIT 1];
    
         if (passportList.size() > 0) {
             passportRecord = passportList[0];
         }
    
         if (passportRecord.id != null) {
             passportNo = passportRecord.WCT_Passport__c;
             employeeFirstName = passportRecord.WCT_Passport_First_Name__c;
             employeeLastName = passportRecord.Passport_Last_Name__c;
             employeeMiddleName = passportRecord.WCT_Passport_Middle_Name__c;
             citizenship = passportRecord.WCT_Citizenship__c;
             passportCountry = passportRecord.WCT_Country_of_Passport__c;
             passportCity = passportRecord.WCT_Passport_Issuing_City__c;
             passportIssuanceDate = passportRecord.WCT_Passport_Issuance_Date__c.format();
             passportExpirationDate = passportRecord.WCT_Passport_Expiration_Date__c.format();
         }
     }

/********************************************************************************************
*Method Name         : <getAttachmentInfo()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetAttachmentInfo() Used for Retrieving Attachment Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/  
     @TestVisible
     private void getAttachmentInfo() {
         listAttachments = [SELECT ParentId,
         Name,
         BodyLength,
         Id,
         CreatedDate
         FROM Attachment
         WHERE ParentId = : passportRecord.Id
         ORDER BY CreatedDate DESC
         LIMIT 50];
  
         for (Attachment a: listAttachments) {
             String size = null;
    
             if (1048576 < a.BodyLength) {
             // Size greater than 1MB
                 size = '' + (a.BodyLength / 1048576) + ' MB';
             } 
             else if (1024 < a.BodyLength) {
             // Size greater than 1KB
                 size = '' + (a.BodyLength / 1024) + ' KB';
             } 
             else {
                 size = '' + a.BodyLength + ' bytes';
             }
             mapAttachmentSize.put(a.id, size);
        }
     }

/********************************************************************************************
*Method Name         : <save()>
*Return Type         : <Page Reference>
*Param’s             : 
*Description         : <Save() Used to Insert & Update Passport Information>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/    
  
     public pageReference save() {
         if (passportNo == '' || passportNo == null || employeeFirstName == '' || employeeFirstName == null || employeeLastName == '' || employeeLastName == null || citizenship == '' || citizenship == null || passportCountry == '' || passportCountry == null || passportCity == '' || passportCity == null || passportIssuanceDate == '' || passportIssuanceDate == null || passportExpirationDate == '' || passportExpirationDate == null) {
             pageErrorMessage = 'Please fill in all the required fields on the form.';
             pageError = true;
             return null;
         }
    
         try {
             passportRecord.WCT_Employee__c = loggedInContact.id;
             passportRecord.WCT_Passport__c = passportNo;
             passportRecord.WCT_Passport_First_Name__c = employeeFirstName;
             passportRecord.Passport_Last_Name__c = employeeLastName;
             passportRecord.WCT_Passport_Middle_Name__c = employeeMiddleName;
             passportRecord.WCT_Citizenship__c = citizenship;
             passportRecord.WCT_Country_of_Passport__c = passportCountry;
             passportRecord.WCT_Passport_Issuing_City__c = passportCity;
             passportRecord.WCT_Passport_Issuance_Date__c = Date.parse(passportIssuanceDate);
             passportRecord.WCT_Passport_Expiration_Date__c = Date.parse(passportExpirationDate);
             passportRecord.WCT_Record_Visible_to_ToD__c = true;
             upsert passportRecord;
     
             if (attachmentHelper.docIdList.isEmpty()) {
                 pageErrorMessage = 'Attachment is required to submit the form.';
                 pageError = true;
                 return null;
             }
             getAttachmentInfo();
             attachmentHelper.uploadRelatedAttachment(passportRecord.Id);
             } 
             catch (Exception e) {
                Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Upsert_PassportInfoController', 'Upsert Passport', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_UPPT_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
             }
             PageReference retPage = Page.WCT_Upsert_PassportThankYou;
             retPage.setRedirect(true);
             retPage.getParameters().put('ss', '1');
             return retPage;
     }
}