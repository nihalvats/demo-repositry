/********************************************************************************************************************************
Apex class         : <WCT_Mob_Visa_Extn_FormController>
Description        : <Controller which allows to Update Task, Assignment and Mobility>
Type               :  Controller
Test Class         : <WCT_Mob_Visa_Extn_Form_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 24/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/

public  class WCT_Mob_Visa_Extn_FormController extends SitesTodHeaderController{

    //TASK RELATED VARIABLES
    public task taskRecord{get;set;}
    public WCT_Task_Reference_Table__c taskRefRecord{get;set;}
    public String taskSubject {get;set;}
    public String taskid{get;set;}
    public boolean display{get;set;}
    public boolean visaExtension{get;set;}
    public boolean noExtension{get;set;}
    public String taskVerbiage{get;set;}
    
    //PUBLIC  VARIABLES
    public WCT_Mobility__c MobilityRec {get;set;}
    public WCT_Assignment__c AssignRec {get;set;}
    
    //ERROR RELATED VARIABLES
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    
    // DEFINING A CONSTRUCTOR 
    public WCT_Mob_Visa_Extn_FormController()
    {
        init();
        taskid = ApexPages.currentPage().getParameters().get('taskid');
        display=true;
        visaExtension=false;
        noExtension=false;
        taskSubject = '';
        taskVerbiage = '';
    }

    /********************************************************************************************
    *Method Name         : <init()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <Init() Used for loading Mobility,Assignment>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/     
   
    private void init(){
        //Custom Object Instances
        MobilityRec = new WCT_Mobility__c();
        AssignRec = new WCT_Assignment__c();
    }   
    
    /********************************************************************************************
    *Method Name         : <updateTaskFlags()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <UpdateTaskFlags() Used for Fetching Task Reference and Mobility/Assignment Record>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/     

    public PageReference updateTaskFlags()
    {
        if(taskid==''|| taskid==null){
            display=false;
            return null;
        }
        try {
        taskRecord=[SELECT id, Subject, WhatId, Ownerid, Status, WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c, WCT_Task_Reference_Table_ID__c FROM Task WHERE Id =: taskid];
        taskRefRecord = [SELECT Id, Name, WCT_Task_for_Object__c, WCT_Task_Subject__c, Form_Verbiage__c, ownerId FROM WCT_Task_Reference_Table__c Where id=:taskRecord.WCT_Task_Reference_Table_ID__c];
        MobilityRec = [SELECT Id, Name, OwnerId FROM WCT_Mobility__c WHERE Id=:taskRecord.WhatId];
        AssignRec = [Select Id, Name, WCT_Status__c, WCT_Mobility__c FROM WCT_Assignment__c WHERE WCT_Mobility__c =: MobilityRec.Id AND (WCT_Status__c = 'Active' OR WCT_Status__c = 'New')];
        
        taskSubject = taskRecord.Subject;
        taskVerbiage = taskRefRecord.Form_Verbiage__c;
        }
        catch (Exception e) {
            //WCT_ExceptionUtility.logException('WCT_Mob_Visa_Extn_FormController', 'Mob Visa Extn Form', e.getMessage());
            
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Mob_Visa_Extn_FormController', 'Mob Visa Extn Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_MVEXT_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
        }
        return null;
    }

        /********************************************************************************************
        *Method Name         : <save()>
        *Return Type         : <PageReference>
        *Param’s             : 
        *Description         : <Save() Used for for Updating Task, Assignment and Mobility>
        
        *Version          Description
        * -----------------------------------------------------------------------------------------------------------                 
        * 01              Original Version
        *********************************************************************************************/         
        
    public pageReference save()
    {
        if(noExtension != true && visaExtension != true)
        {
            pageErrorMessage = 'Please check any one of the checkboxes in order to submit the form.';
            pageError = true;
            return null;
        }
        if(noExtension == true && visaExtension == true)
        {
            pageErrorMessage = 'Please check any ONE checkbox in order to submit the form.';
            pageError = true;
            return null;
        }
        if(noExtension == true){
            MobilityRec.WCT_Visa_Extension_Status__c = 'Extension not required';
            AssignRec.WCT_Assignment_Visa_Extension_Status__c = 'Extension not required';
        }
        else if(visaExtension == true){
            MobilityRec.WCT_Visa_Extension_Status__c = 'Applied for a visa extension';
            AssignRec.WCT_Assignment_Visa_Extension_Status__c = 'Applied for a visa extension';
        }
        update MobilityRec;
        update AssignRec;
    
        /*Changing the Owner Id of Task Temporarily to avoid Integrity Exception*/
        try{
            taskRecord.OwnerId=UserInfo.getUserId();
            upsert taskRecord;
            if(string.valueOf(MobilityRec.OwnerId).startsWith('00G')){
                taskRecord.OwnerId = System.Label.GMI_User;
            }else{
                taskRecord.OwnerId = MobilityRec.Ownerid;
            }
            if(taskRecord.WCT_Auto_Close__c == true){   
                taskRecord.status = 'Completed';
            }else{
                taskRecord.status = 'Employee Replied';  
            }
            taskRecord.WCT_Is_Visible_in_TOD__c = false; 
            upsert taskRecord;
            pageError = false;
        }
        catch (Exception e) {
           // WCT_ExceptionUtility.logException('WCT_Mob_Visa_Extn_FormController', 'Mob Visa Extn Form', e.getMessage());
            
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Mob_Visa_Extn_FormController', 'Mob Visa Extn Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_MVEXT_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
        }
        PageReference pageRef = new PageReference('/apex/WCT_Mob_Visa_Extension_FormThankYou?taskid='+taskid);  
        pageRef.setRedirect(true);
        return pageRef;
    }
}