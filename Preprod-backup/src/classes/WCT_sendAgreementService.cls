/**
    * Class Name  : WCT_sendAgreementService
    * Description : This apex class will use to send agreements. 
*/

global class WCT_sendAgreementService {
      
      
   /** 
        Method Name  : sendOffers
        Return Type  : Void
    */
  
    webService static void sendOffers(Id recordId, Id contactId, String agreementName, String olTitle, String message)
    {
        try {
            if(message == '')
            {
            message = label.ESOfferMessage;
            }
            echosign_dev1__SIGN_Agreement__c agmt = new echosign_dev1__SIGN_Agreement__c();
            agmt.Name = agreementName;
            agmt.echosign_dev1__Recipient__c = ContactId;
            agmt.WCT_Offer__c =  recordId;
            agmt.echosign_dev1__Status__c = 'Draft';
            agmt.echosign_dev1__Background_Action__c = 'Send';
            agmt.echosign_dev1__DaysUntilSigningDeadline__c = 120;
            agmt.echosign_dev1__Message__c = EncodingUtil.urlDecode(message, 'UTF-8');
            agmt.echosign_dev1__Enable_Automatic_Reminders__c = true;
            INSERT agmt;
                    
            Attachment att = [SELECT parentId, name, body, contenttype FROM Attachment WHERE parentId=:recordId];
             
            Attachment att2 = att.clone();
            att2.name = olTitle+'.pdf';
            att2.parentId = agmt.id;
            INSERT att2; 
                    
        } catch(Exception e) {
            WCT_ExceptionUtility.logException('WCT_SendOffers','sendOffers',e.getMessage());
            throw e;
        }
    }
}