public with sharing class WCT_OfferApprover {
    String OfferId;
    public  boolean taleostatus{get;set;}    
    public WCT_OfferApprover(){
        OfferId = System.currentPagereference().getParameters().get('id');
    }
    public PageReference RedirectToInterview(){
        list<WCT_Offer__c> upOffStatus;
        upOffStatus = [select id,WCT_Taleo_Field_Updated__c,WCT_status__c,recordtypeid from WCT_Offer__c where id=:OfferId];
        if(upOffStatus.size()>0)
        {
            
                upOffStatus[0].WCT_status__c = 'Offer Approved';
                update upOffStatus;
                return new PageReference('/apex/WCT_OfferApproverThankYou');
            
        }
        else
        {
            return null;
        }
    }
}