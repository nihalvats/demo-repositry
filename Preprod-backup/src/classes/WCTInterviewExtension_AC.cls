/*
Class name : WCTInterviewExtension_AC
Description : This class is controller for Wct_UpsertInterview page.

Task number     Date            Modified By                         Description
------------    ---------       ---------------------               ----------------------
                18-Dec-13       Sreenivasa Munnangi(Deloitte)           Created
                21-Dec-13       Nilesh Adkar (Deloitte)                 Added logic for Create/edit interview, interview junction.
                                                                        Added methods saveInterview,addCandidate,removeCandidate
                                                                        Constructor modified
                                                            
*/
public class WCTInterviewExtension_AC {
    
    Public Static final String NEW_STATUS='New';
    Public Static final String OPEN_STATUS='Open';
    Public Static final String INPROGRESS_STATUS='In Progress';
    public String contactName {get;set;}
    public String contactPhone{get;set;}
    public String contactEmail{get;set;}
    public String reqNum{get;set;}
    public String rmsID{get;set;}
    public String soql {get;set;}
    private Id candidateRecordTypeId = WCT_Util.getRecordTypeIdByLabel('Contact','Candidate');
    private Id candidateAsdHocRecordTypeId = WCT_Util.getRecordTypeIdByLabel('Contact','Candidate Ad Hoc');
 
    public List<WCT_Candidate_Requisition__c> ctList {get;set;}
    public List<WCT_Candidate_Requisition__c> ctSelectedList {get;set;}
    private List<WCT_Interview_Junction__c > ijList;
    private WCT_Interview__c interviewObj; 
    private string interviewId {get;set;}
    private List<WCT_Interview_Junction__c > lstInterviewJunction {get;set;}
    public boolean isAddError {get;set;}
    private set<Id> setCandReqId = new set<Id>();
    public string strInterviewStatus{get;set;}

   //-----------------------------------
   //    Constructor
   //-----------------------------------    
    public WCTInterviewExtension_AC (ApexPages.StandardController controller) {
    
        //controller.addFields(new LIST<string>{'WCT_Interview_Status__c'});
        ctSelectedList = new List<WCT_Candidate_Requisition__c>();
        
    // Get Interview object
        this.interviewObj= (WCT_Interview__c)controller.getRecord();
        
    // If update operation    
        if(interviewObj.id!=null){
            
            lstInterviewJunction = [select WCT_Candidate_Tracker__c from WCT_Interview_Junction__c where WCT_Interview__c= :interviewObj.id];
            for(WCT_Interview_Junction__c tmpJunction : lstInterviewJunction ){
                setCandReqId.add(tmpJunction.WCT_Candidate_Tracker__c );
            }
            if(setCandReqId.size()>0){
                ctSelectedList =[select WCT_Contact__r.Name, WCT_Requisition__r.Name ,WCT_Select_Candidate_For_Interview__c 
                                 from WCT_Candidate_Requisition__c 
                                 where id in : setCandReqId];
            }
            strInterviewStatus = interviewObj.WCT_Interview_Status__c;
        }
        else
        {
            strInterviewStatus = 'Open';
        }        
        soql = 'SELECT id, Name, WCT_Contact__r.Name,WCT_Requisition__c,WCT_Contact__r.FirstName,WCT_Contact__r.LastName, WCT_Requisition__r.Name,WCT_Select_Candidate_For_Interview__c '+
                                 'from WCT_Candidate_Requisition__c WHERE WCT_Candidate_Requisition_Status__c IN (\''+NEW_STATUS+'\',\''+INPROGRESS_STATUS+'\')'+
                                 ' AND id not in :setCandReqId';
                                 // and WCT_Contact__r.recordtypeid in (\''+candidateRecordTypeId+'\',\''+candidateAsdHocRecordTypeId+'\')';
        ctList = Database.query(soql + ' ORDER BY CREATEDDATE DESC limit 200');
    }

    
   public List<WCT_Candidate_Requisition__c> getCandidateTrackers()
   {
      return ctList;
   }
   
   //-----------------------------------
   //    Search Candidate Tracker
   //-----------------------------------
   public PageReference doSearch() {
   
      soql = 'SELECT id, Name, WCT_Contact__r.Name,WCT_Contact__r.FirstName,WCT_Contact__r.LastName,WCT_Requisition__c, WCT_Requisition__r.Name,WCT_Select_Candidate_For_Interview__c '+
                                'from WCT_Candidate_Requisition__c WHERE WCT_Candidate_Requisition_Status__c IN (\''+NEW_STATUS+'\',\''+INPROGRESS_STATUS+'\')';
                                 //' AND WCT_Contact__r.recordtypeid in (\''+candidateRecordTypeId+'\',\''+candidateAsdHocRecordTypeId+'\') ';
      if (!contactName.equals('')){
          soql += ' AND (WCT_Contact__r.FirstName LIKE \''+String.escapeSingleQuotes(contactName)+'%\' ';
          soql += ' OR WCT_Contact__r.Name LIKE \''+String.escapeSingleQuotes(contactName)+'%\' ';
          soql += ' OR WCT_Contact__r.LastName LIKE \''+String.escapeSingleQuotes(contactName)+'%\') ';
      }
      if(''.equals(contactPhone)==false){
          soql += ' and (WCT_Contact__r.HomePhone Like \''+String.escapeSingleQuotes(contactPhone)+'%\'' +
                  ' OR WCT_Contact__r.MobilePhone Like \''+String.escapeSingleQuotes(contactPhone)+'%\' )';
      }
      if(''.equals(contactEmail)==false){         
          soql += ' and WCT_Contact__r.Email Like \''+String.escapeSingleQuotes(contactEmail)+'%\'';
      }
      if(''.equals(reqNum)==false){   
          soql += ' and WCT_Requisition__r.Name Like \''+String.escapeSingleQuotes(reqNum)+'%\'';       
      
      }
      if(''.equals(rmsID)==false){   
          soql += ' and (WCT_Taleo_Id__c Like \''+String.escapeSingleQuotes(rmsID)+'%\' or WCT_Contact__r.WCT_Taleo_Id__c like\''+String.escapeSingleQuotes(rmsID)+'%\')';       
      
      }
      
      try{
          ctList = Database.query(soql + ' ORDER BY WCT_Contact__r.Name limit 200');
      }    
      catch(DmlException ex){
         for (Integer i = 0; i < ex.getNumDml(); i++) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getDmlMessage(i)));
         }
      }
      catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
      }    
      return null;
      
   }
  
   public List<WCT_Interview_Junction__c > getSelectedCandidates()
   {
      return ijList;
    }
 
    
    //---------------------------
    //    Add candidate to List
    //---------------------------
    public void addCandidate(){
        isAddError = false;
        /*if(interviewObj!=null && interviewObj.id!=null && interviewObj.WCT_Interview_Status__c != OPEN_STATUS){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Candidates Can be added or removed only when the Interview Status is Open !!'));
            isAddError = true;
            return;
        }*/
        for(WCT_Candidate_Requisition__c  tmpCandTrack : ctList){
            if(tmpCandTrack.WCT_Select_Candidate_For_Interview__c==true){
                tmpCandTrack.WCT_Select_Candidate_For_Interview__c=false;
                for(WCT_Candidate_Requisition__c  tmpCandTrackSelect :ctSelectedList){
                    if(tmpCandTrackSelect.id== tmpCandTrack.id){
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Candidate already added !!'));
                        isAddError = true;
                        return;
                    }
                }
                ctSelectedList.add(tmpCandTrack);
            }
        }
        set<WCT_Candidate_Requisition__c> setCandTrack = new set<WCT_Candidate_Requisition__c>(ctList);
        for(WCT_Candidate_Requisition__c  tmpCandTrack : ctSelectedList){
            setCandTrack.remove(tmpCandTrack );
        }
        ctList.clear();
        for(WCT_Candidate_Requisition__c  tmpCandTrack : setCandTrack ){
            ctList.add(tmpCandTrack );
        }
        ctList.sort();
        ctSelectedList.sort();
    }
 
            
    //------------------------------
    //    Remove candidate from List
    //------------------------------
    public void removeCandidate(){
        for(WCT_Candidate_Requisition__c  tmpCandTrack : ctSelectedList){
            if(tmpCandTrack.WCT_Select_Candidate_For_Interview__c==true){
                tmpCandTrack.WCT_Select_Candidate_For_Interview__c=false;
                ctList.add(tmpCandTrack);
            }
        }
        set<WCT_Candidate_Requisition__c> setCandTrack = new set<WCT_Candidate_Requisition__c>(ctSelectedList);
        for(WCT_Candidate_Requisition__c  tmpCandTrack : ctList){
            setCandTrack.remove(tmpCandTrack );
        }
        ctSelectedList.clear();
        for(WCT_Candidate_Requisition__c  tmpCandTrack : setCandTrack ){
            ctSelectedList.add(tmpCandTrack );
        }
        ctList.sort();
        ctSelectedList.sort();
    }

    
    //----------------------------------------------
    //    Create Interview and Interview Junction 
    //----------------------------------------------
    public pageReference saveInterview(){
        if(ctSelectedList.size()==0){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select at least one candidate.'));
            isAddError = true;
            return null;
        }
        isAddError = false;
        WCT_Interview_Junction__c objIntvJunction ;
        List<WCT_Interview_Junction__c > lstIntvJunction = new List<WCT_Interview_Junction__c >();

        //Database.SaveResult result = Database.insert(interviewObj,false);
        try{
            if(interviewObj.id==null){        
                insert interviewObj;
                
                for(WCT_Candidate_Requisition__c tmpCandTrack : ctSelectedList ){
                   objIntvJunction = new WCT_Interview_Junction__c ();
                   objIntvJunction.WCT_Candidate_Tracker__c=tmpCandTrack.id;
                   objIntvJunction.WCT_Interview__c = interviewObj.id;
                   lstIntvJunction.add(objIntvJunction);
                }
                Database.SaveResult[] lstResult = Database.insert(lstIntvJunction,false);
            }
            else{
                    update interviewObj;
                    boolean isRemoved = true;
                    for(WCT_Interview_Junction__c tmpJunction : lstInterviewJunction ){
                        isRemoved = true;
                        for(WCT_Candidate_Requisition__c tmpCandReq : ctSelectedList ){
                            if(tmpJunction.WCT_Candidate_Tracker__c==tmpCandReq.id){
                                isRemoved = false;
                            }
                        }
                        if(isRemoved == true){
                            lstIntvJunction.add(tmpJunction );
                        }
                    }
                    delete lstIntvJunction;
                    boolean isNewInList = true;
                    for(WCT_Candidate_Requisition__c tmpCandReq : ctSelectedList ){ 
                        isNewInList = true;
                        for(WCT_Interview_Junction__c tmpJunction : lstInterviewJunction){
                            if(tmpJunction.WCT_Candidate_Tracker__c==tmpCandReq.id){
                                isNewInList = false;
                            }
                        }
                        if(isNewInList == true){
                           objIntvJunction = new WCT_Interview_Junction__c ();
                           objIntvJunction.WCT_Candidate_Tracker__c=tmpCandReq.id; 
                           objIntvJunction.WCT_Interview__c = interviewObj.id;
                           lstIntvJunction.add(objIntvJunction);
                        }
                    }    
                    Database.SaveResult[] lstResult = Database.insert(lstIntvJunction,false);
                    integer candidateCount = 0;                    
                    for(WCT_Candidate_Requisition__c tmpCandReq : ctSelectedList){
                        for(Id tmpId : setCandReqId ){
                            if(tmpId == tmpCandReq.id)
                                candidateCount++;
                        }
                    }
                    //system.debug('*candidateCount*'+candidateCount+'*lstInterviewJunction.size()*'+lstInterviewJunction.size()+'*ctSelectedList.size()*'+ctSelectedList.size());
                    if(candidateCount != ctSelectedList.size()){
                        List<Event> lstEvent = [select id from event where WhatId=:interviewObj.id];
                        Set<Id> setEventIds = new  Set<Id>();
                        for(Event tmpEvent : lstEvent ){
                            setEventIds.add(tmpEvent.id);
                        }                            
                        WCT_Interview_Scheduling_Utill.handleCandidates(setEventIds);
                    }
            }
            PageReference pageRef = new PageReference('/'+this.interviewObj.id);
            pageRef.setRedirect(true);       
            return pageRef ;            
        }
        catch(DmlException ex){
            for (Integer i = 0; i < ex.getNumDml(); i++) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getDmlMessage(i)));
            }
            return null;
        }
        catch(Exception ex){ 
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,ex.getMessage()));
            return null;
        }

    }
}