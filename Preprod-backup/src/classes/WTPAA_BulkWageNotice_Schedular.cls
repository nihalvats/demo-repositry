global  class WTPAA_BulkWageNotice_Schedular implements Schedulable  {

  global void execute(SchedulableContext sc) {
     System.debug('## Wage Notice Send Schedular Started');
      WTPAA_SendBulkNotices sendBulkNotices = new WTPAA_SendBulkNotices(null);
      //sendBulkNotice();
      System.debug('## Wage Notice Send Schedular Ended');
      
   }
    
    
    /*Backup plan , if nothing work with automated send. */
   @future(callout=true)
    global static void sendBulkNotice()
    {
       WTPAA_SendBulkNotices sendBulkNotices = new WTPAA_SendBulkNotices(null); 
    }
}