public with sharing class WCT_Util {
    //------------------------------
    // User need to pass two parameters SObject Type & 
    //RecordType Label ( not Record Type Developer Name)
    //------------------------------   
    public static Id getRecordTypeIdByLabel(String objectType, String recordTypeLabel){
      SObject obj;
      // Describing Schema
      Schema.SObjectType res = Schema.getGlobalDescribe().get(ObjectType);
      if (res != null)
      {
          obj= Res.newSObject();
          // Describing Object 
          Schema.DescribeSObjectResult desRes = OBJ.getSObjectType().getDescribe(); 
          if (desRes != null){
             Map<String,schema.recordtypeinfo> recordTypeMap = desRes.getRecordTypeInfosByName();
             if (recordTypeMap != null) 
             {
                Schema.RecordTypeInfo RecordTypeRes = recordTypeMap.get(RecordTypeLabel);
                if (recordTypeRes != null) 
                {
                   return recordTypeRes.getRecordTypeId();
                }
              }
           }
       }
        return null;
    } 
    //------------------------------
    // User need to pass two parameters SObject Type & 
    //RecordType Developer Name
    //------------------------------ 
    public static Id getRecordTypeIdByDeveloperName(String objectType, String recordTypeDevName){
      RecordType rtype;
      try{
          rtype = [SELECT id,DeveloperName,IsActive,Name from RecordType where SobjectType =:objectType AND DeveloperName=:recordTypeDevName];
      }catch(QueryException queryException){
           WCT_ExceptionUtility.logException('Rtype Class','Record Type',queryException.getMessage());
      }
      if(rtype != null){
        return rtype.id;
      }
      return null;
    }
  
    // This is a generalised method to send the email notification to the System admin user.
    public static void sendMailToLoginUser(String subject, String body){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        Id reportingUserId = System.Label.SFDC_Admin_User_Id;
        if(reportingUserId != null){
            mail.setTargetObjectId(reportingUserId);
        }else{ 
            mail.setTargetObjectId(UserInfo.getUserId());
        }
        mail.setSubject(subject);
        mail.setPlainTextBody(body);
        mail.setSaveAsActivity(false);
        Messaging.sendEmail(new Messaging.SingleEmailMessage [] {mail});
        System.debug('Mail Sent:: ' +  subject );
    }
        // This is a generalised method to send the email notification to the System admin user.
   /* public static void sendMailToSpecificUser(String subject, String body, String[] ToEmailAddress){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //Id reportingUserId = System.Label.SFDC_Admin_User_Id;
        //if(reportingUserId != null){
        //    mail.setTargetObjectId(reportingUserId);
        //}else{ 
        //    mail.setTargetObjectId(UserInfo.getUserId());
        //}
        if(ToEmailAddress == null){
             ToEmailAddress.add(UserInfo.getUserEmail());
        }        
        mail.settoAddresses(ToEmailAddress);
        mail.setSubject(subject);
        mail.setPlainTextBody(body);
        mail.setSaveAsActivity(false);
        Messaging.sendEmail(new Messaging.SingleEmailMessage [] {mail});
        System.debug('Mail Sent:: ' +  subject );
    }  */

}