public class MIS_Email_Popup {
    public list<Case_form_Extn__c> CFErecord{set;get;}
    public list<Case_form_Extn__c> rec{set;get;}
    public string recordid{set;get;}
    public string status{set;get;}
    public string teamname{set;get;}
    public string owner{set;get;}
    public string tempid{set;get;}
    public string tempbody{set;get;}
    public string tempsub{set;get;}
    public string name{set;get;}
    public string cc{set;get;}
    public string numb{set;get;}
    public boolean checkbox{set;get;}
    public list<Case_form_Extn__c> cfs{set;get;}
    public list<EmailTemplate> email{set;get;}
    public list<Case_form_Extn__c> cfecheck{set;get;}
    
    
    public MIS_Email_Popup(ApexPages.StandardController controller) {      
     //getting the id of the record from parameter
    recordid=ApexPages.currentPage().getParameters().get('id');
    
   if (recordid!=null){
        //querying the record of Case Form Extension
       CFErecord = [select id,TRT_Assigned_Owner__c,MIS_RptName__c,MIS_RptNumber__c,MIS_Status_of_Report__c,MIS_Email_poped__c,MIS_DL_Email_id__c,MIS_Name_of_the_Team__c,Name from Case_form_Extn__c where id =:recordid LIMIT 1];
        
       status=CFErecord[0].MIS_Status_of_Report__c;
       
        owner=CFErecord[0].TRT_Assigned_Owner__c; 
        teamname=CFErecord[0].MIS_Name_of_the_Team__c;
        numb=CFErecord[0].MIS_RptName__c; 
        if(CFErecord[0].MIS_DL_Email_id__c!=null){
        cc=CFErecord[0].MIS_DL_Email_id__c;
        cc=cc.replace(',',';');
        }else{
            cc='';
        }  
        checkbox=CFErecord[0].MIS_Email_poped__c;
        name=CFErecord[0].MIS_RptNumber__c;   
    }  
         tempid= Label.MIS_Repot_submission_Email_Teamp_Id;
      //querying the body and subject from templete
        email=[SELECT Body,Subject FROM EmailTemplate WHERE Id=:tempid LIMIT 1]; 
        tempbody=email[0].Body;
        tempsub=email[0].Subject;
        if(name!=null){
        tempsub=tempsub.replace('{!Case_form_Extn__c.Name}',name);
        }else{
          tempsub=tempsub.replace('{!Case_form_Extn__c.Name}','');  
        }if(numb!=null){
         tempsub=tempsub.replace('Number',numb); 
         }
         else{
         tempsub=tempsub.replace('Number','');
         }
         if(name!=null){
        tempbody=tempbody.replace('{!Case_form_Extn__c.Name}',name);
         }else{
          tempbody=tempbody.replace('{!Case_form_Extn__c.Name}','');   
         }
        if(numb!=null){
        tempbody=tempbody.replace('Number',numb);
         }else{
           tempbody=tempbody.replace('Number','');  
         }
        
        tempbody=tempbody.replace('{!Case_form_Extn__c.TRT_Assigned_Owner__c}',owner); 
        
        if(teamname!=null){
        tempbody=tempbody.replace('{!Case_form_Extn__c.MIS_Name_of_the_Team__c}',teamname);
        }
        else
        {
          tempbody=tempbody.replace('{!Case_form_Extn__c.MIS_Name_of_the_Team__c}','');   
        }  
       system.debug('record is'+CFErecord); 

    } 
     
     
    
        public void init(){
            
           //updating the record of Case Form Extension
             cfecheck=[select id,MIS_Email_poped__c from  Case_form_Extn__c where id=:recordid LIMIT 1 ];   
            
            cfecheck[0].MIS_Email_poped__c=true;
        
            update cfecheck[0];
        } 
          
     
        }