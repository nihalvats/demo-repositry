/**
    * Class Name  : WCT_NominationsApprovedbyImm
    * Description : This apex class will use to change the status. 
*/

global class WCT_NominationsApprovedbyImm{
  
   /** 
        Method Name  : NominationsApprovedbyImm
        Description  : Find all records and changes the status to Practitioner Nominated       
    */
  
    WebService static String NominationsApprovedbyImm(List<Id> recordIds)
    {
        List<WCT_H1BCAP__c> h1bcapList = new List<WCT_H1BCAP__c>();
        String returnStatement=Label.WCT_Message_Processing_Starting;
        List<String> H1bcapstatus =Label.WCT_H1BCAP_Status.split(',');
      
        
        for(WCT_H1BCAP__c H1bcapRecord: [SELECT Id, WCT_H1BCAP_Status__c FROM WCT_H1BCAP__c WHERE WCT_H1BCAP_Status__c = :H1bcapstatus AND ID =:recordIds LIMIT:Limits.getLimitQueryRows()])
        {    
           
            H1bcapRecord.WCT_H1BCAP_Status__c = 'Practitioner Nominated';
            h1bcapList .add(H1bcapRecord);
          
        }
        
        if(!h1bcapList .IsEmpty())
        {
            try{
                system.debug('Size of the h1blist***'+h1bcapList.size());
                UPDATE h1bcapList;
            }
            Catch(Exception e)
            {
                WCT_ExceptionUtility.logException('WCT_FindWageNoticesReadyToSend ','NominationsApprovedbyImm',e.getMessage());
                throw e;
            }
        }
        else
        {
            returnStatement=WCT_UtilConstants.PROCESS_NONE;
        }
        return returnStatement;
    }
}