/******************************************************************************************************************* 
    * Class Name   : LeaveSurveyController
    * VF Page Name : Adoption, Non_Disability_LeaveSurvey, Disability_LeaveSurvey
    * Description  : This Class is leave survey  
    * Developer    : Karthik
  *****************************************************************************************************************/

public with sharing class LeaveSurveyController extends SitesTodHeaderController{    //with sharing
    /*Public Variables*/
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    public integer childleavecnt {get;set;}
    public WCT_Parental_Leave_Survey__c pl {get; set;}
    public integer paidweekcnt {get;set;}
    public integer ptoweekcnt {get;set;}
    public integer unpaidweekcnt {get;set;}
    public integer totalweeks{get;set;}
    public boolean  question1 {get;set;}
    public boolean  question2 {get;set;}
    public boolean  question3 {get;set;}
    public boolean  Insurence {get;set;}
    public boolean  InsurenceN {get;set;}
    public WCT_Leave__c le {get; set;}
    public WCT_Leave__c objLve{get;set;}
    public WCT_Leave__c objLeave{get;set;}
    public integer parentleavecnt {get;set;}
    Public string ParentLeaveRecordName; 
    public integer paidleavecnt{get;set;}
    public integer ptoleavecnt{get;set;}
    public integer unpaidleavecnt {get;set;}
    public integer RemainPTO {get;set;}
    public integer ptohol {get;set;}
    public integer RemainPTOdays {get; set;}
    public integer ptoweekcount {get; set;}
    public integer intDaysRemain {get;set;}
    public String Displaystr {get;set;}
    public String Displaystr1 {get;set;}
    public String Displaystr2 {get;set;}
    public integer paidDaycnt {get;set;}
    public String xLeaveid{ get; set; }
    
    
    // CONSTRUCTOR
    public LeaveSurveyController() {
        xLeaveid = apexpages.currentpage().getparameters().get('Param1');
        pl = new WCT_Parental_Leave_Survey__c();
        le = new WCT_Leave__c();
        
        //leaves calculation method
        objLve = new WCT_Leave__c();
        
        objLeave = new WCT_Leave__c();
       
        // parent record query
        List<WCT_Leave__c> listLeave = [SELECT Id,Name,WCT_Deloitte_Email__c,RecordTypeid,RecordType.Name,WCT_Leave_Category__c,WCT_Sub_Category_2__c,WCT_Related_Record__c,WCT_Sub_Category_1__c,WCT_Leave_Start_Date__c,
                                        WCT_Last_Day_Worked__c,WCT_Return_to_work_date__c,WCT_Leave_End_Date__c,
                                        WCT_Personnel__c,WCT_Parental_Event__c,WCT_Parental_Leave_Exhausted__c,
                                        WCT_Days__c,WCT_Leave_survey_submitted__c FROM WCT_Leave__c
                                        WHERE WCT_Deloitte_Email__c = :loggedInContact.email AND WCT_Related_Record__c = '' AND RecordType.Name = 'LOA Code'
                                        AND Id = :xLeaveid];
                                        //Order By CreatedDate DESC LIMIT 1];
                   
    
        
        parentleavecnt = listLeave.size();
        system.debug('parentleavecnt**************'+parentleavecnt);
        system.debug('listLeave**************'+listLeave);
        try{
            ParentLeaveRecordName = listLeave[0].name;
            
            system.debug('ParentLeaveRecordName**************'+ParentLeaveRecordName);
        }catch(System.ListException e){}
        
        if(ParentLeaveRecordName == null){
            pageErrorMessage  = 'No Records found';
            return ;  //System.ListException: List index out of bounds: 0 
        }
        
        // child record query
        list<WCT_Leave__c> objLve1 = [Select id,Name,RecordTypeid,RecordType.Name,WCT_Leave_Category__c,WCT_Sub_Category_2__c,WCT_Related_Record__c,WCT_Days__c 
                                        From WCT_Leave__c Where WCT_Related_Record__r.name =:ParentLeaveRecordName ];
        system.debug('objLve1**************'+objLve1);
        
        ptohol = 0;
        list<WCT_Leave__c> childlev = [Select id,Name,RecordTypeid,WCT_Days__c,WCT_Leave_Category__c 
                                        From WCT_Leave__c Where WCT_Related_Record__r.name =:ParentLeaveRecordName AND 
                                        RecordType.Name=:'PTO/HOL' 
                                        Order By CreatedDate ASC LIMIT 1];
                                        
                                        //  AND WCT_Leave_Category__c=: 'Disability Parental'
            system.debug('childlev**************'+childlev);
            //system.debug('childlev**************'+childlev[0].WCT_Days__c);
        if(childlev.size() > 0){
            ptohol = integer.valueof(childlev[0].WCT_Days__c);
            system.debug('ptohol**************'+ptohol);
        }
        
        paidleavecnt = 0;
        ptoleavecnt = 0;
        unpaidleavecnt = 0;
        
        childleavecnt = objLve1.size();
        
        if(objLve1.size() < 1){
            pageErrorMessage = 'This Survey Can not be taken due to improper data';
            
            system.debug('return***********'+objLve1.size());
            return;
        }
        // contact records query
        Contact Contactlist = [SELECT Id, Name, FirstName,WCT_Benefit_plan__c,Dependent__c,Plan_Num__c FROM Contact WHERE Email = :loggedInContact.Email LIMIT 1  ];
        
        //string type;
        Map<ID, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.WCT_Leave__c.getRecordTypeInfosById();
        
        if( listLeave[0].WCT_Parental_Event__c == 'Birth' &&  (Contactlist.Plan_Num__c == 371 || Contactlist.Plan_Num__c == 842 ||
                                                               Contactlist.Plan_Num__c == 843 || Contactlist.Plan_Num__c == 844 || Contactlist.Plan_Num__c == 912 || 
                                                               Contactlist.Plan_Num__c == 855 || Contactlist.Plan_Num__c == 856 || Contactlist.Plan_Num__c == 857 ) &&
           Contactlist.WCT_Benefit_plan__c != Null)
        {
            if(Contactlist.Dependent__c == 'EE' || Contactlist.Dependent__c == 'EE+C' || Contactlist.Dependent__c == 'EE+S' || Contactlist.Dependent__c == 'EED' || 
               Contactlist.Dependent__c == 'EE+F' || Contactlist.Dependent__c == 'EEDF' || Contactlist.Dependent__c == 'EEDG' )
            {
                Insurence = True;
                system.debug('Insurence*****'+Insurence);
            } 
            
            if(Contactlist.Dependent__c == 'EE+S' || Contactlist.Dependent__c == 'EED' || Contactlist.Dependent__c == 'EE+F' || 
               Contactlist.Dependent__c == 'EEDF' || Contactlist.Dependent__c == 'EEDG' )
            { InsurenceN = True;  }    
            
        }
        
        for(WCT_Leave__c l : objLve1){
            system.debug('categery*****'+l.WCT_Sub_Category_2__c);
            system.debug('paidleavecnt*****'+l.WCT_Days__c);
            system.debug('leavecategery*****'+l.WCT_Leave_Category__c);
            
            //leave calculations
            if(l.WCT_Sub_Category_2__c == 'Paid'&& (rtMap.get(l.RecordTypeId).getName() == 'Parental' ) && (l.WCT_Leave_Category__c == 'Disability Parental' || l.WCT_Leave_Category__c == 'Non-Disability Parental' || l.WCT_Leave_Category__c == null )  ){
                paidleavecnt = paidleavecnt+integer.valueof(l.WCT_Days__c);    
            }
            else if(l.WCT_Sub_Category_2__c == 'Unpaid' && rtMap.get(l.RecordTypeId).getName() == 'Parental' && (l.WCT_Leave_Category__c == 'Disability Parental' || l.WCT_Leave_Category__c == 'Non-Disability Parental' || l.WCT_Leave_Category__c == null ) ){
                unpaidleavecnt = unpaidleavecnt+integer.valueof(l.WCT_Days__c); 
            }
            else if(l.WCT_Sub_Category_2__c == 'Paid' && (rtMap.get(l.RecordTypeId).getName() == 'PTO/HOL'  ) && (l.WCT_Leave_Category__c == 'Disability Parental' || l.WCT_Leave_Category__c == 'Non-Disability Parental' || l.WCT_Leave_Category__c == null ) ){
                ptoleavecnt = ptoleavecnt+integer.valueof(l.WCT_Days__c); 
            }
        }
        
        // Weeks calculation
        paidweekcnt = paidleavecnt/5;
        paidDaycnt = (paidleavecnt - (paidweekcnt*5));
        ptoweekcnt = (ptoleavecnt-ptohol)/5;
        ptoweekcount = ptoleavecnt/5;
        unpaidweekcnt = unpaidleavecnt/5;
        
         //Remaining Days Calculation
        intDaysRemain = (unpaidleavecnt - (unpaidweekcnt*5));
        RemainPTO = ((ptoleavecnt-ptohol) - (ptoweekcnt*5));
        RemainPTOdays = (ptoleavecnt - (ptoweekcount*5));
        totalweeks = ptoweekcnt+unpaidweekcnt;
        
        if(paidweekcnt >= 8){
            question1= True;
        }
        if(paidweekcnt < 8){
            question2= True;
        }
        if((ptoweekcnt >= 0) || (unpaidweekcnt >= 0)){
            question3= True;
        }
        
        //************ Display string for weeks and days*********************
        
        // paid parental count
        if(paidweekcnt > 0){
            Displaystr1 = '<b>'+paidweekcnt+'</b> week(s) ';
        }
        if(paidDaycnt > 0){
            Displaystr1 = Displaystr1 + '<b>'+paidDaycnt+'</b> day(s)';
        }
        if((paidweekcnt + paidDaycnt) > 0){
            Displaystr1 = Displaystr1 +' of paid parental leave.'; 
        }
        
        // ********non disability count********
        if(ptoweekcount > 0){
            Displaystr = '<b>'+ptoweekcount+'</b> week(s) ';
        }  else {Displaystr = '';}
        if(RemainPTOdays > 0){
            Displaystr = Displaystr +'<b>'+RemainPTOdays+'</b> day(s)';
        }
        if((ptoweekcount + RemainPTOdays) > 0){
            Displaystr = Displaystr +' of PTO'; 
        }
        
        if( ((ptoweekcount + RemainPTOdays) > 0) && (( unpaidweekcnt + intDaysRemain ) > 0) ){
            Displaystr = Displaystr +' and ';
        }
        // **unpaid weeks & days count**
        
        if(unpaidweekcnt > 0){
            Displaystr = Displaystr +'<b>'+unpaidweekcnt+'</b> week(s) ';
        }
        if(intDaysRemain > 0){
            Displaystr = Displaystr + '<b>'+intDaysRemain+'</b> day(s)';
        }
        if(( unpaidweekcnt + intDaysRemain ) > 0){
            Displaystr = Displaystr +' of unpaid parental leave. '; 
        }
        
        //******Disability count*******
        if(ptoweekcnt > 0){
            Displaystr2 = '<b>'+ptoweekcnt+'</b> week(s) ';
        } else {Displaystr2 = '';}
        if(RemainPTO > 0){
            Displaystr2 = Displaystr2 + '<b>'+RemainPTO+'</b> day(s)';
        }
        if((ptoweekcnt+RemainPTO) > 0){
            Displaystr2 = Displaystr2 +' of PTO'; 
        }
        
        if( ((ptoweekcnt+RemainPTO) > 0) && (( unpaidweekcnt + intDaysRemain ) > 0) ){
            Displaystr2 = Displaystr2 +' and ';
        }
        
        // ****unpaid weeks & days count****
        if(unpaidweekcnt > 0){
            Displaystr2 = Displaystr2 +'<b>'+unpaidweekcnt+'</b> week(s) ';        //     
        }
        if(intDaysRemain > 0){
            Displaystr2 = Displaystr2 + '<b>'+intDaysRemain+'</b> day(s)';
        }
        if(( unpaidweekcnt + intDaysRemain ) > 0){
            Displaystr2 = Displaystr2 +' of unpaid parental leave. '; 
        }
        
        //*******************************************************************
        pl.WCT_Paid_Parental_Leave_Weeks__c = paidweekcnt;
        pl.WCT_Employee__c = loggedInContact.id;
        pl.WCT_Leave_Category__c = listLeave[0].WCT_Leave_Category__c;
        pl.WCT_Parental_Event__c = listLeave[0].WCT_Parental_Event__c;
        pl.WCT_Leave_Record_Number__c = ParentLeaveRecordName;
        
        system.debug('paidweekcnt*****************'+paidweekcnt);
        system.debug('ptoweekcnt*****************'+ptoweekcnt);
        system.debug('unpaidweekcnt*******************'+unpaidweekcnt);
        
    }
    // save method
    public pageReference saveleaveRecord() {
        insert pl;
       
        //PageReference ThankyouPage = new PageReference('/apex/Global_Thank_You?id='+pl.id+'&page=Adoption');
        PageReference ThankyouPage = new PageReference('/apex/GBL_Page_Notification?id='+pl.id+'&key=PLThankyou');
        ThankyouPage.setRedirect(true);
        return ThankyouPage;
    }
}