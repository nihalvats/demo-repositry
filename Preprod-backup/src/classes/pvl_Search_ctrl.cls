global class pvl_Search_ctrl {
      
      
        public static integer limitToDisplay=10000;
        public List<PVL_Database__c> pvld{get;set;}   
        public List<PVL_Database__c> topList{get{
            List<PVL_Database__c> temp;
            if(pvld!=null )
            {
                if(pvld.size()>limitToDisplay)
                {
                    for(integer i=0; i<limitToDisplay;i++)
                    {
                        temp.add(pvld[i]);
                    }
                }
                else
                {
                    temp=pvld;
                }
            }
            return temp;
        } set;}   
        public string cln{get;set;}
        public string pvlstate {get;set;}
        public boolean displayresults{get;set;}
        public boolean displayalert{get;set;}
        public string ppe{get;set;}
        public String selectedValues{get; set;}
        public String queryclocs='';
        public Project_H1B__c tempProject{get; set;}
        public PVL_Database__c tempDatabase{get; set;}
        public Client_Location__c tempClientLocation{get; set;}
        public string pskills{get; set;}
        
        public string starsearch{get;set;}
        
    public pvl_Search_ctrl (ApexPages.StandardController controller) 
        {
            displayresults=false;
            pvld= new List<PVL_Database__c>();
          
        }
        public pagereference ClearControls()
        {
           PageReference pageRef = new PageReference('/apex/pvlsearch');
           pageRef.setRedirect(true);
           return pageRef;            
           //  return new PageReference('/apex/pvlsearch' );
        }
        public pvl_Search_ctrl()
        {

            displayresults=false;
            pvld= new List<PVL_Database__c>();
            tempProject= new Project_H1B__c();
            tempDatabase= new PVL_Database__c();
            tempClientLocation= new Client_Location__c();
            tempDatabase.PVL_Date_PVL_Received__c=null;
            tempDatabase.PVL_Expiration_Date__c=null;
            displayresults=false;
           
            topList= new List<PVL_Database__c>();

        }
        Public void search(){
            
             pvld= new List<PVL_Database__c>();
             
             boolean isclientLocationSearch=false;
           
          
            try{
                String queryWhereClause = '';
                
                String fDateQuery = '';
                String tDateQuery = '';
                Set<String> clids = new Set<String>();
                Set<String> cps = new Set<String>();  
                String queryWhere=''; 
                String idStr = '';  
                queryclocs='';
                
                displayalert=true;
                if(tempDatabase.PVL_Expiration_Date__c!= null)
                {
                    string dateString=String.valueOf(tempDatabase.PVL_Expiration_Date__c);
                    dateString=dateString.replace('00:00:00', '');
                    fDateQuery  = ' AND PVL_Expiration_Date__c >= '+dateString+'';
                }
                else
                {
                    fDateQuery = '';
                }
                
                if(tempDatabase.PVL_Date_PVL_Received__c!= null)
                {
                    string dateString=String.valueOf(tempDatabase.PVL_Date_PVL_Received__c);
                    dateString=dateString.replace('00:00:00', '');
                    tDateQuery = ' AND PVL_Expiration_Date__c <= '+dateString;
                }
                else
                {
                     tDateQuery = '';
                }
                
                if(!string.isEmpty(tempClientLocation.H1B_City__c) || !string.isEmpty(pvlstate) || !string.isEmpty(tempClientLocation.Zip_Code__c))
                {
                    
                    isclientLocationSearch=true;
                                    
                    string tempQuery='select Project_Name__c from Client_Location__c where id <> null ';
                    tempQuery+= string.isEmpty(tempClientLocation.H1B_City__c)? '':' AND H1B_City__c like \'%'+tempClientLocation.H1B_City__c+'%\'';
                    
                    pvlstate=pvlstate.replace(',','\',\'');
                    
                    tempQuery+= string.isEmpty(pvlstate)? '':' AND H1B_State__c in (\''+pvlstate+'\')';
                    tempQuery+= string.isEmpty(tempClientLocation.Zip_Code__c)? '':' AND Zip_Code__c = \''+tempClientLocation.Zip_Code__c+'\'';
                    System.debug('tempQuery  '+tempQuery);
                    List<Client_Location__c> locations= Database.query(tempQuery);
                    System.debug('locations '+locations);
                    for(Client_Location__c c :locations)
                    {
                        clids.add(c.Project_Name__c);
                        system.debug('progname'+c.project_name__c);
                    }
                    
                    for(Project_H1B__c p:[select Name from Project_H1B__c where id in :clids])
                    {
                        cps.add(p.id);
                    }
                   
                    
                    for(String s:cps) 
                    {
                        idStr += (idStr == ''?'':',')+'\''+s+'\'';
                    }
                    queryclocs = 'AND Project__r.id in(' + idStr + ')';
                }
             if(idStr!=''||isclientLocationSearch==false)   
            {
                system.debug('Client Names Selected: ' + cln);
                system.debug('Selected Industry values :' + tempProject.Industry__c);
                if(!string.isEmpty(cln))
                {
                   // queryWhere+=' AND Project__r.H1B_Client_Company__c in \''+cln+'\'';
                   cln=cln.replace(',','\',\'');
                    queryWhere+=' AND Project__r.H1B_Client_Company__c in (\''+ cln + '\')';
                }
               
                if(!string.isEmpty(ppe))
                {
                    queryWhere+=' AND Project__r.PL_Engagement_Prtnr_Email__c like \'%'+ppe+'%\'';
                }
                
                if(tempProject.USI_Resources__c!=null && tempProject.USI_Resources__c!='')
                {
                    queryWhere+=' AND Project__r.USI_Resources__c = \''+tempProject.USI_Resources__c+'\'';
                }
                
                if(tempProject.USI_Landed_Resources__c!=null && tempProject.USI_Landed_Resources__c!='')
                {
                    queryWhere+=' AND Project__r.USI_Landed_Resources__c = \''+tempProject.USI_Landed_Resources__c+'\'';
                }
                
                if(tempProject.H1B_Project_WBS__c!=null && tempProject.H1B_Project_WBS__c!='')
                {
                    queryWhere+=' AND Project__r.H1B_Project_WBS__c = \''+tempProject.H1B_Project_WBS__c+'\'';
                }
              
              
                
                system.debug('************************************'+queryWhere); 
                
                
                string query='Select Name,Project__r.PVL_Project_Name__c, Project__r.H1B_Client_Company__r.Company_Name__c, Project__r.PVL_Project_Service_Area__c,Project__r.PVL_Project_Service_Line__c,Project__r.PVL_Project_Lead_Engmt_Partner_Name__c,Project__r.PL_Engagement_Prtnr_Email__c,Project__r.Project_LEP_Service_Area__c,Project__r.H1B_Project_LEP_Service_Line__c,Project__r.H1B_Project_WBS__c,Project__r.Project_Contact_1__r.Name,Project__r.Project_Contact_2__r.Name,Project__r.Project_Contact_3__r.Name,Project__r.USI_Resources__c,Project__r.USI_Landed_Resources__c,Project__r.Vendor__c,Project__r.Industry__c,Project__r.Capability__c,Project__r.Sub_Capability__c,Project__r.PVL_Module__c,Project__r.H1B_Project_Skills__c,PVL_Expiration_Date__c,PVL_Date_PVL_Received__c From PVL_Database__c WHERE  active__c=true '+ queryWhere + fDateQuery + tDateQuery + queryclocs  ;
                system.debug('#################################### query '+query);
                
                system.debug('sqlqery+'+query);
                List<PVL_Database__c> tempList=database.query(query);
                
                system.debug('*display_size*'+tempList);
                

                /*Check the values from the Pvld Size */
                
                if(tempProject.PVL_Project_Service_Area__c!='' || tempProject.PVL_Project_Service_Line__c!='' || tempProject.Industry__c!='' || tempProject.Vendor__c!='' || tempProject.Capability__c!='' ||tempProject.H1B_Project_Skills__c!='' || tempProject.Sub_Capability__c!='' || tempProject.PVL_Module__c!='' )
                {
                      for(PVL_Database__c database:tempList)
                        {
                            boolean isValid=true;
                            /*if(cln!='')
                            {
                                
                                isValid= compareString(cln, database.Project__r.H1B_Client_Company__c)==false?false:isValid;
                            }*/
                            
                            if(tempProject.PVL_Project_Service_Area__c!='')
                            {
                                
                                isValid= compareString(tempProject.PVL_Project_Service_Area__c, database.Project__r.PVL_Project_Service_Area__c)==false?false:isValid;
                            }
                            if(tempProject.PVL_Project_Service_Line__c!='')
                            {
                              
                                isValid= compareString(tempProject.PVL_Project_Service_Line__c, database.Project__r.PVL_Project_Service_Line__c)==false?false:isValid;
                            }
                            if(tempProject.Industry__c!='')
                            {
                                
                                isValid= compareString(tempProject.Industry__c, database.Project__r.Industry__c)==false?false:isValid;
                            }
                            if(tempProject.Vendor__c!='')
                            {
                                
                                isValid= compareString(tempProject.Vendor__c, database.Project__r.Vendor__c)==false?false:isValid;
                            }
                           
                           if(tempProject.Capability__c!='')
                            {
                                
                                isValid= compareString(tempProject.Capability__c, database.Project__r.Capability__c)==false?false:isValid;
                            }
                            
                            if(tempProject.H1B_Project_Skills__c!='')
                            {
                            
                            
                                isValid= compareString(tempProject.H1B_Project_Skills__c, database.Project__r.H1B_Project_Skills__c)==false?false:isValid;
                            }
                            
                            if(tempProject.Sub_Capability__c!='')
                            {
                            

                                isValid= compareString(tempProject.Sub_Capability__c, database.Project__r.Sub_Capability__c)==false?false:isValid;
                            }
                            
                            if(tempProject.PVL_Module__c!='')
                            {
                                
                                
                                isValid= compareString(tempProject.PVL_Module__c, database.Project__r.PVL_Module__c)==false?false:isValid;
                            }
                           
                                                    
                            if(isValid)
                                pvld.add(database);
                        }
                   
                    
                }
                else
                {
                    pvld.addAll(tempList);
                                    
                }
                
                
                }
                system.debug('*pvld*'+pvld);
                if(pvld.size()>0)
                {
                    displayresults=true;
                    displayalert=false;
                    
                }
                else
                {
                    displayresults=true;
                    displayalert=true;
                }
                system.debug('*displayresults*'+displayresults);
                //clearAll();
            }
            catch(System.SearchException ex){
                apexpages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Enter keywords.'));
            }
        }
        
        public boolean compareString(string items, string mainValue)
        {
            boolean isexist=true;
            
            items=items.tolowercase();
            mainValue=mainValue!=null?mainValue.tolowercase():mainValue;
            
            List<String> allItems=items.split(',');
            for(String item : allItems)
                {
                    if(mainValue!=null?(!mainValue.contains(item)):true)
                    {
                        isexist=false;
                    }
                }
            return isexist;
        
            
        }
        
        
        
        public void printAll()
        {
                
            System.debug('Client Name  '+ cln);
            System.debug('Partner Email Id  '+ ppe);
            System.debug('H1B_Capability__c '+ tempProject.Capability__c);
            System.debug('PVL_Vendor__c '+ tempProject.Vendor__c);
            
            System.debug('tempDatabase.PVL_Expiration_Date__c '+ tempDatabase.PVL_Expiration_Date__c);
            System.debug('tempDatabase.PVL_Date_PVL_Received__c '+ tempDatabase.PVL_Date_PVL_Received__c);
            System.debug('tempProject.USI_Resources__c '+ tempProject.USI_Resources__c);
            System.debug(' tempProject.USI_Landed_Resources__ '+  tempProject.USI_Landed_Resources__c);     
      
           // System.debug(' pskills '+ pskills);             
                       
            
            
        }
        
        
        
        public PageReference export()
        {
            PageReference pr= Page.PVL_Search_Export;
            return pr;
        }
        
       /* public void clearAll()
        {
            cln='';
            ssa='';
            ssl='';
            ppe='';
            cpy='';
            ven='';
            cln='';
            pskills='';
            city='';
            state='';
            zip='';
            tempDatabase.PVL_Expiration_Date__c=null;
            tempDatabase.PVL_Date_PVL_Received__c=null;
            tempProject.USI_Landed_Resources__c='';
            tempProject.USI_Resources__c='';
            
            
        }
        */
        
        
         @RemoteAction
        public static  List<H1B_Client_Company__c> getExistingCompanies(String fieldName)
        {
            List<H1B_Client_Company__c> listItems;
            
            listItems= [Select Id, Company_Name__c from H1B_Client_Company__c ORDER BY Company_Name__c ASC  limit 50000];
            return listItems;
         }
         
          
         @RemoteAction
        global static  List<WCT_List_Of_Names__c> getItemsList(String fieldName)
        {
            List<WCT_List_Of_Names__c> listItems;
            
            listItems= [Select Id, Name from WCT_List_Of_Names__c where RecordTypeId =:Label.MultiSelectPicklist and MultiSelectIdentifier__c=:fieldName  ORDER BY Name ASC ];
            return listItems;
            
        }
        
        
        
        
    }