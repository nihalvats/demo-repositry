/**  
    * Class Name  : WCT_Mobility_TriggerHandler 
    * Description : This Trigger Handler class used for Mobility Object
*/


public class WCT_Mobility_TriggerHandler{
    public WCT_Mobility_TriggerHandler(){}
 
    public static void updateIsStatusChanged(Map<Id,WCT_Mobility__c> mapOldMobility, List<WCT_Mobility__c > lstNewMobility){
        List<WCT_Mobility__c> lstMobility = [select id,WCT_Mobility_Status__c,WCT_Is_Mobility_Status_Changed__c,(select id, subject, status from Tasks where status<>'Completed')
                                                  from WCT_Mobility__c where id in : mapOldMobility.keySet()];
        Map<Id,WCT_Mobility__c> mapIdMob = new Map<Id,WCT_Mobility__c> (lstMobility);
        for(WCT_Mobility__c tmpMob : lstNewMobility){
            if(mapIdMob.get(tmpMob.id)!=null 
             && tmpMob.WCT_Mobility_Status__c!=mapIdMob.get(tmpMob.id).WCT_Mobility_Status__c 
             && mapIdMob.get(tmpMob.id).Tasks !=null && mapIdMob.get(tmpMob.id).Tasks.size()>0 ){
                tmpMob.WCT_Is_Mobility_Status_Changed__c=true;
            }    
        } 
    } 
    
    public Static void updateUSRcCode(list<WCT_Mobility__c> list_new_mob)
    {
        set<String> set_USIRCcode= new  set<String>();
        map<string, String> map_Rc_Code= new map<String,string>();
        for(WCT_Mobility__c lp_mob:list_new_mob)
        {
            if(lp_mob.WCT_USI_RCCode__c <> null){
                set_USIRCcode.add(lp_mob.WCT_USI_RCCode__c);
            }
        }
        if(!set_USIRCcode.isEmpty()){
            list<WCT_List_Of_Names__c> list_LON= [select US_RC_code__c, USI_RC_code__c from WCT_List_Of_Names__c where USI_RC_code__c IN:set_USIRCcode];
            for(WCT_List_Of_Names__c lp_loN: list_LON)
            {
                 
                map_Rc_Code.put(lp_loN.USI_RC_code__c, lp_loN.US_RC_code__c);
            }
        }
        
        for(WCT_Mobility__c lp_MobTowork:list_new_mob)
        {
            if(lp_MobTowork.WCT_USI_RCCode__c!= null){
                if(map_Rc_Code.get(lp_MobTowork.WCT_USI_RCCode__c)!=null)
                {
                   lp_MobTowork.WCT_US_RCCode__c= map_Rc_Code.get(lp_MobTowork.WCT_USI_RCCode__c);
                }
                else
                {  
                    if((lp_MobTowork.WCT_US_RCCode__c!='No RC Mapping available.')&&(lp_MobTowork.WCT_US_RCCode__c!=null)){ 
                    }else
                    {
                         lp_MobTowork.WCT_US_RCCode__c='No RC Mapping available.'; 
                    }
                }
            }
        }
    }   
    
    static Id taskMobilityRecordTypeId=Schema.SObjectType.Task.getRecordTypeInfosByName().get('Mobility').getRecordTypeId();
    public static Boolean isCreated = false;
    
    public Static void generateTask(list<WCT_Mobility__c> list_new_mob)
    {
       
        List<Task> lstMobTask = new List<Task>();
        List<Id> lstMob = new List<Id>();
        List<Id> lstMob2 = new List<Id>();
        List<Id> lstMob3 = new List<Id>();
        List<Id> lstMob4 = new List<Id>();
        for(WCT_Mobility__c mob : list_new_mob)
        {
        
              if(mob.WCT_Insurance_Task_Status__c == true && mob.WCT_Mobility_Status__c == 'Travel Extended' && isCreated == false)
            {
          
                Task t = new Task();
                t.Subject = 'Submit Travel Insurance form';
                t.Status = 'Not Started';
                t.ActivityDate = system.today().addDays(5);
                t.Task_Type__c = 'Travel Extended';
                t.RecordTypeId = taskMobilityRecordTypeId;
                t.WCT_ToD_Task_Reference__c = '0053';
                t.WCT_Task_Reference_Table_ID__c = System.Label.TRT_Id_Submit_Travel_Insurance_Form;
                t.WCT_Is_Visible_in_TOD__c = true;
                t.WCT_IsAutoCreate__c = true;
                t.WCT_Auto_Close__c = true;
                t.OwnerId = System.Label.GMI_User;
                t.WhatId = mob.Id;
                t.WhoId = mob.WCT_Mobility_Employee__c;
                t.WCT_Business_Owner__c = mob.WCT_Employee_Email_Address__c;
                t.WCT_Encrypt_Email_Key__c = CryptoHelper.encrypt(mob.WCT_Employee_Email_Address__c);
                t.Priority = 'Normal';
                lstMobTask.add(t);
                isCreated = true;
                lstMob.add(mob.id);
            }
            
            //Added the condition this task only for Employment Visa only dt 04/20/2015
            
            if(mob.WCT_Accompanied_Status__c == true && mob.WCT_DependentVisaTask_Created__c == false && mob.WCT_Mobility_Status__c == 'Onboarding Completed' && isCreated == false && mob.WCT_Mobility_Type__c == 'Employment Visa')
            {
                Task t = new Task();
                t.Subject = 'Apply for dependent Visa';
                t.Status = 'Not Started';
                t.ActivityDate = system.today().addDays(10);
                t.Task_Type__c = 'Initiate Onboarding';
                t.RecordTypeId = taskMobilityRecordTypeId;
                t.WCT_ToD_Task_Reference__c = '0006';
                t.WCT_Task_Reference_Table_ID__c = System.Label.TRT_Id_Apply_for_dependent_Visa;
                t.WCT_Is_Visible_in_TOD__c = true;
                t.WCT_IsAutoCreate__c = true;
                t.WCT_Auto_Close__c = true;
                t.OwnerId = System.Label.GMI_User;
                t.WhatId = mob.Id;
                t.WhoId = mob.WCT_Mobility_Employee__c;
                t.WCT_Business_Owner__c = mob.WCT_Employee_Email_Address__c;
                t.WCT_Encrypt_Email_Key__c = CryptoHelper.encrypt(mob.WCT_Employee_Email_Address__c);
                t.Priority = 'Normal';
                lstMobTask.add(t);
                isCreated = true;
                lstMob2.add(mob.id);
            }
           
            //Generate the task B1 requirements on 05/07/15
            
            if(mob.WCT_OGC_Status__c == false && mob.OGC_Task_status__c== true && isCreated == false && mob.WCT_Mobility_Type__c == 'Business Visa' && mob.WCT_Existing_Business_Visa__c != 'Green Card/ US Citizen')
            {
                Task t = new Task();
                t.Subject = 'Travel Beyond 30 Days';
                t.Status = 'Not Started';
                t.ActivityDate = system.today().addDays(10);
                t.Task_Type__c = 'Travel Extended';
                t.RecordTypeId = taskMobilityRecordTypeId;
                t.WCT_ToD_Task_Reference__c = '0007';
                t.WCT_Task_Reference_Table_ID__c = System.Label.TRT_Id_OGC_Approval_Form;
                t.WCT_Is_Visible_in_TOD__c = true;
                t.WCT_IsAutoCreate__c = true;
                t.WCT_Auto_Close__c = true;
                t.OwnerId = System.Label.GMI_User;
                t.WhatId = mob.Id;
                t.WhoId = mob.WCT_Mobility_Employee__c;
                t.WCT_Business_Owner__c = mob.WCT_Employee_Email_Address__c;
                t.WCT_Encrypt_Email_Key__c = CryptoHelper.encrypt(mob.WCT_Employee_Email_Address__c);
                t.Priority = 'Normal';
                lstMobTask.add(t);
                isCreated = true;
                lstMob3.add(mob.id);
            }
            
             //Generate the task for Track Background Investigation Outcome - for Long Term Assignments 
             
            if(mob.WCT_Mobility_Type__c == 'Employment Visa'&& mob.WCT_Mobility_Status__c=='Onboarding Completed' && mob.Background_Investigation_Long_Term__c== false && mob.WCT_Assignment_Type__c=='Long Term' && isCreated == false)
            {
                Task t = new Task();
                t.Subject = 'Track Background Investigation Outcome - for Long Term Assignments only';
                t.Status = 'Not Started';
                t.ActivityDate = system.today().addDays(5);
                t.RecordTypeId = taskMobilityRecordTypeId;
                t.WCT_ToD_Task_Reference__c = '58';
                t.WCT_Task_Reference_Table_ID__c = System.Label.TRT_ID_Background_Investigation_Task;
                t.WCT_Is_Visible_in_TOD__c = true;
                t.WCT_IsAutoCreate__c = true;
                t.WCT_Auto_Close__c = false;
                t.OwnerId = System.Label.GMI_User;
                t.WhatId = mob.Id;
                t.WhoId = mob.WCT_Mobility_Employee__c;
                t.WCT_Business_Owner__c = mob.WCT_Employee_Email_Address__c;
                t.WCT_Encrypt_Email_Key__c = CryptoHelper.encrypt(mob.WCT_Employee_Email_Address__c);
                t.Priority = 'Normal';
                lstMobTask.add(t);
                isCreated = true;
                lstMob4.add(mob.id);
            }  
          
        }
        if(!lstMobTask.isEmpty()) 
            insert lstMobTask;
        if(!lstMob.isEmpty())
           WCT_Mobility_TriggerHandler.UpDateTravelDateExtensionChkbox(lstMob);
        if(!lstMob3.isEmpty())
           WCT_Mobility_TriggerHandler.UpDateOGCDateExtensionChkbox(lstMob3);   
        if(!lstMob2.isEmpty()) 
            WCT_Mobility_TriggerHandler.UpDateDependentVisaChkbox(lstMob2);
        if(!lstMob4.isEmpty()) 
            WCT_Mobility_TriggerHandler.UpDateBackgroundInvestigationChkbox(lstMob4);        
    }
    
    @future
    public static void UpDateTravelDateExtensionChkbox(List<Id> mobIds){
       
        List<WCT_Mobility__c> mobToUpdateChkbox = new List<WCT_Mobility__c>();
        for(WCT_Mobility__c mob:[SELECT id,WCT_Travel_End_Date_Extended__c FROM WCT_Mobility__c WHERE id IN :mobIds]){
            mob.WCT_Travel_End_Date_Extended__c = false;
            mob.WCT_Insurance_Task_Status__c = false;
            mobToUpdateChkbox.add(mob);
        }
        if(!mobToUpdateChkbox.isEmpty()){
            Database.SaveResult[] lsr = Database.Update(mobToUpdateChkbox,false);
        }
    
    }
   
    @future
    public static void UpDateOGCDateExtensionChkbox(List<Id> mobIds){
       
        List<WCT_Mobility__c> mobToUpdateChkbox = new List<WCT_Mobility__c>();
        for(WCT_Mobility__c mob:[SELECT id,WCT_Travel_End_Date_Extended__c FROM WCT_Mobility__c WHERE id IN :mobIds]){
            
            mob.OGC_Task_status__c= false;
            mobToUpdateChkbox.add(mob);
        }
    
        if(!mobToUpdateChkbox.isEmpty()){
         
            Database.SaveResult[] lsr = Database.Update(mobToUpdateChkbox,false);
        }
    
    }
  
    @future
    public static void UpDateDependentVisaChkbox(List<Id> mobIds){
        List<WCT_Mobility__c> mobToUpdateChkbox = new List<WCT_Mobility__c>();
        for(WCT_Mobility__c mob:[SELECT id,WCT_DependentVisaTask_Created__c FROM WCT_Mobility__c WHERE id IN :mobIds]){
            mob.WCT_DependentVisaTask_Created__c = true;
            mobToUpdateChkbox.add(mob);
        }
        if(!mobToUpdateChkbox.isEmpty()){
            Database.SaveResult[] lsr = Database.Update(mobToUpdateChkbox,false);
        }
    
    }
     @future
    public static void UpDateBackgroundInvestigationChkbox(List<Id> mobIds){
        List<WCT_Mobility__c> mobToUpdateChkbox = new List<WCT_Mobility__c>();
        for(WCT_Mobility__c mob:[SELECT id,Background_Investigation_Long_Term__c FROM WCT_Mobility__c WHERE id IN :mobIds]){
            mob.Background_Investigation_Long_Term__c= true;
            mobToUpdateChkbox.add(mob);
        }
        if(!mobToUpdateChkbox.isEmpty()){
            Database.SaveResult[] lsr = Database.Update(mobToUpdateChkbox,false);
        }
    
    }
    
    
   /* Update Initiate Onboarding/Onboarding Completed Date/Time Fields for Employment Visa */
     
    public static void updateOnboardingDateTime(List <WCT_Mobility__c> listStatus) {
    
         for (WCT_Mobility__c updateMobilityLoop :listStatus) {
             if(updateMobilityLoop.UpdateTimestamp__c == true) {
                  if (updateMobilityLoop.WCT_Mobility_Status__c == 'Initiate Onboarding' && updateMobilityLoop.WCT_Mobility_Type__c == 'Employment Visa')
                       updateMobilityLoop.Initiate_Onboarding_Date_Time__c = System.now();
                  else if (updateMobilityLoop.WCT_Mobility_Status__c == 'Onboarding Completed' && updateMobilityLoop.WCT_Mobility_Type__c == 'Employment Visa') 
                       updateMobilityLoop.Onboarding_Completed_Date_Time__c = System.now();
                  else if (updateMobilityLoop.WCT_Mobility_Status__c == 'Departure Processed' && updateMobilityLoop.WCT_Mobility_Type__c == 'Employment Visa') 
                       updateMobilityLoop.WCT_Retro_Departure__c = System.now();
        }
         updateMobilityLoop.UpdateTimestamp__c = false;
        }
    }

}