/********************************************************************************************************************************
Apex class         : <WCT_H1BCAP_PractitionerController>
Description        : <Controller which allows to Update H1BCAP Record>
Type               :  Controller
Test Class         : <WCT_H1BCAP_PractitionerController_Test>
Author             : Dinesh kumar Rangaswamy
VF Page name       : WCT_H1BCAP_Professional_Link

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 12/17/2015          87%                          --                            License Cleanup Project
************************************************************************************************************************************/   


public with sharing class WCT_H1BCAP_PractitionerController extends SitesTodHeaderController{

    // Declare Public Variables
    public WCT_H1BCAP__c practitionerRecord {get;set;}
    public boolean invalidEmployee{get; set;}
    public String recordId{get;set;}
    public Datetime dateFormat {get;set;} 
    public string d{get;set;}
    
    // Declare Error Message Varaiables
    public boolean pageError {get; set;}
    public boolean pageErrorFlag {get; set;}
    public String pageErrorMessage {get; set;}

   
   
    /********************************************************************************************
    *Method Name         : <WCT_H1BCAP_PractitionerController()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <WCT_H1BCAP_PractitionerController() Used to Display the record>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/ 
   
   // Constructor  
    public WCT_H1BCAP_PractitionerController()
    {
           recordId = ApexPages.currentPage().getParameters().get('id');
            if(recordId == '' || recordId == null) {
             
            pageErrorMessage = 'Not valid';
            pageError = true;
            pageErrorFlag =false;
           
             
             }
             else {
            
            //Query for practitioner invidual record from the url  
             practitionerRecord= [SELECT Id,WCT_H1BCAP_After7thday__c,WCT_Custom_Modified_Date__c,WCT_H1BCAP_Email_ID__c,WCT_H1BCAP_Willingness_Options__c,WCT_H1BCAP_Willingness_Status__c,WCT_H1BCAP_Status__c,WCT_H1BCAP_Business_Days__c,WCT_H1BCAP_Willing_to_Travel__c,WCT_H1BCAP_Practitioner_Comments__c
                                                  FROM WCT_H1BCAP__c WHERE  id = :recordId ];
             
             dateFormat = practitionerRecord.WCT_H1BCAP_After7thday__c;
             d = dateFormat.format('EEE, MMM d, YYYY');
            
         // Displaying error if the practitioner record created date is greater than 7 business days 
           if(practitionerRecord.WCT_H1BCAP_Business_Days__c > 6) {
            pageErrorMessage = 'Your are not eligible to fill this form being time exceeded';
            pageError = true;
            pageErrorFlag =false;
             
             }
             else {
         
                pageError = false;
                pageErrorFlag =true;
                }
           } 
            
    }
    
   
    /********************************************************************************************
    *Method Name         : <UpdateRecord()>
    *Return Type         : <Page Reference>
    *Param’s             : 
    *Description         : <UpdateRecord() Used to Updating H1BCAP Record>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/ 
   
   
   //Update method for the existing record
    public pageReference UpdateRecord()
    {
    try {
        
        /* Validation Rules on Page */
        if(practitionerRecord.WCT_H1BCAP_Willingness_Options__c == 'Willing to Travel' && practitionerRecord.WCT_H1BCAP_Willing_to_Travel__c == null ) {
            pageErrorMessage = 'Please fill whether you are travel less than or greater than 4 months';
            pageError = true;
            return null;
        
        } 
        
        system.debug('loggedInContactemail*****'+loggedInContact.email);
        system.debug('Prac Email*****'+practitionerRecord.WCT_H1BCAP_Email_ID__c);
       // Suppose UnAuthorised person to submit forms
        if(practitionerRecord.WCT_H1BCAP_Email_ID__c != loggedInContact.email){
       
            pageErrorMessage = 'You are not authorised person to submit forms';
            pageError = true;
            return null;
        
        
        }
      
        else {
     
        // Suppose UnAuthorised person to submit forms
        WCT_H1BCAP__c updatepractitionerRecord = [SELECT Id,WCT_Custom_Modified_Date__c,WCT_H1BCAP_Willingness_Options__c,WCT_H1BCAP_Willingness_Status__c,WCT_H1BCAP_Status__c,WCT_H1BCAP_Business_Days__c,WCT_H1BCAP_Willing_to_Travel__c,WCT_H1BCAP_Practitioner_Comments__c FROM WCT_H1BCAP__c WHERE  id = :recordId AND WCT_H1BCAP_Business_Days__c < 6];
        
        updatepractitionerRecord.WCT_H1BCAP_Willing_to_Travel__c = practitionerRecord.WCT_H1BCAP_Willing_to_Travel__c;
        updatepractitionerRecord.WCT_H1BCAP_Willingness_Options__c = practitionerRecord.WCT_H1BCAP_Willingness_Options__c;
        updatepractitionerRecord.WCT_H1BCAP_Status__c= practitionerRecord.WCT_H1BCAP_Willingness_Options__c;   
        updatepractitionerRecord.WCT_H1BCAP_Status__c= practitionerRecord.WCT_H1BCAP_Willingness_Options__c;   
        updatepractitionerRecord.WCT_Custom_Modified_Date__c=Date.today();
        updatepractitionerRecord.WCT_H1BCAP_Practitioner_Comments__c = practitionerRecord.WCT_H1BCAP_Practitioner_Comments__c;
        updatepractitionerRecord.WCT_H1BCAP_Willingness_Status__c=true;
        update updatepractitionerRecord;
       
    
        
    }
   
} catch(Exception e) {

     Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_H1BCAP_PractitionerController', 'H1b_Prof_link', e.getMessage());
            
     Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=H1B_PROF_EXP&expCode='+errLog.Name);
     pg.setRedirect(true);
      return pg;
    
}
  // Redirect to Global Thank you Page
    PageReference newPage = new PageReference('/apex/GBL_Page_Notification?key=H1B_PROF_THNX');  
    newPage.setRedirect(true);
    return newPage;
   
      
}
}