@isTest(seeAllData = true)
public class Wct_eEvents_AR_Eventcheckin_Test {
public static datetime siteenddate {get;set;}
public static datetime sitestartdate {get;set;}
public static boolean Thankyoumess { get; set; }
public static event__c event { get; set; }
public static string lastsearchName{get;set;}
Public static boolean mainpage {get;set;}
Public static boolean nomatches {get;set;}
Public static boolean dateerror {get;set;}
Public static boolean Resultspage {get;set;}
public static String eid = ApexPages.currentPage().getParameters().get('eid');
public static String searchlastname { get; set; }
public static  Recordtype record1 = [SELECT Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'event__c' limit 1];
public static  Recordtype record2 = [SELECT Id,Name,SobjectType FROM RecordType WHERE SobjectType = 'Event_Registration_Attendance__c' and name='Alumni Relation'];
  
public static Wct_eEvents_AR_Eventcheckin.EventRegistration ERList{get;set;}
List<Event_Registration_Attendance__c> selectedERs = new List<Event_Registration_Attendance__c>();
 public static Event_Registration_Attendance__c k=new Event_Registration_Attendance__c();   
    
   public static  event__c createEvent(){
      Profile p = [select id from profile where name='System Administrator'];
       user us = new user();
       us.FirstName='test';
       us.LastName='lasttest';
       us.Alias='test123';
       us.Email='test@email.com';
       us.Username='tes12231132231321312312t@email.com';
       us.CommunityNickname='hellotest';
       us.ProfileId=p.id;
       
       us.Role_Account__c=true;
       us.EmailEncodingKey='UTF-8';
       us.LanguageLocaleKey='en_US'; 
       us.LocaleSidKey='en_US';
       
       us.TimeZoneSidKey='America/Los_Angeles';
       
       insert us;
       
        event__c event1=new event__c(RecordType=record1,Name='AReventNew',Event_Type__c='consulting',Region__c='East',City_Market__c='Boston',FSS__c='consulting',Alumni_Relation_Manager__c=us.id,Level_of_AR_Involvement__c='Limited Support',Event_New_or_Repeat__c='New',Year_FY__c='FY16',Season__c='Winter',Time_Zone__c='Central',NMS_Support__c='Full',AR_Start_Date_Time_Text__c='07/22/2016 12:04:12',AR_End_Date_Time_Text__c='07/23/2016 12:04:12',AR_Add_to_MIX__c='yes',AR_Add_to_Deloitte_com__c='No',CPE_Offered__c='No',NIR_Venue__c='Deloitte',NIR_Venue_Address__c='Hyderabad',Venue_City_AR__c='Hyderabad', Venue_State_AR__c='TN',Venue_Zip_AR__c='500038');

       return event1;
    }
   
static testMethod void myUnitTest() {
  
    Contact con = new Contact();
     con.LastName = 'abc';
     con.FirstName = 'xyz';
     con.Email = 'abc@invalid.com';
     con.Teams__c = 'Campus';
     con.WCT_Candidate_School__c =  'School of Arts';
     con.Degree_program__c = 'BA';
     insert con;            
 test.startTest();
     event=createEvent();
 insert event;
  
   
   k.RecordTypeId=record2.id;
    k.Contact__c=con.id;
    k.Event__c=event.id;
    insert k;
    PageReference pageRef = Page.Wct_eEvents_AR_Eventcheckin;      
       Test.setCurrentPage(pageRef);
       ApexPages.currentPage().getParameters().put('eid', event.id);
    
   Wct_eEvents_AR_Eventcheckin FEV=new Wct_eEvents_AR_Eventcheckin();
  Wct_eEvents_AR_Eventcheckin.EventRegistration FEV1=new Wct_eEvents_AR_Eventcheckin.EventRegistration( k);
    
FEV.Thankyoumess=false;
   FEV.nomatches=false;
  FEV.mainpage = true;
   FEV.Resultspage= false;
   FEV.dateerror= false;
  FEV.backb();
    FEV.ers();
    FEV.getERs();
    FEV.getSelected();
    FEV.GetSelectedErs();
    FEV.getDocumentImageUrl();
    FEV.backtocheckin();
    test.stopTest();
    
}
}