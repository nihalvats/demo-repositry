@isTest
private class WCT_H1BCAPTriggerHandler_Test
{
     static testmethod void test1()
    {
        //Test.starttest();
        
        string orgmail = Label.WCT_H1BCAP_Mailbox;
        OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
       Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'WCT_Action_Required_H1BCAP_Nomination_Professional' AND IsActive = true];
        
        Recordtype r = [select id,Name from Recordtype where DeveloperName  = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createContact();
        con.Email='test@deloitte.com';
        con.Recordtypeid = r.id;
        insert con;
        
        WCT_H1BCAP__c h1 = new WCT_H1BCAP__c();
        
        h1.WCT_H1BCAP_Practitioner_Name__c = con.id;
        h1.WCT_H1BCAP_Email_ID__c = 'svalluru@gmail.com';
        h1.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
        h1.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'abc@yahoo.com';
        h1.recalculateFormulas(); 
        system.debug('Recordid_value ********'+h1.WCT_Contact_Record_ID__c);
        insert h1;
  system.debug('Recordid_value1 ********'+h1.WCT_Contact_Record_ID__c);
    list<String> ToCcAddress =  new List<String>();  
   
     List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();   
         Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
         
          ToCcAddress.add(h1.WCT_H1BCAP_Resource_Manager_Email_ID__c);
          singleMail.setTargetObjectId(h1.WCT_Contact_Record_ID__c);
          singleMail.setSaveAsActivity(false);
          singleMail.setTemplateId(et.Id);
          singleMail.setWhatid(h1.id);
          singleMail.setCcAddresses(ToCcAddress);
          singleMail.setOrgWideEmailAddressId(owe.id);
    
          emails.add(singleMail);
        Test.starttest();  
           Messaging.sendEmail(emails);
     Test.stoptest();
     
  }
  
 static testmethod void MissingdetailsSMTest()
    { 
       
       Profile p1 = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_RM']; 
       User platuser1=WCT_UtilTestDataCreation.createUser( 'siva88',p1.id,' svallurutest11@deloitte.com.preprod','svallueru@deloitte.com');
       insert platuser1;
        
         
     string orgmail = Label.WCT_H1BCAP_Mailbox;
        OrgWideEmailAddress owe2 =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
       Emailtemplate et2 = [select id, developername , IsActive from Emailtemplate where developername = 'WCT_Action_Required_RM_Updated_the_Missing_Details' AND IsActive = true];
       
   
         Recordtype r2 = [select id,Name from Recordtype where DeveloperName  = 'WCT_Employee'];
        Contact con2=WCT_UtilTestDataCreation.createContact();
        con2.Email='svalluru@deloitte.com';
        con2.Recordtypeid = r2.id;
        insert con2;
       
       list<WCT_H1BCAP__c> lstimm = new list<WCT_H1BCAP__c>();
        WCT_H1BCAP__c h3 = new WCT_H1BCAP__c();
        
        h3.WCT_H1BCAP_Practitioner_Name__c = con2.id;
        h3.WCT_H1BCAP_Email_ID__c = 'svalluru@deloitte.com';
        h3.WCT_H1BCAP_Flagged_Off_by_GMI__c = true;
        h3.WCT_H1BCAP_Noti_sent_to_RM_Misseddetails__c  = true;
        h3.WC_H1BCAP_USI_SLL_email_id__c ='standardplatform@testorg.com';
        h3.WCT_H1BCAP_US_PPD_mail_Id__c = 'test99@deloitte.com';
        h3.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
        h3.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'abc@yahoo.com';
        h3.WCT_H1BCAP_Status__c = 'Review Practitioner Nomination';
        h3.WCT_H1BCAP_RM_Name__c= platuser1.id;
        h3.WCT_H1BCAP_Notes_Given_GM_I__c = 'test';
     
        insert h3;
     
                 
      system.debug('ID********'+h3.id);
     List<Messaging.SingleEmailMessage> emails2 = new List<Messaging.SingleEmailMessage>();   
         
   Messaging.SingleEmailMessage singleMail2 = new Messaging.SingleEmailMessage();
    /*          
      singleMail2 .setTargetObjectId(platuser1.id);
      singleMail2 .setSaveAsActivity(false);
      singleMail2 .setTemplateId(et2.Id);
      singleMail2.setWhatid(h3.id);
      singleMail2 .setOrgWideEmailAddressId(owe2.id);
      emails2.add(singleMail2 );
      
     Test.starttest();  
       Messaging.sendEmail(emails2);
     */
       Test.starttest();  
     WCT_H1BCAP_TriggerHandler contr = new WCT_H1BCAP_TriggerHandler();
    
      system.debug('Wht Happening here*********'); 
       h3.WCT_H1BCAP_Noti_sent_to_RM_Misseddetails__c = false;
      
         
       update h3; 
      Test.stoptest();
     
  }
  
   
   /*
     public static testmethod void test2()
    { 
       
         string orgmail = 'replicastatelabor@gmail.com';
        OrgWideEmailAddress owe1 =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
       Emailtemplate et1 = [select id, developername , IsActive from Emailtemplate where developername = 'Notification_to_SLL_for_Nomination' AND IsActive = true];
       
   
     Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SLL']; 
       User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','svalluru@deloitte.com');
       insert platuser;
        Recordtype r1 = [select id,Name from Recordtype where DeveloperName  = 'WCT_Employee'];
        Contact con1=WCT_UtilTestDataCreation.createContact();
        con1.Email='svalluru@deloitte.com';
        con1.Recordtypeid = r1.id;
        insert con1;
        
         list<WCT_H1BCAP__c>  lst = new list<WCT_H1BCAP__c>();
        
        WCT_H1BCAP__c h2 = new WCT_H1BCAP__c();
        h2.WCT_H1BCAP_Practitioner_Name__c = con1.id;
        h2.WCT_H1BCAP_Email_ID__c = 'svalluru@deloitte.com';
        h2.WCT_H1BCAP_Notification_sent_to_SLL__c = True;
        h2.WC_H1BCAP_USI_SLL_email_id__c ='test99@deloitte.com';
        h2.WCT_H1BCAP_US_PPD_mail_Id__c = 'test9@gmail.com';
        h2.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
        h2.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'abc@yahoo.com';
        h2.WCT_H1BCAP_USI_SLL_Name__c = platuser.id;
        
       h2.recalculateFormulas(); 
        insert h2; 
   
     system.debug('ID of Object****'+h2.id); 
      system.debug('Notifification****'+h2.WCT_H1BCAP_Notification_sent_to_SLL__c ); 
      system.debug('SLL Email****'+h2.WC_H1BCAP_USI_SLL_email_id__c );
      system.debug('PPD Email****'+h2.WCT_H1BCAP_US_PPD_mail_Id__c ); 
       system.debug('SLL ID****'+h2.WCT_H1BCAP_USI_SLL_Name__c );  
   
       list<String> ToCcAddress1 =  new List<String>(); 
     List<Messaging.SingleEmailMessage> emails1 = new List<Messaging.SingleEmailMessage>();   
         Messaging.SingleEmailMessage singleMail1 = new Messaging.SingleEmailMessage();
         
         ToCcAddress1.add(h2.WCT_H1BCAP_Resource_Manager_Email_ID__c);
         ToCcAddress1.add(h2.WCT_H1BCAP_US_PPD_mail_Id__c);
     
      singleMail1.setTargetObjectId(h2.WCT_H1BCAP_USI_SLL_Name__c);
      singleMail1.setSaveAsActivity(false);
      singleMail1.setTemplateId(et1.Id);
      singleMail1.setWhatid(h2.id);
      singleMail1.setCcAddresses(ToCcAddress1);
      singleMail1.setOrgWideEmailAddressId(owe1.id);
    
      emails1.add(singleMail1);
       
       Messaging.sendEmail(emails1);   
        
     Test.starttest(); 
      system.debug('Wht Happening here*********');   
     
        
         h2.WCT_H1BCAP_Notification_sent_to_SLL__c = false;
     
       update h2;
      system.debug('Notify*********'+h2.WCT_H1BCAP_Notification_sent_to_SLL__c);   
       Test.stoptest();
       
  
  }
  */
    
      
       }