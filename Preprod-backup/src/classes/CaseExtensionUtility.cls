public abstract  class CaseExtensionUtility {

    //public CaseExtensionUtility(TDR_Learning_Request_CloneBaseClsAtt controller) {
    //    preinit();
    //}


    // public CaseExtensionUtility(TDR_Learning_Request_CloneAttachment controller) {
    //    preinit();
    // }   


    //  public CaseExtensionUtility(TDR_Learning_Request_ctrl_Clone controller) {
    //     preinit();
    //  }
 
    /*
     Apex Class:  CaseExtensionUtility
     Purpose: This class is a base class contains common/reused apex methods which can be consumed by other classes.
     Created On:  30th March,2015.
     Created by:  Balu Devarapu.(Deloitte India)
    */
    // public static Document doc =new Document();
    //public static List<String> docIdList = new List<string>();
    //public static List<AttachmentsWrapper> UploadedDocumentList=new List<AttachmentsWrapper>(); 
    
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    //public List<AttachmentsWrapper> UploadedDocumentList {get; set; }
    //public static List<AttachmentsWrapper> UploadedDocumentListHoler;
    public static Case Ocase{get;set;}
    public Contact loggedInContact {get; set;}    
    public Boolean invalidEmployee {get; set;}
    public String encryptedEmailString {get; set;}
    /* Consutuctor */    
    public CaseExtensionUtility(){
        preinit();
      
    }   
    
   public void preinit(){
    
      
        doc = new Document();
        //UploadedDocumentList = new List<AttachmentsWrapper>();
        
        
        
        invalidEmployee = true;

        //Get Employee Email Id
        String encryptedEmailString = ApexPages.currentPage().getParameters().get('em');

        //Decrypt and Encrypt only when Flag is updated.
        String empEmail = cryptoHelper.decrypt(encryptedEmailString);

        if( (null != empEmail) && ('' != empEmail) ) {
            List<Contact> listContact = [
                                            SELECT 
                                                Id,
                                                Name,
                                                FirstName,
                                                WCT_Middle_Name__c,
                                                WCT_ExternalEmail__c,
                                                WCT_Relationship__c,
                                                WCT_Personnel_Number__c,
                                                LastName,
                                                Email,
                                                Phone 
                                            FROM
                                                Contact
                                            WHERE
                                                Email = :empEmail
                                            LIMIT
                                                1   
                                          ];
             system.debug('###############' + listContact[0].id);
            if( (null != listContact) && (listContact.size() > 0) ) {
            
                loggedInContact = listContact[0];
                invalidEmployee = false;
            }
            else {
                invalidEmployee = true;
            }
        }
        else {
            invalidEmployee = true;
        }
                     system.debug('#$$$$$$$$$$##' + loggedInContact.id);
    
    
   } 
    
    
    
     /*
     Method:  getRecordTypesByPrefix
     Purpose: This method is used to return list of Record Types based on Prefixing of DeveloperName of RecordType( Ex: TDR).
     Input:   Prefix of DeveloperName of RecordType( Ex: TDR).
     Returns: List of Record Types
    */   
    public static List<RecordType> getRecordTypesByPrefix(string sObjectType,string requestType){
       requestType=requestType.replace('_',''); // replaces underscore if already proided in child class.
       List<RecordType>   lstRecordTypes = new List<RecordType>();
       lstRecordTypes  = [SELECT ID, Name FROM RecordType WHERE sObjectType =:sObjectType and DeveloperName like: requestType+'_%' order by Name];
       return lstRecordTypes;
    
    }
    
    //  ,List<AttachmentsWrapper> UploadedDocumentListHoler 
     /*
    public void uploadRelatedAttachment(){
        try{
        string CaseId='';
        system.debug('-----------uploadRelatedAttachment-----------');
         
        //           system.debug('--Show--UploadedDocumentList----'+UploadedDocumentList);
       system.debug('-----UploadedDocumentList--------'+UploadedDocumentListHoler);
          
         // system.debug('-----UploadedDocumentList--------'+CaseExtensionUtility.UploadedDocumentList);
          
         
       
         
            List<String> selectedDocumentId = new List<String>();
            for(AttachmentsWrapper aWrapper : UploadedDocumentListHoler) {
                if(aWrapper.isSelected) {
                    selectedDocumentId.add(aWrapper.documentId);
                }
            }
            
            // Select Documents which are Only Active (Selected) 
            List<Document> selectedDocumentList = new List<Document>();
            if(!selectedDocumentId.isEmpty()){
                selectedDocumentList = [
                                           SELECT 
                                               id,
                                               name,
                                               ContentType,
                                               Type,
                                               Body 
                                           FROM 
                                               Document 
                                           WHERE 
                                               id IN :selectedDocumentId
                                        ];
            }
            system.debug('----selectedDocumentList----'+selectedDocumentList);
            // Convert Documents to Attachment 
            List<Attachment> attachmentsToInsertList = new List<Attachment>();
            for(Document docs : selectedDocumentList){
                Attachment a = new Attachment(); 
                a.body = docs.body;
                a.ContentType = docs.ContentType;
                a.Name= docs.Name;
                a.parentid = CaseId; //Case Record Id which is been recently created.
                attachmentsToInsertList.add(a);                
            }
            
            if(!attachmentsToInsertList.isEmpty()){
                insert attachmentsToInsertList;
            }
            
            List<Document> listDocuments = new List<Document>();
            listDocuments = [
                                SELECT
                                    Id
                                FROM
                                    Document
                                WHERE
                                    Id IN :selectedDocumentId
                            ];
            if( (null != listDocuments) && (0 < listDocuments.size()) ) {
                delete listDocuments;
            }
      
            
        }catch(exception ex){
            system.debug('--------Message-------'+ex.getMessage()+'---------at Line #------'+ex.getLineNumber());
        }     
    }
    */
    /*Invoked when Upload Button in VF page is clicked and the IDs are stored in the docIdList*/
    /*All the Files Uploaded are stored in the Documents. Documents does not require parent Id.This method is used to circumvent to upload
      the documents first and then add as Attachment to the Cases (Related List)*/

    /*public  void uploadAttachment(){
        try{
    
                                   
        
        System.debug('----Doc Name--------' + doc.name);
        system.debug('--Show--UploadedDocumentList----'+UploadedDocumentList);
        doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        if(doc.body != null) {
           insert doc;

            docIdList.add(doc.id);
            doc = [SELECT id, name, bodylength FROM Document WHERE id = :doc.id];

            String size = '';
            if(1048576 < doc.BodyLength) {
              // Size greater than 1MB
                size = '' + (doc.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < doc.BodyLength) {
              // Size greater than 1KB
                size = '' + (doc.BodyLength / 1024) + ' KB';            
            }
            else {
                size = '' + doc.BodyLength + ' bytes';
            } 
            UploadedDocumentList.add(new AttachmentsWrapper(true, doc.name, doc.id,size));
            doc = new Document();
           // TDR_Learning_Request_ctrl_Clone.UploadedDocumentListLocal=UploadedDocumentList; 
            UploadedDocumentListHoler=UploadedDocumentList; 
           // TDR_Learning_Request_ctrl.UploadedDocumentListLocalHost=new List<TDR_Learning_Request_ctrl.AttachmentsWrapper>();
          //  TDR_Learning_Request_ctrl.UploadedDocumentListLocalHost=UploadedDocumentList;
          //System.debug('----UploadedDocumentListHoler Local--------' + TDR_Learning_Request_ctrl_Clone.UploadedDocumentListLocal);
        }
         //  return UploadedDocumentList; 
            
         }catch(exception ex){
            system.debug('--------Message-------'+ex.getMessage()+'---------at Line #------'+ex.getLineNumber());
           //  return null;
        } 
    }     */  
    
    //Documents Wrapper
    /*public class AttachmentsWrapper {
        public Boolean isSelected {get;set;}
        public String docName {get;set;}
        public String documentId {get;set;}
        public String size {get; set;}
        
        public AttachmentsWrapper(Boolean selected, String Name, String Id, String size){
            isSelected = selected;
            docName = Name ;
            documentId = Id;
            this.size = size;
        }        
    } */
         
}