public class WCT_Upload_Wagenotices
{
    public integer totalrecords { get; set; }
    public integer totalsuccessrec { get; set; }
    public integer totalunsuccessrec { get; set; }
    public String nameFile { get; set; }
    public String fileName { get; set; }
    public integer size{get; set;}
       
    transient public Blob contentFile { get; set; }
    transient public List<List<String>> fileLines = new List<List<String>>();
    
    transient List<WTPAA_Wage_Notice__c> wageList;
    List<String> rows;
    List<String> failedLines;
    WCT_parseCSV parseCSVInstance = new WCT_parseCSV();  
    
    public boolean b {get{
       if(rows != null && rows.size() > 1)
       {
          return true;
       }
       else 
       {
          return false;
       }
     }
    }  
      
    public Pagereference ReadFile()
    {   
        wageList = new List<WTPAA_Wage_Notice__c>();
        rows = new List<String>();
        fileLines = new List<List<String>>();
                
        totalrecords = 0;
        totalsuccessrec = 0;
        totalunsuccessrec = 0;
        
        try {
            wageList = uploadProcess();   
        } 
        /*Catch(System.StringException stringException)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_String_Exception_Error);
            ApexPages.addMessage(errormsg);    
        }
        Catch(System.ListException listException)
        {    
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Columns_count_not_proper);
            ApexPages.addMessage(errormsg); 
        }*/
        Catch(Exception e)
        {     
            System.debug('e.getCause'+e.getCause());
            System.debug('e.getLinenumber'+e.getLinenumber());
            System.debug('e.getMessage'+e.getMessage());
            
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.Upload_Case_Exception);
            ApexPages.addMessage(errormsg);    
        }  
        Database.SaveResult[] srList ;                  
        if(wageList!=Null) {
        totalrecords = wageList.size(); 
       srList   = Database.insert(wageList, false);
        System.debug('srList'+srList);
        }
        
        if(srList!=Null){
        for(Database.SaveResult sr: srList)
        {
            if(!sr.IsSuccess())
            {    
                for(Database.Error err : sr.getErrors())
                {
                     ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.Error,Label.WCT_Data_Error+err.getMessage());
                     ApexPages.addMessage(errormsg);       
                }
            }
        }           
       } 
        return null; 
    }     
    
    public List<WTPAA_Wage_Notice__c> uploadProcess() {
        
        /*Validtion if no file selected */
        if(contentFile!=null)
        {
            fileName = nameFile;
            nameFile = contentFile.toString();
            System.debug('nameFile'+nameFile);
            filelines = parseCSVInstance.parseCSV(nameFile, true);
            System.debug('nameFile1'+filelines);
            failedLines = nameFile.split('\r\n');
            System.debug('failedLines'+failedLines);
        }
        else 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Please choose a file to upload !'));
            return null;
        }
        
        List<Decimal> personnelNoList = new List<Decimal>();
        Map<String, Id> conIdPersonnelNoMap = new Map<String, Id>();
        
        for(List<String> inputValues:filelines) {
            personnelNoList.add(Decimal.valueOf(inputvalues[0]));
        }

        List<Contact> conList = [SELECT Id, WCT_Personnel_Number__c FROM Contact WHERE WCT_Personnel_Number__c IN :personnelNoList];
        for(Contact c:conList) {
           conIdPersonnelNoMap.put(String.valueOf(c.WCT_Personnel_Number__c), c.Id);
           System.debug('conIdPersonnelNoMap'+conIdPersonnelNoMap);
        }
        
        rows.add(failedLines[0]);
        Integer counter = 1;        
        
        for(List<String> inputvalues:filelines) 
        {       
            totalrecords++;
            if(!conIdPersonnelNoMap.containsKey(inputvalues[0])) {
                totalunsuccessrec++;
                rows.add(failedLines[counter]);
            } else {
                totalsuccessrec++;
                WTPAA_Wage_Notice__c w = new WTPAA_Wage_Notice__c();
                w.WTPAA_Status__c = 'New';   
                w.WTPAA_File_Name__c = fileName; 
                w.WTPAA_Personnel_Number__c = inputvalues[0];
                if(inputvalues.size()>1 && inputValues[1]!=null)
            {
                if(boolean.valueof(inputvalues[1]))
             {
                w.WTPAA_Notice_Type__c = 'At Hire';
             }
            }
                if(inputvalues.size()>2 && inputValues[2]!=null)
            {
                if(boolean.valueof(inputvalues[2]))
                {
                w.WTPAA_Notice_Type__c = 'Current Employee';
                }
            }
                if(inputvalues.size()>3 && inputValues[3]!=null)
            {
                if(boolean.valueof(inputvalues[3]))
                {
                w.WTPAA_Notice_Type__c = 'Annual - Current Date';
                }
            }
                if(inputvalues.size()>4 && inputValues[4]!=null)
            {
                if(boolean.valueof(inputvalues[4]))
                {
                w.WTPAA_Notice_Type__c = 'Change in pay rates(s) or payday';
                }
            }
         /*   else{
                w.WTPAA_Notice_Type__c = '';
            } */
                w.WTPAA_Related_To__c = conIdPersonnelNoMap.get(w.WTPAA_Personnel_Number__c);             
               // w.WTPAA_Notice_Type__c = 'Current Employee';
                if((inputvalues.size()>5 && inputvalues[5]!=null)?inputvalues[5].trim()!='':false){
                    //w.WTPAA_Effective_Date__c=date.parse(inputvalues[5]!=null?inputvalues[5].trim():inputvalues[5]);
                    w.WTPAA_Effective_Date__c=System.today();
                }
                
                w.WTPAA_Company_Name__c = (inputvalues.size()>6 && inputvalues[6]!='')?inputvalues[6]:'';
              
                w.WTPAA_DBA__c = (inputvalues.size()>7 && inputvalues[7]!='')?inputvalues[7]:'';
                w.WTPAA_Employer_Permanent_Address__c = (inputvalues.size()>8 && inputvalues[8]!='')?inputvalues[8]:'';
                w.WTPAA_Employer_Permanent_Street_Line_2__c = (inputvalues.size()>9 && inputvalues[9]!='')?inputvalues[9]:'';
                w.WTPAA_Employer_Permanent_City__c = (inputvalues.size()>10 && inputvalues[10]!='')?inputvalues[10]:'';
                w.WTPAA_Employer_Permanent_State__c = (inputvalues.size()>11 && inputvalues[11]!='')?inputvalues[11]:'';
                
                if((inputvalues.size()>12 && inputvalues[12]!=null)?inputvalues[12].trim()!='':false){
                    w.WTPAA_Employer_Permanent_Zip_Code__c=decimal.valueOf(inputvalues[12]!=null?inputvalues[12].trim():inputvalues[12]);
                }
                
                if(inputvalues.size()>13 && inputvalues[13]!='') {
                     w.WTPAA_Is_same_Employer_Mailing_Address__c = Boolean.valueOf(inputvalues[13]);
                }
                if(w.WTPAA_Is_same_Employer_Mailing_Address__c == true) {
                    w.WTPAA_Employer_Mailing_Address__c = w.WTPAA_Employer_Permanent_Address__c;
                    w.WTPAA_Employer_Mailing_Street_Line_2__c = w.WTPAA_Employer_Permanent_Street_Line_2__c;
                    w.WTPAA_Employer_Mailing_City__c = w.WTPAA_Employer_Permanent_City__c;
                    w.WTPAA_Employer_Mailing_State__c =  w.WTPAA_Employer_Permanent_State__c;
                    w.WTPAA_Employer_Mailing_Zip_Code__c = w.WTPAA_Employer_Permanent_Zip_Code__c; 
                } else {
                    w.WTPAA_Employer_Mailing_Address__c = (inputvalues.size()>14 && inputvalues[14]!='')?inputvalues[14]:'';
                    w.WTPAA_Employer_Mailing_Street_Line_2__c = (inputvalues.size()>15 && inputvalues[15]!='')?inputvalues[15]:'';
                    w.WTPAA_Employer_Mailing_City__c = (inputvalues.size()>16 && inputvalues[16]!='')?inputvalues[16]:'';
                    w.WTPAA_Employer_Mailing_State__c = (inputvalues.size()>17 && inputvalues[17]!='')?inputvalues[17]:'';
                    if((inputvalues.size()>18 && inputvalues[18]!=null)?inputvalues[18].trim()!='':false){
                        w.WTPAA_Employer_Mailing_Zip_Code__c=decimal.valueOf(inputvalues[18]!=null?inputvalues[18].trim():inputvalues[18]);
                    }
                }
                
                w.WTPAA_Employer_Mailing_Phone__c = (inputvalues.size()>19 && inputvalues[19]!='')?inputvalues[19]:'';
                w.Employee_Name__c = (inputvalues.size()>20 && inputvalues[20]!='')?inputvalues[20]:'';
                w.WTPAA_Employee_Physical_Address__c = (inputvalues.size()>21 && inputvalues[21]!='')?inputvalues[21]:'';
                w.WTPAA_Employee_Physical_City__c = (inputvalues.size()>22 && inputvalues[22]!='')?inputvalues[22]:'';
                w.WTPAA_Employee_Physical_State__c = (inputvalues.size()>23 && inputvalues[23]!='')?inputvalues[23]:'';
                if((inputvalues.size()>24 && inputvalues[24]!=null)?inputvalues[24].trim()!='':false){
                    w.WTPAA_Employee_Physical_Zip_Code__c=decimal.valueOf(inputvalues[24]!=null?inputvalues[24].trim():inputvalues[24]);
                }
               
                if(inputvalues.size()>25 && inputvalues[25]!='') {
                     w.WTPAA_Is_same_Employee_Mailing_Address__c = Boolean.valueOf(inputvalues[25]);
                }
                if(w.WTPAA_Is_same_Employee_Mailing_Address__c == True ) {
                    w.WTPAA_Employee_Mailing_Address__c = w.WTPAA_Employee_Physical_Address__c;
                    w.WTPAA_Employee_Mailing_City__c = w.WTPAA_Employee_Physical_City__c;
                    w.WTPAA_Employee_Mailing_State__c = w.WTPAA_Employee_Physical_State__c;
                    w.WTPAA_Employee_Mailing_Zip_Code__c =  w.WTPAA_Employee_Physical_Zip_Code__c; 
                } else {
                    w.WTPAA_Employee_Mailing_Address__c = (inputvalues.size()>26 && inputvalues[26]!='')?inputvalues[26]:'';
                    w.WTPAA_Employee_Mailing_Street_Line_2__c = (inputvalues.size()>27 && inputvalues[27]!='')?inputvalues[27]:'';
                    w.WTPAA_Employee_Mailing_City__c = (inputvalues.size()>28 && inputvalues[28]!='')?inputvalues[28]:'';
                    w.WTPAA_Employee_Mailing_State__c = (inputvalues.size()>29 && inputvalues[29]!='')?inputvalues[29]:'';
                    if((inputvalues.size()>30 && inputvalues[30]!=null)?inputvalues[30].trim()!='':false){
                        w.WTPAA_Employee_Mailing_Zip_Code__c=decimal.valueOf(inputvalues[30]!=null?inputvalues[30].trim():inputvalues[30]);
                    }
                }
                w.WTPAA_Employee_Mailing_Phone__c = (inputvalues.size()>31 && inputvalues[31]!='')?inputvalues[31]:'';
                w.WTPAA_Pay_Frequency__c = 'bi-weekly';
                w.WTPAA_Designated_Pay_Day__c = 'Friday';
                if(inputvalues.size()>38 && inputvalues[38]!='') {
                     w.WTPAA_Minimum_Wage__c = Boolean.valueOf(inputvalues[38]);
                }
                if(inputvalues.size()>39 && inputvalues[39]!='') {
                     w.WTPAA_Living_Wage__c = Boolean.valueOf(inputvalues[39]);
                }
                if(inputvalues.size()>40 && inputvalues[40]!='') {
                     w.WTPAA_Living_Wage_Exempt__c = Boolean.valueOf(inputvalues[40]);
                }
                if(inputvalues.size()>41 && inputvalues[41]!='') {
                     w.WTPAA_Employer_Wage_Rate__c = Boolean.valueOf(inputvalues[41]);
                }
                w.WTPAA_Pay_Basis__c = (inputvalues.size()>42 && inputvalues[42]!='')?inputvalues[42]:'';
              //  w.WTPAA_Pay_Basis__c = 'Salary';
                if(inputvalues.size()>43 && inputvalues[43]!='') {
                     w.WTPAA_Hourly__c = Boolean.valueOf(inputvalues[43]);
                }
                if(inputvalues.size()>44 && inputvalues[44]!='') {
                     w.WTPAA_Salary__c = Boolean.valueOf(inputvalues[44]);
                }
                if((inputvalues.size()>45 && inputvalues[45]!=null)?inputvalues[45].trim()!='':false){
                    w.WTPAA_RateofPay__c=decimal.valueOf(inputvalues[45]!=null?inputvalues[45].trim():inputvalues[45]);
                }
                if((inputvalues.size()>46 && inputvalues[46]!=null)?inputvalues[46].trim()!='':false){
                    w.WTPAA_Overtime_RateofPay__c=decimal.valueOf(inputvalues[46]!=null?inputvalues[46].trim():inputvalues[46]);
                }           
                if(inputvalues.size()>47 && inputvalues[47]!=''){
                    w.WTPAA_Admin_Pay_Exemption__c=Boolean.valueOf(inputvalues[47]);
                }
                if(inputvalues.size()>48 && inputvalues[48]!=''){
                    w.WTPAA_Executive_Pay_Exemption__c=Boolean.valueOf(inputvalues[48]);
                }
                if(inputvalues.size()>49 && inputvalues[49]!='') {
                    w.WTPAA_Professional_Pay_Exemption__c=Boolean.valueOf(inputvalues[49]);
                }
                w.WTPAA_Other_Pay_Exemption__c = (inputvalues.size()>50 && inputvalues[50]!='')?inputvalues[50]:'';
             /*   if(w.WTPAA_Admin_Pay_Exemption__c == true || w.WTPAA_Professional_Pay_Exemption__c == true || w.WTPAA_Executive_Pay_Exemption__c == true || w.WTPAA_Other_Pay_Exemption__c !=null ) {
                    w.WTPAA_Overtime_Pay_Exempn_for_bona_fide__c = true;
                } 
             */   
                w.WTPAA_Classification1__c = (inputvalues.size()>57 && inputvalues[57]!='')?inputvalues[57]:'';
                w.WTPAA_Classification2__c = (inputvalues.size()>58 && inputvalues[58]!='')?inputvalues[58]:'';
                w.WTPAA_Classification3__c = (inputvalues.size()>59 && inputvalues[59]!='')?inputvalues[59]:'';
                if((inputvalues.size()>60 && inputvalues[60]!=null)?inputvalues[60].trim()!='':false) {
                    w.WTPAA_Prevailing_Rate1__c=decimal.valueOf(inputvalues[60]!=null?inputvalues[60].trim():inputvalues[60]);
                }
                if((inputvalues.size()>61 && inputvalues[61]!=null)?inputvalues[61].trim()!='':false) {
                    w.WTPAA_Prevailing_Rate2__c=decimal.valueOf(inputvalues[61]!=null?inputvalues[61].trim():inputvalues[61]);
                }
                if((inputvalues.size()>62 && inputvalues[62]!=null)?inputvalues[62].trim()!='':false) {
                    w.WTPAA_Prevailing_Rate3__c=decimal.valueOf(inputvalues[62]!=null?inputvalues[62].trim():inputvalues[62]);
                    System.debug('value111'+w.WTPAA_Prevailing_Rate3__c); 
                }
               //Added here by siva on 28/10/15
                if(inputvalues.size()>64 && inputvalues[64]!=''){
                    w.WTPAA_Exemption_Non_Exemption__c = Boolean.valueof(inputvalues[64]);
                } 
                
                if(inputvalues.size()>63 && inputvalues[63]!=''){
                    w.WTPAA_Overtime_Pay_Exempn_for_bona_fide__c = Boolean.valueof(inputvalues[63]);
                } 
                
                if(inputvalues.size()>65 && inputvalues[65]!=''){
                    w.WTPAA_Wage_Transfer__c = string.valueof(inputvalues[65]);
                } 
                
                if(inputvalues.size()>66 && inputvalues[66]!=''){
                    w.WTPAA_Invalid_Acknowlegment__c = Boolean.valueof(inputvalues[66]);
                } 
                
              //Ended here by siva on 28/10/15  
                wageList.add(w);  
            }
            counter++;  
        }
        System.debug('rows'+rows);

        return wageList;
      
    }
        
    public pagereference pg()
    {
        return page.ViewFailedWageNoticesInUI;
    }
    
    public String getFailedRows() {
        String result='';
        result = rows.remove(0);
        while(!rows.isEmpty()) {
            result += '\r\n' + rows[0];
            rows.remove(0);
        }
        
        return result;
    }
}