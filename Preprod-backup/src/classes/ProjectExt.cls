public class ProjectExt {
    public sobject PrjObj{get;set;}
    public boolean inlinepage {get;set;}
    public boolean sec1{get;set;}
    public boolean sec2{get;set;}
    public boolean sec3{get;set;}
    public boolean sec4{get;set;}
    ApexPages.StandardController stdController;
    public ProjectExt(apexpages.StandardController cntllr)
    {
        stdController=cntllr;
        this.PrjObj = (Project__c)stdController.getRecord(); 
        system.debug('$$$$'+PrjObj);
        string pro=String.valueOf(PrjObj.get('Impacts__c'));
        system.debug('$$$'+pro);
        string inlinepg=apexpages.currentPage().getparameters().get('inl');
        if(inlinepg==null || inlinepg=='')
        {
            inlinepage=true;
        }
        else
            inlinepage=false;
    }
    public void sectionload()
    {
        string pro=String.valueOf(PrjObj.get('Impacts__c'));
        system.debug('$$$'+pro);
        if(pro!=null)
        {
            if(pro.contains('Cost Savings'))
            {
                sec1=true;
            }
            if(pro.contains('Efficiencies'))
            {
                sec2=true;
            }
            if(pro.contains('Customer Sat – External'))
            {
                sec3=true;
            }
            if(pro.contains('Regulatory and/or Compliance'))
            {
                sec4=true;
                
            }
        }
    }
}