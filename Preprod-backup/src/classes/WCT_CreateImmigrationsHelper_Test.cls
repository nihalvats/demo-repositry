/**
    * Class Name  : WCT_CreateImmigrationsHelper_Test
    * Description : This apex test class will use to test the WCT_CreateImmigrationsHelper
*/
@isTest       
private class WCT_CreateImmigrationsHelper_Test{

    public static List<Contact> employeeList = new List<Contact>();
    public static List<WCT_Immigration_Stagging_Table__c> immgStageList = new List<WCT_Immigration_Stagging_Table__c>();
    public static String empRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.Employee_RT).getRecordTypeId();
    String adhoccanRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CONTACT_GROUP_ADHOCCONTACTTYPE).getRecordTypeId();
    String canRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CONTACT_GROUP_CONTACTTYPE).getRecordTypeId();
     /** 
        Method Name  : createImmigrationStageTable
        Return Type  : List<WCT_Immigration_Stagging_Table__c>
        Type      : private
        Description  : Create Immigration Stage Table Records.         
    */
    private Static List<WCT_Immigration_Stagging_Table__c> createImmigrationStageTable()
    {
      immgStageList = WCT_UtilTestDataCreation.createImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
      insert immgStageList;
      return  immgStageList;
    }
    
     /** 
        Method Name  : createEmployees
        Return Type  : List<Contact>
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private static List<Contact> createEmployees()
    {
        employeeList = WCT_UtilTestDataCreation.createContactWithEmployee(empRecordTypeId);
        insert employeeList;
        return employeeList;
    }
    
    /** 
        Method Name  : createImmigrationsTestMethod
        Return Type  : void
        Description  : Create Immigrations with all staging Table.         
    */
    static testMethod void createImmigrationsTestMethod() {
        employeeList = createEmployees();
        immgStageList = createImmigrationStageTable();
        Set<Id> stagingIds=new Set<id>();
        for(WCT_Immigration_Stagging_Table__c rec: immgStageList)
        {
                stagingIds.add(rec.Id);
        }        
        Test.startTest();
        WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_Immigration_Stagging_Table__c> staggingList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
       // System.assertEquals(staggingList.Size(), immgStageList.Size());
        
    }
    
    /** 
        Method Name  : createL1BImmigrationsTestMethod
        Return Type  : void
        Description  : Create L1B Immigrations with all staging Table.         
    */
    static testMethod void createL1BImmigrationsTestMethod() {
        employeeList = createEmployees();
        immgStageList = WCT_UtilTestDataCreation.createL1BImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        insert immgStageList;
        Set<Id> stagingIds=new Set<id>();
        for(WCT_Immigration_Stagging_Table__c rec: immgStageList)
        {
                stagingIds.add(rec.Id);
        }        
        Test.startTest();
        WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_Immigration_Stagging_Table__c> staggingList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
     //   System.assertEquals(staggingList.Size(), immgStageList.Size());
        
    }
    
    /** 
        Method Name  : createB1ImmigrationsTestMethod
        Return Type  : void
        Description  : Create L1B Immigrations with all staging Table.         
    */
    static testMethod void createB1ImmigrationsTestMethod() {
        employeeList = createEmployees();
        immgStageList = WCT_UtilTestDataCreation.createB1ImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        insert immgStageList;
        Set<Id> stagingIds=new Set<id>();
        for(WCT_Immigration_Stagging_Table__c rec: immgStageList)
        {
                stagingIds.add(rec.Id);
        }        
        Test.startTest();
        WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_Immigration_Stagging_Table__c> staggingList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
      //  System.assertEquals(staggingList.Size(), immgStageList.Size());
        
    }
    static testMethod void createB1ImmigrationsTestMethod1() {
        employeeList = createEmployees();
        immgStageList = WCT_UtilTestDataCreation.createB1ImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        insert immgStageList;
        Set<Id> stagingIds=new Set<id>();
        for(WCT_Immigration_Stagging_Table__c rec: immgStageList)
        {
                stagingIds.add(rec.Id);
        }        
        Test.startTest();
        WCT_CreateImmigrationsHelper w=new WCT_CreateImmigrationsHelper();
        
        WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_Immigration_Stagging_Table__c> staggingList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
      //  System.assertEquals(staggingList.Size(), immgStageList.Size());
        
    }
    /** 
        Method Name  : createLCAImmigrationsTestMethod
        Return Type  : void
        Description  : Create LCA Immigrations with all staging Table.         
    */
    static testMethod void createLCAImmigrationsTestMethod() {
        employeeList = createEmployees();
        immgStageList = WCT_UtilTestDataCreation.createLCAImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        insert immgStageList;
        Set<Id> stagingIds=new Set<id>();
        for(WCT_Immigration_Stagging_Table__c rec: immgStageList)
        {
                stagingIds.add(rec.Id);
        }        
        Test.startTest();
       
        WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_Immigration_Stagging_Table__c> staggingList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
      
    
        
       // System.assertEquals(staggingList.Size(), immgStageList.Size());
        
    }
    
        /** 
        Method Name  : create H1B ImmigrationsTestMethod
        Return Type  : void
        Description  : Create H1B Immigrations with all staging Table.         
    */
    static testMethod void createH1BImmigrationsTestMethod() {
        employeeList = createEmployees();
        immgStageList = WCT_UtilTestDataCreation.createImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        insert immgStageList;
        Set<Id> stagingIds=new Set<id>();
        for(WCT_Immigration_Stagging_Table__c rec: immgStageList)
        {
                stagingIds.add(rec.Id);
        }        
        Test.startTest();
        WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        Test.stopTest();
        List<WCT_Immigration_Stagging_Table__c> staggingList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
      //  System.assertEquals(staggingList.Size(), immgStageList.Size());
        
    }
    
    /** 
        Method Name  : duplicateRowsError
        Return Type  : void
        Description  : Create Immigrations with all staging Table.         
    */
    static testMethod void duplicateRowsError() {
        employeeList = createEmployees();
        immgStageList = createImmigrationStageTable();
        List<WCT_Immigration_Stagging_Table__c> dupImmigrationList = WCT_UtilTestDataCreation.createImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
       
       /*Upadate the Stagiing record with the sample Test email whose Contact we are craeting. */
       
       for(integer i=0; i<dupImmigrationList.size();i++)
       {
           WCT_Immigration_Stagging_Table__c temp= dupImmigrationList[i];
           if(i==0|| i==1)
           {
                temp.WCT_Employee_Email__c='test@deloitte.com';
                temp.WCT_Identifier__c='test@deloitte.com H1B';
                dupImmigrationList[i]=temp;

           }
       }
       
        insert dupImmigrationList;
        
        
       // System.assertEquals(dupImmigrationList.Size(),10);
        Set<Id> stagingIds=new Set<id>();
        for(WCT_Immigration_Stagging_Table__c rec:dupImmigrationList)
        {
                stagingIds.add(rec.Id);
        }
        Test.startTest();
       // System.assertEquals(stagingIds.Size(),10);
        WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
        //Test.stopTest();
        List<WCT_Immigration_Stagging_Table__c> imgList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
       // System.assertEquals(imgList.Size(), immgStageList.Size());
        
        for(WCT_Immigration_Stagging_Table__c rec:dupImmigrationList)
        {
                stagingIds.add(rec.Id);
        }
        //Test.startTest();
       // System.assertEquals(stagingIds.Size(),20);

         string rtId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
        
       String rt2Id=Schema.SObjectType.WCT_Immigration__c.getRecordTypeInfosByName().get('B1 Visa').getRecordTypeId();
        
       
        Contact con=new Contact();
        con.lastName='LastName';
        con.firstName='FirstName';
        con.Email='test@deloitte.com';
        con.MobilePhone='123456789';
        con.RecordTypeId=rtId;
       
    // Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
    //  con.email = 'test@deloitte.com';
        insert con;
        
        
        WCT_Mobility__c mobRec = new WCT_Mobility__c();
        List<WCT_Mobility__c> stagemob= new List<WCT_Mobility__c>(); 
        string rt1Id= Schema.SObjectType.WCT_Mobility__c.getRecordTypeInfosByName().get('Business Visa').getRecordTypeId();
        mobRec.RecordTypeId = rt1Id;
     //   mobRec.WCT_Employee_Email_Address__c= con.email;
        mobRec.WCT_Mobility_Employee__c = con.Id;
        mobRec.WCT_Mobility_Status__c = 'New';
        
        stagemob.add(mobRec);
       INSERT stagemob;
       
        WCT_Immigration__c immRec1 = new WCT_Immigration__c();
        WCT_Immigration__c immRec2 = new WCT_Immigration__c();
        
        WCT_Immigration__c immRec3 = new WCT_Immigration__c();
      List<WCT_Immigration__c > stageimm= new List<WCT_Immigration__c >(); 
             
        immRec1.WCT_Visa_Type__c = 'B1/B2';
        immRec1.RecordTypeId = rt2Id;
        immRec1.WCT_Immigration_Status__c = 'Petition Denied';
        immRec1.Employee_Email_Id__c=con.email;
        immRec1.WCT_Assignment_Owner__c=con.Id;
           
      
             
        immRec2.WCT_Visa_Type__c = 'B1/B2';
        immRec2.RecordTypeId = rt2Id;
        immRec2.WCT_Immigration_Status__c = 'Petition Denied';
        immRec2.Employee_Email_Id__c=con.email;
        immRec2.WCT_Assignment_Owner__c=con.Id;  
        immRec2.wct_identifier__c='test@deloitte.com H1B';                  
                    
        stageimm.add(immRec1);
        stageimm.add(immRec2);
  
        INSERT stageimm;
      system.debug('employee email :: '+stageimm[0].WCT_Employee_Email_Address__c);
        
        
   
//WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
                
       
        Test.stopTest();
        List<WCT_Immigration_Stagging_Table__c> imggList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
      //  System.assertEquals(imggList.Size(), immgStageList.Size());        List<WCT_Immigration_Stagging_Table__c> imggList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
      //  System.assertEquals(imggList.Size(), immgStageList.Size());
        List<WCT_Immigration_Stagging_Table__c> dupImgList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_ERROR];
       // System.assertEquals(dupImgList.Size(), dupImmigrationList.Size());
    }
    
    
    
      /** 
        Method Name  : duplicateRowsError
        Return Type  : void
        Description  : Create Immigrations with all staging Table.         
    */
    static testMethod void duplicateRowsError2() {
        employeeList = createEmployees();
        immgStageList = createImmigrationStageTable();
        List<WCT_Immigration_Stagging_Table__c> dupImmigrationList = WCT_UtilTestDataCreation.createImmigrationStageTable(WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
       
       /*Upadate the Stagiing record with the sample Test email whose Contact we are craeting. */
       
       for(integer i=0; i<dupImmigrationList.size();i++)
       {
           WCT_Immigration_Stagging_Table__c temp= dupImmigrationList[i];
           if(i==0|| i==1)
           {
                temp.WCT_Employee_Email__c='test@deloitte.com';
                temp.WCT_Identifier__c='test@deloitte.com_H1B';
                dupImmigrationList[i]=temp;

           }
       }
       
        insert dupImmigrationList;
        
        
       // System.assertEquals(dupImmigrationList.Size(),10);
        Set<Id> stagingIds=new Set<id>();
        for(WCT_Immigration_Stagging_Table__c rec:dupImmigrationList)
        {
                stagingIds.add(rec.Id);
        }
        Test.startTest();
       
        List<WCT_Immigration_Stagging_Table__c> imgList=[select Id from WCT_Immigration_Stagging_Table__c where Id IN:stagingIds and Status__c=:WCT_UtilConstants.STAGING_STATUS_COMPLETED];
       // System.assertEquals(imgList.Size(), immgStageList.Size());
        
        for(WCT_Immigration_Stagging_Table__c rec:dupImmigrationList)
        {
                stagingIds.add(rec.Id);
        }
        //Test.startTest();
       // System.assertEquals(stagingIds.Size(),20);

         string rtId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
        
       String rt2Id=Schema.SObjectType.WCT_Immigration__c.getRecordTypeInfosByName().get('B1 Visa').getRecordTypeId();
        
       
        Contact con=new Contact();
        con.lastName='LastName';
        con.firstName='FirstName';
        con.Email='test@deloitte.com';
        con.MobilePhone='123456789';
        con.RecordTypeId=rtId;
       
    // Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
    //  con.email = 'test@deloitte.com';
        insert con;
        
        
        WCT_Mobility__c mobRec = new WCT_Mobility__c();
        List<WCT_Mobility__c> stagemob= new List<WCT_Mobility__c>(); 
        string rt1Id= Schema.SObjectType.WCT_Mobility__c.getRecordTypeInfosByName().get('Business Visa').getRecordTypeId();
        mobRec.RecordTypeId = rt1Id;
     //   mobRec.WCT_Employee_Email_Address__c= con.email;
        mobRec.WCT_Mobility_Employee__c = con.Id;
        mobRec.WCT_Mobility_Status__c = 'New';
        
        stagemob.add(mobRec);
       INSERT stagemob;
       
        WCT_Immigration__c immRec1 = new WCT_Immigration__c();
        WCT_Immigration__c immRec2 = new WCT_Immigration__c();
        
        WCT_Immigration__c immRec3 = new WCT_Immigration__c();
      List<WCT_Immigration__c > stageimm= new List<WCT_Immigration__c >(); 
             
        immRec1.WCT_Visa_Type__c = 'B1/B2';
        immRec1.RecordTypeId = rt2Id;
        immRec1.WCT_Immigration_Status__c = 'Petition Denied';
        immRec1.Employee_Email_Id__c=con.email;
        immRec1.WCT_Assignment_Owner__c=con.Id;
           
      
             
        immRec2.WCT_Visa_Type__c = 'B1/B2';
        immRec2.RecordTypeId = rt2Id;
        immRec2.WCT_Immigration_Status__c = 'Petition Denied';
        immRec2.Employee_Email_Id__c=con.email;
        immRec2.WCT_Assignment_Owner__c=con.Id;  
        immRec2.wct_identifier__c='test@deloitte.com_H1B';                  
                    
        stageimm.add(immRec1);
        stageimm.add(immRec2);
  
        INSERT stageimm;
     // system.debug('employee email :: '+stageimm[0].WCT_Employee_Email_Address__c);
        
        
   
WCT_CreateImmigrationsHelper.createImmigrations(stagingIds, WCT_UtilConstants.STAGING_STATUS_NOT_STARTED);
                
       
        Test.stopTest();
      
    }
    
    static testmethod void m1()
   {
       
       
        Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCvm','Employee','test_mob@deloitte.com');
      insert con;
      
        List<WCT_Mobility__c> mobilities= new List<WCT_Mobility__c>();
       WCT_Mobility__c temp = new WCT_Mobility__c();
       temp.RecordTypeId=Schema.SObjectType.WCT_Mobility__c.getRecordTypeInfosByName().get(WCT_UtilConstants.Business_VISA_RT).getRecordTypeId();
       temp.WCT_Mobility_Employee__c=con.id;
       temp.WCT_Visa_start_date__c=system.today()+2;
       temp.WCT_Visa_expiration_date__c=system.today()+2;
       mobilities.add(temp);
       try
       {
      		WCT_CreateImmigrationsHelper.createMobilityImmigrations(mobilities);
       }
       Catch(Exception e)
       {
           
       }
    }

}