/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class WCT_L2_with_EAD_FormController_Test 

{
    public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        recordtype rt2=[select id from recordtype where Name = 'L2 with EAD'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        WCT_Immigration__c immRec1  = new WCT_Immigration__c();
        immRec1.RecordTypeId = rt2.id;
        immRec1.WCT_Immigration_Status__c= 'New';
        immRec1.WCT_Immigration_Closed__c= false;  
        immRec1.WCT_Assignment_Owner__c = con.id;
        insert immRec1;
        
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        insert mob;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef; 
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        
        Attachment attvar = WCT_UtilTestDataCreation.createAttachment(t.id);
        
        insert attvar;
        
        datetime visastart=date.Today().adddays(-10);
        datetime visaend=date.Today().adddays(-20);
       

        Test.starttest();
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_L2_with_EAD_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_L2_with_EAD_FormController controller=new WCT_L2_with_EAD_FormController();
        
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_L2_with_EAD_FormController();
        controller.immigrationRecordTypeId=rt2.id;
        controller.save();
        controller.l1visaStartDate=visastart.format('MM/dd/yyyy');
        controller.l1visaEndDate=visaend.format('MM/dd/yyyy');
        controller.save();
        controller.l2visaStartDate=visastart.format('MM/dd/yyyy');
        controller.l2visaEndDate=visaend.format('MM/dd/yyyy');
        controller.save();
        controller.eadstart=visastart.format('MM/dd/yyyy');
        controller.eadexpire=visaend.format('MM/dd/yyyy');
        controller.save();
        controller.applicationdate=visaend.format('MM/dd/yyyy');
        controller.save();
        controller.l2visaStartDate=visaend.format('MM/dd/yyyy');
        controller.l2visaEndDate=visastart.format('MM/dd/yyyy');
        controller.save();
        controller.l1visaStartDate=visaend.format('MM/dd/yyyy');
        controller.l1visaEndDate=visastart.format('MM/dd/yyyy');
        controller.save(); 
        controller.eadstart=visaend.format('MM/dd/yyyy');
        controller.eadexpire=visastart.format('MM/dd/yyyy');
        controller.save();
        controller.getAttachmentInfo();
       
        controller.doc=WCT_UtilTestDataCreation.createDocument();
    //    controller.uploadAttachment();
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
     //   controller.uploadAttachment();
    }
    
 public static testmethod void m3()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        
        recordtype rt2=[select id from recordtype where Name = 'L2 with EAD'];
        
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        WCT_Immigration__c immRec1  = new WCT_Immigration__c();
        immRec1.RecordTypeId = rt2.id;
        immRec1.WCT_Immigration_Status__c= 'New';
        immRec1.WCT_Immigration_Closed__c= false;  
        immRec1.WCT_Assignment_Owner__c = con.id;
        immRec1.WCT_EAD_Status__c ='Approved';
        insert immRec1;
        
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        insert mob;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef; 
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        
        Attachment attvar = WCT_UtilTestDataCreation.createAttachment(t.id);
        
        insert attvar;
        
  
         //controller.l1visaStartDate= startdatevar;
        //Date myStartDate = date.newinstance(visastart.year(), visastart.month(), visastart.day());
        
        String startdatevar ='07/09/2016';

        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_L2_with_EAD_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_L2_with_EAD_FormController controller=new WCT_L2_with_EAD_FormController();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        ApexPages.CurrentPage().getParameters().put('l1visaStartDate',startdatevar);
        controller=new WCT_L2_with_EAD_FormController();
        Test.starttest();
      
      controller.l1visaStartDate=date.today().adddays(10).format();
      controller.l1visaEndDate= date.today().adddays(20).format();
          controller.save(); 
        
      controller.l2visaStartDate=date.today().adddays(10).format();
      controller.l2visaEndDate= date.today().adddays(20).format();
      controller.applicationdate= date.today().adddays(30).format();
           controller.save(); 
      controller.eadstart=date.today().adddays(10).format();
      controller.eadexpire= date.today().adddays(20).format();
           controller.save(); 
           controller.getAttachmentInfo();
      controller.doc=WCT_UtilTestDataCreation.createDocument();
    //    controller.uploadAttachment();
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
     //   controller.uploadAttachment();
    }   
    public static testmethod void m2()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        
        recordtype rt2=[select id from recordtype where Name = 'L2 with EAD'];
        
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        WCT_Immigration__c immRec1  = new WCT_Immigration__c();
        immRec1.RecordTypeId = rt2.id;
        immRec1.WCT_Immigration_Status__c= 'New';
        immRec1.WCT_Immigration_Closed__c= false;  
        immRec1.WCT_Assignment_Owner__c = con.id;
        immRec1.WCT_EAD_Status__c ='Approved';
        insert immRec1;
        
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        insert mob;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef; 
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        
        Attachment attvar = WCT_UtilTestDataCreation.createAttachment(t.id);
        
        insert attvar;
        
  
         //controller.l1visaStartDate= startdatevar;
        //Date myStartDate = date.newinstance(visastart.year(), visastart.month(), visastart.day());
        
        String startdatevar ='07/09/2016';

        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_L2_with_EAD_Form;
        Test.setCurrentPage(pageRef); 
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_L2_with_EAD_FormController controller=new WCT_L2_with_EAD_FormController();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        ApexPages.CurrentPage().getParameters().put('l1visaStartDate',startdatevar);
        controller=new WCT_L2_with_EAD_FormController();
        Test.starttest();
      
      controller.l1visaStartDate=date.today().adddays(10).format();
      controller.l1visaEndDate= date.today().adddays(20).format();
        controller.Immigrationrecord.WCT_EAD_Status__c='vfsg';
          controller.save(); 
        
      controller.l2visaStartDate=date.today().adddays(10).format();
      controller.l2visaEndDate= date.today().adddays(20).format();
      controller.applicationdate= date.today().adddays(30).format();
           controller.save(); 
      controller.eadstart=date.today().adddays(10).format();
      controller.eadexpire= date.today().adddays(20).format();
           controller.save(); 
           controller.getAttachmentInfo();
      controller.doc=WCT_UtilTestDataCreation.createDocument();
    //    controller.uploadAttachment();
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
     //   controller.uploadAttachment();
    }   
}