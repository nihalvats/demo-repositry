@istest


public class renewpasstasktest{

static testmethod void inserttask()

{

recordtype contrec = [select id,name from recordtype where name='Employee'];

Contact immcontact=  new contact(LastName='Vats',WCT_ExternalEmail__c = 'abc@deloitte.com', Email ='abc@deloitte.com', recordtypeid=contrec.id);

insert immcontact;

WCT_Passport__c psst=new WCT_Passport__c(WCT_Passport_First_Name__c='Nihal',Passport_Last_Name__c='Vats' ,WCT_Passport__c='test12345' ,WCT_Passport_Issuance_Date__c=date.parse('11/7/2013'), WCT_Passport_Expiration_Date__c=date.parse('11/7/2014') ,WCT_Country_of_Passport__c='INDIA' ,WCT_Passport_Issuing_City__c='Hyderabad' );
psst.WCT_Employee__c=immcontact.id;

insert psst;

recordtype immrecd=[select id,name from recordtype where name='H1 Visa'];

WCT_Immigration__c imminsert = new WCT_Immigration__c(recordtypeid=immrecd.id ,WCT_Assignment_Owner__c=immcontact.id ,WCT_Immigration_Status__c='New' ,WCT_Visa_Type__c='H1B CAP' ,WCT_Visa_Interview_Date__c=datetime.now() ,WCT_OFC_Appointment_Date__c=datetime.now());

insert imminsert ;

recordtype Mobrecd=[select id,name from recordtype where name='Employment Visa'];

WCT_Mobility__c mobinsert= new WCT_Mobility__c(recordtypeid=Mobrecd.id ,WCT_Mobility_Status__c='New' ,WCT_Mobility_Employee__c=immcontact.id  ,Immigration__c=imminsert.id);

insert mobinsert;

WCT_Assignment__c astinsert = new WCT_Assignment__c(WCT_Mobility__c=mobinsert.id,WCT_Employee__c=immcontact.id,WCT_End_Date__c=date.parse('10/7/2013') );

insert astinsert;


}



static testmethod void inserttaskbusinessvisa()

{

recordtype contrec = [select id,name from recordtype where name='Employee'];

Contact immcontact=  new contact(LastName='Vats',WCT_ExternalEmail__c = 'abc@deloitte.com', Email ='abc@deloitte.com',WCT_Citizenship__c='US',WCT_Green_Card_Status__c='Active', recordtypeid=contrec.id);

insert immcontact;


WCT_Passport__c psst=new WCT_Passport__c(WCT_Passport_First_Name__c='Nihal',Passport_Last_Name__c='Vats' ,WCT_Passport__c='test12345' ,WCT_Passport_Issuance_Date__c=date.parse('11/7/2013'), WCT_Passport_Expiration_Date__c=date.parse('11/7/2014') ,WCT_Country_of_Passport__c='INDIA' ,WCT_Passport_Issuing_City__c='Hyderabad' );
psst.WCT_Employee__c=immcontact.id;

insert psst;

recordtype immrecd=[select id,name from recordtype where name='B1 Visa'];

WCT_Immigration__c imminsert = new WCT_Immigration__c(recordtypeid=immrecd.id ,WCT_Assignment_Owner__c=immcontact.id ,WCT_Immigration_Status__c='New' ,WCT_Visa_Type__c='H1B CAP' ,WCT_Visa_Interview_Date__c=datetime.now(),WCT_Visa_Expiration_Date__c=date.parse('11/7/2015') ,WCT_OFC_Appointment_Date__c=datetime.now());

insert imminsert ;

recordtype Mobrecd=[select id,name from recordtype where name='Business Visa'];

WCT_Mobility__c mobinsert= new WCT_Mobility__c(recordtypeid=Mobrecd.id ,WCT_Mobility_Status__c='New' ,WCT_Mobility_Employee__c=immcontact.id  ,Immigration__c=imminsert.id);

insert mobinsert;




WCT_Assignment__c astinsert = new WCT_Assignment__c(WCT_Mobility__c=mobinsert.id,WCT_Status__c = 'Active',WCT_Employee__c=immcontact.id,WCT_End_Date__c=date.parse('10/7/2013') );

insert astinsert;






}




}