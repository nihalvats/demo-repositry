/**
 * Class Name  : WCT_SubmitCandidate_Controller   
 * Description : To create candidate from Phone screen 
 */
public without sharing class WCT_SubmitCandidate_Controller {
    //Variable Declaration
    public Static final string NOTENAMEPREFIX = System.Label.Note_Name_Prefix_in_Phonescreen;
    public String lastname { get; set; }
    public String InitialContact { get; set; }
    public String TechnicalScreener{ get; set; }
    public String TechnicalScreener1 { get; set; }
    public String TechnicalScreener2 { get; set; }
    public String TechnicalScreenerId { get; set; }
    public String TechnicalScreener1Id { get; set; }
    public String TechnicalScreener2Id { get; set; }
    public String CandidateAvailiblity{ get; set; }    
    public String AdditionalNotes{ get; set; }        
    public String officeLocation { get; set; }
    public String taleoId { get; set; }
    public String candidateReqNumber { get; set; }
    public String email { get; set; }
    public String firstname { get; set; }
    public String phone { get; set; }
    public String contactTitle { get; set; }
    public Boolean showError { get; set; }
    public String errorMsg{ get; set; }
    //Contact candidate { get; set; }
    public Id noteParentId {get;set;} 
    public  Contact c {get;set;}
    
    public boolean page1Visibility{get{ if(page1Visibility==null)page1Visibility=true; return page1Visibility;} set;}
    public WCT_Candidate_Requisition__c candidateReq {get;set;}
    
    //Document and Attachment List
    public List<string> docIdList = new List<string>();
    public List<string> SelecteddocIdList = new List<string>();
    public List<Document> selectedDocumentList = new List<Document>();
    public List<AttachmentsOnCandidateWrapper> UploadedDocumentList { get; set; }
    public List<Attachment> attachmentsToInsertList = new List<Attachment>();
    
    //Attachment Variables
    public Document doc {get;set;}
    public Document docNew {get;set;}
    public String docId {get;set;}
    public Attachment attachment{ get;set;}  
    public boolean attachmentSection {get; set;}
    public string attFileName{get;set;}
    public Blob attFile{get;set;}
    public String attFileType{get;set;}
    public boolean removeAttachment{get; set;}
    
    //Documents Wrapper
    public class AttachmentsOnCandidateWrapper{
        public boolean isSelected {get;set;}
        public string docName {get;set;}
        public string documentId {get;set;}
        
        
        public AttachmentsOnCandidateWrapper(boolean selected,String Name,String Id){
            isSelected = selected;
            docName = Name ;
            documentId = Id;
        }
        
    }
    
    
    //Controller
    public WCT_SubmitCandidate_Controller(){
        doc = new document();
        c = new Contact();
        attachment = new Attachment();
        UploadedDocumentList = new List<AttachmentsOnCandidateWrapper>();
        attachmentSection = false;
        removeAttachment = false;
        candidateReq= new WCT_Candidate_Requisition__c();
    }
    //Cancel Method
    public PageReference can() {
        return new PageReference('/home/home.jsp');
    }
    /** 
        Method Name  : saveCandidate   
        Return Type  : Inserts Candidate and sends page to
            Create Candidate Tracker
     */
    
    public pageReference nextPage()
    {
        
           
        if(docIdList.isEmpty()){
            showError = true;
            Apexpages.addmessage(new Apexpages.message(Apexpages.severity.FATAL,'Attachment is required for Candidate.'));
            return null;
        }  
        if(!docIdList.isEmpty()){
            for(AttachmentsOnCandidateWrapper attWrap : UploadedDocumentList){
                if(attWrap.isSelected){
                    SelecteddocIdList.add(attWrap.documentId);
                }
            }
        }
        if(SelecteddocIdList.isEmpty()){
            showError = true;
            Apexpages.addmessage(new Apexpages.message(Apexpages.severity.FATAL,'Please select atleast one of the uploaded Attachment for Candidate.'));
            return null;
        } 
        
        /*Control comes here when no validation Errors.*/
        page1Visibility=false;
        
        return null;
    }
    public void saveCandidate() {

        /*Id recordTypeId = [Select r.Name, r.Id
                           From RecordType r 
                           where name = 'Candidate'].id;*/
        Id recordTypeId = System.Label.CandidateRecordtypeId;
     
        
        List<Contact> lstCon = [Select id, firstname,email,WCT_Contact_Type__c,RecordTypeId ,lastname,WCT_Taleo_Id__c from Contact where WCT_Taleo_Id__c =:taleoId  limit 1 ];

       system.debug('######' + lstCon );
        if(lstCon.size()>0 ){
            c = lstCon[0];
            c.firstname = firstname ;
            c.WCT_Taleo_Id__c = taleoId ;
            c.lastname = lastname ;
            c.email = email ;
            c.WCT_Contact_Type__c = 'Candidate';
            c.RecordTypeId  = recordTypeId  ; 
            if(TechnicalScreenerId.length()>0){
                c.WCT_Technical_Screener__c = TechnicalScreenerId.subString(0, 15);
            }
            if(TechnicalScreener1Id.length()>0){
                c.WCT_Technical_Screener2__c = TechnicalScreener1Id.subString(0, 15);
            }
            if(TechnicalScreener2Id.length()>0){
                c.WCT_Technical_Screener3__c = TechnicalScreener2Id.subString(0, 15);
            }
              
            update c;

            noteParentId = c.id;

        } else {

            c = new Contact();
            c.firstname = firstname ;
            c.WCT_Taleo_Id__c = taleoId ;
            c.lastname = lastname ;
            c.email = email ;
            c.WCT_Contact_Type__c = 'Candidate';
            c.RecordTypeId  = recordTypeId  ;  
            if(TechnicalScreenerId.length()>0){
                c.WCT_Technical_Screener__c = TechnicalScreenerId.subString(0, 15);
            }
            if(TechnicalScreener1Id.length()>0){
                c.WCT_Technical_Screener2__c = TechnicalScreener1Id.subString(0, 15);
            }
            if(TechnicalScreener2Id.length()>0){
                c.WCT_Technical_Screener3__c = TechnicalScreener2Id.subString(0, 15);
            }

            insert c;   
            noteParentId = c.id;

        }

        Note n = new Note();
        n.parentid = noteParentId ;
        n.title = NOTENAMEPREFIX +' '+ firstname + ' '+lastname;
        if(n.title.length()>79){
            n.title = (NOTENAMEPREFIX +' '+ firstname + ' '+lastname).subString(0, 80);
        }
        //n.title = firstname + ' '+lastname + ' Notes';
        n.body = 'Office Location :'+ officeLocation + '\r\n' +'Initial Contact :'+ InitialContact + '\r\n'
        +'Candidate Availiblity  :'+ CandidateAvailiblity + '\r\n'
        +'Additional Notes :'+ AdditionalNotes + '\r\n' +
        'Technical Screener 1:'+ TechnicalScreener + '\r\n'+
        'Technical Screener 2 :'+ TechnicalScreener1 + '\r\n'+
        'Technical Screener 3 :'+ TechnicalScreener2 + '\r\n';
        insert n;
        
        if(!docIdList.isEmpty()){
            /*for(AttachmentsOnCandidateWrapper attWrap : UploadedDocumentList){
                if(attWrap.isSelected){
                    SelecteddocIdList.add(attWrap.documentId);
                }
            }*/
            if(!SelecteddocIdList.isEmpty()){
                selectedDocumentList = [SELECT id,name,ContentType,Type,Body FROM Document where id IN :SelecteddocIdList];
            }
            for(Document doc : selectedDocumentList){
                attachment = new Attachment();
                attachment.body = doc.body;
                attachment.ContentType = doc.ContentType;
                attachment.Name= doc.Name;
                attachment.parentid = noteParentId;
                attachmentsToInsertList.add(attachment);
                
            }
            if(!attachmentsToInsertList.isEmpty()){
                insert attachmentsToInsertList;
                delete selectedDocumentList;
            }

        }
        
        /* */
        
        //page1Visibility=false;
       // return new PageReference('/apex/WCT_CreateCandidateTracker_Page?id='+noteParentId);
        //return null;
    }

    public void setAttachmentSection(){
        if(attachmentSection ){
            attachmentSection = false;
        }else{
            attachmentSection = true;}

    }

    public void uploadAttachment(){

        /*attachment.body = attFile;
        attachment.ContentType = attFileType;
        attachment.Name= attFileName;*/
        doc.folderId=SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
        if(doc.body != null){
            insert doc;
            docIdList.add(doc.id);
            UploadedDocumentList.add(new AttachmentsOnCandidateWrapper(true,doc.name,doc.id));
            doc = new Document();
        }
        removeAttachment = false;
        setAttachmentSection();
    }
    public void cancelAttachment(){
        //attachment = new attachment();
        setAttachmentSection();

    }

    public void removeAttachment(){
        attachment.body = null;
        attachment.ContentType = null;
        attachment.Name= null;
        attFileName = null;
        removeAttachment = true;

    }

    //Error message construction
    public void sub() {   
        if(errorMsg != null){
            String[] arrError = errorMsg.split(',');
            showError = true;
            for(String s:arrError){
                if(s != 'Test')
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,s));
            }
        }
    }
    
    /*Page 2 Action Class*/
    
    public PageReference Save(){
    
        
        /*Persist the details of the page 1 Here :*/
        
        saveCandidate();
        /*Persisted the page 1 details */    
            if(c.id!=null)
            {
                try{
                candidateReq.WCT_Contact__c = c.id;
                insert candidateReq;
                }catch(DMLException ex){
                     Apexpages.addmessage(new Apexpages.message(Apexpages.severity.FATAL,ex.getMessage()));
                     return null;
                }
             }
            else
            {
                
                Apexpages.addmessage(new Apexpages.message(Apexpages.severity.FATAL,'Something went wrong. Please try again later.'));
            }
    return new Pagereference('/'+c.id);
}

    public void goBack()
    {
         page1Visibility=true;
        
    }
}