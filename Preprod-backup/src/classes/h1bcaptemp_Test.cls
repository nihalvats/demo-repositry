@isTest
 public class h1bcaptemp_Test{
 Static testmethod void h1bcaptemp_TestMethod(){
   
    
   Recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
   Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
   con.WCT_Employee_Group__c = 'Active';
   insert con;
  
   list<WCT_H1BCAP__c> lst = new list<WCT_H1BCAP__c>();
   WCT_H1BCAP__c h1 = new WCT_H1BCAP__c();
   h1.WCT_H1BCAP_Practitioner_Name__c = con.id;
   h1.WCT_H1BCAP_Email_ID__c = 'svalluru@gmail.com';
   h1.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
   h1.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'abc@yahoo.com';
   lst.add(h1);
   insert lst;
   
   Test.StartTest(); 
   h1bcaptemp h = new h1bcaptemp();
   h.H1bcapid = lst[0].id;
   h.getHlbcaplst();
   Test.StopTest(); 
   
  }
 }