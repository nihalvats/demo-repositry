@isTest
public class WCT_Visa_POE_Letter_FormController_Test
{
    public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        insert mob;
        mob.WCT_Do_you_have_POE_Letter__c = 'Yes';
        update mob;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef; 
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        //t.OwnerId = '00G40000001WmgO';
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        t.WCT_Is_Visible_in_TOD__c = false; 
        t.status = 'completed';
        update t;
        Test.starttest();
        //String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_Visa_POE_Letter_Form;
        Test.setCurrentPage(pageRef); 
        //ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_Visa_POE_Letter_FormController controller=new WCT_Visa_POE_Letter_FormController();
        GBL_Attachments attachmentHelper= new GBL_Attachments();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_Visa_POE_Letter_FormController();
        //controller.save();
        //controller.doc=WCT_UtilTestDataCreation.createDocument();
        attachmentHelper.doc=WCT_UtilTestDataCreation.createDocument();
        attachmentHelper.uploadDocument();
        attachmentHelper.uploadRelatedAttachment(t.id);
        controller.getAttachmentInfo();
        controller.pageError = false;
        controller.pageErrorMessage ='error message';
        controller.supportAreaErrorMesssage = 'Error Meassage';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        
         Document doc= new Document();
            doc.Name='test';
            doc.Body=Blob.valueOf('test');
            doc.folderId = SYSTEM.LABEL.Attachment_Zip_Document_Folder_Id;
            insert doc;
            
            controller.attachmentHelper.docIdList.add(doc.Id);
            controller.attachmentHelper.UploadedDocumentList.add(new GBL_Attachments.AttachmentsWrapper(true, doc.name, doc.id,'20'));
        
        
        controller.save();
    }
   
   
}