public class AR_jobsearch_cont
{   public string searchemail{get;set;}
    Transient List<Job__c> joblist{get;set;}
    public List<Job__c> cJobList{get;set;}
    public List<Job__c> skJobList = new List<Job__c>();
    public boolean displayresults{get;set;}
    public boolean displayalert{get;set;}
    public List<Job__c> objJob{get;set;}
    public string snotstring{get;set;}
    public string starsearch{get;set;}
    Set<String> sObjectIds = new Set<String>();
    Set<String> nsObjectIds = new Set<String>();
    Public List<Job__c> nososl = new List<Job__c>();
 
 
    public AR_jobsearch_cont(ApexPages.StandardController controller) {
    displayresults=false;
    objJob= new List<Job__c>();
        snotstring='';
   
    }
  Public void search(){
  try{
      
       if(searchemail == '')
      {
          ObjJob =[Select AR_Reopen_Date__c ,Name ,AR_Account_Industry__c ,Confidential__c ,Post_Date__c,Job_ID__c , AR_Primary_Location_City__c ,Country__c , AR_Primary_Location_State__c ,CCL_Company_posting_name_communications__c, Industry__c,Job_Category__c, Status__c from Job__c where Status__C=:'Active'];
            displayresults=true;
            displayalert=false;
            System.debug('#### ObjJob '+ObjJob);
                 return;
      }
      else 
      {
  snotstring='%'+searchemail+'%';
  starsearch='*'+searchemail+' *';
  displayalert=true;
   joblist= new list<Job__c>();
   objJob.clear();
   joblist.clear();
   sObjectIds.clear();
   nsObjectids.clear();
      
     
          
   cJobList=[Select id,name from Job__c where  AR_Company_Name__c Like :snotstring and Confidential__c!=False];
   for(Job__c n : cJobList)
   {
    nsObjectIds.add(n.id);
   }
      
 
   nososl=[select id from job__c where CCL_Contact_for_company_if_applicable__c Like :snotstring or Email__c = :snotstring];      
    for(Job__c s: nososl)
   {    
     nsObjectIds.add(s.id);   
   }
   nososl=null;
   skJobList=[Select id,name from Job__c where Status__c='Active'  and RecordType.Name = 'Alumni Relations' and (AR_Nearest_Metropolitan_Area_1__c Like :snotstring or AR_Nearest_Metropolitan_Area_2__c Like :snotstring or AR_Nearest_Metropolitan_Area_3__c Like :snotstring or Job_Category__c like :snotstring or Industry__c like :snotstring)];
   for(Job__c x:skJobList)
   {
       sObjectIds.add(x.id);       
   }
   List<List <sObject>> searchList = [FIND :starsearch IN All FIELDS RETURNING  Job__c(Job_Category__c,Industry__c,Post_Date__c,Name,Job_ID__c,
Description__c,AR_Requirements__c,AR_Company_Name__c,CCL_Company_posting_name_communications__c,AR_Nearest_Metropolitan_Area_1__c,AR_Nearest_Metropolitan_Area_2__c,AR_Nearest_Metropolitan_Area_3__c)];
   joblist = ((List<Job__c>) searchList[0]);
   for(Job__c j : joblist){
   sObjectIds.add(j.id);
   }
   for(string s:sObjectIds)
    {
    if(nsObjectIds.contains(s))
    {
        system.debug('&&&&'+s);
        sObjectIds.remove(s);     
    }
}
 objJob = [select Name,id,Industry__c,AR_Account_Industry__c ,Country__c,Location__c,AR_Reopen_Date__c,CCL_Company_posting_name_communications__c ,Confidential__c,Job_Category__c,AR_Primary_Location_City__c,AR_Primary_Location_State__c,Job_ID__c,Post_Date__c from Job__c where id in :sObjectIds and Status__c='Active'  and RecordType.Name = 'Alumni Relations'];
  system.debug('*display_size*'+ObjJob.size());
  if(ObjJob.size()>0){
   displayresults=true;
   displayalert=false;
  }
  else
  {
  displayresults=false;
  displayalert=true;
  }
   system.debug('*displayresults*'+displayresults);
  }
  }
  catch(System.SearchException ex){
         apexpages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Enter keywords.'));
    }
  }
  
  }