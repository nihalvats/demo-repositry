public class h1bcaptemp {
    
    public Id H1bcapid{get;set;}
   
    
   
    public List<WCT_H1BCAP__c> getHlbcaplst() {
        
      
      set<id> setids = new set<id>();
      map<set<id>,WCT_H1BCAP__c> Maprm = new  map<set<id>,WCT_H1BCAP__c>();
    List<WCT_H1BCAP__c> h1b = [SELECT id,WCT_H1BCAP_Practitioner_Name__r.Name, WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_RM_Name__c,WCT_H1BCAP_USI_SLL_Name__c FROM WCT_H1BCAP__c WHERE WCT_H1BCAP_Status__c = Null ];
   
   
    for(WCT_H1BCAP__c h : h1b )
    {
        setids.add(h.WCT_H1BCAP_RM_Name__c);
        Maprm.put(setids,h);
    }            
              
       List<WCT_H1BCAP__c> hlbcaplst = [SELECT id,WCT_H1BCAP_Practitioner_Name__r.Name, WCT_H1BCAP_Project_Name__c FROM WCT_H1BCAP__c WHERE WCT_H1BCAP_Status__c = Null AND WCT_H1BCAP_RM_Name__c =:setids];
       
       
     
        
      return hlbcaplst;
      }
        
        
    }