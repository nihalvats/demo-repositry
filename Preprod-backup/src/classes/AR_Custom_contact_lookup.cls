public class AR_Custom_contact_lookup{

   
 // public Account account {get;set;} // new account to create
  public List<contact> results{get;set;} // search results
  public string searchString{get;set;} // search keyword
    public string label{get; set;}  

    public List<string> filterQuery{get; set;}  
  public AR_Custom_contact_lookup() {
      results= new List<contact>();
      filterQuery= new List<string>();
    
    searchString = System.currentPageReference().getParameters().get('lksrch');
         JSONParser parser;
      parser = JSON.createParser(System.currentPageReference().getParameters().get('serachFilters'));
      
      /*Advance Next Token needed*/
      parser.nextToken();
      /*To skip the null for new Interview */
      parser.nextToken();
      
     while(parser.nextToken()!=null)
     {
         filterQuery.add(parser.getText());
     }

      
         //filterQuery.add(parser.getText()+'123'+System.currentPageReference().getParameters().get('serachFilters'));
           
       
     // label='Recently viewed :';
    runSearch();
  }
   
  // performs the keyword search
  public PageReference search() {
    runSearch();

    return null;
  }
  
  // prepare the query and issue the search command
  private void runSearch() {
    // TODO prepare query string for complex serarches & prevent injections
        if(searchString != '' && searchString != null)
        {
            results = performSearch(searchString); 
    }else
    {
           results = recentQuery();
    }              
  } 
  
  // run the search and return the records found. 
  private List<contact> performSearch(string searchString) {
 
       label='Search Results :';
    String soql = 'select id,name,WCT_Office_City_Personnel_Subarea__c from contact where  WCT_Employee_Status__c=\'Active\' and name LIKE \'%' + searchString +'%\'';
   
     
    soql = soql + ' limit 25';
    System.debug(soql);
    return database.query(soql); 
 
  }
    
 public List<contact> recentQuery() {
 
       label='Recently Viewed :';
       
    String soql = 'Select id,name,WCT_Office_City_Personnel_Subarea__c From contact  where LastViewedDate!=null and WCT_Employee_Status__c=\'Active\'' ;
    
    soql = soql + ' order by  LastViewedDate desc limit 10';
    return database.query(soql); 
   }
    


  
  // used by the visualforce page to send the link to the right dom element
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
    
  // used by the visualforce page to send the link to the right dom element for the text box
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  }



















}