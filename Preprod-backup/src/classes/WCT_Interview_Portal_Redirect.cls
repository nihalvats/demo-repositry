public with sharing class WCT_Interview_Portal_Redirect {
public string srchString {get;set;}
  public PageReference SearchRedirectResult(){
        PageReference SearchRes = new PageReference('/apex/WCT_InterviewPortalSearchResult');
        SearchRes.getParameters().put('q',srchString);
        SearchRes.setRedirect(true);
        return SearchRes;
  }

}