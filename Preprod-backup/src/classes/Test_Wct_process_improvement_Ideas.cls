@istest
public class Test_Wct_process_improvement_Ideas{
 
    static testmethod void improvementideas() {
         recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.email='email.tst@deloitte.com';
        insert con;
         Profile p = [SELECT Id FROM Profile WHERE name ='System Administrator']; 
         User u2 = new User(Alias = 'newUser', Email='email.tst@deloitte.com', 
             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
             LocaleSidKey='en_US', ProfileId = p.Id,isactive=true,
             TimeZoneSidKey='America/Los_Angeles', UserName='newusers@testorg.com');
        insert u2;
      System.runAs(u2)
    {
        
        test.startTest();
      Wct_process_improvement_Ideas testcontroller = new Wct_process_improvement_Ideas();
        testcontroller.saveCase();
       // String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
      //  ApexPages.CurrentPage().getParameters().put('em',encrypt);
       
       testcontroller.doc=WCT_UtilTestDataCreation.createDocument();
      // try{testcontroller.uploadAttachment();   }catch(exception e){}
      testcontroller.docIdList.add(testcontroller.doc.id);
      
      
     testcontroller.pageError=true;
        testcontroller.pageErrorMessage='error message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
       // testcontroller.uploadAttachment();
      
      
       //  testcontroller.uploadAttachment(); 
      test.stopTest();
    }  
       
    }
    
 }