@isTest
public class WCT_OfferApprover_Test {
    public static Contact con;
    public static WCT_Offer__c offer;
    public static WCT_Offer_Status__c tempOffSt;
        
    public static void createData(){
        con=WCT_UtilTestDataCreation.createContact();
        con.email='test1062015@deloitte.com';
        con.WCT_Visa_Type__c='temp';
        con.MobilePhone='123';
        con.WCT_User_Group__c = 'United States';
        insert con;
        
        offer =new WCT_Offer__c();
        offer.WCT_status__c = 'Draft In Progress'; 
        offer.WCT_Candidate__c=con.id;
        offer.WCT_Recruiter_Email__c='test@test.com';
        offer.WCT_Recruiting_Coordinator_Email__c='testco@test.com';
        offer.RecordTypeId=system.label.USExp_offer_rcdtype;
        insert offer;
        Attachment atch=new attachment();
        atch.Name='TestOffer1062015';
        atch.Body=  Blob.valueOf('TestBody1062015');
        atch.ParentId=offer.id;
        insert atch;
        tempOffSt = new WCT_Offer_Status__c(WCT_Related_Offer__c=offer.id);
        insert tempOffSt;
    
    }

    public static testmethod void approveoffer()
    {
      createData()  ;
     Test.starttest();   
      PageReference pageRef = Page.WCT_OfferApprover;
     Test.setCurrentPage(pageRef); 
     ApexPages.CurrentPage().getParameters().put('id',offer.id);
     WCT_OfferApprover apvr=new WCT_OfferApprover();  
      apvr.RedirectToInterview()  ;
     Test.stopTest() ;
        
    }
}