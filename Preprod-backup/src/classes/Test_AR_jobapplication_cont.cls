@isTest
public class Test_AR_jobapplication_cont
{
static testMethod void myUnitTest()
 {
 
     Contact con = new Contact();
con.LastName = 'abc';
con.FirstName = 'xyz';
con.Email = 'abc@invalid.com';

insert con;

List<Contact> objContact = new List<Contact>(); 
objContact = [select id,name from Contact where LastName='abc' limit 1]; 
system.debug('love143'+objContact[0].id);


Company__c acc =new Company__c();
acc.name='Ada Account';
acc.DUNS_Number__c='123456789';
insert acc;

job__c job = new job__c();
job.Email__c = 'sabberaboina@deloitte.com';
job.Name ='Finance Manager';
job.CCL_Company_posting_name_communications__c='ccl';
job.AR_Nearest_Metropolitan_Area_1__c='Austin';
job.AR_Primary_Location_City__c='AK';
job.AR_Primary_Location_State__c='India';
job.Confidential__C=true;
 job.Account_Contact_Name__c='ZZZZ';
        job.CCL_Contact_for_company_if_applicable__c='XXXX';
      //  job.CCL_Alumnet_ID__c=
        job.Attest_Client__c= true;
        job.Country__c='International';

        job.CCL_Contact_for_company_if_applicable__c='eeeee';
        job.Comments__c='Hi';
        //job.job__c=
        job.Compensation_Details__c='Good';
        job.AR_Company_Name__c = 'zzzzzzz';
        job.Description__c='Hello';
        //job.End_Date__c=
        job.Industry__c='IT';
        job.Industry_Sector__c='Software';
        job.Instructions_for_Contacting__c='Job requirement';
        job.Job_Category__c='Tax';
        job.AR_Resume_Recipient_Type__c='Agency';
        job.CCL_Job_Type__c='Java';

        job.Location__c='Boston';
        job.Position__c='Senior Analysist';
        job.Position_Code__c=integer.valueOf(23456);
        //job.Post_Date__c=
        job.Preferred_Phone__c='9999999999';
        job.Region__c='Hyderabad';
        job.CCL_Required_Skills__c='Spring';
        job.AR_Requirements__c='Struts';
        job.Service_Area_CCL__c='';
        job.Status__c='Archived';
        job.Source__c='External';
     insert job;

      Invalid_Submission__c invalid = new Invalid_Submission__c();
Invalid.First_Name__c='TEST&^';
Invalid.Last_Name__c='SFF%';
Invalid.Job__c=job.id;
Invalid.Validated__c=false;
Invalid.Existing_Contact__c='ryeturi@deloitte.com';
//Invalid.CurrencyIsoCode='EUR - Euro';
insert Invalid;
//Invalid.First_Name__c='testing';
//Invalid.Validated__c=true;
//update Invalid;

    List<job__c> objJob = new List<job__c>(); 
    objJob  = [select id,name from Job__c limit 1];
     /**  //objJob.Name='jkhkjk;
    //objJob.Description__c='HI how are you';
    objJob.AR_Requirements__c='Hello';
    objJob.Source__c='External';
    objJob.Job_Category__c='Tax';
    objJob.AR_Company_Name__c = 'zzz';
    objJob.AR_Resume_Recipient_Type__c='Agency';
    objJob.AR_Primary_Location_State__c='IL';
    objJob.AR_Nearest_Metropolitan_Area_1__c='Houston';
    objJob.Country__c='International';
    objJob.CCL_Contact_for_company_if_applicable__c='tttt';
    objJob.Email='abc@gmail.com';  
    insert objJob;*/
    //system.debug('love143'+objJob[0].id);
    
     
       Contact_Job_Association__c conjob = new Contact_Job_Association__c();
        conjob.Contact__c=objContact[0].id;
        conjob.Job__c=objJob[0].id;
        insert conjob;
     
        Attachment attach=new Attachment();    
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=conjob.id;
        insert attach;
    
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:conjob.id];
        System.assertEquals(1, attachments.size());

ApexPages.currentPage().getParameters().put('pid',job.id);
AR_jobapplication_cont obj = new AR_jobapplication_cont(new ApexPages.StandardController(new Contact()));
//obj.con ='ryeturi@deloitte.com'; 
obj.upload();
contact con1=new contact(Lastname='A',AR_Personal_Email__c='ryeturi@deloitte.com',Email='ryeturi@deloitte.com', AR_Deloitte_Email__c='ryeturi@deloitte.com',FirstName='Srikanth',Phone='9620566619');
insert con1;
obj.attachment.name = 'test';
obj.upload();


 

}
}