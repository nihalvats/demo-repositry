global class ScheduledApexRecordType_QueueAssignment implements Schedulable {
  
   global void execute(SchedulableContext ctx) {
               map<string,id> rtIdMap = new map<string,id> ();
               for(RecordType rtypes : [Select Name, Id From RecordType where sObjectType='wct_Offer__c' and isActive=true and (not(name like '%Manual')) and (not(name like '%Locked'))])
               {
                              rtIdMap.put(rtypes.Name,rtypes.Id);
               }
               set<String> queueName= new set<string> {'Deloitte India Campus Offers','Deloitte India Experienced Offers','Deloitte US Campus Offers','Deloitte US Experienced Offers'}; 
               String queuesObjectType='Offer'; 
               map<string,id> quIdMap = new map<string,Id>();
               for(QueueSobject queueID : [Select QueueId , Queue.Name from QueueSobject where Queue.Name =:queueName])
               {
                              quIdMap.put(queueID.Queue.Name,queueID.QueueId );
               }
     List<Wct_Offer__c> Offer = [SELECT Id, WCT_User_Group__c, WCT_Job_Type__c, RecordTypeID, WCT_Candidate_Tracker__c
                FROM Wct_Offer__c WHERE ((RecordTypeID = Null OR RecordTypeID = '') AND (WCT_Candidate_Tracker__c != Null OR WCT_Candidate_Tracker__c != ''))];
    
    List<Wct_Offer__c> OfferList = new List<Wct_Offer__c>();

    For(Wct_Offer__c Off : Offer){
    If(Off.WCT_Job_Type__c == 'Experienced' && Off.WCT_User_Group__c == 'United States'){
    Off.RecordTypeID = rtIdMap.get('US Experienced Hire Offer');
    Off.OwnerID = quIdMap.get('Deloitte US Experienced Offers');
    OfferList.add(Off);
    }
    
    If(Off.WCT_Job_Type__c == 'Experienced' && Off.WCT_User_Group__c == 'US GLS India'){
    Off.RecordTypeID = rtIdMap.get('India Experienced Hire Offer');
    Off.OwnerID = quIdMap.get('Deloitte India Experienced Offers');    
    OfferList.add(Off);
    }
    
    If(Off.WCT_Job_Type__c != 'Experienced' && (Off.WCT_Job_Type__c != '' || Off.WCT_Job_Type__c != Null) && Off.WCT_User_Group__c == 'United States'){
    Off.RecordTypeID = rtIdMap.get('US Campus Hire Offer');
    Off.OwnerID = quIdMap.get('Deloitte US Campus Offers');
    OfferList.add(Off);
    }
    
    If(Off.WCT_Job_Type__c != 'Experienced' && (Off.WCT_Job_Type__c != '' || Off.WCT_Job_Type__c != Null) && Off.WCT_User_Group__c == 'US GLS India'){
    Off.RecordTypeID = rtIdMap.get('India Campus Hire Offer');
    Off.OwnerID = quIdMap.get('Deloitte India Campus Offers');
    OfferList.add(Off);
    }
    }
    Update OfferList;  
   }   
}