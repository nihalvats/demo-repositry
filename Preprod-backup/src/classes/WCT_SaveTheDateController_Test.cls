/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class WCT_SaveTheDateController_Test {

    /*******Varibale Declaration********/
    public static Id profileIdRecCoo=[Select id from Profile where Name=:WCT_UtilConstants.Recruiting_Coordinator].Id;
    public static Id profileIdIntv=[Select id from Profile where Name=:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
    public static Id profileIdRecr=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    public static User userRecRecCoo, userRecRecIntv, userRecRecr;
    public static Event InterviewEvent;
    public static EventRelation InterviewEventRelation;
    /***************/

    /** 
        Method Name  : createUserRecCoo
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecCoo()
    {
        userRecRecCoo=WCT_UtilTestDataCreation.createUser('RecCoo', profileIdRecCoo, 'arunsharmaRecCoo@deloitte.com', 'arunsharma4@deloitte.com');
        insert userRecRecCoo;
        return  userRecRecCoo;
    }
    /** 
        Method Name  : createUserRecr
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecr()
    {
        userRecRecr=WCT_UtilTestDataCreation.createUser('Recr', profileIdRecr, 'arunsharmaRecr@deloitte.com', 'arunsharma4@deloitte.com');
        insert userRecRecr;
        return  userRecRecr;
    }
    /** 
        Method Name  : createUserIntv
        Return Type  : User
        Type         : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserIntv()
    {
        userRecRecIntv=WCT_UtilTestDataCreation.createUser('Intv', profileIdIntv, 'arunsharmaIntv@deloitte.com', 'arunsharma4@deloitte.com');
        insert userRecRecIntv;
        return  userRecRecIntv;
    }
    
    /** 
        Method Name  : createEvent
        Return Type  : Event
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static Event createEvent()
    {
        InterviewEvent=new Event( StartDateTime=system.now(),EndDateTime=system.now()+1);
        system.runAs(userRecRecCoo)
        {
            insert InterviewEvent;
        }
        return  InterviewEvent;
    }     
    /** 
        Method Name  : createEventRelation
        Return Type  : EventRelation
        Type         : private
        Description  : Create temp records for data mapping         
    */    
    private Static EventRelation createEventRelation()
    {
        InterviewEventRelation=WCT_UtilTestDataCreation.createEventRelation(InterviewEvent.Id,userRecRecIntv.Id);
        insert InterviewEventRelation;
        return  InterviewEventRelation;
    }   
    
    
    static testMethod void myUnitTest() {
        userRecRecCoo=createUserRecCoo();
        userRecRecIntv=createUserIntv();
        userRecRecr=createUserRecr();
        
        InterviewEvent=createEvent();
        InterviewEventRelation=createEventRelation();
        ApexPages.currentPage().getParameters().put('Id', InterviewEvent.Id);

        test.startTest();
        WCT_SaveTheDateController STD = new WCT_SaveTheDateController();
        STD.sendInvite();
        WCT_CancelSaveTheDateEvent.sendCancelationInvite(InterviewEvent.id);
        test.stopTest();
    }
    
}