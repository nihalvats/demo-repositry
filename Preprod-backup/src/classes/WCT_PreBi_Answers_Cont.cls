public class WCT_PreBi_Answers_Cont {
    public Case Case_Item{get;set;}
    public WCT_PreBIStageTable__c PreStage  {get;set;}
    public String id = ApexPages.currentPage().getParameters().get('id');
    public List<WCT_QA__C> QA{get;set;}

    public WCT_PreBi_Answers_Cont(ApexPages.StandardController controller) {
      Set<String> s = new Set<String>();
         Case_Item = new Case();
     PreStage = new WCT_PreBIStageTable__c();
     Case_Item =[Select Id,CaseNumber,WCT_Pre_BI_Reference_Id__c from Case where id =:id limit 1];
          PreStage =[Select id,WCT_A1__c,WCT_A2__c,WCT_A3__c,WCT_A4__c,WCT_A5__c,WCT_A6__c,WCT_A7__c,WCT_A8__c,WCT_A9__c,WCT_A10__c,WCT_A11__c,WCT_A12__c,WCT_A13__c,WCT_A14__c,WCT_A15__c,WCT_A16__c,WCT_A17__c,WCT_A18__c,WCT_A19__c,WCT_A20__c,WCT_A21__c,WCT_A22__c,WCT_A23__c,WCT_A24__c,WCT_A25__c,WCT_A26__c,WCT_A27__c,WCT_A28__c,WCT_A29__c,WCT_A30__c,WCT_A31__c,WCT_A32__c,WCT_A33__c,WCT_A34__c,WCT_A35__c,WCT_A36__c,WCT_A37__c,WCT_A38__c,WCT_A39__c,WCT_A40__c,WCT_A41__c,WCT_A42__c,WCT_A43__c,WCT_A44__c,WCT_A45__c,WCT_A46__c from WCT_PreBIStageTable__c where id =:Case_Item.WCT_Pre_BI_Reference_Id__c limit 1];
            if(PreStage.WCT_A1__c =='Yes'){            s.add('Q1');             }
 if(PreStage.WCT_A2__c =='Yes'){            s.add('Q2');             }
 if(PreStage.WCT_A3__c =='Yes'){            s.add('Q3');             }
 if(PreStage.WCT_A4__c =='Yes'){            s.add('Q4');             }
 if(PreStage.WCT_A5__c =='Yes'){            s.add('Q5');             }
 if(PreStage.WCT_A6__c =='Yes'){            s.add('Q6');             }
 if(PreStage.WCT_A7__c =='Yes'){            s.add('Q7');             }
 if(PreStage.WCT_A8__c =='Yes'){            s.add('Q8');             }
 if(PreStage.WCT_A9__c =='Yes'){            s.add('Q9');             }
 if(PreStage.WCT_A10__c =='Yes'){            s.add('Q10');             }
 if(PreStage.WCT_A11__c =='Yes'){            s.add('Q11');             }
 if(PreStage.WCT_A12__c =='Yes'){            s.add('Q12');             }
 if(PreStage.WCT_A13__c =='Yes'){            s.add('Q13');             }
 if(PreStage.WCT_A14__c =='Yes'){            s.add('Q14');             }
 if(PreStage.WCT_A15__c =='Yes'){            s.add('Q15');             }
 if(PreStage.WCT_A16__c =='Yes'){            s.add('Q16');             }
 if(PreStage.WCT_A17__c =='Yes'){            s.add('Q17');             }
 if(PreStage.WCT_A18__c =='Yes'){            s.add('Q18');             }
 if(PreStage.WCT_A19__c =='Yes'){            s.add('Q19');             }
 if(PreStage.WCT_A20__c =='Yes'){            s.add('Q20');             }
 if(PreStage.WCT_A21__c =='Yes'){            s.add('Q21');             }
 if(PreStage.WCT_A22__c =='Yes'){            s.add('Q22');             }
 if(PreStage.WCT_A23__c =='Yes'){            s.add('Q23');             }
 if(PreStage.WCT_A24__c =='Yes'){            s.add('Q24');             }
 if(PreStage.WCT_A25__c =='Yes'){            s.add('Q25');             }
 if(PreStage.WCT_A26__c =='Yes'){            s.add('Q26');             }
 if(PreStage.WCT_A27__c =='Yes'){            s.add('Q27');             }
 if(PreStage.WCT_A28__c =='Yes'){            s.add('Q28');             }
 if(PreStage.WCT_A29__c =='Yes'){            s.add('Q29');             }
 if(PreStage.WCT_A30__c =='Yes'){            s.add('Q30');             }
 if(PreStage.WCT_A31__c =='Yes'){            s.add('Q31');             }
 if(PreStage.WCT_A32__c =='Yes'){            s.add('Q32');             }
 if(PreStage.WCT_A33__c =='Yes'){            s.add('Q33');             }
 if(PreStage.WCT_A34__c =='Yes'){            s.add('Q34');             }
 if(PreStage.WCT_A35__c =='Yes'){            s.add('Q35');             }
 if(PreStage.WCT_A36__c =='Yes'){            s.add('Q36');             }
 if(PreStage.WCT_A37__c =='Yes'){            s.add('Q37');             }
 if(PreStage.WCT_A38__c =='Yes'){            s.add('Q38');             }
 if(PreStage.WCT_A39__c =='Yes'){            s.add('Q39');             }
 if(PreStage.WCT_A40__c =='Yes'){            s.add('Q40');             }
 if(PreStage.WCT_A41__c =='Yes'){            s.add('Q41');             }
 if(PreStage.WCT_A42__c =='Yes'){            s.add('Q42');             }
 if(PreStage.WCT_A43__c =='Yes'){            s.add('Q43');             }
 if(PreStage.WCT_A44__c =='Yes'){            s.add('Q44');             }
 if(PreStage.WCT_A45__c =='Yes'){            s.add('Q45');             }
 if(PreStage.WCT_A46__c =='Yes'){            s.add('Q46');             }
                       System.debug('^^^^^^^^^^^^^^^^^^^^^'+s);
            QA=[select id,name,WCT_Question__c,WCT_BI_Team_Action__c,WCT_India_Action__c,WCT_Recrutier_Action__c,WCT_Answer__c from WCT_QA__C where WCT_Case__c=:id and WCT_Question_Number__c in :(s) limit 46];
    } 

}