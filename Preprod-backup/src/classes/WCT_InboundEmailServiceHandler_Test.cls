/*
Class name : WCT_InboundEmailServiceHandler_Test
Description : This class is the Test Class for WCT_InboundEmailServiceHandler.

Task number     Date            Modified By                         Description
------------    ---------       ---------------------               ----------------------
                21-May-14       Yash Agarwal                       Created
                                                            
*/

@isTest
public class WCT_InboundEmailServiceHandler_Test
{
    static testMethod void m1()
    {
        //insert case record
        recordtype rt=[select id from recordtype where DeveloperName = 'Leaves' limit 1];
        Case c=new case();
        c.RecordTypeId =rt.id;
        insert c;
        String caseId=String.valueOf(c.id);
        
        //insert contact record
        rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        //insert immigration record
        WCT_Immigration__c immi=WCT_UtilTestDataCreation.createImmigration(con.id);
        immi.UID_Number__c = '12345678';
        insert immi;
        
        WCT_Immigration__c immirecord=[select name from WCT_Immigration__c limit 1];
        
        //insert email handler record
        WCT_Email_Handler_Sub_To_Obj_Mapping__c emailhandler=new WCT_Email_Handler_Sub_To_Obj_Mapping__c ();
        emailhandler.WCT_Object__c='WCT_Immigration__c';
        emailhandler.name='Test';
        insert emailhandler;
        
        //getting Organisation ID
        Organization orgDetails = [SELECT Id, LanguageLocaleKey FROM Organization WHERE Id = :UserInfo.getOrganizationId()];
        String organId=String.valueOf(orgDetails.id);
        
        Test.startTest();
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope envelope= new Messaging.InboundEnvelope();
        
        // setup the data for the email
        email.subject='[ ref:_' + organId.substring(0,5) + organId.substring(10,15) +'._'+ caseId.substring(0,5) + caseId.substring(10,15) + ':ref ]';
        email.fromAddress = 'someaddress@email.com';
        email.plainTextBody = 'email body\n2225256325\nTitle';
        
        // add an Binary attachment
        
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
        
        
        // add an Text atatchment
        
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
        
        
        // call the email service class and test it with the data in the testMethod
        WCT_InboundEmailServiceHandler controller=new WCT_InboundEmailServiceHandler ();
        controller.handleInboundEmail(email,envelope);
        email.subject=immirecord.name;
        controller.handleInboundEmail(email,envelope);
   }
    static testMethod void m3()
    {
        //insert case record
        recordtype rt=[select id from recordtype where DeveloperName = 'Leaves' limit 1];
        Case c=new case();
        c.RecordTypeId =rt.id;
        insert c;
        String caseId=String.valueOf(c.id);
        
        //insert contact record
        rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        //insert immigration record
        WCT_Immigration__c immi=WCT_UtilTestDataCreation.createImmigration(con.id);
        immi.UID_Number__c = '12345678';
        insert immi;
        
        WCT_Immigration__c immirecord=[select name from WCT_Immigration__c limit 1];
        
        //insert email handler record
        WCT_Email_Handler_Sub_To_Obj_Mapping__c emailhandler=new WCT_Email_Handler_Sub_To_Obj_Mapping__c ();
        emailhandler.WCT_Object__c='WCT_Immigration__c';
        emailhandler.name='Test';
        insert emailhandler;
        
        //getting Organisation ID
        Organization orgDetails = [SELECT Id, LanguageLocaleKey FROM Organization WHERE Id = :UserInfo.getOrganizationId()];
        String organId=String.valueOf(orgDetails.id);
        
        Test.startTest();
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope envelope= new Messaging.InboundEnvelope();
        List<string>mailto =new list<string>();
        mailto.add('eleleavesteam@deloitte.com');
        // setup the data for the email
        email.subject='[ ref:_' + organId.substring(0,5) + organId.substring(10,15) +'._'+ caseId.substring(0,5) + caseId.substring(10,15) + ':ref ]';
        email.fromAddress = 'someaddress@email.com';
        email.plainTextBody = 'email body\n2225256325\nTitle';
        email.toaddresses=mailto;
        email.ccaddresses=mailto;
        
        // add an Binary attachment
        
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
        
        
        // add an Text atatchment
        
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
        
        
        // call the email service class and test it with the data in the testMethod
        WCT_InboundEmailServiceHandler controller=new WCT_InboundEmailServiceHandler ();
        controller.handleInboundEmail(email,envelope);
        email.subject=immirecord.name;
        controller.handleInboundEmail(email,envelope);
   }
   static testMethod void m2()
    {
        //insert case record
        recordtype rt=[select id from recordtype where DeveloperName = 'Leaves' limit 1];
        Case c=new case();
        c.RecordTypeId =rt.id;
        insert c;
        String caseId=String.valueOf(c.id);
        
        //insert contact record
        rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.email = 'someaddress@email.com';
        con.WCT_Cost_Center__c = '56372';
        insert con;
        
        Group grp = new Group(Name='Queue',Type='Queue');
        insert grp;
        
        QueueSobject mappingObject = new QueueSobject(QueueId = grp.Id, SobjectType = 'WCT_Leave__c');
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            insert mappingObject;
        }
        
        
        //insert immigration record
        WCT_Immigration__c immi=WCT_UtilTestDataCreation.createImmigration(con.id);
        immi.UID_Number__c = '12345678';
        insert immi;
        
        WCT_Immigration__c immirecord=[select name,ownerid from WCT_Immigration__c limit 1];
        
        //insert leaves record
        WCT_Leave__c l = new WCT_Leave__c();
        l.WCT_Leave_End_Date__c = system.today()+12;
        l.WCT_Leave_Start_Date__c = system.today()-550;
        l.WCT_Confirm_Return_to_Work__c = true; 
        l.WCT_Leave_Type__c = 'FMLA';
        l.WCT_Return_to_work_date__c = system.today()+12;
        l.WCT_Leave_End_Date_Status__c = 'Confirmed';
        l.WCT_Employee__c = con.Id;
        l.WCT_Estimated_Start_Date__c = system.today()-550;
        l.WCT_Leave_Reason__c = 'Test';
        l.WCT_Leave_Category__c = 'Military';
        l.WCT_Sub_Category_1__c = 'Military Exigency';
        l.WCT_Sub_Category_2__c = 'Paid';
        l.ownerid=grp.id;
        insert l;
        
        WCT_Leave__c leavesrecord=[select name from WCT_Leave__c limit 1];
        
        //insert email handler record
        WCT_Email_Handler_Sub_To_Obj_Mapping__c emailhandler=new WCT_Email_Handler_Sub_To_Obj_Mapping__c ();
        emailhandler.WCT_Object__c='WCT_Immigration__c';
        emailhandler.name=immirecord.name;
        insert emailhandler;
        
        //getting Organisation ID
        Organization orgDetails = [SELECT Id, LanguageLocaleKey FROM Organization WHERE Id = :UserInfo.getOrganizationId()];
        String organId=String.valueOf(orgDetails.id);
        
        Test.startTest();
        // create a new email and envelope object
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope envelope= new Messaging.InboundEnvelope();
        
        // setup the data for the email
        email.subject=immirecord.name;
        email.fromAddress = 'someaddress@email.com';
        email.plainTextBody = 'email body\n2225256325\nTitle';
        
        // add an Binary attachment
        
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
        
        
        // add an Text atatchment
        
        Messaging.InboundEmail.TextAttachment attachmenttext = new Messaging.InboundEmail.TextAttachment();
        attachmenttext.body = 'my attachment text';
        attachmenttext.fileName = 'textfiletwo3.txt';
        attachmenttext.mimeTypeSubType = 'texttwo/plain';
        email.textAttachments =   new Messaging.inboundEmail.TextAttachment[] { attachmenttext };
        
        
        // call the email service class and test it with the data in the testMethod
        WCT_InboundEmailServiceHandler controller=new WCT_InboundEmailServiceHandler ();
        controller.handleInboundEmail(email,envelope);
        
        //some changes to cover uncovered lines
        emailhandler.WCT_Object__c='WCT_Leave__c';
        emailhandler.name=leavesrecord.name;
        update emailhandler;
        email.subject=leavesrecord.name;
        controller.handleInboundEmail(email,envelope);
        l.ownerid=UserInfo.getUserId();
        update l;
        controller.handleInboundEmail(email,envelope);
   }
}