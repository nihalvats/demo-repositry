/*****************************************************************************************
    Name    : WCT_InterviewTriggerHandler_test
    Desc    : Test class for WCT_InterviewTriggerHandler class                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                  Date            Description
---------------------------------------------------------------------------
  Deloitte                 23 Jan 2014         Created 
******************************************************************************************/
@isTest
private class WCT_InterviewTriggerHandler_TEST{
    static testMethod void intrviewHandler(){
        User user = WCT_UtilTestDataCreation.createUser('tinte',SYSTEM.Label.Integration_Profile_Id,'tintegration@deloitte.com.trigger','tintegration@deloitte.com');
        insert user;
        
        User user2 = WCT_UtilTestDataCreation.createUser('tinte2',SYSTEM.Label.Integration_Profile_Id,'tintegration2@deloitte.com.trigger','tintegration2@deloitte.com');
        insert user2;
        
        SYSTEM.RunAs(user){
            Id recTypeIdIEF = WCT_Util.getRecordTypeIdByLabel('WCT_List_Of_Names__c','IEF library');
            
            WCT_List_Of_Names__c IEF = new WCT_List_Of_Names__c(Name='Test IEF',WCT_Type__c='IEF',WCT_User_Group__c='US',WCT_FSS__c='Consulting',WCT_Job_Type__c='Experienced',RecordTypeId=SYSTEM.Label.WCT_IEFLibraryRecordtypeId);
            insert IEF;
            
            Contact con = new Contact(LastName = 'TestContact',phone='8686322286',Email ='tintegration@deloitte.com'); 
            insert con;
            Contact con2 = new Contact(LastName = 'TestContact',phone='8686322286',Email ='tintegration2@deloitte.com'); 
            insert con2; 

            Contact con3 = new Contact(LastName = 'TestContact',phone='8686322286',Email ='tintegration3@deloitte.com'); 
            insert con3;
            
            WCT_Interview__c interview = new WCT_Interview__c(WCT_FSS__c='Consulting',WCT_Job_Type__c='Experienced',WCT_User_group__c='US',WCT_Clicktools_Form__c=IEF.id);
            insert interview;
            
            WCT_Requisition__c req = new WCT_Requisition__c(Name = 'Test');
            insert req;
            
            WCT_Candidate_Requisition__c canReq = new WCT_Candidate_Requisition__c(WCT_Contact__c = con.Id, WCT_Requisition__c = req.Id);
            insert canReq; 
            
            WCT_Candidate_Requisition__c canReq3 = new WCT_Candidate_Requisition__c(WCT_Contact__c = con3.Id, WCT_Requisition__c = req.Id);
            insert canReq3; 
            
            WCT_Interview_Junction__c intrJunct = new WCT_Interview_Junction__c(WCT_Interview__c=interview.Id, WCT_Interviewer__c = user.Id, WCT_Candidate_Tracker__c = canReq.Id);
            insert intrJunct;
            
            WCT_Interview_Junction__c intrJuncttemp = new WCT_Interview_Junction__c(WCT_Interview__c=interview.Id, WCT_Interviewer__c = user.Id, WCT_Candidate_Tracker__c = canReq.Id);
            insert intrJuncttemp;
            
            intrJunct.WCT_Interviewer__c = user2.Id;
            update intrJunct;
            
            interview.WCT_Interview_Status__c = 'Complete';
            update interview;
            
            interview.WCT_Interview_Status__c = 'Open';
            update interview;
            
            interview.WCT_Interview_Start_Time__c = system.now()+2;
            update interview;
            
            interview.WCT_Restricted_Interview__c = true;
            update interview;
            
            WCT_Interview_Junction__c intrJunct3 = new WCT_Interview_Junction__c(WCT_Interview__c=interview.Id, WCT_Interviewer__c = user2.Id, WCT_Candidate_Tracker__c = canReq.Id);
            insert intrJunct3;
            
            delete intrJunct3;
            delete intrJunct;
            
            req.WCT_Recruiting_Coordinator__c = userInfo.getUserId();
            req.WCT_Recruiter__c = userInfo.getUserId();
            update req;
            
            WCT_Interview_Junction__c intrJunct4 = new WCT_Interview_Junction__c(WCT_Interview__c=interview.Id, WCT_Interviewer__c = user2.Id, WCT_Candidate_Tracker__c = canReq3.Id);
            insert intrJunct4;
            
            
            
        }
    }

}