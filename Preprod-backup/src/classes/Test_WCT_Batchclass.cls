@isTest (SeeAllData=false)
private class Test_WCT_Batchclass {
    static testMethod void myTest() {
        date d = System.Today();
     //   date d = Date.newInstance(t.day(),t.month(),t.year());
        List<SOP__kav> SOPList = new List<SOP__kav>();
        List<STP__kav> STPList = new List<STP__kav>();        
        List<FAQ__kav> FAQList = new List<FAQ__kav>();
        Id TestId;
        String Status;   
        STP__kav STP = new STP__kav(Title = 'Testing Article-STP',UrlName = 'Testing-Article-STP',Technology__c = 'Tech- STP'+'i',Data_Source_Link__c = 'Sharepoint link',Expiration_Date__c = d);
        FAQ__kav FAQ = new FAQ__kav(Title = 'Testing Article-FAQ',UrlName = 'Testing-Article-FAQ',Data_Source_Link__c = 'www.google.com',Expiration_Date__c = d.adddays(-1));
        SOP__kav SOP = new SOP__kav(Title = 'Testing Article-FAQ',UrlName = 'Testing-Article-SOP',Data_Source_Link__c = 'Sharepoint link - SOP',Expiration_Date__c = d.adddays(-2));            
        SOPList.add(SOP);
        FAQList.add(FAQ);            
        STPList.add(STP);                    
        insert SOPList;
        insert FAQList;
        insert STPList;
        for(STP__kav STP1: STPList){
        TestId = [select KnowledgeArticleId from KnowledgeArticleVersion where Id =: STP1.Id and PublishStatus = 'Draft' and Language = 'en_US' limit 1].KnowledgeArticleid ;
        System.debug('Status of the inserted Article is'+ TestId);
        KbManagement.PublishingService.publishArticle(TestId,True);
        Status = [select PublishStatus from KnowledgeArticleVersion where Id =: STP1.Id and PublishStatus = 'Online' and Language = 'en_US' limit 1].PublishStatus;
        System.debug('Status of the inserted Article is'+ Status);
        }
        for(FAQ__kav FAQ1: FAQList){
        TestId = [select KnowledgeArticleId from KnowledgeArticleVersion where Id =: FAQ1.Id and PublishStatus = 'Draft' and Language = 'en_US'  limit 1].KnowledgeArticleid;
        System.debug('Status of the inserted Article is'+ TestId);
        KbManagement.PublishingService.publishArticle(TestId,True);
        Status = [select PublishStatus from KnowledgeArticleVersion where Id =: FAQ1.Id and PublishStatus = 'Online' and Language = 'en_US' limit 1].PublishStatus;
        System.debug('Status of the inserted Article is'+ Status);        
//        KbManagement.PublishingService.editOnlineArticle(TestId,TRUE);     
        }
        for(SOP__kav SOP1: SOPList){
        TestId = [select KnowledgeArticleId from KnowledgeArticleVersion where Id =: SOP1.Id and PublishStatus = 'Draft' and Language = 'en_US'  limit 1].KnowledgeArticleid;
        System.debug('Status of the inserted Article is'+ TestId);
        KbManagement.PublishingService.publishArticle(TestId,True);
        Status = [select PublishStatus from KnowledgeArticleVersion where Id =: SOP1.Id and PublishStatus = 'Online' and Language = 'en_US' limit 1].PublishStatus;
        System.debug('Status of the inserted Article is'+ Status);         
//        KbManagement.PublishingService.editOnlineArticle(TestId,TRUE);     
        }       
       
        Test.StartTest();
        WCT_batchArticleExpNotify batchclass = new WCT_batchArticleExpNotify();
        ID batchprocessid = Database.executeBatch(batchclass); 
    }
}