/**
    * Class Name  : WCT_CreateCandidateTracker_Controller   
    * Description : To create Candidate Tracker for candidate 
    *               created from Phone screen 
*/
public with sharing class  WCT_CreateCandidateTracker_Controller{

//Variable Declaration
public WCT_Candidate_Requisition__c candidateReq {get;set;}
public Id reqId {get;set;}
public boolean booAdhocCand {get;set;}
public boolean booCand {get;set;}
public Contact contact {get;set;}
//Controller
public WCT_CreateCandidateTracker_Controller(){
    candidateReq = new WCT_Candidate_Requisition__c();
    booAdhocCand = false;
    booCand = false;
    contact = [SELECT Id,RecordTypeId from Contact where id =:apexpages.currentpage().getparameters().get('id')];
    if(contact.RecordTypeId == System.Label.AdhocCandidateRecordtypeID){
        booAdhocCand = true;
    }else if(contact.RecordTypeId == System.Label.CandidateRecordtypeId){
        booCand = true;
    }
}
/** 
        Method Name  : saveCandTracker   
        Return Type  : Inserts Candidate Tracker       
*/
public PageReference saveCandTracker(){
    try{
    candidateReq.WCT_Contact__c = contact.id;
    insert candidateReq;
    }catch(DMLException ex){
         Apexpages.addmessage(new Apexpages.message(Apexpages.severity.FATAL,ex.getMessage()));
         return null;
    }
    return new Pagereference('/'+contact.id);
}
//Cancel Method
public PageReference can() {
    return new PageReference('/'+contact.id);
}

}