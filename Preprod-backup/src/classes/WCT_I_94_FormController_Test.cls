/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class WCT_I_94_FormController_Test 

{
    public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        insert mob;
        I_94__c I94Record = new I_94__c();
        I94Record.WCT_Mobility__c = mob.id;
        I94Record.WCT_Employee__c = mob.WCT_Mobility_Employee__c;
        I94Record.WCT_I94_Expiration__c = mob.WCT_I_94_Expiration__c;
        insert I94Record;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef; 
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        datetime startdate=date.Today().adddays(-10);
        Test.starttest();
        //String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_I_94_Form;
        Test.setCurrentPage(pageRef); 
        //ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_I_94_FormController controller=new WCT_I_94_FormController();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_I_94_FormController();
        controller.insertorupdate();
        controller.I94Expiration=startdate.format('MM/dd/yyyy');
        controller.insertorupdate(); 
    }
}