@isTest
private class WCT_sendEmailButtonPageController_test {
    
    
    
    public static String TrCaseRecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByName().get(Label.Case_TR_EM_RecordTypeId).getRecordTypeId();
    /* **************************************************************************************
    
    Name : createContact
    Description:- To create a test contact
    
     ***************************************************************************************/
     public static Contact createContact(String lastname){
        
       Contact c = new Contact(LastName = lastname , Email=lastname+'@abc.com');
       insert c;
       return c;
           
    } 
    
    /* **************************************************************************************
    
    Name : createCase
    Description:- To create a test case
    
     ***************************************************************************************/
    public static case  createCase(String ConatctId){
       case c= new case();
      c.ContactId=ConatctId;
      c.RecordTypeId=TrCaseRecordTypeId;
      c.WCT_TRSecureSubject__c='test';
      c.WCT_TRSecureDescription__c='test';
      insert c;
      return c;
           
    }
    
    
    /* **************************************************************************************
    
    Name : createInterestedparty
    Description:- To create a test Interestedparty
    
     ***************************************************************************************/
  
    public static Interested_Party__c createInterestedparty(String ContactId, String CaseId){
        
       
      Interested_Party__c Ip= new Interested_Party__c();
      Ip.WCT_Case__c=CaseId;
      Ip.WCT_Contact__c=ContactId;
      Ip.WCT_Relationship_Type__c='Leader';
      Ip.WCT_Status__c='Active';
      insert Ip;
       return Ip;
           
    }
   
    /* **************************************************************************************
    
    Name : CancelUnitTest
    Description:- this method will test the cancel functionality
    
     ***************************************************************************************/
  
 
    static testMethod void CancelUnitTest() {
       
         list<Case> caseList = new list<Case>();
         Test_Data_Utility.createCase();
         caseList = [select Id from Case limit 1];
        
        test.setCurrentPage(page.WCT_sendEmailButtonPage);
        ApexPages.currentPage().getParameters().put('id', caseList[0].id);
        WCT_sendEmailButtonPageController SEB= new WCT_sendEmailButtonPageController();
        system.assertNotEquals(SEB.caseid,null);
        SEB.cancel();
    
    }
  
    /* **************************************************************************************
    
    Name : AttachmentUnitTest
    Description:- this method will test the add attachment functionality
    
     ***************************************************************************************/
    
    static testMethod void AttachmentUnitTest() {
       
         list<Case> caseList = new list<Case>();
         Test_Data_Utility.createCase();
         caseList = [select Id from Case limit 1];
        
        test.setCurrentPage(page.WCT_sendEmailButtonPage);
        ApexPages.currentPage().getParameters().put('id', caseList[0].id);
        WCT_sendEmailButtonPageController SEB= new WCT_sendEmailButtonPageController();
        String TestStringBody = 'StringToBlob';
        Blob TestBody = Blob.valueof(TestStringBody);
        SEB.OpenAttachmentPopup();
        SEB.cDoc.Body=TestBody;
        SEB.cDoc.Name='Test';
        SEB.save();
        system.assertNotEquals(SEB.ShowAttachmentlist.size(),0);
        SEB.CloseAttachmentPopup();
    
    }   
    
    /* **************************************************************************************
    
    Name : AddTemplateUnitTest
    Description:- this method will unit test the add Template functionality
    
     ***************************************************************************************/
    
    static testMethod void AddTemplateUnitTest() {
       
         list<Case> caseList = new list<Case>();
         Test_Data_Utility.createCase();
         caseList = [select Id from Case limit 1];
        
        test.setCurrentPage(page.WCT_sendEmailButtonPage);
        ApexPages.currentPage().getParameters().put('id', caseList[0].id);
        WCT_sendEmailButtonPageController SEB= new WCT_sendEmailButtonPageController();
        List<Selectoption> sb=SEB.Folders;
        SEB.OpenSelectTemplatePopup();
        SEB.folderid=Label.Attachment_Zip_Document_Folder_Id;
        system.assertNotEquals(SEB.folderid,null);
        SEB.Searchtemplfiles();
        list<EmailTemplate> em= [SELECT Id,Name,Body,FolderId,HtmlValue,TemplateType FROM EmailTemplate];
        String TextEmailTempId;
        for(EmailTemplate lpEm : em)
        {
            if(lpEm.TemplateType=='HTML')
            {
                SEB.sEmailTemplateID=lpEm.Id;
            }else
            {
                TextEmailTempId=lpEm.Id;
            }
        }
        SEB.SelectTemplate();
        SEB.sEmailTemplateID=TextEmailTempId; 
        SEB.SelectTemplate();
    }
     /* **************************************************************************************
    
    Name : SendEmailExceptionUnitTest
    Description:- this method will test the exceptional condition if To field is blank. 
    
     ***************************************************************************************/
    
    
    static testMethod void SendEmailExceptionUnitTest() {
       
         list<Case> caseList = new list<Case>();
         Test_Data_Utility.createCase();
         caseList = [select Id from Case limit 1];
        
        test.setCurrentPage(page.WCT_sendEmailButtonPage);
        ApexPages.currentPage().getParameters().put('id', caseList[0].id);
        WCT_sendEmailButtonPageController SEB= new WCT_sendEmailButtonPageController();
        system.assertEquals(SEB.ToFieldEmail, '');
        
        SEB.ToFieldEmail='';
        SEB.ToField='';
        SEB.ToFieldId='';
        SEB.SendEmail();
    
    } 
 
   /* **************************************************************************************
    
    Name : SendEmailSubjectExceptionUnitTest
    Description:- this method will test the exceptional condition if Subject field is blank.    
    
     ***************************************************************************************/
    static testMethod void SendEmailSubjectExceptionUnitTest() {
       
         list<Case> caseList = new list<Case>();
         Test_Data_Utility.createCase();
         caseList = [select Id,contactid from Case limit 1];
        
        test.setCurrentPage(page.WCT_sendEmailButtonPage);
        ApexPages.currentPage().getParameters().put('id', caseList[0].id);
        WCT_sendEmailButtonPageController SEB= new WCT_sendEmailButtonPageController();
        Contact testContact=createContact('testContact');
        SEB.ToFieldId=testContact.id;
        system.assertEquals(SEB.subjectLine, '');
        
        SEB.SendEmail();
        
   }
   
   /* **************************************************************************************
    
    Name : SendEmailWithoutBodyExceptionUnitTest
    Description:- this method will test the exceptional condition if body field is blank.   
    
     ***************************************************************************************/
 
   static testMethod void SendEmailWithoutBodyExceptionUnitTest() {
       
         list<Case> caseList = new list<Case>();
         Test_Data_Utility.createCase();
         caseList = [select Id,contactid from Case limit 1];
        
        test.setCurrentPage(page.WCT_sendEmailButtonPage);
        ApexPages.currentPage().getParameters().put('id', caseList[0].id);
        WCT_sendEmailButtonPageController SEB= new WCT_sendEmailButtonPageController();
        Contact testContact=createContact('testContact');
        SEB.ToFieldId=testContact.id;
        SEB.BCcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.CcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue= SEB.ShowAdditionalFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        SEB.subjectLine='Test Email';
        system.assertEquals(SEB.BodyContent, null);
        Seb.BodyContent='';
        SEB.SendEmail();
        
   }
   
   /* **************************************************************************************
    
    Name : SendEmailWithoutTemplateUnitTest
    Description:- this method will test the sending plaintext on email if template is nt selected.  
    
     ***************************************************************************************/
 
   
  static testMethod void SendEmailWithoutTemplateUnitTest() {
       
         list<Case> caseList = new list<Case>();
         Test_Data_Utility.createCase();
         caseList = [select Id,contactid from Case limit 1];
        
        test.setCurrentPage(page.WCT_sendEmailButtonPage);
        ApexPages.currentPage().getParameters().put('id', caseList[0].id);
        WCT_sendEmailButtonPageController SEB= new WCT_sendEmailButtonPageController();
        Contact testContact=createContact('testContact');
        SEB.ToFieldId=testContact.id;
        SEB.BCcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.CcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue= SEB.ShowAdditionalFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        system.assertEquals(SEB.templateId, '');
        SEB.subjectLine='Test Email';
        Seb.BodyContent='Test Email body';
        SEB.SendEmail();
        
   }   
   /* **************************************************************************************
    
    Name : SendEmailWithTemplateUnitTest
    Description:- this method will test the send the  email with template.  
    
     ***************************************************************************************/
 
   static testMethod void SendEmailWithTemplateUnitTest() {
       
         list<Case> caseList = new list<Case>();
         Test_Data_Utility.createCase();
         caseList = [select Id,contactid from Case limit 1];
         test.setCurrentPage(page.WCT_sendEmailButtonPage);
        ApexPages.currentPage().getParameters().put('id', caseList[0].id);
        WCT_sendEmailButtonPageController SEB= new WCT_sendEmailButtonPageController();
        Contact testContact=createContact('testContact');
        SEB.ToFieldId=testContact.id;
        SEB.BCcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.CcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue= SEB.ShowAdditionalFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        String fId = [Select Id from Folder where DeveloperName='Case_Folder'].Id;
        list<EmailTemplate> emTmp = [select Id,name,body,HtmlValue from EmailTemplate where FolderId =:fId Limit 1];
        SEB.templateId=emTmp[0].id;
        system.assertNotEquals(SEB.templateId, '');
        SEB.SendEmail();
   }  
    
    
       /* **************************************************************************************
    
    Name : SendEmailWithTemplateUnitTest
    Description:- this method will test the send the  email with attachment.    
    
     ***************************************************************************************/
 
    
    static testMethod void SendEmailWithAttachmentUnitTest() {
       
         list<Case> caseList = new list<Case>();
         Test_Data_Utility.createCase();
         caseList = [select Id from Case limit 1];
        
        test.setCurrentPage(page.WCT_sendEmailButtonPage);
        ApexPages.currentPage().getParameters().put('id', caseList[0].id);
        WCT_sendEmailButtonPageController SEB= new WCT_sendEmailButtonPageController();
        String TestStringBody = 'StringToBlob';
        Blob TestBody = Blob.valueof(TestStringBody);
         system.assertNotEquals(SEB.caseid ,null);
        
        SEB.OpenAttachmentPopup();
        SEB.cDoc.Body=TestBody;
        SEB.cDoc.Name='Test';
        SEB.save();
        
        SEB.CloseAttachmentPopup();
        Contact testContact=createContact('testContact');
        system.assertNotEquals(Seb.ShowAttachmentlist.size(), 0);
        SEB.ToFieldId=testContact.id;
        SEB.subjectLine='test email';
        SEB.BodyContent='test body';
        SEB.SendEmail();
    } 
    
      /* **************************************************************************************
    
    Name : SendEmailButtonUnitTest
    Description:- this method will test the send the email with attachment and template.    
    
     ***************************************************************************************/
 
    static testMethod void SendEmailButtonUnitTest() {
       
         list<Case> caseList = new list<Case>();
         Test_Data_Utility.createCase();
         caseList = [select Id from Case limit 1];
        
        test.setCurrentPage(page.WCT_sendEmailButtonPage);
        ApexPages.currentPage().getParameters().put('id', caseList[0].id);
        WCT_sendEmailButtonPageController SEB= new WCT_sendEmailButtonPageController();
        String TestStringBody = 'StringToBlob';
        Blob TestBody = Blob.valueof(TestStringBody);
        
        SEB.OpenAttachmentPopup();
        SEB.cDoc.Body=TestBody;
        SEB.cDoc.Name='Test';
        SEB.save();
        SEB.CloseAttachmentPopup();
        Contact testContact=createContact('testContact');
        system.assertEquals(SEB.caseid ,caseList[0].id);
        SEB.ToFieldId=testContact.id;
        SEB.BCcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.BCcFieldValue=SEB.BCcFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        SEB.CcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.CcFieldValue=SEB.CcFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue= SEB.ShowAdditionalFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        list<Selectoption> RelatedToField = SEB.RelatedToSelectOptionValues;
        list<Selectoption> FromField = SEB.fromAddressValues;
        String fId = [Select Id from Folder where DeveloperName='Case_Folder'].Id;
        list<EmailTemplate> emTmp = [select Id,name,body,HtmlValue from EmailTemplate where FolderId =:fId Limit 1];
        SEB.templateId=emTmp[0].id;
        SEB.SendEmail();
    } 
    
      /* **************************************************************************************
    
    Name : SendEmailTointerestedPartyUnitTest
    Description:- this method will test the send the email if redirected from send email from interested party button.  
    
     ***********************************************************************************/
    
     static testMethod void SendEmailTointerestedPartyUnitTest() {
       
         list<Case> caseList = new list<Case>();
         Test_Data_Utility.createCase();
         caseList = [select Id from Case limit 1];
        
        test.setCurrentPage(page.WCT_sendEmailButtonPage);
        ApexPages.currentPage().getParameters().put('id', caseList[0].id);
        ApexPages.currentPage().getParameters().put('ISP', 'Yes');
        Contact testContact=createContact('testContact');
        Interested_Party__c IPs=createInterestedparty(testContact.Id, caseList[0].id);
        Interested_Party__c IPs2=createInterestedparty(testContact.Id, caseList[0].id);
        WCT_sendEmailButtonPageController SEB= new WCT_sendEmailButtonPageController();
        String TestStringBody = 'StringToBlob';
        Blob TestBody = Blob.valueof(TestStringBody);
        string ispvalsue=ApexPages.currentPage().getParameters().get('ISP');
        system.assertEquals(ispvalsue,'Yes');
        SEB.OpenAttachmentPopup();
        SEB.cDoc.Body=TestBody;
        SEB.cDoc.Name='Test';
        SEB.save();
        SEB.CloseAttachmentPopup();
        SEB.ToFieldId=testContact.id;
        SEB.BCcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.BCcFieldValue=SEB.BCcFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        SEB.CcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.CcFieldValue=SEB.CcFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue= SEB.ShowAdditionalFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        list<Selectoption> RelatedToField = SEB.RelatedToSelectOptionValues;
        list<Selectoption> FromField = SEB.fromAddressValues;
        String fId = [Select Id from Folder where DeveloperName='Case_Folder'].Id;
        list<EmailTemplate> emTmp = [select Id,name,body,HtmlValue from EmailTemplate where FolderId =:fId Limit 1];
        SEB.templateId=emTmp[0].id;
        SEB.SendEmail();
    } 
    
      /* **************************************************************************************
    
    Name : TrCaseUnitTest
    Description:- this method will test the send the email if case is Tr type.  
    
     ***********************************************************************************/
    
    static testMethod void TrCaseUnitTest() {
       
        Contact con= createContact('TestContact');
        Case TestCase=createCase(con.id);
        test.setCurrentPage(page.WCT_sendEmailButtonPage);
        ApexPages.currentPage().getParameters().put('id', testCase.id);
        WCT_sendEmailButtonPageController SEB= new WCT_sendEmailButtonPageController();
        String TestStringBody = 'StringToBlob';
        Blob TestBody = Blob.valueof(TestStringBody);
        system.assertEquals(SEB.caseid, testCase.id);
        SEB.OpenAttachmentPopup();
        SEB.cDoc.Body=TestBody;
        SEB.cDoc.Name='Test';
        SEB.save();
        SEB.CloseAttachmentPopup();
        Contact testContact=createContact('testContact');
        SEB.ToFieldId=testContact.id;
        String contactName=SEB.getContactName(testContact.id, 'Name');
        SEB.BCcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.BCcFieldValue=SEB.BCcFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        SEB.CcFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.CcFieldValue=SEB.CcFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue=SEB.getContactName(testContact.id, 'Email');
        SEB.ShowAdditionalFieldValue= SEB.ShowAdditionalFieldValue+','+SEB.getContactName(testContact.id, 'Email');
        list<Selectoption> RelatedToField = SEB.RelatedToSelectOptionValues;
        list<Selectoption> FromField = SEB.fromAddressValues;
        String fId = [Select Id from Folder where DeveloperName='Case_Folder'].Id;
        list<EmailTemplate> emTmp = [select Id,name,body,HtmlValue from EmailTemplate where FolderId =:fId Limit 1];
        SEB.templateId=emTmp[0].id;
        SEB.SendEmail();
    } 

 /* **************************************************************************************
    
    Name : TrCaseUnitTest
    Description:- this method will test the send the email if user is CIC agent.    
    
     ***********************************************************************************/

    /*
    static testMethod void CICAgentSendEmailUnitTest() {
       
     Profile p = [SELECT Id FROM Profile WHERE Name=: Label.CIC_agent_Profile]; 
      User u2 = new User(Alias = 'CICent', Email='CICent@WCTTest.com', 
         EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
         LocaleSidKey='en_US', ProfileId = p.Id, 
         TimeZoneSidKey='America/Los_Angeles', UserName='CICent@WCTTest.com');
        Contact con= createContact('TestContact');
        System.runAs(u2){
        system.assertEquals(Userinfo.getProfileId(), p.id);
        Case TestCase=createCase(con.id);
        System.debug(TestCase);
        test.setCurrentPage(page.WCT_sendEmailButtonPage);
        ApexPages.currentPage().getParameters().put('id', testCase.id);
        WCT_sendEmailButtonPageController SEB= new WCT_sendEmailButtonPageController();
        list<Selectoption> FromField = SEB.fromAddressValues;
        
         }
    } 
    */


}