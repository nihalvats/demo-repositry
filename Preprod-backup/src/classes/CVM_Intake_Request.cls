/*

Project : VRM Contractor Management
Associated VF Page : CVM Intake Request
Description:Controller which allows requestors to submit Requests for contractors
Test Class : CVM_Class_Test
Author : Karthik Raju Gollapalli

*/

@TestVisible
public with sharing class CVM_Intake_Request  extends SitesTodHeaderController{



    public String getRenderStaff() {
        return null;
    }

    // UPDATE BELOW VALUES BASED ON DEV OR QA OR UAT Version
    
    public string Request_Page='CVM_InTake_Request';    
    public string TD_Request_Page='CVM_TD_VENDOR_Form';
    public string Advisory_Page = 'CVM_ADVISORY';
    
    //Variables for Value comparisons and Input
    public string strings{get;set;}
    public string stringscopy{get;set;}
    public string requesttype{get;set;}
    public boolean formnameexists{get;set;}
    public string titlename{get;set;}
    public string vendorcontracttype{get;set;}
    public boolean ismissingReceiptsRequest{get; set;}
    public boolean LastName {get{
        
        return Site.getBaseUrl()=='';
    } set;}

    public string redirectparam{get;set;}
    public string param1{get;set;}
    public string fieldsetvar{get;set;}
    public List<Contact> Contractors{get;set;}
    public contact conObj{get;set;}
    public Contact ContractorInstance{get;set;}
    public boolean bCreateRecords{get;set;}
    public integer curEditingIndex{get;set;}
    public String isStaff{get;set;}
    public boolean renderStaff{get; set;}
    public string requestvar{get;set;}
    
    /*Boolean to handle the render and rerender of thr popup, to avoid validation getting triggered in VF page */
    public boolean addOrEditContr{get; set;}
    
    public boolean isEditing{get{
       system.debug('Getting isEditing'+isEditing); 
        return isEditing;
    } set;}
    public boolean displayPopup {get; set;}
    public String amar {get; set;}
    public String HiddenVal {get;set;}
    public CVM_Contractor_Request__c TDRequestInstance{get;set;}
    public CVM_Contractor_Request__c cvmObj{get;set;}
    

    /* Cont Rec */
    
    //Attachments
    public GBL_Attachments attachmentHelper{get; set;}
    
    public blob jobDescFile{get; set;}
    public String jobDescFileName{get; set;}
    public blob internAttachmentFile{get; set;}
    public String internAttachmentName{get; set;}
    
    
    
    //Attachments for Resume
    public blob ResumeFile{get; set;}
    public String ResumeFileName{get; set;}
    
    
    //Attachments ofr ADRS
    public String ADRSFileName{get; set;}
    public blob ADRSFile{get; set;}
    
    
    //Attachments for NVTFile
    public blob NVTFile{get; set;}
    public String NVTFileName{get; set;}
    
    //Attachments for TD Form
    public String ProposalFileName{get;set;}
    public blob ProposalFile{get; set;}
    
    public List<String> docIdList = new List<string>();
    
    public boolean addAttachment{get; set;}
    public string setpopdisplay{get;set;}
    public List<Document> selectedDocumentList {get;set;}
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    
    wrapper objwrapper;
    public String appId;
    
    
    List < wrapper > lstwrapper = new List < wrapper > ();
    public boolean newRec;
    public integer rowIndex{get;set;}
    public Integer counter{get;set;}
    public List <wrapper> lst {get;set;}
    public string formname{get;set;}
    
    public string currentDate {get{
    
    return Date.today().format();
    } set; }
    
    public integer contractorSize{get{
     
        if(Contractors!=null)
        {
        return Contractors.size();
        }
        return 0;
   
    } set;}
    
    public CVM_Intake_Request()
    {
        try
        {
        //emValue = ApexPages.currentPage().getParameters().get('em');
        //string userEmail = cryptoHelper.decrypt(emValue);
        string userEmail = loggedInContact.email;
        System.debug('LOGGED CONTACT IS :'+userEmail);
        conObj=new contact();
        attachmentHelper= new GBL_Attachments();
        ContractorInstance = new Contact();
        ContractorInstance.RecordTypeID=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Contractor').getRecordTypeId();
        Contractors = new List<Contact>();
        bCreateRecords=false;
        fieldsetvar='TD_Vendor_Request';
        isEditing=false;
        addOrEditContr=true;
        formname ='';
        renderStaff=false;               
        TDRequestInstance = new CVM_Contractor_Request__c();
            /* Query Contact Information and populate basic information with data */
            conObj = [select id,name, email from contact where email = : userEmail limit 1];
            if(conObj!=null)
            {
                cvmObj= new CVM_Contractor_Request__c();
                
                //caseObj = new Case();
                //caseObj.ContactId = conObj.id;
                cvmObj.CVM_Requested_By__c=conObj.id;
                cvmObj.CVM_Requestor_Name__c = conObj.name;
                cvmObj.CVM_Request_Date__c= date.today();
                cvmObj.CVM_Request_Type1__c = strings;
                cvmObj.CVM_Request_Type__c = requestvar;
            }
            
            /* Cont Rec*/
            newRec = false;
            
            lst = new list < wrapper > ();
            counter = 0;   
             getfields();
        }
        
        catch(Exception e)
        {
            Exception_Log__c error = WCT_ExceptionUtility.logException('CVM_Intake_Request','CVM_InTake_Request',e.getMessage());
            //Exception_Log__c error = new Exception_Log__c();
            //error.className__c = 
            PageReference errorPageRef = Page.GBL_Page_Notification;
            //errorPageRef.getParameters().put('errorid',ApexPages.currentPage().getParameters().get('em'));
            //errorPageRef.getParameters().put('errorid',ApexPages.currentPage().getParameters().put('errorid'));
            errorPageRef.getParameters().put('key','cvm');
            errorPageRef.getParameters().put('errorid',error.Name);
            errorPageRef.setRedirect(true);
            //return errorPageRef;
            System.debug('----Exception---'+e.getMessage()+'-----at Line #----'+e.getLineNumber());
        } 
    }
    
    
    /* Action Function to retrieve record type and fieldset */
    Public void getfields()
    {
        try
        {
        System.debug(strings);
        if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('Param1')))
        {
          
           strings = apexpages.currentpage().getparameters().get('Param1');
           formname=strings ;
           strings = EncodingUtil.urlDecode(strings , 'UTF-8');
          // ocaseformextn.TDR_Request_Type__c = requesttype ;   
           
           if(strings != 'None')
           {
               strings =strings.replace(' ','_').replace('(' , '').replace(')' , '').replace(',' , '_').replace('-' , '_').replace('/' , '_').replace('.' , '');
           }
           
           if(strings.length() > 41)
           {
               
             strings = strings.substring(0,40);
             system.debug('@@@@@@@@@@@@' + strings  );
             if(strings.endsWith('_'))
             {
              strings = strings.removeEnd('_');               
             }
           }         
       System.debug(strings); 
       updateRecordType();
        }
       }
       
       catch(Exception e)
        {
            Exception_Log__c error = WCT_ExceptionUtility.logException('CVM_Intake_Request','CVM_InTake_Request',e.getMessage());
            //Exception_Log__c error = new Exception_Log__c();
            //error.className__c = 
            PageReference errorPageRef = Page.GBL_Page_Notification;
            //errorPageRef.getParameters().put('errorid',ApexPages.currentPage().getParameters().get('em'));
            //errorPageRef.getParameters().put('errorid',ApexPages.currentPage().getParameters().put('errorid'));
            errorPageRef.getParameters().put('key','cvm');
            errorPageRef.getParameters().put('errorid',error.Name);
            errorPageRef.setRedirect(true);
            //return errorPageRef;
            //System.debug('----Exception---'+e.getMessage()+'-----at Line #----'+e.getLineNumber());
        } 
        
      }  
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  
    /* Add Contractors to the Request */
    public PageReference createContractor1()
    {    

     Contact temp =ContractorInstance.clone(false, true, false, false);
       
      System.debug('createContractor called'+temp);
     if(isEditing)
     {
         if(curEditingIndex!=null)
         {
              Contractors.set(curEditingIndex, temp);
         }
     }
     else
     {
          Contractors.add(temp);
     }
     clearPopup();
     system.debug('## COntractor Added'+temp+':: whats supplied'+Contractors);
     return null;     
    }
    
    public void clearPopup()
    {
        ContractorInstance= new Contact();  
        isEditing=false;
        curEditingIndex=null;
    }
    public void closePopup()
    {
        displayPopup = false;
        amar = HiddenVal;
    }
    
    public void showPopup()
    {
        HiddenVal ='';
        displayPopup = true;
    }
    
    /* Action Function to Edit or delete a contractor from the request */
    public PageReference manageContractor()
    {
        System.debug('### manageContractor ');
        integer action=Integer.valueof(apexpages.currentpage().getparameters().get('action'));
        integer index=Integer.valueof(apexpages.currentpage().getparameters().get('rowNo'));
        System.debug('### manageContractor '+action+' :: '+index);
        if(action==1)
        {
            Contractors.remove(index);
        }
        else if(action==0)
        {
            ContractorInstance=Contractors.get(index).clone(false,true,false,false);
            
            curEditingIndex=index;
            isEditing=true;
        }
        System.debug('### manageContractor ContractorInstance '+ContractorInstance);
         System.debug('### manageContractor isEditing '+isEditing+' :: curEditingIndex '+curEditingIndex);
        return null;
    }
    
    public void SetContractorRecord()
    {
        if(String.IsNotEmpty(apexpages.currentpage().getparameters().get('bCreateRecords')))
        {
            string IsCreateContractRecs=apexpages.currentpage().getparameters().get('bCreateRecords');
            if(IsCreateContractRecs=='true')
            {
                bCreateRecords=true;
                AddRow();  
            }
            else{
                bCreateRecords=false;
                lst=new List<wrapper>();
            }
        }
    }
    
    /*
* @desc : Action method to update the record type based on the function string selected.

*/
    public PageReference updateRecordType()
    {
        try
        {
        /*Get the Record Type id from the custom Setting we created. */
         System.debug('# URL '+ApexPages.currentPage().getUrl());                  
        if(strings == 'TD_Vendor_Request' )
        {
           
            String pageURL = Site.getBaseUrl()==''?'/apex/'+TD_Request_Page:'/tod/'+TD_Request_Page;
            PageReference pageRef= new PageReference(pageURL);
            //pageRef.getParameters().put('em',ApexPages.currentPage().getParameters().get('em'));
            pageRef.getParameters().put('param1',strings);
            pageRef.setredirect(true);
            return pageRef;
        }
        if(strings == 'Advisory_Requests' )
        {
           
            String pageURL = Site.getBaseUrl()==''?'/apex/'+Advisory_Page:'/tod/'+Advisory_Page;
            PageReference pageRef= new PageReference(pageURL);
            //pageRef.getParameters().put('em',ApexPages.currentPage().getParameters().get('em'));
            pageRef.getParameters().put('param1',strings);
            Map<String, RecordType_Function_Mapping__c> recordTypeMappings= RecordType_Function_Mapping__c.getAll();
            if(recordTypeMappings.get(strings)!=null)
            cvmObj.RecordTypeId=recordTypeMappings.get(strings).Record_Type_Id__c;
            pageRef.setredirect(true);
            return pageRef;
        }
        if(strings!=null || strings!='None')
        {   
            Map<String, RecordType_Function_Mapping__c> recordTypeMappings= RecordType_Function_Mapping__c.getAll();
            if(recordTypeMappings.get(strings)!=null)
            cvmObj.RecordTypeId=recordTypeMappings.get(strings).Record_Type_Id__c;
        }
        else
        {
            
            cvmObj.RecordTypeId=null;
        }
        }
        catch(Exception e)
        {
            Exception_Log__c error = WCT_ExceptionUtility.logException('CVM_Intake_Request','CVM_InTake_Request',e.getMessage());
            //Exception_Log__c error = new Exception_Log__c();
            //error.className__c = 
            PageReference errorPageRef = Page.GBL_Page_Notification;
            //errorPageRef.getParameters().put('errorid',ApexPages.currentPage().getParameters().get('em'));
            //errorPageRef.getParameters().put('errorid',ApexPages.currentPage().getParameters().put('errorid'));
            errorPageRef.getParameters().put('key','cvm');
            errorPageRef.getParameters().put('errorid',error.Name);
            errorPageRef.setRedirect(true);
            //return errorPageRef;
            //System.debug('----Exception---'+e.getMessage()+'-----at Line #----'+e.getLineNumber());
        }
        return null;
    }
    
    /* Action function to save the request */
    public PageReference saveAction() 
    {
        System.debug('#### saveAction');
        try
        {
         /* automate change of request status to new  
            if(strings=='CL_Preferred_Vendor_Program'){
             cvmObj.CVM_Request_Status__c='New';
            }
            else{
            cvmObj.CVM_Request_Status__c='';
            }*/
            cvmObj.CVM_Request_Status__c='New';
            String selectedFucnion = strings.replace('_',' ');
       
            stringscopy = strings;
            
        
            cvmObj.CVM_Request_Type1__c = stringscopy.replace('_',' ');
            upsert cvmObj;
            
            /*Attachements to create */
            List<Attachment> attachmentsToCreate= new List<Attachment>();
            if(ADRSFile!=null && ADRSFileName!=null)
            {
                    Attachment attachADRS = new Attachment();
                    attachADRS.body=ADRSFile;
                    attachADRS.name=ADRSFileName;
                    attachADRS.parentId=cvmObj.id;                    
                    attachmentsToCreate.add(attachADRS);
            }
            
            
            if(NVTFile!=null && NVTFileName!=null)
                {
                    Attachment attachNVT = new Attachment();
                    attachNVT.body=NVTFile;
                    attachNVT.name=NVTFileName;
                    attachNVT.parentId=cvmObj.id;
                    
                    attachmentsToCreate.add(attachNVT);
                    //insert attachNVT;
                }           
           
           if(ResumeFile!=null && ResumeFileName!=null)
                {
                    Attachment attachresume = new Attachment();
                    attachresume.body=ResumeFile;
                    attachresume.name=ResumeFileName;
                    attachresume.parentId=cvmObj.id;
                        attachmentsToCreate.add(attachresume);
                   // insert attachresume;
                }
            
            if(cvmObj.CVM_Vendor_Contract__c == 'Work_Order_SOW_Review' || cvmObj.CVM_Request_Type__c == 'Speaker_Facilitator' ||  cvmObj.CVM_Vendor_Contract__c == 'New_Vendor_Contract_Agreement_Amendment_Renewal' || cvmObj.CVM_Vendor_Contract__c == 'Product_License_Vouchers' || cvmObj.CVM_Vendor_Contract__c == 'Miscellaneous_Requests')
           {
                /*Attaching the uploaded document*/
                
                if(ProposalFile!=null && ProposalFileName!=null)
                {
                    System.debug('Proposal Attachment entered');
                    Attachment attachTDProposal = new Attachment();
                    attachTDProposal.body = ProposalFile;
                    attachTDProposal.name = ProposalFileName;
                    attachTDProposal.parentId = cvmObj.id;
                    attachmentsToCreate.add(attachTDProposal);
                   // insert attachTDProposal;
                }
           }
            
           if(cvmObj.CVM_Request_Type1__c=='Campus Unpaid Intern')
           {
                if(internAttachmentName!=''&& internAttachmentName!=null)
                {
                   Attachment unpaidAttachment = new Attachment();
                    unpaidAttachment.body = internAttachmentFile;
                    unpaidAttachment.name = internAttachmentName;
                    unpaidAttachment.parentId = cvmObj.id;
                    attachmentsToCreate.add(unpaidAttachment);
                }
           }
            
            System.debug('Attachments'+attachmentsToCreate);
            if(attachmentsToCreate.size()>0)
            {
                insert attachmentsToCreate;
                
            }

           if( cvmObj.CVM_Do_you_need_assistance_in_finding_id__c=='No')
           {
           
            if(jobDescFile!=null && jobDescFileName!=null)
                {
                    Attachment attach = new Attachment();
                    attach.body=jobDescFile;
                    attach.name=jobDescFileName;
                    attach.parentId=cvmObj.id;
                    
                    insert attach;
                }
            
            system.debug('---------Contractors---------'+Contractors);
            for(Contact contactemp: Contractors)
                {

                    contactemp.CVM_Request_ID__c = cvmObj.id;
                    contactemp.recordTypeId=System.label.ContractorRecordType;
                    contactemp.LastName=contactemp.WCT_Personnel_Area_Code__c;
                }
                if(Contractors.size()>0)
                {
                    insert Contractors;
                }
           }
           else if( cvmObj.CVM_Do_you_need_assistance_in_finding_id__c=='Yes')
           {
                /*Attaching the uploaded document*/
                
                if(jobDescFile!=null && jobDescFileName!=null)
                {
                    Attachment attach = new Attachment();
                    attach.body=jobDescFile;
                    attach.name=jobDescFileName;
                    attach.parentId=cvmObj.id;
                    
                    insert attach;
                }
           
           }
           
            /*Handling Campus Contact*/
           
           if(strings=='Campus_Unpaid_Intern')
           {
               Contact CampusContact = new contact();
               CampusContact.CVM_Request_ID__c = cvmObj.id;
               CampusContact.recordTypeId = System.label.ContractorRecordType;
               CampusContact.FirstName = cvmObj.CVM_Students_First_Name__c;
               CampusContact.LastName= cvmObj.CVM_Students_Last_Name__c;
               CampusContact.Email = cvmObj.CVM_Students_Email_Address__c;
               CampusContact.Phone = cvmObj.CVM_Students_Phone_Number__c;
               insert CampusContact;   
               System.debug('THE ID ERROR IS HERE');           
           } 
        }
        
        
            
        
        catch(Exception e)
        {
            Exception_Log__c error = WCT_ExceptionUtility.logException('CVM_Intake_Request','CVM_InTake_Request',e.getMessage());
            //Exception_Log__c error = new Exception_Log__c();
            //error.className__c = 
            PageReference errorPageRef = Page.GBL_Page_Notification;
            errorPageRef.getParameters().put('key','cvm');
            errorPageRef.getParameters().put('errorid',error.Name);
            errorPageRef.setRedirect(true);
            return errorPageRef;
            //System.debug('----Exception---'+e.getMessage()+'-----at Line #----'+e.getLineNumber());
            
        }
        
        PageReference objPageRef = Page.GBL_Page_Notification;
        objPageRef.getParameters().put('key','cvmthankyou');
        objPageRef.getParameters().put('label','My Requests');
        objPageRef.getParameters().put('url','/apex/CVM_My_Request');
        //objPageRef.getParameters().put('em',ApexPages.currentPage().getParameters().get('em'));
        objPageRef.setRedirect(true); 
        return objPageRef;   
        
    }
    
    /* Action function to navigate to MyRequests page */
    public Pagereference gotorequestpage()
    {    
        system.debug('## gotorequestpage');
        PageReference MyRequestsPageRef = Page.CVM_My_Request;
        //MyRequestsPageRef.getParameters().put('em',ApexPages.currentPage().getParameters().get('em'));
        MyRequestsPageRef.setRedirect(true);
        return MyRequestsPageRef;
    }
    
    
    public pagereference delmethod() {
        if(rowIndex!=null){
            try{
                list < wrapper > lstWrapClone = new list < wrapper > ();
                
                lst.remove(rowIndex);
                lstWrapClone=lst;
                lst = new list < wrapper > ();
                integer lp=0;
                for(wrapper objWrap:lstWrapClone){
                    objWrap.rowNo=lp;
                    lst.add(objWrap);
                    lp++;
                }
            }
            catch(Exception ex){
            }
        }
        return null;
    }
    
    
    public PageReference AddRow() {
        
        objwrapper = new wrapper(new Contact());
        counter++;
        objwrapper.counterWrap = counter;
        objwrapper.isEdit = true;
        objwrapper.rowNo = lst.size();
        lst.add(objwrapper);
        newRec = true;
        return null;
    }
    
    public class wrapper {
        public Contact contractor {get;set;}
        public integer rowNo {get;set;}
        public boolean isEdit{get;set;}
        public Integer counterWrap{get;set;}
        public wrapper(Contact contractor) {
            this.contractor = contractor;
            
        }
    }
   
   /* REdirect to request page */
   public pagereference redirect()
    {
        String pageURL = Site.getBaseUrl()==''?'/apex/'+Request_Page:'/tod/'+Request_Page;
    PageReference pageRef= new PageReference(pageURL);
    //pageRef.getParameters().put('em',ApexPages.currentPage().getParameters().get('em'));
    pageRef.getParameters().put('param1',strings);
    pageRef.setredirect(true);
    return pageRef;
    }
   
   /* Open Service Area Advisor page in new tab */
   public pagereference advisor()
    {
    PageReference pageRef= Page.CVM_Service_Area_Deployment_Advisor;
    pageRef.setredirect(true);
    return pageRef;
    }
    
    /* Navigate to Audit Request */
    public pagereference NavigateAudit()
    {
    PageReference pageRef= new PageReference('/apex/cvm_request_clone');
    //pageRef.getParameters().put('em',ApexPages.currentPage().getParameters().get('em'));
    pageRef.setredirect(true);
    return pageRef;
    }
    
    /* Navigate to Advisory Page */
    public pagereference NavigateAdvisory()
    {
    PageReference pageRef= new PageReference('/apex/CVM_Advisory_Request');
    //pageRef.getParameters().put('em',ApexPages.currentPage().getParameters().get('em'));
    pageRef.setredirect(true);
    return pageRef;
    }
    
    /* Code for TD Request form */
    public void TDSubmit()
    {
        TDRequestInstance = new CVM_Contractor_Request__c();
        
        insert TDRequestInstance;
    }    
    
    /* Update Record Type for TD Vendor Request */
    public void TDupdateRecordType()
    {
        /*Get the Record Type id from the custom Setting we created. */
        if (cvmObj.CVM_Request_Type__c== 'Staff_Aug')
        {
            
            renderStaff=true;
        }
        else
        {
            renderStaff=false;
        }
        
        Map<String, RecordType_Function_Mapping__c> recordTypeMappings= RecordType_Function_Mapping__c.getAll();
            System.debug(strings);
            if(recordTypeMappings.get(strings)!=null)
            cvmObj.RecordTypeId=recordTypeMappings.get(strings).Record_Type_Id__c;
        
    }

    
}