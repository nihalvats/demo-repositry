/********************************************************************************************************************************
Apex class         : User Deactivation 
Description        : Deactivating User who are inactive
Type               : Batch
Test Class         : userDeactivation_test

*Version         Developer                   Date          Code Coverage              Case/Req #        	     Description     
* -----------------------------------------------------------------------------------------------------------------------------------            
* 01             Abhisek                   5/9/2016          91%                                    		  Original Version
************************************************************************************************************************************/

Global class UserDeactivation implements Database.batchable<sObject>,Database.Stateful {
    
    public string query{get; set;} //passing the query from the scheduler
    List<user> updateduserid=new List<user>(); //holds the inactive users id
    List<Exception_Log__c> excp=new List<Exception_Log__c>();//holds the exception log records
    global integer usersupd=0;//holds the count of processed users
    global boolean DeActivation=false;
    
    public  UserDeactivation(String tempquery)
    {
        query=tempquery;
        
    }
    //Querying all the active users
    global Database.QueryLocator start(Database.BatchableContext BC)        
    {
         string logs=System.Label.User_Mangement_Report;
         list<string> strs=new List<string>();
        if(logs != null){
           strs =logs.split(','); 
            }
        if(strs.size()>0){
        for(string s:strs){
            if(s=='DeActivation'){
                DeActivation=true;
            }
            else{
              DeActivation=false;   
            }
        }
        }
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<user> UList) {
        set<string> emails= new set<string>();//Adding emails of all active users to a set
        List<user> upduser=new List<user>();//holds the deactivated users list
        
        //holds the record type id of exception object to log the success
        Schema.DescribeSObjectResult Cas = Exception_Log__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames = Cas.getRecordTypeInfosByName();// getting the record Type Info
        Id userlogid =rtMapByNames.get('User Management').getRecordTypeId();//particular RecordId by  Name
        //holds the record type id of exception object to log the failures.
        Schema.DescribeSObjectResult Cas1 = Exception_Log__c.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames1 = Cas1.getRecordTypeInfosByName();// getting the record Type Info
        Id exceptionid =rtMapByNames1.get('Exception').getRecordTypeId();//particular RecordId by  Name
        List<Id> count=new List<id>();
        for(User ct : UList)  
        {
            if(ct.Email!=null){
                emails.add(ct.Email);
            }
        } 
        Map<string,contact>cmap =new Map<string,contact>();
        // Adding the email of active contacts whose email matches to the email of active users to a map
        for(contact ctcList:[select Id,Email,WCT_Contact_Type__c,WCT_Employee_Status__c,AR_Deloitte_Email__c from Contact where (Email in :emails or AR_Deloitte_Email__c in :emails)  and WCT_Contact_Type__c= 'Employee' and WCT_Employee_Status__c='Active'])
        {               
            cmap.put(ctcList.Email,ctcList);
            cmap.put(ctcList.AR_Deloitte_Email__c,ctcList);
        }
        
        for(User u:UList)
        {
            //IF user email is not present in active contacts then deactivating the user
            if(!cmap.containsKey(u.Email))
            {
               
                if(u.Usertype !='Guest'){
                u.isActive=false;
                upduser.add(u);
                
                }
            }                    
        }
        System.debug('#### User about to be deactivated'+upduser);
        if (upduser.size()>0){
            System.debug('Updating the existing user');
            Database.SaveResult[] upuserList = Database.update(upduser, false);
            for (Integer i=0;i<upuserList.size();i++) {
                if(upuserList.get(i).isSuccess()){
                    updateduserid.add(upduser.get(i));
                    count.add(upuserList.get(i).getId());
                  if(DeActivation==true){
                excp.add(New Exception_Log__c(Exception_Date_and_Time__c=system.now(),Action__c='User Deactivation',User_Description__c='De-activate user'+upduser.get(i).Email,RecordTypeId= userlogid,Running_User__c = UserInfo.getUserId(),Status__c='Success') );    
                }  
                }
                else {
                    for (Database.Error err : upuserList.get(i).getErrors()){
                        string msg='Error while de-activating the user : '+upduser.get(i) +':'+ err.getStatusCode() + ' :' + err.getMessage();
                        // system.debug('error is'+msg);
                        excp.add(new Exception_Log__c(className__c='User de-activation',pageName__c='Batch Class',Detailed_Exception__c=msg,Exception_Date_and_Time__c=system.now(),Running_User__c = UserInfo.getUserId(),RecordTypeId= exceptionid));
                    }
                }
            }
        }
        //count of the number of users that are updated by the batch
        usersupd=usersupd+count.size();
    }
    global void finish(Database.BatchableContext BC){   
       
       
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
                          TotalJobItems, CreatedBy.Email, ExtendedStatus
                          from AsyncApexJob where Id =:BC.getJobId()];   
        //sending the email
        string sendemail=System.Label.User_Management_Emails;
        List<string> Sending=new List<String>();
        if(sendemail != null){
          Sending= sendemail.split(','); 
        }
        string td1='"border:1px solid green; width=200px;"';
        string td2='"width=200px; border:1px solid green; background-color:red; color:white; font-weight:bold;"';
        string tdHead='"border:1px solid green; width=200px; color:white; background-color:green; font-weight:bold;"';
        string htmlBody = '<div style="border:2px solid green; border-radius:15px;"><p>Hi,</p><p><span style="color:brown; font-weight:bolder;">Salesforce</span> completed running <b>Apex Batch Code</b>.</p>'
            +'<p>The batch Apex job processed '+ a.TotalJobItems +
            ' batches with '+ a.NumberOfErrors + ' failures.</p>'
            +'<p>The  users information is below:</p>'+
            '<p>The number of  users deactivated are '+ usersupd+'</p>'+
            
            '<p>The detailed information about the De-activated users is below:</p>'
            +'<center><table style="border:3px solid green; border-collapse:collapse;">'
            +'<th style='+tdHead+'>username</th><th style='+tdHead+'>Email</th>';
        for (User usr : updateduserid)
        {
            htmlbody +='<tr><td style='+td1+'>'+usr.email+'</td></tr>'; 
        }
        htmlbody +='</table></centre></div>';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        // Sending an email
       // String[] toAddresses = new String[] {'amadgikar@deloitte.com','swchundu@deloitte.com'};
       
        mail.setToAddresses(Sending);
        mail.setSenderDisplayName('License management Batch Process');
        mail.setSubject('User Deactivation batch class Status: ' + a.Status);
        mail.setHtmlBody(htmlBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
         //inserting the exceptions into exceptions log
        database.executeBatch(new User_Login_Batch(excp)); 
    }
}