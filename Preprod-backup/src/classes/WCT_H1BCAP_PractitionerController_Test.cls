@isTest

public class WCT_H1BCAP_PractitionerController_Test {


static testMethod void myUnitTest() {
    
   
   		Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SM']; 
          User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','test@deloitte.com');
          insert platuser;
        
          
          system.runas(platuser){
          
           Account acc1 = new Account();
            acc1.Name='Test';
            Insert acc1;
            
           Recordtype rt=[select id from Recordtype where DeveloperName = 'WCT_Employee'];
           Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
           con.Accountid = acc1.id;
           con.Email = 'abc@gmail.com';
           insert con;
 
       
           
           WCT_H1BCAP__c capobj= new WCT_H1BCAP__c();
           capobj.Current_FY_Nomination__c = true;
           capobj.WCT_H1BCAP_Email_ID__c ='test@gmail.com';
           capobj.WCT_H1BCAP_Practitioner_Name__c =con.id;
           capobj.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
           capobj.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'TestRM@deloitte.com';
           insert capobj;
        Test.startTest();   
           PageReference pageRef = Page.WCT_H1BCAP_Professional_Link;
           pageRef.getParameters().put('Id',capobj.id);
          Test.setCurrentPageReference(pageRef); 
    
        system.debug('ID*****'+ApexPages.currentPage().getParameters().get('Id'));
                                                  
        WCT_H1BCAP_PractitionerController pracobj = new WCT_H1BCAP_PractitionerController(); 
           pracobj.updateRecord();
         
             
    Test.stopTest();

    }
 }   
   
    
        static testMethod void myUnitTest1() {
        
   
          Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SM']; 
          User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','test@deloitte.com');
          insert platuser;
        
          
          system.runas(platuser){
          
           Account acc1 = new Account();
            acc1.Name='Test';
            Insert acc1;
          
           Recordtype rt=[select id from Recordtype where DeveloperName = 'WCT_Employee'];
           Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
           con.Email ='test@deloitte.com';
           con.Accountid= acc1.id;
           insert con;
           
           WCT_H1BCAP__c capobj= new WCT_H1BCAP__c();
           capobj.Current_FY_Nomination__c = true;
           capobj.WCT_H1BCAP_Email_ID__c ='test@deloitte.com';
           capobj.WCT_H1BCAP_Practitioner_Name__c =con.id;
           capobj.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
           capobj.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'TestRM@deloitte.com';
           capobj.WCT_H1BCAP_Status__c  = '';
           insert capobj;
        system.debug('capobj****'+capobj);  
        Date d = system.today().adddays(-2); 
        Test.setCreatedDate(capobj.Id, d);
      
      
    WCT_H1BCAP__c lst = [SELECT id ,WCT_H1BCAP_Email_ID__c,WCT_H1BCAP_Practitioner_Name__c from WCT_H1BCAP__c where id =: capobj.id limit 1 ];
      lst.WCT_H1BCAP_Practitioner_Comments__c = 'Test'; 
      lst.WCT_H1BCAP_Willingness_Options__c = 'Willing to Travel';
      lst.WCT_H1BCAP_Willingness_Status__c = true;  
      lst.WCT_H1BCAP_Willing_to_Travel__c = 'Less than 4 months';
      
      update lst;
     
     
        Test.startTest();
       
        PageReference pageRef = Page.WCT_H1BCAP_Professional_Link;
        pageRef.getParameters().put('Id',capobj.id);
        Test.setCurrentPageReference(pageRef); 
    
        system.debug('ID*****'+ApexPages.currentPage().getParameters().get('Id'));
                                                  
        WCT_H1BCAP_PractitionerController pracobj = new WCT_H1BCAP_PractitionerController();
      
       /*
           pracobj.practitionerRecord.WCT_H1BCAP_Practitioner_Comments__c = 'Test';       
           pracobj.practitionerRecord.WCT_H1BCAP_Willingness_Options__c = 'Willing to Travel';
           pracobj.practitionerRecord.WCT_H1BCAP_Willingness_Status__c = true;   
           pracobj.practitionerRecord.WCT_H1BCAP_Willing_to_Travel__c = 'Less than 4 months'; 
       */    
           pracobj.UpdateRecord();
           
          // pracobj.pageError = true;
        Test.stopTest();
    }   
   }  
   
   
   static testMethod void myUnitTest2() {
    
   
   		Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SM']; 
          User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','test@deloitte.com');
          insert platuser;
        
          
          system.runas(platuser){
          
           Account acc1 = new Account();
            acc1.Name='Test';
            Insert acc1;
            
           Recordtype rt=[select id from Recordtype where DeveloperName = 'WCT_Employee'];
           Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
           con.Accountid = acc1.id;
           con.Email = 'abc@gmail.com';
           insert con;
 
       
           
          
        Test.startTest();   
           PageReference pageRef = Page.WCT_H1BCAP_Professional_Link;
           
          Test.setCurrentPageReference(pageRef); 
    
        system.debug('ID*****'+ApexPages.currentPage().getParameters().get('Id'));
                                                  
        WCT_H1BCAP_PractitionerController pracobj = new WCT_H1BCAP_PractitionerController(); 
           pracobj.updateRecord();
         
             
    Test.stopTest();

    }
 }   
   
        
}