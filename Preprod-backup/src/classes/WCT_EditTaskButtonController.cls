/*****************************************************************************************
    Name    : WCT_EditTaskButtonController
    Desc    : WCT_EditTaskButtonController class for multiple tasks editing on open activity realted list in case details page.
    Approach: 
                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
Deloitte          11/26/2013 3:51 AM       Created 
Deloitte          12/21/2013 11:53 AM       Modified

******************************************************************************************/
public class WCT_EditTaskButtonController {


    // public variable declaration.
    // ******************************************************************************************************************************

    public list < cTask > cTaskList {
        get;
        set;
    }
public  String sCaseId = System.currentPageReference().getParameters().get(WCT_UtilConstants.WCT_EditTaskString_caseId); // Retriving the case ID from  the Url
public  String sTaskIds = System.currentPageReference().getParameters().get(WCT_UtilConstants.WCT_EditTaskString_TaskIDs); // Retriving the case IDs from  the Url
public  String sReturnURL= System.currentPageReference().getParameters().get(WCT_UtilConstants.WCT_EditTaskString_ret); // Retriving the Activity list view IDs from  the Url
    String sUserID = UserInfo.getUserId(); // Retriving user id 
    public list < cTask > Temptsk = new list < cTask > ();
    public Integer iCounter = 0;
    public list < cTask > cNewTask {
        get;
        set;
    }
    public list < cTask > cTrTask {
        get;
        set;
    }
    
    public boolean sError {
        get;
        set;
    }
    public String sErr {
        get;
        set;
    }

   public String PreBiRecordID { get; set; }
   public boolean TrtaskSection{get;set;}

    // ******************************************************************************************************************************


    //Cancel method to provide the cancel functionality on the edit page. This method will redirect user to the case detail page.
    // ******************************************************************************************************************************
    public PageReference cancel() {
     
     if(sCaseId!=null){
        PageReference pageRef = new PageReference('/' + sCaseId);
        pageRef.setRedirect(true); //Redirecting user to the case detail page. 
        return pageRef;
       }else
       {
       	PageReference pageRef = new PageReference(WCT_UtilConstants.WCT_EditTaskStringActivityPageRtURL + sReturnURL);
        
        pageRef.setRedirect(true); //Redirecting user to the case detail page. 
        return pageRef;
       } 
    }
    // Add row method is user click on new task button to add task
    //******************************************************************************************************************************

    public void addRow() {
        if (iCounter == 0) {
            task taskToadd = new task();
            taskToadd.OwnerID = sUserID;
            taskToadd.Priority = WCT_UtilConstants.Task_Priority;
            taskToadd.Status = WCT_UtilConstants.Task_Status;
            cNewTask.add(new cTask(taskToadd));
            iCounter++;

        } else {
            task tempTask = new task();
            tempTask.OwnerID = sUserID;
            tempTask.Priority = WCT_UtilConstants.Task_Priority;
            tempTask.Status = WCT_UtilConstants.Task_Status;
            cNewTask.add(new cTask(tempTask));
            iCounter++;
        }

    }



    // Update Task method will update the selected tasks.
    // ******************************************************************************************************************************

    public PageReference updateTask() {
        if (cNewTask.size() > 0) {
            list < task > lstTaskToInsert = new list < task > ();
            for (cTask cT: cNewTask) {

                cT.tsk.WhatId = sCaseId;
                lstTaskToInsert.add(cT.tsk);

            }try{
            insert lstTaskToInsert;
            }catch(Exception e)
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
                apexPages.addMessage(myMsg);
                String s=e.getMessage();
                String Err;
                Integer i= s.lastIndexOf(WCT_UtilConstants.WCT_EditTaskUpdateError);
                if(i!=-1){
                Err=s.substring(i);
                Err=Err.replace(WCT_UtilConstants.WCT_EditTaskUpdateError+',', '');
                Integer j=Err.IndexOf(WCT_UtilConstants.WCT_EditTaskUpdateRemoveErrorString);
                   if(j!=-1){
                 	Err=Err.substring(0,j);
                   }
                 }
                          
                 if(sCaseId!=null){
                            PageReference pageRef = new PageReference('/' + sCaseId  + '&'+WCT_UtilConstants.WCT_EditTaskString_ErrorMessage+'=' + Err);
                            pageRef.setRedirect(true); //Redirecting user to the case detail page. 
                            return pageRef;
                           }else
                           {
                            PageReference pageRef = new PageReference(WCT_UtilConstants.WCT_EditTaskStringActivityPageRtURL + sReturnURL  + '&'+WCT_UtilConstants.WCT_EditTaskString_ErrorMessage+'=' + Err);
                            pageRef.setRedirect(true); //Redirecting user to the case detail page. 
                            return pageRef;
                           } 
              
            
            }

        }
        List < Task > lstTaskToUpdate = new List < Task > ();
        if (getTasks().size() > 0) {
            for (cTask cTskTowork: getTasks())
                {
                //if (cTskTowork.bSelection == true)
                   // {
                    lstTaskToUpdate.add(cTskTowork.tsk);
                    //}
                }
            }
            if (cTrTask.size() > 0) 
            {
             for (cTask cTskTowork: cTrTask)
                {
                //if (cTskTowork.bSelection == true) 
                  //  {
                    lstTaskToUpdate.add(cTskTowork.tsk);
                  //  }
                }
            }
            if (lstTaskToUpdate.size()>0){
                  try{
                update lstTaskToUpdate;
                } catch(Exception e)
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
                apexPages.addMessage(myMsg);
                String s=e.getMessage();
                String Err;
               
                Integer i= s.lastIndexOf(WCT_UtilConstants.WCT_EditTaskInvalidCrossRefError);
                if(i!=-1){
                Err=s.substring(i);
                Err=Err.replace(WCT_UtilConstants.WCT_EditTaskInvalidCrossRefError+',', '');
                
                Integer j=Err.IndexOf(WCT_UtilConstants.WCT_EditTaskUpdateRemoveErrorString);
                   if(j!=-1){
                 	Err=Err.substring(0,j);
                   }
                 }
                if(sCaseId!=null){
                            PageReference pageRef = new PageReference('/' + sCaseId  + '&'+WCT_UtilConstants.WCT_EditTaskString_ErrorMessage+'=' + Err);
                            pageRef.setRedirect(true); //Redirecting user to the case detail page. 
                            return pageRef;
                           }else
                           {
                            PageReference pageRef = new PageReference(WCT_UtilConstants.WCT_EditTaskStringActivityPageRtURL + sReturnURL  + '&'+WCT_UtilConstants.WCT_EditTaskString_ErrorMessage+'=' + Err);
                            pageRef.setRedirect(true); //Redirecting user to the case detail page. 
                            return pageRef;
                           } 
            }
            }
        
        //cTaskList = null;
        if(sCaseId!=null){
        PageReference pageRef = new PageReference('/' + sCaseId);
        pageRef.setRedirect(true); //Redirecting user to the case detail page. 
        return pageRef;
       }else
       {
        PageReference pageRef = new PageReference(WCT_UtilConstants.WCT_EditTaskStringActivityPageRtURL + sReturnURL);
        pageRef.setRedirect(true); //Redirecting user to the case detail page. 
        return pageRef;
       } 

    }

    // Default controller constructor.
    // ******************************************************************************************************************************

    public WCT_EditTaskButtonController() {
        cNewTask = new list < cTask > ();
        sErr = apexpages.currentpage().getparameters().get(WCT_UtilConstants.WCT_EditTaskString_ErrorMessage);
        if (sErr != null) {
            sError = true;
        } else {
            sError = false;
        }
        TrtaskSection=false;
    }

    // Wrapper class to select multiple tasks update.

    // ******************************************************************************************************************************

    public class cTask {
        public Task tsk {
            get;
            set;
        }
        public Boolean bSelection {
            get;
            set;
        }

        public cTask(Task t) {
            tsk = t;
            bSelection = false;

        }
        public cTask() {
            tsk = new task();
            bSelection = false;

        }
    }

    // getTasks method will return the list of the task related to the case and assign to the current user.
    // ******************************************************************************************************************************


    public list < cTask > getTasks() {
        if (cTaskList == null) {
            cTaskList = new list < cTask > ();
            cTrTask   = new list < cTask > ();
            
           list < Task > lstTasksOnPage = new list<Task>();
            try {
                 set<String> setCaseIds= new set<String>();
                 if(sCaseId!=null){
                 setCaseIds.Add(sCaseId);
                 lstTasksOnPage = [select id, Subject, OwnerId,RecordTypeId, ActivityDate, Priority, Status,Secure_Subject__c,Secure_Comments__c, Description, Task_Notes__c from Task where WhatId IN : setCaseIds and Status!='Completed' order by ActivityDate];
                 }else
                 {
                   list<String> lstTaskIds= sTaskIds.split(',');
                   set<String> setTaskIds= new set<String>();
                   setTaskIds.addAll(lstTaskIds); 
                   lstTasksOnPage = [select id, Subject, OwnerId, ActivityDate,RecordTypeId,Secure_Subject__c,Secure_Comments__c, Priority, Status, Description, Task_Notes__c from Task where Id IN : setTaskIds and Status!='Completed' order by ActivityDate];
                 }
               
               list<RecordType> lstRecordType=[select id, name, SobjectType  from RecordType where name =: WCT_UtilConstants.WCT_EditTaskString_TR_Task or name =: WCT_UtilConstants.WCT_EditTaskString_Pre_BI];
               String sTrId;
               for(RecordType lpRd : lstRecordType)
               {
                 if(lpRd.SobjectType==WCT_UtilConstants.WCT_EditTaskString_Task){
                  if(lpRd.name==WCT_UtilConstants.WCT_EditTaskString_TR_Task)
                      {
                       sTrId =  lpRd.id;
                      }
                   if(lpRd.name==WCT_UtilConstants.WCT_EditTaskString_Pre_BI)
                      {
                       PreBiRecordID =  lpRd.id;
                      }
                  }
               }
              String sUserProfie= UserInfo.getProfileId(); 
              Profile listCuuUserProfile= [select id , name from profile where id =:sUserProfie limit 1];
              if(listCuuUserProfile.name==Label.CurrentUserProfile)
              {
               TrtaskSection=true;
              }
              
              cTaskList.clear();
                 cTrTask.clear();
                 for (Task looptask: lstTasksOnPage) {
                    if(sTrId==looptask.RecordTypeId)
                    {
                     cTrTask.add(new cTask(looptask));
                    }else{
                       cTaskList.add(new cTask(looptask));
                        }
                }

            } catch (QueryException e) {
                ApexPages.addMessages(e);
                return null;
            }

        }
        return cTaskList;
 
    }

}