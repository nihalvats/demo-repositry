@isTest

Private class WCT_Mobility_Mass_Close_Cntrlr_Test
{

    public static testmethod void test1()
    {
          contact con = new contact();
          con.FirstName='Test';
          con.LastName='contact';
          Insert con;
          contact con1=[SELECT Id FROM contact where Id =:con.id];
          
       Id EmpRecType = [SELECT Id FROM RecordType WHERE RecordType.Name='Employment Visa'].Id;
        WCT_Mobility__c mobRec1  = new WCT_Mobility__c();
        mobRec1.WCT_Mobility_Status__c = 'New';
        mobRec1.RecordTypeId = EmpRecType;
        mobRec1.WCT_Assignment_Type__c = 'Short Term';
        mobRec1.WCT_First_Working_Day_in_US__c = date.parse('10/10/2014');
        mobRec1.WCT_Last_Working_Day_in_US__c = date.parse('11/11/2014');
        mobRec1.WCT_Mobility_Employee__c = con1.id;
        insert mobRec1;
        
        WCT_Task_Reference_Table__c refRec = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
        refRec.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        refRec.WCT_Visa_Type__c = 'Employment Visa';
        insert refRec;
        
        Task t1 = new Task();
        t1.Subject='Send Visa questionnaire to employee';
        t1.Status='Not Started';
        t1.OwnerId = Label.Fragomen_UserID;
        t1.WCT_Task_Reference_Table_ID__c= refRec.id;
        t1.WhatId = mobRec1.Id;
        t1.Task_Type__c = 'New';
        insert t1;
        
        Test.startTest();
        List<Task> tasklist1 = new List<Task>();
        tasklist1.add(t1);
        
        WCT_Mobility_Mass_Close_Controller.taskListWrapper tasklistWrap1 = new WCT_Mobility_Mass_Close_Controller.taskListWrapper(true, t1.Status, t1.Subject, t1.Id, t1.OwnerId, t1);
        
        
        List<WCT_Mobility_Mass_Close_Controller.taskListWrapper> wraplist1 = new List<WCT_Mobility_Mass_Close_Controller.taskListWrapper>();
        wraplist1.add(tasklistWrap1);
        
        WCT_Mobility_Mass_Close_Controller.taskWrapper taskWrap1 = new WCT_Mobility_Mass_Close_Controller.taskWrapper('test',wraplist1);       
        
        List<WCT_Mobility_Mass_Close_Controller.taskWrapper> taskwraplist1 = new List<WCT_Mobility_Mass_Close_Controller.taskWrapper>();
        taskwraplist1.add(taskWrap1);
       
        WCT_Mobility_Mass_Close_Controller.mobilityWrapper mobWrap1 = new WCT_Mobility_Mass_Close_Controller.mobilityWrapper(mobRec1,taskwraplist1);
        
        WCT_Mobility_Mass_Close_Controller controller1 = new WCT_Mobility_Mass_Close_Controller();
        controller1.selectedStatus = 'New';
        controller1.selectedViewBy = 'Employee';
        controller1.MobilityTypeValue = 'Employment Visa';
        controller1.AssignmentTypeValue = 'Short Term';
        controller1.firstWorkingDay = date.parse('10/10/2014');
        controller1.firstWorkingDayTo = date.parse('10/11/2014');
        controller1.lastWorkingDay = date.parse('11/11/2014');
        controller1.lastWorkingDayTo =date.parse('11/12/2014');
        controller1.getmobilitystatus();
        controller1.getmobilityTypes();
        controller1.getassignmentTypes();
        controller1.displaydependentfilters();
        controller1.getListTasks();
        controller1.checkAll();
        controller1.massClose();
        
        Test.stopTest();
    
    }   
    
    public static testmethod void test2()
    {
     
        contact con2 = new contact();
          con2.FirstName='Test';
          con2.LastName='contact1';
          Insert con2;
          contact con3=[SELECT Id FROM contact where Id =:con2.id];   
        Id BusRecType = [SELECT Id FROM RecordType WHERE RecordType.Name='Employment Visa'].Id;
        WCT_Mobility__c mobRec2  = new WCT_Mobility__c();
        mobRec2.WCT_Mobility_Status__c = 'New';
        mobRec2.RecordTypeId = BusRecType;
                
        mobRec2.WCT_Travel_Start_Date__c = date.parse('10/10/2014');
        mobRec2.WCT_Travel_End_Date__c = date.parse('11/11/2014');
        mobRec2.WCT_Mobility_Employee__c = con3.id;
        insert mobRec2;
                Test.startTest();
        WCT_Task_Reference_Table__c refRec2 = WCT_UtilTestDataCreation.CreateTaskRefTableforCreateTask();
        refRec2.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        refRec2.WCT_Visa_Type__c = 'Business Visa';
        insert refRec2;
        
        Task t2 = new Task();
        t2.Subject='Send Visa questionnaire to employee';
        t2.Status='Not Started';
        t2.OwnerId = Label.Fragomen_UserID;
        t2.WCT_Task_Reference_Table_ID__c= refRec2.id;
        t2.WhatId = mobRec2.Id;
        t2.Task_Type__c = 'New';
        insert t2;
        
        
        List<Task> tasklist2 = new List<Task>();
        tasklist2.add(t2);
        
        WCT_Mobility_Mass_Close_Controller.taskListWrapper tasklistWrap2 = new WCT_Mobility_Mass_Close_Controller.taskListWrapper(true, t2.Status, t2.Subject, t2.Id, t2.OwnerId, t2);
        
        
        List<WCT_Mobility_Mass_Close_Controller.taskListWrapper> wraplist2 = new List<WCT_Mobility_Mass_Close_Controller.taskListWrapper>();
        wraplist2.add(tasklistWrap2);
        
        WCT_Mobility_Mass_Close_Controller.taskWrapper taskWrap2 = new WCT_Mobility_Mass_Close_Controller.taskWrapper('test',wraplist2);       
        
        List<WCT_Mobility_Mass_Close_Controller.taskWrapper> taskwraplist2 = new List<WCT_Mobility_Mass_Close_Controller.taskWrapper>();
        taskwraplist2.add(taskWrap2);
       
        WCT_Mobility_Mass_Close_Controller.mobilityWrapper mobWrap2 = new WCT_Mobility_Mass_Close_Controller.mobilityWrapper(mobRec2,taskwraplist2);
        
        WCT_Mobility_Mass_Close_Controller controller2 = new WCT_Mobility_Mass_Close_Controller();
        controller2.selectedStatus = 'New';
        //controller2.selectedViewBy = 'Fragomen';
        controller2.selectedViewBy = 'Employee';
        controller2.MobilityTypeValue = 'Business Visa';
        controller2.travelStartDate = date.parse('10/10/2014');
        controller2.travelStartDateTo = date.parse('10/11/2014');
        controller2.travelEndDate = date.parse('11/11/2014');
        controller2.travelEndDateTo = date.parse('11/12/2014');
        controller2.getmobilitystatus();
        //controller2.getmobilityTypes();
        controller2.displaydependentfilters();
        controller2.getListTasks();
        controller2.checkAll();
        controller2.massClose();
        
        Test.stopTest();
    
    }   
    
    
}