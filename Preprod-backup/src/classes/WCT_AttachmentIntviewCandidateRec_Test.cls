@isTest
public class WCT_AttachmentIntviewCandidateRec_Test{
	/* Variable Declaration */
	public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get(WCT_UtilConstants.CANDIDATE_RT).getRecordTypeId();
	public static Contact Candidate;
	public static WCT_Candidate_Documents__c CandidateDocument;
    public static Attachment attachment;
    public static WCT_Requisition__c Requisition;
    public static WCT_Candidate_Requisition__c CandidateRequisition;
    public static User userRecRecr;
    public static Id profileIdRecr=[Select id from Profile where Name=:WCT_UtilConstants.RECRUITER_COMPANY].Id;
    
     /** 
        Method Name  : createUserRecr
        Return Type  : User
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static User createUserRecr()
    {
      userRecRecr=WCT_UtilTestDataCreation.createUser('Recr', profileIdRecr, 'arunsharmaRecr@deloitte.com', 'arunsharma4@deloitte.com');
      insert userRecRecr;
      return  userRecRecr;
    }
	
	 /** 
        Method Name  : createCandidate
        Return Type  : Contact
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static Contact createCandidate()
    {
      Candidate=WCT_UtilTestDataCreation.createContactAsCandidate(candidateRecordTypeId);
      insert Candidate;
      return  Candidate;
    }  
    
     /** 
        Method Name  : createCandidateDocument
        Return Type  : WCT_Candidate_Documents__c
        Type      : private
        Description  : Create temp records for data mapping         
    */    
    private Static WCT_Candidate_Documents__c createCandidateDocument()
    {
      	CandidateDocument = new WCT_Candidate_Documents__c(WCT_Candidate__c= Candidate.Id,WCT_File_Name__c='CandidateResume', WCT_File_Path__c='c:/', WCT_RMS_ID__c='1234', WCT_Created_Date__c=system.today(),WCT_Visible_to_Interviewer__c = True);
    	insert CandidateDocument;
      	return  CandidateDocument;
     }
    /** 
        Method Name  : createAttachment
        Return Type  : Attachment
        Type      : private
        Description  : Create temp records for data mapping         
    */    
    private Static Attachment createAttachment()
    {
      	attachment=WCT_UtilTestDataCreation.createAttachment(CandidateDocument.Id);
    	insert attachment;
      	return  attachment;
    } 
       /** 
        Method Name  : createRequisition
        Return Type  : WCT_Requisition__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Requisition__c createRequisition()
    {
      Requisition=WCT_UtilTestDataCreation.createRequisition(userRecRecr.Id);
      insert Requisition;
      return  Requisition;
    }
    /** 
        Method Name  : createCandidateRequisition
        Return Type  : WCT_Candidate_Requisition__c
        Type      : private
        Description  : Create temp records for data mapping         
    */
    private Static WCT_Candidate_Requisition__c createCandidateRequisition()
    {
      CandidateRequisition=WCT_UtilTestDataCreation.createCandidateRequisition(Candidate.ID,Requisition.Id);
      insert CandidateRequisition;
      return  CandidateRequisition;
    }      
   public static testmethod void testConstructor(){
   	
   	//------ Set up test data ------
   		userRecRecr=createUserRecr();
    	Candidate=createCandidate();
   		Requisition=createRequisition();
    	CandidateRequisition=createCandidateRequisition();
    	CandidateDocument=createCandidateDocument();
    	attachment=createAttachment();
    	//set<id> attids = new set<id>();
    	//attids.add(Candidate.id);
    	//attids.add(CandidateDocument.id);
    	
    //------ Start test execution ------	
    	Test.startTest();
    	ApexPages.StandardController stdController = new ApexPages.StandardController(Candidate);
    	    	
    	//System.debug('-------**********TEST CLASS****------'+Candidate.Id);
    	
    	PageReference pageRef = new PageReference('/apex/WCT_ContactDetail?id='+Candidate.Id); 
    	//pageRef.getParameters().put('id', String.valueOf(Candidate.Id));
       	
       	Test.setCurrentPage(pageRef);
        WCT_AttachmentIntviewCandidateRec obj = new WCT_AttachmentIntviewCandidateRec(stdController);
    	obj.getAttachmentsCandidate();
    	System.assertNotEquals(obj.getAttachmentsCandidate(), null);
    	obj.getNotesCandidate();
    	Test.stopTest(); 
    //------ Stop test execution ------
    } 
    
   
}