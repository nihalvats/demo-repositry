/******************************************************************************************************************* 
    * Class Name   : PPDReferralFormController
    * VF Page Name : PPDReferralForm
    * Description  : This Class is PPD  
    * Developer    : Karthik
  *****************************************************************************************************************/
Public with sharing class PPDReferralFormController extends SitesTodHeaderController{
    /*Public Variables*/
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    Public Case Ocase{get;set;}
    public Case caseRecord{get;set;}
    public String radio1 { get; set; }
    public String radio2 { get; set; }
    public String radio3 { get; set; }
    Public List<RecordType> OcaseRecordType;
    //public Boolean showAttachmentButton {get;set;}
    //public Boolean showDownloadButton {get;set;}
    public Boolean chkdisable1 {get;set;}
    public Boolean Resume { get; set; }
    public Boolean Emailcorrespondence { get; set; }
    public Boolean chkdisable {get;set;}
    public Boolean chkBx { get; set; }
    public Boolean display { get; set; }
    public String input { get; set; }
    
    public GBL_Attachments attachmentHelper{get; set;}
    //public List<Attachment> listAttachments {get; set;}
    //public Map<Id, String> mapAttachmentSize {get; set;} 
    //public Integer countattach{get;set;}
    //public Document doc {get;set;}
    
    // Constructor
    public PPDReferralFormController(){
        Ocase = new case();
        OcaseRecordType = new List<RecordType>();
        OcaseRecordType = [SELECT ID, Name FROM RecordType WHERE sObjectType = 'Case' And Name =:'PPD-Referral' Limit 1];
        if(OcaseRecordType[0] != null){
        Ocase.recordtypeid = OcaseRecordType[0].id;
        }
        attachmentHelper= new GBL_Attachments(); 
        //init();
    }
     
    // Enable/Disable check boxes
    public void disa(){
        if(Emailcorrespondence == true || Resume == true){
            chkdisable = True;
        }
        else{
            chkdisable = False; 
        }
    }
    
    // Enable/Disable check boxes
    public PageReference displaytext() {
        if(chkBx){
        display = true;
        chkdisable1 = True;
        
        }
        else{
        display = false;
        chkdisable1 = false;
        }
        return null;
    }
    
    // save method with conditions
    public PageReference saveRequest(){
        pageError =false;
        
          if(Ocase.FCPA_Candidate_First_Name__c=='' || Ocase.FCPA_Candidate_First_Name__c== null)
          {
          pageError =true;
          pageErrorMessage ='Candidate first name is mandatory';  
          return null;  
          }
          if(Ocase.FCPA_Candidate_Last_Name__c=='' || Ocase.FCPA_Candidate_Last_Name__c== null)
          {
          pageError =true;
          pageErrorMessage ='Candidate last name is mandatory';  
          return null;  
          }
          if(Ocase.FCPA_Candidate_Email_Address__c=='' || Ocase.FCPA_Candidate_Email_Address__c== null)
          {
          pageError =true;
          pageErrorMessage ='Candidate Email is mandatory';  
          return null;  
          }
          
          if(Ocase.FCPA_Type_of_Hire__c=='' || Ocase.FCPA_Type_of_Hire__c== null)
          {
          pageError =true;
          pageErrorMessage ='Type of hire is mandatory';  
          return null;  
          }
          if(Ocase.FCPA_Which_country_are_you_referring_to__c=='' || Ocase.FCPA_Which_country_are_you_referring_to__c== null)
          {
          pageError =true;
          pageErrorMessage ='Which country are you referring to? is mandatory';  
          return null;  
          } 
          if(Ocase.FCPA_Which_FSS_are_you_recommending__c=='' || Ocase.FCPA_Which_FSS_are_you_recommending__c== null)
          {
          pageError =true;
          pageErrorMessage ='Which FSS do you recommend? is mandatory';  
          return null;  
          }
          if(Ocase.FCPA_Which_FSS_are_you_recommending__c=='Other'){
              if(Ocase.FCPA_Other__c=='' || Ocase.FCPA_Other__c== null)
              {
              pageError =true;
              pageErrorMessage ='Other is mandatory';  
              return null;  
              }
          } 
          if(Ocase.FCPA_Which_FSS_are_you_recommending__c=='Unknown'){
              if(Ocase.FCPA_Unknown__c=='' || Ocase.FCPA_Unknown__c== null)
              {
              pageError =true;
              pageErrorMessage ='unknown is mandatory';  
              return null;  
              } 
          }
          
          if(Ocase.FCPA_Referring_PPD_function__c=='' || Ocase.FCPA_Referring_PPD_function__c== null)
          {
          pageError =true;
          pageErrorMessage ='Referring PPD function? is mandatory';  
          return null;  
          }
          
          if( (Ocase.FCPA_Referring_PPD_function__c=='Other') && (Ocase.FCPA_Other_PPD_function__c=='' || Ocase.FCPA_Other_PPD_function__c== null) ){
              //if(Ocase.FCPA_Other_PPD_function__c=='' || Ocase.FCPA_Other_PPD_function__c== null)
              //{
              pageError =true;
              pageErrorMessage ='Other PPD function is mandatory';  
              return null;  
              //}
          } 
          
        if(chkBx == True && input == ''){ pageError =true;  pageErrorMessage ='Please Provide Description';  return null; }
          
          if(radio1=='Yes'){  if(Ocase.FCPA_A1__c=='' || Ocase.FCPA_A1__c== null) {
              pageError =true; pageErrorMessage ='Please provide answer for Q1';  return null;  }
          } 
          
          if(radio2=='Yes'){
              if(Ocase.FCPA_A_Name_of_the_requesting_third_part__c=='' || Ocase.FCPA_A_Name_of_the_requesting_third_part__c== null || Ocase.FCPA_A_His_Her_position__c=='' || Ocase.FCPA_A_His_Her_position__c== null || Ocase.FCPA_A_Organization_name__c=='' || Ocase.FCPA_A_Organization_name__c== null || Ocase.FCPA_A_Known_connection__c=='' || Ocase.FCPA_A_Known_connection__c== null)
              {
              pageError =true; pageErrorMessage ='Please provide answer for Q2';  return null;  }
          } 
          if(radio3=='Yes'){  if(Ocase.FCPA_A3__c=='' || Ocase.FCPA_A3__c== null)  {
              pageError =true;  pageErrorMessage ='Please provide answer for Q3';  return null;   }
          }
           
          if(resume == False && EmailCorrespondence == False && chkBx == False){
              pageError =true;  
              pageErrorMessage ='Attachment is mandatory1';  
              return null;  
          }
          
          //if((resume == True || EmailCorrespondence == True) && (attachmentHelper.docIdList.size() == 0)){
          if((resume == True || EmailCorrespondence == True) && (attachmentHelper.UploadedDocumentList.size() == 0)){
              pageError =true;
              pageErrorMessage ='Attachment is required to submit the form.';  
              return null;
          }
          if(radio1==null){
              pageError =true;
              pageErrorMessage ='Please select answer for Q1';  
              return null;  
            }
          if(radio2==null){
              pageError =true;
              pageErrorMessage ='Please select answer for Q2';  
              return null;  
            } 
           if(radio3==null){
              pageError =true;
              pageErrorMessage ='Please select answer for Q3';  
              return null;  
           }
           
           
           
           try{ 
            
            Ocase.Status    = 'New';
            Ocase.Subject = 'PPD Case';
            Ocase.WCT_Category__c=  'PPD-Referral';  
            Ocase.Origin = 'Web';
            Ocase.Description = 'PPD Case';  
            Ocase.Contactid   = loggedInContact.id;
            Ocase.RecordTypeid   = OcaseRecordType[0].Id;
            Ocase.FCPA_PPD_Survey_Submitted__c   = True;
            Ocase.FCPA_PPD_Name__c=loggedInContact.Name;
            Ocase.FCPA_PPD_First_Name__c=loggedInContact.FirstName;
            Ocase.FCPA_PPD_Last_Name__c=loggedInContact.LastName;
            Ocase.FCPA_PPD_Email_Address__c=loggedInContact.Email;
            if(Resume == True){
            ocase.FCPA_Document_Type__c = 'Resume';
            }
            if(Emailcorrespondence == True){
            ocase.FCPA_Document_Type__c = 'Email correspondence';
            }
            if(Resume == True && Emailcorrespondence == True){
            ocase.FCPA_Document_Type__c = 'Resume'+', '+'Email correspondence';
            }
            if(chkBx == True){
            ocase.FCPA_Document_Type__c = input;
            }
           // getting information from list of names
           List<WCT_List_Of_Names__c> listofnames = new List<WCT_List_Of_Names__c>();
            listofnames  = [SELECT ID,Name,WCT_Type__c,FCPA_FSS__c,FCPA_Type_of_Hire__c,FCPA_Referral_SPOC_Email__c,FCPA_Referral_SPOC_Name__c FROM WCT_List_Of_Names__c WHERE FCPA_Type_of_Hire__c=:ocase.FCPA_Type_of_Hire__c AND FCPA_FSS__c=:ocase.FCPA_Which_FSS_are_you_recommending__c];
            if(!listofnames.isEmpty()){
            Ocase.FCPA_Referral_SPOC_Email__c= listofnames[0].FCPA_Referral_SPOC_Email__c ;
            Ocase.FCPA_Referral_SPOC_Name__c= listofnames[0].FCPA_Referral_SPOC_Name__c;
        
           }
        
            Ocase.FCPA_Q1_Answer__c=radio1;
            Ocase.FCPA_Q2_Answer__c=radio2;
            Ocase.FCPA_Q3_Answer__c=radio3;
            Ocase.FCPA_Form_Submitted_By__c='PPD';
            
            Database.DMLOptions dmlOpts = new Database.DMLOptions();
            dmlOpts.EmailHeader.TriggerUserEmail = true;
            Ocase.setOptions(dmlOpts); 
            Database.insert(Ocase, dmlOpts);
            
            //  survey record created
            system.debug('#########Ocase.Id'+Ocase.Id);
            
            attachmentHelper.uploadRelatedAttachment(Ocase.Id);
            
            //PageReference reference=new PageReference('/apex/PPD_Referral_Thankyou_Page?id=' + ocase.id+'&em='+CryptoHelper.encrypt(LoggedInContact.Email));
            PageReference reference=new PageReference('/apex/GBL_Page_Notification?id=' + ocase.id+'&key=PPDThankyou');
             reference.setRedirect(false);
             return reference;
            
          }catch(System.DmlException e){
             return null;  
           }
        
        return null;
    }
    /*
    //INIT METHOD FOR INITIALISING OBJECTS AND ATTACHMENTS 
    
    private void init(){

       caseRecord  = new  case();
       countattach = 0;
       doc = new Document();
       listAttachments = new List<Attachment>();
       mapAttachmentSize = new Map<Id, String>();

    }   
    
    // -----------Attachment code---------------
    
    @TestVisible
    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId , 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId = :ocase.id 
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50 ];    
        system.debug('listAttachments----------'+listAttachments);      
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                //SIZE GREATAR THAN 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                //SIZE GREATER THAN 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }
    
    */
    
}