public  class AdA_PractitionerRegistration {
    public string securityToken{get;set;}
    id pracRecTypeId;
    private Contact con;
    public Boolean showAuthenticationFailed{get;set;}
    public Boolean showPracErMsg{get;set;}
    public Boolean showEmailsent{get;set;}
     public Boolean showPasswordError{get;set;}
     public Boolean showCongrats{get;set;}
      public Invalid_Practitioner__c InsertInvalidPractitioner{get;set;}
      
    public string password{get;set;}
     public string adminEmail{get;set;}
     public AdA_PractitionerRegistration(ApexPages.StandardController stdController) {
          showAuthenticationFailed=false;
          showPracErMsg=false;
          showEmailsent=false;
          showPasswordError=false;
          showCongrats=false;
          pracRecTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
          this.con = (Contact)stdController.getRecord();
           AdA_Admin_Email__c AdAAdminEmail=AdA_Admin_Email__c.getall().values();
         adminEmail=AdAAdminEmail.AdA_Admin_Email__c;
         
          
     }
     
     public pagereference emailSecurityToken(){
     try{
         con=[select id,Security_Key_Ada__c,Security_Token_AdA__c,WCT_Person_Id__c,Email,FirstName,lastName from contact where lastName=:con.LastName 
          and WCT_Person_Id__c=:con.WCT_Person_Id__c and recordtypeId=:pracRecTypeId and WCT_Type__c!= 'Active'];
          
          
         
             system.debug('&&&&'+con);
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
             String[] toAddresses = new String[] {con.Email};
             mail.setToAddresses(toAddresses);
             mail.setSenderDisplayName('usadoptanalum@deloitte.com');
             mail.setReplyTo('usadoptanalum@deloitte.com');
             mail.setSubject('AdA Security Token Confirmation');
             mail.setHtmlBody('Dear  ' + con.FirstName+',<br/> Greetings!<br/>Please use the Pin below to register.<br/>Enter the pin and then enter your new password on the Adopt-an-Alum site.<br/><br/>Pin:'+con.Security_Token_AdA__c+'<br/><br/><br/>Happy Adopting.<br/><br/>Thank you,<br/>National Alumni Relations Team.');
             
             list<Messaging.SendEmailResult> result=Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
             if(result[0].issuccess()){
                 showEmailsent=true;
                 showPracErMsg=false;
             }
             else
                 showEmailsent=false;
             return null;
         }    
         catch(exception e){
             showPracErMsg=true;
             showEmailsent=false;
             return null;
          }
     }
     public pagereference AdA_PractitionerExceptionProcess(){
        
        InsertInvalidPractitioner=new Invalid_Practitioner__c();
        return (new pagereference('/apex/AdA_practitionerExceptionForm'));
    
     }
     
     public pagereference registerPractitioner(){
        InsertInvalidPractitioner.recordTypeId=Schema.SObjectType.Invalid_Practitioner__c.getRecordTypeInfosByName().get('Invalid Practitioner').getRecordTypeId();
        InsertInvalidPractitioner.Admin_EmailId__c=adminEmail;
        insert InsertInvalidPractitioner;
        showCongrats=true;
        return null;
        
     }
     
       
      public pagereference confirmation(){
      
      try{
              system.debug('&&&&'+con.Security_Key_AdA__c+'secToken'+con.Security_Token_AdA__c+'enteerdsectoken'+securityToken);
              string secKey=con.Security_Key_AdA__c;
              
              //if(con.Security_Token_AdA__c==null)
                  con=[Select Id,FirstName,LastName,name,Email,WCT_Function__c,recordtypeId,Charge_Code_AdA__c,Request_Charge_Code_Ad__c ,WCT_Person_Id__c ,
                Security_Key_AdA__c,Security_Token_AdA__c From Contact Where 
                        lastName=:con.LastName  and WCT_Person_Id__c=:con.WCT_Person_Id__c and recordtypeId=:pracRecTypeId];
             
              if(securityToken==con.Security_Token_AdA__c){
              
                  con.Security_Key_AdA__c=password;
                  if(password<>null&&password.length()>6&&password.length()<10){
                    update con;                                        
                    return new pagereference('/apex/AdA_LoginPage');
                  }
                  else{
                    showPasswordError=true;
                    return null;
                  }
                  
              }
              else{
              showAuthenticationFailed=true;
              showPracErMsg=false;
             showEmailsent=false;
              return null;
              }
          
          }
        
      
        catch(exception e){
            system.debug('***'+    e);
            showAuthenticationFailed=true;
            showPracErMsg=false;
             showEmailsent=false;
              return null;
        }
     }
}