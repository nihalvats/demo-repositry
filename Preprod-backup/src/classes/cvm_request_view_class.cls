/*

Project : VRM Contractor Management
Associated VF Page : CVM_Request_View_Page
Description:Controller which displays the view in a read only form
Test Class : CVM_Class_Test
Author : Karthik Raju Gollapalli

*/




public with sharing class cvm_request_view_class extends SitesTodHeaderController 
{

    public String strings { get; set; }
    public String record{get;set;}
    public String records{get;set;}
    /*Declerations*/
    public CVM_Contractor_Request__c TDRequestInstance{get;set;}
    public CVM_Contractor_Request__c cvmObj{get;set;}

    public contact conObj{get;set;}
    public string currentDate{get{return Date.today().format();}set;}
    public String emValue{get;set;}
    public String reqid{get;set;}
    public string requestNumber {get;set;}
    public string TD_Request_Page='CVM_Dev_TD_VENDOR_Form';
    
    /*Declerations*/
    
    public cvm_request_view_class()
    {
        //emValue = ApexPages.currentPage().getParameters().get('em');
        //string userEmail = cryptoHelper.decrypt(emValue);
        string userEmail = loggedInContact.email;
        conObj=new contact();
        conObj = [select id,name, email from contact where email = : userEmail limit 1];
        reqid = ApexPages.currentPage().getParameters().get('id');
        requestNumber =  ApexPages.currentPage().getParameters().get('rnum');
        records = ApexPages.currentPage().getParameters().get('rec');
        record = records.replace(' ','_');        
        TDRequestInstance = new CVM_Contractor_Request__c();
        cvmObj= new CVM_Contractor_Request__c();

        
        cvmObj = [SELECT Name,CVM_Is_the_client_s_a_restricted_entity__c,CVM_Client_Name_and_Address__c,CVM_Name_of_business_entity_you_are_requ__c,CVM_Additional_Comments_Requests_Notes__c,CVM_Address_of_Location_for_Laptop_Pick__c,CVM_Senior_Manager_Name__c,CVM_Admin_Notes__c,CVM_Advisory_Market_Offering__c,CVM_Advisory_Service_Area__c,CVM_Advisory_Sub_Entity__c,CVM_Agency_Vendor_Business_Entity_Name__c,CVM_Agency_Contact_Email_Address__c,CVM_Agency_Contact_Name__c,CVM_Agency_Contact_Phone_Number__c,CVM_Agency_Name__c,CVM_Agency_Phone_Number__c,CVM_Anticipated_amount_of_weekly_hours__c,CVM_Anticipated_Cost__c,CVM_Areas_supported_by_resource__c,CVM_Ariba_DO_if_applicable__c,CVM_Ariba_PR__c,CVM_Ariba_Requisition_Created_if_applica__c,CVM_Ariba_Requisition_Fully_Approved_if__c,CVM_Ariba_Work_Order_Number__c,CVM_Arrival_time__c,CVM_Assign_Candidate__c,CVM_Assigned_Program_Specialist__c,CVM_Audience_Type__c,CVM_Bill_Rate__c,CVM_Business_Case__c,CVM_Business_Case_Rationale__c,CVM_Business_Leaders_who_have_approved__c,CVM_Campus_Recruiting_Sponsor__c,CVM_Case__c,CVM_Case_Name_Project_Name_Case_Num__c,CVM_CLO_Channel_Lead__c,CVM_COE_Team_Review__c,CVM_Comments_For_Contract_Request_Type__c,CVM_Comments_For_Other_contract_request__c,CVM_Confirmation_from_agency_that_BI_is__c,CVM_Confirmation_that_laptop_is_ready_fo__c,CVM_conflict_form_to_be_signed__c,CVM_Contractor_s_Middle_Name_or_Initial__c,CVM_Contractor_Email_Address__c,CVM_Contractor_First_Name__c,CVM_Contractor_is_Providing_Services_to__c,CVM_Contractor_is_working_on_Federal_Gov__c,CVM_Contractor_Last_Name__c,CVM_Contractor_or_Individual_s_office_lo__c,CVM_Contractor_Phone_Number__c,CVM_Role_Job_Title__c,CVM_Contractor_selected__c,CVM_Contract_request_type__c,CVM_Course_Type__c,CVM_Contractor_Rec__c,CVM_CTS_Additional_Comments__c,CVM_CTS_Specialist__c,CVM_Date_of_Joining__c,CVM_Day_1_Logistical_information_sent_EM__c,CVM_Day_1_Logistical_information_sent_to__c,CVM_Deloitte_Entity__c,CVM_Deloitte_issued_laptop_needed__c,CVM_Deloitte_Office_Location__c,CVM_Deloitte_POC__c,CVM_Does_the_contractor_need_a_Security__c,CVM_Do_you_have_additional_contractor_na__c,CVM_Do_you_need_assistance_in_finding_id__c,CVM_DRS_Resume_Review_Required__c,CVM_Duration_of_each_interview_hours__c,CVM_EEDC_Form_Submitted__c,CVM_Effective_Date__c,CVM_Engaging_Manager_Time_Sheet_Approver__c,CVM_Estimated_annual_Spend__c,CVM_Estimated_total_cost_of_services__c,CVM_Expiration_Date__c,CVM_External_Employee_Subgroup__c,CVM_External_Email_Address__c,CVM_External_First_Name__c,CVM_External_Last_Name__c,CVM_External_Phone_Number__c,CVM_First_Day_Schedule__c,CVM_Functional_TA_Leader__r.Name,CVM_Group__c,CVM_Has_pricing_and_associated_fees_been__c,CVM_Has_the_Contract_Previously_Worked_f__c,CVM_Has_the_Resource_Been_Identified__c,CVM_Has_the_Vendor_Business_Relationship__c,CVM_Have_they_been_cleared_by_the_Nation__c,CVM_Have_you_received_approval_for_this__c,CVM_Have_you_worked_with_your_Resource_M__c,CVM_How_will_the_products_or_services_be__c,CVM_How_will_training_service_be_deliver__c,CVM_If_Other_is_selected_please_specify__c,CVM_If_Other_is_selected_Please_Describe__c,CVM_Specify_Your_Own_Office_Location__c,CVM_If_yes_please_provide_the_client__c,CVM_Independence_Representation_Submitte__c,CVM_Industries_Sectors_Specialized_Exper__c,CVM_Interviewing_if_applicable__c,CVM_Interview_names_for_each_round__c,CVM_Interview_Round_No__c,CVM_Is_Resource_Sourcing_Requ__c,CVM_Is_there_an_existing_contract_curren__c,CVM_Is_this_program_project_led_by_TD__c,CVM_Is_this_program_paid_for_by_TD__c,CVM_Job_Description__c,CVM_Job_Qualifications__c,CVM_Length_Of_Trial__c,CVM_List_all_technology_access_needed__c,CVM_Lump_Sum_Fee__c,CVM_Name_of_individual_TD_Speaker__c,CVM_Name_of_Search_Firm__c,CVM_NDA_Form_Submitted__c,CVM_Need_candidates_frm_Diversity_Agency__c,CVM_No_of_Assigned__c,CVM_No_of_Reviewers_for_Arabic__c,CVM_No_of_Reviewers_for_Chinese_Mandarin__c,CVM_No_of_Reviewers_for_Chinese_Specify__c,CVM_No_of_Reviewers_for_Dutch__c,CVM_No_of_Reviewers_for_English__c,CVM_No_of_Reviewers_for_French__c,CVM_No_of_Reviewers_for_German__c,CVM_No_of_Reviewers_for_Hebrew__c,CVM_No_of_Reviewers_for_Italian__c,CVM_No_of_Reviewers_for_Japanese__c,CVM_No_of_Reviewers_for_Korean__c,CVM_No_of_Reviewers_for_Other_Language__c,CVM_No_of_Reviewers_for_Russian__c,CVM_No_of_Reviewers_for_Spanish__c,CVM_Notes_Section__c,CVM_Number_of_Positions__c,CVM_OldFunction__c,CVM_On_Behalf_Of__c,CVM_on_behalf_email__c,CVM_OneTeam_Contact_if_available_applica__c,CVM_Other_Industry__c,CVM_Others__c,CVM_Overall_Comments__c,CVM_Paralegals__c,CVM_Partner_Principal_or_Director_Approv__c,CVM_Pay_Rate__c,CVM_Pay_Type__c,CVM_Please_explain_why_techcomponent_is__c,CVM_Please_provide_a_summary_of_services__c,CVM_Please_provide_a_summary_of_the_proj__c,Please_specify_Talent_Area_which_is_fund__c,CVM_Please_specify_your_Talent_Group__c,CVM_POC_Email__c,CVM_POC_Job_Title__c,CVM_POC_Name__c,CVM_POC_Phone_Number__c,CVM_Function__c,CVM_Position_Level__c,CVM_Position_Service_Area__c,CVM_Position_Title__c,CVM_PPD_approval_is_applicable__c,CVM_Preferred_Work_Location__c,CVM_Pricing_and_associates_fees_descrip__c,CVM_RC_Cost_Center__c,CVM_Reason_Cancelled__c,CVM_Reason_for_Access__c,CVM_Receiving_Resumes__c,CVM_Request_Date__c,CVM_Requested_By__c,CVM_Requesting_Recruiter__c,CVM_Request_ID__c,CVM_Requestor_Name__c,CVM_Request_Status__c,CVM_Request_Type__c,CVM_Requisition_submitted_agency_notifie__c,CVM_Resource_Manager_Name__c,CVM_Reviewer__c,CVM_RMS_Buddy_Name__c,CVM_Search_Firm_Address__c,CVM_Request_Type1__c,CVM_Select_Product_Type__c,CVM_Sent_contractor_details_to_local_TA__c,CVM_Service_Area__c,CVM_Services_Provided_Are__c,CVM_Sourcing_Method__c,CVM_Special_Skills_Required__c,CVM_Sponsoring_Partner_Principal_Directo__c,CVM_Stage__c,CVM_Students_Email_Address__c,CVM_Students_First_Name__c,CVM_Students_Last_Name__c,CVM_Student_Type__c,CVM_Students_Phone_Number__c,CVM_Summary_of_Need_Reason_for_Request__c,CVM_Summary_of_Need_License_Voucher_info__c,CVM_Summary_of_specific_miscellaneous_ne__c,CVM_Summary_of_Technology_Access__c,CVM_Supporting_Area__c,CVM_Talent_Group__c,CVM_Talent_Request_Type__c,CVM_Telephone_Voicemail_and_Fax__c,CVM_Tentative_End_Date__c,CVM_Tentative_Schedule__c,CVM_TentativeSDNoTime__c,CVM_Tentative_Start_Date__c,CVM_Test_Sk__c,CVM_Time_to_call_POC__c,CVM_Time_Zone__c,CVM_Title__c,CVM_Type_of_Contract__c,CVM_Type_of_Interview__c,CVM_Type_of_Sub_Contractor__c,CVM_Type_of_Vendor_pricing_and_fee_desc__c,CVM_Vendor_Contact_First_Name_Last_Name__c,CVM_Vendor_Contract__c,CVM_Vendor_Email_Address__c,CVM_Vendor_Mailing_Address__c,CVM_Vendor_Name__c,CVM_Vendor_Phone_Number__c,CVM_Vendors_Contacted_to_source__c,CVM_Vendor_Website__c,CVM_WBS_for_Invoicing__c,CVM_Welcome_email_sent_to_contractor__c,CVM_What_is_the_projected_total_cost__c,CVM_What_type_of_contractor_is_the_ident__c,CVM_What_type_of_service_does_vendor_pro__c,CVM_Where_will_the_contractor_be_working__c,CVM_Which_Talent_Service_Area_is_funding__c,CVM_Timesheet_Approver__c,CVM_Who_is_on_site_coordinator_for_Day_1__c,CVM_Will_a_technology_component_be_requi__c,CVM_Will_clients_be_engaged_involved_wit__c,CVM_Will_employees_of_other_member_firm__c,CVM_Will_the_Contractor_have_access_toCI__c,CVM_Will_there_be_Deloitte_proprietary_o__c FROM CVM_Contractor_Request__c WHERE id=:reqid];
        
    }
    
    
     public Pagereference gotorequestpage()
    {    
        system.debug('## gotorequestpage');
        PageReference MyRequestsPageRef = Page.CVM_My_Request;
        //MyRequestsPageRef.getParameters().put('em',ApexPages.currentPage().getParameters().get('em'));
        MyRequestsPageRef.setRedirect(true);
        return MyRequestsPageRef;
    }
  
    /*
    public PageReference updateRecordType()
    {
        
         System.debug('# URL '+ApexPages.currentPage().getUrl());
        if(strings == 'TD_Vendor_Request' )
        {
           
            String pageURL = Site.getBaseUrl()==''?'/apex/'+TD_Request_Page:'/tod/'+TD_Request_Page;
            PageReference pageRef= new PageReference(pageURL);
            pageRef.getParameters().put('em',ApexPages.currentPage().getParameters().get('em'));
            pageRef.getParameters().put('param1',strings);
            pageRef.setredirect(true);
            return pageRef;
        }
        if(strings!=null || strings!='None')
        {   
            Map<String, RecordType_Function_Mapping__c> recordTypeMappings= RecordType_Function_Mapping__c.getAll();
            if(recordTypeMappings.get(strings)!=null)
            cvmObj.RecordTypeId=recordTypeMappings.get(strings).Record_Type_Id__c;
        }
        else
        {
            
            cvmObj.RecordTypeId=null;
        }
        
        return null;
        
        
    }
*/
    
    
}