/**
 * Description : Test class for WCT_Mobility_Update_RMandPM
 */
 
@isTest

Private class WCT_Mobility_Update_RMandPM_Test
{
    static testMethod void m1(){
        recordtype recType = [select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con = WCT_UtilTestDataCreation.createEmployee(recType.id);
        INSERT con;
        
        WCT_Mobility__c mobRec = WCT_UtilTestDataCreation.createMobility(con.id);
        mobRec.WCT_USI_Resource_Manager__c = con.Email;
        mobRec.WCT_USI_Report_Mngr__c = con.Email;
        INSERT mobRec;
        WCT_Mobility_Update_RMandPM mob = new WCT_Mobility_Update_RMandPM();
    }
}