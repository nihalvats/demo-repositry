public class WCT_OfferSLAUTIL {

//Variable Declaration
public static boolean isBeforeTrigExecuted = false; 
Public static final Set<String> CLOSED_STATUS = new Set<String>{'Offer Sent','Offer Accepted','Offer Declined','Offer Revoked','Offer Cancelled'};
Public static final Set<String> Analyst = new Set<String>{'Offer to Be Extended','Draft in Progress','Returned for Revisions','Manual (Outside of SFDC)'};
Public static final Set<String> Recruiter = new Set<String>{'','Ready for Review'};
Public static final Set<String> Others = new Set<String>{'Offer Sent','Offer Accepted','Offer Declined','Offer Revoked','Offer Cancelled'};
Public static final Set<String> SEND_OFFER = new Set<String>{'Offer Approved'};
Public static final Set<String> Pause_OFFER = new Set<String>{'On Hold per COE','Candidate application incomplete','Missing Offer Details'};

    //Method to Insert Offer Status Insert on creation of Offer 
    public static void createOfferStateOnOfferInsert(List<WCT_Offer__c> offerList){
        List<WCT_Offer_Status__c> offerStatesToInsert = new List<WCT_Offer_Status__c>();
        for(WCT_Offer__c offer : offerList){
            WCT_Offer_Status__c tempState = new WCT_Offer_Status__c();
            tempState.WCT_Enter_State_Date_Time__c = system.Now();
            tempState.WCT_Offer_Current_Status__c = offer.WCT_status__c;
            tempState.WCT_Related_Offer__c = offer.id;
            tempState.WCT_Status__c = 'Open';
            
            //Add the offer state record to a list
            offerStatesToInsert.add(tempState);
        }
        
        if(!offerStatesToInsert.isEmpty()){
            DataBase.SaveResult[] srList = DataBase.Insert(offerStatesToInsert,false);
        }
    
    }
    
    //Method to Insert/Update Offer Status Records on Update of offer Status
    public static void createAndUpdateOfferStateRecords(Map<Id,WCT_Offer__c> offerOldMap , List<WCT_Offer__c> newOfferList)
    {
        system.debug('isBeforeTrigExecuted******'+isBeforeTrigExecuted);
        //if(offer.No_of_times_In_Analyst__c != offerOldMap.get(offer.id).No_of_times_In_Analyst__c || offer.No_of_times_in_Recruiter__c != offerOldMap.get(offer.id).No_of_times_in_Recruiter__c || offer.No_of_times_in_Candidate__c != offerOldMap.get(offer.id).No_of_times_in_Candidate__c)
        if(!isBeforeTrigExecuted)
        {
            Map<Id,List<WCT_Offer_Status__c>> offerwithOfferStateMap = new Map<Id,List<WCT_Offer_Status__c>>();
            List<WCT_Offer_Status__c> offerStatesToCreate = new List<WCT_Offer_Status__c>();
            List<WCT_Offer_Status__c> offerStatesToUpdate = new List<WCT_Offer_Status__c>();
            
            for(WCT_Offer_Status__c offStatus : [SELECT id,WCT_Enter_State_Date_Time__c,WCT_Leave_State_Date_Time__c,WCT_Offer_Current_Status__c,
                                                 WCT_Offer_Previous_Status__c,WCT_Related_Offer__c,WCT_Status__c,WCT_Related_Offer__r.id FROM WCT_Offer_Status__c WHERE WCT_Related_Offer__c IN :offerOldMap.keySet() ORDER BY createdDate DESC]){
                if(offerwithOfferStateMap.ContainsKey(offStatus.WCT_Related_Offer__c)){
                    List<WCT_Offer_Status__c> tempList = new List<WCT_Offer_Status__c>();
                    tempList = offerwithOfferStateMap.get(offStatus.WCT_Related_Offer__c);
                    tempList.add(offStatus);
                    offerwithOfferStateMap.put(offStatus.WCT_Related_Offer__c,tempList);
                }else{
                    List<WCT_Offer_Status__c> tempList = new List<WCT_Offer_Status__c>();
                    tempList.add(offStatus);
                    offerwithOfferStateMap.put(offStatus.WCT_Related_Offer__c,tempList);
                }
            }
            
            for(WCT_Offer__c offer : newOfferList)
            {
                //Check if status Changed
                
                if(offer.No_of_times_In_Analyst__c != offerOldMap.get(offer.id).No_of_times_In_Analyst__c || offer.No_of_times_in_Recruiter__c != offerOldMap.get(offer.id).No_of_times_in_Recruiter__c || offer.No_of_times_in_Candidate__c != offerOldMap.get(offer.id).No_of_times_in_Candidate__c || offer.No_of_time_Offer_Approved__c != offerOldMap.get(offer.id).No_of_time_Offer_Approved__c || offer.Count_Pause_time__c != offerOldMap.get(offer.id).Count_Pause_time__c)
                {
                    
                    WCT_Offer_Status__c tempRec = new WCT_Offer_Status__c();
                    
                    if(offerwithOfferStateMap.ContainsKey(offer.id))
                    {
                    tempRec = offerwithOfferStateMap.get(offer.id)[0];
                    tempRec.WCT_Leave_State_Date_Time__c = system.Now();
                    if(tempRec.WCT_Status__c == 'Open')
                    {
                        tempRec.WCT_Status__c = 'Closed';
                    }
                    Decimal seconds;
                    
                    //Decimal seconds = BusinessHours.diff(Label.WCT_Default_Business_Hours_ID, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;
                    //seconds = BusinessHours.diff(Label.WCT_Default_Business_Hours_ID, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;                        
                    if (offer.WCT_User_Group__c =='US GLS India')
                    {
                         seconds = BusinessHours.diff(Label.WCT_USI_Offer_Business_Hours_ID, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;                        
                    }
                    system.debug('WCT_User_Group__c******'+offer.WCT_User_Group__c);
                    if (offer.WCT_User_Group__c =='United States')
                    {
                    
                    system.debug('Team_Name__c******'+offer.Team_Name__c);
                    if (offer.Team_Name__c =='North East'|| offer.WCT_Team_Mailbox__c =='ushydnecampus2@deloitte.com' )
                    {
                    system.debug('WCT_User_Group__c******'+offer.WCT_User_Group__c);
                    Boolean isWithin1= BusinessHours.isWithin(Label.US_North_East, tempRec.WCT_Leave_State_Date_Time__c);
                    Boolean isWithin2= BusinessHours.isWithin(Label.US_North_East, tempRec.WCT_Enter_State_Date_Time__c);
                    system.debug('isWithin1******'+isWithin1);
                    system.debug('isWithin2******'+isWithin2); 
                    if(isWithin1 && isWithin2)
                    {
                         seconds = BusinessHours.diff(Label.US_North_East, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;                        
                    }
                    else
                    {
                    system.debug('isWithin2******'+isWithin2); 
                    system.debug('isWithin1******'+isWithin1);
                         seconds = BusinessHours.diff(Label.US_Offer_SLA, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;
                         tempRec.Outside_SLA_Record__c = true;
                    }
                    } 
                    if (offer.Team_Name__c =='South East'|| offer.WCT_Team_Mailbox__c =='ushydserecruiting@deloitte.com')
                    {
                    Boolean isWithin1= BusinessHours.isWithin(Label.US_South_East, tempRec.WCT_Leave_State_Date_Time__c);
                    Boolean isWithin2= BusinessHours.isWithin(Label.US_South_East, tempRec.WCT_Enter_State_Date_Time__c); 
                    system.debug('isWithin1******'+isWithin1);
                    system.debug('isWithin2******'+isWithin1); 
                    if(isWithin1 && isWithin2)
                    {
                         seconds = BusinessHours.diff(Label.US_South_East, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;                        
                    }
                    else
                    {
                         seconds = BusinessHours.diff(Label.US_Offer_SLA, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;
                         tempRec.Outside_SLA_Record__c = true;
                    }
                    }
                    if (offer.Team_Name__c =='Central'|| offer.WCT_Team_Mailbox__c =='ushydcentralrecruiting@deloitte.com')
                    {
                    Boolean isWithin1= BusinessHours.isWithin(Label.US_Central, tempRec.WCT_Leave_State_Date_Time__c);
                    Boolean isWithin2= BusinessHours.isWithin(Label.US_Central, tempRec.WCT_Enter_State_Date_Time__c); 
                    if(isWithin1 && isWithin2)
                    {
                         seconds = BusinessHours.diff(Label.US_Central, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;                        
                    }
                    else
                    {
                         seconds = BusinessHours.diff(Label.US_Offer_SLA, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;
                         tempRec.Outside_SLA_Record__c = true;
                    }
                    }
                    if (offer.Team_Name__c =='West'|| offer.WCT_Team_Mailbox__c =='ushydwestrecruiting@deloitte.com')
                    {
                    Boolean isWithin1= BusinessHours.isWithin(Label.US_West, tempRec.WCT_Leave_State_Date_Time__c);
                    Boolean isWithin2= BusinessHours.isWithin(Label.US_West, tempRec.WCT_Enter_State_Date_Time__c); 
                    if(isWithin1 && isWithin2)
                    {
                         seconds = BusinessHours.diff(Label.US_West, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;                        
                    }
                    else
                    {
                         seconds = BusinessHours.diff(Label.US_Offer_SLA, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;
                         tempRec.Outside_SLA_Record__c = true;
                    }
                    }
                    if (offer.Team_Name__c =='Campus Consulting'|| offer.WCT_Team_Mailbox__c =='ushydconsultingcampus2@deloitte.com')
                    {
                    Boolean isWithin1= BusinessHours.isWithin(Label.US_Consulting, tempRec.WCT_Leave_State_Date_Time__c);
                    Boolean isWithin2= BusinessHours.isWithin(Label.US_Consulting, tempRec.WCT_Enter_State_Date_Time__c); 
                    if(isWithin1 && isWithin2)
                    {
                         seconds = BusinessHours.diff(Label.US_Consulting, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;                        
                    }
                    else
                    {
                         seconds = BusinessHours.diff(Label.US_Offer_SLA, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;
                         tempRec.Outside_SLA_Record__c = true;
                    }
                    }
                    
                    if (offer.Team_Name__c =='Advisory'|| offer.Team_Name__c =='Audit'|| offer.Team_Name__c =='Consulting'|| offer.Team_Name__c =='Enabling Areas'|| offer.Team_Name__c =='Federal'|| offer.Team_Name__c =='Tax'|| offer.WCT_Team_Mailbox__c =='ushydadvisoryrecruiting@deloitte.com'|| offer.WCT_Team_Mailbox__c =='ushydauditrecruiting@deloitte.com'|| offer.WCT_Team_Mailbox__c =='ushydrecruitingtechnologyconsulting@deloitte.com'|| offer.WCT_Team_Mailbox__c =='ushydrecruitingsnohcconsulting@deloitte.com'|| offer.WCT_Team_Mailbox__c =='ushydearecruiting@deloitte.com' || offer.WCT_Team_Mailbox__c =='ushydrecruitingfederal2@deloitte.com' || offer.WCT_Team_Mailbox__c =='ushydtaxrecruiting@deloitte.com')
                    {
                    system.debug('Team_Name__c11******'+offer.Team_Name__c);
                    Boolean isWithin1= BusinessHours.isWithin(Label.US_Exp_Offer, tempRec.WCT_Leave_State_Date_Time__c);
                    Boolean isWithin2= BusinessHours.isWithin(Label.US_Exp_Offer, tempRec.WCT_Enter_State_Date_Time__c); 
                    system.debug('isWithin111******'+isWithin1);
                    system.debug('isWithin222******'+isWithin2);
                    if(isWithin1 && isWithin2)
                    {
                         seconds = BusinessHours.diff(Label.US_Exp_Offer, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;                        
                    }
                    else
                    {
                    system.debug('isWithin111112******'+isWithin1);
                    system.debug('isWithin22211******'+isWithin2);
                         seconds = BusinessHours.diff(Label.US_Offer_SLA, tempRec.WCT_Enter_State_Date_Time__c, tempRec.WCT_Leave_State_Date_Time__c)/ 1000;
                         tempRec.Outside_SLA_Record__c = true;
                    }
                    }
                    
                     
                    }
                                                
                    system.debug('query123******'+seconds);
                    Decimal hrs = seconds / 3600;
                    system.debug('query1234******'+hrs);
                    Integer sec = Integer.valueOf(seconds);
                    Decimal min = math.mod(sec , 3600) / 60;
                    system.debug('query12345******'+min);
                    tempRec.WCT_SLA_TIme_in_Status__c = hrs.setScale(3);                        
                    offerStatesToUpdate.add(tempRec);
                    system.debug('query12345******'+tempRec.WCT_SLA_TIme_in_Status__c);
                    
                    if(Analyst.contains(offerOldMap.get(offer.id).WCT_status__c))
                    {
                    system.debug('Analyst::'+Analyst);  
                     
                    tempRec.Count_Analyst_time__c= offer.No_of_times_In_Analyst__c;
                    tempRec.Count_Recruiter_time__c = 0;  
                    tempRec.Count_Candidate_time__c = 0;
                    tempRec.Count_pasue_time__c = 0;
                                        
                    if(offer.WCT_status__c== 'On Hold per COE'|| offer.WCT_status__c== 'Missing Offer Details'|| offer.WCT_status__c== 'Candidate application incomplete')
                    {
                    
                    system.debug('queryAnalyst1******'+offer.No_of_times_In_Analyst__c);
                    system.debug('querypause1******'+offer.Count_Pause_time__c);
                    tempRec.Pasue_status__c = true;
                    tempRec.Time_capture__c = tempRec.WCT_SLA_TIme_in_Status__c;
                    tempRec.Count_Analyst_time__c = 0;
                    }
                    
                    if(offer.WCT_status__c== 'Ready for Review' && offer.Count_Pause_time__c >=1 )
                    {
                    
                    system.debug('queryAnalyst******'+offer.No_of_times_In_Analyst__c);
                    system.debug('querypause******'+offer.Count_Pause_time__c);
                    tempRec.Offer_pause_2__c = true;
                    tempRec.Count_Analyst_time__c = offer.No_of_times_In_Analyst__c - offer.Count_Pause_time__c;
                    tempRec.WCT_SLA_TIme_in_Status__c = offer.Total_Analyst_Time__c + tempRec.WCT_SLA_TIme_in_Status__c;
                    
                    }
                    
                    }
                   
                    system.debug('Recruiter::'+offer.WCT_status__c);
                    if(Recruiter.contains(offerOldMap.get(offer.id).WCT_status__c))
                    {
                        system.debug('Recruiter::'+Recruiter);
                        tempRec.Count_Analyst_time__c = 0;
                        tempRec.Count_Recruiter_time__c = offer.No_of_times_in_Recruiter__c;  
                        tempRec.Count_Candidate_time__c = 0;
                        tempRec.Count_pasue_time__c = 0;
                    }  
                    
                    if(Others.contains(offerOldMap.get(offer.id).WCT_status__c))
                    {
                        system.debug('Recruiter::'+Recruiter);
                        tempRec.Count_Analyst_time__c = 0;
                        tempRec.Count_Recruiter_time__c = 0;  
                        tempRec.Count_Candidate_time__c = offer.No_of_times_in_Candidate__c;
                        tempRec.Count_pasue_time__c = 0;
                        
                    }
                    
                    if(SEND_OFFER.contains(offerOldMap.get(offer.id).WCT_status__c))
                    {
                        system.debug('Recruiter::'+Recruiter);
                        tempRec.Count_Analyst_time__c = 0;
                        tempRec.Count_Recruiter_time__c = 0;  
                        tempRec.Count_Candidate_time__c = 0;
                        tempRec.Count_Approved__c = offer.No_of_time_Offer_Approved__c;
                        tempRec.Overall_Cycle_time__c = offer.Total_Time_to_send_the_Offer__c;
                        tempRec.Count_pasue_time__c = 0;
                    }
                     if(Pause_OFFER.contains(offerOldMap.get(offer.id).WCT_status__c))
                    {
                        system.debug('Recruiter::'+Recruiter);
                        tempRec.Count_Analyst_time__c = 0;
                        tempRec.Count_Recruiter_time__c = 0;  
                        tempRec.Count_Candidate_time__c = 0;
                        tempRec.Count_Approved__c =0;
                        tempRec.Overall_Cycle_time__c = 0;
                        tempRec.Count_pasue_time__c = offer.Count_Pause_time__c;
                        
                    }
                    }
                    
                    //Provide condition for which new record need to be created
                    if(!CLOSED_STATUS.contains(offer.WCT_status__c)){
                        WCT_Offer_Status__c tempState = new WCT_Offer_Status__c();
                        tempState.WCT_Enter_State_Date_Time__c = system.Now();
                        tempState.WCT_Offer_Current_Status__c = offer.WCT_status__c;
                        tempState.WCT_Offer_Previous_Status__c = offerOldMap.get(offer.id).WCT_status__c;
                        tempState.WCT_Related_Offer__c = offer.id;
                        if(offer.WCT_Waiting_for_inputs_from__c <> null){
                            tempState.WCT_Waiting_for_inputs_from__c = offer.WCT_Waiting_for_inputs_from__c;
                        }
                       tempState.WCT_Status__c = 'Open';
                        offerStatesToCreate.add(tempState);
                    }
                    else
                    {
                        WCT_Offer_Status__c tempState = new WCT_Offer_Status__c();
                        tempState.WCT_Enter_State_Date_Time__c = system.Now();
                        tempState.WCT_Offer_Current_Status__c = offer.WCT_status__c;
                        tempState.WCT_Offer_Previous_Status__c = offerOldMap.get(offer.id).WCT_status__c;
                        tempState.WCT_Related_Offer__c = offer.id;
                        
                        if(offer.WCT_Waiting_for_inputs_from__c <> null)
                        {
                            tempState.WCT_Waiting_for_inputs_from__c = offer.WCT_Waiting_for_inputs_from__c;
                        }
                        tempState.WCT_Status__c = 'Post Sent';
                        offerStatesToCreate.add(tempState);
                    
                    }
                    
                }
                
            
            }
            
            if(!offerStatesToUpdate.isEmpty())
            {
                DataBase.SaveResult[] srList = DataBase.Update(offerStatesToUpdate,false);
            }
            
            if(!offerStatesToCreate.isEmpty())
            {
                DataBase.SaveResult[] srList = DataBase.Insert(offerStatesToCreate,false);
            
            }
            
           
             
        }
    
    }

}