@isTest
public class WCT_I_129_FormController_Test
{
     public static testmethod void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        WCT_Mobility__c mob=WCT_UtilTestDataCreation.createMobility(con.id);
        insert mob;
        WCT_Task_Reference_Table__c  taskRef = WCT_UtilTestDataCreation.CreateTaskRefTable();
        taskRef.WCT_Task_for_Object__c = 'WCT_Mobility__c';
        taskRef.WCT_Visa_Type__c = 'WCT_Mobility__c.RecordType';
        insert taskRef; 
        task t=WCT_UtilTestDataCreation.createTask(mob.id);
        t.OwnerId=UserInfo.getUserId();
        t.WCT_Task_Reference_Table_ID__c= taskRef.id;
        t.WCT_Auto_Close__c=false;
        insert t;
        Document document;
        document = new Document();
        document.Body = Blob.valueOf('Some Text');
        document.ContentType = 'application/pdf';
        document.DeveloperName = 'my_document';
        document.IsPublic = true;
        document.Name = 'My Document';
        document.FolderId = [select id from folder where name = 'BME Documentation'].id;
        insert document;

   //  ID tskid =  ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));    
        Attachment attachment = new Attachment();
        attachment.ParentId = t.ID;
        attachment.Name = 'Test Attachment for Parent';
        attachment.Body = document.Body ;
        insert attachment;
        
        Test.starttest();
        //String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
        PageReference pageRef = Page.WCT_I_129_Form;
        Test.setCurrentPage(pageRef); 
        //ApexPages.CurrentPage().getParameters().put('em',encrypt);
        WCT_I_129_FormController controller=new WCT_I_129_FormController();
        WCT_Task_ManageHandler taskInstance=new WCT_Task_ManageHandler();
        GBL_Attachments attachmentHelper = new GBL_Attachments();
        ApexPages.CurrentPage().getParameters().put('taskid',String.valueof(t.id));
        controller=new WCT_I_129_FormController();
        
        //controller.save();
        attachmentHelper.doc=WCT_UtilTestDataCreation.createDocument();
        attachmentHelper.uploadDocument();
        attachmentHelper.uploadRelatedAttachment(t.id);
        controller.getAttachmentInfo();
        controller.save(); 
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        controller.supportAreaErrorMesssage = 'Supported Area Error Message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        //controller.uploadAttachment();
    }
}