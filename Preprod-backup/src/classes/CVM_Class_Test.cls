/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public with sharing class CVM_Class_Test extends SitesTodHeaderController{

    public  static testmethod void test1()
    {
         //Contact con=WCT_UtilTestDataCreation.createContact();
         //con.email='test@deloitte.com';       
         //insert con;
      Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCvm','Employee','test@cvm.com');
      insert con;
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test@cvm.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','cvm@deloitte.com');   
      insert u;
      
      System.debug('TEST 1 LOGGED IN USER NAME');
      System.debug('TEST 1 LOGGED IN USER PROFILE');
      System.debug('TEST 1 LOGGED IN USER RECORD TYPE');
      System.debug('TEST 1 LOGGED IN USER EMAIL');
      System.debug('TEST 1 LOGGED IN USER ROLE');      
      
      
      System.runAs(u)
      {

         // The following code runs as user 'u' 
         System.debug('Current User Test 1: ' + UserInfo.getUserName());
         System.debug('Current Profile Test 1: ' + UserInfo.getProfileId());
        
        
          System.debug('TEST 1 AFTER LOGGED IN USER NAME'+UserInfo.getUserName());
          System.debug('TEST 1 AFTER LOGGED IN USER PROFILE'+UserInfo.getProfileId());
          //System.debug('TEST 1 AFTER LOGGED IN USER RECORD TYPE');
          System.debug('TEST 1 AFTER LOGGED IN USER EMAIL'+UserInfo.getUserEmail());
          System.debug('TEST 1 AFTER LOGGED IN USER ROLE'+UserInfo.getUserRoleId()); 
         
         
        Test.setCurrentPageReference(new PageReference('CVM_Intake_Request'));
        CVM_Intake_Request cvmRequest=new CVM_Intake_Request();
        System.currentPageReference().getParameters().put('param1', 'TD_Vendor_Request');
        //System.currentPageReference().getParameters().put('param1', 'Testing');
        cvmRequest.getfields();
        
        cvmRequest.internAttachmentFile=blob.valueOf('TEst String');
        cvmRequest.ADRSFileName='Test';
        cvmRequest.ProposalFileName='Test';
        cvmRequest.ProposalFile=blob.valueOf('TEst String');
        cvmRequest.isStaff='True';
        cvmRequest.isStaff='True';
        cvmRequest.redirectparam='Test';
        cvmRequest.ismissingReceiptsRequest=True;
        
        System.currentPageReference().getParameters().put('param1', 'New_Vendor_Contract_Agreement_Amendment_Renewal');
        cvmRequest.getfields();
        
        cvmRequest.createContractor1();
        cvmRequest.clearPopup();
        cvmRequest.closePopup();
        cvmRequest.showPopup();
        
        System.currentPageReference().getParameters().put('action', '0');
        System.currentPageReference().getParameters().put('rowNo', '0');
        cvmRequest.manageContractor();
            cvmRequest.SetContractorRecord();
            cvmRequest.updateRecordType();
        
        cvmRequest.strings='Campus_Unpaid_Intern';
        cvmRequest.cvmObj.CVM_Do_you_need_assistance_in_finding_id__c='Yes';
        cvmRequest.saveAction();
        
        cvmRequest.cvmObj.CVM_Do_you_need_assistance_in_finding_id__c='No';
        cvmRequest.saveAction();
        cvmRequest.delmethod();
         
        cvmRequest.AddRow();
        cvmRequest.redirect();
        cvmRequest.advisor();
        cvmRequest.NavigateAudit();
        cvmRequest.TDSubmit();
        cvmRequest.TDupdateRecordType();
        
        /*Calling fields*/
        
        cvmRequest.addAttachment=cvmRequest.addAttachment;
        cvmRequest.setpopdisplay=cvmRequest.setpopdisplay;
        cvmRequest.selectedDocumentList=cvmRequest.selectedDocumentList;
        cvmRequest.pageError=cvmRequest.pageError;
        cvmRequest.pageErrorMessage=cvmRequest.pageErrorMessage;
        cvmRequest.currentDate=cvmRequest.currentDate;
        cvmRequest.contractorSize=cvmRequest.contractorSize;
        cvmRequest.ResumeFileName=cvmRequest.ResumeFileName;
        cvmRequest.jobDescFileName=cvmRequest.jobDescFileName;
        cvmRequest.NVTFileName=cvmRequest.NVTFileName;
        
        

        
        cvmRequest.ProposalFileName=cvmRequest.ProposalFileName;
        cvmRequest.ProposalFile=cvmRequest.ProposalFile;
        cvmRequest.requesttype=cvmRequest.requesttype;
        cvmRequest.formnameexists=cvmRequest.formnameexists;
        cvmRequest.ismissingReceiptsRequest=cvmRequest.ismissingReceiptsRequest;
        cvmRequest.redirectparam=cvmRequest.redirectparam;
        cvmRequest.param1=cvmRequest.param1;
        cvmRequest.titlename=cvmRequest.titlename;
        cvmRequest.vendorcontracttype=cvmRequest.vendorcontracttype;
        cvmRequest.LastName=cvmRequest.LastName;
        cvmRequest.getRenderStaff();
        
        cvmRequest.gotorequestpage();
        cvmRequest.NavigateAdvisory();        
         
        Test.setCurrentPageReference(new PageReference('GBL_Page_Notification'));
        System.currentPageReference().getParameters().put('key', 'cvmthankyou'); 
         //String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');      
         //String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(loggedInContact.email), 'UTF-8');
         //String strEncryptEmail = UserInfo.getUserName();
          
         //System.currentPageReference().getParameters().put('em', strEncryptEmail);
        //SitesTodHeaderController sthc;
          
      }
         
        
        
        
        
    }
    
    public  static testmethod void test2()
    {
        //Contact con=WCT_UtilTestDataCreation.createContact();
        //con.email='test@deloitte.com';       
        //insert con;
        
      Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCvm','Employee','test@cvm.com');
      insert con; 
      String profile = System.label.Label_for_Employee_Profile_Name;  
      User u = WCT_UtilTestDataCreation.createUser('test@cvm.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','cvm@deloitte.com');   
      insert u;
        
      System.debug('TEST 2 LOGGED IN USER NAME');
      System.debug('TEST 2 LOGGED IN USER PROFILE');
      System.debug('TEST 2 LOGGED IN USER RECORD TYPE');
      System.debug('TEST 2 LOGGED IN USER EMAIL');
      System.debug('TEST 2 LOGGED IN USER ROLE');
      
      
      System.runAs(u)
      {
         // The following code runs as user 'u' 
         try
         {
         String s;
         s.toLowerCase();
         System.debug('Current User Test 2: ' + UserInfo.getUserName());
         System.debug('Current Profile Test 2: ' + UserInfo.getProfileId()); 
         
         System.debug('TEST 2 AFTER LOGGED IN USER NAME'+UserInfo.getUserName());
          System.debug('TEST 2 AFTER LOGGED IN USER PROFILE'+UserInfo.getProfileId());
          //System.debug('TEST 2 AFTER LOGGED IN USER RECORD TYPE');
          System.debug('TEST 2 AFTER LOGGED IN USER EMAIL'+UserInfo.getUserEmail());
          System.debug('TEST 2 AFTER LOGGED IN USER ROLE'+UserInfo.getUserRoleId());
         
         //String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
         //String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(loggedInContact.email), 'UTF-8');
         //String strEncryptEmail = UserInfo.getUserName();
         
         CVM_Contractor_Request__c TDRequestInstance = new CVM_Contractor_Request__c();
         TDRequestInstance.CVM_Requestor_Name__c = 'Karthik Raju Gollapalli';
         TDRequestInstance.CVM_Request_Type1__c = 'Advisory Requests';
         insert TDRequestInstance;         
         
         Test.setCurrentPageReference(new PageReference('GBL_Page_Notification'));
         GBL_Page_Notification thankYou=new GBL_Page_Notification(); 
         System.currentPageReference().getParameters().put('key','cvmthankyou');
         System.currentPageReference().getParameters().put('label','My Requests');
         System.currentPageReference().setRedirect(true);
         //System.currentPageReference().getParameters().put('url','https://talent--2016q1dev--c.cs14.visual.force.com/apex/CVM_My_Request');
        
         //GBL_Page_Notification thanksYou=new GBL_Page_Notification();
         //thanksYou.gotohomepage();
         }
         catch(Exception e)
         {
            Exception_Log__c error = WCT_ExceptionUtility.logException('CVM_Intake_Request','CVM_InTake_Request',e.getMessage());
            Test.setCurrentPageReference(new PageReference('GBL_Page_Notification'));
            PageReference errorPageRef = Page.GBL_Page_Notification;
            GBL_Page_Notification thankYou=new GBL_Page_Notification(); 
            System.currentPageReference().getParameters().put('key','cvmthankyou');
            System.currentPageReference().getParameters().put('errorid',error.Name);
            System.currentPageReference().setRedirect(true);
         }
         
      }
                           
    }
    
    public  static testmethod void test3()
    {
        //Contact con=WCT_UtilTestDataCreation.createContact();
         //con.email='test@deloitte.com';       
         //insert con;
      
      Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCvm','Employee','test@cvm.com');
      insert con; 
      String profile = System.label.Label_for_Employee_Profile_Name;  
      User u = WCT_UtilTestDataCreation.createUser('test@cvm.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','cvm@deloitte.com');   
      insert u;
        
      System.debug('TEST 3 LOGGED IN USER NAME');
      System.debug('TEST 3 LOGGED IN USER PROFILE');
      System.debug('TEST 3 LOGGED IN USER RECORD TYPE');
      System.debug('TEST 3 LOGGED IN USER EMAIL');
      System.debug('TEST 3 LOGGED IN USER ROLE');   
         

      System.runAs(u)
      {
         // The following code runs as user 'u' 
         System.debug('Current User Test 3: ' + UserInfo.getUserName());
         System.debug('Current Profile Test 3: ' + UserInfo.getProfileId()); 
         
         System.debug('TEST 3 AFTER LOGGED IN USER NAME'+UserInfo.getUserName());
         System.debug('TEST 3 AFTER LOGGED IN USER PROFILE'+UserInfo.getProfileId());
         //System.debug('TEST 3 AFTER LOGGED IN USER RECORD TYPE');
         System.debug('TEST 3 AFTER LOGGED IN USER EMAIL'+UserInfo.getUserEmail());
         System.debug('TEST 3 AFTER LOGGED IN USER ROLE'+UserInfo.getUserRoleId());
         
         CVM_Contractor_Request__c TDRequestInstance = new CVM_Contractor_Request__c();
         TDRequestInstance.CVM_Requestor_Name__c = 'Karthik Raju Gollapalli';
         TDRequestInstance.CVM_Request_Type1__c = 'Advisory Requests';         
         insert TDRequestInstance;
         
         CVM_Intake_Request cvmRequest=new CVM_Intake_Request();
         
         //cvmRequest.CVM_Request_Type__c = 'Vendor_Contracts';
         //cvmRequest.CVM_Vendor_Contract__c = 'New_Vendor_Contract_Agreement_Amendment_Renewal';
         
         cvmRequest.ProposalFileName = cvmRequest.ProposalFileName;
         cvmRequest.ProposalFile = cvmRequest.ProposalFile;
         
         List<Attachment> attachmentsToCreate= new List<Attachment>();
         Attachment attachNVT = new Attachment();
         attachNVT.body=blob.valueOf('TEst String');
         attachNVT.name=cvmRequest.NVTFileName;
         attachNVT.parentId=TDRequestInstance.id;
         attachmentsToCreate.add(attachNVT);
         
         /*Test.setCurrentPageReference(new PageReference('CVM_Intake_Request'));
         CVM_TD_VENDOR_Form cvmRequest=new CVM_TD_VENDOR_Form();
         System.currentPageReference().getParameters().put('param1', 'New_Vendor_Contract_Agreement_Amendment_Renewal');
         Map<String, RecordType_Function_Mapping__c> recordTypeMappings= RecordType_Function_Mapping__c.getAll();
         if(recordTypeMappings.get(strings)!=null)
         TDRequestInstance.RecordTypeId=recordTypeMappings.get(strings).Record_Type_Id__c;*/
         //String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
         //String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(loggedInContact.email), 'UTF-8');
         //String strEncryptEmail = UserInfo.getUserName();
         Test.setCurrentPageReference(new PageReference('CVM_Request_View_Page')); 
         //System.currentPageReference().getParameters().put('em', strEncryptEmail);
         System.currentPageReference().getParameters().put('id', TDRequestInstance.ID);
         System.currentPageReference().getParameters().put('rnum', TDRequestInstance.Name);
         System.currentPageReference().getParameters().put('rec', TDRequestInstance.CVM_Request_Type1__c);
        
         cvm_request_view_class ReqViewClass = new cvm_request_view_class();
         ReqViewClass.gotorequestpage();
         
         
      }
                                             
    }
    
    
    public  static testmethod void test4()
    {
        //Contact con=WCT_UtilTestDataCreation.createContact();
         //con.email='test@deloitte.com';       
         //insert con;
      
      Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCvm','Employee','test@cvm.com');
      insert con; 
      String profile = System.label.Label_for_Employee_Profile_Name;  
      User u = WCT_UtilTestDataCreation.createUser('test@cvm.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','cvm@deloitte.com');   
      insert u;
        
      System.debug('TEST 4 LOGGED IN USER NAME');
      System.debug('TEST 4 LOGGED IN USER PROFILE');
      System.debug('TEST 4 LOGGED IN USER RECORD TYPE');
      System.debug('TEST 4 LOGGED IN USER EMAIL');
      System.debug('TEST 4 LOGGED IN USER ROLE');   
         

      System.runAs(u)
      {
         // The following code runs as user 'u' 
         System.debug('Current User Test 4: ' + UserInfo.getUserName());
         System.debug('Current Profile Test 4: ' + UserInfo.getProfileId()); 
         
         System.debug('TEST 4 AFTER LOGGED IN USER NAME'+UserInfo.getUserName());
         System.debug('TEST 4 AFTER LOGGED IN USER PROFILE'+UserInfo.getProfileId());
         //System.debug('TEST 4 AFTER LOGGED IN USER RECORD TYPE');
         System.debug('TEST 4 AFTER LOGGED IN USER EMAIL'+UserInfo.getUserEmail());
         System.debug('TEST 4 AFTER LOGGED IN USER ROLE'+UserInfo.getUserRoleId());
         
         CVM_Contractor_Request__c TDRequestInstance = new CVM_Contractor_Request__c();
         TDRequestInstance.CVM_Requestor_Name__c = 'Karthik Raju Gollapalli';
         TDRequestInstance.CVM_Request_Type1__c = 'Advisory Requests';
         TDRequestInstance.CVM_Request_Type__c = 'Vendor_Contracts';
         TDRequestInstance.CVM_Vendor_Contract__c = 'New_Vendor_Contract_Agreement_Amendment_Renewal';       
         insert TDRequestInstance;
         
         CVM_Intake_Request cvmRequest=new CVM_Intake_Request();
         System.currentPageReference().getParameters().put('param1', 'Advisory_Requests');
         List<Attachment> attachmentsToCreate= new List<Attachment>();
         Attachment attachNVT = new Attachment();
         
         attachNVT.parentId=TDRequestInstance.id;
         attachmentsToCreate.add(attachNVT);
         cvmRequest.strings='Advisory_Requests';
         cvmRequest.updateRecordType();
         attachNVT.body=blob.valueOf('TEst String');
         attachNVT.name=cvmRequest.NVTFileName;
         cvmRequest.saveAction();
         
         /*Test.setCurrentPageReference(new PageReference('CVM_Intake_Request'));
         CVM_TD_VENDOR_Form cvmRequest=new CVM_TD_VENDOR_Form();
         System.currentPageReference().getParameters().put('param1', 'New_Vendor_Contract_Agreement_Amendment_Renewal');
         Map<String, RecordType_Function_Mapping__c> recordTypeMappings= RecordType_Function_Mapping__c.getAll();
         if(recordTypeMappings.get(strings)!=null)
         TDRequestInstance.RecordTypeId=recordTypeMappings.get(strings).Record_Type_Id__c;*/
         //String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
         //String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(loggedInContact.email), 'UTF-8');
         //String strEncryptEmail = UserInfo.getUserName();
         Test.setCurrentPageReference(new PageReference('CVM_Request_View_Page')); 
         //System.currentPageReference().getParameters().put('em', strEncryptEmail);
         System.currentPageReference().getParameters().put('id', TDRequestInstance.ID);
         System.currentPageReference().getParameters().put('rnum', TDRequestInstance.Name);
         System.currentPageReference().getParameters().put('rec', TDRequestInstance.CVM_Request_Type1__c);
        
         cvm_request_view_class ReqViewClass = new cvm_request_view_class();
         ReqViewClass.gotorequestpage();
         
         
      }
                                             
    }
    
     public  static testmethod void test5()
    {
        //Contact con=WCT_UtilTestDataCreation.createContact();
         //con.email='test@deloitte.com';       
         //insert con;
      
      Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCvm','Employee','test@cvm.com');
      insert con; 
      String profile = System.label.Label_for_Employee_Profile_Name;  
      User u = WCT_UtilTestDataCreation.createUser('test@cvm.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','cvm@deloitte.com');   
      insert u;
        
      System.debug('TEST 4 LOGGED IN USER NAME');
      System.debug('TEST 4 LOGGED IN USER PROFILE');
      System.debug('TEST 4 LOGGED IN USER RECORD TYPE');
      System.debug('TEST 4 LOGGED IN USER EMAIL');
      System.debug('TEST 4 LOGGED IN USER ROLE');   
         

      System.runAs(u)
      {
      
      CVM_Thank_You_Class CVMThankYouClass = new CVM_Thank_You_Class();
          CVMThankYouClass.gotohomepage();
      }
   } 
    
    
   public  static testmethod void test6()
    {
        //Contact con=WCT_UtilTestDataCreation.createContact();
         //con.email='test@deloitte.com';       
         //insert con;
      
      Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCvm','Employee','test@cvm.com');
      insert con; 
      String profile = System.label.Label_for_Employee_Profile_Name;  
      User u = WCT_UtilTestDataCreation.createUser('test@cvm.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','cvm@deloitte.com');   
      insert u;
        
      System.debug('TEST 4 LOGGED IN USER NAME');
      System.debug('TEST 4 LOGGED IN USER PROFILE');
      System.debug('TEST 4 LOGGED IN USER RECORD TYPE');
      System.debug('TEST 4 LOGGED IN USER EMAIL');
      System.debug('TEST 4 LOGGED IN USER ROLE');   
         

      System.runAs(u)
      {
      
      CVM_RequestorView CVMRequestorView = new CVM_RequestorView();
          CVMRequestorView.gotohomepage();
          CVMRequestorView.displayrequest();
          CVMRequestorView.Init();
          CVMRequestorView.getReportingInfo();
          
      }
   } 
    
}