/**************************************************************************************
Class       :  ELE_employeeDataReadonlyController
Version     : 1.0 
Created Date: 25 April 2015
Function    : Employee detail page which is redirected to when clicked on employee name link in stakeholder dashboard
for closed separation cases
Test Class  : ELE_Separation_Handler_Test

 
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   25/04/2015            Original Version
*************************************************************************************/
public with sharing class ELE_employeeDataReadonlyController extends sitesTodHeaderController {
    public string status {get;set;}
    public Transient List<Attachment> attchList{get;set;}
    public PageReference AddAsset() {
        return null;
    }
    
    public List<Asset__c> asstLst = new List<Asset__c>();
   
    PUBLIC list<Clearance_separation__c> cClearanceToupdate{get;set;}
    PUBLIC list<Asset__c> cAssetToupdate{get;set;}
    public String sTeamName{get;set;}
    public Clearance_separation__c cClToUpdate{get;set;}
    public Asset__c cAstToUpdate{get;set;}
    public string customLabelValue{get;set;}
    public boolean rendering {get;set;}
    public list<Contact> conObj {get;set;}
    public string encryptedEmailString {get;set;}
    public String sEncryptedEmail{get;set;}
    public string email{get;set;}
    // public string sEmailId{get;set;}
    public String stakeholderType{get;set;}
    public String sAssetToupdateID{get;set;}
    public String sClearanceId{get;set;}
    public blob blEncriptedTeamname{get;set;}    
    public string sValue {get;set;}
    public list<cAsset> listcAssetToUpdate{get;set;}
    
    public list<cAsset> getList_cAssetToDisplay(){
        
       // system.debug('>>>>>>>inside getList_cAssetToDisplay>>listcAssetToUpdate>>'+listcAssetToUpdate);
        if(listcAssetToUpdate==null){
            listcAssetToUpdate= new list<cAsset>();  
            asstLst = [select id,ELE_Team_Name__c,ELE_Asset_Status__c,ELE_Asset_status_values__c,ELE_Cost__c, Ele_Description__c, ELE_Number_of_Assets__c,Ele_Payable_Amount__c,
                       ELE_Status__c,ELE_TeamName__c,ELE_Asset_Name__c,Ele_Total_Amount__c,ELE_Waived_Amount__c,ELE_Ref_No__c,ELE_Asset_updatedby__c from Asset__c where /*ELE_Team_Name__c =: sTeamName and*/ ELE_Clearance__c =: sClearanceId order by ELE_Asset_Name__c ASC];  
            for(Asset__c cAssetToupdate : asstLst)
            {
                
                
                List<SelectOption> selectOptions = new List<SelectOption>();
                String str = cAssetToupdate.ELE_Asset_status_values__c;
                listcAssetToUpdate.add(new cAsset(cAssetToupdate, str));
            }
        }  
        
      //  System.debug('listcAssetToUpdate == >'+listcAssetToUpdate);
        return listcAssetToUpdate;
        
    }
    
    
    public String getsDashBoardView(){ 
        
        if(IsVaidUser){
            if(stakeholderType=='p')
            {
                return 'p';
            }
            else if(stakeholderType=='s')
            {
                if(setAccessLevel.size()>0){
                    if(cClToUpdate.ELE_Stakeholder_Designation__c!='Treasury')
                    {
                        return 'Asset';
                    }
                    else
                    {
                        return 'Treasury';
                    }
                }
                return null;
            }
            return null;
        }
        return null;
    }
    public boolean IsVaidUser{get;set;}    
    public string sClearanceOldStatus{get;set;}    
    public set<string> setAccessLevel= new set<string>(); 
    public string sLWD {get;set;}
    public string sDOJ {get;set;}
    public string sELD {get;set;}
    public string sDOR {get;set;}
    public attachment attNotice{get;set;}
    public attachment attSpecialLeav{get;set;}
    public attachment attRehireStatus{get;set;}
    public attachment attNoticePay{get;set;}
    public attachment attPartial{get;set;}
    public list<attachment> List_attToAdd{get;set;}
    
    public ELE_employeeDataReadonlyController()
    {
        
        attNotice= new Attachment();
        attSpecialLeav= new Attachment();
        attRehireStatus= new Attachment();
        attNoticePay= new Attachment();
        attPartial= new Attachment();
        List_attToAdd = new list<Attachment>();
        encryptedEmailString = '' ;
        
        sClearanceId=ApexPages.currentPage().getParameters().get('id');
        customLabelValue = System.Label.TeamNameLabel;
        List<String> strParts = customLabelValue.split('\\,');
        system.debug('strparts == '+strParts);
        sTeamName= ApexPages.currentPage().getParameters().get('tm');
        
        String sCurrentSUserMail=ApexPages.currentPage().getParameters().get('cue');
        stakeholderType=ApexPages.currentPage().getParameters().get('type');
        
        // sEncryptedEmail = ApexPages.currentPage().getParameters().get('em');      
        //email = cryptoHelper.decrypt(sEncryptedEmail);
        
        email=loggedInContact.Email;
        
        // sEmailId = cryptoHelper.encrypt(email);
        attchList = new List<Attachment>();
        list<string > listAccessLevel= new list<String>();
        
        conObj  = [select id,name,email,WCT_Personnel_Number__c,WCT_Gender__c,ELE_Access_Level__c from contact where email = :email and RecordType.Name = 'Employee' limit 1];
        if((conObj.size()>0)&&(conObj[0].ELE_Access_Level__c!=null)){
            listAccessLevel=conObj[0].ELE_Access_Level__c.split(';'); 
            setAccessLevel.addAll(listAccessLevel); 
        }
        String filter= '%'+email+'%';
        
        
        cClearanceToupdate= [select id,ELE_Notice_Period__c,ELE_Contact_Emp_Name__c,
                             ELE_FSS__c,ELE_Comments__c,
                             ELE_Stakeholder_Designation__c,ELE_Clearance_Authority_Type__c,
                             ELE_Status__c,ELE_Notice_Period_Reason__c,
                             ELE_Reason_for_Exit__c,ELE_Notice_or_Severance_pay__c ,
                             ELE_Onsite_Separation__c,ELE_Special_Leaves__c,
                             ELE_Other_recoveries__c,ELE_Notice_period_partial__c,
                             ELE_Last_updatedby__c,ELE_Notice_peroid_served_days__c,ELE_Notice_peroid_waived_days__c,
                             ELE_Status_of_the_payment__c,ELE_Mode_of_Payment_by_the_employee__c,ELE_DD_or_Managers_cheque_Number__c,
                             ELE_Recovery_Bank_Name__c,ELE_Date_on_DD_or_Cheque__c,ELE_Transaction_Remarks__c,ELE_Transaction_reference_no__c,ELE_Recovery_Online_BankName__c,ELE_Date_Online_Transfer__c,
                             ELE_Rehire_Status__c,ELE_Future_member_firm__c,ELE_Notice_period_waived__c,ELE_Partially_waived_or_recovered__c,
                             ELE_Comments_For_Employee__c, ELE_Comments_For_USI_team__c,ELE_Employee_name__c,ELE_Global_Personnel_Number__c,ELE_EMP_Personal_no__c,ELE_DateOfJoining__c ,
                             ELE_Deloitte_Email__c,ELE_Service_Line__c,ELE_Service_Area__c,ELE_Entity__c,ELE_Gender__c,ELE_Designation__c,ELE_Location__c,
                             ELE_Last_Working_Day__c,
                             ELE_Separation__r.ELE_Last_Date__c,ELE_Separation__r.ELE_DOJ__c, 
                             ELE_Separation__r.ELE_Date_of_resignation__c, 
                             ELE_Recovery_Amount1__c,ELE_Partially_recovered__c
                             from Clearance_separation__c where ID =: sClearanceId and ((ELE_Primary_Stakeholder_Email_id__c=:email OR ELE_Additional_Fields__c LIKE :filter)OR(ELE_Stakeholder_Designation__c IN: listAccessLevel))];
        
        
        if(cClearanceToupdate.size()>0){
            cClToUpdate=cClearanceToupdate[0];
            
            Date LWD =  Date.valueOf(cClToUpdate.ELE_Last_Working_Day__c);
            if(LWD!= null)
                sLWD = LWD.format();
            Date DOJ =  Date.valueOf(cClToUpdate.ELE_DateOfJoining__c );
            if(DOJ != null)
                sDOJ = DOJ.format();
            Date ELD =  Date.valueOf(cClToUpdate.ELE_Separation__r.ELE_Last_Date__c );
            if(ELD != null)
                sELD = ELD.format();
            
            Date DOR =Date.valueOf(cClToUpdate.ELE_Separation__r.ELE_Date_of_resignation__c);
            if(DOR != null)
                sDOR = DOR.format();
            
            sClearanceOldStatus=cClToUpdate.ELE_Status__c;
            
            IsVaidUser=true;
        }else
        {
            IsVaidUser=false;
            
        }
        
    }
    
    public class cAsset
    {
        public Asset__c cAsset{get;set;}
        public boolean bSelectedAsset{get;set;}
        public Attachment att{get;set;}
        public List<selectoption> cAccetstatusValues{get;set;}
        public string sSelectedStatusValues {get;set;}
        public string sValue{get;set;}
        public cAsset(Asset__c cAsst, String statusValues)
        {
            this.att = new Attachment();
            cAsset= cAsst;
            bSelectedAsset=false;
            
            
            List<String> stringPts = new List<String>();
            cAccetstatusValues= new List<selectoption>();
            sSelectedStatusValues='--None-';
            
            
            if(cAsst.ELE_Asset_Status__c != null){
                sValue = cAsst.ELE_Asset_Status__c;
                SelectOption selectOption2 = new SelectOption(sValue,sValue);
                cAccetstatusValues.add(selectOption2 );
            }
            
            
            if(statusValues!= null )
                stringPts = statusValues.split('\\/');
            for (Integer i = 0; i < stringPts.size(); i++) {
                if(sValue != stringPts[i] ){
                    SelectOption selectOption1 = new SelectOption(stringPts[i],stringPts[i]);
                    cAccetstatusValues.add(selectOption1);
                    
                }
            }
            
        }
    }
    
}