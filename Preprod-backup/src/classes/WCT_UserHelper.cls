/**
    * Class Name  : WCT_UserHelper 
    * Description : Class will use to create & update User information 
*/

global class WCT_UserHelper {

    /** 
        Method Name  : createUsers
        Return Type  : String
        Description  : Create users from contact as community users.        
    */

    WebService static String createUsers(List<Id> contactIds)
    {
        //Initialize variables
        Id interViewerProfileId=[Select id from profile where name =:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
        Id permissionSetID=[Select id from PermissionSet where name=:WCT_UtilConstants.INTERVIWER_PERMISSION_SET_NAME].Id;
        Id PermissionSetLicenseId = [SELECT Id FROM PermissionSetLicense where DeveloperName = :WCT_UtilConstants.INTERVIWER_PERMISSION_SET_LIC_NAME].Id;
        Id InterViewRoleId = [SELECT Id FROM UserRole where DeveloperName = :WCT_UtilConstants.CTS_Interviewer].Id;
        String returnStatement=null;
        List<Contact> contactList=new List<Contact>();
        List<User> userList=new List<User>();
        Set<String> contactEmails=new Set<String>();
        Map<String,User> oldUserExistMapping=new Map<String,User>();
        Set<Id> existingPermissionSetUserSet=new Set<Id>();
        Map<Id,id> existingPermssionSetAlreadyAssigeeList=new Map<Id,Id>();
        List<PermissionSetAssignment> permissionSetNewAssigmentList=new List<PermissionSetAssignment>();
        set<id> contactSetIds=new Set<id>();
        List<User> userListFlag=new List<User>();
        Map<String,String> timeZoneForUser=new Map<String,String>(); // Region , TimeZonSidCode
        timeZoneForUser.put('EAST','America/New_York');
        timeZoneForUser.put('CENTRAL','America/Chicago');
        timeZoneForUser.put('WEST','America/Los_Angeles');
        timeZoneForUser.put('INDIA/REGION 10','Asia/Kolkata');
        
        
        for(Contact contactRecord:[Select id, name,email,LastName,FirstName,WCT_Home_City__c,WCT_Region__c from Contact where Id IN: contactIds])
        {
            contactList.add(contactRecord);
            contactEmails.add(contactRecord.Email);
        }
        
        if(!contactEmails.IsEmpty())
        {
            for(User userRecord: [Select id,Email,ProfileId,profile.Name,isActive,WCT_Riva_Flag__c from User where Email IN:contactEmails])
            {
                oldUserExistMapping.put((userRecord.Email).toLowerCase(),userRecord);
                existingPermissionSetUserSet.add(userRecord.Id);
                
            }
        }
        if(!existingPermissionSetUserSet.IsEmpty())
        {
            for(PermissionSetAssignment permissionSetAssignee:[SELECT AssigneeId,Id,PermissionSetId FROM PermissionSetAssignment 
                                                                    where AssigneeId IN:existingPermissionSetUserSet and permissionSetId =:permissionSetID]) 
                {
                    existingPermssionSetAlreadyAssigeeList.put(permissionSetAssignee.AssigneeId,permissionSetAssignee.Id);
                    
                }
        }
        if(!ContactList.IsEmpty())
        {
            for(Contact contact : ContactList)
            {
                if(!(oldUserExistMapping.Containskey((contact.Email).toLowerCase())))
                {
                    system.debug('#######New User');
                    String timeZone=null;
                    if(contact.WCT_Region__c != null && timeZoneForUser.ContainsKey((contact.WCT_Region__c).toUpperCase()) )
                    {
                        timeZone=timeZoneForUser.get(contact.WCT_Region__c);
                    }
                    User u= WCT_UserHelper.createUser(contact, interViewerProfileId, InterViewRoleId,timeZone );
                    
                    userList.add(u);
                    contactSetIds.add(contact.Id);
                }
                else if(oldUserExistMapping.Containskey((contact.Email).toLowerCase()))
                {
                    system.debug('#######Existing User');
                     if((oldUserExistMapping.get((contact.Email).toLowerCase())).ProfileId!=interViewerProfileId  )
                     {
                        if(!existingPermssionSetAlreadyAssigeeList.ContainsKey((oldUserExistMapping.get((contact.Email).toLowerCase())).Id) || (oldUserExistMapping.get((contact.Email).toLowerCase())).isActive==false || (oldUserExistMapping.get((contact.Email).toLowerCase())).WCT_Riva_Flag__c==false )
                        {
                            User user=new USer(id=(oldUserExistMapping.get((contact.Email).toLowerCase())).Id);
                            if(!existingPermssionSetAlreadyAssigeeList.ContainsKey((oldUserExistMapping.get((contact.Email).toLowerCase())).Id))
                            {
                                PermissionSetAssignment assignmentUser=new PermissionSetAssignment();
                                assignmentUser.AssigneeId=(oldUserExistMapping.get((contact.Email).toLowerCase())).Id;
                                assignmentUser.PermissionSetId=permissionSetID;
                                permissionSetNewAssigmentList.add(assignmentUser);
                                
                                user.WCT_Riva_Flag__c=true;
                           }
                           
                             if((oldUserExistMapping.get((contact.Email).toLowerCase())).WCT_Riva_Flag__c==false)
                            {
                                user.WCT_Riva_Flag__c=true;
                                returnStatement=Label.WCT_User_Riva_Flag_Active;
                            }
                             if((oldUserExistMapping.get((contact.Email).toLowerCase())).isActive==false)
                            {
                                user.WCT_Riva_Flag__c=true;
                                user.IsActive=true;
                                returnStatement=Label.WCT_User_Activate;
                            }
                            
                            userListFlag.add(user);
                            contactSetIds.add(contact.Id);
                            
                            
                        }
                        else
                        {
                            returnStatement=Label.WCT_User_Pemission_Granted;
                        }
                        
                     }
                     else if((oldUserExistMapping.get((contact.Email).toLowerCase())).isActive==false)
                     {
                         User user=new USer(id=(oldUserExistMapping.get((contact.Email).toLowerCase())).Id);
                         user.IsActive=true;
                          user.WCT_Riva_Flag__c=true;
                         userListFlag.add(user);
                         contactSetIds.add(contact.Id);
                         returnStatement=Label.WCT_User_Activate;
                     }
                    else if((oldUserExistMapping.get((contact.Email).toLowerCase())).WCT_Riva_Flag__c==false)
                     {
                         User user=new USer(id=(oldUserExistMapping.get((contact.Email).toLowerCase())).Id);
                         user.WCT_Riva_Flag__c=true;
                         userListFlag.add(user);
                         contactSetIds.add(contact.Id);
                         returnStatement=Label.WCT_User_Riva_Flag_Active;
                     }
                    else
                     {
                         
                        returnStatement=Label.WCT_User_Present;
                     }
                }
            
            }
            set<id> userIdSetNeedPerm = new set<id>();
            if(!userList.IsEmpty())
            {
                Database.SaveResult[] srList = Database.insert(userList, false);
                
                for(Database.SaveResult sr: srList)
                {
                    if(sr.isSuccess())
                    {
                        returnStatement= Label.WCT_User_Successfully_created;
                        userIdSetNeedPerm.add(sr.getId());
                    }
                    if(!sr.IsSuccess())
                    {
                        for(Database.Error err : sr.getErrors())
                        {
                        
                            if(returnStatement !=null)
                                returnStatement=returnStatement + err.getMessage();
                            else
                                returnStatement=err.getMessage();
                        }
                    }
                
                }
                list<PermissionSetLicenseAssign> perSetLicLst = new list<PermissionSetLicenseAssign>();
                if(!userIdSetNeedPerm.IsEmpty()){
                    for(Id userId : userIdSetNeedPerm){
                        PermissionSetLicenseAssign perSetLicARec =  new PermissionSetLicenseAssign();
                        perSetLicARec.AssigneeId = userId;
                        perSetLicARec.PermissionSetLicenseId = PermissionSetLicenseId;
                        perSetLicLst.add(perSetLicARec);
                    }
                }
                if(!perSetLicLst.IsEmpty()){insert perSetLicLst;}
            }
            if(!userListFlag.IsEmpty())
            {
                update userListFlag;
            }
            if(!permissionSetNewAssigmentList.IsEmpty())
            {
                try
                {
                    insert permissionSetNewAssigmentList;
                    returnStatement=Label.WCT_User_exist_permissionSet_Granted;
                }catch(DMLException e )
                {
                    returnStatement=e.getMessage();
                }
                
                
            }
            
        }    
        else
        {
            returnStatement='No record Found';
        }
        WCT_UserHelper.updateContacts(contactSetIds, true);
        return returnStatement;
    
    }
    
    // User Mapping
    
    private static User createUser(Contact contact, id interViewerProfileId, id InterViewRoleId,String timezone)
    {
        
        User user=new User();
        user.Alias=(contact.Name).substring(0,4);
        user.UserName=contact.Email+'.wct.prd';// +'.interviwerUser';
        user.Email=contact.Email;
        user.City = contact.WCT_Home_City__c;
        user.CommunityNickname=(contact.Email).split('@')[0];
        user.EmailEncodingKey='ISO-8859-1';
        user.EmailPreferencesAutoBcc=false;
        user.EmailPreferencesAutoBccStayInTouch=false;
        user.EmailPreferencesStayInTouchReminder=true;
        user.FederationIdentifier=(contact.Email).split('@')[0];
        user.LanguageLocaleKey='en_US';
        user.LastName=contact.LastName;
        user.FirstName=contact.FirstName;
        user.LocaleSidKey='en_US';
        user.ProfileId=interViewerProfileId;
        if(timezone==null)
            user.TimeZoneSidKey='America/Chicago';
        else
            user.TimeZoneSidKey=timezone;
        user.UserPermissionsAvantgoUser=false;
        user.UserPreferencesTaskRemindersCheckboxDefault=true;
        user.ReceivesAdminInfoEmails=false;
        user.ReceivesInfoEmails=false;
        user.UserPreferencesEventRemindersCheckboxDefault=true;
        user.WCT_Riva_Flag__c=true;
        user.IsActive=true;
        user.UserRoleId=InterViewRoleId;
        
            return user;
    }

    private Static User assignPermissionSet(User u)
    {
        // Assiging Permission Set to User
        return u;
        
    }
    
    @future(Callout=true)
    private static void updateContacts(Set<Id> contactIds,Boolean interviewFlag)
    {
        List<Contact> contactList=new List<Contact>();
        for(Id contactId:contactIds)
        {
            Contact contact=new Contact(id=contactId);
            contact.WCT_Interviewer__c=interviewFlag;
            contactList.add(contact);
            
        }
        if(!contactList.IsEmpty())
        {
            try{
                update contactList;
            }
            catch(DMLException e)
            {
                throw e;
            }
        }
    }
    
    /** 
        Method Name  : deactivateUser
        Return Type  : String
        Description  : Deactivate Users from community users.        
    */
    
    WebService Static String deactivateUsers(List<Id> contactIds)
    {
        List<Contact> contactList=new List<Contact>();
        set<id> contactSetIds=new Set<id>();
        Id interViewerProfileId=[Select id from profile where name =:WCT_UtilConstants.INTERVIWER_PROFILE_NAME].Id;
        Id permissionSetID=[Select id from PermissionSet where name=:WCT_UtilConstants.INTERVIWER_PERMISSION_SET_NAME].Id;
        String returnStatement=null;
        Set<String> userEmails=new Set<String>();
        List<User> userListNeedsToBeDeactivated=new List<User>();
        Set<String> contactEmails=new Set<String>();
        Map<String,User> oldUserExistMapping= new Map<String,User>();
        Map<Id, id> existingPermssionSetAlreadyAssigeeList=new Map<Id,Id>();
        List<PermissionSetAssignment> permissionSetDeletionList=new List<PermissionSetAssignment>();
        Set<Id> oldUserIds=new Set<Id>();
        List<User> userListFlag=new List<User>();
        for(Contact contactRecord:[Select id, name,email,LastName,FirstName,WCT_Home_City__c from Contact where Id IN: contactIds])
        {
            contactList.add(contactRecord);
            contactEmails.add(contactRecord.Email);
        }
        if(!contactEmails.IsEmpty())
        {
            for(User userRecord: [Select id,Email,ProfileId,profile.Name from User where Email IN:contactEmails and IsActive=true])
            {
                oldUserExistMapping.put((userRecord.Email).toLowerCase(),userRecord);
                oldUserIds.add(userRecord.Id);
            }
        }
        if(!oldUserIds.IsEmpty())
        {
            for(PermissionSetAssignment permissionSetAssignee:[SELECT AssigneeId,Id,PermissionSetId FROM PermissionSetAssignment 
                                                                    where AssigneeId IN:oldUserIds and permissionSetId =:permissionSetID])
            {
                existingPermssionSetAlreadyAssigeeList.put(permissionSetAssignee.AssigneeId,permissionSetAssignee.Id);
            }
        
        }
        if(!contactList.IsEmpty()) {
                
            for(Contact contact:contactList)
            {
                if((oldUserExistMapping.Containskey((contact.Email).toLowerCase())))
                {
                    if((oldUserExistMapping.get((contact.Email).toLowerCase())).ProfileId==interViewerProfileId)
                    {
                        User u=new User(Id=(oldUserExistMapping.get((contact.Email).toLowerCase())).Id);
                        u.IsActive=false;
                        u.WCT_Riva_Flag__c=false;
                        u.FederationIdentifier=null;
                        userListNeedsToBeDeactivated.add(u);
                        contactSetIds.add(contact.Id);
                    }
                    else
                    {
                        if(existingPermssionSetAlreadyAssigeeList.ContainsKey((oldUserExistMapping.get((contact.Email).toLowerCase())).Id))
                        {
                            PermissionSetAssignment permissionSetRec=new PermissionSetAssignment(id=existingPermssionSetAlreadyAssigeeList.get((oldUserExistMapping.get((contact.Email).toLowerCase())).Id));
                            permissionSetDeletionList.add(permissionSetRec);
                            contactSetIds.add(contact.Id);
                            User user=new USer(id=(oldUserExistMapping.get((contact.Email).toLowerCase())).Id);
                            user.WCT_Riva_Flag__c=false;
                            userListFlag.add(user);
                        }
                        else
                        {
                            returnStatement=Label.WCT_NO_Permisison_Exist;
                        }
                    }
                }
            }
            if(!userListNeedsToBeDeactivated.IsEmpty())
            {
                Database.SaveResult[] srList = Database.update(userListNeedsToBeDeactivated, false);
                for(Database.SaveResult sr: srList)
                {
                    if(sr.IsSuccess())
                    {
                        returnStatement=Label.WCT_User_Inactive;
                    }
                    if(!sr.IsSuccess())
                    {
                        for(Database.Error err : sr.getErrors())
                        {
                                
                            if(returnStatement !=null)
                                returnStatement=returnStatement + err.getMessage();
                            else
                                returnStatement=err.getMessage();
                         }
                     }
                }
              }
            if(!userListFlag.IsEmpty())
            {
                update userListFlag;
            }
            if(!permissionSetDeletionList.IsEmpty())
            {
                Database.DeleteResult[] srList = Database.delete(permissionSetDeletionList, false);
                for(Database.DeleteResult sr: srList)
                {
                    if(sr.IsSuccess())
                    {
                        returnStatement=Label.WCT_PermisisonSet_Deleted;
                    }
                    if(!sr.IsSuccess())
                    {
                        for(Database.Error err : sr.getErrors())
                        {
                                
                            if(returnStatement !=null)
                                returnStatement=returnStatement + err.getMessage();
                            else
                                returnStatement=err.getMessage();
                         }
                     }
                }
              }
                
       }
       WCT_UserHelper.updateContacts(contactSetIds,false ); 
       return returnStatement;    
        
    
    }


}