/*****************************************************************************************
    Name    : SCCQuickCreateToolbar_Controller
    Desc    : Controller Class used to create a Case from Service Cloud Console
              'SCCQuickCreateToolbar' Page
    Approach: 
                
                                                
    Modification Log : 
---------------------------------------------------------------------------
 Developer                      Date            Description
---------------------------------------------------------------------------
 Sreenivasa Munnangi            8 Aug, 2013         Created 
 Nageswara Rao B                18 Mar, 2015        Modified to populate correct value into 'Reported By' field
******************************************************************************************/
public with sharing class SCCQuickCreateToolbar_Controller {

    // The record Id in context. This is passed in when exposed as a Console Component
    public String currRecordId {get; set;}

    // The type of object that is currently in context
    public String currObjectType {
        get {
            if (currRecordId != null && currObjectType == null) {
                // Get the key prefix from the id
                String currKeyPrefix = ((String)currRecordId).substring(0, 3);
    
                // Get the record type
                Schema.sObjectType currsObjectType = sObjectByKeyPrefix.get(currKeyPrefix);
                
                currObjectType =  currsObjectType.getDescribe().getName();
            }
            return currObjectType;
        }
        set;
    }

    // sObject representing the current record
    public sObject currRecord {
        get {
            if (currRecord == null && currRecordId != null) {
                // Support for contacts - uncomment to use
               
                if (currObjectType == 'Contact') {
                    currRecord = Database.query('SELECT ID, AccountId, Name from Contact where id = \'' + currRecordId + '\'');     
                }
                if (currObjectType == 'Case') {
                    currRecord = Database.query('SELECT ID, CaseNumber,ContactId,Contact.Name from Case where id = \'' + currRecordId + '\'');     
                }
                else if (currObjectType != null) {
                    currRecord = Database.query('SELECT ID, Name from ' + currObjectType + ' where id = \'' + currRecordId + '\'');
                }
            }
            return currRecord ;
        }
        private set;
    }
    

    // List of the available record types for the user
    public List<SelectOption> caseRecordTypes {
        get {
            if (caseRecordTypes != null) return caseRecordTypes;
        
            // Use dynamic Apex to generate a select list of Record Ids + Name
            Map<Id,Schema.RecordTypeInfo> caseRTMapById = Schema.SObjectType.Case.getRecordTypeInfosById();

            caseRecordTypes = new List<SelectOption>();
            caseRecordTypes.add(new SelectOption('', 'Select One...'));
                
            // Format the select list in case record type name order
            for (RecordType recType : [Select id, Name, DeveloperName, SobjectType from RecordType where SobjectType = 'Case' order by Name]) {
                // Get the schema info for the record type using the record type id
                Schema.RecordTypeInfo caseRT = caseRTMapById.get(recType.id);

                // the record type is available to the logged in user
                // Exclude the Master record type as it shouldn't be visible
                if (caseRT.isAvailable() && caseRT.getName() != 'Master') { 
                    caseRecordTypes.add(new SelectOption(caseRT.getRecordTypeId(), caseRT.getName()));
                }
            }
            return caseRecordTypes; 
        }
        private set;
    }

    // Generate the URL to open the new subtab in
    public String caseCreateURLPrefix{
        get{
            if(caseCreateURLPrefix == null){
                // Use a page reference to generate the URL
                
                String caseReportedById =  System.Label.Case_Reported_By_Id;
                // Prefix to create the case (/500/e)
                PageReference pageRef = new PageReference('/' + Case.sObjectType.getDescribe().getKeyPrefix() + '/e');
                pageRef.getParameters().put('isdtp','vw');
                // return URL
                //pageRef.getParameters().put('retURL','/' + currRecordId );
                system.debug('currObjectType : ' + currObjectType);
                // add context (e.g. the current account)
                if(currObjectType == 'Case'){
                    pageRef.getParameters().put('def_contact_id',  ((Case) currRecord).ContactId);
                    //pageRef.getParameters().put('CF'+caseReportedById,  ((Case) currRecord).ContactId); 
                    pageRef.getParameters().put('CF'+caseReportedById+'_lkid',  ((Case) currRecord).ContactId);
                    pageRef.getParameters().put('CF'+caseReportedById,  ((Case) currRecord).Contact.Name); 
                    
                    //system.debug('caseReportedById: ' + caseReportedById); 
                    //system.debug('currObjectType : ' + ((Case) currRecord).ContactId); 
                    //system.debug('currObjectType : ' + ((Case) currRecord).ContactId); 
                    //system.debug('currObjectType : ' + ((Case) currRecord).Contact.Name);
                }
                
                // add context for contacts (uncomment to make it work)
                 else if (currObjectType == 'Contact') {
                    pageRef.getParameters().put('def_contact_id', currRecordId);
                    //pageRef.getParameters().put('CF'+caseReportedById, currRecordId);
                    pageRef.getParameters().put('CF'+caseReportedById+'_lkid', currRecordId);
                    pageRef.getParameters().put('CF'+caseReportedById, ((Contact) currRecord).Name);
                    

                    //system.debug('caseReportedById: ' + caseReportedById); 
                    //system.debug('currObjectType : ' + currRecordId); 
                    //system.debug('currObjectType : ' + currRecordId); 
                    //system.debug('currObjectType : ' + ((Contact) currRecord).Name);
                  
                }
               
                String SupportArea='';

                // additional parameters to make the URL work properly
                pageRef.getParameters().put('ent', Case.sObjectType.getDescribe().getName());
                caseCreateURLPrefix = pageRef.getURL();
                String CurrUserRoleDeveloperName=[select id,DeveloperName from  userRole where Id=:UserInfo.getUserRoleId()].DeveloperName;
                if(CurrUserRoleDeveloperName=='CIC_Manager' ||CurrUserRoleDeveloperName=='CIC_Agent')
                    SupportArea='US';
                else
                {if(CurrUserRoleDeveloperName=='CIC_Manager_USI' ||CurrUserRoleDeveloperName=='CIC_Agent_USI')                
                    SupportArea='INDIA';
                 else{
                 if(System.Label.PSN_User_Roles.contains(CurrUserRoleDeveloperName))
                    SupportArea='US-PSN';}
                }
                     
                if(SupportArea!='')
                    caseCreateURLPrefix =caseCreateURLPrefix +'&'+System.Label.WCT_Case_SupportAreaID+'='+SupportArea;                
                        
            }
            //system.debug('caseCreateURLPrefix : ' + caseCreateURLPrefix);
            return caseCreateURLPrefix;
        }
        private set;
    }

    // Map of sObject Types by their key prefix
    private static Map<String, Schema.sObjectType> sObjectByKeyPrefix {
        get {
            if (sObjectByKeyPrefix == null) {
                sObjectByKeyPrefix = new Map<String, Schema.sObjectType>();
                for (Schema.SObjectType sObj : Schema.getGlobalDescribe().values()) {
                    sObjectByKeyPrefix.put(sObj.getDescribe().getKeyPrefix(), sObj);
                }
            }
            return sObjectByKeyPrefix;
        }
        
        private set;
    }    

    
    // Constructor
    public SCCQuickCreateToolbar_Controller () {
        // Get the current record Id
        currRecordId = ApexPages.currentPage().getParameters().get('Id');
    }
    
    

}