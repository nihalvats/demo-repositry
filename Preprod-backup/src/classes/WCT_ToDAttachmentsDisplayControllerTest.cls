/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test  
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class WCT_ToDAttachmentsDisplayControllerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Test.startTest();
        
        
        Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCvm','Employee','test@cvm.com');
      insert con;
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test@cvm.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','cvm@deloitte.com');   
      insert u;
        
        System.runAs(u)
      {
     
        Contact conObj=WCT_UtilTestDataCreation.createContact();
        conObj.Email='test@deloitte.com';
        insert conObj;
         
        Case caseObj=WCT_UtilTestDataCreation.createAdhocCase(conObj.id);
        //caseObj.CaseNumber='12345';
        caseObj.WCT_Support_Area__c='USA';
        insert caseObj;
        
        String encrypt=EncodingUtil.urlDecode(CryptoHelper.encrypt(conObj.email), 'UTF-8');
        //setting the todCaseUS page as a test page
        PageReference pageRefs = Page.todCaseUS;
        Test.setCurrentPage(pageRefs); 
        ApexPages.CurrentPage().getParameters().put('id',caseObj.id);
        ApexPages.CurrentPage().getParameters().put('em',encrypt);
        
        Attachment attchObj=WCT_UtilTestDataCreation.createAttachment(caseObj.id);
        insert attchObj;
        list<Attachment> lstAttachment=new list<Attachment>();
        lstAttachment.add(attchObj);
        
        //creating an instance of the ToDAttachmentsDisplayController class
        ApexPages.StandardController sc = new ApexPages.StandardController(conObj);
        ToDAttachmentsDisplayController accoppextn = new ToDAttachmentsDisplayController();
        accoppextn.listAttachments=lstAttachment;
       
        system.assertEquals(accoppextn.listAttachments.size(), 1);
      }
        Test.stopTest();
    }
}