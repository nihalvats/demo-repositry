/**************************************************************************************
Apex class       : Emp_ResignationFormController
Version          : 1.0 
Created Date     : 25 April 2015
Function         : Controller for US resignation form

* Developer          Date            Description 			Test Class	
* -------------------------------------------------------------------------------------                
* Deloitte        25/04/2016        Original Version	ELE_resignation_Contrl_Tests
*************************************************************************************/
public class Emp_ResignationFormController extends SitesTodHeaderController{
    
    public Case casesep{get;set;}
    public ELE_Separation__c oelesep {get;set;}
    Public Contact resgncontact {get;set;}
    public string pageerrormsg {get;set;}
    public Exit_Survey__c oelext{get;set;}
    public boolean panel1{get;set;}
    public boolean panel2{get;set;}
    public boolean panel3{get;set;}
    public boolean panel4{get;set;}
    Public Document doc {get;set;}
   	public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public boolean reset {get; set;}
    public String targetField{get; set;}
    public string employeenameid{get;set;}
    public string employeenamefortesxt{get;set;}
    public string Submittedbyvalue {get;set;}
    public boolean checkseattle{get;set;}
    public GBL_Attachments attachmentHelper{get; set;}
    
    public Emp_ResignationFormController (){
        string volRecordType=Schema.SObjectType.case.getRecordTypeInfosByName().get('Employee Lifecycle Events - Voluntary Separations').getRecordTypeId();
        casesep= new Case();
        casesep.recordtypeId=volRecordType;
        panel1=true;
        panel2=false;
        panel3=false;
        panel4=false;
        oelext = new Exit_Survey__c();
        oelesep = new ELE_Separation__c ();
        resgncontact = new contact();
        doc = new Document();
        
        pageerrormsg ='false';
        reset = false;
        attachmentHelper= new GBL_Attachments(); 
        
        if(loggedInContact !=null){
            resgncontact=[select id, WCT_Hiring_location__c,WCT_Office_City_Personnel_Subarea__c from contact where ID=:loggedInContact.id];
            
            if(resgncontact.WCT_Office_City_Personnel_Subarea__c != null){
                if(resgncontact.WCT_Office_City_Personnel_Subarea__c.containsIgnoreCase('Seattle')){
                    checkseattle  = true;
                }
                else{
                    checkseattle=false;
                }
            }
        }
    }
    
    //Method to enable Screen 2 
    public void next1(){
        
        
        panel1=false;
        panel2=true;
        panel3=false; 
        panel4=false;
        doc = new Document();
        
    }
    //Method to enable Screen 3 
    public void next2(){
        
        panel1=false;
        panel2=false;
        panel3=true; 
        panel4=false;
        doc = new Document();
        
    }
    //Method to enable Screen 4
    public void next3(){
        
        panel1=false;
        panel2=false;
        panel3=false;
        panel4=true;
        
    }
    
    /*************************** 
Method Name  : Updateques
Return Type  : Pagereference
Description  : Saves CI Questionnaire   
*******************************/
    Public pagereference Updateques()
    {
        id sepid = apexpages.currentpage().getparameters().get('id');
        id empid = apexpages.currentpage().getparameters().get('empid');
        String extRecordType=Schema.SObjectType.Exit_Survey__c.getRecordTypeInfosByName().get('CI Questionnaire').getRecordTypeId();
        oelext.RecordTypeID=extRecordType;
        oelext.Case__c = sepid;
        oelext.Employee__c=empid;
        system.debug('oelesep.id'+oelesep.id);
        insert oelext;
        system.debug('olesep'+oelesep);
        
        
        PageReference pageRef= new pagereference('/'+sepid);
        pageRef.setRedirect(true);
        return pageref;
    }
    
    /*************************** 
Method Name  : Savecase
Return Type  : Pagereference
Description  : Saves Resignation form   
*******************************/
    
    Public Pagereference Savecase(){
        try{
        pageerrormsg = 'false';
       
      	 PageReference pageRef= Page.GBL_Page_Notification;
        if(casesep.ELE_Resignation_Submitted_on_Behalf_US__c == 'No'){
            casesep.ContactId= loggedInContact.id;
            
        }
        if(casesep.ELE_Resignation_Submitted_on_Behalf_US__c  == 'Yes'){
            casesep.WCT_ReportedBy__c = loggedInContact.id;           
            
        }
        list<MAP_ROL_SEPL__c> trtd = MAP_ROL_SEPL__c.getall().values();
        for (MAP_ROL_SEPL__c maprol:trtd)
        {if(casesep.ELE_Reason_for_Leaving__c == maprol.Reason_of_Leaving__c){
            casesep.WCT_SeparationReason__c=maprol.Separation_Reason__c;
        }
         
        }
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= Label.Case_Assignment_Rule_Id;
        casesep.RecordTypeId =Schema.SObjectType.Case.getRecordTypeInfosByName().get('Employee Lifecycle Events - Voluntary Separations').getRecordTypeId();
        
        // system.debug('24123421' + casesep.RecordTypeId);
        casesep.Status='New';
        casesep.Priority='3-Medium';
        casesep.Origin='Web';
        casesep.WCT_Category__c='Separations';
        casesep.WCT_SubCategory1__c='Voluntary Separation';
        casesep.Subject='Voluntary Separation via webform';
        casesep.Description='Voluntary Separation via webform';
        casesep.WCT_Support_Area__c='US';
        casesep.setOptions(dmlOpts);
        
        insert casesep;
        List<case> Cseid=[Select id,CaseNumber from case where id=:casesep.id];
        
        //Globla attachment method
        attachmentHelper.uploadRelatedAttachment(casesep.Id);
        //Passing parameters in url
        //key provided here is in list of names
            if(casesep.ELE_Resignation_Submitted_on_Behalf_US__c == 'No'){
                pageRef.getParameters().put('key','success');
            }
            if(casesep.ELE_Resignation_Submitted_on_Behalf_US__c == 'Yes'){
                pageRef.getParameters().put('key','success1');
            }
        pageRef.getParameters().put('cid',Cseid[0].CaseNumber);
        PageRef.getParameters().put('res',casesep.ELE_Resignation_Submitted_on_Behalf_US__c );
        PageRef.getParameters().put('can',employeenamefortesxt);
        
        return pageRef;  
        }
        catch (Exception e) {
            
            Exception_Log__c log=WCT_ExceptionUtility.logException('Emp_ResignationFormController', 'ELE_Resignation_Form_new', e.getMessage());
            
            pagereference pg = new pagereference('/apex/GBL_Page_Notification?key=ErrorMsg&expCode='+log.Name);
            pg.setRedirect(true);
            return pg;
        }
            
    }
   
    
}