@isTest(seeAllData = true)
public class Event_checkin_Cont_Test
{
String eid = ApexPages.currentPage().getParameters().get('eid');
   
    public static testMethod void Event_checkin_ContTest()
     {
     Contact con = new Contact();
     con.LastName = 'abc';
     con.FirstName = 'xyz';
     con.Email = 'abc@invalid.com';
     con.Teams__c = 'Campus';
     con.WCT_Candidate_School__c =  'School of Arts';
     con.Degree_program__c = 'BA';
     insert con;
     
     Account acc=new Account(name='test');                  
     insert acc;  
     
     School__c school = new School__c(name = 'New York University');
     insert school;
        
     Event__c event = new Event__c();
     event.School__c = school.id;
     event.Name = 'Test eventsss1';
     event.Event_Cap__c = 1000;
     event.Account__c = acc.id;
     event.Start_Date_Time__c=system.now();
     event.end_date_time__c=system.now();
     insert event;
 
     Event_Registration_Attendance__c ObjEventReg = new  Event_Registration_Attendance__c();
     ObjEventReg.Contact__c = con.id;
     ObjEventReg.Event__c = event.id;       
     ObjEventReg.Attended__C = true;    
     ObjEventReg.Attending__C = true;  
     ObjEventReg.Attendee_Type__c = 'Alumni';
     ObjEventReg.invited__C=true;
     insert ObjEventReg;
        
     PageReference pageRef = Page.checkin;      
     Test.setCurrentPage(pageRef);
     ApexPages.currentPage().getParameters().put('eid', event.id);
     ApexPages.currentPage().getParameters().put('objcontactid',con.id);
     Event_checkin_Cont  eventcheckin = new Event_checkin_Cont();
     eventcheckin.confirmp = false;
     eventcheckin.welcomep= false;
     eventcheckin.thankyoup= false;
     eventcheckin.search();
     eventcheckin.existedcontact();
     eventcheckin.returntocheckin();
    }
    
    
     static testMethod void Testcheckinconfirm()
     {
       contact pract=new contact(Lastname='Rajesh',Email='avittal@deloitte.com',FirstName='Narayan',Title='SiteDemo', WCT_Candidate_School__c ='School of Arts');
       insert pract;
     
       Account acc=new Account(name='test');                  
       insert acc;  
                                
       School__c school = new School__c(name = 'New York University');
        insert school;
        
       Event__c event = new Event__c();
       event.School__c = school.id;
       event.Name ='National';
       event.Event_Cap__c = 1000;
       event.Account__c = acc.id;
       event.Start_Date_Time__c=system.now();
       event.end_date_time__c=system.now();
       insert event;
       
       Event_Registration_Attendance__c   conneweventReg = new Event_Registration_Attendance__c();
       conneweventReg.Contact__c = pract.id;
       conneweventReg.Event__c = event.id;       
       conneweventReg.Attended__C = true;    
       conneweventReg.Attending__C = true;  
       conneweventReg.Attendee_Type__c = 'Alumni';
       conneweventReg.invited__C=true;
       insert conneweventReg;
      
       conneweventReg.Attendee_Type__c = '--Select--';
       update conneweventReg;
      
       PageReference pageRef = Page.checkin;      
       Test.setCurrentPage(pageRef);
       ApexPages.currentPage().getParameters().put('eid', event.id);
        
       Event_checkin_Cont pracReg = new Event_checkin_Cont();
       pracReg.getsaoptions();
       pracReg.getnirgradmonth();
       pracReg.getsaOptions();
       pracReg.getposofint();
       pracReg.getnirgradyear();
       pracReg.getnirdegreetype();
       pracReg.getnirfss();
       pracReg.getnirserpre();
       pracReg.getlocpf();
       pracreg.eventreg= conneweventReg;
       // pracReg.Search();
       pracReg.checkinconfirm();
       conneweventReg.Attendee_Type__c = '';
       update conneweventReg;
       pracReg.checkinconfirm();
                
      }

      static testMethod void Testnewregistration() 
      {
      contact cont=new contact(Lastname='Hirthvik',Email='hrithik@gmail.com',FirstName='Roshan',Title='SiteDemo',Phone='6865434257');
      insert cont;
   
      Account acc=new Account(name='test1234');       
      insert acc;  
      
      School__c school = new School__c(name = 'New York University');
        insert school;
      Event__c event = new Event__c();
      event.School__c = school.id;
      event.Name ='National';
      event.Event_Cap__c = 1000;
      event.Account__c = acc.id;
      event.Start_Date_Time__c=system.now();
      event.end_date_time__c=system.now();
      insert event;    
      
      PageReference pageRef = Page.checkin;      
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('eid', event.id);
         ApexPages.currentPage().getParameters().put('SC', school.Name);
      // ApexPages.StandardController sc = new ApexPages.StandardController(neweventReg);  
       Event_checkin_Cont createCon = new Event_checkin_Cont();
       createcon.consp='Forensic';
       createcon.conlocpf='Boise';
       createcon.conpi='Campus FT';
       createcon.confp='Financial Advisory Services (FAS)';
       createcon.getsaoptions();
       createcon.getnirgradmonth();
       createcon.getsaOptions();
       createcon.getposofint();
       createcon.getnirgradyear();
       createcon.getnirdegreetype();
       createcon.getnirfss();
       createcon.getnirserpre();
       createcon.getlocpf();
       contact tempcont=new contact(Lastname='Hirthvik',Email='hrithik@gmail.com',FirstName='Roshan',Title='SiteDemo',Phone='6865434257');
       createCon.con = tempcont; 
       createCon.newregistration();
}
////////////////////////////////NIR EVENT INSERT/////////////////////////////////////////////////

     static testMethod void TestNIRnewregistration() {
     
     School__c school = new School__c(name = 'New York University');
     insert school;
     
     contact nircont=new contact();
     nircont.Lastname='Hirthviknir';
     nircont.wct_school_name__c=school.id;
     nircont.Email='vkniknir@gmail.com';
     nircont.FirstName='Roshan';
     nircont.Title='SiteDemo';
     nircont.Phone='6865434257';
     insert nircont;
         
     Account acc=new Account(name='test1234');       
     insert acc;  
    
     Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    
     User u = new User(Alias = 'standNIRt', Email='standardNIRuser@testorg.com', 
     EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
     LocaleSidKey='en_US', ProfileId = p.Id, 
     TimeZoneSidKey='America/Los_Angeles', UserName='standardNIRuser@testorg.com');
              
     Event__c nirevent = new Event__c();
     Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Event__c; 
     Map<String,Schema.RecordTypeInfo> deloitteeventRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
     Id rtId = deloitteeventRecordTypeInfo.get('National Inclusion Recruiting').getRecordTypeId();
      
     nirevent.Name ='NIR_National';
     nirevent.recordtypeid = rtId;
     nirevent.cr_recruiter_lead1__c=u.id;
     nirevent.Target_Demographic__c='Asian';
     nirevent.Event_Cap__c = 1000;
     nirevent.Start_Date_Time__c=system.now();
     nirevent.end_date_time__c=system.now();
     insert nirevent;    
        
     Event_Registration_Attendance__c   nirneweventReg = new Event_Registration_Attendance__c();
     nirneweventReg.Contact__c = nircont.id;
     nirneweventReg.Event__c = nirevent.id;       
     nirneweventReg.Attended__C = true;    
     nirneweventReg.Attending__C = true;  
     nirneweventReg.Attendee_Type__c = 'Alumni';
     nirneweventReg.invited__C=true;
     insert nirneweventReg;
        
     PageReference pageRef = Page.checkin;      
     Test.setCurrentPage(pageRef);
     ApexPages.currentPage().getParameters().put('eid', nirevent.id);
  
     contact nircontup=new contact();
     nircontup.id=nircont.id;
     update nircontup;
  
     Event_Registration_Attendance__c   nirneweventRegtoupdate = new Event_Registration_Attendance__c();
     nirneweventRegtoupdate =[select  id,name from Event_Registration_Attendance__c where event__c=:nirevent.id and Contact__c=:nircontup.id];
     Event_Registration_Attendance__c   nirneweventRegupdate = new Event_Registration_Attendance__c();
     nirneweventRegupdate.id=nirneweventRegtoupdate.id;
     nirneweventRegupdate.FSS__c='Financial Advisory Services (FAS)';
     nirneweventRegupdate.service_Area__c='Forensic';
     nirneweventRegupdate.level__c='Campus FT';
     nirneweventRegupdate.Location_Preference__c='Boise';
     update nirneweventRegupdate;
     
     Event_Registration_Attendance__c   nirneweventReginsert = new Event_Registration_Attendance__c();
     nirneweventReginsert.FSS__c='Financial Advisory Services (FAS)';
     nirneweventReginsert.Contact__c = nircont.id;
     nirneweventReginsert.Event__c = nirevent.id;
     nirneweventReginsert.service_Area__c='Forensic';
     nirneweventReginsert.level__c='Campus FT';
     nirneweventReginsert.Location_Preference__c='Boise';
     insert nirneweventReginsert;
      
     Event_checkin_Cont createCon = new Event_checkin_Cont();
     createcon.consp='Forensic';
     createcon.consp='Technology';
     createcon.confirstname='Firstrtfst'; 
     createcon.conlastname='TESTesttt'; 
     createcon.conemail='TSTTTTTTTT@gmail.com';
     createcon.condegreetype='Masters'; 
     createcon.conegm='May'; 
     createcon.conegy='2015';
     createcon.conlocpf='Boise';
     createcon.conpi='Campus FT';
     createcon.conmajor='CCCEESSSSSFR';
     createcon.congpa=3.3;
     createcon.conschool='schoollsch';
     createcon.conmobile='mobileelibome';
     createcon.nirwelcomep=false;
     createcon.nirthankyoup=true;
     createcon.nirexisted=false;
     createcon.confirmuser='xxxxyyyyaaaa';
     createcon.selectedsa='werttrewwert';
     createcon.confp='Financial Advisory Services (FAS)';
     createcon.confp='Consulting';
     createcon.getsaoptions();
     createcon.getnirgradmonth();
     createcon.getsaOptions();
     createcon.getposofint();
     createcon.getnirgradyear();
     createcon.getnirdegreetype();
     createcon.getnirfss();
     createcon.getnirserpre();
     createcon.getlocpf();
     contact tempcont=new contact(Lastname='Hirthvik',Email='hrithik@gmail.com',FirstName='Roshan',Title='SiteDemo',Phone='6865434257');
     createCon.con = tempcont; 
     createCon.newregistration();
     createCon.nir_con.add(nircont);
     createCon.searchemail='vkniknir@gmail.com';
     createCon.existedcontact();
     createCon.search();
     }
 
 /////////////////////////////////////////PostConferenceFeedback Class test////////////////////////////////
 static testMethod void TestNIRPostConferenceFeedback() {
     
     School__c school = new School__c(name = 'New York University');
     insert school;
     
     contact nircont=new contact();
     nircont.Lastname='Hirthviknir';
     nircont.wct_school_name__c=school.id;
     nircont.Email='vkniknir@gmail.com';
     nircont.FirstName='Roshan';
     nircont.Title='SiteDemo';
     nircont.Phone='6865434257';
     insert nircont;
         
     Account acc=new Account(name='test1234');       
     insert acc;  
    
     Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    
     User u = new User(Alias = 'standNIRt', Email='standardNIRuser@testorg.com', 
     EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
     LocaleSidKey='en_US', ProfileId = p.Id, 
     TimeZoneSidKey='America/Los_Angeles', UserName='standardNIRuser@testorg.com');
              
     Event__c nirevent = new Event__c();
     Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Event__c; 
     Map<String,Schema.RecordTypeInfo> deloitteeventRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
     Id rtId = deloitteeventRecordTypeInfo.get('National Inclusion Recruiting').getRecordTypeId();
      
     nirevent.Name ='NIR_National';
     nirevent.recordtypeid = rtId;
     nirevent.cr_recruiter_lead1__c=u.id;
     nirevent.Target_Demographic__c='Asian';
     nirevent.Event_Cap__c = 1000;
     nirevent.Start_Date_Time__c=system.now();
     nirevent.end_date_time__c=system.now();
     insert nirevent;    
        
     Event_Registration_Attendance__c   nirneweventReg = new Event_Registration_Attendance__c();
     nirneweventReg.Contact__c = nircont.id;
     nirneweventReg.Event__c = nirevent.id;       
     nirneweventReg.Attended__C = true;    
     nirneweventReg.Attending__C = true;  
     nirneweventReg.Attendee_Type__c = 'Alumni';
     nirneweventReg.invited__C=true;
     insert nirneweventReg;
        
     PageReference pageRef = Page.checkin;      
     Test.setCurrentPage(pageRef);
     ApexPages.currentPage().getParameters().put('eid', nirevent.id);
  
     contact nircontup=new contact();
     nircontup.id=nircont.id;
     update nircontup;
  
     Event_Registration_Attendance__c   nirneweventRegtoupdate = new Event_Registration_Attendance__c();
     nirneweventRegtoupdate =[select  id,name from Event_Registration_Attendance__c where event__c=:nirevent.id and Contact__c=:nircontup.id];
     Event_Registration_Attendance__c   nirneweventRegupdate = new Event_Registration_Attendance__c();
     nirneweventRegupdate.id=nirneweventRegtoupdate.id;
     nirneweventRegupdate.FSS__c='Financial Advisory Services (FAS)';
     nirneweventRegupdate.service_Area__c='Forensic';
     nirneweventRegupdate.level__c='Campus FT';
     nirneweventRegupdate.Location_Preference__c='Boise';
     update nirneweventRegupdate;
     
     Event_Registration_Attendance__c   nirneweventReginsert = new Event_Registration_Attendance__c();
     nirneweventReginsert.FSS__c='Financial Advisory Services (FAS)';
     nirneweventReginsert.Contact__c = nircont.id;
     nirneweventReginsert.Event__c = nirevent.id;
     nirneweventReginsert.service_Area__c='Forensic';
     nirneweventReginsert.level__c='Campus FT';
     nirneweventReginsert.Location_Preference__c='Boise';
     insert nirneweventReginsert;
     PageReference pageRefe = Page.PostConferenceFeedback;      
     Test.setCurrentPage(pageRefe);
     ApexPages.currentPage().getParameters().put('eid', nirevent.id);
     ApexPages.currentPage().getParameters().put('cid',nircont.id); 
     ApexPages.currentPage().getParameters().put('erid',nirneweventReg.id);
     PostConferenceFeedback createCon = new PostConferenceFeedback();
     createCon.getnirpoi();
     createCon.getnirstat();
     createCon.savenir();
     }

 
 
    }