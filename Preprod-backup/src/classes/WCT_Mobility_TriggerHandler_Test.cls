@isTest
private class WCT_Mobility_TriggerHandler_Test
{
    public static testmethod void m1()
    {
        Test.startTest();
        WCT_Mobility_TriggerHandler mobTrig = new WCT_Mobility_TriggerHandler();
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        
        WCT_Mobility__c mobRec = new WCT_Mobility__c();
        recordtype rt1=[select id from recordtype where name='Employment Visa'];
        mobRec.RecordTypeId = rt1.id;
        mobRec.WCT_Mobility_Employee__c = con.Id;
        mobRec.WCT_Mobility_Status__c = 'New';
        INSERT mobRec;
        
        Task tempTask = new Task();
        tempTask.WhatId = mobRec.Id;
        tempTask.Status = 'Not Started';
        INSERT tempTask;
        
        mobRec.WCT_Mobility_Status__c = 'Employee on Travel';
        mobRec.WCT_Travel_End_Date_Extended__c = true;
        UPDATE mobRec;
        
        mobRec.WCT_Accompanied_Status__c = true; 
        mobRec.WCT_DependentVisaTask_Created__c = false;
        mobRec.WCT_Mobility_Status__c = 'Onboarding Completed';
        
        
        Update mobRec;
        Test.stopTest();
    }
    public static testmethod void m2()
    {
        Test.startTest();
        WCT_Mobility_TriggerHandler mobTrig = new WCT_Mobility_TriggerHandler();
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        
        WCT_Mobility__c mobRec = new WCT_Mobility__c();
        recordtype rt1=[select id from recordtype where name='Employment Visa'];
        mobRec.RecordTypeId = rt1.id;
        mobRec.WCT_Mobility_Employee__c = con.Id;
        mobRec.WCT_Mobility_Status__c = 'New';
        INSERT mobRec;
        
        Task tempTask = new Task();
        tempTask.WhatId = mobRec.Id;
        tempTask.Status = 'Not Started';
        INSERT tempTask;
        
        mobRec.WCT_Accompanied_Status__c = true; 
        mobRec.WCT_DependentVisaTask_Created__c = false;
        mobRec.WCT_Mobility_Status__c = 'Onboarding Completed';
        
        Update mobRec;
        Test.stopTest();
    }
    
    public static testmethod void m3()
    {
        WCT_Mobility_TriggerHandler mobTrig = new WCT_Mobility_TriggerHandler();
        
        WCT_Mobility__c mob=new WCT_Mobility__c();
        recordtype rt1=[select id from recordtype where name='Employment Visa'];
        mob.RecordTypeId = rt1.id;
        mob.WCT_Mobility_Status__c = 'Initiate Onboarding';
        INSERT mob;   
       
     } 
     
     public static testmethod void m4()
    {
        Test.startTest();
        WCT_Mobility_TriggerHandler mobTrig = new WCT_Mobility_TriggerHandler();
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        
        WCT_Mobility__c mobRec = new WCT_Mobility__c();
        recordtype rt1=[select id from recordtype where name='Employment Visa'];
        mobRec.RecordTypeId = rt1.id;
        mobRec.WCT_Mobility_Employee__c = con.Id;
        mobRec.WCT_Mobility_Status__c = 'New';
        INSERT mobRec;
        
        Task tempTask = new Task();
        tempTask.WhatId = mobRec.Id;
        tempTask.Status = 'Not Started';
        INSERT tempTask;
        
        mobRec.WCT_Insurance_Task_Status__c = true; 
        mobRec.WCT_Mobility_Status__c = 'Travel Extended';
        
        Update mobRec;
        Test.stopTest();
    }
    
     public static testmethod void m5()
    {
        Test.startTest();
        WCT_Mobility_TriggerHandler mobTrig = new WCT_Mobility_TriggerHandler();
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        
        WCT_Mobility__c mobRec = new WCT_Mobility__c();
        recordtype rt1=[select id from recordtype where name='Business Visa'];
        mobRec.RecordTypeId = rt1.id;
        mobRec.WCT_Mobility_Employee__c = con.Id;
        mobRec.WCT_Mobility_Status__c = 'New';
        INSERT mobRec;
        
        Task tempTask = new Task();
        tempTask.WhatId = mobRec.Id;
        tempTask.Status = 'Not Started';
        INSERT tempTask;
        
        mobRec.WCT_OGC_Status__c = false; 
        mobRec.OGC_Task_status__c = true;
        mobRec.WCT_Existing_Business_Visa__c = 'Yes';
        Update mobRec;
        Test.stopTest();
    }
    
    public static testmethod void m6()
    {
        Test.startTest();
        WCT_Mobility_TriggerHandler mobTrig = new WCT_Mobility_TriggerHandler();
        
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        
        WCT_Mobility__c mobRec = new WCT_Mobility__c();
        recordtype rt1=[select id from recordtype where name='Employment Visa'];
        mobRec.RecordTypeId = rt1.id;
        mobRec.WCT_Mobility_Employee__c = con.Id;
        mobRec.WCT_Mobility_Status__c = 'New';
        INSERT mobRec;
        
        Task tempTask = new Task();
        tempTask.WhatId = mobRec.Id;
        tempTask.Status = 'Not Started';
        INSERT tempTask;
        
        mobRec.WCT_Mobility_Status__c = 'Onboarding Completed'; 
        mobRec.Background_Investigation_Long_Term__c = false;
        mobRec.WCT_Assignment_Type__c = 'Long Term';
        Update mobRec;
        Test.stopTest();
    }
}