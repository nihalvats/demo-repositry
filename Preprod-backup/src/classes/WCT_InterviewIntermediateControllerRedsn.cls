public with sharing class WCT_InterviewIntermediateControllerRedsn {
String InterviewId,JunctionId,contactid,interviewdue;
    public WCT_InterviewIntermediateControllerRedsn() {
    InterviewId = System.currentPagereference().getParameters().get('id');
    JunctionId= System.currentPagereference().getParameters().get('juncId');
    contactid=System.currentPagereference().getParameters().get('cntid');
    interviewdue=System.currentPagereference().getParameters().get('intdue');
    }
    public PageReference RedirectToInterview(){
	    if(InterviewId!='' && InterviewId != null){    
	    	list<WCT_Interview_Junction__c> IntvJuncRec = [select id from WCT_Interview_Junction__c 
	                                                        where WCT_Interview__c=:InterviewId
	                                                        and WCT_Interviewer__c=:UserInfo.getUserId()];
	        if(JunctionId==null && JunctionId==''){
	            JunctionId = IntvJuncRec[0].Id;
	        }
	        if(!IntvJuncRec.IsEmpty()){
	            if(IntvJuncRec.size()>1){
	                return new PageReference('/apex/WCT_InterviewDetailRedesign?id='+JunctionId+'&cntid='+contactid+'&interviewdue='+interviewdue+'&View=GIView');           
	            }
	            else{
	                return new PageReference('/apex/WCT_InterviewDetailRedesign?id='+JunctionId+'&cntid='+contactid+'&interviewdue='+interviewdue);
	            }
	        }
	        else{
	        return null;
	        }
	    }
	    else{
	        return null;
		}
    }
}