@isTest
private class WCT_Add_Update_DepVisaInfo_Test {

    static testMethod void myUnitTest() {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Id mobId=[select id, Name from recordtype where sObjectType = 'WCT_Mobility__c' AND Name='Employment Visa'].Id;
        Id immId=[select id, Name from recordtype where sObjectType = 'WCT_Immigration__c' AND Name='L1 Visa'].Id;
        //Id employeeId = [SELECT id, Name, RecordTypeId, Email FROM Contact WHERE RecordType.Name = 'Employee'].Id;
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        WCT_Immigration__c immi=WCT_UtilTestDataCreation.createImmigration(con.id);
        immi.WCT_Immigration_Status__c='Visa Approved';
        immi.RecordTypeId = immId;
        immi.WCT_Visa_Type__c='L1B Individual';
        upsert immi;
        
     
        
        Contact dep= new Contact();
        
        dep.WCT_Primary_Contact__c= con.id;
        dep.Email = 'test@gamil.com';
        dep.FirstName= 'Test 1';
        dep.LastName= 'Test 1';
        dep.WCT_UID_Number__c='T1234567';
        insert dep;
       
        datetime startdate=date.Today().adddays(10);
        datetime enddate=date.Today().adddays(20);
       
        
        Contact dep1= new Contact();
        dep1.RecordTypeId = rt.id;
        dep1.Email = 'test@gamil.com';
        dep1.WCT_Primary_Contact__c= con.id;
        dep1.FirstName= 'Test 1';
        dep1.LastName= 'Test 1';
        dep1.WCT_UID_Number__c='T1234567';
        insert dep1;
        Contact dep2= new Contact();
        dep2.RecordTypeId = rt.id;
        dep2.Email = 'email.tst@deloitte.com';
        
        dep2.FirstName= 'Test 1';
        dep2.LastName= 'Test 1';
       
        insert dep2;
       
        Profile p = [SELECT Id FROM Profile WHERE Name='System administrator']; 
        User ur1 = new User(Alias = 'sample1', Email='email.tst@deloitte.com', 
            EmailEncodingKey='UTF-8', LastName='testct1', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,CommunityNickname='testct1',IsActive=true,
            FederationIdentifier='testct1', 
            TimeZoneSidKey='America/Los_Angeles', UserName='testct1@deloitte.com.wct.prd');
      
        insert ur1;
        system.runas(ur1){
         
        WCT_Add_Update_DepVisaInfoController controller = new WCT_Add_Update_DepVisaInfoController();
       
       
       controller.getParameterInfo();
       controller.init();
       
        system.debug('controller***'+ controller );
        
        GBL_Attachments attachmentHelper = new GBL_Attachments();
        
        attachmentHelper.doc=WCT_UtilTestDataCreation.createDocument();
        attachmentHelper.uploadDocument();
        attachmentHelper.uploadRelatedAttachment(dep.id);
       // attachmentHelper.docIdList.add(dep.id);
        
        controller.pageError=true;
        controller.pageErrorMessage='error message';
        controller.employeeEmail='email.tst@deloitte.com';      
        controller.getImmigrationDetails();
        controller.getAttachmentInfo();
        controller.getExistingDepInfo();
        controller.visaStatus='Have an existing Visa';
        controller.visaStartDate=startdate.format('MM/dd/yyyy');
        controller.visaEndDate=enddate.format('MM/dd/yyyy');
        controller.save();
        controller.exVisaStartDate=startdate.format('MM/dd/yyyy');
        controller.exVisaEndDate=enddate.format('MM/dd/yyyy');
        controller.updateonly();
        controller.save();
        controller.visaStatus='Applying for a new Visa';
        controller.employeeFirstName='Test';
        controller.employeeLastName='Test';
        controller.employeeRelationship='Husband/Wife';
        controller.dependentGender='Female';
        controller.UIDNumber='12345';
        controller.preferredLocation='Test';
        controller.visaStartDate=startdate.format('MM/dd/yyyy');
        controller.visaEndDate=enddate.format('MM/dd/yyyy');
        controller.save();
        controller.tempRefresh();
        controller.visaStatus='Applying for a new Visa';
        controller.employeeFirstName='Test';
        controller.employeeLastName='Test';
        controller.employeeRelationship='Husband/Wife';
        controller.dependentGender='Female';
        controller.visaStartDate=startdate.format('MM/dd/yyyy');
        controller.visaEndDate=enddate.format('MM/dd/yyyy');
        controller.save();
        controller.visaStatus='Applying for a new Visa';
        controller.employeeFirstName='Test';
        controller.employeeLastName='Test';
        controller.UIDNumber='12345678';
        controller.employeeRelationship='Husband/Wife';
        controller.dependentGender='Female';
        controller.preferredLocation='Test';
        controller.visaStartDate=startdate.format('MM/dd/yyyy');
        controller.visaEndDate=enddate.format('MM/dd/yyyy');
        controller.save();
        controller.visaStatus='Have an existing Visa';
        controller.visaStartDate=startdate.format('MM/dd/yyyy');
        controller.save();
        controller.updateonly();
        controller.visaStatus='Have an existing Visa';
        controller.visaStartDate=startdate.format('MM/dd/yyyy');
        controller.visaEndDate=enddate.format('MM/dd/yyyy');
        controller.save();
        controller.exVisaStartDate=startdate.format('MM/dd/yyyy');
        controller.exVisaEndDate=enddate.format('MM/dd/yyyy');
        controller.updateonly();
        // controller.attachmentHelper.docIdList= null;
        //controller.pageErrorMessage = 'Attachment is required to submit the form.';
        PageReference pageRef = new PageReference ('/apex/WCT_Add_Update_DependentVisaInfoThankYou');

        Test.setCurrentPage(pageRef);
        System.debug('Calling Done');
        controller.done();
    
        controller.supportAreaErrorMesssage = 'Supported Area Error Message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        
        }
    }
    static testMethod void myUnitTest1() {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Id mobId=[select id, Name from recordtype where sObjectType = 'WCT_Mobility__c' AND Name='Employment Visa'].Id;
        Id immId=[select id, Name from recordtype where sObjectType = 'WCT_Immigration__c' AND Name='L1 Visa'].Id;
        //Id employeeId = [SELECT id, Name, RecordTypeId, Email FROM Contact WHERE RecordType.Name = 'Employee'].Id;
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con;
        
        WCT_Immigration__c immi=WCT_UtilTestDataCreation.createImmigration(con.id);
        immi.WCT_Immigration_Status__c='Visa Approved';
        immi.RecordTypeId = immId;
        immi.WCT_Visa_Type__c='L1B Individual';
        upsert immi;
        
       
        
        Contact dep= new Contact();
        
        dep.WCT_Primary_Contact__c= con.id;
        dep.Email = 'test@gamil.com';
        dep.FirstName= 'Test 1';
        dep.LastName= 'Test 1';
        dep.WCT_UID_Number__c='T1234567';
        insert dep;
       
        datetime startdate=date.Today().adddays(10);
        datetime enddate=date.Today().adddays(20);
       
        
        Contact dep1= new Contact();
        dep1.RecordTypeId = rt.id;
        dep1.Email = 'test@gamil.com';
        dep1.WCT_Primary_Contact__c= con.id;
        dep1.FirstName= 'Test 1';
        dep1.LastName= 'Test 1';
        dep1.WCT_UID_Number__c='T1234567';
        insert dep1;
        Contact dep2= new Contact();
        dep2.RecordTypeId = rt.id;
        dep2.Email = 'email.tst@deloitte.com';
        
        dep2.FirstName= 'Test 1';
        dep2.LastName= 'Test 1';
       
        insert dep2;
       
        Profile p = [SELECT Id FROM Profile WHERE Name='System administrator']; 
        User ur1 = new User(Alias = 'sample1', Email='email.tst@deloitte.com', 
            EmailEncodingKey='UTF-8', LastName='testct1', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id,CommunityNickname='testct1',IsActive=true,
            FederationIdentifier='testct1', 
            TimeZoneSidKey='America/Los_Angeles', UserName='testct1@deloitte.com.wct.prd');
      
        insert ur1;
        system.runas(ur1){
         
        WCT_Add_Update_DepVisaInfoController controller = new WCT_Add_Update_DepVisaInfoController();
       
       
       controller.getParameterInfo();
       controller.init();
       
        system.debug('controller***'+ controller );
        
        
       controller.attachmentHelper.docIdList.add(dep.id);
         controller.save();   
         controller.done(); 
      
        // controller.attachmentHelper.docIdList= null;
        //controller.pageErrorMessage = 'Attachment is required to submit the form.';
        PageReference pageRef = new PageReference ('/apex/WCT_Add_Update_DependentVisaInfoThankYou');

        Test.setCurrentPage(pageRef);
        System.debug('Calling Done');
        controller.done();
    
        controller.supportAreaErrorMesssage = 'Supported Area Error Message';
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'A user account for this email address already exists, would you like to reset your password?');
        ApexPages.addMessage(msg);
        
        }
    }
    
     }