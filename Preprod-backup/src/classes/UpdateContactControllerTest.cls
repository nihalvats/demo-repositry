@isTest
public class UpdateContactControllerTest {

    public static final id candRecTypeId=WCT_Util.getRecordTypeIdByLabel('Contact','Employee');

    static testMethod void testUpdateContactController(){
        list<contact> conlist = new list<contact>();
        contact con = new contact();
        con.lastname = 'test';
        con.WCT_Personnel_Number__c = 12345;
        con.WCT_Benefit_plan__c = 'EE';
        con.Dependent__c = 'Freind';
        con.Plan_Num__c = 123;
        con.Recordtypeid = candRecTypeId;
        conlist.add(con); 
        insert conlist;
        
        string csvcontent =  '12345,567890,98765,456789'+'\n';
        csvcontent = csvcontent + '12345,567890,98765,456789';
        string namefile = 'test.csv';
        string extension = namefile.substring(namefile.lastindexof('.')+1);
        integer totalsuccess = 0;
        integer totalunsuccess = 0;
        string error ;
        test.startTest();
        pagereference p = page.UpdateContact;
       Test.SetCurrentPage(p); 
       
        UpdateContactController imp= new UpdateContactController();
        imp.contentFile = Blob.valueof(csvContent);
        imp.totalsuccessrec=0;
        imp.totalrecords=1;
        imp.Error='sample';
        imp.totalunsuccessrec=0;
        imp.totalsuccessreccount=1;
        imp.nameFile = 'sample test';
        imp.size = 1;
        List<UpdateContactController.MyWrapper> wraplist = new List<UpdateContactController.MyWrapper>();
        imp.wrapper = wraplist;
        imp.getuploadedAccounts();
        imp.pg();
        imp.ReadFile();
       // imp.b=false; 
        
       
        //wrapper class coverage
        String benefitPlanValue = 'benefit';
        Integer personnelNumberValue = 1;
        Integer planNumberValue = 2;
        String dependentPlanValue = 'dependent';
        
        UpdateContactController.MyWrapper testWrapper = new UpdateContactController.MyWrapper(personnelNumberValue,benefitPlanValue,dependentPlanValue,planNumberValue);
        //System.assertEquals(benefitPlanValue,testWrapper.BenefitPlan1,'Benefit plan value not set properly!');
        System.assertEquals(personnelNumberValue,testWrapper.PersonnelNumber1,'Personnel number value not set properly!');
        System.assertEquals(planNumberValue,testWrapper.PlanNumber1,'Plan number value not set properly!');
        System.assertEquals(dependentPlanValue,testWrapper.DependentPlan1,'Dependent plan value not set properly!');

        
    }
    public testmethod static void test1(){
        list<contact> conlist = new list<contact>();
        contact con = new contact();
        con.lastname = 'test';
        con.WCT_Personnel_Number__c = 2345780;
        con.WCT_Benefit_plan__c = 'EE';
        con.Dependent__c = 'Freind';
        con.Plan_Num__c = 123;
        con.Recordtypeid = candRecTypeId;
        conlist.add(con); 
        insert conlist;
        UpdateContactController imp= new UpdateContactController();
        imp.contentFile = null;
        imp.totalsuccessrec=0;
        imp.totalrecords=1;
        imp.Error='sample';
        imp.totalunsuccessrec=0;
        imp.totalsuccessreccount=1;
        imp.nameFile = 'sample test';
        imp.size = 1;
        List<UpdateContactController.MyWrapper> wraplist = new List<UpdateContactController.MyWrapper>();
        imp.wrapper = wraplist;
        imp.getuploadedAccounts();
        imp.pg();
        imp.ReadFile();
    }
}