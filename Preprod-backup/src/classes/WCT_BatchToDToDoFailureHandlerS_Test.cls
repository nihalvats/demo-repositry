@isTest
public class WCT_BatchToDToDoFailureHandlerS_Test
{
    @isTest public static void m1()
    {
        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con1=WCT_UtilTestDataCreation.createEmployee(rt.id);
        insert con1;

        Task t1 = new Task();
        t1.subject = 'aaaaaaa Success';
        t1.description = 'bbbbb';
        t1.whoId=con1.id;
        t1.ActivityDate=date.today().adddays(5);
        insert t1;

        Task t2 = new Task();
        t2.subject = 'aaaaaaa Error';
        t2.description = 'bbbbb';
        t2.whoId=con1.id;
        t2.WCT_Is_Visible_in_TOD__c=false;
        t2.ActivityDate=date.today().adddays(5);
        insert t2;

        WCT_ToD_Task_ToDo_Relation__c tasktod1=new WCT_ToD_Task_ToDo_Relation__c();
        tasktod1.SFDC_Task_Id__c=t1.id;
        tasktod1.Status__c = 'Create Failed';
        tasktod1.Status_Details__c = 'errorMessage';
        insert tasktod1;       
        
        WCT_ToD_Task_ToDo_Relation__c tasktod2=new WCT_ToD_Task_ToDo_Relation__c();
        tasktod2.SFDC_Task_Id__c=t2.id;
        tasktod2.TOD_TODO_Id__c='3357';
        tasktod2.Status__c = 'Delete Failed';
        tasktod2.Status_Details__c = 'errorMessage';
        insert tasktod2;               
        
        Test.setMock(HttpCalloutMock.class, new WCT_MockHttpResponseGenerator_Test());
                        
        Test.starttest();
        System.schedule('ScheduleApexClassTest',
                        '0 0 0 15 3 ? 2022', 
                        new WCT_BatchToDToDoFailureHandlerS());
        Test.stoptest();
    }
}