/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AttachmentUploadController_Test {
	
    public static Contact TestContact=new Contact();
   	public static Case TestCase=new Case();
   
   	 /** 
        Method Name  : createContacts
        Return Type  : Contact
        Type 		 : private
        Description  : Create temp records for data mapping         
    **/
    private static Contact createContact()
    {
    	Testcontact=WCT_UtilTestDataCreation.createContact();
    	insert TestContact;
    	return TestContact;
    }
    
     /** 
        Method Name  : Create Case
        Return Type  : Case
        Type 		 : private
        Description  : Create temp records for data mapping         
    **/
    private static case createCase()
    {
    	TestCase =WCT_UtilTestDataCreation.createCase(TestContact.id);
    	insert TestCase;
    	return TestCase;
    }
	
	
	  /** 
        Method Name  : myUnitTest
        Return Type  : Attachment
        Type 		 : private
        Description  : upload method if flag value is null.         
    **/

     static testMethod void myUnitTest() {
     Contact con= createContact();
     Case  testCase1=createCase();
     Test.setCurrentPage(Page.AddAttachment);
     system.currentPageReference().getParameters().put('id', testCase1.Id);
  	 AttachmentUploadController Acc= new AttachmentUploadController();
  	 Blob b = Blob.valueOf('Test Data');  
     Acc.attachment.Name='Test Attachment for Parent';  
     Acc.attachment.Body=b;
     Acc.parentId=testCase1.id;
     Acc.upload();    
     Acc.cancel();
    }

	  /** 
        Method Name  : myUnitTest2
        Return Type  : Attachment
        Type 		 : private
        Description  : upload method if flag value is yes.         
    **/
    static testMethod void myUnitTest2() {
     Contact con= createContact();
     Case  testCase1=createCase();
     Test.setCurrentPage(Page.AddAttachment);
     system.currentPageReference().getParameters().put('id', testCase1.Id);
  	 system.currentPageReference().getParameters().put('Flag', 'yes');
  	 AttachmentUploadController Acc= new AttachmentUploadController();
     Blob b = Blob.valueOf('Test Data');  
     Acc.attachment.Name='Test Attachment for Parent';  
     Acc.attachment.Body=b;
     Acc.parentId=testCase1.id;
     Acc.upload();    
     Acc.cancel();
    }
    
    	  /** 
        Method Name  : myUnitTest3
        Return Type  : Attachment
        Type 		 : private
        Description  : upload method with exception.         
    **/
    static testMethod void myUnitTest3() {
     Test.setCurrentPage(Page.AddAttachment);
     AttachmentUploadController Acc= new AttachmentUploadController();
     Acc.upload();    
    }
    
     static testMethod void myUnitTest4() {
     Contact con= createContact();
     WCT_List_Of_Names__c listnm= WCT_UtilTestDataCreation.createListOfNames('TestDebrief',system.label.LON_IEF_Record_type,'IEF');
     listnm.WCT_FSS__c='All';
     listnm.WCT_Job_Type__c='US-Experienced';
     listnm.WCT_User_Group__c='US';
     insert listnm;
     WCT_Interview__c  testCase1= WCT_UtilTestDataCreation.createInterview(listnm.id);
     testCase1.Debrief_Notes__c=false;
     insert testCase1;
     System.debug('!!!!'+testCase1);     
     Test.setCurrentPage(Page.AddAttachment);
     system.currentPageReference().getParameters().put('id', testCase1.Id);
     system.currentPageReference().getParameters().put('pg', 'debrief');
     system.currentPageReference().getParameters().put('dbchk', '0');        
  	 AttachmentUploadController Acc= new AttachmentUploadController();
  	 Blob b = Blob.valueOf('Test Data');  
     Acc.attachment.Name='Test Attachment for Parent';  
     Acc.attachment.Body=b;
     Acc.parentId=testCase1.id;
     Acc.upload();    
     Acc.cancel();
    }
}