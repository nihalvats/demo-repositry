@isTest
public class PVL_Client_Project_Test {
//extends SitesTodHeaderController 


    static testMethod void Test0()
    {
    
        
         Contact con1=WCT_UtilTestDataCreation.createContact();
             con1.email='test@deloitte.com';       
             insert con1;
        
        H1B_Client_Company__c objClntCmpny = new H1B_Client_Company__c(Company_Name__c='SYSCO CORPORATION');
        insert objClntCmpny; 
        
        Project_H1B__c pvl_clprj = new Project_H1B__c();
        pvl_clprj.Are_you_the_PLE_Partner__c = 'Yes';
        pvl_clprj.PVL_Module__c = 'module';
        pvl_clprj.Capability__c = 'sample';
        pvl_clprj.vendor__c = 'Vendor';
        pvl_clprj.H1B_Client_Company__c =  objClntCmpny.id;
        pvl_clprj.USI_Landed_Resources__c = 'yes';
        pvl_clprj.USI_Resources__c = 'yes';
        pvl_clprj.PVL_Primary_Visa_Contact__c = 'PrimaryContact1';
        pvl_clprj.PVL_Project_Start_Date__c = system.today();
        pvl_clprj.PVL_Project_End_Date__c  = system.today()+1;
        
       
        pvl_clprj.PVL_PL_Engagement_Partner__c = con1.Id;
        pvl_clprj.PVL_Project_Name__c = 'Test Project'; 
        pvl_clprj.Industry__c = 'Energy and Resources'; 
        pvl_clprj.PVL_Project_Service_Area__c = 'DC Technology'; 
        pvl_clprj.PVL_Project_Service_Line__c = 'Deloitte Digital';        
        
        pvl_clprj.USI_Landed_Resources__c = 'yes';
        pvl_clprj.USI_Resources__c = 'yes';
        
        insert pvl_clprj;
    
/*  
        PVL_Database__c pvldb = new PVL_Database__c(); 
        pvldb.PVL_Date_PVL_Received__c = system.today();
        pvldb.PVL_Expiration_Date__c = system.today()+1;
        pvldb.Active__c = true;
        insert pvldb;
        */
        
        
        

         Test.setCurrentPageReference(new PageReference('PVL_Client_Project')); 
         
         System.currentPageReference().getParameters().put('id', pvl_clprj.id);
         
         
        PVL_Client_Project form=new  PVL_Client_Project(null);
        
        form.ClientProject.Are_you_the_PLE_Partner__c ='No';
        form.hideSectionOnChange();
        form.ClientProject.Are_you_the_PLE_Partner__c ='Yes';
        form.hideSectionOnChange();
        form.updatePartnerDetails();
        form.file=Blob.valueOf('Text File');
        form.InputFileName='Test File Name';
        form.selectedClientId='New';
        form.newClientName='Test Company';
        form.SavePvl();
        form.newProject();
        form.pageErrorMessageAll=form.pageErrorMessageAll;
        form.locAddList=form.locAddList;
        form.locList=form.locList;
        form.LoggedInUser=form.LoggedInUser;
        
        form.BusinessVisa=form.BusinessVisa;
        form.BusinessVisaOptions=form.BusinessVisaOptions;
        form.PLEngagementPrtnr_Service_Line=form.PLEngagementPrtnr_Service_Line;
        form.PLEngagementPrtnr_Contact=form.PLEngagementPrtnr_Contact;
        
        form.selectedValues=form.selectedValues;
        form.searchemail=form.searchemail;
        form.RowIndex=form.RowIndex;
        form.recordType=form.recordType;



        form.pageError=form.pageError;
        form.pageErrorMessage=form.pageErrorMessage;
        
        form.getEmployeeDetails();
        form.DiscardSave1();

        
        PVL_Client_Project.getExistingCompanies('Test');
        PVL_Client_Project.getItemsList('Test');
        
        
        
    }


  
    
    
}