global class VTS_ProcessStaggingImigrationRecords 
{
    
    WebService static void processImmigrationRecords(String Status)
    {
        
        /*Building the query string to query the reasy to process */
        String query='Select ID';
        
        List<WCT_List_Of_Names__c> configs= [Select Id,H1B_Column_Name__c,H1B_Data_Type_of_the_Field__c, H1B_Stagging_API_Name__c,H1B_Immigration_API_Name__c, H1B_Is_Required__c,H1B_Static_Value__c,wct_type__c From WCT_List_Of_Names__c where RecordType.Name='H1B Immigration Upload Settings'];
        system.debug('field###'+configs.size());
        /*Using set to avoid the duplicate of the columns in query*/
        Set<String> columns= new Set<String>();
        
        Map<String, String> existingImmigrationMap= new Map<String, string>();
        Map<String, String> existingImmigrationMap_newId= new Map<String, string>();
        List<WCT_Immigration__c> immigrationToUpsert=new List<WCT_Immigration__c>();
        String immigrationH1RT=Schema.SObjectType.WCT_Immigration__c.getRecordTypeInfosByName().get(WCT_UtilConstants.H1_VISA_RT).getRecordTypeId();

        
        /*Building column set need to be quried  dynmically based on what configured in the Custom settings*/
        for(WCT_List_Of_Names__c config:configs)
        {
            columns.add(config.H1B_Stagging_API_Name__c);
        }
        columns.add('WCT_Identifier__c');
        columns.add('H1B_Identifier_new__c');
        columns.add('Contact__c');
        for(string column : columns)
        {
            query+=', '+column;
        }
        
        query+=' From WCT_Immigration_Stagging_Table__c where recordType.Name=\'VTS Upload Record\' and (Status__c=\''+Status+'\')';
        System.debug('### '+query);
        /*
         * Query the stagging records,
         * IF at least one record the,
         *  Query the Immigration with same (identifier Or identifier (new)).
         *  Put it into a Map<Identifier, ImmigrationRecord>
         *           Map<Identifier(new), ImmigrationRecord>
         *  Loopthough the stagging records
         *      If immigration record present in Map<Identifier, ImmigrationRecord> then,
         *          update the record with the new fields if not empty. 
         *          add to the list.
         *     Else If immigration record present in Map<Identifier(new), ImmigrationRecord> then,
         *      Else
         *          Create the new Immigration and update the fields if not empty.
         *          add to the list.
         */
        
        List<WCT_Immigration_Stagging_Table__c> stagingRecords=Database.query(query);
        Set<String> identifiers= new Set<String>();
        Set<String> identifiers_new= new Set<String>();
        for(WCT_Immigration_Stagging_Table__c staging:stagingRecords)
        {
       
            if(staging.get('WCT_Identifier__c')!=null)
            {
                if(String.valueOf(staging.get('WCT_Identifier__c'))!='')
                {
                    identifiers.add(String.valueOf(staging.get('WCT_Identifier__c')));
                }
            }
            if(staging.get('H1B_Identifier_new__c')!=null)
            {
                if(String.valueOf(staging.get('H1B_Identifier_new__c'))!='')
                {
                    identifiers_new.add(String.valueOf(staging.get('H1B_Identifier_new__c')));
                }
            }
        }
        
        System.debug('### identifiers '+identifiers);
        System.debug('### identifiers_new '+identifiers_new);
        
        /*
         * Put it into a Map<Identifier, ImmigrationRecord>
         */
        if(identifiers.size()>0)
        {
           List<WCT_Immigration__c>   existingImmigration=[Select Id,WCT_Identifier__c  from WCT_Immigration__c Where WCT_Identifier__c in :identifiers];
           for(WCT_Immigration__c tempImmigration : existingImmigration)
            {
                /*Adding Small Case Identifier to the Existing Immigration Map*/
                String smallCaseIdentifier=tempImmigration.WCT_Identifier__c!=null?tempImmigration.WCT_Identifier__c.toLowercase():tempImmigration.WCT_Identifier__c;
                existingImmigrationMap.put(smallCaseIdentifier, tempImmigration.Id);
            }
        }
        
        if(identifiers_new.size()>0)
        {
           List<WCT_Immigration__c>   existingImmigration=[Select Id,H1B_Identifier_new__c  from WCT_Immigration__c Where H1B_Identifier_new__c in :identifiers_new ];
           for(WCT_Immigration__c tempImmigration : existingImmigration)
            {
                /*Adding Small Case Identifier to the Existing Immigration Map*/
                 String smallCaseIdentifier_new=tempImmigration.H1B_Identifier_new__c!=null?tempImmigration.H1B_Identifier_new__c.toLowerCase():tempImmigration.H1B_Identifier_new__c;
                 existingImmigrationMap_newId.put(smallCaseIdentifier_new, tempImmigration.Id);
            }
        }
        
        
        System.debug('### existingImmigrationMap '+existingImmigrationMap);
        System.debug('### existingImmigrationMap '+existingImmigrationMap_newId);
        /*
         * Loopthough the stagging records
         */
        
        Set<String> alreadyAdded= new Set<String>();
        
        List<WCT_Immigration_Stagging_Table__c> immigratedRecords=new List<WCT_Immigration_Stagging_Table__c>();
        for(WCT_Immigration_Stagging_Table__c stagingRecord :stagingRecords)
        {
            
            /*
             *  Check for If Idetifier is not empty . 
             *  After GSS the Identifier new will be enpty.
            */
             
             if(stagingRecord.get('WCT_Identifier__c')!=null &&  !(alreadyAdded.contains(String.valueOf(stagingRecord .get('WCT_Identifier__c')))))
            {
                 stagingRecord.Status__c='Completed';
                immigratedRecords.add(stagingRecord);
               alreadyAdded.add(String.valueOf(stagingRecord .get('WCT_Identifier__c')));
                WCT_Immigration__c immigration = new WCT_Immigration__c();
                
                /*Converting the identifier to small case to avoid duplicate issue.*/
                String identifier_new_sc=String.valueOf(stagingRecord.get('H1B_Identifier_new__c'))!=null?String.valueOf(stagingRecord.get('H1B_Identifier_new__c')).toLowercase():String.valueOf(stagingRecord.get('H1B_Identifier_new__c'));
                String identifier_sc=String.valueOf(stagingRecord.get('WCT_Identifier__c'))!=null?String.valueOf(stagingRecord.get('WCT_Identifier__c')).toLowercase():String.valueOf(stagingRecord.get('WCT_Identifier__c'));
                
                /*Immigration present with same identifier(new) */
                if(existingImmigrationMap_newId.get(identifier_new_sc)!=null)
                {
                    immigration.Id=existingImmigrationMap_newId.get(identifier_new_sc);
                }
                else if(existingImmigrationMap.get(identifier_sc)!=null)
                {
                    /*Immigration present with same identifier*/
                    immigration.Id=existingImmigrationMap.get(identifier_sc);
                }
                else
                {
                    /*Set Record Type ID */
                    immigration.RecordTypeId=immigrationH1RT;
                }
                
                /*Set the field for VTS Upload : 
                 * Is_US_Upload__c
                 * H1B_Active_in_process_H1B__c  */
                immigration.H1B_IS_US_Upload__c=True;
                immigration.H1B_Active_in_process_H1B__c='Yes';
                
                /*Update Employee Details */
                   System.debug('### stagingRecord.Contact__c  ::  '+stagingRecord.Contact__c);
                if(stagingRecord.Contact__c!=null)
                {
                    immigration.WCT_Assignment_Owner__c=stagingRecord.Contact__c ;
                }
              System.debug('### immigration before '+immigration);
            
                for(WCT_List_Of_Names__c config:configs)
                {
                    System.debug('### immigration config '+config);
                    
                       /*
                        *   Check if the config is type of same as law firm in Stagging. 
                        */
                    if(config.WCT_Type__c==stagingRecord.get('H1B_Law_Firm_Name__c'))
                    {
                        /*
                         * Check if Stagging has this field.
                        */
                        if(stagingRecord.get(config.H1B_Stagging_API_Name__c)!=null)
                        {   
                         System.debug('### stagging value '+stagingRecord.get(config.H1B_Stagging_API_Name__c));
                            /*
                             * Check if data type is Date the, null check is enough and update the field.
                            */
                            if(config.H1B_Data_Type_of_the_Field__c=='Date')
                            {
                                System.debug('### stagging Date Value  '+stagingRecord.get(config.H1B_Stagging_API_Name__c));
                                immigration.put(config.H1B_Immigration_API_Name__c,stagingRecord.get(config.H1B_Stagging_API_Name__c));
                            }
                            else
                            {
                                System.debug('### stagging String Value  '+stagingRecord.get(config.H1B_Stagging_API_Name__c));
                                
                                /*
                                 * If Other than Date then check that the value is not empty.
                                */
                                
                                if(String.valueOf(stagingRecord.get(config.H1B_Stagging_API_Name__c))!='')
                                {
                                    System.debug('### stagging String Value Not empty  '+stagingRecord.get(config.H1B_Stagging_API_Name__c)+' sample'+config.H1B_Immigration_API_Name__c);
                                  immigration.put(config.H1B_Immigration_API_Name__c,stagingRecord.get(config.H1B_Stagging_API_Name__c));
                                }
                            }
                        }
                    }
                    
                }
                System.debug('### immigration after '+immigration);
                immigrationToUpsert.add(immigration);
            }
            else
            {
                stagingRecord.Status__c='Failed';
                stagingRecord.WCT_Error_Message__c='No Indentifier Or Duplicate Identifier.';
                immigratedRecords.add(stagingRecord);
            }
        }
        
        
        System.debug('### immigrationToUpsert '+immigrationToUpsert);
        /*
         * Upsert the immigration . 
         * 
        */
        if(immigrationToUpsert.size()>0)
        {
            Database.UpsertResult[] srList = Database.upsert(immigrationToUpsert);
            
            Database.Update(immigratedRecords);
            
        }
    }
    
   
    
}