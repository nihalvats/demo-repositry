/********************************************************************************************************************************
Apex class         : trtproj_ctrl
Description        : Controller which handles to createe a project in project object
Type               : Controller
Test Class         : test_trtproj_ctrl

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 02                 Deloitte                   27/02/2015          83%                         <Req / Case #>                  Original Version
************************************************************************************************************************************/
public with sharing class trtproj_ctrl extends SitesTodHeaderController {
    public project__c prj {get;set;}
    public boolean fieldRender {get;set;}
    public boolean OTRender {get;set;}
    public boolean PARender {get;set;}
    public boolean IntangRender{get;set;}
    public String requesttype {get;set;}
    public String inc {get;set;}
    public string ProcessArea {get;set;}
    Map < string, string > mapFS = new Map < string, string > ();
    public boolean IsCreated {get;set;}
    public List < project__c > prjLst {get;set;}  
    public string SrvldOrAnltc{get;set;}
    public boolean IsSrvcarealed {get;set;}
    public boolean IsAnalyticsprj {get;set;}
   // public string EfficiencyType{get;set;}
    public string EfficiencyCategory{get;set;}
    public string Anayticsfield{get;set;}
    public string useremail{get;set;}
    public trtproj_ctrl() {
        fieldRender = false;
        Preinit();
    }
/********************************************************************************************
*Method Name             : PreInit
*Return Type             : None
*Param’s                 : None
*Description             : method that loads the initial values. 
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 02                 Original Version
*********************************************************************************************/
    public Void Preinit() {
        IsCreated = false;
        IsSrvcarealed=false;
        IsAnalyticsprj=false;
        //Create New Record in project Obj
        //Updated code in hotfix
        // emValue = ApexPages.currentPage().getParameters().get('em');
        SrvldOrAnltc=ApexPages.currentPage().getParameters().get('Param1');
        if (SrvldOrAnltc=='Srvcarealed') {
            IsSrvcarealed = true;
        }
        else if(SrvldOrAnltc=='Analytics'){
            IsAnalyticsprj=true;
        }
        if (string.IsNotEmpty(ApexPages.currentPage().getParameters().get('IsCreated'))) {
            IsCreated = true;
        }
        userEmail = loggedInContact.Email;
        try {
            contact conObj = [select id, email from contact where email = : userEmail limit 1];
            prj = new project__c();
            if (conObj.id != null) prj.Requestor__c = conObj.id;
            ProcessArea = '';
            Map < String, Schema.fieldset > fieldSetMap = Schema.SObjectType.Project__c.fieldSets.getMap();
            for (string strFS: fieldSetMap.keyset()) {
                mapFS.put(fieldSetMap.get(strFS).getLabel(), strFS);
            }
            prjLst = new List < project__c > ();
            prjLst = [select id, Name, Request_Type__c, Project_Type__c, Project_Status__c, Overall_Project_Status__c, Status__c, Project_Number__c, Description__c, Project_Lead_USI__c from project__c where Requestor__r.email = : userEmail];
           system.debug('satus is'+prjLst);
            Anayticsfield=mapFS.get('Analytics Pipeline');
          
        } catch (Exception e) {
             Exception_Log__c log = WCT_ExceptionUtility.logException('TRT Project controller', 'TRT project', e.getMessage());
               
        }
        
    }
    
    
    public Attachment attachment {
        get {
            if (attachment == null) attachment = new Attachment();
            return attachment;
        }
        set;
    }
    
    public void getProcessArea() {
        if (string.IsNotEmpty(ApexPages.currentPage().getParameters().get('ProcessArea'))) {
            ProcessArea = ApexPages.currentPage().getParameters().get('ProcessArea');
            ProcessArea = mapFS.get(ProcessArea);
            fieldRender = true;
        }
    }
    
    
    public void getEfficiencyCategory() {
        
        EfficiencyCategory = ApexPages.currentPage().getParameters().get('EfficiencyCategory');
        if(EfficiencyCategory=='Direct FTE Reduction' || EfficiencyCategory=='Work Movement/Scope of Service Enhancement' || EfficiencyCategory=='Contractor Fee' || EfficiencyCategory=='Volume Intake')
        {
            IntangRender=false;
            PARender=false;
            OTRender=false;
            fieldRender = true;
        }
        else if(EfficiencyCategory=='Overtime Reduction')
        {
            IntangRender=false;
            PARender=false;
            OTRender=true;
            fieldRender = false;
        }
        else if(EfficiencyCategory=='P&A')
        {
            IntangRender=false;
            PARender=true;
            OTRender=false;
            fieldRender = false;
        }
        else if(EfficiencyCategory=='Quality Improvement' || EfficiencyCategory=='CSAT Improvement' || EfficiencyCategory=='Cost Prevention')
        {
            IntangRender=true;
            PARender=false;
            OTRender=false;
            fieldRender = false;
        }
        else
        {
            IntangRender=false;
            PARender=false;
            OTRender=false;
            fieldRender = false;   
        }
        
        prj.Primary_Category__c='Service Management';
        prj.Project_Category__c='SERVICE AREA';
        prj.Project_Sub_Category__c='Service Management';
    }
    /********************************************************************************************
*Method Name             : Save
*Return Type             : Page Reference
*Param’s                 : None
*Description             : method that saves the record in the project object
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 02                 Original Version
*********************************************************************************************/
    
    public pagereference Save() {
        try {
            
            system.debug('----attachment-----' + attachment + ' ---- prj ---' + prj);
            if(IsAnalyticsprj==true)
            {
                prj.recordtypeid=system.label.BME_Amalytics_Pipeline;
                //system.debug('###'+prj);
              
            }
             insert prj; 
            system.debug('----prj-----' + prj);
            if (attachment != null && attachment.Name != null) {
                attachment.OwnerId = UserInfo.getUserId();
                attachment.ParentId = prj.Id; // the record the file is attached to
                attachment.IsPrivate = true;
                
                try {
                    insert attachment;
                    
                } catch (DMLException e) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error uploading attachment'));
                    
                } finally {
                    attachment = new Attachment();
                }
            }
            // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,'Project is Created'));
            PageReference objPageRef = new PageReference('/apex/trtproject');
            if (prj.Id != null) {
                
                objPageRef = new PageReference('/apex/GBL_Page_Notification?key=TRTPRJ_ThankYou');
                objPageRef.setRedirect(true);
            }
            return objPageRef;
        } catch (exception ex) {
            string tempmsg=ex.getMessage();
            ApexPages.Message myMsg;
            if(tempmsg.contains('Project dates cannot be less than today') && !tempmsg.contains('Planned End Date cannot be a date less that start date'))
            {
                myMsg=new ApexPages.Message(ApexPages.Severity.ERROR,'Please provide a date greater than or equal to today');
            }
            else if(tempmsg.contains('Planned End Date cannot be a date less that start date') && !tempmsg.contains('End date cannot be less than today'))
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,'Planned End Date cannot be a date less that start date'));
            // myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Planned End Date cannot be a date less that start date');
            else if(tempmsg.contains('Planned End Date cannot be a date less that start date') && tempmsg.contains('End date cannot be less than today'))
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,'Something went wrong . Please reload the page and retry'));
            //  myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Something went wrong . Please reload the page and retry');
            // ApexPages.addMessage(myMsg);
            
            system.debug('----Exception-----' + ex.getMessage());
            return null;
        }
    }
    
    public void Clear() {
        Preinit();
    }
}