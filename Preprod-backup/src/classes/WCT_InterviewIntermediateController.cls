public with sharing class WCT_InterviewIntermediateController {
String InterviewId,JunctionId;
    public WCT_InterviewIntermediateController() {
    InterviewId = System.currentPagereference().getParameters().get('id');
    JunctionId= System.currentPagereference().getParameters().get('juncId');
    }
    public PageReference RedirectToInterview(){
	    if(InterviewId!='' && InterviewId != null){    
	    	list<WCT_Interview_Junction__c> IntvJuncRec = [select id from WCT_Interview_Junction__c 
	                                                        where WCT_Interview__c=:InterviewId
	                                                        and WCT_Interviewer__c=:UserInfo.getUserId()];
	        if(JunctionId==null && JunctionId==''){
	            JunctionId = IntvJuncRec[0].Id;
	        }
	        if(!IntvJuncRec.IsEmpty()){
	            if(IntvJuncRec.size()>1){
	                return new PageReference('/apex/WCT_InterviewGroupDetail?id='+JunctionId);           
	            }
	            else{
	                return new PageReference('/apex/WCT_InterviewDetail?id='+JunctionId);
	            }
	        }
	        else{
	        return null;
	        }
	    }
	    else{
	        return null;
		}
    }
}