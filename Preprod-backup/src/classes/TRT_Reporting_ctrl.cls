/********************************************************************************************************************************
Apex class         : TRT_Reporting_ctrl
Description        : Controller which handles the operation for creating a request on Case_form_Extn__c object for Reporting team.
Type               : Controller
Test Class         : Test_TRT_Reporting_Ctrl

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 02                 Deloitte                   27/05/2016          86%                         <Req / Case #>                  Original Version
************************************************************************************************************************************/

public class TRT_Reporting_ctrl extends SitesTodHeaderController{
    
    public Case_form_Extn__c trtReport{get;set;} //  maintain the state of the wizard and the user input is stored 
    public string fileName {get;set;}// holds the attachment name
    public string efileName {get;set;}// holds the email attachment name
    public transient Blob fileBody {get;set;}// holds the attachment body
    public list<contact> contact= new list<contact>();// holds the contact records
    public transient Blob efileBody {get;set;}// holds the email attachment body
    public boolean pageMessage{get;set;}//Variable which handles the page Messages on the page
    public map<string,string> mapQueueNames;
    public list<string> checkboxSelections {get;set;}
    public list<string> leaCheckboxSelections {get;set;}
    public list<string> evaCheckboxSelections {get;set;}
    public GBL_Attachments attachmentHelper{get; set;}
    public boolean Enrollments{get;set;}
    public boolean Completions{get;set;}
    public boolean Compliance{get;set;}
    public boolean CancellationsandNoshow{get;set;}
    public boolean Instructor{get;set;}
    public list<case> caseQuestionsList{get;set;}// holds the case question/suggestions list
    public list<string> leacompCheckboxSelections{get; set;}
    public list<string>  surveyListCheckBox{get;set;}// holds the mutlicheck box list 
   // public boolean addAttachment{get;set;}
    public static datetime datedelivery {get;set;}
    public list<string> surveyReportType {get;set;}
    public string rtype;
    public ID recid;
    public  boolean checkingerror;
    public string mainvaluereporting {get;set;}
    public string subvaluereporting {get;set;}
    // constructor
    public TRT_Reporting_ctrl()
    {
        init();
    }
    /********************************************************************************************
*Method Name             : Init
*Return Type             : None
*Param’s                 : None
*Description             : method that loads the initial values of the report for the requester
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 02                 Original Version
*********************************************************************************************/
    
    Public Void Init()
    {
        System.debug('Init'+mainvaluereporting+' subvaluereporting '+subvaluereporting);
        // subvaluereporting='';
        datedelivery = BusinessHours.add(system.label.TRT_BussinessHours, Datetime.now()  , 172800000);
        attachmentHelper= new GBL_Attachments();
        checkboxSelections = new list<string>();
        leaCheckboxSelections = new list<string>();
        evaCheckboxSelections = new list<string>();
        surveyListCheckBox = new list<string>();
        surveyReportType =  new list<string>();
        leacompCheckboxSelections = new list<string>();//for learning competancy multiselect***added by nihal***
        caseQuestionsList = new list<case>();
        pageMessage=false;
        rtype=apexPages.currentPage().getParameters().get('Reqn');// fecthing value from parameter passed for cloning
        recid =apexPages.currentPage().getParameters().get('cse');
        if( recid !=null )
        {
            //id for clone fetching from paramenter
            DescribeSObjectResult describeResult = recid.getSObjectType().getDescribe();
            List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );
            String query = ' SELECT ' +   String.join( fieldNames, ',' ) + ' FROM ' +  describeResult.getName() + ' WHERE ' + ' id = :recid ' + ' LIMIT 1 ';
            List<Case_form_Extn__c> records = Database.query( query );
            
            trtreport=records[0].clone();
            
            /*Added code handle the issue of CFE not getting created. */
           
            /*Added code handle the issue of CFE not getting created. */
            
            trtreport.TRT_Delivery_Date__c =null;
            trtreport.TRT_Delivery_Time__c=null;
            trtreport.TRT_ReOpen_Comment__c=null;
            trtreport.TRT_Reopen_Time_Taken__c=null;
            trtreport.TRT_Reopen_UT_Updated__c=false;
            trtreport.TRT_Renegotiate_Delivery_Date__c=null;
            trtreport.TRT_Renegotiate_Delivery_Time__c=null;
            trtreport.TRT_Renegotiate_Comments__c=null;
            trtreport.TRT_Feedback_Comments__c=null;
            trtreport.TRT_Feedback__c=null;
            trtreport.TRT_Helpfulness__c=null;
            trtreport.TRT_Accuracy__c=null;
            trtreport.TRT_Promptness__c         =null;
            trtreport.TRT_Report_Cateogary__c=null;
            trtreport.TRT_TalentReporting_Activity_List__c=null;
            trtreport.TRT_SubProcess_List__c=null;
            trtreport.TRT_Time_Taken__c=null;
            trtreport.TRT_Reopen_Time_Taken__c=null;
            trtreport.TRT_Status_Checkbox__c=false;
            trtreport.TRT_Report_closed_date_time__c=null;
            trtreport.TRT_Assigned_time_BH__c=null;
            trtreport.TRT_Closed_Email_Comments__c=null;
            
        }
        list<TRT_Record_Type_Details__c> trtd = TRT_Record_Type_Details__c.getall().values();//custom setting all values
        //Added for clone functionality by nihal
        if(string.isNotBlank(rtype) && string.isNotEmpty(rtype) )
        {
            for(TRT_Record_Type_Details__c trloop:trtd){
                if(rtype == trloop.TRT_Parameter_Value__c)
                {
                   mainvaluereporting=trloop.TRT_Main_Value__c;
                   subvaluereporting=trloop.TRT_SubCategory__c;
                }
                
            } 
        }
        
        if(trtReport == null)
        {
            trtReport = new Case_form_Extn__c();
        }    
        system.debug(loggedInContact);
        if(loggedInContact != null){
            contact = [SELECT Name,email,WCT_Function__c,WCT_Job_Level_Text__c,id,
                       Account.Name FROM contact WHERE id=:loggedInContact.id limit 1];// querying the conatct details on user entry
        }
        system.debug(contact);
        if(!contact.isEmpty() && contact[0] != null )
        {
            trtReport.TRT_Requestor_Name__c = contact[0].id;// auto populating the contact name
            trtReport.TRT_Requestor_Function__c=contact[0].WCT_Function__c;
            trtReport.TRT_Requestor_Level__c=contact[0].WCT_Job_Level_Text__c;
            trtReport.TRT_Requester_Group__c=contact[0].Account.Name;
            
        }
        mapQueueNames =new map<string,string>();
        for(group g:[Select g.Id, g.Name, g.Email,g.Type from Group g where g.Type = 'Queue' and g.name like '%TRT%'])
        {
            mapQueueNames.put(g.Name,g.Id);
        }
        if(datedelivery != null)
        {
            //string sFormattedDate =datedelivery.format('MM/dd/yyyy');
            trtReport.TRT_Delivery_Date_lrng__c=date.valueOf(datedelivery);
        }
    }
    /********************************************************************************************
*Method Name            : Enddatecheck
*Return Type            : Page reference
*Param’s                : None
*Description            : method to validate the end date should be less than today **Added by nihal**
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01                 Original Version
*********************************************************************************************/
    
    public PageReference Enddatecheck(){
        
        if(trtReport.TRT_End_Date_RM__c<trtReport.TRT_Start_Date_RM__c )
        {
            trtReport.TRT_End_Date_RM__c.addError('End date cannot be earlier than start date');
            checkingerror=true;
        }
        
        if(trtReport.TRT_Delivery_Date_lrng__c  <= date.today() )
        {
            
            trtReport.TRT_Delivery_Date_lrng__c.addError('Delivery date should be later than today.' );
            checkingerror=true;
        }
        return null;
        
    }
    /********************************************************************************************
*Method Name            : compPannel
*Return Type            : Page reference
*Param’s                : None
*Description            : method for learning competancy multiselect***added by nihal***
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01                 Original Version
*********************************************************************************************/
    public PageReference compPannel(){
        
        String[] values = leacompCheckboxSelections;
        system.debug(leacompCheckboxSelections);
        String s1 = '';
        string s2 = '';
        string s3 = '';
        string s4 = '';
        string s5 = '';
        for(String s : values)
        {
            system.debug(s);
            if(s=='Consulting Specific'){
                s1='Consulting Specific';
            }else if(s=='FAS Specific'){
                s2='FAS Specific';
            }else if(s=='TAX Specific'){
                s3='TAX Specific';
            }else if(s=='AERS Specific'){
                s4='AERS Specific';
            }else if(s=='Enabling Area Specific'){
                s5='Enabling Area Specific';
            }
        }
        if(s1=='Consulting Specific'){
            Enrollments=true;
        }else{
            Enrollments=false;
        }
        if(s2=='FAS Specific'){
            Completions=true;
        }else{
            Completions=false;
        }if(s3=='TAX Specific'){
            Compliance=true;
        }else{
            Compliance=false;
        }if(s4=='AERS Specific'){
            CancellationsandNoshow=true;
        }else{
            CancellationsandNoshow=false;
        }if(s5=='Enabling Area Specific'){
            Instructor=true;
        }else {
            Instructor=false;
        }
        return null;
    }
    /********************************************************************************************
*Method Name            : getLearning
*Return Type            : List of select options
*Param’s                : None
*Description            : select radio values for Learning
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01                 Original Version
*********************************************************************************************/
    
    public List<SelectOption> getLearning() 
    {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Catalog','Catalog')); 
        options.add(new SelectOption('Certification','Certification'));
        options.add(new SelectOption('Offering','Offering'));
        options.add(new SelectOption('Chargeback','Chargeback')); 
        options.add(new SelectOption('Common','Enrollments/Completions/Compliance/Cancellations/Learning Hours and No show/Instructor')); 
        options.add(new SelectOption('Competency','Competency')); 
        options.add(new SelectOption('Learning Evaluation','Learning Evaluation'));
        return options; 
    }
    // Select radio values for Resource Management
    public List<SelectOption> getRManagement() 
    {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('GSS/StaffTrak','GSS/StaffTrak/STAFFIT')); 
        return options; 
    }
    // Select radion values for Project Management
    public List<SelectOption> getPManagement() 
    {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Reinvention','Reinvention'));
        options.add(new SelectOption('DPME (Old)','DPME')); 
        return options; 
    }
    public list<selectOption> getMyCheckboxes()
    {
        list<selectOption> Options = new list<selectOption>();
        options.add(new SelectOption('Reinvention','Reinvention'));
        options.add(new SelectOption('DPME (Old)','DPME (Old)')); 
        return Options;
    }
    
    // method that hold trtReport record
    public Case_form_Extn__c getTrtReport() 
    {
        if(trtReport == null)
        {
            trtReport = new Case_form_Extn__c();
        }
        return trtReport;
    }
    /********************************************************************************************
*Method Name         : sapReport
*Return Type         : Page refernce
*Param’s             : None
*Description         : method to move to next page from the from landing page
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01                 Original Version
*********************************************************************************************/
    
    public PageReference sapReport() 
    {
        list<TRT_Record_Type_Details__c> trtd = TRT_Record_Type_Details__c.getall().values();//custom setting all values
        String tempSubCat=subvaluereporting==''?null:subvaluereporting;
        try{
        for(TRT_Record_Type_Details__c  trvalue:trtd){
            if(mainvaluereporting == trvalue.TRT_Main_Value__c && tempSubCat == trvalue.TRT_SubCategory__c)
            {
                PageReference pr = new PageReference('/apex/'+trvalue.TRT_Page_Name__c);
                //pr.setRedirect(true);  
                return pr;  
                
            }   
        }
        
        return null; 
        }
        catch(Exception e)
        {
           Exception_Log__c log = WCT_ExceptionUtility.logException('TRT_Reporting_ctrl', 'TRT_ReportRequestForm', e.getMessage());
                    
                    pagereference pg = new pagereference('/apex/GBL_Page_Notification?key=TRT_Error&expCode='+log.name);
                    pg.setRedirect(true);
                    return pg;  
           
        }
    }
    
    
    
    public PageReference questions()
    {
        return Page.TRT_Suggestion;
    }
    /********************************************************************************************
*Method Name         : submit
*Return Type         : Page reference
*Param’s             : None
*Description         : Method the saves the values in salesforce for Case_form_Extn__c for SAP reporting
*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01                 Original Version
*********************************************************************************************/
    
    public PageReference submit()  
    {
        checkingerror=false;
        Enddatecheck();
        if(checkingerror ==true){
            
            return null;
                }
        system.debug('mainvaluereporting'+mainvaluereporting);
        system.debug('subvaluereporting'+subvaluereporting);
        contact con;
        boolean questionsCase=false;
        string questionRecordType='';
        list<case> listCase = new list<case>();
        Case objCase= new Case();
        
        ID rtId;
        String tempSubCat=subvaluereporting==''?null:subvaluereporting;
        list<TRT_Record_Type_Details__c> trtd = TRT_Record_Type_Details__c.getall().values();//custom setting all values
        for(TRT_Record_Type_Details__c  trvalue:trtd){
            if(mainvaluereporting == trvalue.TRT_Main_Value__c && tempSubCat == trvalue.TRT_SubCategory__c)
            { 
                system.debug('start the value '+rtId);
                Schema.DescribeSObjectResult Rep = Case_form_Extn__c.SObjectType.getDescribe();// getting Sobject Type
                Map<String,Schema.RecordTypeInfo> rtMapByName = Rep.getRecordTypeInfosByName();// getting the record Type Info
                rtId =rtMapByName.get(trvalue.Record_Type__c).getRecordTypeId();//particular RecordId by  Name
                system.debug('before the value '+rtid);
            }
        }
        system.debug('After the value '+rtId);
        
        
        Schema.DescribeSObjectResult Cas = Case.SObjectType.getDescribe();// getting Sobject Type
        Map<String,Schema.RecordTypeInfo> rtMapByNames = Cas.getRecordTypeInfosByName();// getting the record Type Info
        Id caseRtId =rtMapByNames.get('Case Mail Consolidation').getRecordTypeId();//particular RecordId by  Name
        
        for(TRT_Record_Type_Details__c  trvalue:trtd){
            system.debug('trtReport.TRT_Group_sugg__c'+trtReport.TRT_Group_sugg__c+'trvalue.TRT_Main_Value__c'+trvalue.TRT_Main_Value__c+'tempSubCat'+tempSubCat+'trvalue.TRT_SubCategory__c'+trvalue.TRT_SubCategory__c);
            if(trtReport.TRT_Group_sugg__c==trvalue.Questions_Value__c && trtReport.TRT_Group_sugg__c!= null ){
                mainvaluereporting =trvalue.TRT_Main_Value__c;
                subvaluereporting=trvalue.TRT_SubCategory__c;
                questionsCase=true;
                questionRecordType=trvalue.Case_Record_Type__c;
                system.debug('record tyep is'+questionRecordType);
                system.debug('mainvaluereporting'+mainvaluereporting+'subvaluereporting'+subvaluereporting);
            }
        }
        boolean learningPII=true;
        if(mainvaluereporting == 'Learning Reporting' && subvaluereporting =='Learning Evaluation')
        {
            learningPII=false;
        }
        else
        {
            learningPII=true;
        }
        
        
        
        if(contact != null && !contact.isEmpty())
        {
            if(contact[0]!=null && contact[0].Email != null)
            {
                objCase.ContactId=contact[0].Id;
            }
        }
        
        // initializing case feilds
        objCase.Status='New';
        objCase.RecordTypeId=caseRtId;
        objCase.WCT_Category__c='TRT Reporting';  
        objCase.Origin = 'Web';
        objCase.Gen_Request_Type__c ='';
        objCase.Priority='3 - Medium';
        if(questionsCase == true && questionRecordType!='')
        {
            objCase.Description ='TRT question and suggestions request';  
            objCase.Gen_RecordType__c =questionRecordType; 
            objCase.TRT_Questions__c=true;
            objCase.Subject='A new TRT-Reporting request';
            objCase.TRT_Requestor_Suggestions__c=trtReport.TRT_ques_sugg__c;
        }
        for(TRT_Record_Type_Details__c  trvalue:trtd){
            if(mainvaluereporting == trvalue.TRT_Main_Value__c && tempSubCat == trvalue.TRT_SubCategory__c &&  questionsCase==false)
            { 
                objCase.Description = trtReport.TRT_Business_need_for_the_data_request__c;  
                objCase.Gen_RecordType__c = trvalue.Case_Record_Type__c; 
                objCase.Subject='A new TRT-Reporting request';
            }
        }
        
        //assigning the record to assignment rule and inserting case
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= Label.Case_Assignment_Rule_Id ;
        dmlOpts.EmailHeader.TriggerUserEmail = true;
        objCase.setOptions(dmlOpts); 
        listCase.add(objCase);
        Database.SaveResult[] csList = Database.insert(listCase, dmlOpts);
        system.debug(csList+'************');
        // Added by deepthi for adding attachment in question and sugestion page start
        if(csList[0].isSuccess())
        {
            if(questionsCase==true)
            {
                //inserting attachment upon successfull insertion of case
                if(fileBody != null && fileName != null)  
                {  
                    Attachment myAttachment  = new Attachment();  
                    myAttachment.Body = fileBody;  
                    myAttachment.Name = fileName;  
                    myAttachment.ParentId = csList[0].id;
                    insert myAttachment;  
                }
            }
        }
        // Added by deepthi for adding attachment in question and sugestion page end
        // creating case form extension record
        
        if(csList[0].isSuccess())
        {
            trtReport.GEN_Case__c   =csList[0].id;
            system.debug('case id is'+csList[0].id);
            
        }
        trtReport.RecordTypeId=rtId;
        system.debug('case form record type is'+rtId);
        trtReport.Case_Form_App__c ='TRT Reporting';
        trtReport.TRT_Request_Status__c='New';
        
        if(!checkboxSelections.isEmpty() && checkboxSelections != null)
        {
            trtReport.TRT_Reporting_Request_Forms_othr__c=getMultiSelect(checkboxSelections);
        }
        if(!leaCheckboxSelections.isEmpty() && leaCheckboxSelections != null)
        {
            trtReport.TRT_Common_Rtype_Lrng__c=getMultiSelect(leaCheckboxSelections );
        }
        //for learning competancy multiselect***added by nihal***
        if(!leacompCheckboxSelections.isEmpty() && leacompCheckboxSelections != null)
        {
            trtReport.TRT_Job_Role_Lrn_Comp__c=getMultiSelect(leacompCheckboxSelections);
        }
        
        if(!evaCheckboxSelections.isEmpty() && evaCheckboxSelections != null)
        {
            trtReport.TRT_survey_type_LE__c=getMultiSelect(evaCheckboxSelections);
        }
        if(!surveyListCheckBox.isEmpty() && surveyListCheckBox != null){
            trtReport.TRT_Level_of_Analysis_srvy__c=getMultiSelect(surveyListCheckBox);
        }
        if(!surveyReportType.isEmpty() && surveyReportType != null)
        {
            system.debug('values'+surveyReportType);
            trtReport.TRT_Report_Type_srvy__c=getMultiSelect(surveyReportType);
        }
        
        try
        {
            //inserting CFE
            if(csList[0].isSuccess())
            {
                if(questionsCase==false)
                {
                   
                    insert trtReport;
                }
            }
        }catch(Exception e)
        {
            Exception_Log__c log = WCT_ExceptionUtility.logException('TRT_Reporting_ctrl', 'TRT_ReportRequestForm', e.getMessage());
                 
            system.debug('DML Exception '+e);
             pagereference pg = new pagereference('/apex/GBL_Page_Notification?key=TRT_Error&expCode='+log.name);
                    pg.setRedirect(true);
                    return pg;  
        }
        if(trtReport.id != null)
        {
            //inserting attachments
            if(fileBody != null && fileName != null)  
            {  
                Attachment myAttachment  = new Attachment();  
                myAttachment.Body = fileBody;  
                myAttachment.Name = fileName;  
                myAttachment.ParentId = trtReport.id;  
                insert myAttachment;  
            } 
            if(eFileBody != null && eFileName != null)  
            {  
                Attachment myAttachment  = new Attachment();  
                myAttachment.Body = efileBody;  
                myAttachment.Name = efileName;  
                myAttachment.ParentId = trtReport.id;  
                insert myAttachment;  
            } 
            if(mainvaluereporting == 'Survey')
            { 
                
                //attachment component method for uploading multiple attachments at a time.
                attachmentHelper.uploadRelatedAttachment(trtReport.id);
            }
        }
        PageReference pr = new PageReference('/apex/GBL_Page_Notification?key=TRT_ThankYou&label=HOME&url=/apex/TRT_ReportRequestForm');  // redirects to the thank you page
        pr.setRedirect(true);  
        return pr; 
        
    }
    
   
    // Method with returns list of case who raised suggestrion on TRT reporting tool
    public list<Case> getQuestionsInfo()
    {  
        // List the pass the values in Apex Page 
        caseQuestionsList=[Select id,TRT_Requestor_Suggestions__c,status,WCT_ResolutionNotes__c,
                           CaseNumber  from Case where  TRT_Questions__c=true and Contact.id=:contact[0].Id];
        return caseQuestionsList;
        
    }
    // Method which to handle mutliselect values
    public string getMultiSelect(list<string> leaCheckboxSelections)
    {
        String  multiString = '';
        if(!leaCheckboxSelections.isEmpty() && leaCheckboxSelections != null)
        {
            system.debug(leacheckboxSelections);
            Boolean Start = true;
            for(string c:leacheckboxSelections)
            {
                if(Start) 
                {
                    multiString = c;
                    Start = false;
                }else 
                {               
                    multiString = multiString + ';' + c;
                }
            }
        }
        return multiString;
    }
    
    
}