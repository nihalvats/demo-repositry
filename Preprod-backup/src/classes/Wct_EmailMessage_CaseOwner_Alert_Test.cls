@isTest
private class Wct_EmailMessage_CaseOwner_Alert_Test {

    static testmethod void test_trigger(){
    recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
    Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
      con.Email='testoast@deloitte.com';
      con.WCT_Person_Id__c='104032';
      con.WCT_Cost_Center__c='42324';
      insert con;
     Case c = new  Case(); 
     c.ContactId =  con.id ;
     c.WCT_Category__c = 'Benefits'; 
     c.subject = 'Test Case';
     c.description= 'Test Descriptiopn'; 
     c.Origin='Chat';
     c.wct_category__c='Technology Issue - Talent';
     c.WCT_Requisition_FSS__c ='test';
     c.Priority='1-High';
     c.WCT_Requisition_Service_Area__c='testdata';
     insert c;
 
    Profile p = [SELECT Id FROM Profile WHERE name ='system administrator']; 
    User u2 = new User(Alias = 'newpega', Email='newpega@testorg.com', 
              EmailEncodingKey='UTF-8', LastName='pegaTesting', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName='newuserpega@testorg.com');
    System.runAs(u2) {
      emailmessage em= new emailmessage();
      em.parentid=c.id;
      em.status='0';
      em.subject='re:test';
        em.FromAddress='nvats@deloitte.com';
        em.ToAddress=null;
        //em.Subject='FtpImport Success';
      insert em;
        WCT_EmailMessageTriggerHandler mesemail = new WCT_EmailMessageTriggerHandler();
        mesemail.checkEmailWithRole(em);
      //  em.parentid=c.id;
      //em.status='0';
     // em.subject='re:test';
     Email_Handler_Drop__c eh = new Email_Handler_Drop__c();
        eh.Name='rule1';
        eh.FromAdrress__c='nvats@deloitte.com';
        eh.TOAdrress__c=null;
        eh.Keyword__c='FtpImport Success';
        insert eh;
        em.FromAddress='nvats@deloitte.com';
        em.ToAddress=null;
        em.Subject='FtpImport Success';
        mesemail.CheckExistingCase(em);
        
         
      }
     }
    
      static testmethod void test_trigger1(){
    recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
    Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
      con.Email='testoast@deloitte.com';
      con.WCT_Person_Id__c='104032';
      con.WCT_Cost_Center__c='42324';
      insert con;
     Case c = new  Case(); 
     c.ContactId =  con.id ;
     c.WCT_Category__c = 'Benefits'; 
     c.subject = 'Test Case';
     c.description= 'Test Descriptiopn'; 
     c.Origin='Chat';
     c.wct_category__c='Technology Issue - Talent';
     c.WCT_Requisition_FSS__c ='test';
     c.Priority='1-High';
     c.WCT_Requisition_Service_Area__c='testdata';
     insert c;
 
    Profile p = [SELECT Id FROM Profile WHERE name ='system administrator']; 
    User u2 = new User(Alias = 'newpega', Email='newpega@testorg.com', 
              EmailEncodingKey='UTF-8', LastName='pegaTesting', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName='newuserpega@testorg.com');
    System.runAs(u2) {
      emailmessage em= new emailmessage();
      em.parentid=c.id;
      em.status='0';
      em.subject='re:test';
        em.FromAddress='nvats@deloitte.com';
        em.ToAddress=null;
        //em.Subject='FtpImport Success';
      insert em;
        WCT_EmailMessageTriggerHandler mesemail = new WCT_EmailMessageTriggerHandler();
        mesemail.checkEmailWithRole(em);
      //  em.parentid=c.id;
      //em.status='0';
     // em.subject='re:test';
     Email_Handler_Drop__c eh = new Email_Handler_Drop__c();
        eh.Name='rule1';
        eh.FromAdrress__c='nvats@deloitte.com';
        eh.TOAdrress__c='nvats@deloitte.com';
        eh.Keyword__c='FtpImport Success';
        insert eh;
        em.FromAddress='nvats@deloitte.com';
        em.ToAddress='nvats@deloitte.com';
        em.Subject='FtpImport Success';
        mesemail.CheckExistingCase(em);
        
         
      }
     }
    }