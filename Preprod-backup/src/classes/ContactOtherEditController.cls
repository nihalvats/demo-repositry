Public Class ContactOtherEditController {

   public Id contactId{get;set;}
   public Contact con{get;set;}
   public ContactOtherEditController (){
      contactId = apexpages.currentpage().getparameters().get('id');
      con = [Select id,phone from contact where id =: contactId];
      System.debug('******ContactId********'+contactId);
      }
  public PageReference updateContact()      {
      System.debug('******ContactId********'+contactId);
      String p_phone = Apexpages.currentPage().getParameters().get('phone');
      Contact con1 = [Select id,phone from contact where id =: contactId];
      con1.phone = p_phone ;
      update con1;
      PageReference ref = new PageReference('/' + contactId);
      return ref;
      }
      
  }