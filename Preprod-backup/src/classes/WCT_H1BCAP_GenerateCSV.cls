public class WCT_H1BCAP_GenerateCSV {

public String header {get;set;}
public String profileName {get;set;}
public String exportType{get;set;}
public String URLType{get;set;}
//public string rec{get;set;}
public String exportStatus{get;set;}

public WCT_H1BCAP_GenerateCSV () {

    User UserInf =[Select profile.Name from User where id=:UserInfo.getUserId()];
    profileName = UserInf.profile.Name;
    
   URLType = ApexPages.currentPage().getParameters().get('type');

    if(profileName =='12_GM_&_I_RO') {
        exportStatus ='Sent to GM & I Team,Super RM Approved';
            header = 'S.No,Id,Practitioner Personnel  ID,Practitioner RecordID,Practitioner Name (as per GSS),Practitioner Email ID,Practitioner Level,Practitioner Date of Joining firm(Month/Date/Year),Deloitte Tenure (Years),Deloitte Entity,USI Base Office Location,Service Area,Service Line,Capability,Primary Module/ Skillset,Educational Background (graduation+ additional),Resource Manager Id,Resource Manager Name,Resource Manager Email ID,USI SM / GDM Id,USI SM Name / GDM,USI SM email id / GDM email id,Processing Type,Project Name,Rationale for project alignment,Client Name,Client Location :Suite number / Flat number / House number Please DO NOT include P.O. Box numbers in address,Client Location :Building name / Street name / Avenue / Drive / Road,Client Location :City and State,Client Location:ZIP Code,US PPD Name / LCSP Id,US PPD Name / LCSP Name,US PPD email Id / LCSP Email Id,USI SLL Id,USI SLL Name,USI SLL email id,Employment Status,RM Comments(if any),Service Area Leader Id,Service Area Leader Name,Service Area Leader email Id,FSS Leader Id,FSS Leader Name,FSS Leader Email Id,Delegate for USI SM / GDM Id,Delegate for USI SM / GDM,Delegate for USI SM / GDM Email Id,Project Manager Id,Project Manager Name,Project Manager Email Id,USI Expat Id,USI Expat,USI Expat email id,Deployment Advisor Id,Deployment Advisor Name,Deployment Advisor Email id,USI Business Immigration champion Id,USI Business Immigration champion Name,USI Business Immigration champion Email Id,CE Level(Mastery / Advanced / Foundation),YE Rating,Industry,Year At Level,Certification,CPA Certification,Total experience as of 31 March 2016,Status,Flagged of by GMI(TRUE/FALSE),Notes-GMI \r';   
         }
        else {
        if(URLType == 'sll') {
            exportStatus='USI SLL Realigned,USI SLL Rejected';
            }
            else {
            exportStatus='Ready for SLL approval';
            }
            header = 'S.No,Id,Practitioner Personnel  ID,Practitioner RecordID,Practitioner Name (as per GSS),Practitioner Email ID,Practitioner Level,Practitioner Date of Joining firm(Month/Date/Year),Deloitte Tenure (Years),Deloitte Entity,USI Base Office Location,Service Area,Service Line,Capability,Primary Module/ Skillset,Educational Background (graduation+ additional),Resource Manager Id,Resource Manager Name,Resource Manager Email ID,USI SM / GDM Id,USI SM Name / GDM,USI SM email id / GDM email id,Processing Type,Project Name,Rationale for project alignment,Client Name,Client Location :Suite number / Flat number / House number Please DO NOT include P.O. Box numbers in address,Client Location :Building name / Street name / Avenue / Drive / Road,Client Location :City and State,Client Location:ZIP Code,US PPD Name / LCSP Id,US PPD Name / LCSP Name,US PPD email Id / LCSP Email Id,USI SLL Id,USI SLL Name,USI SLL email id,Employment Status,RM Comments(if any),Service Area Leader Id,Service Area Leader Name,Service Area Leader email Id,FSS Leader Id,FSS Leader Name,FSS Leader Email Id,Delegate for USI SM / GDM Id,Delegate for USI SM / GDM,Delegate for USI SM / GDM Email Id,Project Manager Id,Project Manager Name,Project Manager Email Id,USI Expat Id,USI Expat,USI Expat email id,Deployment Advisor Id,Deployment Advisor Name,Deployment Advisor Email id,USI Business Immigration champion Id,USI Business Immigration champion Name,USI Business Immigration champion Email Id,CE Level(Mastery / Advanced / Foundation),YE Rating,Industry,Year At Level,Certification,CPA Certification,Total experience as of 31 March 2016,Status \r';   
        }
}
                  

    public  List<WCT_H1BCAP__c> getH1BCAPList() {
    system.debug('Export Values*****'+exportStatus );
  if(profileName =='12_GM_&_I_RO') {
          return [select WCT_Sl_No__c,Id,WCT_H1BCAP_Practitioner_Personal_Number__c,WCT_H1BCAP_Practitioner_Name__c,WCT_H1BCAP_Practitioner_Name__r.Name,WCT_H1BCAP_Email_ID__c,WCT_H1BCAP_Practitioner_Level__c,WCT_H1BCAP_Practitioner_Date_of_Joining__c,WCT_H1BCAP_Deloitte_Tenure_Years__c,WCT_H1BCAP_Deloitte_Entity__c,WCT_H1BCAP_Deloitte_Reporting_Office_Loc__c,WCT_H1BCAP_Practitioner_Service_Area__c,WCT_H1BCAP_Practitioner_Service_Line__c,WCT_H1BCAP_Capability__c,WCT_H1BCAP_Skill_set__c,WCT_H1BCAP_Educational_Background__c,WCT_H1BCAP_RM_Name__c,WCT_H1BCAP_RM_Name__r.Name,WCT_H1BCAP_Resource_Manager_Email_ID__c,WCT_H1BCAP_USI_SM_Name_GDM__c,WCT_H1BCAP_USI_SM_Name_GDM__r.Name,WCT_H1BCAP_GDM_Email_id__c,WCT_H1BCAP_Processing_Type__c,WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_Rationale_project_alignment__c,WCT_H1BCAP_Client_Name__c,WCT_H1BCAP_Client_Location_Flat_Number__c,WCT_H1BCAP_Client_location_Building__c,WCT_H1BCAP_Client_Location_City_State__c,WCT_H1BCAP_Client_Location_Zip_code__c,WCT_H1BCAP_US_PPD_Name__c,WCT_H1BCAP_US_PPD_Name__r.Name,WCT_H1BCAP_US_PPD_mail_Id__c,WCT_H1BCAP_USI_SLL_Name__c,WCT_H1BCAP_USI_SLL_Name__r.Name,WC_H1BCAP_USI_SLL_email_id__c,WCT_H1BCAP_Employment_Status__c,WCT_H1BCAP_RM_comments__c,WCT_H1BCAP_Service_Leader_Name__c,WCT_H1BCAP_Service_Leader_Name__r.Name,WCT_H1BCAP_Service_Area_Leader_email_Id__c,WCT_H1BCAP_FSS_Leader_Name__c,WCT_H1BCAP_FSS_Leader_Name__r.Name,WCT_H1BCAP_FSS_Leader_Email_Id__c,WCT_H1BCAP_Delegate_for_USI_SM_GDM__c,WCT_H1BCAP_Delegate_for_USI_SM_GDM__r.Name,WCT_H1BCAP_Delegate_USI_SM_GDM_Email__c,WCT_H1BCAP_Project_Manager_Name__c,WCT_H1BCAP_Project_Manager_Name__r.Name,WCT_H1BCAP_Project_Manager_Email_Id__c,WCT_H1BCAP_USI_Expat__c,WCT_H1BCAP_USI_Expat__r.Name,WCT_H1BCAP_USI_Expat_email_id__c,WCT_H1BCAP_Deployment_Advisor_Name__c,WCT_H1BCAP_Deployment_Advisor_Name__r.Name,WCT_H1BCAP_Deployment_Advisor_Email_id__c,WCT_H1BCAP_Business_immigration_champ__c,WCT_H1BCAP_Business_immigration_champ__r.Name,WCT_H1BCAP_Business_email__c,WCT_H1BCAP_CE_Level__c,WCT_H1BCAP_YE_Rating__c,WCT_H1BCAP_Industry__c,WCT_H1BCAP_Year_At_Level__c,WCT_H1BCAP_Certification__c,WCT_H1BCAP_CPA_Certification__c,WCT_H1BCAP_Total_experience__c,WCT_H1BCAP_Status__c,WCT_H1BCAP_Flagged_Off_by_GMI__c,WCT_H1BCAP_Notes_Given_GM_I__c from WCT_H1BCAP__c where WCT_H1BCAP_Status__c IN ('Sent to GM & I Team','Super RM Approved')];
  }
  
  else {
  //WCT_H1BCAP_RM_Onlys__c used this field to restrict others RM's 
  
  return [select WCT_Sl_No__c,Id,WCT_H1BCAP_Practitioner_Personal_Number__c,WCT_H1BCAP_Practitioner_Name__c,WCT_H1BCAP_Practitioner_Name__r.Name,WCT_H1BCAP_Email_ID__c,WCT_H1BCAP_Practitioner_Level__c,WCT_H1BCAP_Practitioner_Date_of_Joining__c,WCT_H1BCAP_Deloitte_Tenure_Years__c,WCT_H1BCAP_Deloitte_Entity__c,WCT_H1BCAP_Deloitte_Reporting_Office_Loc__c,WCT_H1BCAP_Practitioner_Service_Area__c,WCT_H1BCAP_Practitioner_Service_Line__c,WCT_H1BCAP_Capability__c,WCT_H1BCAP_Skill_set__c,WCT_H1BCAP_Educational_Background__c,WCT_H1BCAP_RM_Name__c,WCT_H1BCAP_RM_Name__r.Name,WCT_H1BCAP_Resource_Manager_Email_ID__c,WCT_H1BCAP_USI_SM_Name_GDM__c,WCT_H1BCAP_USI_SM_Name_GDM__r.Name,WCT_H1BCAP_GDM_Email_id__c,WCT_H1BCAP_Processing_Type__c,WCT_H1BCAP_Project_Name__c,WCT_H1BCAP_Rationale_project_alignment__c,WCT_H1BCAP_Client_Name__c,WCT_H1BCAP_Client_Location_Flat_Number__c,WCT_H1BCAP_Client_location_Building__c,WCT_H1BCAP_Client_Location_City_State__c,WCT_H1BCAP_Client_Location_Zip_code__c,WCT_H1BCAP_US_PPD_Name__c,WCT_H1BCAP_US_PPD_Name__r.Name,WCT_H1BCAP_US_PPD_mail_Id__c,WCT_H1BCAP_USI_SLL_Name__c,WCT_H1BCAP_USI_SLL_Name__r.Name,WC_H1BCAP_USI_SLL_email_id__c,WCT_H1BCAP_Employment_Status__c,WCT_H1BCAP_RM_comments__c,WCT_H1BCAP_Service_Leader_Name__c,WCT_H1BCAP_Service_Leader_Name__r.Name,WCT_H1BCAP_Service_Area_Leader_email_Id__c,WCT_H1BCAP_FSS_Leader_Name__c,WCT_H1BCAP_FSS_Leader_Name__r.Name,WCT_H1BCAP_FSS_Leader_Email_Id__c,WCT_H1BCAP_Delegate_for_USI_SM_GDM__c,WCT_H1BCAP_Delegate_for_USI_SM_GDM__r.Name,WCT_H1BCAP_Delegate_USI_SM_GDM_Email__c,WCT_H1BCAP_Project_Manager_Name__c,WCT_H1BCAP_Project_Manager_Name__r.Name,WCT_H1BCAP_Project_Manager_Email_Id__c,WCT_H1BCAP_USI_Expat__c,WCT_H1BCAP_USI_Expat__r.Name,WCT_H1BCAP_USI_Expat_email_id__c,WCT_H1BCAP_Deployment_Advisor_Name__c,WCT_H1BCAP_Deployment_Advisor_Name__r.Name,WCT_H1BCAP_Deployment_Advisor_Email_id__c,WCT_H1BCAP_Business_immigration_champ__c,WCT_H1BCAP_Business_immigration_champ__r.Name,WCT_H1BCAP_Business_email__c,WCT_H1BCAP_CE_Level__c,WCT_H1BCAP_YE_Rating__c,WCT_H1BCAP_Industry__c,WCT_H1BCAP_Year_At_Level__c,WCT_H1BCAP_Certification__c,WCT_H1BCAP_CPA_Certification__c,WCT_H1BCAP_Total_experience__c,WCT_H1BCAP_Status__c from WCT_H1BCAP__c where WCT_H1BCAP_Status__c =:exportStatus AND WCT_H1BCAP_RM_Onlys__c = true];
  }
  
  }
  
    
}