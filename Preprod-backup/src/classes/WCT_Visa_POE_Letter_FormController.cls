/********************************************************************************************************************************
Apex class         : <WCT_Visa_POE_Letter_FormController>
Description        : <Controller which allows to Update Task and Mobility>
Type               :  Controller
Test Class         : <WCT_Visa_POE_Letter_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 24/05/2016          0%                          --                            License Cleanup Project
************************************************************************************************************************************/ 
public class WCT_Visa_POE_Letter_FormController extends SitesTodHeaderController{

    // PUBLIC VARIABLES
    public WCT_Mobility__c mobilityRecord{get;set;}
    // UPLOAD RELATED VARIABLES
    
    public Document doc {get;set;}
    public List<String> docIdList = new List<string>();
    public Task t{get;set;}
    public String taskid{get;set;}
    public List<Attachment> listAttachments {get; set;}
    public Map<Id, String> mapAttachmentSize {get; set;}  
    public Integer countattach{get;set;}


    // ERROR MESSAGE RELATED VARIABLES
    public boolean pageError {get; set;}
    public String pageErrorMessage {get; set;}
    public String supportAreaErrorMesssage {get; set;}
    public boolean validPage {get;set;}
    public GBL_Attachments attachmentHelper{get; set;}
    public String taskVerbiage{get;set;}
    public WCT_Task_Reference_Table__c taskRefRecord{get;set;}
    
    //DEFINING A CONSTRUCTOR 
    public WCT_Visa_POE_Letter_FormController()
    {
         init();
        attachmentHelper= new GBL_Attachments();   
        getParameterInfo();  
        
        if(taskid=='' || taskid==null)   
        {
           invalidEmployee=true;
           return;
        }
        getTaskInstance();
        getAttachmentInfo();
        getMobilityDetails();
        taskVerbiage = '';
    }
    
/********************************************************************************************
*Method Name         : <init()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <Init() Used for loading Mobility,Attachments>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/   

    private void init(){

       mobilityRecord = new  WCT_Mobility__c();
       countattach = 0;
       doc = new Document();
       listAttachments = new List<Attachment>();
       mapAttachmentSize = new Map<Id, String>();
   } 

/********************************************************************************************
*Method Name         : <getMobilityDetails()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetMobilityDetails() Used for Fetching Mobility Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/       
 
    public void getMobilityDetails()
    {
        mobilityRecord = [SELECT id,OwnerId FROM WCT_Mobility__c
                            where id=:t.WhatId];
                           
    }

/********************************************************************************************
*Method Name         : <getParameterInfo()>
*Return Type         : <Null>
*Param’s             : URL
*Description         : <GetParameterInfo() Used to get URL Params for TaskID>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/      

    private void getParameterInfo(){
        taskid = ApexPages.currentPage().getParameters().get('taskid');
    }

/********************************************************************************************
*Method Name         : <getTaskInstance()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetTaskInstance() Used for Querying Task Instance>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/     
    
    private void getTaskInstance(){
        t=[SELECT Status,OwnerId,WhatId,WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c,WCT_Task_Reference_Table_ID__c   FROM Task WHERE Id =: taskid];
        taskRefRecord = [SELECT Id, Name, WCT_Task_for_Object__c, WCT_Task_Subject__c, Form_Verbiage__c, ownerId FROM WCT_Task_Reference_Table__c Where id=:t.WCT_Task_Reference_Table_ID__c];
        taskVerbiage = taskRefRecord.Form_Verbiage__c;
    }

/********************************************************************************************
*Method Name         : <getAttachmentInfo()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <GetAttachmentInfo() Used for Retrieving Attachment Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/
   
    @TestVisible
    private void getAttachmentInfo(){
        listAttachments = [SELECT ParentId , 
                                    Name, 
                                    BodyLength, 
                                    Id,
                                    CreatedDate
                                    FROM  Attachment 
                                    WHERE ParentId = :taskid  
                                    ORDER BY CreatedDate DESC
                                    LIMIT 50 ];    
              
        for(Attachment a : listAttachments) {
            String size = null;

            if(1048576 < a.BodyLength){
                //SIZE GREATAR THAN 1MB
                size = '' + (a.BodyLength / 1048576) + ' MB';
            }
            else if(1024 < a.BodyLength){
                //SIZE GREATER THAN 1KB
                size = '' + (a.BodyLength / 1024) + ' KB';            
            }
            else{
                size = '' + a.BodyLength + ' bytes';
            }
            mapAttachmentSize.put(a.id, size);
        }
    }

/********************************************************************************************
*Method Name         : <save()>
*Return Type         : <PageReference>
*Param’s             : 
*Description         : <GetMobilityDetails() Used for for Updating Task and Mobility>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/    
     
     public pageReference save()  {
        //Changing  Owner Inorder To Avoid Integrity Exception
        try {
        t.OwnerId=UserInfo.getUserId();
        upsert t;
        
        //Check whether file is attached   
        if(attachmentHelper.docIdList.isEmpty()) {
            pageErrorMessage = 'Attachment is required to submit the form.';
            pageError = true;
            return null;

        }
         attachmentHelper.uploadRelatedAttachment(t.id);

        if(string.valueOf(mobilityRecord.Ownerid).startsWith('00G')){
           t.Ownerid = System.Label.GMI_User;
        }else{
            t.OwnerId = mobilityRecord.Ownerid;
        }
        
       //UPDATING TASK RECORD
        if(t.WCT_Auto_Close__c == true){   
            t.status = 'completed';
        }else{
            t.status = 'Employee Replied';  
        }

        t.WCT_Is_Visible_in_TOD__c = false; 
        upsert t;
        
        //RESET ERROR MESSAGE
        pageError = false;

        getAttachmentInfo();
        mobilityRecord.WCT_Do_you_have_POE_Letter__c = 'Yes';
        update mobilityRecord;
        }
        catch (Exception e) {
        Exception_Log__c errLog = WCT_ExceptionUtility.logException('WCT_Visa_POE_Letter_FormController', 'Visa POE Letter Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
        
        Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_POE_EXP&expCode='+errLog.Name);
        pg.setRedirect(true);
        return pg;
        }
        //REDIRECTING TO THANK YOU PAGE PAGE
        Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_VPOE_MSG');
        pg.setRedirect(true);
        return pg;
        }
}