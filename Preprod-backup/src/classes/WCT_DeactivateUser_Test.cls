/**
    * Class Name  : WCT_DeactivateUser_Test
    * Description : This apex test class will use to test the WCT_DeactivateUser
*/
@isTest
private class WCT_DeactivateUser_Test {

	public static String candidateRecordTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
    
	public static List<Contact> contactList=new List<Contact>();
    /** 
        Method Name  : createContacts
        Return Type  : List<Contact>
        Type 		 : private
        Description  : Create temp records for data mapping         
    */
    private static List<Contact> createContacts()
    {
    	contactList=WCT_UtilTestDataCreation.createContactWithCandidate(candidateRecordTypeId);
    	insert contactList;
    	return contactList;
    }
    
    static testMethod void myUnitTest() {
    	Test.startTest();
        contactList=createContacts();
        for(Contact con:contactList)
        {
        	con.WCT_Employee_Status__c='Withdrawn';
        }
        update contactList;
        String query='Select id,Email from Contact where WCT_Employee_Status__c=\'Withdrawn\' or WCT_Employee_Status__c=\'Retiree\' ';
        WCT_DeactivateUser b = new WCT_DeactivateUser (query); 
      	database.executebatch(b,2000);
        Test.stopTest();
    }
}