@istest
public class WCT_H1bcap_SM_SLL_Reject_Test{
static testmethod void WCT_H1bcap_SM_SLL_Reject_TestMethod(){
    
     recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
     Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
     con.WCT_Employee_Group__c = 'Active';
     insert con;
    
     Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_RM']; 
     User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','test@deloitte.com');
     insert platuser;
    
     
     WCT_H1BCAP__c h1 = new WCT_H1BCAP__c();
     h1.WCT_H1BCAP_Practitioner_Name__c = con.id;
     h1.WCT_H1BCAP_Email_ID__c = 'test@deloitte.com';
     h1.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
     h1.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'test@deloitte.com';
     h1.Wct_H1bcap_SM_Realigned_Date__c  = system.today();
     h1.Wct_H1bCap_SM_RejectedDate__c  = system.today();
     h1.Wct_H1bcap_SLL_RejectDate__c  = system.today();
     h1.Wct_H1bcap_SLL_RealignedDate__c  = system.today();
     h1.WCT_H1BCAP_Status__c = 'USI SM Rejected '; 
     h1.WCT_H1BCAP_RM_Name__c = platuser.id;
     insert h1;
     
    string orgmail =  Label.WCT_H1BCAP_Mailbox;
    OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
    Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'wct_H1bcap_Notification_to_SM_Reject' AND IsActive = true];
    
    Test.StartTest(); 
    WCT_H1bcap_SM_SLL_Reject  h1b = new WCT_H1bcap_SM_SLL_Reject ();
    WCT_H1bcap_SM_SLL_Reject_Schedule  createCon = new WCT_H1bcap_SM_SLL_Reject_Schedule();
    system.schedule('New','0 0 2 1 * ?',createCon); 
    database.Executebatch(h1b); 
    Test.StopTest();   
 
    
        }
        
 static testmethod void WCT_H1bcap_SM_SLL_Reject_TestMethod1(){
    
     recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
     Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
     con.WCT_Employee_Group__c = 'Active';
     insert con;
    
     Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_RM']; 
     User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','test@deloitte.com');
     insert platuser;
    
     
     WCT_H1BCAP__c h1 = new WCT_H1BCAP__c();
     h1.WCT_H1BCAP_Practitioner_Name__c = con.id;
     h1.WCT_H1BCAP_Email_ID__c = 'test@deloitte.com';
     h1.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
     h1.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'test@deloitte.com';
     h1.Wct_H1bcap_SM_Realigned_Date__c  = system.today();
     h1.Wct_H1bCap_SM_RejectedDate__c  = system.today();
     h1.Wct_H1bcap_SLL_RejectDate__c  = system.today();
     h1.Wct_H1bcap_SLL_RealignedDate__c  = system.today();
      h1.WCT_H1BCAP_Status__c = 'USI SLL Rejected ';  
     h1.WCT_H1BCAP_RM_Name__c = platuser.id;
     insert h1;
    
     
    string orgmail =  Label.WCT_H1BCAP_Mailbox;
    OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
    Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'wct_H1bcap_Notification_to_SM_Reject' AND IsActive = true];
    
    Test.StartTest(); 
    WCT_H1bcap_SM_SLL_Reject  h1b = new WCT_H1bcap_SM_SLL_Reject ();
    WCT_H1bcap_SM_SLL_Reject_Schedule  createCon = new WCT_H1bcap_SM_SLL_Reject_Schedule();
    system.schedule('New','0 0 2 1 * ?',createCon); 
    database.Executebatch(h1b); 
    Test.StopTest();   
 
    
        }       
        
    static testmethod void WCT_H1bcap_SM_SLL_Reject_TestMethod2(){
    
     recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
     Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
     con.WCT_Employee_Group__c = 'Active';
     insert con;
    
     Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_RM']; 
     User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','test@deloitte.com');
     insert platuser;
    
     
     WCT_H1BCAP__c h1 = new WCT_H1BCAP__c();
     h1.WCT_H1BCAP_Practitioner_Name__c = con.id;
     h1.WCT_H1BCAP_Email_ID__c = 'test@deloitte.com';
     h1.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
     h1.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'test@deloitte.com';
     h1.Wct_H1bcap_SM_Realigned_Date__c  = system.today();
     h1.Wct_H1bCap_SM_RejectedDate__c  = system.today();
     h1.Wct_H1bcap_SLL_RejectDate__c  = system.today();
     h1.Wct_H1bcap_SLL_RealignedDate__c  = system.today();
      h1.WCT_H1BCAP_Status__c = 'USI SLL Realigned ';  
     h1.WCT_H1BCAP_RM_Name__c = platuser.id;
     insert h1;
    
     
    string orgmail =  Label.WCT_H1BCAP_Mailbox;
    OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
    Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'wct_H1bcap_Notification_to_SLL_Realigned' AND IsActive = true];
    
    Test.StartTest(); 
    WCT_H1bcap_SM_SLL_Reject  h1b = new WCT_H1bcap_SM_SLL_Reject ();
    WCT_H1bcap_SM_SLL_Reject_Schedule  createCon = new WCT_H1bcap_SM_SLL_Reject_Schedule  ();
   system.schedule('New','0 0 2 1 * ?',createCon); 
    database.Executebatch(h1b); 
    Test.StopTest();   
 
    
        }           
        
        
    }