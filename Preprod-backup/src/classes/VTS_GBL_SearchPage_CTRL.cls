global class VTS_GBL_SearchPage_CTRL {
        public static integer limitToDisplay=1000;
        public static integer limitToExport=1000;
        public List<WCT_Immigration__c> idsd{get;set;}   
        public List<WCT_Immigration__c> topList{get{
        List<WCT_Immigration__c> temp= new List<WCT_Immigration__c>();
            if(idsd!=null)
            {
                if(idsd.size()>limitToDisplay)
                {
                    for(integer i=0; i<limitToDisplay;i++)
                    {
                        temp.add(idsd[i]);
                    }
                }
                else
                {
                    temp=idsd;
                }
            }
            return temp;
        } set;} 
        
        
        public boolean displayresults{get;set;}
        public boolean displayalert{get;set;}
        
       /* public String selectedValues{get; set;}
        public String queryclocs='';
*/
        
        public WCT_Immigration__c tempImmigration{get; set;}
        public WCT_Immigration__c tempImmigration2{get; set;}
        
        /*public string pskills{get; set;}
        public string starsearch{get;set;} */
        public string searchType{get; set;}
        
        public string type{get; set;}
        /*Multi Select Fields */
        public String deloitteOffice{get; set;}
        public String RFEPVL{get; set;}
        public String PVL{get; set;}
        public String serviceArea{get; set;}
        public String serviceLine{get; set;}
        public String industry{get; set;}
        public String capability{get; set;}
        public String region{get; set;}
        public String hireStatus{get; set;}
    
    
        public String OBS{get; set;}
        public String recruiter{get; set;}
        public String Type_of_H1B_process{get; set;}
        public String processStatus{get; set;}
        public String RFEStatus{get; set;}
        public String Escalation{get; set;}
        public String H1B_No_PVL_PPD_Letter_Required{get; set;}
        public String US_Or_USI{get; set;}
        public List<SelectOption> options {get; set;}
        public String TypeApplicationProcess {get; set;}
        
       
        public pagereference ClearControls()
        {
           PageReference pageRef = new PageReference('/apex/VTS_GBL_SearchPage');
            pageRef.getParameters().put('type',searchType);
           pageRef.setRedirect(true);
           return pageRef;            
           //  return new PageReference('/apex/IDSsearch' );
        }
        public VTS_GBL_SearchPage_CTRL ()
        {

            displayresults=false;
            idsd= new List<WCT_Immigration__c>();
            
            tempImmigration= new WCT_Immigration__c();
            tempImmigration.WCT_B_Package_Sent_Date__c=null;
            tempImmigration.WCT_B_Package_Sent_Date__c=null;
            tempImmigration.H1B_No_PVL_PPD_Letter_Required__c=null;
            
            
            searchType= ApexPages.CurrentPage().getParameters().get('type');
            tempImmigration2= new WCT_Immigration__c();
           
            topList= new List<WCT_Immigration__c>();
            
            /*Multi Select Fields*/
            deloitteOffice='';
            RFEPVL='';
            PVL='';
            serviceArea='';
            serviceLine='';
            industry='';
            capability='';
            region='';
            hireStatus='';
            OBS='';
            recruiter='';
            Type_of_H1B_process='';
            processStatus='';
            RFEStatus='';
            H1B_No_PVL_PPD_Letter_Required='';
            Escalation='';
            US_Or_USI='';
            TypeApplicationProcess='';
            
            
                        options= new List<SelectOption>();
               Schema.DescribeFieldResult fieldResult =
               WCT_Immigration__c.H1B_Type_of_H1B_Application_Process__c.getDescribe();
               List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                    
               for( Schema.PicklistEntry f : ple)
               {
                  options.add(new SelectOption(f.getLabel(), f.getValue()));
               } 
        }
        Public void search(){
            
             idsd= new List<WCT_Immigration__c>();
             
             
            try{
                String queryWhereClause = '';
                
                String fDateQuery = '';
                String tDateQuery = '';
                Set<String> clids = new Set<String>();
                Set<String> cps = new Set<String>();  
                String queryWhere=''; 
                String idStr = '';  
                
                
                displayalert=true;
                if(tempImmigration.H1B_Date_PPD_Letter_Received__c!= null)
                {
                    string dateString=String.valueOf(tempImmigration.H1B_Date_PPD_Letter_Received__c);
                    queryWhere+= ' AND H1B_Date_PPD_Letter_Received__c>= '+dateString+'';
                    
                }
                
                
                if(tempImmigration2.H1B_Date_PPD_Letter_Received__c!= null)
                {
                    string dateString=String.valueOf(tempImmigration2.H1B_Date_PPD_Letter_Received__c);
                    queryWhere+= ' AND H1B_Date_PPD_Letter_Received__c<= '+dateString+'';
                }
                
                
                
                if(US_Or_USI!='')
                {
                    
                    String temp=US_Or_USI.replace(',','\',\'');
                    temp='\''+temp+'\'';
                    queryWhere+=' AND H1B_US_or_USI__c in ('+temp+')';
                }
                
                if(deloitteOffice!='' && deloitteOffice!=null)
                {
                    String temp=deloitteOffice.replace(',','\',\'');
                    temp='\''+temp+'\'';
                    queryWhere+=' AND H1B_Deloitte_Office__c in ('+temp+')';
                    
                }
                if(RFEPVL!='' && RFEPVL!=null)
                {
                    List<PVL_Database__c> pvls=[Select Id, Name From PVL_Database__c Where Name =:RFEPVL];
                    if(pvls.size()>0)
                    {
                        queryWhere+=' AND RFE_PVL_lk__c =\''+pvls[0].Id+'\'';
                    }
                    
                }
                if(PVL!='' && PVL!=null)
                {
                    List<PVL_Database__c> pvls=[Select Id, Name From PVL_Database__c Where Name =:PVL];
                    if(pvls.size()>0)
                    {
                        queryWhere+=' AND PVL_Database__c =\''+pvls[0].Id+'\'';
                    }
                    
                }
                
                if(serviceArea!='' & serviceArea!=null)
                {
                    String temp=serviceArea.replace(',','\',\'');
                    temp='\''+temp+'\'';
                    queryWhere+=' AND Service_Area__c in ('+temp+')';
                    
                }
                System.debug('## serviceLine '+serviceLine);
                if(serviceLine!='' & serviceLine!=null )
                {
                    String temp=serviceLine.replace(',','\',\'');
                    temp='\''+temp+'\'';
                    queryWhere+=' AND Service_Line__c in ('+temp+')';
                    
                }
                if(Escalation!='' && Escalation!=null )
                {
                    System.debug(''+Escalation);
                    String temp=(Escalation=='Yes'?'true':'false');
                     queryWhere+=' AND H1B_Escalation__c ='+temp;
                }
                if(industry!='' && industry!=null)
                {
                    String temp=industry.replace(',','\',\'');
                    temp='\''+temp+'\'';
                    queryWhere+=' AND H1B_Industry__c in ('+temp+')';                  
                    
                }
                if(capability!='' && capability!=null)
                {
                    String temp=capability.replace(',','\',\'');
                    temp='\''+temp+'\'';
                    queryWhere+=' AND H1B_Capability__c in ('+temp+')';                    
                    
                }
                
                if(H1B_No_PVL_PPD_Letter_Required!='' && H1B_No_PVL_PPD_Letter_Required!=null)
                {
                    
                    String temp=(H1B_No_PVL_PPD_Letter_Required=='Yes'?'true':'false');
                    queryWhere+=' AND H1B_No_PVL_PPD_Letter_Required__c ='+temp;
                }
                
                if(region!='' && region!=null)
                {
                    String temp=region.replace(',','\',\'');
                    temp='\''+temp+'\'';
                    queryWhere+=' AND H1B_Region__c in ('+temp+')';                    
                    
                }
                
                System.debug(' TypeApplicationProcess '+TypeApplicationProcess);
                if(TypeApplicationProcess!='' && TypeApplicationProcess!=null)
                {
                    String temp=TypeApplicationProcess.replace(',','\',\'');
                    temp='\''+temp+'\'';
                    queryWhere+=' AND H1B_Type_of_H1B_Application_Process__c in ('+temp+')';  
                    
                   // queryWhere+=' AND H1B_Type_of_H1B_Application_Process__c in \''+tempImmigration.H1B_Type_of_H1B_Application_Process__c+'\''; 
                }
                
                if(tempImmigration.H1B_Process_Status__c!=null && tempImmigration.H1B_Process_Status__c!='')
                {
                    queryWhere+=' AND H1B_Process_Status__c = \''+tempImmigration.H1B_Process_Status__c+'\'';
                    
                }
                
                if(tempImmigration.H1B_RFE_Status__c!=null && tempImmigration.H1B_RFE_Status__c!='')
                {
                    queryWhere+=' AND H1B_RFE_Status__c = \''+tempImmigration.H1B_RFE_Status__c+'\'';
                    
                }
                
                if(tempImmigration.H1B_Confirmation_Status_from_PPD__c!=null && tempImmigration.H1B_Confirmation_Status_from_PPD__c!='')
                {
                    queryWhere+=' AND H1B_Confirmation_Status_from_PPD__c = \''+tempImmigration.H1B_Confirmation_Status_from_PPD__c+'\'';
                    
                }
                
                /*CAP Related Queries*/
                
                if(hireStatus!='' && hireStatus!=null)
                {    System.debug('hireStatus---------------------------------------------------------------------------------------->>>>>>>>>>>>');
                    queryWhere+=' AND H1B_Hire_Status_CAP__c like \'%'+hireStatus+'%\'';
                }
                
                if(tempImmigration.H1B_Work_Authorization_Through_4_1_CAP__c!='' && tempImmigration.H1B_Work_Authorization_Through_4_1_CAP__c!=null)
                {System.debug('4/1cap---------------------------------------------------------------------------------------->>>>>>>>>>>>');
                   queryWhere+=' AND H1B_Work_Authorization_Through_4_1_CAP__c like \''+tempImmigration.H1B_Work_Authorization_Through_4_1_CAP__c+'\'';
                }
                if(tempImmigration.H1B_Work_Authorization_Through_10_1_CAP__c!='' && tempImmigration.H1B_Work_Authorization_Through_10_1_CAP__c!=null)
                {System.debug('10/1CAP---------------------------------------------------------------------------------------->>>>>>>>>>>>');
                   queryWhere+=' AND H1B_Work_Authorization_Through_10_1_CAP__c like \''+tempImmigration.H1B_Work_Authorization_Through_10_1_CAP__c+'\'';
                }
                
                if(tempImmigration.H1B_RFE_Due_Date__c!= null )
                {
                    string dateString=String.valueOf(tempImmigration.H1B_RFE_Due_Date__c);
                    queryWhere+= ' AND H1B_RFE_Due_Date__c>= '+dateString+'';
                    
                }
                
                
                if(tempImmigration2.H1B_RFE_Due_Date__c!= null)
                {
                    string dateString=String.valueOf(tempImmigration2.H1B_RFE_Due_Date__c);
                    queryWhere+=' AND H1B_RFE_Due_Date__c<= '+dateString;
                }
                
                 if( tempImmigration.H1BVTS_Business_Advisor_CAP__c!=null)
                {
                   queryWhere+=' AND H1BVTS_Business_Advisor_CAP__c = \''+tempImmigration.H1BVTS_Business_Advisor_CAP__c+'\'';
                }
                
                
                if(OBS!='' && OBS!=null)
                {
                    queryWhere+=' AND H1B_OBS__c like \'%'+OBS+'%\'';
                    
                }
                if(Type_of_H1B_process!='' && Type_of_H1B_process!=null)
                {
                    queryWhere+=' AND H1B_Type_of_H1B_Application_Process__c like \'%'+Type_of_H1B_process+'%\'';
                    
                }
                
                if(recruiter!='' && recruiter!=null)
                {
                    queryWhere+=' AND H1B_Recruiter__c like \'%'+recruiter+'%\'';
                }
                
              
              
                
                system.debug('************************************ queryWhere '+queryWhere); 
                
                
               
                string query='Select Name, Id,Last_Name__c , First_Name__c,  H1B_RFE_Status__c,H1B_Process_Status__c,H1B_Type_of_H1B_Application_Process__c,H1B_Date_PPD_Letter_Received__c,H1B_Escalation__c,H1B_No_PVL_PPD_Letter_Required__c,RFE_PVL_lk__r.Name,H1B_Confirmation_Status_from_PPD__c,H1B_Deloitte_Office__c,H1B_Industry__c,H1B_Capability__c,H1B_US_or_USI__c,H1B_Region__c,Service_Area__c,Service_Line__c ,PVL_Database__r.Name,H1B_Service_Area_Fragomen__c ,H1B_Service_Line_Fragomen__c , H1B_Resource_Manager_Fragomen__c , H1B_Recuiter_Fragomen__c ,H1B_Recuiter_Email_Fragomen__c , H1B_PVL_Fragomen__c , H1B_Project_PPD_Name_Fragomen__c , H1B_Project_PPD_Email_Fragomen__c , H1B_Project_Name_Fragomen__c , Personnel_Number_Fragomen__c , H1B_Office_Location_Fragomen__c , H1B_Level_Fragomen__c , H1B_Hire_Status_CAP__c , WCT_Fragomen_Milestone_Change__c from WCT_Immigration__c where  id<>null and H1B_Is_US_Upload__c=True '   ;
                System.debug('Query---------------------------------------------------------------------------------------->>>>>>>>>>>>');
               if(queryWhere!='')
               {
                  query+= queryWhere;
               }
               
               if(fDateQuery!='')
               {
                   query+= fDateQuery;
               }
               if(tDateQuery!='')
               {
                   query+= tDateQuery;
               }
                
                query+=' limit '+limitToDisplay;
               
               
                system.debug('#################################### query '+query);
                
                system.debug('sqlqery+'+query);
                idsd =database.query(query);
                
                system.debug('*idsd*'+idsd);
                if(idsd.size()>0)
                {
                    displayresults=true;
                    displayalert=false;
                    
                }
                else
                {
                    displayresults=true;
                    displayalert=true;
                }
                system.debug('*displayresults*'+displayresults);
                //clearAll();
            }
            catch(System.SearchException ex){
                apexpages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please Enter keywords.'));
            }
        }
        
      
        
        
        
        
        
        
        
        public PageReference export()
        {
            PageReference pr= Page.VTS_GBL_SearchPage_Export;
            return pr;
        }
        
       
        
        
         @RemoteAction
        public static  List<WCT_Immigration__c> getExistingCompanies(String fieldName)
        {
            List<WCT_Immigration__c> listItems;
            
            listItems= [Select Id, PVL_Database__c from WCT_Immigration__c ORDER BY PVL_Database__c ASC  limit 50000];
            return listItems;
         }
         
          
         @RemoteAction
        global static  List<WCT_List_Of_Names__c> getItemsList(String fieldName)
        {
            List<WCT_List_Of_Names__c> listItems;
            
            listItems= [Select Id, Name from WCT_List_Of_Names__c where RecordTypeId =:Label.MultiSelectPicklist and MultiSelectIdentifier__c=:fieldName  ORDER BY Name ASC ];
            return listItems;
            
        }
    
                

    }