public class Adopt_VF_Tab_Controller {
    public List<Contact> contacts = new List<Contact>();
    Id recTypeId;
    public Adopt_VF_Tab_Controller(ApexPages.StandardController controller) {
        recTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Employee').getRecordTypeId();
        contacts = [Select WCT_Person_Id__c,Name,WCT_Region__c,CreatedDate,WCT_Employee_Status__c,Adopted_Ada_c__c 
                    From Contact
                    Where WCT_Type__c =:'Employee' limit 1000 ];
                 
    }
    public List<Contact> getContacts(){
        return contacts;
    }
    public pageReference redToDetail()      
    {           
        PageReference viewPage = new PageReference('/'+ApexPages.currentPage().getParameters().get('cid'));
        viewPage.setRedirect(true);           
        return viewPage;      
    }
    public PageReference createNew()
    {
         
        PageReference newEditPage = new PageReference('/'+'003/e'+'?RecordType='+recTypeId+'&ent=Contact');
        newEditPage.setRedirect(true);           
        return newEditPage; 
    
    }

}