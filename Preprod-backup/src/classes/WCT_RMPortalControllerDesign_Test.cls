@isTest
public class WCT_RMPortalControllerDesign_Test{
    
   // String smemailAddress = UserInfo.getUserEmail();
   // public static final id candRecTypeId=WCT_Util.getRecordTypeIdByLabel('Contact','Employee');
    
  static testMethod void WCT_RMPortalControllerDesign_TestMethod(){ 
 
 
 /* 
  Profile Sp = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SUPERRM']; 
  User platuserSRM =WCT_UtilTestDataCreation.createUser( 'sivaSRM',Sp.id,' svallurutestSRM@deloitte.com.preprod','svalluruSRM@deloitte.com');
  insert platuserSRM;
 */ 
  Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_RM']; 
  User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','svalluru@deloitte.com');
  insert platuser;
  Profile PSM = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SM']; 
  User platSMuser=WCT_UtilTestDataCreation.createUser( 'siva283',PSM.id,' svallurutest99@deloitte.com.preprod','svalluru1@deloitte.com');
  insert platSMuser;
  Profile PPPD = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_PPD']; 
  User platPPDuser=WCT_UtilTestDataCreation.createUser( 'sivappd',PPPD.id,' svallurutestppd@deloitte.com.preprod','svalluruppd@deloitte.com');
  insert platPPDuser;
  Profile PSLL = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SLL']; 
  User platSLLuser=WCT_UtilTestDataCreation.createUser( 'sivasll',PPPD.id,' svallurutestsll@deloitte.com.preprod','svallurusll@deloitte.com');
  insert platSLLuser;
  
  system.runas(platuser){
Recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
  //        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          Contact con= new contact();
          con.WCT_Employee_Status__c= 'Active';
          con.WCT_Personnel_Number__c = 12345;
          con.WCT_Contact_Type__c = 'Employee';
          con.lastname = 'Testuser';
          con.Email = 'svalluru@deloitte.com';
          con.Recordtypeid = rt.id;
          insert con;
   system.debug('********'+con);   
    //FSS Leader Name Insert
    
    Contact conFSS =WCT_UtilTestDataCreation.createEmployee(rt.id);
          conFSS.WCT_Employee_Group__c = 'Active';
          conFSS.WCT_Personnel_Number__c = 45785;
          conFSS.WCT_Contact_Type__c = 'Employee';
          conFSS.lastname = 'TestFSSuser';
          conFSS.Email = 'testFSS@deloitte.com';
          insert conFSS;      
   
   // Service Area Leader Name Insert
   
   Contact conSAL =WCT_UtilTestDataCreation.createEmployee(rt.id);
          conSAL.WCT_Employee_Group__c = 'Active';
          conSAL.WCT_Personnel_Number__c = 457985;
          conSAL.WCT_Contact_Type__c = 'Employee';
          conSAL.lastname = 'TestSALuser';
          conSAL.Email = 'testSAL@deloitte.com';
          insert conSAL; 
  
    // Project Manager Name insert
          Contact conPM =WCT_UtilTestDataCreation.createEmployee(rt.id);
          conPM.WCT_Employee_Group__c = 'Active';
          conPM.WCT_Personnel_Number__c = 985;
          conPM.WCT_Contact_Type__c = 'Employee';
          conPM.lastname = 'TestPMuser';
          conPM.Email = 'testPM@deloitte.com';
          insert conPM;
          
    // USI Expat Insert    
          Contact conUSI = WCT_UtilTestDataCreation.createEmployee(rt.id);
          conUSI.WCT_Employee_Group__c = 'Active';
          conUSI.WCT_Personnel_Number__c = 98578;
          conUSI.WCT_Contact_Type__c = 'Employee';
          conUSI.lastname = 'TestUSIuser';
          conUSI.Email = 'testUSI@deloitte.com';
          insert conUSI;
   
    // Deployment Advisor Name Insert
          Contact conDA = WCT_UtilTestDataCreation.createEmployee(rt.id);
          conDA.WCT_Employee_Group__c = 'Active';
          conDA.WCT_Personnel_Number__c = 985789;
          conDA.WCT_Contact_Type__c = 'Employee';
          conDA.lastname = 'TestDAuser';
          conDA.Email = 'testDA@deloitte.com';
          insert conDA;
   
    // USI Business Immigration champion Name Insert
          Contact conBI = WCT_UtilTestDataCreation.createEmployee(rt.id);
          conBI.WCT_Employee_Group__c = 'Active';
          conBI.WCT_Personnel_Number__c = 1985789;
          conBI.WCT_Contact_Type__c = 'Employee';
          conBI.lastname = 'TestBIuser';
          conBI.Email = 'testBI@deloitte.com';
          insert conBI;
       
  // Contact c=[ SELECT Id, WCT_Personnel_Number__c,Email,WCT_Employee_Status__c  FROM Contact WHERE WCT_Personnel_Number__c =: con.WCT_Personnel_Number__c limit 1 ];
   
 //  system.debug('Value*******'+c);       
         
  String csvcontent = 'Sl.No,Practitioner Personnel ID,Practitioner Name (as per GSS),Practitioner Email ID,Practitioner Level,Practitioner Date of Joining firm(Month/Date/Year),Deloitte Tenure ,Deloitte Entity,'+
            'USI Base Office Location,Service Area,Service Line,Capability,Primary Module/ Skillset,'+
            'Educational Background,Resource Manager Name,Resource Manager Email ID,USI SM Name / GDM,USI SM email id / GDM email id,Processing Type,'+
            'Project Name,Rationale for project alignment,Client Name,Client Location,Client Location :Building name,Client Location :City and State,'+
            'Client Location:ZIP Code,USI PPD Name,US PPD email Id,USI SLL Name,USI SLL email id,Employment Status,RM Comments(if any),'+
            'Service Area Leader Name,Service Area Leader email Id,FSS Leader Name,FSS Leader Email Id,Delegate for USI SM / GDM,Delegate for USI SM / GDM Email Id,'+
            'Project Manager Name,Project Manager Email Id,USI Expat,USI Expat email id,Deployment Advisor Name,Deployment Advisor Email id,'+
            'USI Business Immigration champion Name,USI Business Immigration champion Email Id,CE Level,YE Rating,Industry,Year At Level,Certification,CPA Certification,Total experience';
            
     string   data = ' 1,12345,Testuser,svalluru@deloitte.com,3,12/5/2011,4,Deloitte Services LP,'+
            'MUMBAI ,Business Tax Svcs,Tax Management Consulting (TMC),NA,Salesforce,B.Tech,siva83,svalluru@deloitte.com,siva283,svalluru1@deloitte.com,Premium,'+
            'WCT_Project,MO,Testclient,210,West Street,NY,10030,sivappd,svalluruppd@deloitte.com,sivasll,svallurusll@deloitte.com,Active,TestcommentbyRM,'+
            'TestSALuser,testSAL@deloitte.com,TestFSSuser,testFSS@deloitte.com,,,TestPMuser,testPM@deloitte.com,TestUSIuser,testUSI@deloitte.com,TestDAuser,testDA@deloitte.com,'+
            'TestBIuser,testBI@deloitte.com,Advanced,5,Deloitte,7,Dev 401,sfdc.com,5';
            
   String blobCreator = csvcontent + '\r\n' + data ;   
   WCT_parseCSV parseCSVInstance = new WCT_parseCSV();  
   string namefile = 'test.csv';
   List<List<String>> fileLines = new List<List<String>>(); 
   filelines = parseCSVInstance.parseCSV(nameFile, true);
   string extension = namefile.substring(namefile.lastindexof('.')+1);
  
        Test.startTest();
        pagereference pge = page.WCT_RMPortal;
        Test.SetCurrentPage(pge); 
       list<WCT_H1BCAP__c> lst = new list<WCT_H1BCAP__c>();
       WCT_RMPortalControllerDesign contr = new WCT_RMPortalControllerDesign();
       contr.filterId = 'Not Willing To Travel';
       contr.contentFile = Blob.valueof(blobCreator);  
       contr.Dosumaction();
       contr.uploadProcess();
       contr.pgupdate();
       contr.getFailedRows();
       contr.export_upload();
       contr.SLLAction();
       contr.callPageLoad();
       contr.refreshRequests();
       contr.processRequests();
       contr.processSMRequests();
       contr.getSLLItems();
       contr.getSMItems();
       contr.getPracItems();
       contr.getItems();
       contr.processReviewRequests();
       contr.processSLLRequests();
       ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error');
       ApexPages.addMessage(msg);
        
        system.assertequals(contr.filterId , 'Not Willing To Travel');  
      
     
        test.stoptest(); 
       
       }
       
    }
    
    
  }