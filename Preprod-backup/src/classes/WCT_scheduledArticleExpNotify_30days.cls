global class WCT_scheduledArticleExpNotify_30days implements Schedulable{
   global void execute(SchedulableContext SC) {
      WCT_batchArticleExpNotify_30days artExp = new WCT_batchArticleExpNotify_30days(); 
      Database.executeBatch(artExp,200);
   }
}