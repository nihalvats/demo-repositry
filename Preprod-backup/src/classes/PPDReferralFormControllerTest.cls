@isTest(SeeAllData=false)
public class PPDReferralFormControllerTest{
   
    public static testMethod void m1(){
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m1@deloitte.com';
          insert con;
      
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m1@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m1@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
   
        PageReference pageRef = Page.PPDReferralForm;
        Test.setCurrentPage(pageRef);
        
        case cas = new case();
        cas.ContactId = con.Id;
        Cas.FCPA_PPD_First_Name__c = 'jeevan';
        Cas.FCPA_PPD_Last_Name__c = 'kumar';
        cas.FCPA_PPD_Email_Address__c = 'test@deloitte.com';
        cas.FCPA_Candidate_First_Name__c = 'test';
        cas.FCPA_Candidate_Last_Name__c = 'test1';
        insert cas;
        
        Test.startTest(); 
        PPDReferralFormController Controller = new PPDReferralFormController();
        Controller.radio1 = 'Yes';
        Controller.radio2 = 'Yes';
        Controller.radio3 = null;
        Controller.chkBx = True;
        Controller.display= False;
        Controller.saveRequest();
        Controller.displaytext();
        Controller.disa();
        Controller.supportAreaErrorMesssage = 'Error Message';
        Controller.input = 'test data';
        Test.stopTest();
    }
    }
    
    public static testMethod void m2(){
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m2@deloitte.com';
          insert con;
      
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m2@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m2@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
        
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = '';
          cas.FCPA_Candidate_Email_Address__c = 'test@deloitte.com';
          cas.FCPA_Type_of_Hire__c = 'test';
          cas.FCPA_Which_country_are_you_referring_to__c='test';
          cas.FCPA_Candidate_Last_Name__c = null;
          cas.FCPA_Which_FSS_are_you_recommending__c = 'test';
          cas.FCPA_Referring_PPD_function__c = 'Tax';
          insert cas;
          
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;
          rrc.saveRequest();
    }
    }
    
    public static testMethod void m3(){
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m3@deloitte.com';
          insert con;
      
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m3@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m3@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
        
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = 'can LastName';
          cas.FCPA_Candidate_Email_Address__c = '';
          cas.FCPA_Type_of_Hire__c = 'test';
          cas.FCPA_Which_country_are_you_referring_to__c='test';
          cas.FCPA_Candidate_Email_Address__c = null;
          cas.FCPA_Which_FSS_are_you_recommending__c = 'test';
          cas.FCPA_Referring_PPD_function__c = 'Tax';
          insert cas;
          
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;
          rrc.saveRequest();
    }
    }
    
    public static testMethod void m4(){
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m4@deloitte.com';
          insert con;
      
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m4@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m4@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
            
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = 'can LastName';
          cas.FCPA_Candidate_Email_Address__c = 'karthikvalakonda@deloitte.com';
          cas.FCPA_Type_of_Hire__c = '';
          cas.FCPA_Which_country_are_you_referring_to__c='test';
          cas.FCPA_Type_of_Hire__c = null;
          cas.FCPA_Which_FSS_are_you_recommending__c = 'test';
          cas.FCPA_Referring_PPD_function__c = 'Tax';
          insert cas;
          
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;          
          rrc.saveRequest();
    }
    }
    
    public static testMethod void m5(){
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m5@deloitte.com';
          insert con;
      
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m5@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m5@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
            
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = 'can LastName';
          cas.FCPA_Candidate_Email_Address__c = 'karthikvalakonda@deloitte.com';
          cas.FCPA_Type_of_Hire__c = 'TypeHire';
          cas.FCPA_Which_country_are_you_referring_to__c='';
          cas.FCPA_Which_country_are_you_referring_to__c=null;
          cas.FCPA_Which_FSS_are_you_recommending__c = 'test';
          cas.FCPA_Referring_PPD_function__c = 'Tax';
          insert cas;
          
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;
          rrc.saveRequest();
    }
    }
    
    public static testMethod void m6(){
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m6@deloitte.com';
          insert con;
      
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m6@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m6@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
          
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = 'can LastName';
          cas.FCPA_Candidate_Email_Address__c = 'karthikvalakonda@deloitte.com';
          cas.FCPA_Type_of_Hire__c = 'TypeHire';
          cas.FCPA_Which_country_are_you_referring_to__c='country Referring';
          cas.FCPA_Which_FSS_are_you_recommending__c = '';
          cas.FCPA_Which_FSS_are_you_recommending__c = null;
          cas.FCPA_Referring_PPD_function__c = 'Tax';
          insert cas;
          
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;
          rrc.saveRequest();
    }
    }
    
    public static testMethod void m7(){
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m7@deloitte.com';
          insert con;
      
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m7@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m7@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
            
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = 'can LastName';
          cas.FCPA_Candidate_Email_Address__c = 'karthikvalakonda@deloitte.com';
          cas.FCPA_Type_of_Hire__c = 'TypeHire';
          cas.FCPA_Which_country_are_you_referring_to__c='country Referring';
          cas.FCPA_Which_FSS_are_you_recommending__c = 'FSS';
          cas.FCPA_Referring_PPD_function__c = 'Tax';
          insert cas;
          
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;
          rrc.resume = False;
          rrc.EmailCorrespondence = False;
          rrc.chkBx = False;
          rrc.saveRequest();
    }
    }
    
    public static testMethod void m8(){
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m8@deloitte.com';
          insert con;
      
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m8@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m8@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
        
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = 'can LastName';
          cas.FCPA_Candidate_Email_Address__c = 'karthikvalakonda@deloitte.com';
          cas.FCPA_Type_of_Hire__c = 'TypeHire';
          cas.FCPA_Which_country_are_you_referring_to__c='country Referring';
          cas.FCPA_Which_FSS_are_you_recommending__c = 'FSS';
          cas.FCPA_Referring_PPD_function__c = 'Tax';
          insert cas;
          
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;
          rrc.resume = True;
          rrc.EmailCorrespondence = True;
          rrc.chkBx = False;
          rrc.saveRequest();
    }
    }
    
    public static testMethod void m9(){
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m9@deloitte.com';
          insert con;
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            System.debug('##   profiles'+profiles);
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m9@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m9@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = 'can LastName';
          cas.FCPA_Candidate_Email_Address__c = 'karthikvalakonda@deloitte.com';
          cas.FCPA_Type_of_Hire__c = 'TypeHire';
          cas.FCPA_Which_country_are_you_referring_to__c='country Referring';
          cas.FCPA_Which_FSS_are_you_recommending__c = 'FSS';
          cas.FCPA_Referring_PPD_function__c = 'Tax';
          insert cas;
          
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;
          //rrc.resume = False;
          //rrc.EmailCorrespondence = False;
          rrc.chkBx = True;
          rrc.input = '';
          rrc.input = null;
          rrc.saveRequest();
        }
    }
    
    public static testMethod void m10(){
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m10@deloitte.com';
          insert con;
      
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m10@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m10@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
        
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = 'can LastName';
          cas.FCPA_Candidate_Email_Address__c = 'karthikvalakonda@deloitte.com';
          cas.FCPA_Type_of_Hire__c = 'TypeHire';
          cas.FCPA_Which_country_are_you_referring_to__c='country Referring';
          cas.FCPA_Which_FSS_are_you_recommending__c = 'Other';
          cas.FCPA_Other__c = '';
          cas.FCPA_Other__c = null;
          cas.FCPA_Referring_PPD_function__c = 'Tax';
          insert cas;
          
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;
          rrc.saveRequest();
        }
    }
    
    
    
    public static testMethod void m11(){
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m11@deloitte.com';
          insert con;
      
      
          List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m11@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m11@deloitte.com');   
      insert u;
        
        System.runAs(u)
        { 
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = 'can LastName';
          cas.FCPA_Candidate_Email_Address__c = 'karthikvalakonda@deloitte.com';
          cas.FCPA_Type_of_Hire__c = 'TypeHire';
          cas.FCPA_Which_country_are_you_referring_to__c='country Referring';
          cas.FCPA_Which_FSS_are_you_recommending__c = 'Unknown';
          cas.FCPA_Unknown__c = '';
          cas.FCPA_Unknown__c = null;
          cas.FCPA_Referring_PPD_function__c = 'Tax';
          insert cas;
          
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;
          rrc.saveRequest();
        }
    }
    
    public static testMethod void m12(){
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m12@deloitte.com';
          insert con;
      
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m12@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m12@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = 'can LastName';
          cas.FCPA_Candidate_Email_Address__c = 'karthikvalakonda@deloitte.com';
          cas.FCPA_Type_of_Hire__c = 'TypeHire';
          cas.FCPA_Which_country_are_you_referring_to__c='country Referring';
          cas.FCPA_Which_FSS_are_you_recommending__c = 'india';
          cas.FCPA_Referring_PPD_function__c = 'Tax';
          insert cas;
          
          Test.startTest();
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;
          rrc.radio1 = 'Yes';
          rrc.saveRequest();
          Test.stopTest();
    }
    }
    
    public static testMethod void m13(){
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m13@deloitte.com';
          insert con;
      
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m13@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m13@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
        
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = 'can LastName';
          cas.FCPA_Candidate_Email_Address__c = 'karthikvalakonda@deloitte.com';
          cas.FCPA_Type_of_Hire__c = 'TypeHire';
          cas.FCPA_Which_country_are_you_referring_to__c='country Referring';
          cas.FCPA_Which_FSS_are_you_recommending__c = 'india';
          cas.FCPA_Referring_PPD_function__c = 'Tax';
          insert cas;
          
          Test.startTest();
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;
          rrc.radio3 = 'Yes';
          rrc.saveRequest();
          Test.stopTest();
    }
    }
    public static testMethod void m14(){
        
        
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m14@deloitte.com';
          insert con;
      
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m14@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m14@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
        
        /*  recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          insert con;
*/
           
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = 'can LastName';
          cas.FCPA_Candidate_Email_Address__c = 'karthikvalakonda@deloitte.com';
          cas.FCPA_Type_of_Hire__c = 'TypeHire';
          cas.FCPA_Which_country_are_you_referring_to__c='country Referring';
          cas.FCPA_Which_FSS_are_you_recommending__c = 'india';
          cas.FCPA_Referring_PPD_function__c = 'Tax';
          insert cas;
          
          Test.startTest();
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;
          rrc.radio2 = 'Yes';
          rrc.saveRequest();
          Test.stopTest();
        
    }
    }
    public static testMethod void m15(){
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m15@deloitte.com';
          insert con;
      
       List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            
            System.debug('##   profiles'+profiles);
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m15@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m15@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = 'can LastName';
          cas.FCPA_Candidate_Email_Address__c = 'karthikvalakonda@deloitte.com';
          cas.FCPA_Type_of_Hire__c = 'TypeHire';
          cas.FCPA_Which_country_are_you_referring_to__c='country Referring';
          cas.FCPA_Which_FSS_are_you_recommending__c = 'india';
          cas.FCPA_A1__c = '';
          cas.FCPA_Referring_PPD_function__c = 'Tax';
          insert cas;
          
          Test.startTest();
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;
          rrc.chkBx = True;
          rrc.input = 'country Referring';
          rrc.radio1 = '';
          rrc.radio2 = '';
          rrc.radio3 = '';
          rrc.saveRequest();
          Test.stopTest();
       }
    }
    
    public static testMethod void m16(){
        
          recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
          Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.Email='test.m16@deloitte.com';
          insert con;
      
          List<Profile> profiles = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
            System.debug('##   profiles'+profiles);
            
      String profile = System.label.Label_for_Employee_Profile_Name;   
      User u = WCT_UtilTestDataCreation.createUser('test.m16@deloitte.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CVMLast','CVMFirst','cvmcvm','test.m16@deloitte.com');   
      insert u;
        
        System.runAs(u)
        {
          case cas = new case();
          cas.ContactId = con.Id;
          cas.FCPA_PPD_First_Name__c= 'First';
          cas.FCPA_PPD_Last_Name__c = 'last';
          cas.FCPA_PPD_Email_Address__c = 'kvalakonda@deloitte.com';
          cas.FCPA_Candidate_First_Name__c = 'Con FirstName';
          cas.FCPA_Candidate_Last_Name__c = 'can LastName';
          cas.FCPA_Candidate_Email_Address__c = 'karthikvalakonda@deloitte.com';
          cas.FCPA_Type_of_Hire__c = 'TypeHire';
          cas.FCPA_Which_country_are_you_referring_to__c='country Referring';
          cas.FCPA_Which_FSS_are_you_recommending__c = 'Audit';
          cas.FCPA_Referring_PPD_function__c = 'Other';
          cas.FCPA_Other_PPD_function__c = null;
          insert cas;
          
          PPDReferralFormController rrc = new PPDReferralFormController();
          rrc.Ocase = cas;
          rrc.saveRequest();
        }
    }
  
  
}