@isTest
public class EmailFormerIntern_Test {
	public static testmethod void EmailFormerIntern()
	{
        Event__c evt=new Event__c();
        evt.Name='EventP0304215';
        evt.Start_Date_Time__c=system.Datetime.now();
        evt.End_Date_Time__c=system.Datetime.now()+1;
        Insert evt;
		PageReference pageRef = Page.EmailToFormerInterns;
		Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',string.valueof(evt.id));
        ApexPages.currentPage().getParameters().put('name',evt.name);
        ApexPages.currentPage().getParameters().put('Start_Date_Time__c',string.valueof(evt.Start_Date_Time__c));
        ApexPages.currentPage().getParameters().put('End_Date_Time__c',string.valueof(evt.End_Date_Time__c));
        EmailFormerInternController efcont=new EmailFormerInternController();
        efcont.emailarea='testp342015@invalid.com';
        efcont.getItemsmailbox();
        efcont.sendmail();
    }
    
}