@isTest
 public class WCT_H1bcap_SLL_Reminders_Test{
 Static testmethod void WCT_H1bcap_SLL_Reminders_TestMethod(){
   
   Date td = system.Today().adddays(-5); 
   Decimal i = 3;
   Decimal i4 = 4;
   Decimal i5 = 5;
     
    recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
    Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
    con.WCT_Employee_Group__c = 'Active';
    insert con;
            
  Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SLL']; 
  User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','svalluru@deloitte.com');
  insert platuser;
  
  list<WCT_H1BCAP__c> lst = new list<WCT_H1BCAP__c>();
  WCT_H1BCAP__c h1 = new WCT_H1BCAP__c();
  h1.WCT_H1BCAP_Practitioner_Name__c = con.id;
  h1.WCT_H1BCAP_Email_ID__c = 'svalluru@deloitte.com';
  h1.WCT_H1BCAP_Practitioner_Personal_Number__c = '12345';
  h1.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'test@deloitte.com';
  h1.WCT_H1BCAP_Status__c  = 'Ready for SLL approval';
  h1.WCT_H1BCAP_USI_SLL_Name__c= platuser.id;
  h1.WCT_H1BCAP_Capture_BeginingDate_SLL__c = td; 
  h1.recalculateFormulas();                     //Used to Recalculate the Formula Fields
  lst.add(h1);
  insert lst;  
    
   System.assertEquals(platuser.id, lst[0].WCT_H1BCAP_USI_SLL_Name__c) ;   
   System.assertEquals(1, lst.size()) ;
    string orgmail =  Label.WCT_H1BCAP_Mailbox;
    OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
    Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'WCT_Reminder_1_notification_to_SLL' AND IsActive = true];
   
   
      list<string>  currentRMEmail = new list<string>();
      currentRMEmail.add( lst[0].WCT_H1BCAP_Resource_Manager_Email_ID__c);
    
    List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
   
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
     
            mail.SetTemplateid(et.id);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(lst[0].WCT_H1BCAP_USI_SLL_Name__c);
            mail.setWhatid(lst[0].id);
            mail.setCcAddresses(currentRMEmail);
            mail.setOrgWideEmailAddressId(owe.id);   
            mailList.add(mail); 
      
  
    Test.StartTest(); 
    WCT_H1bcap_SLL_Reminders h1b = new WCT_H1bcap_SLL_Reminders();
    WCT_H1bcap_SLL_Schedule_Reminders createCon = new WCT_H1bcap_SLL_Schedule_Reminders();
    system.schedule('New','0 0 2 1 * ?',createCon); 
    database.Executebatch(h1b); 
    Test.StopTest();   
 
     }
     
   Static testmethod void WCT_H1bcap_SM_Reminders_TestMethod2(){  
    
   Date td = system.Today().adddays(-6);
 
       Profile p = [SELECT Id FROM Profile WHERE Name='20_H1BCAP_SLL']; 
       User platuser=WCT_UtilTestDataCreation.createUser( 'siva83',p.id,' svallurutest@deloitte.com.preprod','svalluru@deloitte.com');
       insert platuser;
 
    recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
    Contact con1=WCT_UtilTestDataCreation.createEmployee(rt.id);
    con1.WCT_Employee_Group__c = 'Active';
    insert con1;
   
   list<WCT_H1BCAP__c> ls = new list<WCT_H1BCAP__c>();
        WCT_H1BCAP__c h2 = new WCT_H1BCAP__c();
        h2.WCT_H1BCAP_Practitioner_Name__c = con1.id;
        h2.WCT_H1BCAP_Email_ID__c = 'svalluru@gmail.com';
        h2.WCT_H1BCAP_Resource_Manager_Email_ID__c = 'abc@yahoo.com';
        h2.WCT_H1BCAP_Status__c  = 'Ready for SLL approval';
        h2.WCT_H1BCAP_Capture_BeginingDate_SLL__c = td; 
        h2.WCT_H1BCAP_USI_SLL_Name__c = platuser.id; 
        h2.recalculateFormulas(); 
        
        ls.add(h2);
        insert ls;   
   string orgmail =  Label.WCT_H1BCAP_Mailbox;
   OrgWideEmailAddress owe1 =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
   Emailtemplate et1 = [select id, developername , IsActive from Emailtemplate where developername = 'WCT_Reminder_2_notification_to_SLL' AND IsActive = true];
   List<Messaging.SingleEmailMessage> mailList1 = new List<Messaging.SingleEmailMessage>();   
   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
  
   list<string>  currentRMEmail1 = new list<string>();
   currentRMEmail1.add( ls[0].WCT_H1BCAP_Resource_Manager_Email_ID__c);
            mail.SetTemplateid(et1.id);
            mail.setSaveAsActivity(false);
            mail.setTargetObjectId(ls[0].WCT_H1BCAP_USI_SLL_Name__c );
            mail.setCcAddresses(currentRMEmail1);
            mail.setWhatid(ls[0].id);
            mail.setOrgWideEmailAddressId(owe1.id);   
            mailList1.add(mail); 
             
    Test.StartTest(); 
    WCT_H1bcap_SLL_Reminders h1b = new WCT_H1bcap_SLL_Reminders();
    WCT_H1bcap_SLL_Schedule_Reminders createCon = new WCT_H1bcap_SLL_Schedule_Reminders();
    system.schedule('New','0 0 2 1 * ?',createCon); 
    database.Executebatch(h1b); 
    Test.StopTest();   
  }
     
     
     
 }