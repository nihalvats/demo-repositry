public class CER_Candidate_Expense_Helper {

    @InvocableMethod
    public static void createEventToFollowRejection(List<ID> ids)
    {
        system.debug('### the ids in invocable Method'+ids);
        if(ids.size()>0)
        {
            List<CER_Expense_Reimbursement__c> requests = [Select id, OwnerId, Name from CER_Expense_Reimbursement__c Where id in :ids];
            
            List<Event> events = new List<Event>();
            for(CER_Expense_Reimbursement__c request : requests)
            {
                Event tempEvent = new Event();
                tempEvent.IsReminderSet=true;
                tempEvent.ReminderDateTime= DateTime.now();
                tempEvent.StartDateTime= DateTime.now();
                tempEvent.EndDateTime= DateTime.now();
                tempEvent.Subject='Follow the Recruiter Coordinator regarding rejection of -'+request.Name;
                tempEvent.OwnerId=request.OwnerId;
                events.add(tempEvent);
                
            }
            insert events;
        }
        
    }
    
    
}