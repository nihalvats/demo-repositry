public with sharing class WCT_RM_DetailView {

    

    Public WCT_H1BCAP__c WCTH1BCAP {get;set;}
    public string getRecordId {get;set;}
    public boolean pageError {get; set;}
    public boolean rejectblock {get; set;}
    public String RMComments {get; set;} 
    public String pageErrorMessage {get; set;}
    public boolean submitbutton {get;set;}
    public boolean editbutton {get;set;}
    public boolean rejectbutton {get;set;}
    public boolean Cancelbutton {get;set;}
    
     public string RMDTpageErrorMessage {get;set;}
     public boolean RMDTpageError {get;set;}
     public boolean SRMDTpageError{get;set;}
     public string SRMDTpageErrorMessage {get;set;}
    
     
    public WCT_RM_DetailView() {
    editbutton = true;
    rejectbutton = true;
     submitbutton = false;
    getRecordId = ApexPages.currentPage().getParameters().get('id'); 
     if(getRecordId == '' || getRecordId == null) {
             
            pageErrorMessage = 'Not valid';
            pageError = true;
            //return null;
             
             }
             else {
             
                    
      editbutton = true;
      rejectbutton = true;
      Cancelbutton = false;
       submitbutton = false;
        RMDTpageError = false;
    WCTH1BCAP =  [SELECT WCT_H1BCAP_prac_name__c,OwnerId,WCT_Custom_Modified_Date__c,CreatedDate,Name,Id,WCT_H1BCAP_Case_status__c, WCT_H1BCAP_CE_Level__c, WCT_H1BCAP_Certification__c, 
        WCT_H1BCAP_Client_location_Building__c, WCT_H1BCAP_Client_Location_City_State__c, WCT_H1BCAP_Client_Location_Flat_Number__c, WCT_Sl_No__c ,WCT_H1BCAP_Practitioner_DOJ__c ,WCT_H1BCAP_Capability__c ,WCT_Realigned_Reason_from_SM__c ,WCT_Realigned_Reason_from_SLL__c ,WCT_H1BCAP_Notes_Given_GM_I__c ,
        WCT_H1BCAP_Client_Location_Zip_code__c, WCT_H1BCAP_Client_Name__c, WCT_H1BCAP_Comments__c, WCT_H1BCAP_Confirmation_US_Eng_Manager__c, 
        WCT_Rejection_Reason_from_RM__c,WCT_H1BCAP_CPA_Certification__c, WCT_H1BCAP_Delegate_for_USI_SM_GDM__c, WCT_H1BCAP_Delegate_USI_SM_GDM_Email__c, WCT_H1BCAP_Deloitte_Entity__c, WCT_H1BCAP_Deloitte_Tenure_Years__c, WCT_H1BCAP_Deloitte_Tenure_Year__c, WCT_H1BCAP_Deployment_Advisor_Email_id__c, 
        WCT_H1BCAP_Deployment_Advisor_Name__c, WCT_H1BCAP_Educational_Background__c, WCT_H1BCAP_Employment_Status__c, 
        WCT_H1BCAP_FSS_Leader_Email_Id__c, WCT_H1BCAP_FSS_Leader_Name__c, WCT_H1BCAP_FY15_AERS_US_level__c, WCT_H1BCAP_H1B_Status__c, 
        WCT_H1BCAP_Industry__c, WCT_H1BCAP_Email_ID__c, WCT_H1BCAP_Practitioner_Level__c,
         WCT_Rejected_Reason_from_SLL__c,WCT_H1BCAP_Practitioner_Personal_Number__c, WCT_H1BCAP_Skill_set__c, WCT_H1BCAP_Processing_Type__c, 
         WCT_H1BCAP_Project_Manager_Email_Id__c, WCT_H1BCAP_Project_Manager_Name__c, WCT_H1BCAP_Project_Name__c, 
         WCT_H1BCAP_Rationale_project_alignment__c, WCT_H1BCAP_Region__c, WCT_H1BCAP_Resource_Manager_Email_ID__c, WCT_H1BCAP_RM_Name__c, 
         WCT_H1BCAP_RM_comments__c, WCT_H1BCAP_Service_Area_Leader_email_Id__c, WCT_H1BCAP_Service_Leader_Name__c, 
         WCT_H1BCAP_Status__c, WCT_H1BCAP_Total_experience__c, WCT_H1BCAP_Deloitte_Reporting_Office_Loc__c, 
         WCT_H1BCAP_Willingness_Options__c,WCT_H1BCAP_Willingness_Status__c,WCT_H1BCAP_Rejected_Reason_from_SM__c ,WCT_H1BCAP_Practitioner_Comments__c,WCT_H1BCAP_Willing_to_Travel__c ,WCT_H1BCAP_Business_email__c, WCT_H1BCAP_Business_immigration_champ__c, WCT_H1BCAP_USI_Expat__c, WCT_H1BCAP_USI_Expat_email_id__c, 
         Super_RM_Comments__c,WCT_H1BCAP_USI_PPD_mail_Id__c, WC_H1BCAP_USI_SLL_email_id__c, WCT_H1BCAP_USI_SLL_Name__c, WCT_H1BCAP_GDM_Email_id__c, 
         LastModifiedById,LastModifiedDate,WCT_H1BCAP_Practitioner_Service_Area__c,WCT_H1BCAP_Practitioner_Service_Line__c,Multi_checkbox__c,WCT_H1BCAP_USI_SM_Name_GDM__c, WCT_H1BCAP_US_PPD_mail_Id__c, WCT_H1BCAP_US_PPD_Name__c, WCT_H1BCAP_Year_At_Level__c, WCT_H1BCAP_YE_Rating__c from WCT_H1BCAP__c where id = : getRecordId limit 1];
    rejectblock=false;
    }
    }
    
    /* 
    Added by Siv
    
     public PageReference dosuperaccess() {
        //update WCTH1BCAP;
        return null;
     
    }
    
     Ended here by Siv
   */ 
   public PageReference Cancel() {
      pageReference page = new pageReference('/apex/WCT_RM_Detail?id='+getRecordId );
      page.setRedirect(true);
      return page;
    }
   
    public PageReference Submitvalues() {
        editbutton = false;
        Cancelbutton = true;
        pageReference page;
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;  
             
       system.debug('profile  Name is:****'+profileName);
       if(null <> WCTH1BCAP.WCT_H1BCAP_RM_comments__c && '' <> WCTH1BCAP.WCT_H1BCAP_RM_comments__c && profileName == '20_H1BCAP_RM' )
      
        {
        system.debug('Entering the value******');
        system.debug('Value is1****'+WCTH1BCAP.WCT_H1BCAP_RM_comments__c);
         RMDTpageError = false;
         update WCTH1BCAP;
       page = new pageReference('/apex/WCT_RM_Detail?id='+getRecordId );
       page.setRedirect(true);
       }
       
            
       else if(null == WCTH1BCAP.WCT_H1BCAP_RM_comments__c && profileName == '20_H1BCAP_RM')
          { 
          
           RMDTpageErrorMessage = 'Please fill the RM Comments';
           RMDTpageError = true;
          
           }
        
         else if(null == WCTH1BCAP.Super_RM_Comments__c && profileName == '20_H1BCAP_SUPERRM')
          { 
          
           RMDTpageErrorMessage = 'Please fill the SUPER RM Comments';
           RMDTpageError = true;
          
           }
        
         else if(null <> WCTH1BCAP.Super_RM_Comments__c && '' <> WCTH1BCAP.Super_RM_Comments__c && profileName == '20_H1BCAP_SUPERRM' )
      
        {
        
           RMDTpageError = false;
           update WCTH1BCAP;
           page = new pageReference('/apex/WCT_RM_Detail?id='+getRecordId );
           page.setRedirect(true);
       }
        
        
       return page;
     
    }
    public PageReference RejectCall() {
        rejectblock = true;
        return null;
     
    }
       public PageReference cancelAction() {
        rejectblock = false;
       
         pageReference  page  = new pageReference('/apex/WCT_RM_Detail?id='+getRecordId );
         page.setRedirect(true);
        return page;
    }
    
         public PageReference Editvalues() {
         submitbutton = true;
         Cancelbutton = true;
         editbutton = false;
         rejectbutton = false;
         
        return null;
    }
    
        public PageReference rejectRecord() 
       {
         pageReference  page;
        submitbutton = false;
     
          if(getRecordId != '' && String.isNotBlank(RMComments) ) 
          {
       
              WCTH1BCAP.WCT_Rejection_Reason_from_RM__c = RMComments;
              WCTH1BCAP.WCT_H1BCAP_Status__c = 'Super RM Rejected';
              update WCTH1BCAP;
              //cancelAction();
             page  = new pageReference('/apex/WCT_RM_Detail?id='+getRecordId );
             page.setRedirect(true);
          }
       
           else if (String.isBlank(RMComments)) 
           {
           
               SRMDTpageErrorMessage = 'Please fill the Reject Reason SUPER RM Comments';
               SRMDTpageError = true;
               
           }
           
        return page;
    }  

}