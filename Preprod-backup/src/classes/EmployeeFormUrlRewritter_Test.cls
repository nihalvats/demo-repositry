@isTest
public class EmployeeFormUrlRewritter_Test {
    
    public static urlForRedirect__c customSettingObj;
    
    public static void createcustomSettingObj(){
        customSettingObj=new urlForRedirect__c();
        customSettingObj.fullVfPage__c='todnewcase';
        customSettingObj.Redirect_URL__c='/apex/testPage';
        customSettingObj.Name='vfpagename';
        customSettingObj.Pass_parameter__c=true;
        insert customSettingObj;
    }
    
     public static void createcustomSettingObj2(){
        customSettingObj=new urlForRedirect__c();
        customSettingObj.fullVfPage__c='todnewcase';
        customSettingObj.Redirect_URL__c='/apex/testPage?param=1';
        customSettingObj.Name='vfpagename';
        customSettingObj.Pass_parameter__c=true;
        insert customSettingObj;
    }
    
    //Test case to check the generateUrlFor method by passing null.
    public static testmethod void testGenerateUrlFor(){
        EmployeeFormUrlRewritter rewriter = new EmployeeFormUrlRewritter();
        System.assert(rewriter.generateUrlFor(null) == null);       
    }

    
    
    //Passing the Url(without Parameters) and checking in the maprequestUrl.
    public static testmethod void checkUrlFormapRequestUrlMethod(){
        
        createcustomSettingObj();
        EmployeeFormUrlRewritter rewriter = new EmployeeFormUrlRewritter();
        PageReference friendlyUrl = rewriter.mapRequestUrl(new PageReference('/tod/todnewcase?param=value'));
        
    }
    
    public static testmethod void checkUrlFormapRequestUrlMethod2(){
        
        createcustomSettingObj2();
        EmployeeFormUrlRewritter rewriter = new EmployeeFormUrlRewritter();
        PageReference friendlyUrl = rewriter.mapRequestUrl(new PageReference('/tod/todnewcase?param=value'));
        
    }
    

    
    //Passing the Url(with Parameters) and checking in the maprequestUrl.
    public static testmethod void checkUrlFormapRequestUrlMethod3(){
        
        createcustomSettingObj();
        EmployeeFormUrlRewritter rewriter = new EmployeeFormUrlRewritter();
        PageReference friendlyUrl = rewriter.mapRequestUrl(new PageReference('/tod/todnewcase'));
        
    }


}