@isTest (SeeAllData=false)
 global class WTPAA_sendReminderwgntoemployees_Test{
  Static testmethod void WTPAA_sendReminderwgntoemployees_TestMethod(){
 
    Date td = system.Today();
    recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
          con.WCT_Employee_Group__c = 'Active';
        insert con;
        
     //Used to insert create date with records thruJSON.Deserialize method   
  /*     String caseJSON = '{"attributes":{"type":"CasSe","url":"/services/data/v25.0/sobjects/WTPAA_Wage_Notice__c/a2Te0000000bTFu"},"Id":"a2Te0000000bTFu","WTPAA_Status__c":"Sent","WTPAA_Is_Notice_Sent__c":"true","CreatedDate":"2015-10-07T00:00:00","WTPAA_Reminder_for_2nd_week__c":"2015-10-13","WTPAA_Reminder_for_3rd_week__c":"2015-10-20","WTPAA_Reminder_for_4nd_week__c":"2015-10-27"}'; 
 
      
  
   WTPAA_Wage_Notice__c wgn = (WTPAA_Wage_Notice__c) JSON.deserialize(caseJSON, WTPAA_Wage_Notice__c.class ); 
    System.debug('Test case:' + wgn.createdDate); 
    System.debug('Test caseId:' + wgn.Id); 
    System.debug('Test caseStatus:' + wgn.WTPAA_Status__c); 
    System.debug('Reminder2:' + wgn.WTPAA_Reminder_for_2nd_week__c);  
   System.debug('Reminder3:' + wgn.WTPAA_Reminder_for_3rd_week__c); 
   System.debug('Reminder4:' + wgn.WTPAA_Reminder_for_4nd_week__c); 
  wgn.WTPAA_Related_To__r.id = con.id;
  
    insert wgn;
//Ended Json here   
    */
    list<WTPAA_Wage_Notice__c> lstwgn = new list<WTPAA_Wage_Notice__c >();
        WTPAA_Wage_Notice__c wgn = new WTPAA_Wage_Notice__c();
        wgn.WTPAA_Related_To__c = con.id;
        wgn.WTPAA_Status__c = 'sent';
        wgn.WTPAA_Is_Notice_Sent__c = true;
         lstwgn.add(wgn);
        insert lstwgn;

     
       Test.StartTest();
      system.debug('Batch class is started');
      
  WTPAA_sendReminderwagenoticestoemployees objBatch = new WTPAA_sendReminderwagenoticestoemployees ();
     WTPAA_Schedulewagenoticestoemployees  createCon = new WTPAA_Schedulewagenoticestoemployees ();
        system.schedule('New','0 0 2 1 * ?',createCon); 
 
      ID batchprocessid = Database.executeBatch(objBatch,1);
  Test.StopTest();
  }
 
}