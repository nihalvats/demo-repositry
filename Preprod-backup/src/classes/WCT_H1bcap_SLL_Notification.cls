global class  WCT_H1bcap_SLL_Notification Implements Database.Batchable <sObject> {
    global Database.queryLocator start(Database.BatchableContext bc) {
        
    Date d= system.today();
            
        String SOQL = 'SELECT Id,WCT_H1BCAP_Status__c,WCT_H1BCAP_US_PPD_mail_Id__c,WCT_H1BCAP_US_PPD_Name__c,WCT_H1BCAP_Resource_Manager_Email_ID__c ,WCT_H1BCAP_USI_SLL_Name__c,WCT_H1BCAP_After7thBusinessday_SLL__c  FROM WCT_H1BCAP__c WHERE WCT_H1BCAP_Status__c =  \'USI SM Approved\'  AND (WCT_H1BCAP_After7thBusinessday_SM__c =:d)' ;
            
        return Database.getQueryLocator(SOQL);
       
    }

    global void execute(Database.BatchableContext bc, List<WCT_H1BCAP__c> H1bcap) {
    
       
        string orgmail =  Label.WCT_H1BCAP_Mailbox;
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        List<Messaging.SingleEmailMessage> mailList1 = new List<Messaging.SingleEmailMessage>();
        OrgWideEmailAddress owe =[select id, Address from OrgWideEmailAddress where Address = :orgmail  limit 1];
       Emailtemplate et = [select id, developername , IsActive from Emailtemplate where developername = 'Notification_to_SLL_for_Nomination' AND IsActive = true];
    
      
        
         for(WCT_H1BCAP__c w: H1bcap) 
  {
       System.debug('Entering ind loop*******');   
          if(w.WCT_H1BCAP_Status__c  == 'USI SM Approved') 
     {    
          System.debug('Entering ind loop2*******'); 
            
          w.WCT_H1BCAP_Status__c = 'Ready for SLL approval';  
          w.WCT_H1BCAP_Capture_BeginingDate_SLL__c = system.today(); // capturing SLL action date
     }         
     
   }  
     
  // Here captures the SLL id's
     
   Set<id> SMids = new set<id> (); 
   Map<id,WCT_H1BCAP__c> MapSLLrecord = new Map<id,WCT_H1BCAP__c>();
   Map<id,WCT_H1BCAP__c> MapPPDrecord = new Map<id,WCT_H1BCAP__c>();
      System.debug('Start the execute*******'+H1bcap.size());   
         for(WCT_H1BCAP__c w: H1bcap) 
     {
       System.debug('Entering ind loop*******');   
         
        
       SMids.add(w.WCT_H1BCAP_USI_SM_Name_GDM__c);
        MapSLLrecord.put(w.WCT_H1BCAP_USI_SLL_Name__c, w); 
        MapPPDrecord.put(w.WCT_H1BCAP_US_PPD_Name__c, w);
    }
    
     list<WCT_H1BCAP__c> ls = new list<WCT_H1BCAP__c>();  
       for(id key: MapSLLrecord.keyset())
    {
          for(WCT_H1BCAP__c h1 :MapSLLrecord.values() )
          
        {
           
           System.debug('H1bcap value *******'+h1); 
           ls.add(h1);
             
        }
      
        Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
     //   ToCcAddress.add(w.WCT_H1BCAP_Resource_Manager_Email_ID__c);
     //   ToCcAddress.add(w.WCT_H1BCAP_US_PPD_mail_Id__c);
        singleMail.setTargetObjectId(key);
        singleMail.setSaveAsActivity(false);
        singleMail.setTemplateId(et.Id);
        singleMail.setWhatid(ls[0].id);
    //    singleMail.setCcAddresses(ToCcAddress);
        singleMail.setOrgWideEmailAddressId(owe.id);
    
      mailList.add(singleMail);
     
        ls.clear();
    //Clear the Cc address for not repeating again
   // ToCcAddress.clear();  
           
     }
     if(mailList.size() >0)
      {  
        try{
          update H1bcap;
          System.debug('Mail Has been sent*******');
          Messaging.sendEmail(mailList); 
          }   
          catch(Exception npe) {
          System.debug('The following exception has occurred: ' + npe.getMessage());
             }            
      }   
 
 // Sending Mails to PPD's
      
  list<WCT_H1BCAP__c> ls1 = new list<WCT_H1BCAP__c>();  
       for(id key1: MapPPDrecord.keyset())
    {
          for(WCT_H1BCAP__c h1b :MapPPDrecord.values() )
          
        {
           
           System.debug('H1bcap value *******'+h1b); 
                   ls1.add(h1b);
             
        }
      
        Messaging.SingleEmailMessage singleMail1 = new Messaging.SingleEmailMessage();
     //   ToCcAddress.add(w.WCT_H1BCAP_Resource_Manager_Email_ID__c);
     //   ToCcAddress.add(w.WCT_H1BCAP_US_PPD_mail_Id__c);
        singleMail1.setTargetObjectId(key1);
        singleMail1.setSaveAsActivity(false);
        singleMail1.setTemplateId(et.Id);
        singleMail1.setWhatid(ls1[0].id);
    //    singleMail.setCcAddresses(ToCcAddress);
        singleMail1.setOrgWideEmailAddressId(owe.id);
        mailList1.add(singleMail1);
     
        ls1.clear();
               
  }    
      
    if(mailList1.size() >0)
      {  
         try{
          System.debug('Mail Has been sent*******1');
          Messaging.sendEmail(mailList1);   
           }   
          catch(Exception npe) {
          System.debug('The following exception has occurred: ' + npe.getMessage());
             }    
      }     
      
}     
      
     global void finish(Database.BatchableContext bc) {
    }
  }