/******************************************************************************************
Name  :  TCDSearchRecord
Description:  Based  on entering the Cost Center Number it display's all the cost Center information. 

-----------------------------------------------------------------------------------------
Developer                Date                  Coverage           Version  
-----------------------------------------------------------------------------------------
Deloitte             03/24/2016                 100%              Original
*****************************************************************************************/

public with sharing class TCDSearchRecord1_controller {
    
    public  list<Talent_Delivery_Contact_Database__c> TDCD {get; set;}
    public  Talent_Delivery_Contact_Database__c TCDinstance {get; set;}
    public  list<Talent_Delivery_Contact_Database__c> maxTCD {
        get{
 transient   list<Talent_Delivery_Contact_Database__c> maxTCDTemp = new list<Talent_Delivery_Contact_Database__c>();
        integer i=1;
        for(Talent_Delivery_Contact_Database__c temp :TDCD )
        {
            i++;
            if(i<MAX_LIMIT)
            {
                maxTCDTemp.add(temp);
            }
        }
        return maxTCDTemp ;
        
    } set;} 
    
    public static Integer MAX_LIMIT=500;
    public boolean displayPopup {get; set;} 
    public boolean hidetable {get; set;}
    
    
    public string TCDid {get; set;}
    public string CostCenterNumber {get; set;}
    public string HRfunction {get; set;}
    public string HRServiceArea  {get; set;}
    // public string TCDNumber {get; set;}
    
    
    
    //   Costructor initially loads and calls reset method.
    public TCDSearchRecord1_controller ()
        
    {    
    TDCD = new list<Talent_Delivery_Contact_Database__c>();
     hidetable =  false;
       // reset();
    }
    
    
    /* This SearchRecord method will search from Talent Contact Database Object , based on the three fields(Cost center Number , HR function  and HR Service Area)
        it is going to return the result  using LIKE (wild card search) ..   */
    public void SearchRecord()
    {
      /*if(CostCenterNumber != null)
        {
            CostCenterNumber=CostCenterNumber;
        }  
       */
        String Query='select id, name, HR_Function__c, HR_Service_Area__c,HR_Service_Line__c , India_Talent_Contact_1__c,US_Talent_Contact_1__c, Payroll_Contact_Email__c, US_Talent_Contact_3__c,US_Talent_Contact_4__c ,India_Talent_Contact_2__c,US_Talent_Contact_2__c,  Workers_Comp_Contact_Email__c ,Cost_Center_Description__c, Resource_Manager__c  from Talent_Delivery_Contact_Database__c where';
        boolean isFirstCondAdded=false;
      
        
        if(CostCenterNumber!= '')
        {
            Query=Query+' name like  \'%'+CostCenterNumber+'%\'';
            system.debug('Query is like ---->'+Query);
            isFirstCondAdded=true;
        }
       
         if(HRfunction!= '')
        {
            if(isFirstCondAdded==true)
            {
           	 Query=Query+' and HR_Function__c like  \'%'+HRfunction+'%\'';
                 system.debug('Query is like ---->'+Query);
               isFirstCondAdded = true;
            }
            else
            {
                 Query=Query+'  HR_Function__c like  \'%'+HRfunction+'%\'';
                 system.debug('Query is like ---->'+Query);
                isFirstCondAdded=true;
            }
            
        }
        if(HRServiceArea != '')
        {
            if(isFirstCondAdded == true)
            {
                Query = Query+' and HR_Service_Area__c Like \'%'+HRServiceArea+'%\'';
                 system.debug('Query is like ---->'+Query);
            }
            else
            {
                Query = Query+'  HR_Service_Area__c Like  \'%'+HRServiceArea+'%\'';
                 system.debug('Query is like ---->'+Query);
            }
        }
       
        //TDCD = [select id, name, HR_Function__c, HR_Service_Area__c,HR_Service_Line__c , India_Talent_Contact_1__c,US_Talent_Contact_1__c, Payroll_Contact_Email__c, US_Talent_Contact_3__c,US_Talent_Contact_4__c , India_Talent_Contact_2__c,US_Talent_Contact_2__c,  Workers_Comp_Contact_Email__c ,Cost_Center_Description__c, Resource_Manager__c  from Talent_Delivery_Contact_Database__c  where name like :'%'+CostCenterNumber+'%'  AND HR_Function__c like :'%'+HRfunction+'%' AND  HR_Service_Area__c like : '%' +HRServiceArea+'%'   ];
         TDCD = database.query(Query);  
        system.debug('The TDCD is having the list of -->'+TDCD.size());
           if(TDCD.size()>0)
           {
               hidetable = true;
           }     
    }
    
    // This void method will refreshes the list  and  makes boolean  RenderTCDinfo as false.
    public void RefreshTCDlist()
        
    {
        
        TDCD = [select id, name, HR_Function__c, HR_Service_Area__c,HR_Service_Line__c , India_Talent_Contact_1__c,US_Talent_Contact_1__c, Payroll_Contact_Email__c, 
               India_Talent_Contact_2__c,US_Talent_Contact_2__c, US_Talent_Contact_3__c, US_Talent_Contact_4__c, Workers_Comp_Contact_Email__c ,Cost_Center_Description__c, Resource_Manager__c  from Talent_Delivery_Contact_Database__c  limit :MAX_LIMIT];
     
    }
    
    // This method returns only one record  based on the  record id  passed from the param tag  and makes RenderTCDinfo boolean variable as false.. ...
    
    public void SearchById()
        
    {
        TCDinstance = [select id, name, HR_Function__c, HR_Service_Area__c,HR_Service_Line__c , India_Talent_Contact_1__c,US_Talent_Contact_1__c, Payroll_Contact_Email__c, 
                       India_Talent_Contact_2__c,US_Talent_Contact_2__c,US_Talent_Contact_3__c, US_Talent_Contact_4__c,   Workers_Comp_Contact_Email__c ,Cost_Center_Description__c, Resource_Manager__c  from Talent_Delivery_Contact_Database__c  where id =:TCDid ];
        displayPopup = true;  
        
    }  
    
    // This Pagereference method will navigate to another vf page and from there it downloads the record which were in the TDCD list..
    
    
    
    public void reset()
    {
        TDCD = [select id, name, HR_Function__c, HR_Service_Area__c,HR_Service_Line__c , India_Talent_Contact_1__c,US_Talent_Contact_1__c, Payroll_Contact_Email__c, 
                India_Talent_Contact_2__c,US_Talent_Contact_2__c, US_Talent_Contact_3__c ,US_Talent_Contact_4__c,  Workers_Comp_Contact_Email__c ,Cost_Center_Description__c, Resource_Manager__c  from Talent_Delivery_Contact_Database__c  limit :MAX_LIMIT];
        CostCenterNumber ='';
        HRfunction ='';
        HRServiceArea  ='';
     
    }
    
    public void closePopup() 
    {        
        displayPopup = false;    
    } 
    
    public pagereference DownloadToExcel()
    {
        pagereference p = new pagereference('/apex/TCDExcelDownload');
        p.setredirect(false);
        return p;
    }
}