/********************************************************************************************************************************
Apex class         : <WCT_OnboardingInitiationForm>
Description        : <Controller which allows to Update Mobility, Assignment and Task>
Type               :  Controller
Test Class         : <WCT_Mobility_Onboard_FormController_Test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 25/05/2016          87%                          --                            License Cleanup Project
************************************************************************************************************************************/ 

public class WCT_Apply_Dependent_Visa_FormController extends SitesTodHeaderController{
    
    //TASK RELATED VARIABLES
    public task taskRecord{get;set;}
    public WCT_Task_Reference_Table__c taskRefRecord{get;set;}
    public String taskSubject {get;set;}
    public String taskid{get;set;}
    public boolean display{get;set;}
    public String taskVerbiage{get;set;}
    public String siteURL{get;set;}
    
    //INTIALISING CONSTRUCTOR
    public WCT_Apply_Dependent_Visa_FormController()
    {
        taskid = ApexPages.currentPage().getParameters().get('taskid');
        display=true;
        taskSubject = '';
        taskVerbiage = '';
        siteURL = '';
    }
    //UPDATE TASK FLAGS FOR DISPLAYING VERBIAGES
/********************************************************************************************
*Method Name         : <updateTaskFlags()>
*Return Type         : <Null>
*Param’s             : 
*Description         : <UpdateTaskFlags() Used for Fetching Task Details>

*Version          Description
* -----------------------------------------------------------------------------------------------------------                 
* 01              Original Version
*********************************************************************************************/      
    public pagereference updateTaskFlags()
    {
        if(taskid==''|| taskid==null){
            display=false;
            return null;
        }
        
        try {    
        taskRecord=[SELECT id, Subject,Ownerid, Status,WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c, WCT_Task_Reference_Table_ID__c, Site_URL__c FROM Task WHERE Id =: taskid];
        taskRefRecord = [SELECT Id, Name, WCT_Task_Subject__c, Form_Verbiage__c, ownerId FROM WCT_Task_Reference_Table__c Where id=:taskRecord.WCT_Task_Reference_Table_ID__c];

        if(taskRecord != null){
            taskSubject = taskRecord.Subject;
            taskVerbiage = taskRefRecord.Form_Verbiage__c;
            siteURL = Label.Partial_Site_URL+'WCT_Add_Update_DependentVisaInfo';

           return null;
        }else{
            return null;
        }
    }
        catch(Exception e){
         Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Apply_Dependent_Visa_FormController', 'Apply Dependent Form', e.getMessage()+'   in:'+e.getStackTraceString()+ '   due to:'+e.getCause());
            
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_ADVF_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
              
      }    
    }
}