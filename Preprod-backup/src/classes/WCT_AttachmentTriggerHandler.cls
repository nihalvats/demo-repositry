/**************************************************************************************
Apex Trigger Name:  WCT_AttachmentTriggerHandler
Version          : 1.0 
Created Date     : 19 December 2013
Function         : Handler class to 
                    ->get ParentId from Candidate Document using Name field to attachment 
                    -> To update Attachment link in Candidate Document after insert
                   
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   19/12/2013             Original Version
* Deloitte                   26/12/2013             Updated Version
*************************************************************************************/
public class WCT_AttachmentTriggerHandler{
    //Variable declaration
    public list<String> liAttachmentNames {get;set;}
    public Map<String,WCT_Candidate_Documents__c> mapCandidateDoc {get;set;}
   
    public static final string CandDoc_PREFIX = WCT_Candidate_Documents__c.sObjectType.getDescribe().getKeyPrefix();
    public static final string IntvJunction_PREFIX = WCT_Interview_Junction__c.sObjectType.getDescribe().getKeyPrefix();
    public static final String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
    public list<Attachment> liAttachmentsToUpdateCD = new list<Attachment>();
    public list<Id> liCandDocIds = new list<Id>();
    public Map<Id,WCT_Candidate_Documents__c> mapCandDocToUpdate = new Map<Id,WCT_Candidate_Documents__c>();
    public list<WCT_Candidate_Documents__c> liCandDocToUpdate = new list<WCT_Candidate_Documents__c>();
    public list<Id> liCandDocIdsToDeleteAttachments = new list<Id>();
    public list<Attachment> liAttachmentsToDelete = new list<Attachment>();
    //Variables for Attachment URL on Interview Tracker
    public list<Attachment> liAttachmentsToUpdateIT = new list<Attachment>();
    public list<Id> liIntvTrackerIds = new list<Id>();
    public Map<Id,WCT_Interview_Junction__c> mapIntvTrackerToUpdate = new Map<Id,WCT_Interview_Junction__c>();
    public list<WCT_Interview_Junction__c> liIntvTrackerToUpdate = new list<WCT_Interview_Junction__c>();
    
     /** 
        Method Name  : UpdateParentIdOnInsert
        Return Type  : Void
        Description  : Pull the Candidate Document Records to update Parent Id on Attachments .        
     */
    public void UpdateParentIdOnInsert(List<Attachment> AttachmentList){
            liAttachmentNames = new list<String>();
            system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@method1');
            mapCandidateDoc = new Map<String,WCT_Candidate_Documents__c>();
            for(Attachment att : AttachmentList){
                if(att.Name <> null && att.ParentId == null){
                    liAttachmentNames.add(att.Name);
                }
            }
            for(WCT_Candidate_Documents__c candDoc : [SELECT id,Name,WCT_Candidate__r.Name,WCT_File_Name__c,WCT_RMS_ID__c from WCT_Candidate_Documents__c 
                                WHERE WCT_File_Name__c IN :liAttachmentNames ]){
                mapCandidateDoc.put(candDoc.WCT_File_Name__c,candDoc);
            }
            
            if(!mapCandidateDoc.IsEmpty()){
            for(Attachment attachment : AttachmentList){
                if(mapCandidateDoc.containsKey(attachment.Name)){
                    String attName = '';
                    attName = attachment.Name;
                    //attachment.Name = (mapCandidateDoc.get(attachment.Name)).WCT_Candidate__r.Name+'_'+attName;
                    attachment.ParentId = (mapCandidateDoc.get(attachment.Name)).id;
                    liCandDocIdsToDeleteAttachments.add((mapCandidateDoc.get(attachment.Name)).id);
                    if((mapCandidateDoc.get(attachment.Name)).WCT_Candidate__r.Name != null && (mapCandidateDoc.get(attachment.Name)).WCT_Candidate__r.Name != ''){
                        attachment.Name = (mapCandidateDoc.get(attachment.Name)).WCT_Candidate__r.Name+'_'+attName;
                    }
                }
            }
            }
            if(!liCandDocIdsToDeleteAttachments.IsEmpty()){
                try{
                liAttachmentsToDelete = [Select id from Attachment where ParentId IN :liCandDocIdsToDeleteAttachments];
                }Catch(QueryException queryException){ 
                    WCT_ExceptionUtility.logException('Attachment trigger','Deleting Attachments',queryException.getMessage());
                }
            }
            if(!liAttachmentsToDelete.IsEmpty()){
                Database.Deleteresult[] deleteRec = DataBase.Delete(liAttachmentsToDelete,false);
            }
    
    }
    
    /** 
        Method Name  : UpdateAttachmentURL
        Return Type  : Void
        Description  : Update Attachment URL on Candidate Document Records .        
    **/
    
    public void UpdateAttachmentURL(List<Attachment> AttachmentList) {
     system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@method2');
        Set<WCT_Candidate_Documents__c> CandDocSet = new Set<WCT_Candidate_Documents__c>();
        Map<Id,WCT_Candidate_Documents__c> tempMap = new Map<Id,WCT_Candidate_Documents__c>();
        for(Attachment att : AttachmentList){
            if((String.valueOf(att.ParentId)).startsWith(CandDoc_PREFIX)){
                liAttachmentsToUpdateCD.add(att);
                liCandDocIds.add(att.ParentId);
            }
        }
        if(!liCandDocIds.IsEmpty()){
            for(WCT_Candidate_Documents__c cd : [SELECT id,WCT_Attachment_URL__c,WCT_Body_Length__c from WCT_Candidate_Documents__c where id IN :liCandDocIds]){
                mapCandDocToUpdate.put(cd.id,cd);
            }
        }
        if(!mapCandDocToUpdate.IsEmpty()){
            for(Attachment attachment : liAttachmentsToUpdateCD){
                WCT_Candidate_Documents__c cd = new WCT_Candidate_Documents__c();
                if(mapCandDocToUpdate.containsKey(attachment.ParentId)){
                    cd = mapCandDocToUpdate.get(attachment.ParentId);
                    cd.WCT_Attachment_URL__c = sfdcBaseURL+'/'+attachment.Id;
                    cd.WCT_Body_Length__c = attachment.BodyLength;
                    if(!tempMap.containsKey(cd.id)){
                        tempMap.put(cd.id,cd);
                        CandDocSet.add(cd);
                    }
                }
            }
        }
        //update liCandDocToUpdate;
    
        if(!CandDocSet.isEmpty()){
            liCandDocToUpdate.addAll(CandDocSet);
            DataBase.SaveResult[] srList = DataBase.Update(liCandDocToUpdate,false);
                for(Database.SaveResult sr: srList){
                    if(!sr.IsSuccess()){
                        for(Database.Error err : sr.getErrors()){
                            WCT_ExceptionUtility.logException('Attachment Trigger','Update URL on Candidate Doc',err.getMessage());
                        }
                    }
                }
        }

    }
    
    public void DeleteOldAttachmentHavingSameDesc(List<Attachment> attachmentList){
          system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@method3');
        Set<Id> setInterviewTrackerIds=new Set<Id>();
        Set<String> setInterviewTrackerDscriptions=new Set<String>();
        Set<Attachment> setnewInterviewTrackerAttachments=new Set<Attachment>();
        Map<String,Attachment> mapExistingInterviewTrackerAttachments= new Map<String,Attachment>();
        List<Attachment> lstExistingInterviewTrackerAttachmentsWithSameDesc= new list<Attachment>();
        
        for(Attachment att : attachmentList){
            if(att.ParentId.getSObjectType().getDescribe().getName().contains('WCT_Interview_Junction__c') && att.Description!=null ){                 
               setnewInterviewTrackerAttachments.add(att);
               setInterviewTrackerDscriptions.add(att.Description);
               setInterviewTrackerIds.add(att.ParentId);
            }                
        }
        

        if(!setInterviewTrackerIds.isempty()){
            for(Attachment existingAtt :[SELECT Id, Description,Parentid FROM Attachment WHERE Parentid in :setInterviewTrackerIds AND 
                                                  Description in :setInterviewTrackerDscriptions and id not in:setnewInterviewTrackerAttachments ]){
                
                mapExistingInterviewTrackerAttachments.put(existingAtt.Description,existingAtt);
                }
        }
        
        if(!mapExistingInterviewTrackerAttachments.isEmpty()){
            for(Attachment att : setnewInterviewTrackerAttachments){
                if(mapExistingInterviewTrackerAttachments.get(att.Description).parentId==att.ParentId )
                    lstExistingInterviewTrackerAttachmentsWithSameDesc.add(mapExistingInterviewTrackerAttachments.get(att.Description));
            }
        
        }
        
        if(!lstExistingInterviewTrackerAttachmentsWithSameDesc.IsEmpty()){
                Database.Deleteresult[] deleteRec = DataBase.Delete(lstExistingInterviewTrackerAttachmentsWithSameDesc,false);
            }
    }
    
    /** 
        Method Name  : UpdateAttachmentURLOnInterviewTracker
        Return Type  : Void
        Description  : Update Attachment URL on Interview Tracker Records .        
    **/
    
    public void UpdateAttachmentURLonIntvJun(List<Attachment> AttachmentList) {
     system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@method4');
        for(Attachment att : AttachmentList){
            if((String.valueOf(att.ParentId)).startsWith(IntvJunction_PREFIX)){
                liAttachmentsToUpdateIT.add(att);
                liIntvTrackerIds.add(att.ParentId);
            }
        }
        if(!liIntvTrackerIds.IsEmpty()){
            for(WCT_Interview_Junction__c intTr : [SELECT id,Name,WCT_Attachment_Created_Updated__c,    WCT_IEF_Submission_Status__c,
                                                    WCT_Attachment_URL__c from WCT_Interview_Junction__c where id IN :liIntvTrackerIds]){
                mapIntvTrackerToUpdate.put(intTr.id,intTr);
            }
        }
        if(!mapIntvTrackerToUpdate.IsEmpty()){
            for(Attachment attachment : liAttachmentsToUpdateIT){
                WCT_Interview_Junction__c tempIT = new WCT_Interview_Junction__c();
                if(mapIntvTrackerToUpdate.containsKey(attachment.ParentId)){
                    tempIT = mapIntvTrackerToUpdate.get(attachment.ParentId);
                    if(attachment.name.contains(tempIT.Name)){
                        if(tempIT.WCT_Attachment_URL__c == null){
                            tempIT.WCT_Attachment_URL__c = Label.WCT_SalesforceContentDownloadURL+attachment.Id;
                            if(tempIT.WCT_IEF_Submission_Status__c == 'Submitted'){
                                tempIT.WCT_Attachment_Created_Updated__c = true;
                            }
                        }else if(tempIT.WCT_IEF_Submission_Status__c == 'Submitted'){
                            tempIT.WCT_Attachment_Created_Updated__c = true;
                        }
                        liIntvTrackerToUpdate.add(tempIT);
                    }
                }
            }
        }
        //update liIntvTrackerToUpdate;    
        if(!liIntvTrackerToUpdate.isEmpty()){
            DataBase.SaveResult[] srList = DataBase.Update(liIntvTrackerToUpdate,false);
        }
    }
    
    /** 
        Method Name  : DeleteDuplicateAttachment
        Return Type  : Void
        Description  : Delete Duplicate Attachment if an Attachment with same name already exists        
    **/
    
    public void DeleteDuplicateAttachment(List<Attachment> attachmentList){
     system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@method5');
        List<Attachment> duplicateattachmentList = new List<Attachment>();
        List<String> attachmentnameList = new List<String>();
        List<Id> attachmentIdList = new List<Id>();
        for(Attachment attachment : attachmentList){
            if(attachment.ParentId<> null && (attachment.ParentId.getSobjectType() == WCT_Offer__c.SobjectType || attachment.ParentId.getSobjectType() == WTPAA_Wage_Notice__c.SobjectType)){
                 attachmentIdList.add(attachment.ParentId);
            }
        }
        
        If(attachmentIdList.size()>0){
            duplicateattachmentList = [Select Id, Name from Attachment WHERE ParentId In : attachmentIdList];
        }
        
        If(duplicateattachmentList.size()>0){
            DELETE duplicateattachmentList;
        }  
    }
    
     /** 
        Method Name  : DeleteOldAttachments
        Return Type  : Void
        Description  : Delete Old Attachments when adding new attachments to the passport record        
    **/
    
    public void DeleteOldAttachments(List<Attachment> attachmentList){
     system.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@method6');
        List<Attachment> oldAttachmentList = new List<Attachment>();
        List<Id> attachmentIdList = new List<Id>();
        
        for(Attachment attachment : attachmentList){
            if(attachment.ParentId<> null && attachment.ParentId.getSobjectType() == WCT_Passport__c.SobjectType){
                attachmentIdList.add(attachment.ParentId);
            }
        }
        
        if(attachmentIdList.size()>0){
            oldAttachmentList = [Select Id from Attachment WHERE ParentId In : attachmentIdList];
            if(oldAttachmentList.size()>0){
                DELETE oldAttachmentList;
            }
        } 
    }  
    
     
    
    
}