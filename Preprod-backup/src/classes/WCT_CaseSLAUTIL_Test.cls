@istest

public class WCT_CaseSLAUTIL_Test {
    
    static testmethod void test_trigger(){
            String voluntaryRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(WCT_UtilConstants.CASE_EMPLOYEE_VOL_RC).getRecordTypeId();

        recordtype rt=[select id from recordtype where DeveloperName = 'WCT_Employee'];
        WCT_BusinessHours__c wctb = new WCT_BusinessHours__c();
        wctb.India_Business_Hours_Id__c='01m40000000Ph0j';
        insert wctb;
        Contact con=WCT_UtilTestDataCreation.createEmployee(rt.id);
        con.Email='testoast@deloitte.com';
        con.WCT_Person_Id__c='104032';
        con.WCT_Cost_Center__c='42324';
con.WCT_Region__c= 'INDIA/REGION 10';  
        insert con;
        WCT_SLA_Values__c wctsla = new WCT_SLA_Values__c();
        wctsla.WCT_Business_Hours_Days__c = 24;
        wctsla.WCT_Category__c = 'ELE';
        insert wctsla;
        case c1 = new case();
        c1.WCT_SLA_Breach_Date__c= system.today() ;
        c1.WCT_Related_SLA__c=wctsla.id;
      
        c1.ContactId =  con.id ;
        c1.WCT_Category__c = 'Talent Relations'; 
        c1.subject = 'Test Case';
        c1.description= 'Test Descriptiopn'; 
        c1.Origin='Chat';
      // c1.wct_category__c='Technology Issue - Talent';
        c1.WCT_Requisition_FSS__c ='test';
        c1.Priority='1-High';
        c1.WCT_Requisition_Service_Area__c='testdata';
        c1.RecordTypeId=voluntaryRecordTypeId;
        c1.WCT_Send_Email_On__c=null;
        insert c1;
        Case c = new  Case();
        c.WCT_SLA_Breach_Date__c= system.today() ;
        c.WCT_Related_SLA__c=wctsla.id;
        c.WCT_RelatedCase__c=c1.id;
        c.ContactId =  con.id ;
        c.WCT_Category__c = 'Benefits'; 
        c.subject = 'Test Case';
        c.description= 'Test Descriptiopn'; 
        c.Origin='Chat';
       c.wct_category__c='Technology Issue - Talent';
        c.WCT_Requisition_FSS__c ='test';
        c.Priority='1-High';
        c.WCT_Requisition_Service_Area__c='testdata';
        
        insert c;
        
        list <case> c2 =[select id,Priority,WCT_IsSLAInitiated__c,WCT_SubCategory1__c,ContactId,RecordTypeId,IsClosed,WCT_Related_Business_Hours_Id__c,WCT_Requisition_Service_Area__c,Status,WCT_User_Tier__c,OwnerId,WCT_SLA_Breach_Date__c,WCT_Related_SLA__c,subject,description,wct_category__c,WCT_Requisition_FSS__c from case where id =: c.id];
          
        WCT_CaseSLAUTIL.createCaseStateRecords(c2);
        
        map<id,case> test1 = new map<id,case>();
        map<id,case> test2 = new map<id,case>();
    test1.put(c.id, c);
        test2.put(c.id, c);
        WCT_CaseSLAUTIL.createAndUpdateCaseStateRecords(test1, test2, c2);
    WCT_CaseSLAUTIL.assignSLAoNCase(c2, test2);
        
    }
    static testmethod void test_trigger1(){
        WCT_CaseSLAUTIL.createAndUpdateCaseStateRecords(null, null, null);
    WCT_CaseSLAUTIL.assignSLAoNCase(null, null);
        WCT_CaseSLAUTIL.createCaseStateRecords(null) ;
         WCT_CaseSLAUTIL.assignSLAAndCalculateBeachTime(null, null);
        
        
    }
    
}