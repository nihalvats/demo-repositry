/**************************************************************************************
Apex class       : EmpDashBoardController
Version          : 1.0 
Created Date     : 25 April 2015
Function         : Controller to display data in EmployeedashboardPage
Test Class       : ELE_Separation_handler_test
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Deloitte                   25/04/2015            Original Version
*************************************************************************************/

public with sharing class EmpDashBoardController extends SitesTodHeaderController {
    
    
    //Public Class Variables    
    public String types{get;set;}
    public List<Clearancewrapperclass> wrapList {get;set;}
    public boolean displayList{get;set;}
    public string emailId='';
    public set<string> setStr = new set<string>();
    public Contact contactObj = new Contact();
    public List<Clearance_separation__c> listAppForUpdt;
    public Transient List<Attachment> attchList {get;set;}   
    List<Contact> conList = new List<Contact>();
    public   ELE_Separation__c Oelerec {get;set;}   
    public string empcmmts{get;set;}
    public boolean recordExist{get;set;}
    
    public EmpDashBoardController()
    {
        types='';  
        listAppForUpdt  = new List<Clearance_separation__c>();
        Oelerec = new ELE_Separation__c ();
        displayList = false;
        attchList = new List<Attachment>();
        emailId=loggedInContact.Email;
        conList = [select id,name,email,WCT_Personnel_Number__c,ELE_Open_ELE_case__c from contact where email = : emailId and RecordType.Name = 'Employee'];
        contactObj= conList[0];
        recordExist= true;
        List<ELE_Separation__c> seprecList=[Select id,ELE_Case_status__c,ELE_Last_Date__c,Name,ELE_IPSF_Proof_Submission_Consent__c,ELE_Comments_emp__c,Employee_Comments__c,Employee_Comment_Unread__c,ELE_Contact_Email__c From ELE_Separation__c
                                            Where ELE_Contact_Email__c=:emailId order by createddate desc Limit 1];
        
        
        if(seprecList.size()>0)
        {
                       
            Oelerec = [Select id,ELE_Case_status__c,ELE_Last_Date__c,Name,ELE_IPSF_Proof_Submission_Consent__c,ELE_Comments_emp__c,Employee_Comments__c,Employee_Comment_Unread__c,ELE_Contact_Email__c From ELE_Separation__c
                       Where ELE_Contact_Email__c=:emailId order by createddate desc Limit 1];
            
            empcmmts=Oelerec.ELE_Comments_emp__c;
            if(Oelerec.ELE_IPSF_Proof_Submission_Consent__c == null || Oelerec.ELE_IPSF_Proof_Submission_Consent__c == ''){
                Oelerec.ELE_IPSF_Proof_Submission_Consent__c = 'No';
            }
            if(Oelerec.Employee_Comments__c !='')
            {
                Oelerec.Employee_Comment_Unread__c=true;    
            } 
            
        }
        else{
            
            recordExist=false;             
        }
        
    }
    
/*************************************************************
Method Name  : getWrapperList
Return Type  : List<wrapperclass>
Description  : getter method for wrapperlist   
*************************************************************/
    public List<Clearancewrapperclass> getWrapperList()
    {
       wrapList = New List<Clearancewrapperclass>();
        List<clearance_separation__c> appObjs=[Select name,id,ELE_Stakeholder_Designation__c,ELE_Last_Working_Day__c,ELE_Deloitte_Email__c,
                                            ELE_Status__c,ELE_Contact_Email_Separation__c,ELE_Separation__c,ELE_Separation__r.ELE_IPSF_Proof_Submission_Consent__c,ELE_Separation__r.id,
                                            Ele_Contact_Email__c,ELE_Comments_For_Employee__c,ELE_Employee_Comments__c,ELE_Employee_comment_updated__c
                                            from clearance_separation__c where ELE_Deloitte_Email__c =:emailId and (ELE_Stakeholder_Designation__c = 'Reporting Manager' OR ELE_Stakeholder_Designation__c = 'CRM')  and ELE_Status__c !='Revoke Resignation Approved'];
       
        for(clearance_separation__c appObj : appObjs ){
            
            if(contactObj.Email == appObj.ELE_Deloitte_Email__c)
            {
                wrapList.add(New Clearancewrapperclass(appObj,contactObj));    
            } 
            
        } 
        return wrapList;
    }
    
/********************************************* 
Method Name  : UpdateDetails
Return Type  : void
Description  : Invokes when the update button is clicked on EmployeeDashboard
*******************************************************************************/
    public pagereference UpdateDetails()
    {
        try{
            List<Clearancewrapperclass> listTempWrapper = new List<Clearancewrapperclass>();
            attchList = new List<Attachment>();
            for(Clearancewrapperclass w : wrapList) 
            {
                if(w.appRec != null) 
                { 
                    
                    w.appRec.ELE_Employee_comment_updated__c = w.appRec.ELE_Employee_Comments__c;
                    listAppForUpdt.add(w.appRec);
                    if(w.att != null && w.att.name != null && w.att.body != null)
                    {
                        w.att.parentid = w.appRec.id;
                        attchList.add(w.att);
                    }
                    
                } 
                else 
                {
                    listTempWrapper.add(w);
                }
            }
            if(listAppForUpdt != null && listAppForUpdt.size() > 0) 
            {
                update listAppForUpdt;
                wrapList = listTempWrapper;
                listAppForUpdt  = new List<Clearance_separation__c>();
            } 
            
            
            if(attchList != null && attchList.size() > 0)
                insert attchList;
            
            update Oelerec;
            return null;
        }
        catch (Exception e) {
            
            Exception_Log__c log=WCT_ExceptionUtility.logException('EmpDashBoardController', 'EmployeeDashboard', e.getMessage());
            
            pagereference pg = new pagereference('/apex/GBL_Page_Notification?key=ErrorMsg&expCode='+log.Name);
            pg.setRedirect(true);
            return pg;
        }
    }
    
    /************************************************************************************
Method Name  : revokeresign
Return Type  : void
Description  : Invokes when the Revoke Resignation button is clicked on EmployeeDashboard
****************************************************************************************/ 
    public void revokeresign()
    {
        try{
            Oelerec.ELE_Case_status__c='Revoke Resignation Requested';
            update Oelerec;   
        }
        catch (Exception e) {
            WCT_ExceptionUtility.logException('EmpDashBoardController', 'EmployeeDashboard', e.getMessage());
        }
    }
    
    /********************************************************************************************
Class Name   : Clearance separation wrapperclass
Description  : Wrapper class to get the clearance separation and contact along with attachment
***************************************************************************************************/
    public class Clearancewrapperclass
    {
        public Clearance_separation__c appRec{get;set;}
        public contact con{get;set;}
        public Attachment att{get;set;}
        public Clearancewrapperclass(Clearance_separation__c app,Contact con)
        {
            this.att = new Attachment() ;
            this.con = con;
            appRec = app;
            
        }
    }
    
 /********************************************************************************************
Method Name  : redirect
Description  : Method executeson load 
Purpose		 : To handle empty query in constructor
***************************************************************************************************/  
    public pagereference redirect()
    {
        
        if(recordExist==false){
            
            pagereference pg = new pagereference('/apex/GBL_Page_Notification?key=AuthError');
            pg.setRedirect(true);
            return pg;
        }
        else{return null;}
        
    }
    
}