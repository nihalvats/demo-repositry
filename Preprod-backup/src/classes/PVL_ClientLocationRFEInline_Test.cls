@isTest
public  class PVL_ClientLocationRFEInline_Test 
{
    
    public static testmethod void test1()
    {
        
        Project_H1B__c project = new Project_H1B__c();
        
        insert project;
        
        PVL_Database__c database = new PVL_Database__c();
        database.Project__c=project.id;
        insert database;
        
        WCT_Immigration__c immigration = new WCT_Immigration__c();
        immigration.RFE_PVL_lk__c=database.Id;
        insert immigration;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(immigration);
        PVL_ClientLocationRFEInline testAccPlan = new PVL_ClientLocationRFEInline(sc);
        
        PageReference pageRef = Page.PVL_ClientLocationRFEInline;
        pageRef.getParameters().put('id', String.valueOf(immigration.Id));
        Test.setCurrentPage(pageRef);
    }

}