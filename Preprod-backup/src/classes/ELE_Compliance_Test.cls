/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class ELE_Compliance_Test
{
    
    
    public static testMethod void testAll()
    {
       Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCase','Employee','test@case.com');
       insert con;
       String profile = System.label.Label_for_Employee_Profile_Name;   
       User u = WCT_UtilTestDataCreation.createUser('test@case.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CaseLast','CaseFirst','casecase','case@deloitte.com');   
       insert u;
       System.runAs(u)
        {
            Test.setCurrentPageReference(new PageReference('todNewCase')); 
            System.currentPageReference().getParameters().put('sa', 'IN');
            TodNewCaseUSController tod=new TodNewCaseUSController();
            tod.caseRecord.WCT_Support_Area__c='INDIA';
            tod.caseRecord.WCT_ContactChannel__c='Phone';
            tod.caseRecord.ToD_Case_Category__c='Outside Employment and Activities (APR218) – Active Employment (excludes Partners and Principals)';
            tod.caseRecord.subject='Test Sub';
            tod.caseRecord.description='Test description';
            tod.saveCase();
            tod.getContactPhone();
            tod.getContactPersonalId();
            tod.getContactOffice();
            tod.getContactEntity();
            tod.getContactJobLevel();
            tod.getContactFunction();
            tod.getContactCostCenter();
            tod.getUSIBLItems();
        
       }
    }
    
    public static testMethod void TestALL2()
    {
        
         Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCase','Employee','test@case.com');
         insert con;
         String profile = System.label.Label_for_Employee_Profile_Name;   
         User u = WCT_UtilTestDataCreation.createUser('test@case.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CaseLast','CaseFirst','casecase','case@deloitte.com');   
         insert u;
        System.runAs(u)
      {
            String strEncryptEmail = EncodingUtil.urlDecode(CryptoHelper.encrypt(con.email), 'UTF-8');
            Test.setCurrentPageReference(new PageReference('todNewCase')); 
            System.currentPageReference().getParameters().put('em', strEncryptEmail);
           System.currentPageReference().getParameters().put('sa', 'US');
           TodNewCaseUSController tod=new TodNewCaseUSController();
           tod.caseRecord.WCT_Support_Area__c='US';
            tod.caseRecord.WCT_ContactChannel__c='Phone';
            tod.caseRecord.ToD_Case_Category__c='Military Time Compliance - General Inquires';
            tod.caseRecord.subject='Test Sub';
            tod.caseRecord.description='Test description';
            tod.saveCase();
            // TodNewCaseUSController.AttachmentsWrapper tempWrapper= new TodNewCaseUSController.AttachmentsWrapper(true, 'Test', 'sample', '34');
       } 
    }
    
       
    
     public static testMethod void TestALL3()
       {
        
        Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCase','Employee','test@case.com');
       insert con;
       String profile = System.label.Label_for_Employee_Profile_Name;   
       User u = WCT_UtilTestDataCreation.createUser('test@case.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CaseLast','CaseFirst','casecase','case@deloitte.com');   
       insert u;
       System.runAs(u)
        {
         Test.setCurrentPageReference(new PageReference('todNewCase')); 
         System.currentPageReference().getParameters().put('sa', 'IN');
        TodNewCaseUSController tod=new TodNewCaseUSController();
        tod.caseRecord.WCT_Support_Area__c='INDIA';
        tod.caseRecord.WCT_ContactChannel__c='Phone';
        tod.caseRecord.ToD_Case_Category__c='Submit an Alert or To Know Request';
        tod.caseRecord.WCT_Category__c =  system.label.Knowledge_Management_Case_Category;
        tod.caseRecord.subject='Test Sub';
        tod.caseRecord.description='Test description';
        tod.caseRecord.Ron_URL__c='www.salesforce.com';
        tod.caseRecord.Comments__c='Test Comments';
        tod.saveCase();
        tod.renderoutempfields();
        //TodNewCaseUSController.AttachmentsWrapper tempWrapper= new TodNewCaseUSController.AttachmentsWrapper(true, 'Test', 'sample', '34');
       } 
        }
    
    
     public static testMethod void TestALL4()
       {
        
        Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCase','Employee','test@case.com');
       insert con;
       String profile = System.label.Label_for_Employee_Profile_Name;   
       User u = WCT_UtilTestDataCreation.createUser('test@case.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CaseLast','CaseFirst','casecase','case@deloitte.com');   
       insert u;
       System.runAs(u)
        {
         
        
         Test.setCurrentPageReference(new PageReference('todNewCase')); 
         
        System.currentPageReference().getParameters().put('sa', 'IN');
        TodNewCaseUSController tod=new TodNewCaseUSController();   
        tod.caseRecord.WCT_Support_Area__c='INDIA';
        tod.caseRecord.WCT_ContactChannel__c='Phone';
        tod.caseRecord.ToD_Case_Category__c='Nepotism - Active Personnel';
        tod.caseRecord.subject='Test Sub';
        tod.caseRecord.description='Test description';
        tod.saveCase();
        
        
            if(tod.caseExt.id!=null)
            {
                GENERATE_PDF_OF_RECORD.generateCFEPDF(tod.caseExt.id);
            }
        //tod.doc.body=Blob.valueOf('Test Sample ');
        //tod.doc.name='Test Name';
       // tod.uploadAttachment();
        tod.getParameterInfo();
        //newTodRequest.privatemethods();
        tod.getItems();
        tod.getItems2A();
        
        tod.getCategoryList();
       // tod.uploadRelatedAttachment(); 
      }
    }
    
    
      public static testMethod void PDFTEST()
       {
       List<Case> cases = new List<Case>{};
       List<Case_form_Extn__c> caseformextensions = new List<Case_form_Extn__c>{};
       
       Case_form_Extn__c CFEInsert = new Case_form_Extn__c(GEN_Case__c = '5004000000u7VLD');
       caseformextensions.add(CFEInsert);
       insert caseformextensions;
       
       
       
       ApexPages.StandardController sc = new ApexPages.StandardController(CFEInsert);
       todgeneratepdf_cfe CFERecord = new todgeneratepdf_cfe(sc);

        
       PageReference pageRef = Page.TOD_RECORD_TO_PDF;
       pageRef.getParameters().put('id', String.valueOf(CFEInsert.Id));
       Test.setCurrentPage(pageRef);        
        
       }
      public static testMethod void PDFTEST1()
       {
           Contact con = WCT_UtilTestDataCreation.createContact('Deloitte US Offices','TestCase','Employee','test@case.com');
       insert con;
       String profile = System.label.Label_for_Employee_Profile_Name;   
       User u = WCT_UtilTestDataCreation.createUser('test@case.com','System Administrator','CTS Out of Scope - Vendor Relationship Management','CaseLast','CaseFirst','casecase','case@deloitte.com');   
       insert u;
       //Id listNamesRt=Schema.SObjectType.WCT_List_Of_Names__c.getRecordTypeInfosByName().get('ToD Case Categories').getRecordTypeId();
       
       //WCT_List_Of_Names__c listNames=WCT_UtilTestDataCreation.createListOfNames('testList',listNamesRt,'ToD');
      
       //WCT_List_Of_Names__c listNames1= [select id,ToD_Case_Category_Instructions__c from WCT_List_Of_Names__c where Id=:listNames.Id];
       // listNames1.ToD_Case_Category_Instructions__c='test';
      // Update listNames;
       System.runAs(u)
        {
         
        
         Test.setCurrentPageReference(new PageReference('todNewCase')); 
         
        System.currentPageReference().getParameters().put('sa', 'IN');
        TodNewCaseUSController tod=new TodNewCaseUSController(); 
        
        tod.ElecomQ1other='ELEQ1';
        tod.ElecomQ2B='ELEQ2';
        tod.pageErrorMessage='Common Error Msg';
        tod.AttachmentErrorMessage='Attache error msg';
        tod.supportAreaErrorMesssage='SA error msg';
          
        tod.caseRecord.WCT_Support_Area__c='INDIA';
        tod.caseRecord.WCT_ContactChannel__c='Phone';
        
        tod.caseRecord.subject='Test Sub';
        tod.caseRecord.description='Test description';
        tod.caseRecord.ToD_Case_Category__c=system.label.ELEC_OE_ACTEMP_LABEL;
        tod.saveCase();
        tod.caseRecord.ToD_Case_Category__c=system.label.ELEC_MTC_GEN_INQ_LABEL;
        
        tod.saveCase();
        }  
        
       }
       
}