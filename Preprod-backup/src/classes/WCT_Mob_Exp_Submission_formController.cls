/********************************************************************************************************************************
Apex class         : <WCT_Mob_Exp_Submission_formController>
Description        : <Controller which allows to Update Task and Mobility>
Type               :  Controller
Test Class         : <WCT_Mob_Exp_Submission_formController_test>

*Version         Developer                   Date          Code Coverage              Case/Req #                           Description     
* ------------------------------------------------------------------------------------------------------------------------------------------------            
* 01             Deloitte                 24/05/2016          78%                          --                            License Cleanup Project
************************************************************************************************************************************/

public class WCT_Mob_Exp_Submission_formController{
    public task taskRecord{get;set;}
    public WCT_Task_Reference_Table__c taskRefRecord{get;set;}
    public String taskSubject {get;set;}
    public String taskid{get;set;}
    public boolean display{get;set;}
    public String taskVerbiage{get;set;}
    
    // DEFINING A CONSTRUCTOR 
    public WCT_Mob_Exp_Submission_formController()
    {
        taskid = ApexPages.currentPage().getParameters().get('taskid');
        display=true;
        taskSubject = '';
        taskVerbiage = '';
    }

    /********************************************************************************************
    *Method Name         : <updateTaskFlags()>
    *Return Type         : <Null>
    *Param’s             : 
    *Description         : <UpdateTaskFlags() Used to Update Mobility and Task>
    
    *Version          Description
    * -----------------------------------------------------------------------------------------------------------                 
    * 01              Original Version
    *********************************************************************************************/    
    
    public PageReference updateTaskFlags()
    {
       try {
        if(taskid==''|| taskid==null){
            display=false;
           return null;
        }
     
        taskRecord=[SELECT id, Subject,Ownerid, Status,WCT_Is_Visible_in_TOD__c, WCT_Auto_Close__c,WCT_Task_Reference_Table_ID__c FROM Task WHERE Id =: taskid];
        taskRefRecord = [SELECT Id, Name, WCT_Task_Subject__c, Form_Verbiage__c, ownerId FROM WCT_Task_Reference_Table__c Where id=:taskRecord.WCT_Task_Reference_Table_ID__c];
        if(taskRecord != null){
            //Get Task Subject
            taskSubject = taskRecord.Subject;
            taskVerbiage = taskRefRecord.Form_Verbiage__c;
            //Set Task Status
            if(taskRecord.WCT_Auto_Close__c == true){
                taskRecord.status='Completed';
            }
            
            if(taskRecord.WCT_Auto_Close__c == false){
            taskRecord.status='Not Started';
            }
            update taskRecord;
            }else{
            return null;
           }
        }
        
         catch (Exception e) {
           
            Exception_Log__c errLog=WCT_ExceptionUtility.logException('WCT_Task_Completion_formController', 'Task Completion Form', e.getMessage());
            
            Pagereference pg = new Pagereference('/apex/GBL_Page_Notification?key=GMI_TSKCMP_EXP&expCode='+errLog.Name);
            pg.setRedirect(true);
            return pg;
            
        }
        return null;
    }
}