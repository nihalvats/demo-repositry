/**  
    * Class Name  : WCT_SaveTheDateController 
    * Description : This apex class is used to send Save the date invite 
*/
public class WCT_SaveTheDateController{

    //Variable Declaration
    public string EmailBodyFromTemp {get;set;}
    public string Subject {get;set;}
    public String BodyFromPage {get;set;}
    public Id EventId {get;set;}
    public list<EventRelation> InterviewersInEvent {get;set;}
    public List<EmailTemplate> saveThDateTemp {get;set;}
    public static final string SAVETHEDATETEMP_NAME = 'Interviewer Save the Date - Campus Recruiting' ;
    public string evDescription {get;set;}
    public string evLocation {get;set;}
    public string emailSubject {get;set;}
    public String  evEndDateTimeStr {get;set;}
    public String evStartDateTimeStr {get;set;}
    public string userTimeZone {get;set;}
    public string EventStartTime {get;set;}
    public string EventEndTime {get;set;}
    Datetime evStartDateTime;
    
    public Map<string,string> inviteeEmailMap {get;set;}
    public boolean noInvitee {get;set;}
    //Cancel Method
    public pagereference Cancel(){
        return new Pagereference('/'+EventId);
    }
    
    /**********************Date to GMT convetion****************/    
    private static String DateFormatConv(DateTime dt){
        String myDate = string.valueOfGMT(dt);
        String stringDate = myDate.substring(0,4) + '' +
                            myDate.substring(5,7) + '' + 
                            myDate.substring(8,10) + '' +
                            myDate.substring(10,10) + 'T' +
                            myDate.substring(11,13) + '' +
                            myDate.substring(14,16)+ ''+ 
                            myDate.substring(17,19)+ 'Z';
       return stringDate;
    }
    //Controller
    public WCT_SaveTheDateController(){
        inviteeEmailMap = new Map<String,String>();
        EventId = ApexPages.currentpage().getParameters().get('Id');
        system.debug('%%%12'+EventId);
        saveThDateTemp = new List<EmailTemplate>();
        User u = [select timezonesidkey from user where id=:UserInfo.getUserId()];
        if(u.TimeZoneSidKey!=null)userTimeZone = u.TimeZoneSidKey;
        saveThDateTemp = [SELECT Id,Name,Body,subject,FolderId,HtmlValue,TemplateType FROM EmailTemplate where Name = :SAVETHEDATETEMP_NAME ];
        if(!saveThDateTemp.isEmpty()){
            EmailBodyFromTemp = saveThDateTemp[0].HtmlValue ;
        }
        BodyFromPage = '<html>'+'<body>'+'<table><tr><img alt="" src="https://talent--c.na2.content.force.com/servlet/servlet.ImageServer?id=01540000001BWuB&oid=00D40000000MxFD" align="left" border="0"></img></tr><Br/><Br/><tr>';
        BodyFromPage += EmailBodyFromTemp+'</tr></table>';
        BodyFromPage =BodyFromPage.replaceAll(']]>','');
        BodyFromPage +='</boby>'+'</html>';
        InterviewersInEvent = new list<EventRelation>();
         
        InterviewersInEvent= [SELECT AccountId,EventId,IsInvitee,RelationId,relation.email,relation.name,Status, Event.Description, Event.EndDateTime, 
                                    Event.Location,  Event.StartDateTime, Event.What.Name, Event.IsDateEdited__c, Event.Subject,Event.WCT_Save_The_Date_EventId__c 
                                    FROM EventRelation WHERE EventId = :EventId and IsInvitee=true and Relation.type = 'User' and Status = 'New'];
                                    
        if(!InterviewersInEvent.isEmpty()){
                noInvitee = false;
                if(InterviewersInEvent[0].Event.Description!=null)evDescription = InterviewersInEvent[0].Event.Description; 
                if(InterviewersInEvent[0].Event.EndDateTime!=null)evEndDateTimeStr = DateFormatConv(InterviewersInEvent[0].Event.EndDateTime); 
                if(InterviewersInEvent[0].Event.Location!=null)evLocation = InterviewersInEvent[0].Event.Location; 
                if(InterviewersInEvent[0].Event.StartDateTime!=null)evStartDateTimeStr = DateFormatConv(InterviewersInEvent[0].Event.StartDateTime); 
                if(InterviewersInEvent[0].Event.StartDateTime!=null)evStartDateTime = InterviewersInEvent[0].Event.StartDateTime;
                if(InterviewersInEvent[0].Event.subject!=null)emailSubject = InterviewersInEvent[0].Event.Subject;
                subject = emailSubject +'[Id: '+InterviewersInEvent[0].Event.WCT_Save_The_Date_EventId__c+']';
                EventStartTime = InterviewersInEvent[0].Event.StartDateTime.format('MM/dd/yyyy HH:mm:ss a');
                EventEndTime = InterviewersInEvent[0].Event.EndDateTime.format('MM/dd/yyyy HH:mm:ss a');
        }else{noInvitee = true;}
    
    }
    
    //Method to Create ICS Invite
    private static Blob doIcsAttachment(String EventId, String startDate, String endDate, String location, String body, 
                                        String subject, String fromAddress, String displayName, 
                                        Datetime evStartDateTime, String evLocation,Map<string,string> inviteeEmails) {
        Map<string,string> inviteeEmailsMap = inviteeEmails;
        string Attendees = '';
        string evId = EventId.substring(0,15);
        for(String str : inviteeEmailsMap.keyset()){
            Attendees += 'ATTENDEE;CN="' + inviteeEmailsMap.get(str)+'";ROLE=REQ-PARTICIPANT;RSVP=TRUE:mailto: ' +str+'\n';
        }
        system.debug('%%%12'+EventId+evId );
        body = body.replaceAll('%0A','+');
        system.debug('%%%1'+body +'decoded'+EncodingUtil.urlDecode(body, 'UTF-8'));
       // Attendees = Attendees.substring(0,(Attendees.length()-2));
       // system.debug('%%%'+Attendees );
        if (location== null){location='';}
        String [] icsTemplate = new List<String> {  
            'BEGIN:VCALENDAR',
            'PRODID:-//Microsoft Corporation//Outlook 14.0 MIMEDIR//EN',
            'VERSION:2.0',
            'METHOD:REQUEST',
            //'X-MS-OLK-FORCEINSPECTOROPEN:TRUE',
            //'BEGIN:VTIMEZONE',
            //'TZID:Central Standard Time',
            //'BEGIN:STANDARD',
            //'DTSTART:16011104T020000',
            //'RRULE:FREQ=YEARLY;BYDAY=1SU;BYMONTH=11',
            //'TZOFFSETFROM:-0500',
            //'TZOFFSETTO:-0600',
            //'END:STANDARD',
            //'BEGIN:DAYLIGHT',
            //'DTSTART:16010311T020000',
            //'RRULE:FREQ=YEARLY;BYDAY=2SU;BYMONTH=3',
            //'TZOFFSETFROM:-0600',
            //'TZOFFSETTO:-0500',
            //'END:DAYLIGHT',
            //'END:VTIMEZONE',
            'BEGIN:VEVENT',
            'CLASS:PUBLIC',
            'CREATED:20140217T210432Z',
            'DESCRIPTION: '+ EncodingUtil.urlDecode(body, 'UTF-8'),
            'DTSTART:'+startDate,
            'DTSTAMP:'+startDate,
            'DTEND:'+endDate,
            'LAST-MODIFIED:20140217T210432Z',
            'LOCATION:'+ location,
            //'PRIORITY:5',
            //'SEQUENCE:0',
            'SUMMARY;LANGUAGE=en-us:'+ subject,
            'TRANSP:OPAQUE',
            'ORGANIZER;CN=eInterview:MAILTO:' + fromAddress,
            Attendees,
            'ATTENDEE;CN="' + userInfo.getName()+ '";ROLE=OPT-PARTICIPANT;RSVP=TRUE:mailto: ' + userinfo.getUserEmail(),
            'UID:'+ evId,
            'X-ALT-DESC;FMTTYPE=text/html:'+ EncodingUtil.urlDecode(body, 'UTF-8'),
            'X-MICROSOFT-CDO-BUSYSTATUS:BUSY',
            'X-MICROSOFT-CDO-IMPORTANCE:1',
            'X-MICROSOFT-DISALLOW-COUNTER:FALSE',
            'X-MS-OLK-AUTOFILLLOCATION:FALSE',
            'X-MS-OLK-CONFTYPE:0',
            'BEGIN:VALARM',
            'TRIGGER:-PT30M',
            'ACTION:DISPLAY',
            'DESCRIPTION:Reminder',
            'END:VALARM',
            'END:VEVENT',
            'END:VCALENDAR'
        };
        String attachment = String.join(icsTemplate, '\n');
        system.debug('%%%'+icsTemplate);
        return Blob.valueof(attachment ); 

    }
    /** 
        Method Name  : sendInvite
        Return Type  : Page Reference to Event
        Description  : Used to send Invite to all Invitees in event record        
    */
    
    Public pageReference sendInvite(){
         if(!InterviewersInEvent.isEmpty()){
            
                for(EventRelation er : InterviewersInEvent){
                    inviteeEmailMap.put(er.relation.email,er.relation.Name);
                }
                List<string> fromAdd = new List<string>();
                 fromAdd.AddAll(inviteeEmailMap.keySet());
                Messaging.SingleEmailMessage msg = new Messaging.SingleEmailMessage();  
                msg.setReplyTo(system.label.WCT_Save_The_Date_Email_Id);
                msg.setSenderDisplayName('eInterview');
                msg.setToAddresses(fromAdd);
                msg.setCcAddresses(new String[] {userinfo.getUserEmail()});// it is optional, only if required
                msg.setSubject(subject);
                msg.setHtmlBody(BodyFromPage);

                // Create the attachment
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName('Meeting.ics' );
                string encodedBody = EncodingUtil.urlEncode(BodyFromPage, 'UTF-8');
                Blob b = doIcsAttachment(EventId, evStartDateTimeStr,evEndDateTimeStr,evLocation, encodedBody, 
                                        emailSubject, system.label.WCT_Save_The_Date_Email_Id, 'eInterview',
                                        evStartDateTime, evLocation,inviteeEmailMap);
                efa.setBody(b);
                
                efa.setContentType('text/Calendar');
                efa.setInLine(true);//New
                msg.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
                // Send the email you have created.
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { msg });
                Event ev = new Event(id=EventId);
                ev.WCT_Event_Status__c = 'Invite Sent';
                Update ev;
         }  
         return new Pagereference('/'+EventId);
    }
    
}