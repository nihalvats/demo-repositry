global class WCT_Batch_Offer_Status_SentOffer_Update implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.StateFul {  
    
    List<Wct_Offer__c> lstOff = new List<Wct_Offer__c>();
    global String sSessionID;
    global List<String> urls;
    
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
        Date offerCreatedDate = Date.valueOf(System.Label.WCT_Offer_CreatedDate);
        String query = 'SELECT id, WCT_First_Name__c , WCT_Last_Name__c , Offer_Password__c, WCT_Candidate__c, Offer_Letter_Template_Id__c, WCT_status__c, WCT_Offer_Letter_Sent__c, WCT_User_Group__c, Send_Email_Copies_to__c, WCT_Message__c FROM Wct_Offer__c WHERE WCT_status__c =\'Offer Approved\' and WCT_Offer_Letter_Sent__c=false and WCT_Manual_Offer__c=false and createddate >=:offerCreatedDate';
        return Database.getQueryLocator(query);
    }
    
    global WCT_Batch_Offer_Status_SentOffer_Update(string sessionId) 
    {
          sSessionID = sessionId;
          
    }

    global WCT_Batch_Offer_Status_SentOffer_Update()
    {
    
    }        
    
    global void execute(Database.BatchableContext BC, List<Wct_Offer__c> offerList) 
    {   
       String sOrgID = UserInfo.getOrganizationId().substring(0,15);
       Id userID = UserInfo.getUserId();
       String sCongaUrl = 'https://www.appextremes.com/apps/Conga/PM.aspx?sessionId=';
       urls = new List<String>();
       Id QueryId = label.WCT_Conga_Query_Id;

       for (WCT_Offer__c OffRecord: offerList)
       {
          if(OffRecord!=NULL && OffRecord.WCT_status__c=='Offer Approved' && OffRecord.WCT_Offer_Letter_Sent__c==false)
          { 
              UsernamePasswordFlow upf = new UsernamePasswordFlow(label.WCT_Oauth_UsernameFlow,label.WCT_Oauth_PasswordFlow,label.WCT_Oauth_ClientKey,label.WCT_Oauth_ClientTokenID);
             
              try
              {
                  sSessionID = upf.requestAccessToken();
              }
              catch(Exception e)
              {
              }
              
              Id TemplateId = OffRecord.Offer_Letter_Template_Id__c;
              String offerId = OffRecord.ID;
              offerId = offerId.substring(0,15);
              String message = label.ESOfferMessage;
              
             if(OffRecord.WCT_Message__c != '' && OffRecord.WCT_Message__c != null) 
             {
                      message = EncodingUtil.urlEncode(OffRecord.WCT_Message__c, 'UTF-8');
             } else 
             {
                      message = label.ESOfferMessage;
             }              
              
              
              String docUrl = sCongaUrl
                            + sSessionID
                            + '&serverUrl='
                            + EncodingUtil.urlEncode( URL.getSalesforceBaseUrl().toExternalForm(), 'UTF-8')
                            + '%2Fservices%2FSoap%2Fu%2F9.0%2F'
                            + sOrgID
                            + '&Id='
                            + offerId
                            + '&TemplateId='
                            + TemplateId 
                            + '&QueryId=[Contact]'
                            + QueryId
                            + '&DefaultPDF=1'
                            + '&OFN=Deloitte_Offer_Letter'
                            + '&ESVisible=1'
                            + '&ESExpireInXDays=120' 
                            + '&ESMessage='
                            + message                            
                            + '&ESRemindRecipient=1' 
                            + '&ESContactId='
                            + OffRecord.WCT_Candidate__c
                            + '&ESAgreementName=Deloitte' + '+' + 'Offer'
                            + '&ESSignatureType=2'
                            + '&ESCustomField1=WCT_Offer__c'
                            + '&ESCustomFieldValue1='
                            + offerId
                            + '&ESCustomField2=WCT_isCreatedByBatch__c'
                            + '&ESCustomFieldValue2=true'                            
                            + '&ESSVBA=1&DS7=14';
                  urls.add(docUrl);
               
           }
       }
       
       for(String sURL : urls)
       {        
            Http oHttp = new Http();          
            HttpRequest oHttpReq = new HttpRequest();            
            oHttpReq.setEndpoint(sURL);
            oHttpReq.setMethod('GET');
            oHttpReq.setTimeout(120000);
            HttpResponse oHttpRes;
            try
            {
                oHttpRes = oHttp.send(oHttpReq);
               
            }
            catch(CalloutException e)
            {
              WCT_ExceptionUtility.logException('WCT_Batch_Offer_Status_SentOffer_Update-execute', null, e.getMessage() + ' ::: ' + e.getStackTraceString());
            }
       }
    }
    
    global void finish(Database.BatchableContext BC)
    {
            
    }
}