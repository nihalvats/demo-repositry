@isTest
public class WCT_Upload_Wagenotices_Test{
    
   
    public static final id candRecTypeId=WCT_Util.getRecordTypeIdByLabel('Contact','Employee');
    
   static testMethod void WCT_Upload_Wagenotices_TestMethod(){  
    list<contact> conlist = new list<contact>();
        contact con = new contact();
        con.lastname = 'test';
        con.WCT_Personnel_Number__c = 12345;
        con.WCT_Benefit_plan__c = 'EE';
        con.Dependent__c = 'Freind';
        con.Plan_Num__c = 123;
        con.Recordtypeid = candRecTypeId;
        conlist.add(con); 
        insert conlist;
        
       integer size = 0;  
     List<Decimal> personnelNoList = new List<Decimal>();    
   Map<String, Id> conIdPersonnelNoMap = new Map<String, Id>();

   /* String csvcontent = 'Personnel Number,At Hire,Current Employee,Annual - Current Date,Change in pay rates(s) or payday,Effective Date,Company Name,'+
            'DBA,Employer Permanent Address,Street Line 2,City,State,'+
            'Zip Code,Same as Physical Address,Mailing Address,Street Line 2,'+
            'City,State,Zip Code,Same as Physical Address,Mailing Address,Street Line 2,city';
     string   data = '12345,TRUE,FALSE,FALSE,FALSE,11/15/2015,Deloitte Services LP,'+
            'Yes,11501 Zane Circle N,Test Street,Champlin,MN,553162929,FALSE,Test Address,Test Street1,champin,Test state,517501,9849871535, Wilson Blvd,Suite 1450,Test City2';
      String blobCreator = csvcontent + '\r\n' + data ;
    */  
      //Negative record
     
      String csvcontent1 = 'Personnel Number, At Hire, Current Employee, Annual - Current Date, Change in pay rates(s) or payday, Effective Date, Company Name, DBA, Employer Permanent Address, Street Line 2, City, State, Zip Code, Same as Physical Address, Mailing Address, Street Line 2, City, State, Zip Code, Phone, Employee Name, Physical Address, City, State, Zip Code, Same as Physical Address, Mailing Address, Street Line 2, City, State, Zip Code, Phone, Pay Frequency, Designated Pay Day, Tips, Meals, Lodging, Other, Minimum Wage, Living Wage, Living Wage Exempt, Employer Determined Wage Rate, Pay Basis, Hourly, Salary, Rate of Pay Per Week [could this change to annual salary?], Overtime Rate of Pay Per Hour, Overtime Pay Exemption for bona fide – Administrative, Overtime Pay Exemption for bona fide –Executive, Overtime Pay Exemption for bona fide –Professional, Overtime Pay Exemption for bona fide – Other, Rate of Pay 1, Rate of Pay 2, Rate of Pay 3, Overtime Rate 1, Overtime Rate 2, Overtime Rate 3, Classification 1, Classification 2, Classification 3, Prevailing Rate 1, Prevailing Rate 2, Prevailing Rate 3, Exempt/Non Exempt, Overtime Pay Exemption for Bona Fide, Transfer, Invalid Acknowledgement';
     
     string   data1 = '12345,TRUE,FALSE,FALSE,FALSE,11/15/2015, Deloitte Consulting LLP, '+
                         'Yes, 556 12th St. N.W., Suite 401, Washington, D.C., 20004, FALSE, Andhra Pradesh, Madurai street, Madurai, tamilnadu, 5644654, 203 879 5600, '+
                         'Siva Kumar Valluru, 1235 Oak Street, Washington, D.C., 20004, FALSE, AndhraPradesh, Madurai street,Madurai, tamilnadu, 625009, '+
                         '202-124-4568, bi-weekly, Friday,,,,,,,, TRUE, Day, TRUE,, 2000, 500,, TRUE,,,,,,,,, testClassification1, testClassification2, testClassification3, 5665, 555, 666, TRUE,TRUE,Entity Transfer,FALSE';

      String blobCreator1 = csvcontent1 + '\r\n' + data1 ;
      
      WCT_parseCSV parseCSVInstance = new WCT_parseCSV();  
       string namefile = 'test.csv';
      List<List<String>> fileLines = new List<List<String>>(); 
       filelines = parseCSVInstance.parseCSV(nameFile, true);
        string extension = namefile.substring(namefile.lastindexof('.')+1);
        
    
       
       
        test.startTest();
        pagereference p = page.WCT_Upload_Wagenotices;
       Test.SetCurrentPage(p); 
       list<WTPAA_Wage_Notice__c> wlistg = new list<WTPAA_Wage_Notice__c>();
        
        WCT_Upload_Wagenotices cont= new WCT_Upload_Wagenotices();
      //  cont.contentFile = Blob.valueof(blobCreator);
        cont.contentFile = Blob.valueof(blobCreator1);
       //   List<WCT_Upload_Wagenotices.MyWrapper> wraplist = new List<WCT_Upload_Wagenotices.MyWrapper>();
       cont.ReadFile();
       cont.uploadProcess();
       cont.pg();
       cont.getFailedRows();
       ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error');
        ApexPages.addMessage(msg);

       list<WTPAA_Wage_Notice__c> lst= new list<WTPAA_Wage_Notice__c>();
      Database.SaveResult[] srList = Database.insert(lst, false);
       WTPAA_Wage_Notice__c wg = new WTPAA_Wage_Notice__c();
     /*  wg.WTPAA_Personnel_Number__c = string.valueof(12345);
       wg.WTPAA_Related_To__c = con.id;
       wg.WTPAA_Status__c = 'New';
       wg.WTPAA_File_Name__c = namefile ;
       wg.WTPAA_Notice_Type__c = 'At Hire';
       wg.WTPAA_DBA__c = 'Yes';
       wg.WTPAA_Company_Name__c = 'Deloitte Services LP';
       wg.WTPAA_Is_same_Employer_Mailing_Address__c = FALSE;
       wg.WTPAA_Employer_Permanent_Address__c = '11501 Zane Circle N';
       wg.WTPAA_Employer_Permanent_Street_Line_2__c = 'Test Street';
       wg.WTPAA_Employer_Mailing_Address__c = 'Test Address';
       wg.WTPAA_Employer_Mailing_Street_Line_2__c = 'Test Street1';
       wg.WTPAA_Employer_Mailing_City__c = 'champin';
       wg.WTPAA_Employer_Mailing_State__c ='Test state';
       wg.WTPAA_Employer_Mailing_Zip_Code__c = 517501;
       
       
       wg.WTPAA_Employee_Physical_City__c = 'Test city2';
       wg.WTPAA_Employee_Physical_Address__c = 'Suite 1450';
       wg.Employee_Name__c  = 'Wilson Blvd';
       wg.WTPAA_Employer_Mailing_Phone__c = '9849871535';
       wg.WTPAA_Employer_Permanent_City__c =  'champin';
       wg.WTPAA_Employer_Permanent_State__c = 'MN';
       wg.WTPAA_Employer_Permanent_Zip_Code__c = 553162929;
      wg.WTPAA_Effective_Date__c = System.today(); 
     */
     
       wg.WTPAA_Personnel_Number__c = string.valueof(12345);
       wg.WTPAA_Related_To__c = con.id;
       wg.WTPAA_Status__c = 'New';
       wg.WTPAA_Pay_Frequency__c = 'bi-weekly';
       wg.WTPAA_Designated_Pay_Day__c = 'Friday';
       wg.WTPAA_File_Name__c = namefile ;
       wg.WTPAA_Notice_Type__c = 'At Hire';
       wg.WTPAA_DBA__c = 'Yes'; 
        wg.WTPAA_Company_Name__c ='Deloitte Consulting LLP';
        wg.WTPAA_Employer_Permanent_Address__c = '556 12th St. N.W.';
        wg.WTPAA_Employer_Permanent_Street_Line_2__c = 'Suite 401';
        wg.WTPAA_Employer_Permanent_City__c = 'Washington';
        wg.WTPAA_Employer_Permanent_State__c = 'D.C.';
        wg.WTPAA_Employer_Permanent_Zip_Code__c = 20004;
        wg.WTPAA_Is_same_Employer_Mailing_Address__c = FALSE;
        wg.WTPAA_Employer_Mailing_Address__c = 'Andhra Pradesh';
        wg.WTPAA_Employer_Mailing_Street_Line_2__c = 'Madurai street';
        wg.WTPAA_Employer_Mailing_City__c = 'Madurai';
        wg.WTPAA_Employer_Mailing_State__c ='tamilnadu';
        wg.WTPAA_Employer_Mailing_Zip_Code__c = 5644654;
        wg.WTPAA_Employer_Mailing_Phone__c = '203 879 5600';
        wg.Employee_Name__c  = 'Siva Kumar Valluru';
        wg.WTPAA_Employee_Physical_Address__c = '1235 Oak Street';
        wg.WTPAA_Employee_Physical_City__c ='Washington';
        wg.WTPAA_Employee_Physical_State__c ='D.C.';
        wg.WTPAA_Employee_Physical_Zip_Code__c = 20004;
        wg.WTPAA_Is_same_Employee_Mailing_Address__c = FALSE;
        wg.WTPAA_Employee_Mailing_Address__c ='AndhraPradesh';
        wg.WTPAA_Employee_Mailing_Street_Line_2__c = 'Madurai street';
        wg.WTPAA_Employee_Mailing_City__c  = 'Madurai';
        wg.WTPAA_Employee_Mailing_State__c ='tamilnadu';
        wg.WTPAA_Employee_Mailing_Zip_Code__c =625009;
        wg.WTPAA_Employee_Mailing_Phone__c  = '202-124-4568';
        wg.WTPAA_Minimum_Wage__c = TRUE;
       
       lst.add(wg);
       
       insert lst;
     
    
      
       test.stoptest();
       
   }
  }