public class WCT_BatchToDToDoFailureHandler implements Database.Batchable<sObject>, Database.AllowsCallouts { 
    
    // Relation Records Failure Statuses
    private static final String[] FAILURE_STATUSES = new String[] {'Create Failed'};    
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        // Pull the relation records with failure statuses
        String query = 'SELECT ' +
                            'Id, SFDC_Task_Id__c, ToD_TODO_Id__c, Status__c, Status_Details__c, UpdatedDate__c, CreatedDate  ' +
                        'FROM ' +
                            'WCT_ToD_Task_ToDo_Relation__c ' +
                        'WHERE ' +
                            'Status__c in :FAILURE_STATUSES';
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<WCT_ToD_Task_ToDo_Relation__c> listFailedRecords) { 
        Map<String, WCT_ToD_Task_ToDo_Relation__c> mapCreateFailedRecords = new Map<String, WCT_ToD_Task_ToDo_Relation__c>();
        Map<String, WCT_ToD_Task_ToDo_Relation__c> mapDeleteFailedRecords = new Map<String, WCT_ToD_Task_ToDo_Relation__c>();
        List<String> listTaskIds = new List<String>();
        
       // WCT_ToD_Task_ToDo_Relation__c ToDTaskStatus = new WCT_ToD_Task_ToDo_Relation__c();
       // for(WCT_ToD_Task_ToDo_Relation__c dueRecords : listFailedRecords){
       //     if((dueRecords.Status__c == 'Created' || dueRecords.Status__c == 'Updated') && dueRecords.UpdatedDate__c <= System.today().addDays(-20)){
       //         dueRecords.Status__c = 'Ready for Update';
       //         dueRecords.WCT_Next_Due_Date__c = System.today().addDays(25);
       //     }
       // }
       // update listFailedRecords;
        
        // Parse the relation-records and separate out the 'Create Failed' and 'Delete Failed' records
        for(WCT_ToD_Task_ToDo_Relation__c failedRecord : listFailedRecords) {
            if(failedRecord.Status__c == 'Create Failed') {
                mapCreateFailedRecords.put(failedRecord.SFDC_Task_Id__c, failedRecord);
            }
           // else if(failedRecord.Status__c == 'Delete Failed') {
             //   mapDeleteFailedRecords.put(failedRecord.SFDC_Task_Id__c, failedRecord);
           // }
            listTaskIds.add(failedRecord.SFDC_Task_Id__c);
        }
        
        // Pull all the required task-details from the DB
        Map<Id, Task> mapTasks = new Map<Id, Task>([SELECT Id, WCT_Is_Visible_in_TOD__c FROM TASK WHERE Id IN :listTaskIds]);
        
        if(0 < mapCreateFailedRecords.size()) {
            processCreateFailedRecords(mapCreateFailedRecords, mapTasks);
        }
        
       // if(0 < mapDeleteFailedRecords.size()) {
         //   processDeleteFailedRecords(mapDeleteFailedRecords, mapTasks);
       // }
    }
    
    public void finish(Database.BatchableContext BC){    
    }
    
    private void processCreateFailedRecords(Map<String, WCT_ToD_Task_ToDo_Relation__c> mapFailedRecords, Map<Id, Task> mapTasks) {
        List<WCT_ToD_Task_ToDo_Relation__c> listToBeDeleted = new List<WCT_ToD_Task_ToDo_Relation__c>();
        List<Id> listTaskIds = new List<String>();
        
        // Pick the ones for which the task is not yet complete and delete the others
        for(WCT_ToD_Task_ToDo_Relation__c failedRecord : mapFailedRecords.values()) {
            Task t = mapTasks.get(failedRecord.SFDC_Task_Id__c);
            if( (null == t) || (false == t.WCT_Is_Visible_in_TOD__c) ) {
                listToBeDeleted.add(failedRecord);
            }
            else {
                listTaskIds.add(t.id);
            }            
        }
        
        WCT_ToDToDoItemsManager.addToDo(listTaskIds);        
    }
    
  /*  private void processDeleteFailedRecords(Map<String, WCT_ToD_Task_ToDo_Relation__c> mapFailedRecords, Map<Id, Task> mapTasks) {
        List<Id> listTaskIds = new List<Id>();
        listTaskIds.addAll(mapTasks.keySet());
        WCT_ToDToDoItemsManager.deleteToDo(listTaskIds);
    }*/
}